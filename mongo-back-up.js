const MBackup = require('s3-mongo-backup');

// const sendmail = require('sendmail')({smtpPort: 2525});
const mailer = require('./server/common/mailer');


const package = require('./package.json');

var cron = require('node-cron');


const initialize = (dbConfig, s3Config, backUpConfig) => {

  return new Promise((resolve, reject) => {
    console.log(dbConfig);

    if (!dbConfig.database || !dbConfig.host || !dbConfig.port) {
      reject("Invalid dbConfig"); return;
    }

    if (s3Config.accessKey && s3Config.secretKey && s3Config.region && s3Config.accessPerm && s3Config.bucketName) {
    } else {
      reject("Invalid s3Config"); return;
    }

    if (backUpConfig.cronJobTimeString) {
    } else {
      reject("Invalid backUpConfig"); return;
    }


    resolve(null);

    cron.schedule(backUpConfig.cronJobTimeString, () => {

      var d = new Date();

      s3Config.bucketName = s3Config.bucketName;
      var backupConfig = {
        mongodb: dbConfig,
        s3: s3Config,
        keepLocalBackups: false,  //If true, It'll create a folder in project root with database's name and store backups in it and if it's false, It'll use temporary directory of OS
        noOfLocalBackups: 5, //This will only keep the most recent 5 backups and delete all older backups from local backup directory
        timezoneOffset: 0 //Timezone, It is assumed to be in hours if less than 16 and in minutes otherwise
      }
      MBackup(backupConfig)
        .then(done => {

          var AWS = require('aws-sdk');
          AWS.config.update({
            accessKeyId: s3Config.accessKey,
            secretAccessKey: s3Config.secretKey
          });
          var s3 = new AWS.S3();
          var params = {
            Bucket: s3Config.bucketName, /* Another bucket working fine */
            CopySource: s3Config.bucketName + "/" + done.data.key, /* required */
            Key: dbConfig.database + ".gz", /* required */
          };
          s3.copyObject(params, function (err, data) {
            if (err)
              console.log("err", err); // an error occurred
            else {
              console.log(data); // successful response
              params = {
                Bucket: s3Config.bucketName, /* Another bucket working fine */
                Key: done.data.key, /* required */
              };
              s3.deleteObject(params, function (err, data) {
                if (err)
                  console.log("err", err); // an error occurred
                else {
                  console.log(data); // successful response
                }
              });
            }
          });
          console.log(done);
          if (backUpConfig.mailIds && backUpConfig.fromMailId) {
            mailer.sendMail(backUpConfig.mailIds, backUpConfig.fromMailId, 'Mongo Back up - ' + package.name, "ZenworkHR", "<h3>Mongo backup has happened with key <b>" + done.data.key + "</b> in <b>" + s3Config.bucketName + "</b> bucket.</h3>")
            // sendmail({
            //   from: backUpConfig.fromMailId,
            //   to: backUpConfig.mailIds,
            //   subject: 'Mongo Back up - ' + package.name,
            //   html: "<h3>Mongo backup has happened with key <b>" + done.data.key + "</b> in <b>" + s3Config.bucketName + "</b> bucket.</h3>",
            // }, function (err, reply) {
            //   console.log(err && err.stack);
            //   console.dir(reply);
            //   if (err) {
            //     console.log("Error while mail sending");
            //   } else {
            //     console.log("Mail sent successfully");
            //   }
            // });
          }
          console.log("Back up done", done);
        }).catch(err => {
          console.log(err);
          reject(err);
        });
    }, {
      scheduled: true,
      timezone: "Asia/Kolkata"
    });
  });
}


module.exports = initialize;





// mongorestore --host localhost --port=27017 --archive=zenwork_hr_dev_2019-01-25T12-14-27
