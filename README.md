# README #

### Repo Summary ###
Backend API for Zenwork HR

### Set up ###

git clone

cd zenwork-hr-node

npm i

npm start

###  Dependencies ###
* [Node.js](https://nodejs.org/)
* [Express.js](https://expressjs.com/)
* [MongoDB](https://www.mongodb.com/)

### Deployment instructions ###

Steps to restart server in AWS instance:

1) SSH into server using pem file and elastic ip

2) cd zenwork-hr-node

3) git pull origin sprint3 

4) pm2 restart 0