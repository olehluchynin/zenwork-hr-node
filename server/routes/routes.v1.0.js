let express = require('express');
let multiparty = require('connect-multiparty');
let multipartymiddleware = multiparty();

let router = express.Router();


let UserRouter = require('../api/v1.0/users/route');

let ConstantsRouter = require('../api/v1.0/constants/route');

let myinfoRouter = require('../api/v1.0/myInfo/route');

let assignedTrainingsRouter = require('../api/v1.0/assigned-trainings/route');


router.use('/zenworkers', require('../api/v1.0/zenworkers/route'));

router.use('/companies', require('../api/v1.0/companies/route'));

router.use('/packages', require('../api/v1.0/packages/route'));

router.use('/ad-ons', require('../api/v1.0/ad-ons/route'));

router.use('/coupons', require('../api/v1.0/coupons/route'));

router.use('/plans', require('../api/v1.0/plans/route'));

router.use('/orders', require('../api/v1.0/orders/route'));

router.use('/employees', require('../api/v1.0/employees/route'));

router.use('/timeSchedule', require('../api/v1.0/leaveManagement/route'));

router.use('/zenwork', require('../api/v1.0/zenwork/route'));

router.use('/roles', require('../api/v1.0/roles/route'));

router.use('/pages', require('../api/v1.0/pages/route'));

router.use('/workflow', require('../api/v1.0/workflow/router'));

router.use('/offboarding', require('../api/v1.0/offboarding/router'));

router.use('/training', require('../api/v1.0/training/route'));

router.use('/structure', require('../api/v1.0/settings-structure/route'));

router.use('/employee-management', require('../api/v1.0/employee-management/route'));

router.use('/users', UserRouter);

router.use('/constants', ConstantsRouter);

router.use('/myinfo', myinfoRouter);

router.use('/assigned-trainings', assignedTrainingsRouter);

router.use('/manager-requests', require('../api/v1.0/manager-requests/route'));

router.use('/notifications', require('../api/v1.0/inbox/route'));

router.use('/leaveEligibilityGroup', require('../api/v1.0/leaveManagement/leaveEligibilityGroup/route'));

router.use('/timeOffPolicies', require('../api/v1.0/leaveManagement/TimeOffPolicies/route'));

router.use('/scheduler', require('../api/v1.0/leaveManagement/Scheduler/route'));

router.use('/contact-us', require('../api/v1.0/contact-us/route'));

router.use('/manager-front-page', require('../api/v1.0/manager-front-page/route'));

router.use('/employee-front-page', require('../api/v1.0/employee-front-page/route'));

router.use('/payroll-groups', require('../api/v1.0/payroll-groups/route'));

router.use('/salary-grades', require('../api/v1.0/salary-grades/route'));

router.use('/payroll-calendars', require('../api/v1.0/payroll-calendars/route'));

router.post('/users/uploader', multipartymiddleware, function (req, res) {
    var fs = require('fs');

    fs.readFile(req.files.upload.path, function (err, data) {
        var timeStamp = new Date() - 0;
        var newPath = 'client/images/editorImages/' + timeStamp + "" + req.files.upload.name;
        fs.writeFile(newPath, data, function (err) {
            if (err) {
                res.send(err)
            }
            else {
                html = "";
                html += "<script type='text/javascript'>";
                html += "    var funcNum = " + req.query.CKEditorFuncNum + ";";
                html += "    var url     = \"/images/editorImages/" + timeStamp + "" + req.files.upload.name + "\";";
                html += "    var message = \"Uploaded file successfully\";";
                html += "";
                html += "    window.parent.CKEDITOR.tools.callFunction(funcNum, url, message);";
                html += "</script>";
                res.send(html);
            }
        });
    });
});

module.exports = router;
