let express = require('express');
let router = express.Router();

let clientRouter = require('../api/v1.0/client-demo/route');

router.use('/client',clientRouter);

module.exports = router;