let _ = require('lodash');

// process.env.NODE_ENV = 'local'; // local,dev, qa, production
console.log('process.env.NODE_ENV ', process.env.NODE_ENV)

let config = _.merge(require('./env/' + process.env.NODE_ENV + '.js') || {});


// switch(process.env.NODE_ENV) {
//     case 'dev':
//         break;
//     case 'qa':
//         break;
//     case 'production':
//         break;
// }

module.exports = config;