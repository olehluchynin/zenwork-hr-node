module.exports = {
    port : 3003,
    jwt: {
        secret: "SECRET_KEY",
        options: {expiresIn: 365 * 60 * 60 * 24} // 365 days
    },
    // db : {
    //     mongo : {
    //         uri:"mongodb://localhost:27017/zenwork_hr_dev",
    //         options: {
    //             useNewUrlParser: true,
    //             user: '',
    //             pass: '',
    //         }
    //     }
    // },
    db : {
        mongo : {
            uri:"mongodb://34.200.79.60:27017/zenwork_hr_dev",
            database: "zenwork_hr_dev",
            host: "34.200.79.60",
            options: {
                useNewUrlParser: true,
                useCreateIndex: true,
                useFindAndModify: false,
                autoIndex: true,
                user: 'ZenworkAdmin',
                pass: 'zenworkhr@MTW@2018',
            }
        }
    },
    emailVericationLinkExpireTime : 365 * 60 * 60 * 24,//365 days
    mail:{
        // defaultFromAddress:'noreply@mtwlabs.com',        
        defaultFromAddress: "info@zenworkhr.com",
        mailGun: {
            apiKey:"apikey",
        },
        sendGrid : {
            apiKey : "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",
        }
    },
    paymentGateway:{
        paytm:{
            MID: '-',
            WEBSITE: '-',
            CHANNEL_ID: '-',
            INDUSTRY_TYPE_ID: '-',
            MERCHANT_KEY : '-'
        },
        razorPay:{
            keyId:'-',
            keySecret:'-'
        },
        payPal: {
            clientId: '-',
            clientSecret: '-'
        }
    },
    aws:{
        bucketName:'-',
        accessKey:'-',
        secretKey:'-',
        region:'-',
        presignedUrlExpiration : 24*60*60 //seconds
    },
    facebook: {
        clientId: 220939165057827,
        clientSecret:"-"
    },

    // baseUrl:'http://localhost:'+3001,
    baseUrl:'http://52.207.167.138:3001',
    frontEndUrl:'http://52.207.167.138:3001/login'
}


