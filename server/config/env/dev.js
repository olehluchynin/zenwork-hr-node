module.exports = {
    port : 3003,
    jwt: {
        secret: "SECRET_KEY",
        options: {expiresIn: 365 * 60 * 60 * 24} // 365 days
    },
    db : {
        mongo : {
            uri:"mongodb://34.200.79.60:27017/zenwork_hr_dev",
            database: "zenwork_hr_dev",
            host: "34.200.79.60",
            options: {
                useNewUrlParser: true,
                useCreateIndex: true,
                useFindAndModify: false,
                autoIndex: true,
                user: 'ZenworkAdmin',
                pass: 'zenworkhr@MTW@2018',
                socketTimeoutMS: 0,
                keepAlive: true,
                reconnectTries: 30
            }
        }
    },
    emailVericationLinkExpireTime : 365 * 60 * 60 * 24,//365 days
    mail:{
        // defaultFromAddress:'noreply@mtwlabs.com',        
        defaultFromAddress: "info@zenworkhr.com",
        mailGun: {
            apiKey:"apikey",
        },
        sendGrid : {
            // apiKey : "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", // mtwlabs
            apiKey: "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"
        }
    },
    paymentGateway:{
        paytm:{
            MID: '-',
            WEBSITE: '-',
            CHANNEL_ID: '-',
            INDUSTRY_TYPE_ID: '-',
            MERCHANT_KEY : '-'
        },
        razorPay:{
            keyId:'-',
            keySecret:'-'
        },
        payPal: {
            clientId: '-',
            clientSecret: '-'
        }
    },
    aws:{
        bucketName:'-',
        accessKey:'-',
        secretKey:'-',
        region:'-',
        presignedUrlExpiration : 24*60*60 //seconds
    },
    facebook: {
        clientId: 220939165057827,
        clientSecret:"-"
    },

    google: {
        clientId: "332522302230-98g9vloe9j8sr1cqv091kfv465dkt72v.apps.googleusercontent.com",
        clientSecret: "AdemDHlMEsqcpiF0VffMC8Zv"
    },

    baseUrl:"http://52.207.167.138:3003",
    // baseUrl:'http://localhost:3003',
    // frontEndUrl:'http://52.207.167.138:3001'
    frontEndUrl:'http://52.207.167.138:3001'
}


