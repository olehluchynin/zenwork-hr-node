let _ = require('lodash');

let config = _.merge(require('./../../../../zenwork-s3-config.json') || {});

module.exports = config;
