let awsConfig = require('./awsConfig');
const AWS = require('aws-sdk/index');

let fs = require('fs');
let s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    accessKeyId: awsConfig.aws.accessKey,
    secretAccessKey: awsConfig.aws.secretKey,
    region: awsConfig.aws.region
});

let uploadFile = function (fileKey, filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, function (err, data) {
            if (err) {
                console.log(err);
                reject(err);
            } else {

                let base64Data = new Buffer(data, 'binary');

                var params = { Bucket: awsConfig.aws.bucketName, Key: fileKey, Body: base64Data};
                s3.upload(params, function (err, s3data) {
                    if (err) {
                        console.log(err);
                        reject(err);
                    } else {
                        resolve(s3data);
                    }
                });
            }
        });
    });
};

let deleteFile = function (fileKey) {
    return new Promise((resolve, reject) => {
        var params = { Bucket: awsConfig.aws.bucketName, Key: fileKey};
        s3.deleteObject(params, function (err, s3data) {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('s3data ==> ',s3data);
                resolve(s3data);
            }
        });
    });
};

let getPresignedUrlOfAfile = function (bucketName, path, obj) {
    return new Promise((resolve, reject) => {
        s3.getSignedUrl('getObject', {
            Bucket: bucketName,
            Key: path+'/'+obj.s3Path,
            Expires: awsConfig.aws.presignedUrlExpiration
        }, function (err, url){
            if (err) {
                console.log(err);
                reject(err);
            } else {
                // console.log(url);
                obj.url = url;
                resolve(obj);
            }
        });
    });
};

module.exports = {
    uploadFile,
    deleteFile,
    getPresignedUrlOfAfile
};
