let jwt = require('jsonwebtoken');
let config = require('./../config/config');

let generatePassword = require('generate-password');

let awsLib = require('./../config/libs/aws');

let uuidv4 = require('uuid/v4');

var ShortUniqueId = require('short-unique-id');

var uid = new ShortUniqueId();

let keyForPasswordEncryption = "YOUR_KEY";

var Cryptr = require('cryptr'),
    cryptr = new Cryptr(keyForPasswordEncryption);

let generateJWTForUser = (userId, companyId, zenworkerId, type) => {
    return jwt.sign({userId, companyId, zenworkerId ,type},config.jwt.secret,config.jwt.options);
}

let decodeJWTForUser = (token)=>{
    // console.log("Decode",jwt.verify(token,config.jwt.secret))
    return jwt.verify(token,config.jwt.secret);
}

let encryptPassword = (password)=>{
    // return cryptr.encrypt(password);
    return password;
}

let decryptPassword = (encryptedPassword)=>{
    // return cryptr.decrypt(encryptedPassword);
    return encryptedPassword;
}

let generateUniqueId = ()=>{
    return uuidv4(); // ⇨ 'df7cca36-3d7a-40f4-8f06-ae03cc22f045'
}

let generateUserEmailVerificationLink = (token,email)=>{
    // return config.baseUrl+'/v1.0/users/verify/email/'+token; // ⇨ 'df7cca36-3d7a-40f4-8f06-ae03cc22f045'

    return config.baseUrl+'/verify-email/'+token; // ⇨ 'df7cca36-3d7a-40f4-8f06-ae03cc22f045'
}

let generateRandomPassword = ()=>{

    // return "123456789";
    return generatePassword.generate({
        length: 8,
        numbers: true,
        symbols:false,
        strict:true
    });
}


let getDayFromDate = (date)=>{
    try{
    let days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    return days[new Date(date).getDay()];
    }catch(err){
        console.log(err);
        throw err;
    }
}


let getUniqueBookingID = ()=>{
    return "BOOK"+uid.randomUUID(8).toLowerCase();
}

let getUniquePaymentID = ()=>{
    return "PAYMENT"+uid.randomUUID(8).toLowerCase();    
}

let getUniqueOrderID = ()=>{
    return "ZEN"+uid.randomUUID(10).toLowerCase();        
}


function formatUserData(userData) {
    if(userData){
        if(userData.password)
            delete userData.password;
        delete userData.isApproved;
    }
    return userData;
}



function formatZenworkerData(userData) {
    if(userData){
        if(userData.password)
            delete userData.password;
    }
    return userData;
}

function formatCompanyData(userData) {
    if(userData){
        if(userData.password)
            delete userData.password;
    }
    return userData;
}


function formatEmployeeData(userData) {
    if(userData){
        if(userData.password)
            delete userData.password;
    }
    return userData;
}



let generatePasswordResetLinkForUser = (email)=>{
    // let baseUrl = config.baseUrl;
    let frontEndUrl = config.frontEndUrl
    // let frontEndUrl = 'http://localhost:4200'
    return frontEndUrl+"/reset-password/"+jwt.sign({email:email},config.jwt.secret,{expiresIn: 10*60});//10 mins
};

let generateEmailVerifyLink = (data)=>{
        let baseUrl = config.baseUrl;
        return baseUrl+"/email-verify/"+jwt.sign(data,config.jwt.secret,{expiresIn: 30*60});//30 mins
};

let decodePasswordResetToken = (token)=>{
    return new Promise((resolve,reject)=>{
        try{
        let decodedData =  jwt.verify(token,config.jwt.secret);
        resolve(decodedData);
        }catch(err){
            reject("Invalid or Expired");
        }
    });
};

module.exports = {
    generateJWTForUser,
    decodeJWTForUser,
    encryptPassword,
    decryptPassword,
    generateUniqueId,
    generateUserEmailVerificationLink,
    generateRandomPassword,
    getDayFromDate,
    getUniqueBookingID,
    getUniquePaymentID,
    getUniqueOrderID,
    formatUserData,
    formatZenworkerData,
    formatEmployeeData,
    generatePasswordResetLinkForUser,
    generateEmailVerifyLink,
    decodePasswordResetToken,
    formatCompanyData
}
