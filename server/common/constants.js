const zenworkRoles = {
    'sr-support':'Sr Support',
    'support':'Support',
    'billing':'Billing'
}

const pageType = {
    'base': 'BasePage',
    'derived': 'DerivedPage'
}


const companyTypes = {
    'company':'Client', //direct company
    'reseller':'Reseller',
    'reseller-dr':'Reseller and Direct Client',
    'reseller-client':'Reseller Client'
}


const userTypes = {
    'sr-support':'Sr Support', //Zenwork
    'support':'Support', //Zenwork
    'administrator':'Administrator',
    'billing':'Billing', //Zenwork
    'company':'Company',//direct company
    'reseller':'Reseller',
    'employee':'Employee',
    'reseller-dr':'Reseller and Direct Client',
    'reseller-client':'Reseller Client',
    "zenworker": "zenworker"
}

const activeStatus = {
    0:'Inactive',
    1:'Active'
}

const roleTypes = {
    0: 'Base',
    1: 'Derived'
};

const roleAccess= {
    'view':'View Only',
    'full':'Full Access',
    'no': 'No Access'
};

const StructureFields = ["Location","Department","Business Unit","Salary Grade","Union"];

const StructureFieldsUpdated = ["Termination Reason","Pay Type","Pay Frequency","Employee Type","FLSA Code","EEO Job Category","Employment Status","Emergency Contact Relationship","Assets","Training Subject","Training Type","Training - Average Time to Complete","Union","Location","Department","Termination Type","Document Type","Visa Type","Visa Status","Schedule Type","Workflow Request Reasons","Martial Status","Veteran Status","Race","Ethnicity","State","Country","Language","Language Fluency","Rehire Status","Onboarding Template Tasks","Offboarding Template Tasks"];



const idNumberType = ["System Generated", "Manual"];

const templateTypes = ['base', 'derived', 'custom'];

const workflowType = {
    'demographic': 'demographic',
    'job': 'job',
    'compensation': 'compensation'
}

const dayOfWeek = {
    'Sunday': 1,
    'Monday': 2,
    'Tuesday': 3,
    'Wednesday': 4,
    'Thursday': 5,
    'Friday': 6,
    'Saturday': 7
}

module.exports = {
    zenworkRoles,
    companyTypes,
    userTypes,
    activeStatus,
    roleAccess,
    StructureFields,
    idNumberType,
    roleTypes,
    pageType,
    templateTypes,
    workflowType,
    dayOfWeek
};
