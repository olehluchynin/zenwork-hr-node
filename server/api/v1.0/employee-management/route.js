let express = require('express');

let router = express.Router();

router.use('/onboarding/onboard-template', require('./onboarding/onboard-template/route'));
router.use('/onboarding/hire-wizard', require('./onboarding/hire-wizard/route'));
router.use('/directory-settings', require('./directory-settings/route'));

module.exports = router;
