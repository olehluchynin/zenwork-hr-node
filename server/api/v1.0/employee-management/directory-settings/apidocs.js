/**
 * @apiDefine DataNotFoundError
 *
 * @apiError DataNotFound The data the you are requested was not found.
 *
 * @apiErrorExample Data Not Found Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "message": "Data Not Found"
 *       "data": null 
 *     }
 */


 /**
 * @apiDefine InternalServerError
 *
 * @apiError InternalServerError Internal Server error.
 *
 * @apiErrorExample Internal Server Error:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "status": false,
 *       "message": "Internal server error"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine TokenNotFound
 *
 * @apiError TokenNotFound User Token Not Found.
 *
 * @apiErrorExample Token Not Found:
 *     HTTP/1.1 400 Token Not Found
 *     {
 *       "status": false,
 *       "message": "Token Not Found"
 *       "data": null 
 *     }
 */

  /**
 * @apiDefine BadRequest
 *
 * @apiError BadRequest Bad request.
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": false,
 *       "message": "Invalid key"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine AccessDeneid
 *
 * @apiError AccessDenied Access denied for this operation.
 *
 * @apiErrorExample Access Deneid:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "status": false,
 *       "message": "Access Denied for this operation"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine DataAlreadyExists
 *
 * @apiError DataConflict Date already exists.
 *
 * @apiErrorExample Data Conflict Error:
 *     HTTP/1.1 402 Data Conflict
 *     {
 *       "status": false,
 *       "message": "Data already exist"
 *       "data": null 
 *     }
 */

//================================================================================================

 /**
 * @api {get} /employee-management/directory-settings/get/:company_id GetDirectorySettings
 * @apiName GetDirectorySettings
 * @apiGroup DirectorySettings
 * 
 * @apiPermission company
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Added directory settings for this company",
    "data": {
        "settings": {
            "photo": true,
            "name": true,
            "job_title": true,
            "location": true,
            "email": true,
            "work_phone": true,
            "cell_phone": true,
            "department": true,
            "business_unit": true
        },
        "_id": "5cd148fc74224f3ae0f0e8d6",
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-05-07T08:59:40.338Z",
        "updatedAt": "2019-05-07T08:59:40.338Z"
    }
}
 *
 * @apiSuccessExample Success-Response2:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched directory settings",
    "data": {
        "_id": "5cd148fc74224f3ae0f0e8d6",
        "settings": {
            "photo": false,
            "name": true,
            "job_title": true,
            "location": true,
            "email": true,
            "work_phone": true,
            "cell_phone": true,
            "department": true,
            "business_unit": true
        },
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-05-07T08:59:40.338Z",
        "updatedAt": "2019-05-07T09:31:29.566Z"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /employee-management/directory-settings/update UpdateDirectorySettings
 * @apiName UpdateDirectorySettings
 * @apiGroup DirectorySettings
 * 
 * @apiPermission company
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "settings": {
        "photo": false,
        "name": true,
        "job_title": true,
        "location": true,
        "email": true,
        "work_phone": true,
        "cell_phone": true,
        "department": true,
        "business_unit": true
    },
    "companyId": "5c9e11894b1d726375b23058"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully updated directory settings",
    "data": {
        "settings": {
            "photo": false,
            "name": true,
            "job_title": true,
            "location": true,
            "email": true,
            "work_phone": true,
            "cell_phone": true,
            "department": true,
            "business_unit": true
        },
        "_id": "5cd148fc74224f3ae0f0e8d6",
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-05-07T08:59:40.338Z",
        "updatedAt": "2019-05-07T09:31:29.566Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

