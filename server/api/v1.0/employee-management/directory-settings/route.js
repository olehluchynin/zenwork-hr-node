let express = require('express');

let router = express.Router();

let authMiddleware = require('../../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../../middlewares/validations');

router.get(
    '/get/:company_id',
    authMiddleware.isUserLogin,
    authMiddleware.isCompany,
    Controller.getDirectorySettings
);

router.put(
    '/update',
    validationsMiddleware.reqiuresBody,
    authMiddleware.isUserLogin,
    authMiddleware.isCompany,
    Controller.updateDirectorySettings
);

module.exports = router;