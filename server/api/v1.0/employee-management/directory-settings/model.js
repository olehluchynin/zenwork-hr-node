/**
*@author Asma
*@date 07/05/2019 11:44

*/
let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('../../../../common/constants');

let Schema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', unique: true},
    // which fields should be displayed in the directory tab - employees list
    settings: {
        photo: {type : Boolean, default: true},
        name: {type : Boolean, default: true},
        job_title: {type : Boolean, default: true},
        location: {type : Boolean, default: true},
        email : {type : Boolean, default: true},
        work_phone : {type : Boolean, default: true},
        cell_phone: {type : Boolean, default: true},
        department: {type : Boolean, default: true},
        business_unit: {type : Boolean, default: true}
    }
},{
    timestamps: true,
    versionKey: false,
    strict: false
});

// Schema.plugin(autoIncrement.plugin, {model:'wizards', field:'wizardId', startAt: 1, incrementBy: 1});

let DirectorySettingsModel = mongoose.model('directory_settings',Schema);

module.exports = DirectorySettingsModel;