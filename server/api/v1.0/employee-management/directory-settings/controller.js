let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../../config/config');
let utils = require('./../../../../common/utils');
let helpers = require('./../../../../common/helpers');
let _ = require('underscore');
let DirectorySettings = require('./model');

/**
 * API to get directory settings of a company
 * If the directory settings are not found for a given company, a default document is created for that company
 * @param {*} req 
 * @param {*} res 
 */
let getDirectorySettings = (req, res) => {
    let companyId = req.params.company_id
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        resolve(null);
    })
        .then(() => {
            DirectorySettings.findOne({ companyId: companyId }).lean().exec()
                .then(directory_settings => {
                    if (!directory_settings) {
                        DirectorySettings.create({ companyId: companyId })
                            .then(result => {
                                result.save(function (err, response) {
                                    if (!err) {
                                        res.status(200).json({
                                            status: true,
                                            message: 'Added directory settings for this company',
                                            data: response
                                        })
                                    }
                                    else {
                                        res.status(500).json({
                                            status: false,
                                            message: err
                                        })
                                    }
                                })
                            })
                            .catch(err => {
                                res.status(500).json({
                                    status: false,
                                    message: err
                                })
                            })
                    }
                    else {
                        res.status(200).json({
                            status: true,
                            message: "Successfully fetched directory settings",
                            data: directory_settings
                        })
                    }
                })
                .catch(err => {
                    res.status(500).json({
                        status: false,
                        message: err
                    })
                })
        })
        .catch(err => {
            res.status(500).json({
                status: false,
                message: err
            })
        });
}

/**
 * API to update directory settings of a company
 * companyId and suitable body are required
 * @param {*} req 
 * @param {*} res 
 */
let updateDirectorySettings = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        resolve(null);
    })
        .then(() => {
            delete body.companyId
            delete body.createdAt
            delete body.updatedAt
            delete body._id
            DirectorySettings.findOneAndUpdate(
                {
                    companyId: companyId
                },
                {
                    $set: body
                },
                {
                    new: true
                }
            )
            .then(response => {
                res.status(200).json({
                    status: true,
                    message: 'Successfully updated directory settings',
                    data: response
                })
            })
            .catch(err => {
                res.status(500).json({
                    status: false,
                    message: err
                })
            });
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: err
            })
        });
}

module.exports = {
    getDirectorySettings,
    updateDirectorySettings
}