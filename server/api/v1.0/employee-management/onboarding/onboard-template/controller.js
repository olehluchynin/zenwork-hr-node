const mongoose = require('mongoose');

const OnboardingModel = require('./model');
const templates = require('./templates');

/**
 * Author:Sai Reddy  Date:17/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * Function to add default Onboarding Template of a company while onboarding
 */
let addDefaultOBTemplate = (companyId) => {
    console.log('companyId ==> ', companyId);
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let defObj = templates.StandardOnboardTemplate;
        defObj.companyId = companyId;
        return OnboardingModel.create(defObj);
    }).then(data => {
        return {status: true, message: "Success"}; //res.status(200).json({status: true, message: "Success"});
    }).catch((err) => {
        console.log(err);
        return {status: false, message: err, data: null}//res.status(400).json({status: false, message: err, data: null});
    });
};

// API to add standard template 
// let addDefaultOBTemplate = (req,res) => {
//     let companyId = req.body.companyId
//     console.log('companyId ==> ', companyId);
//     return new Promise((resolve, reject) => {
//         resolve(null);
//     }).then(() => {
//         let defObj = templates.StandardOnboardTemplate;
//         defObj.companyId = companyId;
//         return OnboardingModel.create(defObj);
//     }).then(data => {
//         // return {status: true, message: "Success"}; 
//         res.status(200).json({status: true, message: "Success"});
//     }).catch((err) => {
//         console.log(err);
//         // return {status: false, message: err, data: null}
//         res.status(400).json({status: false, message: err, data: null});
//     });
// };

/**
 * Author:Sai Reddy  Date:17/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to get Standard Template of a company
 */
let getStandardTemplate = (req, res) => {
    let companyId = req.jwt.companyId;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return OnboardingModel.findOne({companyId:companyId, templateType: 'base'},{companyId:0,otId:0,'Onboarding Tasks':0});
    }).then(data => {
        res.status(200).json({status: true, message: "Success", data});
    }).catch((err) => {
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:17/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to get all Custom Onboarding Templates of a company
 */
let getCustomTemplates = (req, res) => {
    let companyId = req.jwt.companyId;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return OnboardingModel.find({companyId:companyId, templateType: 'base'},{companyId:0,otId:0,'Onboarding Tasks':0}).skip(1);
    }).then(data => {
        res.status(200).json({status: true, message: "Success", data});
    }).catch((err) => {
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:17/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-26
 * API to get all Onboarding Templates of a company
 */
let getTemplates = (req, res) => {
    let companyId = req.jwt.companyId;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return OnboardingModel.find({companyId:companyId, templateType: 'base'},{templateName:1});
    }).then(data => {
        res.status(200).json({status: true, message: "Success", data});
    }).catch((err) => {
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:17/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to EDIT Onboarding Template of a company
 */
let editTask = (req, res) => {
    var body = req.body;
    delete body.data.isChecked;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let findQuery = {
            _id:body._id
        };
        let updateQuery = {
            $set: {}
        };
        findQuery['Onboarding Tasks.'+body.data.category+'._id'] = body.data._id;
        updateQuery['$set']['Onboarding Tasks.'+body.data.category+'.$'] = body.data;
        console.log('findQuery ==> ', findQuery);
        console.log('updateQuery ==> ', updateQuery);
        return OnboardingModel.update(findQuery,updateQuery);
    }).then(data => {
        res.status(200).json({status: true, message: "Success"});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:17/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to add Onboarding Template of a company
 */
let addEditTemplate = (req, res) => {
    var body = req.body;
    let companyId = req.jwt.companyId;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        body.companyId = companyId;
        for(let category in body['Onboarding Tasks']) {
            if(body['Onboarding Tasks'].hasOwnProperty(category)) {
                for(let i = 0;i < body['Onboarding Tasks'][category].length;i++) {
                    if(!body['Onboarding Tasks'][category][i]._id) {
                        body['Onboarding Tasks'][category][i]._id = mongoose.Types.ObjectId();
                    }
                }
            }
        }
        if(body._id) {
            return OnboardingModel.update({_id:body._id},body, {$new : true});
        } else {
            return OnboardingModel.create(body);
        }
        // return OnboardingModel.create(body);
    }).then(data => {
        res.status(200).json({status: true, message: "Success", data: data});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:17/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to get single Onboarding Template of a company
 */
let getTemplate = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return OnboardingModel.findOne({_id:req.params._id},{companyId:0,otId:0}).lean();
        /*return OnboardingModel.aggregate([
            {$match:{_id:mongoose.Types.ObjectId(req.params._id)}},
            /!*{
                $lookup: {
                    from: "users",
                    localField: "Onboarding Tasks.$.taskAssignee",
                    foreignField: "email",
                    as: "fasak"
                }
            }*!/
        ]);*/
    }).then(data => {
        for(let category in data['Onboarding Tasks']) {
            if(data['Onboarding Tasks'].hasOwnProperty(category)) {
                if(data['Onboarding Tasks'][category].length === 0) {
                    delete data['Onboarding Tasks'][category];
                }
            }
        }
        res.status(200).json({status: true, message: "Success", data});
    }).catch((err) => {
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:17/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to delete single Onboarding Template of a company
 */
let deleteTask = (req, res) => {
    var body = req.body;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        // let findQuery = {
        //     _id:body._id
        // };
        let updateQuery = {
            $pull: {}
        };
        // findQuery['Onboarding Tasks.'+body.data.category+'._id'] = body.data._id;
        updateQuery['$pull']['Onboarding Tasks.'+body.data.category] = {_id:mongoose.Types.ObjectId(body.data._id)};
        // console.log('findQuery ==> ', findQuery);
        console.log('updateQuery ==> ', updateQuery,body.data._id);
        // return true;
        return OnboardingModel.update({_id:body._id},updateQuery).exec();
    }).then(data => {
        console.log(data);
        res.status(200).json({status: true, message: "Success"});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};



/**
 * Author:Sai Reddy  Date:17/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-26
 * API to get delete Onboarding Template of a company
 */
let deleteTemplate = (req, res) => {
    let companyId = req.jwt.companyId;
    if(!req.body.ids) req.body.ids = [];
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return OnboardingModel.remove({companyId:companyId,_id:{$in:req.body}});
    }).then(data => {
        res.status(200).json({status: true, message: "Success"});
    }).catch((err) => {
        res.status(400).json({status: false, message: err, data: null});
    });
};


module.exports = {
    addDefaultOBTemplate,
    getStandardTemplate,
    getTemplates,
    getCustomTemplates,
    editTask,
    addEditTemplate,
    getTemplate,
    deleteTask,
    deleteTemplate
};
