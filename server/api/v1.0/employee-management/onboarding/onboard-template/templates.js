const StandardOnboardTemplate = {

    "templateName":"Standard Onboarding Template",
    "Onboarding Tasks": {
        "HR Tasks":[
            {
                taskName: "New Hire Orientation",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "HR Tasks"
            },{
                taskName: "Employee Handbook Introduction",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "HR Tasks"
            },{
                taskName: "Confidentiality Agreement",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "HR Tasks"
            },{
                taskName: "Issue T-shirt and 'Welcome Package'",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "HR Tasks"
            }
        ],
        "New Hire Forms":[
            {
                taskName: "I-9 Form",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "New Hire Forms"
            },{
                taskName: "W-4 Form",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "New Hire Forms"
            },{
                taskName: "State Tax Form",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "New Hire Forms"
            }
        ],
        "IT Setup":[
            {
                taskName: "Desk Setup",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "IT Setup"
            },{
                taskName: "Create Work Email",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "IT Setup"
            },{
                taskName: "Phone Setup",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "IT Setup"
            },{
                taskName: "Issue Security Bandage",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "IT Setup"
            }
        ],
        "Manager Tasks":[
            {
                taskName: "Lunch With Employee",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "Manager Tasks"
            },{
                taskName: "Team Introduction",
                timeToComplete: {
                    time: 1,
                    unit: 'Day'
                },
                category: "Manager Tasks"
            }
    ]
    }
};


const CustomOnboardTemplate = {

    "templateName":"",
    "Onboarding Tasks": {
        "HR Tasks":[

        ],
        "New Hire Forms":[

        ],
        "IT Setup":[

        ],
        "Manager Tasks":[

        ]
    }
};

module.exports = {
    StandardOnboardTemplate,
    CustomOnboardTemplate
};
