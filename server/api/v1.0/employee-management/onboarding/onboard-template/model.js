let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('../../../../../common/constants');

let Schema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies'},
    templateType: {type: String, enum: constants.templateTypes, default: 'base'},
    templateName: {type: String},
    assigned_date: {type: Date},
    completion_date: {type: Date},
    onboarding_complete: {
        type: String,
        enum: ['Yes', 'No']
    },    
    "Onboarding Tasks": {
        "HR Tasks":[{
            taskName: String,
            taskAssignee: String,
            assigneeEmail: String,
            taskDesription: String,
            timeToComplete: {
                time: Number,
                unit: {type:String, enum:["Day","Week","Month"]},
                days: Number
            },
            category: String,
            task_assigned_date: {type:Date},
            task_completion_date: {type:Date},
            task_assigned_by: {type: ObjectID, ref: 'users'},
            notes: String,
            Date: {type:Date},
        }],
        "New Hire Forms":[{
            taskName: String,
            taskAssignee: String,
            assigneeEmail: String,
            taskDesription: String,
            timeToComplete: {
                time: Number,
                unit: {type:String, enum:["Day","Week","Month"]},
                days: Number
            },
            category: String,
            task_assigned_date: {type:Date},
            task_completion_date: {type:Date},
            task_assigned_by: {type: ObjectID, ref: 'users'},
            notes: String,
            Date: {type:Date},
        }],
        "IT Setup":[{
            taskName: String,
            taskAssignee: String,
            assigneeEmail: String,
            taskDesription: String,
            timeToComplete: {
                time: Number,
                unit: {type:String, enum:["Day","Week","Month"]},
                days: Number
            },
            category: String,
            task_assigned_date: {type:Date},
            task_completion_date: {type:Date},
            task_assigned_by: {type: ObjectID, ref: 'users'},
            notes: String,
            Date: {type:Date},
        }],
        "Manager Tasks":[{
            taskName: String,
            taskAssignee: String,
            assigneeEmail: String,
            taskDesription: String,
            timeToComplete: {
                time: Number,
                unit: {type:String, enum:["Day","Week","Month"]},
                days: Number
            },
            category: String,
            task_assigned_date: {type:Date},
            task_completion_date: {type:Date},
            task_assigned_by: {type: ObjectID, ref: 'users'},
            notes: String,
            Date: {type: Date},
        }]
    },

    createdBy: {type:ObjectID,ref:'users'},
    updatedBy:{type:ObjectID,ref:'users'},
},{
    timestamps: true,
    versionKey: false,
    strict: false
});

Schema.plugin(autoIncrement.plugin, {model:'onboarding_templates', field:'otId', startAt: 1, incrementBy: 1});

let TrainingModel = mongoose.model('onboarding_templates',Schema);

module.exports = TrainingModel;
