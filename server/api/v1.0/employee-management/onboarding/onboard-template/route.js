let express = require('express');

let router = express.Router();

let authMiddleware = require('../../../middlewares/auth');

let OnboardingController = require('./controller');

let validationsMiddleware = require('../../../middlewares/validations');

router.get('/getStandardTemplate', authMiddleware.isUserLogin, authMiddleware.isCompany, OnboardingController.getStandardTemplate);

router.get('/getCustomTemplates', authMiddleware.isUserLogin, authMiddleware.isCompany, OnboardingController.getCustomTemplates);

router.get('/getTemplates', authMiddleware.isUserLogin, authMiddleware.isCompany, OnboardingController.getTemplates);

router.post('/editTask', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, authMiddleware.isCompany, OnboardingController.editTask);

router.post('/addEditTemplate', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, authMiddleware.isCompany, OnboardingController.addEditTemplate);

router.get('/getTemplate/:_id', authMiddleware.isUserLogin, authMiddleware.isCompany, OnboardingController.getTemplate);

router.post('/deleteTask', authMiddleware.isUserLogin, authMiddleware.isCompany, OnboardingController.deleteTask);


router.post('/deleteTemplate', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, authMiddleware.isCompany, OnboardingController.deleteTemplate);

// router.post('/addStandardTemplate', validationsMiddleware.reqiuresBody, OnboardingController.addDefaultOBTemplate)

module.exports = router;
