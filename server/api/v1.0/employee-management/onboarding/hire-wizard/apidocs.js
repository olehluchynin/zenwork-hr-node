/**
 * @api {post} /employee-management/onboarding/hire-wizard/listEmployees ListEmployees
 * @apiName ListEmployees
 * @apiGroup HireWizard
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
    "search_query": "sur",
    "per_page": 10,
    "page_no": 1,
	"sort": {
		"personal.name.firstName": 1
	},
	"employment_status": ["active"],
    "employment_type": ["Full Time"],
    "location": ["Hyderabad"],
    "department": ["Sales"]
}
 * @apiParamExample {json} Request-Example2:
 *
{
    "Specific_Workflow_Approval": 'Yes'
}
 * @apiParamExample {json} Request-Example3:
 *
{
	"per_page": 10,
	"page_no": 1,
	"_ids": [
		"5d564db9a0157c5995264c64",
		"5d4d446dd43bfd7832622463"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Success",
    "total_count": 4,
    "data": [
        {
            "_id": "5cd02c6e0606e63cd0b385bb",
            "personal": {
                "name": {
                    "firstName": "Suresh",
                    "middleName": "Malapati",
                    "lastName": "Reddy",
                    "preferredName": "Sureshh"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "Florida",
                    "zipcode": "123256666",
                    "country": "India"
                },
                "contact": {
                    "workPhone": "1324432423",
                    "extention": "1234",
                    "mobilePhone": "2134321421",
                    "homePhone": "1243321456",
                    "workMail": "suresh@hjdsgf.com",
                    "personalMail": "suri@kjsdhfjksd.com"
                },
                "dob": "2019-05-15T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-28T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "132487",
                "ethnicity": "Ethnicity11"
            },
            "compensation": {
                "payRate": {
                    "amount": 2134
                },
                "NonpaidPosition": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Yearly",
                "First_Pay_Date": "2019-05-09T18:30:00.000Z",
                "compensationRatio": "1234",
                "Salary_Grade": "a"
            },
            "status": "active",
            "groups": [],
            "job": {
                "employeeId": "",
                "employeeType": "Part Time",
                "hireDate": "2019-05-21T18:30:00.000Z",
                "jobTitle": "Internship",
                "department": "Sales",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Technicians",
                "Site_AccessRole": "5ccfbd540606e63cd0b38568",
                "VendorId": "2134214",
                "Specific_Workflow_Approval": "Approval1"
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": false,
                "assign_performance_management": true,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cb870f2b494651a2bd98750"
            },
            "email": "suresh@hjdsgf.com",
            "companyId": "5c9e11894b1d726375b23058",
            "createdAt": "2019-05-06T12:45:34.595Z",
            "updatedAt": "2019-05-08T11:17:23.213Z",
            "userId": 75,
            "type": "employee"
        },
        {
            "_id": "5cc95a0d90f4222864baeeb4",
            "personal": {
                "name": {
                    "firstName": "malapati",
                    "middleName": "suresh",
                    "lastName": "reddy",
                    "preferredName": "suri"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "secc",
                    "city": "hyd",
                    "state": "Florida",
                    "zipcode": "123",
                    "country": "India"
                },
                "contact": {
                    "workPhone": "6456464356456",
                    "extention": "123",
                    "mobilePhone": "456464356546",
                    "homePhone": "45643665466",
                    "workMail": "sdkjfhgdshg@dergtdfg.com",
                    "personalMail": "dsgfdg3465444444@dsgfdg.com"
                },
                "dob": "2019-05-23T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Married",
                "ssn": "12267",
                "ethnicity": "Ethnicity11"
            },
            "compensation": {
                "payRate": {
                    "amount": 134
                },
                "NonpaidPosition": false,
                "Pay_group": "Group1",
                "Pay_frequency": "Weekly",
                "First_Pay_Date": "2019-05-15T18:30:00.000Z",
                "compensationRatio": "123",
                "Salary_Grade": "a"
            },
            "status": "active",
            "job": {
                "employeeId": "",
                "employeeType": "Full Time",
                "hireDate": "2019-05-12T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "Site_AccessRole": "5ccaae31172e1531c7f05b1f",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Professionals",
                "VendorId": "`3421655",
                "Specific_Workflow_Approval": "Approval2"
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": true,
                "assign_performance_management": false,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cb870f2b494651a2bd98750"
            },
            "email": "sdkjfhgdshg@dergtdfg.com",
            "companyId": "5c9e11894b1d726375b23058",
            "employeeId": "13",
            "createdAt": "2019-05-01T08:34:21.432Z",
            "updatedAt": "2019-05-09T12:45:57.036Z",
            "userId": 63,
            "groups": [
                "5cc9186ab71b1f1e10416b82",
                "5cca67b94b59ba2b22ccf8e7",
                "5cca68c84b59ba2b22ccf8f7",
                "5cca6cc74b59ba2b22ccf907",
                "5cca6ebb4b59ba2b22ccf917",
                "5ccafba395a1a7354dded820",
                "5ccbcf29e394c03b6f9475ff",
                "5ccbd17799cbfb3c9143868e",
                "5ccfbb9c0606e63cd0b3855a",
                null,
                "5ccfbd540606e63cd0b38568",
                "5cd420e6c583847552fa33da"
            ],
            "type": "employee"
        },
        {
            "_id": "5ccbd77c0606e63cd0b3852d",
            "personal": {
                "name": {
                    "firstName": "suresh",
                    "middleName": "malapati",
                    "lastName": "reddy",
                    "preferredName": "suri"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "Florida",
                    "zipcode": "123456789",
                    "country": "India"
                },
                "contact": {
                    "workPhone": "4567567567",
                    "extention": "3453",
                    "mobilePhone": "4573724654",
                    "homePhone": "3453452345",
                    "workMail": "suresh@gmail.com",
                    "personalMail": "mxcvb@gmail.com"
                },
                "dob": "2019-05-16T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-28T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "1345456",
                "ethnicity": "Ethnicity11"
            },
            "compensation": {
                "payRate": {
                    "amount": 1234
                },
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Bi-Weekly",
                "First_Pay_Date": "2019-05-08T18:30:00.000Z",
                "compensationRatio": "12",
                "Salary_Grade": "a"
            },
            "status": "active",
            "groups": [],
            "job": {
                "employeeId": "",
                "employeeType": "Full Time",
                "hireDate": "2019-05-06T18:30:00.000Z",
                "jobTitle": "Internship",
                "department": "Sales",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Technicians",
                "Site_AccessRole": "5ccaae31172e1531c7f05b1f",
                "VendorId": "1234234",
                "Specific_Workflow_Approval": "Approval2"
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": false,
                "assign_performance_management": true,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cb870f2b494651a2bd98750"
            },
            "email": "suresh@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "createdAt": "2019-05-03T05:54:04.029Z",
            "updatedAt": "2019-05-03T05:54:04.029Z",
            "userId": 72,
            "type": "employee"
        },
        {
            "_id": "5cc96b82b34d032a16f02be9",
            "personal": {
                "name": {
                    "firstName": "suresh1",
                    "middleName": "suresh m",
                    "lastName": "suresh reddy",
                    "preferredName": "suri2"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "Florida",
                    "zipcode": "123",
                    "country": "India"
                },
                "contact": {
                    "workPhone": "3245432534",
                    "extention": "123",
                    "mobilePhone": "234523453425",
                    "homePhone": "345345345555",
                    "workMail": "asdfghjk@xcvbn.com",
                    "personalMail": "qwert@sfgh.com"
                },
                "dob": "2019-05-29T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-28T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "456712343",
                "ethnicity": "Ethnicity11"
            },
            "compensation": {
                "payRate": {
                    "amount": 123
                },
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Bi-Weekly",
                "First_Pay_Date": "2019-05-23T18:30:00.000Z",
                "compensationRatio": "12",
                "Salary_Grade": "a"
            },
            "status": "active",
            "job": {
                "employeeId": "",
                "employeeType": "Full Time",
                "hireDate": "2019-05-28T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Non-Exempt",
                "EEO_Job_Category": "Technicians",
                "Site_AccessRole": "5cc983434b59ba2b22ccf893",
                "VendorId": "132434",
                "Specific_Workflow_Approval": "Approval2"
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": true,
                "assign_performance_management": true,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cbad559a145693218c30921"
            },
            "email": "asdfghjk@xcvbn.com",
            "companyId": "5c9e11894b1d726375b23058",
            "employeeId": "14",
            "createdAt": "2019-05-01T09:48:50.507Z",
            "updatedAt": "2019-05-09T12:45:57.038Z",
            "userId": 64,
            "groups": [
                "5cca67b94b59ba2b22ccf8e7",
                "5cca68c84b59ba2b22ccf8f7",
                "5cca6cc74b59ba2b22ccf907",
                "5cca6ebb4b59ba2b22ccf917",
                "5ccafba395a1a7354dded820",
                "5ccbcf29e394c03b6f9475ff",
                "5ccbd17799cbfb3c9143868e",
                "5ccfbb9c0606e63cd0b3855a",
                null,
                "5ccfbd540606e63cd0b38568",
                "5cd420e6c583847552fa33da"
            ],
            "type": "employee"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /employee-management/onboarding/hire-wizard/getEmployeeId/:company_id GetEmployeeId
 * @apiName GetEmployeeId
 * @apiGroup HireWizard
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully generated employee ID",
    "data": {
        "employeeId": "25210001"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /employee-management/onboarding/hire-wizard/users-under-group/:company_id/:role_id GetUsersUnderGroup
 * @apiName GetUsersUnderGroup
 * @apiGroup HireWizard
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} role_id Roles unique _id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successsfully fetched users under this group/role",
    "data": [
        "5cf6095fecc3dd104e07cd7d",
        "5cf60c35ecc3dd104e07cda2"
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /employee-management/onboarding/hire-wizard/cal-compensation-ratio CalculateCompensationRatio
 * @apiName CalculateCompensationRatio
 * @apiGroup HireWizard
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} RequestExample1:
{
	"payRate" : 24,
	"payType" : "Salary",
	"pay_frequency": "Weekly",
	"grade_band": "5d529665a07285293e8dcb52"
}
 * @apiParamExample {json} RequestExample2:
{
	"payRate" : 12,
    "payType" : "Hourly",
    "workHours": 8,
	"pay_frequency": "Weekly",
	"grade_band": "5d529665a07285293e8dcb52"
}
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully calculated compensation ratio",
    "data": {
        "compensationRatio": 0.96
    }
}
 * @apiErrorExample Error-Example: 
{
    "status": false,
    "message": "Compensation does not fall under the specified grade band"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */