let express = require('express');

let router = express.Router();

let authMiddleware = require('../../../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../../../middlewares/validations');

// router.get(
//     '/getEmployeeId/:company_id', 
//     authMiddleware.isUserLogin, 
//     // authMiddleware.isCompany, 
//     Controller.generateEmployeeId
// );

router.get(
    '/getEmployeeId/:company_id', 
    authMiddleware.isUserLogin, 
    Controller.generateEmployeeIdNew
);

// router.post('/addEmployee', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, authMiddleware.isCompany, Controller.addEmployee);
router.post(
    '/addEmployee', 
    validationsMiddleware.reqiuresBody, 
    authMiddleware.isUserLogin, 
    authMiddleware.isCompany, 
    Controller.addEmployeeNew
);

router.post(
    '/searchEmployee', 
    validationsMiddleware.reqiuresBody, 
    authMiddleware.isUserLogin, 
    // authMiddleware.isCompany, 
    Controller.searchEmployee);

router.post('/chooseEmployeesForRoles', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, authMiddleware.isCompany, Controller.chooseEmployeesForRoles);

router.put('/addGroupsToUsers', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, authMiddleware.isCompany, Controller.addGroupsToUsers);

router.get('/users-under-group/:company_id/:role_id', authMiddleware.isUserLogin, authMiddleware.isCompany, Controller.getUsersUnderGivenGroup);



router.post('/addUpdateCustomWizard', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, authMiddleware.isCompany, Controller.addUpdateCustomWizard);

router.get('/getAllCustomWizards', authMiddleware.isUserLogin, authMiddleware.isCompany, Controller.getAllCustomWizards);

router.get('/getCustomWizard/:_id', authMiddleware.isUserLogin, authMiddleware.isCompany, Controller.getCustomWizard);

router.post('/deleteCustomWizards', authMiddleware.isUserLogin, authMiddleware.isCompany, Controller.deleteCustomWizards);

router.post(
    '/listEmployees', 
    authMiddleware.isUserLogin, 
    // authMiddleware.isCompany, 
    Controller.listEmployees
);

router.post(
    "/cal-compensation-ratio",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.calculateCompensationRatio
);

module.exports = router;
