const mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
const HireWizardModel = require('./model');
const UsersModel = require('./../../../users/model');
const CompSettingsModel = require('./../../../companies/settings/model');
const OnboardingModel = require('./../onboard-template/model');
let utils = require('./../../../../../common/utils');
let mailer = require('./../../../../../common/mailer');
const async = require('async');
// let constants = require('./../../../../../common/constants');
let InboxModel = require('../../../inbox/model')
let s3 = require('./../../../../../common/s3/s3');
const config = require('./../../../../../config/config')
let CompaniesModel = require('./../../../companies/model');
var Handlebars = require('handlebars');
let fs = require('fs');
let GradeBandsModel = require('../../../salary-grades/model').GradeBandsModel;
let convertTo12Format = require('../../../leaveManagement/Scheduler/controller').convertTo12Format

/**
*@author Asma
*@date 17/05/2019 08:12

*/
let getNextEmployeeId = (employee, idNumberSettings) => {
    let prefix = String(idNumberSettings.firstNumber)
    console.log('prefix ', prefix)

    let regex_pattern = new RegExp('^(' + prefix + ')')
    console.log('regex_pattern ', regex_pattern)

    let latestEmployeeIdInt = parseInt(employee.job.employeeId.replace(regex_pattern, ''))
    console.log('latestEmployeeIdInt ', latestEmployeeIdInt)

    if (isNaN(latestEmployeeIdInt)) {
        throw 'Error while latest employee id formatting'
    }
    else if (latestEmployeeIdInt.toString().length == idNumberSettings.digits) {
        console.log('current selected format = last employee number of digits')
        let nextEmployeeId = String(latestEmployeeIdInt + 1)
        console.log('nextEmployeeId - ', prefix + nextEmployeeId)
        return prefix + nextEmployeeId
    }
    else {
        // throw "Current selected format doesn't match with the last employee ID"
        console.log("Current selected format doesn't match with the last employee ID number of digits \n genrating first employee id with given format")
        return getFirstEmployeeId(idNumberSettings)
    }

    // if (!isNaN(latestEmployeeIdInt)) {
    //     let nextEmployeeId = String(latestEmployeeIdInt + 1)
    //     console.log('nextEmployeeId - ', prefix + nextEmployeeId)
    //     return prefix + nextEmployeeId
    // }
    // else {
    //     throw 'Error while latest employee id formatting'
    // }
}

/**
*@author Asma
*@date 27/08/2019 16:32

*/
let getNextEmployeeIdNew = (employee, idNumberSettings) => {
    let latestEmployeeIdInt = parseInt(employee.job.employeeId)
    console.log('latestEmployeeIdInt ', latestEmployeeIdInt)

    if (isNaN(latestEmployeeIdInt)) {
        throw 'Error while latest employee id formatting'
    }
    else if (latestEmployeeIdInt.toString().length == idNumberSettings.digits) {
        console.log('current selected format = last employee number of digits')
        let nextEmployeeId = String(latestEmployeeIdInt + 1)
        console.log('nextEmployeeId - ', nextEmployeeId)
        if (!nextEmployeeId.toString().length == idNumberSettings.digits) {
            throw 'Could not generate employee ID. Reached the limit for ' + idNumberSettings.digits + 'digit numbers. Please change the employee ID generation settings in company setup.'
        }
        else {
            return nextEmployeeId
        }
    }
    else {
        
        console.log("Current selected format doesn't match with the last employee ID number of digits \n generating first employee id with given format")
        return {
            employeeID : getFirstEmployeeIdNew(idNumberSettings),
            message: "Current selected format doesn't match with the last employee ID number of digits. Generating first employee ID with given format"
        }
        // throw "Current selected format doesn't match with the last employee ID format"
        // return getFirstEmployeeIdNew(idNumberSettings)
    }
}

/**
*@author Asma
*@date 17/05/2019 08:12

*/
let getFirstEmployeeId = (idNumberSettings) => {
    let prefix = String(idNumberSettings.firstNumber)
    let firstEmployeeId = String(parseInt('1' + '0'.repeat(idNumberSettings.digits - 1)) + 1)
    console.log('first employee id - ', prefix + firstEmployeeId)
    return prefix + firstEmployeeId
}

/**
*@author Asma
*@date 27/08/2019 16:45

*/
let getFirstEmployeeIdNew = (idNumberSettings) => {
    return {
        employeeID : idNumberSettings.firstNumber,
        message: "Successfully fetched first employee ID"
    }
}

let generateEmployeeId = (req, res) => {
    let companyId = req.params.company_id
    console.log(companyId)
    return new Promise((resolve, reject) => {
        if (!companyId) reject('company_id is required');
        else resolve(null)
    })
        .then(() => {
            return Promise.all([
                CompSettingsModel.findOne({ companyId: ObjectID(companyId) }, { idNumber: 1 }).lean().exec(),
                UsersModel.findOne({ companyId: companyId, type: 'employee' }, { 'job.employeeId': 1 }).sort({ _id: -1 })
            ]);
        })
        .then(([company, latestEmployee]) => {

            if (company.idNumber && Object.keys(company.idNumber).length > 0) {
                console.log(company.idNumber)
                if (company.idNumber.numberType === 'system Generated') {
                    //generate idnumber
                    // body.employeeId = latestuser.employeeId ? Number(latestuser.employeeId) + 1 : company.idNumber.firstNumber;

                    try {
                        let employeeId = latestEmployee ? getNextEmployeeId(latestEmployee, company.idNumber) : getFirstEmployeeId(company.idNumber);
                        res.status(200).json({
                            status: true,
                            message: "Successfully generated employee ID",
                            data: {
                                employeeId: employeeId
                            }
                        });
                    }
                    catch (error) {
                        console.log(error)
                        res.status(400).json({
                            status: false,
                            message: "Erro while generating employee ID",
                            error: error
                        })
                    }
                }
                else {
                    console.log("company employee ID generation is set to manual")
                    res.status(400).json({
                        status: false,
                        message: "company employee ID generation is set to manual"
                    })
                }
            }
            else {
                console.log("company employee ID generation settings not found")
                res.status(400).json({
                    status: false,
                    message: "company employee ID generation settings not found"
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

/**
*@author Asma
*@date 27/08/2019 16:47

*/
let generateEmployeeIdNew = (req, res) => {
    let companyId = req.params.company_id
    console.log(companyId)
    return new Promise((resolve, reject) => {
        if (!companyId) reject('company_id is required');
        else resolve(null)
    })
        .then(() => {
            return Promise.all([
                CompSettingsModel.findOne({ companyId: ObjectID(companyId) }, { idNumber: 1 }).lean().exec(),
                UsersModel.findOne({ companyId: companyId, type: 'employee' }, { 'job.employeeId': 1 }).sort({ _id: -1 })
            ]);
        })
        .then(([company, latestEmployee]) => {

            if (company.idNumber && Object.keys(company.idNumber).length > 0) {
                console.log(company.idNumber)
                if (company.idNumber.numberType === 'system Generated') {
                    //generate idnumber
                    // body.employeeId = latestuser.employeeId ? Number(latestuser.employeeId) + 1 : company.idNumber.firstNumber;

                    try {
                        let employeeId = latestEmployee ? getNextEmployeeIdNew(latestEmployee, company.idNumber) : getFirstEmployeeIdNew(company.idNumber);
                        if(employeeId.message) {
                            res.status(200).json({
                                status: true,
                                message: employeeId.message,
                                data: {
                                    employeeId: employeeId.employeeID
                                }
                            });
                        }
                        else {
                            res.status(200).json({
                                status: true,
                                message: "Successfully generated employee ID",
                                data: {
                                    employeeId: employeeId
                                }
                            });
                        }
                    }
                    catch (error) {
                        console.log(error)
                        res.status(400).json({
                            status: false,
                            message: "Erro while generating employee ID",
                            error: error
                        })
                    }
                }
                else {
                    console.log("company employee ID generation is set to manual")
                    res.status(400).json({
                        status: false,
                        message: "company employee ID generation is set to manual"
                    })
                }
            }
            else {
                console.log("company employee ID generation settings not found")
                res.status(400).json({
                    status: false,
                    message: "company employee ID generation settings not found"
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

/**
 * Method to gather required info to compile employee onboard email template and send email
 * @param {object} details 
 * @author Asma
 */
let send_creds_to_employee_email = (details) => {
    return new Promise((resolve, reject) => {
        if (!details.HR_Contact) reject('HR_Contact is required');
        else if (!details.companyId) reject('companyId is required');
        else {
            Promise.all([
                CompaniesModel.findOne({ _id: ObjectID(details.companyId) }, { name: 1 }).lean().exec(),
                UsersModel.findOne({ _id: ObjectID(details.HR_Contact) }, { 'personal.name': 1 }).lean().exec()
            ])
                .then(([company, hr_contact]) => {
                    details.company_name = company.name
                    details.HR_Contact_Name = Object.values(hr_contact.personal.name).slice(0, Object.values(hr_contact.personal.name).length - 1).join(" ")
                    fs.readFile(__dirname + '/employee_onboard_email.hbs', 'utf8', async (err, file_data) => {
                        if (!err) {
                            // console.log(file_data)
                            let html_data = (Handlebars.compile(file_data))(details)
                            // console.log(details.email, config.mail.defaultFromAddress, "ZenworkHR - Credentials", "ZenworkHR", html_data)
                            resolve(mailer.sendEmployeeCredsEmail(details.email, config.mail.defaultFromAddress, "ZenworkHR - Credentials", "ZenworkHR", html_data))
                        }
                        else {
                            console.log(err)
                            reject(err)
                        }
                    })
                })
                .catch(err => {
                    reject(err)
                })
        }

    })
}

/**
 * Author:Sai Reddy  Date:22/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to add Personal fiels of a user
 */
let addEmployee = (req, res) => {
    var body = req.body;
    // console.log(body);
    var companyId = req.jwt.companyId;
    let password_to_send_to_email;

    // delete body.job.HR_Contact;
    // delete body.job.ReportsTo;
    // delete body.job.Specific_Workflow_Approval;
    body.email = body.personal.contact.workMail;
    body.companyId = companyId;
    body.type = "employee"
    body.onboardedBy = req.jwt.userId
    let selected_template;
    return new Promise((resolve, reject) => {
        resolve(null);
    })
        .then(() => {
            return Promise.all([
                CompSettingsModel.findOne({ companyId: companyId }, { idNumber: 1 }),
                UsersModel.findOne({ companyId: companyId, type: 'employee' }, { 'job.employeeId': 1 }).sort({ _id: -1 }),
                UsersModel.findOne({ email: body.email })
            ]);
        })
        .then(async ([company, latestEmployee, userexists]) => {
            console.log('userexists - ', userexists)
            if (userexists) {
                // throw new Error("Email already exists");
                // reject("Email already exists");
                res.status(409).json({ status: false, message: "Email already exists", data: null });
            } else {
                body.createdBy = req.jwt.userId;
                body.updatedBy = req.jwt.userId;
                password_to_send_to_email = utils.generateRandomPassword()
                body.password = utils.encryptPassword(password_to_send_to_email);

                if (!body.job.employeeId) {
                    if (company.idNumber) {
                        if (company.idNumber.numberType === 'system Generated') {
                            //generate idnumber
                            // body.employeeId = latestuser.employeeId ? Number(latestuser.employeeId) + 1 : company.idNumber.firstNumber;
                            try {
                                // body.job.employeeId = latestEmployee ? getNextEmployeeId(latestEmployee, company.idNumber) : getFirstEmployeeId(company.idNumber);
                                body.job.employeeId = latestEmployee ? getNextEmployeeIdNew(latestEmployee, company.idNumber) : parseInt(company.idNumber.firstNumber);
                            }
                            catch (e) {
                                console.log(err);
                            }
                        }
                    }
                }

                body.job.current_employment_status = {
                    status: "Active",
                    effective_date: new Date()
                }
                body.job.employment_status_history = [
                    body.job.current_employment_status
                ]
                body.job.job_information_history = {
                    jobTitle: body.job.jobTitle,
                    department: body.job.department,
                    businessUnit: body.job.businessUnit,
                    unionFields: body.job.unionFields,
                    ReportsTo: body.job.ReportsTo,
                    effective_date: new Date()
                }
                let given_compensation_details = body.compensation
                delete body.compensation
                if (given_compensation_details.NonpaidPosition) {
                    body.compensation = {
                        "NonpaidPosition": given_compensation_details.NonpaidPosition
                    }
                }
                else {
                    body.compensation = {
                        "NonpaidPosition": given_compensation_details.NonpaidPosition,
                        // "Pay_group": given_compensation_details.Pay_group,
                        "Salary_Grade": given_compensation_details.Salary_Grade,
                        "grade_band": given_compensation_details.grade_band,
                        // "compensationRatio": given_compensation_details.compensationRatio,
                        "current_compensation": [{
                            "effective_date": given_compensation_details.First_Pay_Date,
                            "payRate": given_compensation_details.payRate.amount,
                            "payRateUnit": given_compensation_details.payRateUnit,
                            "payType": given_compensation_details.payRate.payType,
                            "Pay_frequency": given_compensation_details.Pay_frequency,
                            "Pay_group": given_compensation_details.Pay_group,
                            "compensationRatio": given_compensation_details.compensationRatio,
                            "changeReason": "--",
                            "notes": "--"
                        }],
                        "compensation_history": []
                    }
                }
                // mailer.sendUsernamePasswordToCompany(body.email, body.personal.name.firstName, body.email, body.password, "Employee");
                let details_to_send_email = {
                    logo_url: config.baseUrl + '/api/client/images/mainblacklogo.png',
                    login_url: config.frontEndUrl + '/zenwork-login',
                    employee_name: Object.values(body.personal.name).slice(0, Object.values(body.personal.name).length - 1).join(" "),
                    // company_name:
                    // HR_Contact_Name: 
                    email: body.email,
                    password: password_to_send_to_email,
                    companyId: companyId,
                    HR_Contact: body.job.HR_Contact
                }
                let email_response = send_creds_to_employee_email(details_to_send_email);
                console.log(email_response)
                console.log('body.module_settings.onboarding_template - ', body.module_settings.onboarding_template)
                if (body.module_settings.onboarding_template == "") {
                    delete body.module_settings.onboarding_template
                }
                if (body.module_settings.onboarding_template) {
                    selected_template = await OnboardingModel.findOne({ _id: data.module_settings.onboarding_template }).lean().exec()
                    for (let category of Object.keys(selected_template['Onboarding Tasks'])) {
                        if (selected_template['Onboarding Tasks'][category].length > 0) {
                            for (const onboarding_task of selected_template['Onboarding Tasks'][category]) {
                                console.log(onboarding_task.assigneeEmail)
                                let taskAssignee = await UsersModel.findOne({ email: onboarding_task.assigneeEmail })
                                if (!taskAssignee) {
                                    return res.status(400).json({ status: false, message: "Employee selected for onboarding tasks is no longer active. Please update onboarding template", data: null });
                                }
                            }
                        }
                    }
                }
                return UsersModel.create(body);
            }
        })
        .then(data => {
            console.log('data ======>>> ', data);
            console.log('body.module_settings.onboarding_template - ', body.module_settings.onboarding_template)
            console.log('body.module_settings.skip_template_assignment - ', body.module_settings.skip_template_assignment)
            if (!body.module_settings.onboarding_template && body.module_settings.skip_template_assignment) {
                return res.status(200).json({ status: true, message: "Success", data: data });
            }
            else {
                /**
                    *@author Asma
                    *@date 07/05/2019 11:42
                    Below code adds a derived template with all the task statuses = pending, based on the selected onboarding template given in req.body
                */
                OnboardingModel.findOne({ _id: data.module_settings.onboarding_template }).lean().exec()
                    .then(selected_template => {
                        let new_template = selected_template
                        delete new_template._id, delete new_template.otId, delete new_template.createdAt, delete new_template.updatedAt;
                        new_template.templateType = 'derived'
                        new_template.assigned_date = new Date()
                        new_template.onboarding_complete = 'No'
                        console.log(new_template)
                        async.each(Object.keys(new_template['Onboarding Tasks']), (category, callback) => {
                            console.log(category)
                            if (new_template['Onboarding Tasks'][category]) {
                                new_template['Onboarding Tasks'][category].forEach(onboarding_task => {
                                    onboarding_task.status = 'Pending'
                                    onboarding_task._id = mongoose.Types.ObjectId();
                                    onboarding_task.task_assigned_date = new Date()
                                    onboarding_task.task_assigned_by = req.jwt.userId
                                });
                            }
                            callback()
                        }, (err) => {
                            if (!err) {
                                console.log('done')
                                OnboardingModel.create(new_template)
                                    .then(saved_template => {
                                        saved_template.save(function (err, template_save_response) {
                                            data.module_settings.onboarding_template = template_save_response._id
                                            template_save_response = template_save_response.toJSON()
                                            data.save(async function (err, user_save_response) {
                                                if (!err) {
                                                    // send notifications
                                                    let notifications = []
                                                    console.log('template save response - ', template_save_response['Onboarding Tasks']['HR+sales'])
                                                    console.log('new template keys', Object.keys(template_save_response['Onboarding Tasks']))
                                                    for (let category of Object.keys(template_save_response['Onboarding Tasks'])) {
                                                        console.log(category)
                                                        if (template_save_response['Onboarding Tasks'][category].length > 0) {
                                                            console.log(category)

                                                            for (const onboarding_task of template_save_response['Onboarding Tasks'][category]) {
                                                                let insert_notif = {};
                                                                console.log(onboarding_task.assigneeEmail)
                                                                let taskAssignee = await UsersModel.findOne({ email: onboarding_task.assigneeEmail })
                                                                if (!taskAssignee) {
                                                                    return res.status(400).json({ status: false, message: "Employee selected for onboarding tasks is no longer active. Please update onboarding template", data: null });
                                                                }
                                                                insert_notif['companyId'] = companyId
                                                                insert_notif['from'] = req.jwt.userId
                                                                insert_notif['to'] = taskAssignee._id
                                                                insert_notif['impactedEmployee'] = user_save_response._id
                                                                insert_notif['subject'] = "On-boarding Tasks"
                                                                insert_notif['onboarding_template_id'] = template_save_response._id
                                                                insert_notif['task_id'] = onboarding_task._id
                                                                insert_notif['task_category'] = category
                                                                console.log('insert notif', insert_notif)
                                                                notifications.push(insert_notif)
                                                            }
                                                        }
                                                    }
                                                    console.log('notifications', notifications);
                                                    InboxModel.insertMany(notifications)
                                                        .then(notifications_response => {
                                                            console.log('notifs resp', notifications_response)
                                                            return res.status(200).json({ status: true, message: "Success", data: data });
                                                        })
                                                        .catch(err => {
                                                            console.log(err)
                                                            return res.status(400).json({ status: false, message: err, data: null });
                                                        })
                                                }
                                                else {
                                                    console.log(err);
                                                    return res.status(400).json({ status: false, message: err, data: null });
                                                }
                                            })
                                        })
                                    })
                                    .catch(err => {
                                        console.log(err);
                                        return res.status(400).json({ status: false, message: err, data: null });
                                    })
                            }
                            else {
                                console.log(err)
                                return res.status(400).json({ status: false, message: err, data: null });
                            }
                        })
                    })
                    .catch(err => {
                        console.log(err);
                        return res.status(400).json({ status: false, message: err, data: null });
                    })
            }
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).json({ status: false, message: err, data: null });
        });
};


let addEmployeeNew = (req, res) => {
    var body = req.body;
    // console.log(body);
    var companyId = req.jwt.companyId;
    let password_to_send_to_email, selected_template, userWithEmpID;

    body.email = body.personal.contact.workMail;
    body.companyId = companyId;
    body.type = "employee"
    body.onboardedBy = req.jwt.userId
    return new Promise((resolve, reject) => {
        if (!body.email) reject('enter email')
        resolve(null);
    })
        .then(() => {
            Promise.all([
                CompSettingsModel.findOne({ companyId: companyId }, { idNumber: 1 }),
                UsersModel.findOne({ companyId: companyId, type: 'employee' }, { 'job.employeeId': 1 }).sort({ _id: -1 }),
                UsersModel.findOne({ email: body.email })
            ])
                .then(async ([company, latestEmployee, userexists]) => {
                    console.log('userexists - ', userexists)
                    if(body.job.employeeId) {
                        userWithEmpID = await UsersModel.findOne({'job.employeeId': body.job.employeeId}).lean().exec()
                    }
                    if (userexists) {                        
                        res.status(409).json({ status: false, message: "Email already exists", data: null });
                    } else if (userWithEmpID) {
                        res.status(409).json({ status: false, message: "Employee already exists with this ID. Please change the employee ID by changing the settings to manual", data: null });
                    } else {
                        body.createdBy = req.jwt.userId;
                        body.updatedBy = req.jwt.userId;
                        password_to_send_to_email = utils.generateRandomPassword()
                        body.password = utils.encryptPassword(password_to_send_to_email);

                        // if (!body.job.employeeId) {
                        //     if (company.idNumber) {
                        //         if (company.idNumber.numberType === 'system Generated') {
                        //             try {
                        //                 body.job.employeeId = latestEmployee ? getNextEmployeeIdNew(latestEmployee, company.idNumber) : parseInt(company.idNumber.firstNumber);
                        //             }
                        //             catch (e) {
                        //                 console.log(e);
                        //             }
                        //         }
                        //     }
                        // }

                        body.job.current_employment_status = {
                            status: "Active",
                            effective_date: new Date()
                        }
                        body.job.employment_status_history = [
                            body.job.current_employment_status
                        ]
                        body.job.job_information_history = {
                            jobTitle: body.job.jobTitle,
                            department: body.job.department,
                            businessUnit: body.job.businessUnit,
                            unionFields: body.job.unionFields,
                            ReportsTo: body.job.ReportsTo,
                            effective_date: new Date()
                        }
                        let given_compensation_details = body.compensation
                        delete body.compensation
                        if (given_compensation_details.NonpaidPosition) {
                            body.compensation = {
                                "NonpaidPosition": given_compensation_details.NonpaidPosition
                            }
                        }
                        else {
                            body.compensation = {
                                "NonpaidPosition": given_compensation_details.NonpaidPosition,
                                // "Pay_group": given_compensation_details.Pay_group,
                                "Salary_Grade": given_compensation_details.Salary_Grade,
                                "grade_band": given_compensation_details.grade_band,
                                // "compensationRatio": given_compensation_details.compensationRatio,
                                "current_compensation": [{
                                    "effective_date": given_compensation_details.First_Pay_Date,
                                    "payRate": given_compensation_details.payRate.amount,
                                    "payRateUnit": given_compensation_details.payRateUnit,
                                    "payType": given_compensation_details.payRate.payType,
                                    "Pay_frequency": given_compensation_details.Pay_frequency,
                                    "Pay_group": given_compensation_details.Pay_group,
                                    "compensationRatio": given_compensation_details.compensationRatio,
                                    "changeReason": "--",
                                    "notes": "--"
                                }],
                                "compensation_history": []
                            }
                        }
                        
                        console.log('body.module_settings.onboarding_template - ', body.module_settings.onboarding_template)
                        if (body.module_settings.onboarding_template == "") {
                            delete body.module_settings.onboarding_template
                        }
                        if (body.module_settings.onboarding_template) {
                            selected_template = await OnboardingModel.findOne({ _id: body.module_settings.onboarding_template }).lean().exec()
                            for (let category of Object.keys(selected_template['Onboarding Tasks'])) {
                                if (selected_template['Onboarding Tasks'][category].length > 0) {
                                    for (const onboarding_task of selected_template['Onboarding Tasks'][category]) {
                                        console.log(onboarding_task.assigneeEmail)
                                        let taskAssignee = await UsersModel.findOne({ email: onboarding_task.assigneeEmail })
                                        if (!taskAssignee) {
                                            // return res.status(400).json({ status: false, message: "Employee selected for onboarding tasks is no longer active. Please update onboarding template", data: null });
                                            throw {
                                                message: 'Employee selected for onboarding tasks is no longer active. Please update onboarding template',
                                                code: 1
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        let data = await UsersModel.create(body)
                        let details_to_send_email = {
                            logo_url: config.baseUrl + '/api/client/images/mainblacklogo.png',
                            login_url: config.frontEndUrl + '/zenwork-login',
                            employee_name: Object.values(body.personal.name).slice(0, Object.values(body.personal.name).length - 1).join(" "),
                            // company_name:
                            // HR_Contact_Name: 
                            email: body.email,
                            password: password_to_send_to_email,
                            companyId: companyId,
                            HR_Contact: body.job.HR_Contact
                        }
                        let email_response = send_creds_to_employee_email(details_to_send_email);
                        console.log(email_response)
                        console.log('data ======>>> ', data);
                        console.log('body.module_settings.onboarding_template - ', body.module_settings.onboarding_template)
                        console.log('body.module_settings.skip_template_assignment - ', body.module_settings.skip_template_assignment)
                        if (!body.module_settings.onboarding_template && body.module_settings.skip_template_assignment) {
                            return res.status(200).json({ status: true, message: "Success", data: data });
                        }
                        else {
                            /**
                                *@author Asma
                                *@date 07/05/2019 11:42
                                Below code adds a derived template with all the task statuses = pending, based on the selected onboarding template given in req.body
                            */
                            selected_template = await OnboardingModel.findOne({ _id: data.module_settings.onboarding_template }).lean().exec()
                            let new_template = selected_template
                            delete new_template._id, delete new_template.otId, delete new_template.createdAt, delete new_template.updatedAt;
                            new_template.templateType = 'derived'
                            new_template.assigned_date = new Date()
                            new_template.onboarding_complete = 'No'
                            console.log(new_template)
                            for (let category of Object.keys(new_template['Onboarding Tasks'])) {
                                console.log(category)
                                if (new_template['Onboarding Tasks'][category]) {
                                    for (let onboarding_task of new_template['Onboarding Tasks'][category]) {
                                        onboarding_task.status = 'Pending'
                                        onboarding_task._id = mongoose.Types.ObjectId();
                                        onboarding_task.task_assigned_date = new Date()
                                        onboarding_task.task_assigned_by = req.jwt.userId
                                    }
                                }
                            }
                            let saved_template = await OnboardingModel.create(new_template)
                            data.module_settings.onboarding_template = saved_template._id
                            saved_template = saved_template.toJSON()
                            let user_save_response = await data.save()
                            // send notifications
                            let notifications = []
                            console.log('template save response - ', saved_template['Onboarding Tasks']['HR+sales'])
                            console.log('new template keys', Object.keys(saved_template['Onboarding Tasks']))
                            for (let category of Object.keys(saved_template['Onboarding Tasks'])) {
                                console.log(category)
                                if (saved_template['Onboarding Tasks'][category].length > 0) {
                                    console.log(category)
                                    for (const onboarding_task of saved_template['Onboarding Tasks'][category]) {
                                        let insert_notif = {};
                                        console.log(onboarding_task.assigneeEmail)
                                        let taskAssignee = await UsersModel.findOne({ email: onboarding_task.assigneeEmail })
                                        if (!taskAssignee) {
                                            throw {
                                                message: 'Employee selected for onboarding tasks is no longer active. Please update onboarding template',
                                                code: 1
                                            }
                                        }
                                        insert_notif['companyId'] = companyId
                                        insert_notif['from'] = req.jwt.userId
                                        insert_notif['to'] = taskAssignee._id
                                        insert_notif['impactedEmployee'] = user_save_response._id
                                        insert_notif['subject'] = "On-boarding Tasks"
                                        insert_notif['onboarding_template_id'] = saved_template._id
                                        insert_notif['task_id'] = onboarding_task._id
                                        insert_notif['task_category'] = category
                                        console.log('insert notif', insert_notif)
                                        notifications.push(insert_notif)
                                    }
                                }
                            }
                            console.log('notifications', notifications);
                            let notifications_response = await InboxModel.insertMany(notifications)
                            console.log('notifications_response - ', notifications_response)
                            res.status(200).json({ status: true, message: "Success", data: data });
                        } 
                    }
                })
                .catch(err => {
                    console.log(err)
                    if (err.code == 1) {
                        res.status(400).json({
                            status: false,
                            message: err.message
                        })
                    }
                    else {
                        res.status(500).json({
                            status: false,
                            message: "Error while onboarding employee",
                            error: err
                        })
                    }
                })
        })
        .catch((err) => {
            console.log(err);
            res.status(400).json({ status: false, message: err, data: null });
        });
};


/**
 * Author:Sai Reddy  Date:29/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to Search Employees
 */
let searchEmployee = (req, res) => {
    var body = req.body;
    var CompanyId = req.jwt.companyId;
    console.log(CompanyId)
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let query = {
            companyId: CompanyId,
            // type: { $ne: 'company' }, 
            type: 'employee',
            'job.current_employment_status.status': { $ne: 'Terminated' }
        };
        return Promise.all([
            UsersModel.find(query, { email: 1, type: 1, "personal.name": 1, "personal.email": 1 }),
            UsersModel.countDocuments(query)
        ])
    }).then(([data, count]) => {
        return res.status(200).json({ status: true, message: "Success", count, data });
    }).catch((err) => {
        console.log(err);
        return res.status(400).json({ status: false, message: err, data: null });
    });
};

/**
*@author Asma
*@date 07/05/2019 20:37

*/
let listEmployees = (req, res) => {
    var body = req.body;
    var CompanyId = req.jwt.companyId;
    console.log('companyId: ', CompanyId)
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let query = { companyId: ObjectID(CompanyId), type: { $ne: 'company' } };

        if (body.Specific_Workflow_Approval) {
            query['job.Specific_Workflow_Approval'] = body.Specific_Workflow_Approval
            console.log(query)
            return Promise.all([
                UsersModel.find(query, { password: 0 }).sort(body.sort)/*.skip((page_no - 1) * per_page).limit(per_page) */.lean().exec(),
                UsersModel.countDocuments(query)
            ])
        }
        // else if (body._ids && body._ids.length) {
        else if (body._ids) {
            let per_page = body.per_page ? body.per_page : 10
            let page_no = body.page_no ? body.page_no : 1

            let sort_obj = { createdAt: -1 }

            query['_id'] = {
                $in: body._ids
            }
            console.log(query)
            return Promise.all([
                UsersModel.find(query, { password: 0 }).populate('job.ReportsTo').populate('position_work_schedule_id').sort(sort_obj).skip((page_no - 1) * per_page).limit(per_page).lean().exec(),
                UsersModel.countDocuments(query)
            ])
        }
        else {
            let per_page = body.per_page ? body.per_page : 10
            let page_no = body.page_no ? body.page_no : 1
            if (body.employment_status && body.employment_status.length > 0) {
                query['job.current_employment_status.status'] = { $in: body.employment_status }
            }
            if (body.employment_type && body.employment_type.length > 0) {
                query['job.employeeType'] = { $in: body.employment_type }
            }
            if (body.location && body.location.length > 0) {
                query['job.location'] = { $in: body.location }
            }
            if (body.department && body.department.length > 0) {
                query['job.department'] = { $in: body.department }
            }

            if (body.search_query) {
                query['$or'] = [
                    {
                        'personal.name.firstName': { '$regex': body.search_query, '$options': 'i' }
                    },
                    {
                        'personal.name.middleName': { '$regex': body.search_query, '$options': 'i' }
                    },
                    {
                        'personal.name.lastName': { '$regex': body.search_query, '$options': 'i' }
                    },
                    {
                        'personal.name.preferredName': { '$regex': body.search_query, '$options': 'i' }
                    },
                    {
                        'job.jobTitle.name': { '$regex': body.search_query, '$options': 'i' }
                    },
                    {
                        'job.department': { '$regex': body.search_query, '$options': 'i' }
                    },
                    {
                        'job.employeeId': { '$regex': body.search_query, '$options': 'i' }
                    }
                ]
            }

            let lookup_stage = {
                $lookup:
                {
                    from: "structures",
                    localField: "job.jobTitle",
                    foreignField: "_id",
                    as: "jobTitle"
                }
            }

            let add_field_stage = {
                $addFields: {
                    "job.jobTitle": { $arrayElemAt: ["$jobTitle", 0] }
                }
            }

            let project_stage = {
                $project: {
                    "jobTitle": 0
                }
            }

            let match_stage = {
                $match: query
            }

            let sort_obj = { createdAt: -1 }

            let sort_stage = {
                $sort: sort_obj
            }

            let skip_stage = {
                $skip: (page_no - 1) * per_page
            }

            let limit_stage = {
                $limit: per_page
            }

            console.log(query)
            // if (!body.sort) {
            //     body.sort = {
            //         updatedAt: -1
            //     }
            // }
            // else if (!body.sort.hasOwnProperty("updatedAt")){ 
            //     body.sort.updatedAt = -1
            // }
            // return Promise.all([
            //     // UsersModel.find(query, { password: 0 }).sort(body.sort).skip((page_no - 1) * per_page).limit(per_page).lean().exec(),
            //     UsersModel.find(query, { password: 0 }).populate('job.job_information_history.ReportsTo', 'personal.name').sort({ createdAt: -1 }).skip((page_no - 1) * per_page).limit(per_page).lean().exec(),
            //     UsersModel.count(query)
            // ])

            return Promise.all([
                UsersModel.aggregate([
                    lookup_stage,
                    add_field_stage,
                    project_stage,
                    match_stage,
                    sort_stage,
                    skip_stage,
                    limit_stage
                ]),
                UsersModel.aggregate([
                    lookup_stage,
                    add_field_stage,
                    match_stage,
                    {
                        $count: "total_count"
                    }
                ])
            ])
        }

    }).then(async ([data, total_count_array]) => {
        console.log('total_count_array - ', total_count_array)
        for (let user of data) {
            user = utils.formatUserData(user);
            // user = user.toJSON()
            // console.log(user)
            // console.log(user.personal.profile_pic_details)
            if (user.personal.profile_pic_details) {
                let user_profile_pic_details_with_url = await s3.getPresignedUrlOfAfile('zenworkhr-docs', 'profile_photos', user.personal.profile_pic_details)
                user.personal.profile_pic_details = user_profile_pic_details_with_url
                // console.log(user.personal.profile_pic_details)
            }
            if (user.position_work_schedule_id && user.position_work_schedule_id.weekApplicable) {
                for (let dayDetails of user.position_work_schedule_id.weekApplicable) {
                    dayDetails.shiftStartTime = convertTo12Format(dayDetails.shiftStartTime)
                    dayDetails.shiftEndTime = convertTo12Format(dayDetails.shiftEndTime)
                    if (dayDetails.lunchStartTime) {
                        dayDetails.lunchStartTime = convertTo12Format(dayDetails.lunchStartTime)
                    }
                    if (dayDetails.lunchEndTime) {
                        dayDetails.lunchEndTime = convertTo12Format(dayDetails.lunchEndTime)
                    }
                }
            }
        }
        if (Array.isArray(total_count_array)) {
            return res.status(200).json(
                {
                    status: true,
                    message: "Success",
                    total_count: total_count_array[0] ? total_count_array[0].total_count : 0,
                    data: data
                }
            );
        }
        else {
            return res.status(200).json(
                {
                    status: true,
                    message: "Success",
                    total_count: total_count_array,
                    data: data
                }
            );
        }

    }).catch((err) => {
        console.log(err);
        return res.status(400).json({ status: false, message: err, data: null });
    });
};

/**
 * Author:Sai Reddy  Date:01/05/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to Choose Employees for roles
 */
let chooseEmployeesForRoles = (req, res) => {
    var body = req.body;
    var CompanyId = req.jwt.companyId;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let query = { companyId: CompanyId };
        if (body.field === 'Business Unit') query['job.businessUnit'] = { $in: body.chooseEmployeesForRoles };
        else if (body.field === 'Department') query['job.department'] = { $in: body.chooseEmployeesForRoles };
        else if (body.field === 'Location') query['job.location'] = { $in: body.chooseEmployeesForRoles };
        else if (body.field === 'Employee Type') query['job.employeeType'] = { $in: body.chooseEmployeesForRoles };
        else if (body.field === 'Union') query['job.unionFields'] = { $in: body.chooseEmployeesForRoles };
        console.log(query);
        return UsersModel.find(query, { email: 1, type: 1, "personal.name": 1, "personal.email": 1, job: 1 });
    }).then(data => {
        return res.status(200).json({ status: true, message: "Success", data });
    }).catch((err) => {
        console.log(err);
        return res.status(400).json({ status: false, message: err, data: null });
    });
};


/**
 * Author:Sai Reddy  Date:01/05/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to Add Groups to Employees
 */
// let addGroupsToUsers = (req, res) => {
//     var body = req.body;
//     var CompanyId = req.jwt.companyId;
//     return new Promise((resolve, reject) => {
//         resolve(null);
//     }).then(() => {
//         let array = [];
//         for (let i = 0; i < body.users.length; i++) {
//             array.push(new Promise((resolve, reject) => {
//                 UsersModel.findOneAndUpdate({ _id: body.users[i] }, { $pull: { groups: body.roleId } }).exec()
//                     .then(response => {
//                         UsersModel.findOneAndUpdate({ _id: body.users[i] }, { $push: { groups: body.roleId } }).exec()
//                             .then(response => {
//                                 resolve(response);
//                             }).catch(err => {
//                                 reject(err);
//                             })
//                     }).catch(err => {
//                         reject(err);
//                     })
//             }));
//         }
//         // let query = {companyId:CompanyId,type:{$ne:'company'}};
//         return Promise.all(array);
//     }).then(data => {
//         return res.status(200).json({ status: true, message: "Success", data });
//     }).catch((err) => {
//         console.log(err);
//         return res.status(400).json({ status: false, message: err, data: null });
//     });
// };


let addGroupsToUsers = (req, res) => {
    var body = req.body;
    var companyId = req.jwt.companyId;
    return new Promise((resolve, reject) => {
        if (!body.users) reject('select employees')
        if (!body.users.length) reject('select employees')
        resolve(null);
    })
        .then(() => {
            UsersModel.updateMany({ companyId: companyId, _id: { $in: body.users } }, { $addToSet: { groups: body.roleId } })
                .then(response => {
                    res.status(200).json({ status: true, message: "Success", response });
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).json({
                        status: false,
                        message: "Error while adding employees under role",
                        error: err
                    })
                })
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).json({ status: false, message: err, data: null });
        });
}

/**
*@author Asma
*@date 10/07/2019 14:09
API to get users with given group/ role
*/
let getUsersUnderGivenGroup = (req, res) => {
    let companyId = req.params.company_id
    let roleId = req.params.role_id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
        .then(() => {
            UsersModel.find({ companyId: ObjectID(companyId), groups: roleId }, { 'personal.name': 1 }).lean().exec()
                .then(users => {
                    let user_ids = []
                    user_ids = users.map(x => x._id)
                    console.log(user_ids)
                    res.status(200).json({
                        status: true,
                        message: "Successsfully fetched users under this group/role",
                        data: user_ids
                    })
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).json({
                        status: false,
                        message: "Error while fetching users under this group/role",
                        error: err
                    })
                })
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                status: false,
                message: "Error while fetching users under this group/role",
                error: err
            })
        })
}


/**
 * Author:Sai Reddy  Date:22/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to add Custom wizard
 */
let addUpdateCustomWizard = (req, res) => {
    var body = req.body;
    var CompanyId = req.jwt.companyId;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        if (body._id) {
            return HireWizardModel.findOneAndUpdate({ _id: body._id }, { $set: body });
        } else {
            body.companyId = CompanyId;
            return HireWizardModel.create(body);
        }
    }).then(data => {
        if (data) {
            return res.status(200).json({ status: true, message: "Success" });
        } else {
            return res.status(404).json({ status: false, message: "User Not Found" });
        }
    }).catch((err) => {
        console.log(err);
        return res.status(400).json({ status: false, message: err, data: null });
    });
};

/**
 * Author:Sai Reddy  Date:01/05/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to GET all Custom wizards
 */
let getAllCustomWizards = (req, res) => {
    var CompanyId = req.jwt.companyId;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return HireWizardModel.find({ companyId: CompanyId });
    }).then(data => {
        if (data) {
            return res.status(200).json({ status: true, message: "Success", data });
        } else {
            return res.status(404).json({ status: false, message: "Data Not Found" });
        }
    }).catch((err) => {
        console.log(err);
        return res.status(400).json({ status: false, message: err, data: null });
    });
};

/**
 * Author:Sai Reddy  Date:29/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to add or update Custom wizard
 */
let getCustomWizard = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return HireWizardModel.findOne({ _id: req.params._id });
    }).then(data => {
        if (data) {
            return res.status(200).json({ status: true, message: "Success", data });
        } else {
            return res.status(404).json({ status: false, message: "Data Not Found" });
        }
    }).catch((err) => {
        console.log(err);
        return res.status(400).json({ status: false, message: err, data: null });
    });
};


/**
 * Author:Sai Reddy  Date:29/04/2019 JiraId: https://zenwork.atlassian.net/browse/ZV-2
 * API to delete Custom wizard
 */
let deleteCustomWizards = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return HireWizardModel.remove({ _id: { $in: req.body } });
    }).then(data => {
        if (data) {
            return res.status(200).json({ status: true, message: "Success" });
        } else {
            return res.status(404).json({ status: false, message: "Data Not Found" });
        }
    }).catch((err) => {
        console.log(err);
        return res.status(400).json({ status: false, message: err, data: null });
    });
};

// let build_notif_objs = (template) => {
//     return new Promise(async (resolve, reject) => {
//         let notifications = []
//         Object.keys(template_save_response['Onboarding Tasks']).forEach(async category => {
//             if (template_save_response['Onboarding Tasks'][category].length > 0) {
//                 console.log(category)
//                 await template_save_response['Onboarding Tasks'][category].forEach(async onboarding_task => {
//                     let insert_notif = {};
//                     console.log(onboarding_task.assigneeEmail)
//                     let taskAssignee = await UsersModel.findOne({email: onboarding_task.assigneeEmail})
//                     insert_notif['companyId'] = companyId
//                     insert_notif['from'] = req.jwt.userId
//                     insert_notif['to'] = taskAssignee._id
//                     insert_notif['impactedEmployee'] = user_save_response._id
//                     insert_notif['subject'] = "On-boarding Tasks"
//                     insert_notif['onboarding_template_id'] = template_save_response._id
//                     insert_notif['task_id'] = onboarding_task._id
//                     insert_notif['task_category'] = category
//                     console.log('insert notif', insert_notif)
//                     notifications.push(insert_notif)
//                 });
//                 resolve(notifications)
//             }
//         })
//     })
// }

let calculateCompensationRatio = (req, res) => {
    let body = req.body
    let companyId = req.jwt.companyId
    return new Promise((resolve, reject) => {
        if (!body.payRate) reject('payRate is required');
        else if (!body.payType) reject('payType is required');
        else if (!body.pay_frequency) reject('pay_frequency is required');
        else if (!body.grade_band) reject('grade_band is required');
        else resolve(GradeBandsModel.findOne({ companyId: companyId, _id: body.grade_band }).lean().exec())
    })
        .then(grade_band => {
            let total_compensation;
            if (body.payType == 'Salary') {
                total_compensation = body.payRate
            }
            else if (body.payType == 'Hourly') {
                if (!body.workHours) {
                    res.status(400).json({
                        status: false,
                        message: "enter work hours"
                    })
                }

                let pay_frequency_days = {
                    "Weekly": 7,
                    "Bi-Weekly": 14,
                    "Semi-Monthly": 15,
                    "Monthly": 31
                }

                // total_compensation = body.payRate * body.workHours * pay_frequency_days[body.pay_frequency]
                total_compensation = body.payRate * body.workHours

            }

            console.log('total_compensation - ', total_compensation)

            if (!(grade_band.min <= total_compensation && total_compensation <= grade_band.max)) {
                res.status(400).json({
                    status: false,
                    message: "Compensation does not fall under the specified grade band"
                })
            }
            else {
                let compensationRatio = total_compensation / grade_band.mid
                console.log('compensationRatio - ', compensationRatio)
                res.status(200).json({
                    status: true,
                    message: "Successfully calculated compensation ratio",
                    data: {
                        compensationRatio: compensationRatio
                    }
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

module.exports = {
    addEmployee,
    searchEmployee,
    chooseEmployeesForRoles,
    addGroupsToUsers,
    addUpdateCustomWizard,
    getAllCustomWizards,
    getCustomWizard,
    deleteCustomWizards,
    listEmployees,
    getUsersUnderGivenGroup,
    generateEmployeeId,
    calculateCompensationRatio,
    generateEmployeeIdNew,
    addEmployeeNew
};


// body.compensation = {
//     "NonpaidPosition": given_compensation_details.NonpaidPosition,
//     "Pay_group": given_compensation_details.Pay_group,
//     "Pay_frequency": given_compensation_details.Pay_frequency,
//     "Salary_Grade": given_compensation_details.Salary_Grade,
//     "compensationRatio": given_compensation_details.compensationRatio,
//     "current_compensation": [{
//         "effective_date": given_compensation_details.First_Pay_Date,
//         "payRate": {
//             "amount": given_compensation_details.payRate.amount
//         },
//         "payType": given_compensation_details.payType,
//         "Pay_frequency": given_compensation_details.Pay_frequency,
//         "changeReason": "--",
//         "notes": "--"
//     }],
//     "compensation_history": []
// }