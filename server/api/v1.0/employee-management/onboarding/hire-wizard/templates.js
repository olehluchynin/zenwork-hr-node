let addEmployeeData = {
    personal: {
        name: {
            firstName: "",
            lastName: "",
            middleName: "",
            preferredName: ""
        },
        dob: "",
        gender: "",
        maritalStatus: "",
        effectiveDate: "",
        veteranStatus: "",
        ssn: "",
        ethnicity: "",
        address: {
            street1: "",
            street2: "",
            city: "",
            state: "",
            zipcode: "",
            country: ""
        },
        contact: {
            workPhone: "",
            extention: "",
            mobilePhone: "",
            homePhone: "",
            workMail: "",
            personalMail: ""
        },
    },

    job: {
        employeeId: "",
        employeeType: "",
        hireDate: Date,
        jobTitle: "",
        department: "",
        location: "",
        businessUnit: "",
        unionFields: "",
        FLSA_Code: "",
        EEO_Job_Category: "",
        Site_AccessRole: "",
        UserRole: "",
        VendorId: "",
        Specific_Workflow_Approval: "",
        HR_Contact: "",
        ReportsTo: "",
    },

    compensation: {
        NonpaidPosition: "",
        Pay_group: "",
        Pay_frequency: "",
        First_Pay_Date: "",
        payRate: {
            payType: "",
            amount: "",
            unit: ""
        },
        compensationRatio: "",
        Salary_Grade: ""
    },

    module_settings: {
        eligible_for_benifits: true,
        assign_leave_management_rules: true,
        assign_performance_management: false,
        assaign_employee_number: 'System Generated',    //any one in ["System Generated", "Manual"]
        onboarding_template: ""
    }
};
