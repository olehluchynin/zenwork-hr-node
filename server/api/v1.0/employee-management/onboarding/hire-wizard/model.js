let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('../../../../../common/constants');

let Schema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies'},

    module_settings: {
        wizardName: {type: String},
        eligible_for_benifits: Boolean,
        assign_leave_management_rules: Boolean,
        assign_performance_management: Boolean,
        assaign_employee_number: {type: String, enum: constants.idNumberType},
        onboarding_template: {type:ObjectID,ref:'onboarding_templates'}
    },

    createdBy: {type:ObjectID,ref:'users'},
    updatedBy:{type:ObjectID,ref:'users'},
},{
    timestamps: true,
    versionKey: false,
    strict: false
});

Schema.plugin(autoIncrement.plugin, {model:'wizards', field:'wizardId', startAt: 1, incrementBy: 1});

let TrainingModel = mongoose.model('wizards',Schema);

module.exports = TrainingModel;
