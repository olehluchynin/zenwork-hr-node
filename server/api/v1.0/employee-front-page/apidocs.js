/**
 * @api {post} /employee-front-page/announcement/add AddCompanyAnnouncement
 * @apiName AddCompanyAnnouncement
 * @apiGroup EmployeeFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"description": "company outing on 25th",
	"title": "outing",
	"scheduled_date": "2019-06-27T06:13:49.909Z",
	"expiration_date": "2019-06-30T06:13:49.909Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Added company announcement successfully",
    "data": {
        "_id": "5d1d8d6f5e9aa93030a9364c",
        "companyId": "5c9e11894b1d726375b23058",
        "description": "company outing on 25th",
        "title": "outing",
        "scheduled_date": "2019-06-27T06:13:49.909Z",
        "expiration_date": "2019-06-30T06:13:49.909Z",
        "createdAt": "2019-07-04T05:23:59.657Z",
        "updatedAt": "2019-07-04T05:23:59.657Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /employee-front-page/announcement/list/:company_id ListCompanyAnnouncements
 * @apiName ListCompanyAnnouncements
 * @apiGroup EmployeeFrontPage
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched list of company announcements",
    "data": [
        {
            "_id": "5d1d8fb15e9aa93030a9364d",
            "companyId": "5c9e11894b1d726375b23058",
            "description": "Workshop on 19th",
            "title": "workshop",
            "scheduled_date": "2019-06-27T06:13:49.909Z",
            "expiration_date": "2019-06-30T06:13:49.909Z",
            "createdAt": "2019-07-04T05:33:37.967Z",
            "updatedAt": "2019-07-04T05:33:37.967Z"
        },
        {
            "_id": "5d1d8d6f5e9aa93030a9364c",
            "companyId": "5c9e11894b1d726375b23058",
            "description": "company outing on 25th",
            "title": "outing",
            "scheduled_date": "2019-06-27T06:13:49.909Z",
            "expiration_date": "2019-06-30T06:13:49.909Z",
            "createdAt": "2019-07-04T05:23:59.657Z",
            "updatedAt": "2019-07-04T05:23:59.657Z"
        }
    ]
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {put} /employee-front-page/announcement/edit EditCompanyAnnouncement
 * @apiName EditCompanyAnnouncement
 * @apiGroup EmployeeFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "_id": "5d1d8fb15e9aa93030a9364d",
    "companyId": "5c9e11894b1d726375b23058",
    "description": "Workshop on 19th",
    "title": "Workshop",
    "scheduled_date": "2019-06-27T06:13:49.909Z",
    "expiration_date": "2019-06-30T06:13:49.909Z",
    "createdAt": "2019-07-04T05:33:37.967Z",
    "updatedAt": "2019-07-04T05:33:37.967Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Company announcement updated successfully",
    "data": {
        "_id": "5d1d8fb15e9aa93030a9364d",
        "companyId": "5c9e11894b1d726375b23058",
        "description": "Workshop on 19th",
        "title": "Workshop",
        "scheduled_date": "2019-06-27T06:13:49.909Z",
        "expiration_date": "2019-06-30T06:13:49.909Z",
        "createdAt": "2019-07-04T05:33:37.967Z",
        "updatedAt": "2019-07-04T05:41:03.555Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /employee-front-page/announcement/delete DeleteEmployeeAnnouncement
 * @apiName DeleteEmployeeAnnouncement
 * @apiGroup EmployeeFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"_ids": [
		"5d1d8d6f5e9aa93030a9364c"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /employee-front-page/settings/addOrUpdate AddOrUpdateSettings
 * @apiName AddOrUpdateSettings
 * @apiGroup EmployeeFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "show_time": true,
    "show_benifits": true,
    "show_training": true,
    "show_company_policies": false,
    "show_birthdays": false,
    "birthday_settings": "My Team Only",
    "show_work_anniversary": true,
    "work_anniversary_settings": "My Team Only",
    "show_company_links": "true"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Employee settings updated successfully",
    "data": {
        "_id": "5d1dc514c8f4768976d8bf3c",
        "companyId": "5c9e11894b1d726375b23058",
        "birthday_settings": "My Team Only",
        "createdAt": "2019-07-04T09:21:25.541Z",
        "show_benifits": true,
        "show_birthdays": false,
        "show_company_links": true,
        "show_company_policies": false,
        "show_time": true,
        "show_training": true,
        "show_work_anniversary": true,
        "updatedAt": "2019-07-04T09:21:25.541Z",
        "work_anniversary_settings": "My Team Only",
        "company_links": []
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /employee-front-page/settings/get/:company_id GetEmployeeSettings
 * @apiName GetEmployeeSettings
 * @apiGroup EmployeeFrontPage
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Fetched employee settings successfully",
    "data": {
        "_id": "5d1dc514c8f4768976d8bf3c",
        "companyId": "5c9e11894b1d726375b23058",
        "birthday_settings": "My Team Only",
        "createdAt": "2019-07-04T09:21:25.541Z",
        "show_benifits": true,
        "show_birthdays": false,
        "show_company_links": true,
        "show_company_policies": false,
        "show_time": true,
        "show_training": true,
        "show_work_anniversary": true,
        "updatedAt": "2019-07-04T09:21:25.541Z",
        "work_anniversary_settings": "My Team Only"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

//===============================================================================

/**
 * @api {post} /employee-front-page/links/add AddCompanyLink
 * @apiName AddCompanyLink
 * @apiGroup EmployeeFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
[
    {
        "site_name": "zenwork",
        "site_link": "zenworkhr.com"
    },
    {
    	"site_name": "zenwork_testing",
        "site_link": "test.zenworkhr.com"
    }
]
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Company links added successfully",
    "data": {
        "_id": "5d286af6c8f4768976dd5ac0",
        "companyId": "5c9e11894b1d726375b23058",
        "birthday_settings": "My Team Only",
        "createdAt": "2019-07-12T11:11:50.677Z",
        "createdBy": "5c9e11894b1d726375b23057",
        "show_benifits": false,
        "show_birthdays": true,
        "show_company_links": false,
        "show_company_policies": false,
        "show_time": false,
        "show_training": false,
        "show_work_anniversary": false,
        "updatedAt": "2019-08-07T09:01:51.099Z",
        "work_anniversary_settings": "My Department Only",
        "company_links": [
            {
                "_id": "5d286b9f438fc06e8614902c",
                "site_name": "mtwlabs",
                "site_link": "https://www.mtwlbas.com",
                "createdBy": "5c9e11894b1d726375b23057",
                "updatedBy": "5c9e11894b1d726375b23057"
            },
            {
                "_id": "5d4a937fe21fa13b68a138bd",
                "site_name": "zenwork",
                "site_link": "zenworkhr.com",
                "createdBy": "5cda377f51f0be25b3adceb4"
            },
            {
                "_id": "5d4a937fe21fa13b68a138bc",
                "site_name": "zenwork_testing",
                "site_link": "test.zenworkhr.com",
                "createdBy": "5cda377f51f0be25b3adceb4"
            }
        ]
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {put} /employee-front-page/links/edit EditCompanyLink
 * @apiName EditCompanyLink
 * @apiGroup EmployeeFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "linkId": "5d1dc8dfdbbfbc0d8489539c",
    "site_name": "zenworkHR",
    "site_link": "https://zenworkhr.com"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Updated company link successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /employee-front-page/links/delete DeleteCompanyLinks
 * @apiName DeleteCompanyLinks
 * @apiGroup EmployeeFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "_ids": [
    	"5d1dca85dbbfbc0d8489539d"
    	]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted links successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */
