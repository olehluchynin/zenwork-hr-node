let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../config/config');
let _ = require('underscore');
let CompanyAnnouncementsModel = require('./model').CompanyAnnouncementsModel;
let EmployeeFrontPageSettingsModel = require('./model').EmployeeFrontPageSettingsModel;

let addAnnouncement = (req,res) => {
    let body = req.body
    return new Promise((resolve,reject) => {
        resolve(null);
    })
    .then(() => {
        body.createdBy =  ObjectID(req.jwt.userId)
        CompanyAnnouncementsModel.create(body)
            .then(CompanyAnnouncement => {
                CompanyAnnouncement.save(function (err, response) {
                    if (!err) {
                        res.status(200).json({
                            status: true,
                            message: "Added company announcement successfully",
                            data: response
                        })
                    }
                    else {
                        console.log(err)
                        res.status(400).json({
                            status: false,
                            message: "Error while adding company announcement",
                            error: err
                        })
                    }
                })
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while adding company announcement",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    });
}

let list = (req, res ) => {
    let companyId = req.params.company_id
    // let userId = req.params.user_id
    // let announcements_filter = re.params.announcements_filter
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        let sort = {
            updatedAt : -1
        }
        let filter = {
            companyId: ObjectID(companyId),
            // userId : ObjectID(userId)
        }
        // if (announcements_filter == 'active') {
        //     let 
        // }
        CompanyAnnouncementsModel.find(filter)
            .sort(sort)
            .lean().exec()
            .then(CompanyAnncmnts => {

                res.status(200).json({
                    status: true, message: "Successfully fetched list of company announcements", data: CompanyAnncmnts
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while fetching company announcements",
                    error: err
                })
            });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let editCA = (req,res) => {
    let body = req.body
    let ca_id = body._id
    let companyId = body.companyId
    // let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!ca_id) {
            reject('_id is required')
        }
        else if (!companyId) {
            reject('companyId is required')
        }
        // else if (!userId) {
        //     reject('userId is required')
        // }
        else {
            resolve(null);
        }
    })
    .then(() => {
        delete body.companyId
        // delete body.userId
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        body.updatedBy = ObjectID(req.jwt.userId)
        console.log(body)
        let filter = {
            _id: ObjectID(ca_id), 
            companyId : ObjectID(companyId), 
            // userId: ObjectID(userId)
        }
        console.log(filter)
        CompanyAnnouncementsModel.findOneAndUpdate(filter, {$set: body}, {new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                res.status(200).json({
                    status: true,
                    message: "Company announcement updated successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while updating company announcement",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    });
}

let deleteCA = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    // let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        // else if (!userId) reject('userId is required');
        else resolve(null);
    })
    .then(() => {
        CompanyAnnouncementsModel.deleteMany({
            companyId: companyId,
            // userId: userId,
            _id: {$in : body._ids}
        })
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting company announcements",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let getSettings = (req, res) => {
    let companyId = req.params.company_id
    // let userId = req.params.user_id
    // let MS_id = req.params._id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        let find_query = {
            // _id: ObjectID(MS_id),
            // userId: ObjectID(userId),
            companyId: ObjectID(companyId)
        }
        EmployeeFrontPageSettingsModel.findOne(find_query)
            .then(employee_settings => {
                if(employee_settings) {
                    if (employee_settings.company_links) {
                        employee_settings.company_links = employee_settings.company_links.reverse();
                    }
                    res.status(200).json({
                        status: true,
                        message: "Fetched employee settings successfully",
                        data: employee_settings
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "Employee settings not found",
                        data: null
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while fecthing employee settings",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err);
        res.status(400).json({
            status: false,
            message: "Error while fecthing employee settings",
            error: err
        })
    })
}

let addOrUpdateSettings = (req,res) => {
    let body = req.body
    // let ms_id = body._id
    let companyId = body.companyId
    // let userId = body.userId
    return new Promise((resolve, reject) => {
        // if (!ms_id) {
        //     reject('_id is required')
        // }
        if (!companyId) {
            reject('companyId is required')
        }
        // else if (!userId) {
        //     reject('userId is required')
        // }
        else {
            resolve(null);
        }
    })
    .then(() => {
        // delete body.companyId
        // delete body.userId
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        console.log(body)
        let filter = {
            // _id: ObjectID(ms_id), 
            companyId : ObjectID(companyId), 
            // userId: ObjectID(userId)
        }
        console.log(filter)
        if (!body._id) {
            body.createdBy =  ObjectID(req.jwt.userId)
        }
        else {
            body.updatedBy = ObjectID(req.jwt.userId)
        }
        EmployeeFrontPageSettingsModel.findOneAndUpdate(filter, {$set: body}, {upsert: true, new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                res.status(200).json({
                    status: true,
                    message: "Employee settings updated successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while updating employee settings",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    });
}

let addLink = (req, res) =>{
    let body = req.body
    let companyId = req.jwt.companyId
    // let userId = body.userId

    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        
        let filter = {
            companyId : ObjectID(companyId), 
            // userId: ObjectID(userId)
        }
        body = body.map(x => {
            x.createdBy = ObjectID(req.jwt.userId)
            return x
        })
        EmployeeFrontPageSettingsModel.findOne(filter)
            .then(settings => {
                if (settings) {
                    settings.company_links = settings.company_links.concat(body)
                    settings.save((err, response) => {
                        if (!err) {
                            res.status(200).json({
                                status: true,
                                message: "Company links added successfully",
                                data: response
                            })
                        }
                        else {
                            console.log(err);
                            res.status(400).json({
                                status: false,
                                message: "Error while adding company links",
                                error: err
                            })
                        }
                    })
                }
                else {
                    EmployeeFrontPageSettingsModel.create({
                        companyId: companyId,
                        company_links: body
                    })
                    .then(response => {
                        res.status(200).json({
                            status: true,
                            message: "Company links added successfully",
                            data: response
                        })
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(400).json({
                            status: false,
                            message: "Error while adding company links",
                            error: err
                        })
                    })
                }
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({
                    status: false,
                    message: "Error while adding company links",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err);
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let editLink = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    // let userId = body.userId
    let linkId = body.linkId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        // else if (!userId) reject('userId is required');
        else if (!linkId) reject('linkId is requried')
        else resolve(null);
    })
    .then(() => {
        delete body.companyId
        // delete body.userId
        delete body.linkId
        let update_filter = {
            companyId: companyId,
            // userId: userId,
            "company_links._id" : linkId
        }
        body.updatedBy = ObjectID(req.jwt.userId)
        console.log(body)
        var updateObj = {};
        for (let key in body) {
            updateObj['company_links.$.' + key] = body[key]
        }
        console.log(updateObj)

        EmployeeFrontPageSettingsModel.updateOne(
            update_filter, 
            {
                $set: updateObj
            }, 
            { 
                new: true
            }
        )
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Updated company link successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while updating company link",
                error : err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let deleteLinks = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    // let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        // else if (!userId) reject('userId is required');
        else resolve(null);
    })
    .then(() => {
        let update_filter = {
            companyId: companyId,
            // userId: userId
        }
        EmployeeFrontPageSettingsModel.updateOne(update_filter, {
            $pull: {
                'company_links' : {_id: {$in : body._ids}}
            }
        }, {new: true})
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted links successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting links",
                error : err
            })
        });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

module.exports = {
    addAnnouncement,
    list,
    editCA,
    deleteCA,
    addOrUpdateSettings,
    getSettings,
    addLink,
    editLink,
    deleteLinks
}