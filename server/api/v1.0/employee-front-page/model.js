let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;

let CompanyAnnouncementsSchema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    // userId: {type: ObjectID, ref: 'users', required:true},
    createdBy: {type: ObjectID, ref: 'users'},
    updatedBy: {type: ObjectID, ref: 'users'},
    description: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    scheduled_date: {
        type: Date
    },
    expiration_date: {
        type: Date
    }

}, {
    timestamps: true,
    versionKey: false
})

let CompanyAnnouncementsModel = mongoose.model('company_announcements', CompanyAnnouncementsSchema, 'company_announcements');

let EmployeeFrontPageSettingsSchema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    // userId: {type: ObjectID, ref: 'users', required: true},
    createdBy: {type: ObjectID, ref: 'users'},
    updatedBy: {type: ObjectID, ref: 'users'},
    show_time: { type: Boolean },
    show_benifits: { type: Boolean },
    show_training: { type: Boolean },
    show_company_policies: { type: Boolean },
    show_birthdays: { type: Boolean },
    birthday_settings: {type: String, enum: ['My Team Only', 'My Department Only', 'Entire Company']},
    show_work_anniversary: { type: Boolean },
    work_anniversary_settings: {type: String, enum: ['My Team Only', 'My Department Only', 'Entire Company']},
    show_company_links: { type: Boolean },
    company_links: [
        {
            site_name: {
                type:String
            },
            site_link: {
                type: String
            },
            createdBy: {type: ObjectID, ref: 'users'},
            updatedBy: {type: ObjectID, ref: 'users'},
        }
    ]

}, {
    timestamps: true,
    versionKey: false
})

// EmployeeFrontPageSettingsSchema.index({userId: 1, companyId: 1}, {unique: true})
EmployeeFrontPageSettingsSchema.index({companyId: 1}, {unique: true})

let EmployeeFrontPageSettingsModel = mongoose.model('employee_front_page_settings', EmployeeFrontPageSettingsSchema, 'employee_front_page_settings');


module.exports = {
    CompanyAnnouncementsModel,
    EmployeeFrontPageSettingsModel
};