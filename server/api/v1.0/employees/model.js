let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    userId: { type: ObjectID, ref: 'users' },
    name: { firstName: String, lastName: String, middleName: String, prefferedName: String },
    birth: String,
    gender: String,
    maritalStatus: String,
    effectiveDate: String,
    veteranStatus: String,
    ssn: String,
    sin: String,
    nin: String,
    ethnicity: String,
    type: { type: String, enum: Object.keys(constants.userTypes) },
    contact: {
        workPhone: String,
        extention: String,
        mobilePhone: String,
        homePhone: String,
        workMail: String,//{ type: String, required: true, unique: true },
        personalMail: String//{ type: String, required: true, unique: true }
    },
    address: {
        address1: String,
        address2: String,
        city: String,
        state: String,
        zipcode: String,
        country: String
    },
    socialLinks:{ linkedIn : String , twitter : String , facebook : String },
    education : [],
    language : [],
    visaInformation : [] ,
    emergencyContact : [],
    companyId: { type: ObjectID, ref: 'companies' },
    createdBy: { type: ObjectID, ref: 'companies' },
    createdDate: { type: Date, default: Date.now() },
    updatedDate: { type: Date, default: Date.now() },
});
Schema.plugin(autoIncrement.plugin, {model:'employees', field:'employeeId', startAt: 1, incrementBy: 1});

let EmployeesModel = mongoose.model('employees', Schema);

module.exports = EmployeesModel;


