let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let EmployeeController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');


router.post('/add', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, EmployeeController.addNew);

router.put('/', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, EmployeeController.updateSelfData);

router.get('/all', EmployeeController.getAll);

router.put('/:_id/:section', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, authMiddleware.isCompany, EmployeeController.udpateById);

router.post('/deleteEmergencyContactById', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, authMiddleware.isCompany, EmployeeController.deleteEmergencyContactById);

router.post('/addNote', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, authMiddleware.isCompany, EmployeeController.addNote);

router.get('/allNotes/:_id', authMiddleware.isUserLogin, EmployeeController.getAllNotes);

module.exports = router;