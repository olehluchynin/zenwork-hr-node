let mongoose = require('mongoose');
let EmployeeModel = require('./model');
let UserModel = require('./../users/model');
let UserController = require('./../users/controller');
let ObjectID = mongoose.Types.ObjectId;
let NotesModel = require('./notesSchema');


let jwt = require('jsonwebtoken');
let config = require('../../../config/config');

let utils = require('./../../../common/utils');
let helpers = require('./../../../common/helpers');
let constants = require('./../../../common/constants');

let mailer = require('./../../../common/mailer');

let addNew = (req, res) => {
    let body = req.body;
    body.type = "employee";
    body.companyId = req.jwt._id;
    return new Promise((resolve, reject) => {
        if (!body.name.firstName) reject("Invalid name");
        else if (!body.contact.workMail || !helpers.isValidEmail(body.contact.workMail)) reject("Invalid Work email");
        else if (!body.type) reject('Invalid type');
        else resolve(null);
    }).then(() => {
        return UserModel.findOne({ email: body.contact.workMail })
    }).then((user) => {
        if (user) {
            res.status(402).json({ status: false, message: "User already exists", data: null });
            return;
        } else {
            body.createdBy = req.jwt._id;
            body.password = utils.encryptPassword(utils.generateRandomPassword());
            return Promise.all([UserController.addNewEmployee(body), EmployeeModel.create(body)]);
        }
    }).then((data) => {
        console.log("Data", data);
        if (data) {
            var user = data[0];
            var employee = data[1];
            if (employee && user) {
                employee.userId = user._id;
                user.employeeId = employee._id;
                employee.save();
                user.save();
            }
            if (employee) {
                mailer.sendUsernamePasswordToCompany(employee.contact.workMail, employee.name.firstName, employee.contact.workMail, body.password, "employee");
                employee = utils.formatEmployeeData(employee.toJSON());
                res.status(200).json({ status: true, message: "New Employee added successfully", data: employee });
            }
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
};


let getById = (req, res) => {

    let userId = req.params._id;

    return new Promise((resolve, reject) => {
        if (!userId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return CompaniesModel.findOne({ _id: userId });
    }).then(zenworker => {
        if (!zenworker) {
            res.status(400).json({ status: false, message: "User not found", data: null });
            return null;
        } else {
            zenworker = utils.formatZenworkerData(zenworker.toJSON());
            res.status(200).json({ status: true, message: "User details fetched successfully", data: zenworker });
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

let getSelfData = (req, res) => {

    let userId = req.jwt._id;

    return new Promise((resolve, reject) => {
        if (!userId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return CompaniesModel.findOne({ _id: userId });
    }).then(zenworker => {
        if (!zenworker) {
            res.status(400).json({ status: false, message: "User not found", data: null });
            return null;
        } else {
            zenworker = utils.formatZenworkerData(zenworker.toJSON());
            res.status(200).json({ status: true, message: "User details fetched successfully", data: zenworker });
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}


let updateSelfData = (req, res) => {

    let body = req.body;

    let _id = req.jwt._id;

    return new Promise((resolve, reject) => {
        if (!userId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return CompaniesModel.findOne({ _id: userId });
    }).then(company => {
        if (!company) {
            res.status(400).json({ status: false, message: "User not found", data: null });
            return null;
        } else {
            if (body.password) delete body.password;
            body.udpateById = req.jwt._id;
            return CompaniesModel.update({ _id: _id }, body);
        }
    }).then(updated => {
        if (updated) {
            res.status(200).json({ status: true, message: "Updated successfully", data: null });
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}


let udpateById = (req, res) => {

    let body = req.body;

    let _id = req.params._id;
    let section = req.params.section;

    return new Promise((resolve, reject) => {
        if (!_id) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return EmployeeModel.findOne({ _id: _id });
    }).then(employee => {
        if (!employee) {
            res.status(400).json({ status: false, message: "Employee not found", data: null });
            return null;
        } else {
            if (body.password) delete body.password;
            body.udpateById = req.jwt._id;
            delete body._id;
            // console.log(req.jwt);
            if (section == "personal") {
                return EmployeeModel.findOneAndUpdate({ _id: _id }, body, { new: true });
            } else if (section == "emergencyContact") {
                return EmployeeModel.findOneAndUpdate({ _id: _id }, { $set: { 'emergencyContact': body } }, { new: true });
            }
        }
    }).then(updated => {
        if (updated) {
            res.status(200).json({ status: true, message: "Updated successfully", data: updated });
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}


let deleteEmergencyContactById = (req, res) => {
    let body = { workMail: req.body.workEmail };
    let _id = ObjectID(req.body._id);
    return new Promise((resolve, reject) => {
        if (!_id) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return EmployeeModel.findOne({ _id: _id });
    }).then(employee => {
        if (!employee) {
            res.status(400).json({ status: false, message: "Employee not found", data: null });
            return null;
        } else {
            if (body.password) delete body.password;
            body.udpateById = req.jwt._id;
            return EmployeeModel.findOneAndUpdate({ _id: _id }, { $pull: { "emergencyContact": { "$elemMatch": { "workMail": body.workMail } } } });
        }
    }).then(updated => {
        if (updated) {
            res.status(200).json({ status: true, message: "Updated successfully", data: null });
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}



let getAll = (req, res) => {

    let queryParams = req.query;

    var searchQuery = req.query.searchQuery || '';
    var status = req.query.status;
    var role = req.query.type;


    return new Promise((resolve, reject) => {
        // if (!body.name || !body.name.first || !body.name.last) reject("invalid name field");
        // else if (!body.email || !helpers.isValidEmail(body.email)) reject("invalid email");
        // else if (!body.password || !helpers.isValidPasword(body.password)) reject("invalid password");
        // else if(!body.mobile || !helpers.isValidPhoneNumber(body.mobile.number))reject("invalid Phone number");
        // else if (body.role === "admin") reject('User type admin cannot be allowed');
        // else
        resolve(null);
    }).then(() => {
        let options = {};
        if (queryParams && queryParams.requiredFields) {
            queryParams.requiredFields = JSON.parse(queryParams.requiredFields);
            options = queryParams.requiredFields;
            options.createdDate = 1;
            options.updatedDate = 1;
        }

        let sortOptions = {};
        if (queryParams && queryParams.sort) {
            sortOptions = JSON.parse(queryParams.sort);
        }

        let perPage = queryParams.perPage ? parseInt(queryParams.perPage) : 20;
        let pageNumber = queryParams.pageNumber ? parseInt(queryParams.pageNumber) : 1;

        let findQuery;
        if (!findQuery) findQuery = {};
        if (searchQuery) {
            findQuery['$or'] = [{ name: new RegExp(searchQuery, 'i') }, { email: new RegExp(searchQuery, 'i') }];
        }

        /* if(status){
            if(!findQuery)findQuery = {};
            findQuery['status']= status;
        } */

        if (role) {
            findQuery['type'] = role;
        }

        console.log("findQuery", findQuery);
        console.log("sortOptions", sortOptions);
        console.log("options", options);

        var aggregatePipes = [];
        if (findQuery) {
            aggregatePipes.push(
                {
                    $match: findQuery
                }
            );
        }

        var aggregatePipeForCount = [];

        aggregatePipes.forEach(pipe => {
            aggregatePipeForCount.push(pipe);
        });
        aggregatePipeForCount.push(
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 }
                }
            }
        )

        aggregatePipes.push(
            {
                $skip: perPage * (pageNumber - 1)
            }
        );
        aggregatePipes.push(
            {
                $limit: perPage
            },
        );

        if (queryParams.requiredFields) {
            aggregatePipes.push(
                {
                    $project: options
                },
            );
        }
        return Promise.all([
            EmployeeModel.aggregate(aggregatePipes),
            EmployeeModel.aggregate(aggregatePipeForCount),
        ]);
    }).then(([employees, countJSON]) => {
        var count = 0;
        if (countJSON && countJSON.length > 0) count = countJSON[0].count;
        res.status(200).json({ status: true, message: "Employees data Fetched", data: employees, count });
    }).catch((err) => {
        console.log(err);
        if (typeof err == 'object') {
            res.status(500).json({ status: false, message: "Internal server error", data: null });
        } else {
            res.status(400).json({ status: false, message: err, data: null });
        }
    });

}


let addNote = (req, res) => {
    let body = {
        description: req.body.description,
        employeeId: req.body._id,
        createdBy: req.jwt._id,
        updatedBy: req.jwt._id
    };

    NotesModel.create(body)
        .then(
            (note) => {
                res.status(200).json({ status: true, message: "Note Successfully Added", data: note });
            }
        ).catch(
            (err) => {
                console.log(err);
                if (typeof err == 'object') {
                    res.status(500).json({ status: false, message: "Internal server error", data: null });
                } else {
                    res.status(400).json({ status: false, message: err, data: null });
                }
            }
        )
}


let getAllNotes = (req, res) => {
    NotesModel.find({ employeeId: req.params._id })
        .populate('createdBy')
        .then(
            (notes) => {
                res.status(200).json({ status: true, message: "Notes Successfully Fetched", data: notes });
            }
        ).catch(
            (err) => {
                console.log(err);
                if (typeof err == 'object') {
                    res.status(500).json({ status: false, message: "Internal server error", data: null });
                } else {
                    res.status(400).json({ status: false, message: err, data: null });
                }
            }
        )
}


let updateNote = (req, res) => {
    let body = {
        description: req.body.description,
        updatedBy: req.jwt._id
    };

    NotesModel.findOneAndUpdate({ _id: req.body._id }, { $set: body })
        .then(
            (note) => {
                res.status(200).json({ status: true, message: "Note Successfully Updated", data: note });
            }
        ).catch(
            (err) => {
                console.log(err);
                if (typeof err == 'object') {
                    res.status(500).json({ status: false, message: "Internal server error", data: null });
                } else {
                    res.status(400).json({ status: false, message: err, data: null });
                }
            }
        )
}

module.exports = {
    addNew,
    updateSelfData,
    getSelfData,
    udpateById,
    getById,
    getAll,
    deleteEmergencyContactById,
    addNote,
    getAllNotes
}
