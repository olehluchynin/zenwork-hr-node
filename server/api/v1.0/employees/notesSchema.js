let mongoose = require('mongoose');
let ObjectID = mongoose.Schema.ObjectId;
let constants = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    employeeId: { type: ObjectID, ref: 'employees' },
    description : String,
    createdBy: { type: ObjectID, ref: 'companies' },
    createdDate: { type: Date, default: Date.now() },
    updatedDate: { type: Date, default: Date.now() },
});

let NotesModelOld = mongoose.model('notes_old', Schema);

module.exports = NotesModelOld;


