let CouponsModel = require('./model');

let addNew = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        if (!body.name) reject("Invalid name");
        else if (!(body.percentage || body.flatDiscountAmount)) reject("Either percentage or flatDiscountAmount is required");
        // else if (!body.maxAmount) reject("Invalid Maximum Amount");
        else resolve(null);
    }).then(()=>{
        return CouponsModel.findOne({name:body.name})
    }).then((package) => {
        if(package){
            res.status(401).json({status:false,message:"Coupon already exists",data:null});
            return;
        }else{
            body.createdBy = req.jwt.zenworkerId;
            return CouponsModel.create(body);
        }
    }).then((package)=>{
        if(package){
            res.status(200).json({status:true,message:"New Coupon added successfully",data:package});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
};


let getById = (req, res) => {

    let packageId = req.params._id;

    return new Promise((resolve, reject) => {
        if (!packageId) reject("Invalid Coupon Id");
        else resolve(null);
    }).then(() => {
        return CouponsModel.findOne({_id:packageId});
    }).then(package=>{
        if(!package){
            res.status(400).json({ status: false, message: "Data not found", data: null });
            return null;
        }else{
            res.status(200).json({status:true,message:"User details fetched successfully",data:package});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}


let udpateById = (req, res) => {

    let body = req.body;

    let couponId = req.params._id;

    return new Promise((resolve, reject) => {
        if (!couponId) reject("Invalid Coupon Id");
        else resolve(null);
    }).then(() => {
        return CouponsModel.findOne({_id:couponId});
    }).then(coupon=>{
        if(!coupon){
            res.status(400).json({ status: false, message: "Coupon not found", data: null });
            return null;
        }else{
            body.udpateById  = req.jwt.zenworkerId;
            body.updatedDate = new Date();
            return CouponsModel.update({_id:couponId},body); 
        }
    }).then(updated=>{
        if(updated){
            res.status(200).json({status:true,message:"Coupon Updated successfully",data:null});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

let getAll = (req,res)=>{
    if (req.route.methods.get) {
        return new Promise((resolve, reject) => {
            resolve(null);
        }).then(() => {
            return  CouponsModel.find({});
        }).then(coupons=>{
            res.status(200).json({ status: true, message: "Data Fetched", data: coupons});
        }).catch((err) => {
            console.log(err);
            if(typeof err =='object'){
                res.status(500).json({ status: false, message: "Internal server error", data: null });
            }else{
                res.status(400).json({ status: false, message: err, data: null });
            }
        });
    }
    else if (req.route.methods.post) {
        let body = req.body
        return new Promise((resolve, reject) => {
            resolve(null);
        }).then(() => {
            let per_page = body.per_page ? body.per_page : 10
            let page_no = body.page_no ? body.page_no : 1
            let sort = {
                updatedAt : -1
            }
            return  Promise.all([
                CouponsModel.find({}).sort(sort).skip((page_no - 1) * per_page).limit(per_page).lean().exec(),
                CouponsModel.countDocuments({})
                ]);
        }).then(([coupons, count])=>{
            res.status(200).json({ status: true, message: "Data Fetched", data: coupons, count: count});
        }).catch((err) => {
            console.log(err);
            if(typeof err =='object'){
                res.status(500).json({ status: false, message: "Internal server error", data: null });
            }else{
                res.status(400).json({ status: false, message: err, data: null });
            }
        });
    }
    

}

let validateCoupon = (req, res) => {
    let body = req.body

    return new Promise((resolve, reject) => {
        if (!body.coupon_name) reject('coupon_name is required')
        resolve(null);
    })
    .then(() => {
        CouponsModel.findOne({name: body.coupon_name})
            .then(coupon => {
                if (coupon) {
                    if (coupon.startDate.getTime() < new Date().getTime() && coupon.endDate.getTime() > new Date().getTime()) {
                        res.status(200).json({
                            status: true,
                            message: 'Coupon applied successfully',
                            data: coupon
                        })
                    }
                    else if (coupon.startDate.getTime() > new Date().getTime()){
                        res.status(200).json({
                            status: false,
                            message: 'This coupon is not applicable at the moment',
                            data: null
                        })
                    }
                    else if (new Date().getTime() > coupon.endDate.getTime()) {
                        res.status(200).json({
                            status: false,
                            message: 'This coupon has expired',
                            data: null
                        })
                    }
                }
                else {
                    res.status(200).json({
                        status: false,
                        message: 'Invalid coupon',
                        data: null
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: 'Error while fetching coupon',
                    error: err
                })
            });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let deleteCoupons = (req, res) => {
    let body = req.body
    return new Promise((resolve, reject) => {
        if(!body._ids) reject('_ids are required')
        resolve(null);
    })
    .then(() => {
        CouponsModel.deleteMany({
            _id: {$in : body._ids}
        })
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted coupons successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting coupons",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while deleting coupons",
            error: err
        })
    })
}

module.exports = {
    addNew,
    getById,
    udpateById,
    getAll,

    validateCoupon,
    deleteCoupons
}
