let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let CouponController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');


router.post('/add',authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, CouponController.addNew);
router.get('/all', authMiddleware.isUserLogin, CouponController.getAll); // without pagination
router.post('/all', authMiddleware.isUserLogin, CouponController.getAll); // with pagination

router.put(
    '/:_id', 
    authMiddleware.isUserLogin, 
    authMiddleware.isZenworkerLogin, 
    validationsMiddleware.reqiuresBody, 
    // authMiddleware.isSrSupport, 
    authMiddleware.isSrSupportOrZenworkerOrAdministrator,
    CouponController.udpateById
);

router.get(
    '/:_id', 
    authMiddleware.isUserLogin, 
    // authMiddleware.isSrSupport, 
    authMiddleware.isSrSupportOrZenworkerOrAdministrator,
    CouponController.getById
);

router.post(
    '/validate-coupon',
    authMiddleware.isUserLogin,
    CouponController.validateCoupon
);

router.post(
    '/delete',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    CouponController.deleteCoupons
);

module.exports = router;