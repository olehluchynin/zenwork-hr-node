/**
 * @apiDefine DataNotFoundError
 *
 * @apiError DataNotFound The data the you are requested was not found.
 *
 * @apiErrorExample Data Not Found Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "message": "Data Not Found"
 *       "data": null 
 *     }
 */


 /**
 * @apiDefine InternalServerError
 *
 * @apiError InternalServerError Internal Server error.
 *
 * @apiErrorExample Internal Server Error:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "status": false,
 *       "message": "Internal server error"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine TokenNotFound
 *
 * @apiError TokenNotFound User Token Not Found.
 *
 * @apiErrorExample Token Not Found:
 *     HTTP/1.1 400 Token Not Found
 *     {
 *       "status": false,
 *       "message": "Token Not Found"
 *       "data": null 
 *     }
 */

  /**
 * @apiDefine BadRequest
 *
 * @apiError BadRequest Bad request.
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": false,
 *       "message": "Invalid key"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine AccessDeneid
 *
 * @apiError AccessDenied Access denied for this operation.
 *
 * @apiErrorExample Access Deneid:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "status": false,
 *       "message": "Access Denied for this operation"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine DataAlreadyExists
 *
 * @apiError DataConflict Date already exists.
 *
 * @apiErrorExample Data Conflict Error:
 *     HTTP/1.1 402 Data Conflict
 *     {
 *       "status": false,
 *       "message": "Data already exist"
 *       "data": null 
 *     }
 */

  /**
 * @api {post} /coupons/all ListCoupons
 * @apiName ListCoupons
 * @apiGroup Coupons
  * @apiHeader {String} x-access-token User login token.
 * @apiParamExample {json} Request-Example:
{
	"per_page": 10,
	"page_no": 1
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Data Fetched",
    "data": [
        {
            "_id": "5c895fd52e43cc36dc1d21f6",
            "createdDate": "2019-02-28T06:43:06.581Z",
            "updatedDate": "2019-05-28T08:53:07.506Z",
            "name": "zensmash",
            "startDate": "2019-03-26T18:30:00.000Z",
            "endDate": "2019-07-18T18:30:00.000Z",
            "percentage": 10,
            "maxAmount": -10,
            "createdBy": "5c6ba80fe342799adf1b177c",
            "__v": 0
        },
        {
            "_id": "5c9e6a032bdee465121c8103",
            "createdDate": "2019-03-29T12:55:37.258Z",
            "updatedDate": "2019-03-29T12:55:37.258Z",
            "name": "Zensmash",
            "startDate": "2019-03-29T18:54:10.031Z",
            "endDate": "2019-04-14T18:30:00.000Z",
            "percentage": 20,
            "maxAmount": 20,
            "createdBy": "5c6ba80fe342799adf1b177c",
            "couponsId": 1,
            "__v": 0
        },
        {
            "_id": "5ca264312bdee465121c8134",
            "createdDate": "2019-03-29T12:55:37.258Z",
            "updatedDate": "2019-05-28T07:02:20.170Z",
            "name": "bondo Discount",
            "startDate": "2019-04-01T19:18:46.093Z",
            "endDate": "2019-03-31T18:30:00.000Z",
            "percentage": 14,
            "maxAmount": 50,
            "createdBy": "5c6ba80fe342799adf1b177c",
            "couponsId": 2,
            "__v": 0
        },
        {
            "_id": "5ccacddf172e1531c7f05b36",
            "createdDate": "2019-05-02T06:06:59.898Z",
            "updatedDate": "2019-05-30T08:50:42.266Z",
            "name": "as",
            "startDate": "2019-05-02T10:52:35.466Z",
            "endDate": "2019-05-02T10:52:35.466Z",
            "percentage": 1,
            "maxAmount": 1,
            "createdBy": "5c6ba80fe342799adf1b177c",
            "couponsId": 3,
            "__v": 0
        },
        {
            "_id": "5ce779ca934708342c6ab0dd",
            "createdDate": "2019-05-24T03:40:02.715Z",
            "updatedDate": "2019-05-24T03:40:02.715Z",
            "name": "coupon",
            "startDate": "2019-05-24T04:50:54.783Z",
            "endDate": "2019-05-14T18:30:00.000Z",
            "percentage": 10,
            "maxAmount": 100,
            "createdBy": "5c6ba80fe342799adf1b177c",
            "couponsId": 4,
            "__v": 0
        },
        {
            "_id": "5ce77bea934708342c6ab0de",
            "createdDate": "2019-05-24T03:40:02.715Z",
            "updatedDate": "2019-05-24T03:40:02.715Z",
            "name": "New",
            "startDate": "2019-05-24T05:04:23.982Z",
            "endDate": "2019-05-29T18:30:00.000Z",
            "percentage": 10,
            "maxAmount": 25,
            "createdBy": "5c6ba80fe342799adf1b177c",
            "couponsId": 5,
            "__v": 0
        },
        {
            "_id": "5cecefa30ef791519fcd0123",
            "createdDate": "2019-05-27T12:35:11.271Z",
            "updatedDate": "2019-05-27T12:35:11.271Z",
            "name": "kjok",
            "startDate": "2019-05-28T08:21:29.578Z",
            "endDate": "2019-05-28T08:21:29.578Z",
            "percentage": 0,
            "maxAmount": 9,
            "createdBy": "5c6ba80fe342799adf1b177c",
            "couponsId": 6,
            "__v": 0
        },
        {
            "_id": "5cecefcd0ef791519fcd0124",
            "createdDate": "2019-05-27T12:35:11.271Z",
            "updatedDate": "2019-05-27T12:35:11.271Z",
            "name": " vbhvb",
            "startDate": "2019-05-28T08:22:30.379Z",
            "endDate": "2019-05-28T08:22:30.379Z",
            "percentage": 8,
            "maxAmount": 88,
            "createdBy": "5c6ba80fe342799adf1b177c",
            "couponsId": 7,
            "__v": 0
        }
    ],
    "count": 8
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /coupons/validate-coupon ValidateCoupon
 * @apiName ValidateCoupon
 * @apiGroup Coupons
  * @apiHeader {String} x-access-token User login token.
 * @apiParamExample {json} Request-Example:
{
	"coupon_name": "zen100"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Valid coupon",
    "data": {
        "_id": "5cf0ede0daea4a701cbbc345",
        "createdDate": "2019-05-31T08:45:47.584Z",
        "updatedDate": "2019-05-31T08:45:47.584Z",
        "name": "zen100",
        "startDate": "2019-04-30T18:30:00.000Z",
        "endDate": "2019-06-29T18:30:00.000Z",
        "percentage": null,
        "flatDiscountAmount": 100,
        "createdBy": "5c6ba80fe342799adf1b177c",
        "couponsId": 11,
        "__v": 0
    }
}
 * @apiErrorExample ErrorResponse:
{
    "status": false,
    "message": "Invalid coupon",
    "data": null
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /coupons/delete DeleteCoupons
 * @apiName DeleteCoupons
 * @apiGroup Coupons
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"_ids": [
		"5ca26ce12bdee465121c8136"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted coupons successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */