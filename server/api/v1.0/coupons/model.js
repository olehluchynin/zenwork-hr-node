let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    name:{type:String,unique:true,required:true},
    percentage:Number,
    maxAmount : Number,
    flatDiscountAmount: Number,
    couponType: {
        type: String,
        enum: ['Percentage', 'Value']
    },
    startDate : Date,
    endDate : Date,
    createdBy:{type:ObjectID,ref:'zenworkers'},
    updatedBy:{type:ObjectID,ref:'zenworkers'},
},{
    timestamps: true,
    versionKey: false
});
Schema.plugin(autoIncrement.plugin, {model:'coupons', field:'couponsId', startAt: 1, incrementBy: 1});
// Schema.index({loc: '2dsphere'}); //loc: { type: "Point", coordinates: [ longitude, latitude ] },

let CouponsModel = mongoose.model('coupons',Schema);

module.exports = CouponsModel;
