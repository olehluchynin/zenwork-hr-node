let AdonsModel = require('./model');

let addNew = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        if (!body.name) reject("Invalid name");
        else if (!body.price) reject("Invalid price");
        else resolve(null);
    }).then(()=>{
        return AdonsModel.findOne({name:body.name})
    }).then((adon) => {
        if(adon){
            res.status(401).json({status:false,message:"Adon already exists",data:null});
            return;
        }else{
            body.createdBy = req.jwt.zenworkerId;
            return AdonsModel.create(body);
        }
    }).then((adon)=>{
        if(adon){
            res.status(200).json({status:true,message:"New adon added successfully",data:adon});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
};


let getById = (req, res) => {

    let adonId = req.params._id;

    return new Promise((resolve, reject) => {
        if (!adonId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return AdonsModel.findOne({_id:adonId});
    }).then(adon=>{
        if(!adon){
            res.status(400).json({ status: false, message: "Data not found", data: null });
            return null;
        }else{
            res.status(200).json({status:true,message:"User details fetched successfully",data:adon});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}


let udpateById = (req, res) => {

    let body = req.body;

    let adonId = req.params._id;

    return new Promise((resolve, reject) => {
        if (!adonId) reject("Invalid pacakgeId");
        else resolve(null);
    }).then(() => {
        return AdonsModel.findOne({_id:adonId});
    }).then(adon=>{
        if(!adon){
            res.status(400).json({ status: false, message: "Data not found", data: null });
            return null;
        }else{
            body.udpateById  = req.jwt.zenworkerId;
            body.updatedDate = new Date();
            return AdonsModel.update({_id:adonId},body); 
        }
    }).then(updated=>{
        if(updated){
            res.status(200).json({status:true,message:"Updated successfully",data:null});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

let getAll = (req,res)=>{
    if (req.route.methods.get) {
        return new Promise((resolve, reject) => {
            resolve(null);
        }).then(() => {
            return  AdonsModel.find({});
        }).then(adons=>{
            res.status(200).json({ status: true, message: "Data Fetched", data: adons});
        }).catch((err) => {
            console.log(err);
            if(typeof err =='object'){
                res.status(500).json({ status: false, message: "Internal server error", data: null });
            }else{
                res.status(400).json({ status: false, message: err, data: null });
            }
        });
    }
    else if (req.route.methods.post){
        let body = req.body
        return new Promise((resolve, reject) => {
            resolve(null);
        }).then(() => {
            let per_page = body.per_page ? body.per_page : 10
            let page_no = body.page_no ? body.page_no : 1
            let sort = {
                updatedAt : -1
            }
            return  Promise.all([
                AdonsModel.find({}).sort(sort).skip((page_no - 1) * per_page).limit(per_page).lean().exec(),
                AdonsModel.countDocuments({})
                ]);
        }).then(([adons, count])=>{
            res.status(200).json({ status: true, message: "Data Fetched", data: adons, count: count});
        }).catch((err) => {
            console.log(err);
            if(typeof err =='object'){
                res.status(500).json({ status: false, message: "Internal server error", data: null });
            }else{
                res.status(400).json({ status: false, message: err, data: null });
            }
        });
    }
    

}

let deleteAddOns = (req, res) => {
    let body = req.body
    return new Promise((resolve, reject) => {
        if(!body._ids) reject('_ids are required')
        resolve(null);
    })
    .then(() => {
        AdonsModel.deleteMany({
            _id: {$in : body._ids}
        })
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted add-ons successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting add-ons",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while deleting add-ons",
            error: err
        })
    })
}


module.exports = {
    addNew,
    getById,
    udpateById,
    getAll,
    deleteAddOns
}
