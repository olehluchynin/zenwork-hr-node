/**
 * @apiDefine DataNotFoundError
 *
 * @apiError DataNotFound The data the you are requested was not found.
 *
 * @apiErrorExample Data Not Found Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "message": "Data Not Found"
 *       "data": null 
 *     }
 */


 /**
 * @apiDefine InternalServerError
 *
 * @apiError InternalServerError Internal Server error.
 *
 * @apiErrorExample Internal Server Error:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "status": false,
 *       "message": "Internal server error"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine TokenNotFound
 *
 * @apiError TokenNotFound User Token Not Found.
 *
 * @apiErrorExample Token Not Found:
 *     HTTP/1.1 400 Token Not Found
 *     {
 *       "status": false,
 *       "message": "Token Not Found"
 *       "data": null 
 *     }
 */

  /**
 * @apiDefine BadRequest
 *
 * @apiError BadRequest Bad request.
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": false,
 *       "message": "Invalid key"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine AccessDeneid
 *
 * @apiError AccessDenied Access denied for this operation.
 *
 * @apiErrorExample Access Deneid:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "status": false,
 *       "message": "Access Denied for this operation"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine DataAlreadyExists
 *
 * @apiError DataConflict Date already exists.
 *
 * @apiErrorExample Data Conflict Error:
 *     HTTP/1.1 402 Data Conflict
 *     {
 *       "status": false,
 *       "message": "Data already exist"
 *       "data": null 
 *     }
 */



  /**
 * @api {post} /ad-ons/add Add new
 * @apiName AddNewAdons
 * @apiGroup Ad-ons
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
 *
 {
    "name":"test",
    "price":2000
 }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "New ad-on added successfully",
    "data": {
        "status": "active",
        "createdDate": "2019-01-02T09:11:10.383Z",
        "updatedDate": "2019-01-02T09:11:10.383Z",
        "_id": "5c2c80593112f91c21db2077",
        "name": "test",
        "price": 2000,
        "createdBy": "5c126409d84721019cf167be",
        "__v": 0
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */



   /**
 * @api {put} /ad-ons/:_id Update ad-on
 * @apiName UpdateAdon
 * @apiGroup  Ad-ons
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
 *
 {
    "name":"test",
    "price":2000
 }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Ad-on updated",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */



 /**
 * @api {get} /ad-ons/:_id Get ad-on Info
 * @apiName GetAdOnsData
 * @apiGroup  Ad-ons
  * @apiHeader {String} x-access-token User login token.

 * @apiParam {String} _id package unique _id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Ad-on details fetched successfully",
   "data": {
        "status": "active",
        "createdDate": "2019-01-02T09:11:10.383Z",
        "updatedDate": "2019-01-02T09:11:10.383Z",
        "_id": "5c2c80593112f91c21db2077",
        "name": "test",
        "price": 2000,
        "createdBy": "5c126409d84721019cf167be",
        "__v": 0
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

  /**
 * @api {get} /ad-ons/all Get All add-ons
 * @apiName GetAllAdd-onsData
 * @apiGroup  Ad-ons
  * @apiHeader {String} x-access-token User login token.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Data Fetched",
    "data": [
        {
            "status": "active",
            "createdDate": "2019-01-02T09:11:10.383Z",
            "updatedDate": "2019-01-02T09:11:10.383Z",
            "_id": "5c2c804e3112f91c21db2076",
            "name": "Basic",
            "price": 1000,
            "createdBy": "5c126409d84721019cf167be",
            "__v": 0
        },
        {
            "status": "active",
            "createdDate": "2019-01-02T09:11:10.383Z",
            "updatedDate": "2019-01-02T09:11:10.383Z",
            "_id": "5c2c80593112f91c21db2077",
            "name": "Premius",
            "price": 2000,
            "createdBy": "5c126409d84721019cf167be",
            "__v": 0
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

  /**
 * @api {post} /ad-ons/all ListAdd-Ons
 * @apiName ListAdd-Ons
 * @apiGroup  Ad-ons
  * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
{
	"per_page": 10,
	"page_no": 1
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Data Fetched",
    "data": [
        {
            "_id": "5c73c118a81a0215bb704c5a",
            "status": "active",
            "createdDate": "2019-02-19T07:47:28.553Z",
            "updatedDate": "2019-03-26T09:09:49.528Z",
            "name": "Bio metric",
            "price": 200,
            "subscriptionType": "monthly",
            "startDate": "2019-02-25T10:18:33.636Z",
            "endDate": "2019-12-19T18:30:00.000Z",
            "createdBy": "5c73c074a81a0215bb704c59",
            "__v": 0
        },
        {
            "_id": "5c754a4ea81a0215bb704c7f",
            "status": "active",
            "createdDate": "2019-02-19T07:47:28.553Z",
            "updatedDate": "2019-02-19T07:47:28.553Z",
            "name": "Remote work tracker",
            "price": 2000,
            "subscriptionType": "monthly",
            "startDate": "2019-02-26T14:16:17.582Z",
            "endDate": "2020-02-25T18:30:00.000Z",
            "createdBy": "5c6ba80fe342799adf1b177c",
            "__v": 0
        },
        {
            "_id": "5c8960302e43cc36dc1d21f8",
            "status": "active",
            "createdDate": "2019-02-28T06:43:06.579Z",
            "updatedDate": "2019-02-28T06:43:06.579Z",
            "name": "ACA Compliance",
            "price": 1234,
            "subscriptionType": "yearly",
            "startDate": "2019-03-13T19:55:03.938Z",
            "endDate": "2019-03-26T05:00:00.000Z",
            "createdBy": "5c6ba80fe342799adf1b177c",
            "__v": 0
        },
        {
            "_id": "5ca26ce12bdee465121c8136",
            "status": "inactive",
            "createdDate": "2019-03-29T12:55:37.256Z",
            "updatedDate": "2019-04-01T19:56:26.771Z",
            "name": "asdf",
            "price": 1,
            "subscriptionType": "yearly",
            "startDate": "2019-04-01T19:56:07.477Z",
            "endDate": "2019-04-01T19:56:07.477Z",
            "createdBy": "5c6ba80fe342799adf1b177c",
            "ad_onId": 1,
            "__v": 0
        },
        {
            "_id": "5cd020cf0606e63cd0b385ba",
            "status": "active",
            "createdDate": "2019-05-03T05:44:23.957Z",
            "updatedDate": "2019-05-24T06:57:34.322Z",
            "name": "special addon",
            "price": 10,
            "subscriptionType": "monthly",
            "startDate": "2019-05-06T11:50:23.138Z",
            "endDate": "2019-05-30T18:30:00.000Z",
            "createdBy": "5c6ba80fe342799adf1b177c",
            "ad_onId": 2,
            "__v": 0
        },
        {
            "_id": "5ce77f204a8b1d3470f58a67",
            "status": "active",
            "createdDate": "2019-05-24T05:12:05.496Z",
            "updatedDate": "2019-05-24T05:12:05.496Z",
            "name": "NEW",
            "price": 150,
            "subscriptionType": "yearly",
            "startDate": "2019-05-24T05:19:49.925Z",
            "endDate": "2019-05-29T18:30:00.000Z",
            "createdBy": "5c6ba80fe342799adf1b177c",
            "ad_onId": 3,
            "__v": 0
        }
    ],
    "count": 6
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /ad-ons/delete DeleteAd-ons
 * @apiName DeleteAd-ons
 * @apiGroup Ad-ons
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"_ids": [
		"5ca26ce12bdee465121c8136"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted add-ons successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */