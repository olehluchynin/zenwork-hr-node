let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let AdonsController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');


router.post('/add',authMiddleware.isUserLogin,validationsMiddleware.reqiuresBody,AdonsController.addNew);

router.get('/all'/*,authMiddleware.isUserLogin*/,AdonsController.getAll); // without pagination
router.post('/all'/*,authMiddleware.isUserLogin*/,AdonsController.getAll); // with pagination

router.put('/:_id',authMiddleware.isUserLogin,authMiddleware.isZenworkerLogin,validationsMiddleware.reqiuresBody,/*authMiddleware.isSrSupport,*/ authMiddleware.isSrSupportOrZenworkerOrAdministrator,AdonsController.udpateById);

router.get(
    '/:_id',
    authMiddleware.isUserLogin,
    // authMiddleware.isSrSupport,
    authMiddleware.isSrSupportOrZenworkerOrAdministrator,
    AdonsController.getById
);

router.post(
    '/delete',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    AdonsController.deleteAddOns
);


module.exports = router;
