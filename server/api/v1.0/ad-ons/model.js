const mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
const ObjectID = mongoose.Schema.ObjectId;
const constants  = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    name:{type:String,unique:true,required:true},
    price:Number,//In USDs
    subscriptionType : {type:String},
    startDate : String,
    endDate : String,
    status:{type:String,enum:['active','inactive'],default:'active'},
    createdBy:{type:ObjectID,ref:'zenworkers'},
    updatedBy:{type:ObjectID,ref:'zenworkers'},
    createdDate:{type:Date, default:Date.now()},
    updatedDate:{type:Date,default:Date.now()},
});
Schema.plugin(autoIncrement.plugin, {model:'ad_ons', field:'ad_onId', startAt: 1, incrementBy: 1});

// Schema.index({loc: '2dsphere'}); //loc: { type: "Point", coordinates: [ longitude, latitude ] },

let AdonsModel = mongoose.model('ad_ons',Schema);

module.exports = AdonsModel;
