let pages = [
    {
        name: "Dashboard - ESS",
        employee: "full",
        manager: "full",
        hr: "full",
        sysadmin: "full"
    },
    {
        name: "Dashboard - MSS",
        employee: "no",
        manager: "full",
        hr: "full",
        sysadmin: "view"
    },
    {
        name: "My Info (EE's) - Personal",
        employee: "view",
        manager: "view",
        hr: "full",
        sysadmin: "view"
    },
    {
        name: "My Info (EE's) - Job",
        employee: "view",
        manager: "view",
        hr: "full",
        sysadmin: "view"
    },
    {
        name: "My Info (EE's) - Time Schedule",
        employee: "view",
        manager: "view",
        hr: "full",
        sysadmin: "view"
    },
    {
        name: "My Info (EE's) - Emergency Contact",
        employee: "view",
        manager: "view",
        hr: "full",
        sysadmin: "view"
    },
    {
        name: "My Info (EE's) - Compensation",
        employee: "view",
        manager: "view",
        hr: "full",
        sysadmin: "view"
    },
    {
        name: "My Info (EE's) - Notes",
        employee: "no",
        manager: "no",
        hr: "full",
        sysadmin: "no"
    },
    {
        name: "My Info (EE's) - Benifits",
        employee: "view",
        manager: "view",
        hr: "full",
        sysadmin: "view"
    },
    {
        name: "My Info (EE's) - Training",
        employee: "view",
        manager: "view",
        hr: "full",
        sysadmin: "view"
    },
    {
        name: "My Info (EE's) - Documents",
        employee: "view",
        manager: "view",
        hr: "full",
        sysadmin: "view"
    },
    {
        name: "My Info (EE's) - Assets",
        employee: "view",
        manager: "view",
        hr: "full",
        sysadmin: "view"
    },
    {
        name: "My Info (EE's) - On-Boarding",
        employee: "no",
        manager: "no",
        hr: "full",
        sysadmin: "no"
    },
    {
        name: "My Info (EE's) - Off-Boarding",
        employee: "no",
        manager: "no",
        hr: "full",
        sysadmin: "no"
    },
    {
        name: "My Info (EE's) - Custom",
        employee: "view",
        manager: "view",
        hr: "full",
        sysadmin: "view"
    },
    {
        name: "My Info (EE's) - Audit Trial",
        employee: "no",
        manager: "no",
        hr: "full",
        sysadmin: "no"
    }
]

module.exports = {
    pages
}