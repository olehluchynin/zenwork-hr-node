let mongoose = require('mongoose')
    // autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    name: {type: String, required: true},
    employee:{type:String,enum: Object.keys(constants.roleAccess)},
    manager: {type:String,enum: Object.keys(constants.roleAccess)},
    hr: {type:String,enum: Object.keys(constants.roleAccess)},
    sysadmin: {type:String,enum: Object.keys(constants.roleAccess)},
    // status:{type:String,enum: Object.keys(constants.activeStatus),default:1},
    createdBy:{type:ObjectID,ref:'users'},
    updatedBy:{type:ObjectID,ref:'users'},
    companyId: {type: ObjectID, ref: 'companies', required: true},
    roleId: {type: ObjectID, ref: 'roles'},
    pageType: {type: String, enum: Object.keys(constants.pageType), default: 'base'}
},{
    timestamps: true,
    versionKey: false
});

// Schema.plugin(autoIncrement.plugin, {model:'pages', field:'pageid', startAt: 1, incrementBy: 1});

let PagesModel = mongoose.model('pages',Schema);

PagesModel.bulkUpdateRecords = (docsList) => {
    return new Promise((resolve, reject) => {
        let promise_array = [];
        docsList.forEach((doc) => {
            promise_array.push(new Promise((resolve, reject) => {
                PagesModel.update({_id: doc._id}, doc).then((result) => {
                    resolve(result)
                }).catch((error) => {
                    reject(error)
                })
            }));
        });
        Promise.all(promise_array).then((result) => {
            resolve(result)
        }).catch((error) => {
            reject(error);
        });
    });
}

module.exports = PagesModel;
