/**
 * @api {get} /pages/all/:company_id GetPageAclsForStandardRoles
 * @apiName GetPageAclsForStandardRoles
 * @apiGroup Pages
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Getting ACL success",
    "data": [
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3ed",
            "name": "Dashboard - ESS",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.177Z",
            "updatedAt": "2019-09-06T09:09:22.177Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3ee",
            "name": "Dashboard - MSS",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.177Z",
            "updatedAt": "2019-09-06T09:09:22.178Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3ef",
            "name": "My Info (EE's) - Personal",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.178Z",
            "updatedAt": "2019-09-06T09:09:22.178Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3f0",
            "name": "My Info (EE's) - Job",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.178Z",
            "updatedAt": "2019-09-06T09:09:22.178Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3f1",
            "name": "My Info (EE's) - Time Schedule",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.178Z",
            "updatedAt": "2019-09-06T09:09:22.178Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3f2",
            "name": "My Info (EE's) - Emergency Contact",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.178Z",
            "updatedAt": "2019-09-06T09:09:22.178Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3f3",
            "name": "My Info (EE's) - Compensation",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.179Z",
            "updatedAt": "2019-09-06T09:09:22.179Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3f4",
            "name": "My Info (EE's) - Notes",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.179Z",
            "updatedAt": "2019-09-06T09:09:22.179Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3f5",
            "name": "My Info (EE's) - Benifits",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.179Z",
            "updatedAt": "2019-09-06T09:09:22.179Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3f6",
            "name": "My Info (EE's) - Training",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.179Z",
            "updatedAt": "2019-09-06T09:09:22.179Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3f7",
            "name": "My Info (EE's) - Documents",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.179Z",
            "updatedAt": "2019-09-06T09:09:22.179Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3f8",
            "name": "My Info (EE's) - Assets",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.180Z",
            "updatedAt": "2019-09-06T09:09:22.180Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3f9",
            "name": "My Info (EE's) - On-Boarding",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.180Z",
            "updatedAt": "2019-09-06T09:09:22.180Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3fa",
            "name": "My Info (EE's) - Off-Boarding",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.180Z",
            "updatedAt": "2019-09-06T09:09:22.180Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3fb",
            "name": "My Info (EE's) - Custom",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.180Z",
            "updatedAt": "2019-09-06T09:09:22.180Z"
        },
        {
            "employee": "no",
            "manager": "no",
            "hr": "no",
            "sysadmin": "no",
            "pageType": "base",
            "_id": "5d7222426ffa610e6ce5c3fc",
            "name": "My Info (EE's) - Audit Trial",
            "companyId": "5d7222416ffa610e6ce5c3dc",
            "createdBy": "5d7222416ffa610e6ce5c3dd",
            "updatedBy": "5d7222416ffa610e6ce5c3dd",
            "createdAt": "2019-09-06T09:09:22.180Z",
            "updatedAt": "2019-09-06T09:09:22.180Z"
        }
    ]
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */