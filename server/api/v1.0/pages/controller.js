let mongoose = require('mongoose');
let ObjectID = require('mongodb').ObjectID;
let PagesModel = require('./model');

/**
 * This method is used to create a custom page for custom role
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let createCustomRoleACL = (req, res) => {
    let req_data = req.body;

    if(req_data.company_id == undefined || req_data.company_id == ""
        || req_data.role_id == undefined || req_data.role_id == "") {
            res.status(400).json({
                status: false,
                message: 'Page creation for custom role failed',
                error: []
            })
        } else {
            let promise_array = []
            req_data.forEach(page => {
                page.roleId = req_data.role_id;
                page.companyId = req_data.company_id;
                promise_array.push(
                    new Promise((resolve, reject) => {
                        PagesModel.create(page).then((result) => {
                            resolve(result)
                        }).catch((error) => {
                            reject(error)
                        })
                    })
                )
            });
            Promise.all(promise_array).then((result) => {
                res.staus(200).json({
                    status: true,
                    message: 'pages creation for custom role success',
                    data: (result) ? result : []
                })
            }).catch((error) => {
                res.staus(400).json({
                    status: true,
                    message: 'pages creation for custom role failed',
                    error: error
                })
            })
        }
}

/**
 * This method is used to create new ACL
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let createNewPageACL = (req, res) => {
    let acl_body = req.body;

    return new Promise((resolve, reject) => {
        PagesModel.create(acl_body).then((result) => {
            resolve(result);
        }).catch((error) => {
            reject(error);
        })
    }).then((result) => {
        res.status(200).json({
            status: true,
            message: "ACL creation success",
            data: (result) ? result : []
        })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "ACL creation failed",
            error: error
        })
    })
}

/**
 * This method is used to insert bulk records at once
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let createMultiple = (req, res) => {
    let company_id = req.body.company_id,
        role_id = req.body.role_id,
        page_type = req.body.page_type,
        pages = req.body.pages,
        updated_pages = [];

    if(company_id == undefined || company_id == "" ||
        role_id == undefined || role_id == "" ||
        page_type == undefined || page_type == "") {
            res.status(400).json({
                status: false,
                message: "Invalid data found",
                error: []
            })
    } else {
        pages.forEach(page => {
            page["companyId"] = company_id;
            page["roleId"] = role_id;
            page["pageType"] = page_type;
            delete page._id;
            delete page.pageid;
            delete page.createdAt;
            delete page.updatedAt;
            updated_pages.push(page);
        });
    
        return new Promise((resolve, reject) => {
            PagesModel.create(updated_pages).then((result) => {
                resolve(result);
            }).catch((error) => {
                reject(error);
            })
        }).then((result) => {
            res.status(200).json({
                status: true,
                message: "ACL creation success",
                data: (result) ? result : []
            })
        }).catch((error) => {
            res.status(400).json({
                status: false,
                message: "ACL creation failed",
                error: error
            })
        })
    }
}

/**
 * This method is used to update bulk records at once
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let updateMultiple = (req, res) => {
    let acl_body = req.body;

    return new Promise((resolve, reject) => {
        PagesModel.bulkUpdateRecords(acl_body).then((result) => {
            resolve(result)
        }).catch((error) => {
            reject(error);
        });
    }).then((result) => {
        res.status(200).json({
            status: true,
            message: "ACL Update success",
            data: (result) ? result : []
        })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "ACL update failed",
            error: error
        })
    })
}

/**
 * This method is used to get all page ACLs
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

/**
*@author Asma
*@date 06/09/2019 15:25
Method to get ACLs for base roles for each company
*/
let getAllCompanyPageACLS = (req, res) => {
    let companyId = req.params.company_id
    return new Promise((resolve, reject) => {
        PagesModel.find({pageType: 'base', companyId: companyId}).then((acls) => {
            resolve(acls);
        }).catch((error) => {
            reject(error);
        })
    }).then((result) => {
        res.status(200).json({
            status: true,
            message: "Getting ACL success",
            data: (result) ? result : []
        })
    }).catch((error) => {
        res.status(400).json({
            status: true,
            message: "Getting ACL failed",
            error: error
        })
    });
}

/**
 * This method is used to delete ACL by id
 * 
 * @param {*} req 
 * @param {*} res 
 */

let deleteACL = (req, res) => {
    let acl_id = req.params.acl_id;

    return new Promise((resolve, reject) => {
        PagesModel.remove({_id : new ObjectID(acl_id)}).then((result) => {
            resolve(result)
        }).catch((error) => {
            reject(error);
        })
    }).then((result) => {
        res.status(200).json({
            status: true,
            message: "ACL Deletion Success",
            data: (result) ? result : []
        })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "ACL Deletion Failed",
            error: error
        })
    })
}

/**
 * This method is used to get ACL by ACL id
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getACLById = (req, res) => {
    let acl_id = req.params.acl_id;

    return new Promise((resolve, reject) => {
        PagesModel.find({_id: new ObjectID(acl_id)}).then((acl) => {
            resolve(acl);
        }).catch((error) => {
            reject(error)
        })
    }).then((result) => {
        res.status(200).json({
            status: true,
            message: "Getting single acl success",
            data: (result) ? result : []
        })
    }).catch((error) => {
        res.status(400).json({
            status: true,
            message: "Getting single acl failed",
            error: error
        })
    })
}

/**
 * This method is used to update ACL details by ACL id
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let updateACL = (req, res) => {
    let acl_id = req.params.acl_id;
    let acl_body = req.body;
    return new Promise((resolve, reject) => {
        PagesModel.updateOne({_id: acl_id}, acl_body).then((result) => {
            resolve(result);
        }).catch((error) => {
            reject(error)
        })
    }).then((result) => {
        res.status(200).json({
            status: true,
            message: "ACL update success",
            data: (result) ? result : []
        })
    }).catch((error) => {
        res.status(200).json({
            status: true,
            message: "ACL update success",
            error: error
        })
    })
}

/**
 * This method is used to get all ACLs related to company
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getCompanyACL = (req, res) => {
    let company_id = req.params.company_id;

    return new Promise((resolve, reject) => {
        PagesModel.find({companyId : new ObjectID(company_id)}).then((result) => {
            resolve(result)
        }).catch((error) => {
            reject(error)
        })
    }).then((result) => {
        res.status(200).json({
            status: true,
            message: "Getting company ACLs success",
            data: (result) ? result : []
        })
    }).catch((error) => {
        res.status(400).json({
            status: true,
            message: "Getting company ACLs success",
            error: error
        })
    })
}

/**
 * This method is used to get all the page documents of a specific role in 
 * specific company
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getPagesOfSpecificRoleOfCompany = (req, res) => {
    let company_id = req.params.company_id,
        role_id = req.params.role_id;
    
    if(company_id == undefined || company_id == "" ||
        role_id == undefined || role_id == "") {
            res.status(200).json({
                status: false,
                message: "Invalid data found",
                data: []
            })
    } else {
        return new Promise((resolve, reject) => {
            PagesModel.find({companyId: company_id, roleId: role_id}).then((result) => {
                resolve(result)
            }).catch((error) => {
                reject(error)
            })
        }).then((result) => {
            res.status(200).json({
                status: true,
                message: "Getting pages success",
                data: (result) ? result: []
            })
        }).catch((error) => {
            res.status(200).json({
                status: true,
                message: "Getting pages failed",
                error: error
            })
        })
    }
}

module.exports = {
    createNewPageACL,
    getAllCompanyPageACLS,
    deleteACL,
    getACLById,
    updateACL,
    getCompanyACL,
    createMultiple,
    updateMultiple,
    createCustomRoleACL,
    getPagesOfSpecificRoleOfCompany
};
