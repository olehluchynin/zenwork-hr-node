let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let PagesController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.post('/create', /*[authMiddleware.isUserLogin],*/ PagesController.createNewPageACL);
router.post('/create/all', /*[authMiddleware.isUserLogin],*/ PagesController.createMultiple);

router.get(
    '/all/:company_id', 
    authMiddleware.isUserLogin,
    authMiddleware.isCompany, 
    PagesController.getAllCompanyPageACLS
);

router.get('/find/:acl_id', /*[authMiddleware.isUserLogin],*/ PagesController.getACLById);
router.put('/update/:acl_id', /*[authMiddleware.isUserLogin],*/ PagesController.updateACL);
router.put('/update/all/pages', /*[authMiddleware.isUserLogin],*/ PagesController.updateMultiple);
router.get('/company/:company_id', /*[authMiddleware.isUserLogin],*/ PagesController.getCompanyACL);
router.delete('/delete/:acl_id', /*[authMiddleware.isUserLogin],*/ PagesController.deleteACL);
router.post('/role/page/create', /*[authMiddleware.isUserLogin],*/ PagesController.createCustomRoleACL);
router.get('/:company_id/:role_id', PagesController.getPagesOfSpecificRoleOfCompany);

module.exports = router;
