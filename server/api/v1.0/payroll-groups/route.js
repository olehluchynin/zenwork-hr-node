let express = require('express');

let router = express.Router();

let authMiddleware = require('../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.post(
    '/add',
    authMiddleware.isUserLogin,
    authMiddleware.isCompany,
    validationsMiddleware.reqiuresBody,
    Controller.addPayrollGroup
);


router.get(
    '/get/:company_id/:_id',
    authMiddleware.isUserLogin,
    Controller.getPayrollGroup
);

router.post(
    '/list',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.list
);

router.put(
    '/edit',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editPayrollGroup
);

router.post(
    '/delete',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deletePayrollGroups2
);

// router.put(

// );



module.exports = router