/**
 * @api {post} /payroll-groups/add AddPayrollGroup
 * @apiName AddPayrollGroup
 * @apiGroup PayrollGroups
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"payroll_group_name": "Tech PartTime",
	"pay_frequency": "Weekly",
	"pay_group_code": "T_PT"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Added payroll group successfully",
    "data": {
        "archive": false,
        "_id": "5d2d6772d940063dd8a15b6c",
        "companyId": "5c9e11894b1d726375b23058",
        "payroll_group_name": "Tech PartTime",
        "pay_frequency": "Weekly",
        "pay_group_code": "T_PT",
        "createdBy": "5c9e11894b1d726375b23057",
        "createdAt": "2019-07-16T05:58:10.816Z",
        "updatedAt": "2019-07-16T05:58:10.816Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /payroll-groups/get/:company_id/:_id GetPayrollGroup
 * @apiName GetPayrollGroup
 * @apiGroup PayrollGroups
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} _id PayrollGroups unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Fetched payroll group successfully",
    "data": {
        "_id": "5d2d6772d940063dd8a15b6c",
        "archive": false,
        "companyId": "5c9e11894b1d726375b23058",
        "payroll_group_name": "Tech PartTime",
        "pay_frequency": "Weekly",
        "pay_group_code": "T_PT",
        "createdBy": "5c9e11894b1d726375b23057",
        "createdAt": "2019-07-16T05:58:10.816Z",
        "updatedAt": "2019-07-16T05:58:10.816Z"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /payroll-groups/delete DeletePayrollGroups
 * @apiName DeletePayrollGroups
 * @apiGroup PayrollGroups
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "companyId": "5c9e11894b1d726375b23058",
    "_ids": [
        "5d2d6e806f01882f48ad2add"
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /payroll-groups/list ListPayrollGroups
 * @apiName ListPayrollGroups
 * @apiGroup PayrollGroups
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"per_page": 5,
    "page_no": 1,
    "status": "Active"
}
* @apiParamExample {json} Request-Example2:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "status": "Active"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched list of payroll groups",
    "total_count": 4,
    "data": [
        {
            "_id": "5d2d702f71d81b0d1a012762",
            "archive": false,
            "companyId": "5c9e11894b1d726375b23058",
            "payroll_group_name": "Billing",
            "pay_frequency": "Weekly",
            "pay_group_code": "bil",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-07-16T06:35:27.045Z",
            "updatedAt": "2019-07-16T06:35:27.045Z"
        },
        {
            "_id": "5d2d6ed85836370c5dd90e0f",
            "archive": false,
            "companyId": "5c9e11894b1d726375b23058",
            "payroll_group_name": "HILdd",
            "pay_frequency": "Bi-Weekly",
            "pay_group_code": "hild",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-07-16T06:29:44.714Z",
            "updatedAt": "2019-07-16T06:29:44.714Z"
        },
        {
            "_id": "5d2d6dc65836370c5dd90e0e",
            "archive": false,
            "companyId": "5c9e11894b1d726375b23058",
            "payroll_group_name": "HIL india",
            "pay_frequency": "Bi-Weekly",
            "pay_group_code": "hil",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-07-16T06:25:10.144Z",
            "updatedAt": "2019-07-16T06:25:10.144Z"
        },
        {
            "_id": "5d2d6772d940063dd8a15b6c",
            "archive": false,
            "companyId": "5c9e11894b1d726375b23058",
            "payroll_group_name": "Tech PartTime",
            "pay_frequency": "Weekly",
            "pay_group_code": "T_PT",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-07-16T05:58:10.816Z",
            "updatedAt": "2019-07-16T05:58:10.816Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {put} /payroll-groups/edit EditPayrollGroup
 * @apiName EditPayrollGroup
 * @apiGroup PayrollGroups
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "archive": false,
    "status": "Active",
    "_id": "5d2d8a1dc9dcf43300723e19",
    "companyId": "5c9e11894b1d726375b23058",
    "payroll_group_name": "Tech PartTime",
    "pay_frequency": "Semi-Monthly",
    "pay_group_code": "T_PT",
    "createdBy": "5c9e11894b1d726375b23057",
    "createdAt": "2019-07-16T08:26:05.108Z",
    "updatedAt": "2019-07-16T08:26:05.108Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Payroll group updated successfully",
    "data": {
        "archive": false,
        "status": "Active",
        "_id": "5d2d8a1dc9dcf43300723e19",
        "companyId": "5c9e11894b1d726375b23058",
        "payroll_group_name": "Tech PartTime",
        "pay_frequency": "Semi-Monthly",
        "pay_group_code": "T_PT",
        "createdBy": "5c9e11894b1d726375b23057",
        "createdAt": "2019-07-16T08:26:05.108Z",
        "updatedAt": "2019-07-16T08:26:36.081Z",
        "updatedBy": "5c9e11894b1d726375b23057"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */
