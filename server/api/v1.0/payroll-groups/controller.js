let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../config/config');
let _ = require('underscore');
let PayrollGroupsModel = require('./model');
let PayrollCalendarsModel = require('./../payroll-calendars/model').PayrollCalendarsModel;

let addPayrollGroup = (req,res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve,reject) => {
        if (!companyId) reject('companyId is required')
        else resolve(null);
    })
    .then(() => {
        body.createdBy =  ObjectID(req.jwt.userId)
        Promise.all([
            PayrollGroupsModel.findOne({companyId: companyId, payroll_group_name: body.payroll_group_name}).lean().exec(),
            PayrollGroupsModel.findOne({companyId: companyId, pay_group_code: body.pay_group_code}).lean().exec()
        ])
        .then(([paygroup_with_name, paygroup_with_code]) => {
            if(paygroup_with_name) {
                res.status(400).json({
                    status: false,
                    message: "Payroll group already exists with this name"
                })
            }
            else if (paygroup_with_code) {
                res.status(400).json({
                    status: false,
                    message: "Payroll group already exists with this code"
                })
            }
            else {
                PayrollGroupsModel.create(body)
                    .then(payroll_group => {
                        payroll_group.save(function (err, response) {
                            if (!err) {
                                res.status(200).json({
                                    status: true,
                                    message: "Added payroll group successfully",
                                    data: response
                                })
                            }
                            else {
                                console.log(err)
                                res.status(400).json({
                                    status: false,
                                    message: "Error while adding payroll group",
                                    error: err
                                })
                            }
                        })
                    })
                    .catch(err => {
                        console.log(err)
                        res.status(400).json({
                            status: false,
                            message: "Error while adding payroll group",
                            error: err
                        })
                    })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: "Error while adding payroll group",
                error: err
            })
        })
        
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    });
}

let getPayrollGroup = (req, res) => {
    let companyId = req.params.company_id
    let payroll_group_id = req.params._id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        let find_query = {
            _id: ObjectID(payroll_group_id),
            companyId: ObjectID(companyId)
        }
        PayrollGroupsModel.findOne(find_query).lean().exec()
            .then(payroll_group => {
                if(payroll_group) {
                    res.status(200).json({
                        status: true,
                        message: "Fetched payroll group successfully",
                        data: payroll_group
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "Payroll group not found",
                        data: null
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while fecthing payroll group",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err);
        res.status(400).json({
            status: false,
            message: "Error while fecthing payroll group",
            error: err
        })
    })
}

let list = (req, res ) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if(!companyId) reject('companyId is required')
        else resolve(null);
    })
    .then(() => {
        let sort = {
            updatedAt : -1
        }
        let filter = {
            companyId: ObjectID(companyId),
        }
        let per_page = body.per_page ? body.per_page : null
        let page_no = body.page_no ? body.page_no : 1
        let skip = per_page ? (page_no - 1) * per_page : 0
        let limit = per_page
        if (body.status) {
            filter['status'] = body.status
        }
        Promise.all([
            PayrollGroupsModel.find(filter).sort(sort).skip(skip).limit(limit).lean().exec(),
            PayrollGroupsModel.countDocuments(filter)
        ])
        .then(([payroll_groups, total_count]) => {
            res.status(200).json({
                status: true, message: "Successfully fetched list of payroll groups", total_count: total_count, data: payroll_groups
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while fetching payroll groups",
                error: err
            })
        });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let editPayrollGroup = (req,res) => {
    let body = req.body
    let payroll_group_id = body._id
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!payroll_group_id) {
            reject('_id is required')
        }
        else if (!companyId) {
            reject('companyId is required')
        }
        else {
            resolve(null);
        }
    })
    .then(() => {
        delete body.companyId
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        console.log(body)
        let filter = {
            _id: ObjectID(payroll_group_id), 
            companyId : ObjectID(companyId)
        }
        console.log(filter)
        body.updatedBy = ObjectID(req.jwt.userId)
        PayrollGroupsModel.findOneAndUpdate(filter, {$set: body}, {new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                res.status(200).json({
                    status: true,
                    message: "Payroll group updated successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while updating payroll group",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    });
}


let deletePayrollGroups = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else resolve(null);
    })
    .then(() => {
        PayrollGroupsModel.deleteMany({
            companyId: companyId,
            _id: {$in : body._ids}
        })
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting payroll groups",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

/**
*@author Asma
*@date 22/07/2019 15:10
delete payroll groups if not being used in payroll calendars
*/
let deletePayrollGroups2 = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else resolve(null);
    })
    .then(async () => {
        let promises = []
        for (let _id of body._ids) {
            let responses = await Promise.all([
                PayrollCalendarsModel.findOne({companyId: companyId, assigned_payroll_group: _id}).lean().exec(),
                PayrollGroupsModel.findOne({companyId: companyId, _id: _id}).lean().exec()
            ])

            if (responses[0]) {
                promises.push(Promise.reject(responses[1].payroll_group_name + ' is assigned to a payroll calendar. It cannot be deleted'))
            }
            else {
                promises.push(PayrollGroupsModel.deleteOne({companyId: companyId, _id: _id}))
            }
                
        }
        // console.log(promises)
        Promise.all(promises)
            .then(responses => {
                console.log(responses)
                res.status(200).json({
                    status: true,
                    message: "Successfully deleted payroll groups",
                    data: responses
                })
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: err
                })
            })
        
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let changeStatus = (req, res) => {
    let body = req.body
    let payroll_group_id = body._id
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!payroll_group_id) {
            reject('_id is required')
        }
        else if (!body.hasOwnProperty("archive")) {
            reject('archive field is required')
        }
        else if (!companyId) {
            reject('companyId is required')
        }
        else {
            resolve(null);
        }
    })
    .then(()=> {
        delete body.companyId
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        let filter = {
            _id: ObjectID(payroll_group_id),
            companyId : ObjectID(companyId), 
        }
        console.log(filter)
        body.createdBy =  ObjectID(req.jwt.userId)
        PayrollGroupsModel.findOneAndUpdate(filter, {$set: body}, {new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                res.status(200).json({
                    status: true,
                    message: "Changed status successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while changing status",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while changing status",
            error: err
        })
    })
}

module.exports = {
    addPayrollGroup,
    getPayrollGroup,
    list,
    editPayrollGroup,
    deletePayrollGroups,
    changeStatus,
    deletePayrollGroups2
}

