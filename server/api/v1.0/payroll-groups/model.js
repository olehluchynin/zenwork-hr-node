let mongoose = require('mongoose')
let ObjectID = mongoose.Schema.ObjectId;

let PayrollGroupSchema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    createdBy: {type: ObjectID, ref: 'users'},
    updatedBy: {type: ObjectID, ref: 'users'},
    payroll_group_name: {type: String, required: true},
    pay_frequency: {type: String, required: true},
    pay_group_code: {type: String, required: true},
    archive: {type: Boolean, default: false},
    status: {type: String, enum: ['Active', 'Inactive'], default: 'Active'}
}, {
    timestamps: true,
    versionKey: false
})

PayrollGroupSchema.index({companyId: 1, payroll_group_name: 1}, {unique: true})

PayrollGroupSchema.index({companyId: 1, pay_group_code: 1}, {unique: true})

let PayrollGroupModel = mongoose.model('payroll_groups', PayrollGroupSchema, 'payroll_groups');


module.exports = PayrollGroupModel