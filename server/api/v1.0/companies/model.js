let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    userId:{type:ObjectID,ref:'users'},
    settingsId:{type:ObjectID,ref:'company_settings'},
    email:{type:String,required:true,unique:true},
    name:String,    //Company Legal Name
    fein: String,   //Federal Employer ID Number
    tradeName:String,
    employeCount:Number,
    stepsCompleted: {type: Number, default:1},
    type:{type:String,enum:Object.keys(constants.companyTypes)},
    resellerId: { type: ObjectID, ref: 'users' },
    referralBy: { type: ObjectID, ref: 'companies' },
    primaryContact: {
        name:String,
        phone:String,
        email:String,
        isEmailVerified: {type: Boolean, default: false},
        extention:String,
        mobilePhone: String
    },
    secondaryContact: {
        name:String,
        phone:String,
        email:String,
        isEmailVerified: {type: Boolean, default: false},
        extention:String,
        mobilePhone: String
    },
    billingContact: {
        name:String,
        phone:String,
        email:String,
        isEmailVerified: {type: Boolean, default: false},
        extention:String,
        mobilePhone: String
    },
    isPrimaryContactIsBillingContact:Boolean,
    primaryAddress:{
        address1:String,
        address2:String,
        city:String,
        state:String,
        zipcode:String,
        country:String,
        phone: String
    },
    billingAddress:{
        address1:String,
        address2:String,
        city:String,
        state:String,
        zipcode:String,
        country:String
    },
    isPrimaryAddressIsBillingAddress:Boolean,
    createdBy:{type:ObjectID,ref:'zenworkers'},
    updatedBy:{type:ObjectID,ref:'zenworkers'},
    // createdDate:{type:Date, default:Date.now()},
    // updatedDate:{type:Date,default:Date.now()},
},{
    timestamps: true,
    versionKey: false
});
Schema.plugin(autoIncrement.plugin, {model:'companies', field:'companyId', startAt: 1, incrementBy: 1});

let CompaniesModel = mongoose.model('companies',Schema);

module.exports = CompaniesModel;






// {
// "email":"mtwlabs@gmail.com",
// "name":"MTWLabs",
// "tradeName":"New Trade",
// "employeCount":10,
// "type":"client",
// "primaryContact":{
//     "name":"ROhith",
//     "phone":"9876543211",
//     "extention":""
// },
// "billingContact":{
//     "name":"Maruthi",
//     "phone":"987654322",
//     "email":"maruthi@mtwlabs.com",
//     "extention":""
// },
// "isPrimaryContactIsBillingContact":false,
// "address":{
//     "address1":"Madhapur",
//     "address2":"Ayyappa society",
//     "city":"Hyderabad",
//     "state":"Telangana",
//     "zipcode":"500081",
//     "country":"India"
// },
// "billlingAddress":{
//     "address1":"Madhapur",
//     "address2":"Ayyappa society",
//     "city":"Hyderabad",
//     "state":"Telangana",
//     "zipcode":"500081",
//     "country":"India"
// },
// "isPrimaryAddressIsBillingAddress":false
// }
