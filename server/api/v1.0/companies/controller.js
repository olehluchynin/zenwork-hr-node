let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let CompaniesModel = require('./model');
let UserModel = require('./../users/model');
let OrdersModel = require('./../orders/model');
let UserController = require('./../users/controller');
let CompanySettingsModel = require('./settings/model');

let onboardingController = require('../employee-management/onboarding/onboard-template/controller');

let jwt = require('jsonwebtoken');
let config = require('../../../config/config');

let utils = require('./../../../common/utils');
let helpers = require('./../../../common/helpers');
let constants = require('./../../../common/constants');

let mailer = require('./../../../common/mailer');
var Handlebars = require('handlebars');
let fs = require('fs');

let addNew = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        if (!body.name) reject("Invalid name");
        // else if (!body.email || !helpers.isValidEmail(body.email)) reject("Invalid email");
        else if (!body.type) reject('Invalid type');
        else resolve(null);
    }).then(() => {
        return UserModel.findOne({email: body.email})
    }).then((user) => {
        console.log("uesr data===>", user);
        if (user) {
            res.status(402).json({status: false, message: "User already exists", data: null});
        } else {
            body.createdBy = req.jwt.zenworkerId;
            body.password = utils.encryptPassword(utils.generateRandomPassword());
            return Promise.all([UserController.addNewUser(body), CompaniesModel.create(body), CompanySettingsModel.create(body)]);
        }
    }).then((data) => {
        // console.log("Data", data);
        if (data) {
            var user = data[0];
            var company = data[1];
            var companySettings = data[2];
            if (company && user) {
                company.userId = user._id;
                user.companyId = company._id;
                companySettings.companyId = company._id;
                company.settingsId = companySettings._id;
                company.save();
                user.save();
                companySettings.save();
            }
            if (company) {
                mailer.sendUsernamePasswordToCompany(body.email, body.name, body.email, body.password, constants.companyTypes[company.type]);
                company = utils.formatCompanyData(company.toJSON());
                res.status(200).json({status: true, message: "New company added successfully", data: company});
            }
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};  //no

let addClientDetails = (req, res) => {
    console.log('in add client')
    let body = req.body;
    let password_to_send_to_email;
    console.log('add client body - ', body)
    return new Promise((resolve, reject) => {
        if (!body.name) reject("Invalid name");
        else if (!body.type) reject('Invalid type');
        else resolve(null);
    }).then(() => {
        body.email = body.email.toLowerCase()
        return UserModel.findOne({email: body.email})
    }).then((user) => {
        if (user) {
            console.log("User already exists")
            res.status(409).json({status: false, message: "Company already exists with same email", data: null});
        } else {
            console.log("New User")
            body.primaryContact = {
                email: body.email
            };
            if(body.resellerId === "") delete body.resellerId;
            if(req.jwt.zenworkerId) body.createdBy = req.jwt.zenworkerId;
            password_to_send_to_email = utils.generateRandomPassword()
            body.password = utils.encryptPassword(password_to_send_to_email);
            return Promise.all([UserController.addNewUser(body), CompaniesModel.create(body), CompanySettingsModel.create(body)]);
        }
    }).then((data) => {
        if (data) {
            var user = data[0];
            var company = data[1];
            var companySettings = data[2];
            onboardingController.addDefaultOBTemplate(company._id);
            if (company && user) {
                company.userId = user._id;
                console.log('company id - ', company._id)
                user.companyId = company._id;
                companySettings.companyId = company._id;
                company.settingsId = companySettings._id;
                company.save();
                user.save();
                companySettings.save();
            }
            // #TODO send creds to user email 
            let logo_url = "http://52.207.167.138:3003/api/client/images/mainblacklogo.png"
            let login_url = 'https://zenworkhr.com/zenwork-login'
            let to = body.email
            let from = config.mail.defaultFromAddress
            let subject = 'Welcome to the Zenwork HR family'
            let email_body = {
                logo_url: config.baseUrl + '/api/client/images/mainblacklogo.png',
                login_url: config.frontEndUrl + '/zenwork-login',
                // name: body.primaryContact.name,
                company_name: body.name,
                email: body.email,
                password: password_to_send_to_email
            }
            let text = "ZenworkHR"
            fs.readFile(__dirname + '/add_client_email.hbs', 'utf8', async (err, file_data) => {
                if (!err) {
                    // console.log(file_data)
                    let html_data = (Handlebars.compile(file_data))(email_body)
                    console.log(to, from, subject, text, html_data)
                    try {
                        await mailer.sendClientCredsEmail(to, from, subject, text, html_data)
                        res.status(200).json({ status: true, message: "Client Details added successfully", data: company });
                    }
                    catch (e) {
                        console.log(err)
                        res.status(400).json({
                            status: false,
                            message: "Error while sending email",
                            error: err
                        })
                    }
                }
                else {
                    console.log(err)
                    res.status(400).json({
                        status: false,
                        message: "Error while sending email",
                        error: err
                    })
                }
            })
            // res.status(200).json({status: true, message: "Client Details added successfully", data: company});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let addPackages = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        //will add validations.
        resolve(null);
    }).then(() => {
        return UserModel.findOne({email: body.email})
    }).then((user) => {
        console.log("uesr data ===>", user);
        if (user) {
            res.status(402).json({status: false, message: "User already exists", data: null});
        } else {
            body.createdBy = req.jwt.zenworkerId;
            body.password = utils.encryptPassword(utils.generateRandomPassword());
            return Promise.all([UserController.addNewUser(body), CompaniesModel.create(body), CompanySettingsModel.create(body)]);
        }
    }).then((data) => {
        console.log("Data", data);
        if (data) {
            var user = data[0];
            var company = data[1];
            var companySettings = data[2];
            if (company && user) {
                company.userId = user._id;
                user.companyId = company._id;
                companySettings.companyId = company._id;
                company.settingsId = companySettings._id;
                company.save();
                user.save();
                companySettings.save();
            }
            if (company) {
                mailer.sendUsernamePasswordToCompany(body.email, body.name, body.email, body.password, constants.companyTypes[company.type]);
                company = utils.formatCompanyData(company.toJSON());
                res.status(200).json({status: true, message: "New company added successfully", data: company});
            }
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};  //no

let addContactDetails = (req, res) => {
    // console.log("jwt ===> ", req.jwt);
    let body = req.body;
    return new Promise((resolve, reject) => {
        // if (!body.name) reject("Invalid name");
        // else if (!body.email || !helpers.isValidEmail(body.email)) reject("Invalid email");
        // else if (!body.type) reject('Invalid type');
        // else
            resolve(null);
    }).then(() => {
        // if(req.jwt.userId) body.updatedBy = req.jwt.userId;
        body.stepsCompleted = 3;
        return CompaniesModel.findOneAndUpdate({_id: body._id},{$set:body},{returnOriginal:false});
    }).then((data) => {
        console.log("Data", data);
        if (data) {
            res.status(200).json({status: true, message: "Added contact details successfully", data: data});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let login = (req, res) => {
    let body = req.body;

    return new Promise((resolve, reject) => {
        if (!body.email || !helpers.isValidEmail(body.email)) reject("Invalid email");
        else if (!body.password) reject("Invalid passoword");
        else resolve(null);
    }).then(() => {
        return CompaniesModel.findOne({email: body.email});
    }).then(company => {
        if (!company) {
            res.status(400).json({status: false, message: "Company not found", data: null});
        } else {
            if (body.password == utils.decryptPassword(zenworker.password)) {
                zenworker = utils.formatCompanyData(zenworker.toJSON());
                zenworker.token = utils.generateJWTForUser(zenworker);
                res.status(200).json({status: true, message: "Logged successfully", data: zenworker});
            } else {
                res.status(200).json({status: false, message: "Invalid password", data: null});
            }
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let getById = (req, res) => {

    let userId = req.params._id;

    return new Promise((resolve, reject) => {
        if (!userId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return CompaniesModel.findOne({_id: userId});
    }).then(zenworker => {
        if (!zenworker) {
            res.status(400).json({status: false, message: "User not found", data: null});
            return null;
        } else {
            zenworker = utils.formatZenworkerData(zenworker.toJSON());
            res.status(200).json({status: true, message: "User details fetched successfully", data: zenworker});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let getSelfData = (req, res) => {

    // let userId = req.jwt._id;
    let userId = req.jwt.companyId;

    return new Promise((resolve, reject) => {
        if (!userId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return CompaniesModel.findOne({_id: userId});
    }).then(zenworker => {
        if (!zenworker) {
            res.status(400).json({status: false, message: "User not found", data: null});
            return null;
        } else {
            zenworker = utils.formatZenworkerData(zenworker.toJSON());
            res.status(200).json({status: true, message: "User details fetched successfully", data: zenworker});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let updateSelfData = (req, res) => {

    let body = req.body;

    // let _id = req.jwt._id;
    let _id = req.jwt.companyId;

    return new Promise((resolve, reject) => {
        if (!userId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return CompaniesModel.findOne({_id: userId});
    }).then(company => {
        if (!company) {
            res.status(400).json({status: false, message: "User not found", data: null});
            return null;
        } else {
            if (body.password) delete body.password;
            // body.udpateById = req.jwt._id;
            body.udpateById = req.jwt.zenworkerId;
            return CompaniesModel.update({_id: _id}, body);
        }
    }).then(updated => {
        if (updated) {
            res.status(200).json({status: true, message: "Updated successfully", data: null});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let udpateById = (req, res) => {

    let body = req.body;

    let _id = req.params._id;

    return new Promise((resolve, reject) => {
        if (!_id) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return CompaniesModel.findOne({_id: _id});
    }).then(company => {
        if (!company) {
            res.status(400).json({status: false, message: "Company not found", data: null});
            return null;
        } else {
            if (body.password) delete body.password;
            delete body._id
            delete body.email
            delete body.companyId
            body.udpateById = req.jwt.zenworkerId;
            return CompaniesModel.findOneAndUpdate({_id: _id}, {$set: body},{upsert: false});
        }
    }).then(updated => {
        if (updated) {
            res.status(200).json({status: true, message: "Updated successfully", data: null});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let getAll = (req, res) => {

    let queryParams = req.query;

    var searchQuery = req.query.searchQuery || '';
    var type = req.query.type;


    return new Promise((resolve, reject) => {
        // if (!body.name || !body.name.first || !body.name.last) reject("invalid name field");
        // else if (!body.email || !helpers.isValidEmail(body.email)) reject("invalid email");
        // else if (!body.password || !helpers.isValidPasword(body.password)) reject("invalid password");
        // else if(!body.mobile || !helpers.isValidPhoneNumber(body.mobile.number))reject("invalid Phone number");
        // else if (body.role === "admin") reject('User type admin cannot be allowed');
        // else
        resolve(null);
    }).then(() => {
        let options = {};
        if (queryParams && queryParams.requiredFields) {
            queryParams.requiredFields = JSON.parse(queryParams.requiredFields);
            options = queryParams.requiredFields;
            options.createdDate = 1;
            options.updatedDate = 1;
        }

        let sortOptions = {};
        if (queryParams && queryParams.sort) {
            sortOptions = JSON.parse(queryParams.sort);
        }

        let perPage = queryParams.perPage ? parseInt(queryParams.perPage) : 20;
        let pageNumber = queryParams.pageNumber ? parseInt(queryParams.pageNumber) : 1;

        let findQuery;

        if (searchQuery) {
            if (!findQuery) findQuery = {};
            findQuery['$or'] = [{name: new RegExp(searchQuery, 'i')}, {email: new RegExp(searchQuery, 'i')}];
        }

        if (type) {
            if (!findQuery) findQuery = {};
            findQuery['type'] = type;
        }

        console.log("findQuery", findQuery);
        console.log("sortOptions", sortOptions);
        console.log("options", options);

        var aggregatePipes = [];
        if (findQuery) {
            aggregatePipes.push(
                {
                    $match: findQuery
                }
            );
        }

        var aggregatePipeForCount = [];

        aggregatePipes.forEach(pipe => {
            aggregatePipeForCount.push(pipe);
        });
        aggregatePipeForCount.push(
            {
                $group: {
                    _id: null,
                    count: {$sum: 1}
                }
            }
        );

        aggregatePipes.push(
            {
                $skip: perPage * (pageNumber - 1)
            }
        );
        aggregatePipes.push(
            {
                $limit: perPage
            },
        );

        if (queryParams.requiredFields) {
            aggregatePipes.push(
                {
                    $project: options
                },
            );
        }
        return Promise.all([
            CompaniesModel.aggregate(aggregatePipes),
            CompaniesModel.aggregate(aggregatePipeForCount),
        ]);
    }).then(([companies, countJSON]) => {
        var count = 0;
        if (countJSON && countJSON.length > 0) count = countJSON[0].count;
        res.status(200).json({status: true, message: "Companies data Fetched", data: companies, count});
    }).catch((err) => {
        console.log(err);
        if (typeof err == 'object') {
            res.status(500).json({status: false, message: "Internal server error", data: null});
        } else {
            res.status(400).json({status: false, message: err, data: null});
        }
    });

};

let getAllresellers = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return UserModel.aggregate([
            {$match:{type:'reseller'}},
            {
                $lookup: {
                    from: "companies",
                    localField: "companyId",
                    foreignField: "_id",
                    as: "companyId"
                }
            },
            {
                $unwind: "$companyId"
            },
            {
                $project: {_id:1,name:"$companyId.name"}
            }
        ])//.populate('companyId',{name:1});
    }).then(clients => {
        // if (!clients) {
        //     res.status(400).json({status: false, message: "User not found", data: null});
        //     return null;
        // } else {
            res.status(200).json({status: true, message: "fetched successfully", data: clients});
        // }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let getResellerDetails = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        if(!body.resellerId) {
            reject("Invalid CompanyId");
        } else {
            resolve(null);
        }
    }).then(() => {
        let query = {resellerId: body.resellerId, type: 'reseller-client'};
        return Promise.all([
            UserModel.find(query,{password:0}).populate('companyId',{name:1,tradeName:1}),
            UserModel.findOne({_id:body.resellerId},{email:1}).populate('companyId',{name:1,tradeName:1})
        ]);
    }).then(data => {
        if (!data[1]) {
            res.status(404).json({status: false, message: "Reseller not found", data: null});
        } else {
            res.status(200).json({status: true, message: "fetched successfully", data: data[0], reseller: data[1]});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let getCompanyDetails = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompaniesModel.findOne({userId:req.params._id});
    }).then(company => {
        return Promise.all([
            company, 
            OrdersModel.findOne({clientId:company._id,paymentStatus:'success'},{startDate:1,endDate:1})
        ]);
    }).then(data => {
        res.status(200).json({status: true, message: "Fetched successfully", data: data[0],order: data[1]});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let getOwnCompanyDetails = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompaniesModel.findOne({userId:req.jwt.userId});
    }).then(company => {
        res.status(200).json({status: true, message: "fetched successfully", data: company});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

//gives the data requires in general settings page from 2 tables.
let getGeneralSettings = (req, res) => {
    return new Promise((resolve, reject) => {
        if(!req.jwt.companyId) reject("Not Allowed");
        resolve(null);
    }).then(() => {
        return Promise.all([CompaniesModel.findOne({_id:req.jwt.companyId},{_id:0,name:1,fein:1,primaryContact:1,secondaryContact:1,primaryAddress:1}), CompanySettingsModel.findOne({companyId:req.jwt.companyId},{_id:0,general:1})]);
    }).then(companyDetails => {
        if(companyDetails[0] && companyDetails[1]) {
            res.status(200).json({status: true, message: "Success", companyDetails: companyDetails[0], settings: companyDetails[1]});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

//gives the data requires in general settings page from 2 tables.
let updateGeneralSettings = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        if(!req.jwt.companyId) reject("Not Allowed");
        resolve(null);
    }).then(() => {
        // delete body.companyDetails.primaryContact.email; //not allowed to update here
        // delete body.companyDetails.primaryContact.isEmailVerified; //not allowed to update here
        // delete body.companyDetails.secondaryContact.isEmailVerified; //not allowed to update here
        // console.log(body);
        return Promise.all([CompaniesModel.findOneAndUpdate({_id:req.jwt.companyId},body.companyDetails), CompanySettingsModel.findOneAndUpdate({companyId:req.jwt.companyId},body.settings)]);
    }).then(companyDetails => {
        if(companyDetails[0] && companyDetails[1]) {
            res.status(200).json({status: true, message: "Success"});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let sendVerificationEmail = (req, res) => {
    let body = req.body;
    body.companyId = req.jwt.companyId;
    console.log(req.jwt);
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let token = utils.generateEmailVerifyLink(body);
        mailer.sendEmailVerifyLink(body.email, body.name, token);
        res.status(200).send({ status: true, message: "Verification mail sent", data: null });
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let verifyEmail = (req, res) => {
    let token = req.body.token;
    let decoded;
    utils.decodePasswordResetToken(token)
        .then(data => {
            decoded = data;
            console.log('decoded ==> ', decoded, typeof decoded);
            return CompaniesModel.findOne({_id: decoded.companyId}, {primaryContact: 1, secondaryContact: 1}).lean().exec();
        }).then((company) => {
            // console.log();
            if (company) {
                if (company.primaryContact.email === decoded.email) {
                    company.primaryContact.isEmailVerified = true;
                    return company;
                } else if (company.secondaryContact.email === decoded.email) {
                    company.secondaryContact.isEmailVerified = true;
                    return company;
                } else {
                    res.status(409).json({status: false, message: "Email not matching"});
                }
            } else {
                res.status(404).json({status: false, message: "Company not found"});
            }
    }).then(company => {
        delete company._id;
        return CompaniesModel.findOneAndUpdate({_id: decoded.companyId}, company);
    }).then(updated => {
        res.status(200).json({status: true, message: "Successfully updated"});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let updateBasicCompanyDetails = (req, res) => {
    console.log('in update company details')
    let body = req.body;
    let companyId = body.companyId
    console.log('company Id ', companyId)
    return new Promise((resolve, reject) => {
        if(!companyId) reject("Not Allowed");
        resolve(null);
    }).then(() => {
        // delete body.companyDetails.primaryContact.email; //not allowed to update here
        // delete body.companyDetails.primaryContact.isEmailVerified; //not allowed to update here
        // delete body.companyDetails.secondaryContact.isEmailVerified; //not allowed to update here
        // console.log(body);
        return CompaniesModel.findOneAndUpdate({_id:ObjectID(companyId)},{$set: body.companyDetails}, {new : true});
    }).then(companyDetails => {
        console.log(companyDetails)
        if(companyDetails) {
            res.status(200).json({status: true, message: "Success", data: companyDetails});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

module.exports = {
    addNew,
    login,
    updateSelfData,
    getSelfData,
    udpateById,
    getById,
    getAll,
    addClientDetails,
    addPackages,
    addContactDetails,
    getAllresellers,
    getResellerDetails,
    getCompanyDetails,
    getOwnCompanyDetails,
    getGeneralSettings,
    updateGeneralSettings,
    sendVerificationEmail,
    verifyEmail,

    updateBasicCompanyDetails
};
