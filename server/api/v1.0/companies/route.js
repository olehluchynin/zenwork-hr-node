let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let CompaniesController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');


//router.post('/add', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, CompaniesController.addNew);

router.put('/updateCompanyDetails',
    validationsMiddleware.reqiuresBody,
    CompaniesController.updateBasicCompanyDetails);

router.post('/addClientDetails', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, CompaniesController.addClientDetails);

router.post('/addContactDetails',/*authMiddleware.isUserLogin , */validationsMiddleware.reqiuresBody, CompaniesController.addContactDetails);

// router.post('/addSelfContactDetails', validationsMiddleware.reqiuresBody, CompaniesController.addContactDetails);

router.get('/all', CompaniesController.getAll);

router.put('/updateGeneralSettings', authMiddleware.isUserLogin, authMiddleware.isCompany, CompaniesController.updateGeneralSettings);

router.put('/:_id', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, authMiddleware.isCompany, CompaniesController.udpateById);

router.get('/resellers'/*, authMiddleware.isUserLogin, authMiddleware.isCompany*/, CompaniesController.getAllresellers);

router.post(
    '/resellerDetails',
    authMiddleware.isUserLogin,
    // authMiddleware.isCompany, 
    CompaniesController.getResellerDetails
);

router.get('/getCompanyDetails/:_id', authMiddleware.isUserLogin, CompaniesController.getCompanyDetails);

router.get('/getOwnCompanyDetails', authMiddleware.isUserLogin, CompaniesController.getOwnCompanyDetails);

//getting general settings of a company.
router.get('/getGeneralSettings', authMiddleware.isUserLogin, authMiddleware.isCompany, CompaniesController.getGeneralSettings);

//getting general settings of a company.


router.post('/sendVerificationEmail', authMiddleware.isUserLogin, authMiddleware.isCompany, CompaniesController.sendVerificationEmail);

router.post('/verifyEmail', CompaniesController.verifyEmail);


router.put('/', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, CompaniesController.updateSelfData);

router.use('/settings', require('./settings/route'));



module.exports = router;
