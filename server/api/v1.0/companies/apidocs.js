/**
 * @apiDefine DataNotFoundError
 *
 * @apiError DataNotFound The data the you are requested was not found.
 *
 * @apiErrorExample Data Not Found Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "message": "Data Not Found"
 *       "data": null 
 *     }
 */


 /**
 * @apiDefine InternalServerError
 *
 * @apiError InternalServerError Internal Server error.
 *
 * @apiErrorExample Internal Server Error:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "status": false,
 *       "message": "Internal server error"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine TokenNotFound
 *
 * @apiError TokenNotFound User Token Not Found.
 *
 * @apiErrorExample Token Not Found:
 *     HTTP/1.1 400 Token Not Found
 *     {
 *       "status": false,
 *       "message": "Token Not Found"
 *       "data": null 
 *     }
 */

  /**
 * @apiDefine BadRequest
 *
 * @apiError BadRequest Bad request.
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": false,
 *       "message": "Invalid key"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine AccessDeneid
 *
 * @apiError AccessDenied Access denied for this operation.
 *
 * @apiErrorExample Access Deneid:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "status": false,
 *       "message": "Access Denied for this operation"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine DataAlreadyExists
 *
 * @apiError DataConflict Date already exists.
 *
 * @apiErrorExample Data Conflict Error:
 *     HTTP/1.1 402 Data Conflict
 *     {
 *       "status": false,
 *       "message": "Data already exist"
 *       "data": null 
 *     }
 */


 /**
 * @api {post} /companies/add Add new 
 * @apiName AddNewCompany
 * @apiGroup Companies
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "email":"mtwlabs@gmail.com",
    "name":"MTWLabs",
    "tradeName":"New Trade",
    "employeCount":10,
    "type":"company",
    "primaryContact":{
        "name":"ROhith",
        "phone":"9876543211",
        "extention":""
    },
    "billingContact":{
        "name":"Maruthi",
        "phone":"987654322",
        "email":"maruthi@mtwlabs.com",
        "extention":""
    },
    "isPrimaryContactIsBillingContact":false,
    "address":{
        "address1":"Madhapur",
        "address2":"Ayyappa society",
        "city":"Hyderabad",
        "state":"Telangana",
        "zipcode":"500081",
        "country":"India"
    },
    "billlingAddress":{
        "address1":"Madhapur",
        "address2":"Ayyappa society",
        "city":"Hyderabad",
        "state":"Telangana",
        "zipcode":"500081",
        "country":"India"
    },
    "isPrimaryAddressIsBillingAddress":false
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "New company added successfully",
    "data": {
        "createdDate": "2018-12-31T11:29:20.551Z",
        "updatedDate": "2018-12-31T11:29:20.551Z",
        "_id": "5c29fda0919757387912827a",
        "email": "mtwlabs@gmail.com",
        "name": "MTWLabs",
        "tradeName": "New Trade",
        "employeCount": 10,
        "type": "company",
        "primaryContact": {
            "name": "ROhith",
            "phone": "9876543211",
            "extention": ""
        },
        "billingContact": {
            "name": "Maruthi",
            "phone": "987654322",
            "email": "maruthi@mtwlabs.com",
            "extention": ""
        },
        "isPrimaryContactIsBillingContact": false,
        "address": {
            "address1": "Madhapur",
            "address2": "Ayyappa society",
            "city": "Hyderabad",
            "state": "Telangana",
            "zipcode": "500081",
            "country": "India"
        },
        "isPrimaryAddressIsBillingAddress": false,
        "createdBy": "5c126409d84721019cf167be",
        "__v": 0,
        "userId": "5c29fda09197573879128279",
        "settingsId": "5c29fda0919757387912827b"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */




  /**
 * @api {put} /companies/ Update self info 
 * @apiName UpdateSelfDataCompany
 * @apiGroup Companies
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "name":"MTWLabs",
    "tradeName":"New Trade",
    "employeCount":10,
    "type":"company",
    "primaryContact":{
        "name":"ROhith",
        "phone":"9876543211",
        "extention":""
    },
    "billingContact":{
        "name":"Maruthi",
        "phone":"987654322",
        "email":"maruthi@mtwlabs.com",
        "extention":""
    },
    "isPrimaryContactIsBillingContact":false,
    "address":{
        "address1":"Madhapur",
        "address2":"Ayyappa society",
        "city":"Hyderabad",
        "state":"Telangana",
        "zipcode":"500081",
        "country":"India"
    },
    "billlingAddress":{
        "address1":"Madhapur",
        "address2":"Ayyappa society",
        "city":"Hyderabad",
        "state":"Telangana",
        "zipcode":"500081",
        "country":"India"
    },
    "isPrimaryAddressIsBillingAddress":false
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Updated successfully",
    "data":null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */



  /**
 * @api {put} /companies/ Update company info
 * @apiName UpdateCompanyData
 * @apiGroup Companies
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "name":"MTWLabs",
    "tradeName":"New Trade",
    "employeCount":10,
    "type":"company",
    "primaryContact":{
        "name":"ROhith",
        "phone":"9876543211",
        "extention":""
    },
    "billingContact":{
        "name":"Maruthi",
        "phone":"987654322",
        "email":"maruthi@mtwlabs.com",
        "extention":""
    },
    "isPrimaryContactIsBillingContact":false,
    "address":{
        "address1":"Madhapur",
        "address2":"Ayyappa society",
        "city":"Hyderabad",
        "state":"Telangana",
        "zipcode":"500081",
        "country":"India"
    },
    "billlingAddress":{
        "address1":"Madhapur",
        "address2":"Ayyappa society",
        "city":"Hyderabad",
        "state":"Telangana",
        "zipcode":"500081",
        "country":"India"
    },
    "isPrimaryAddressIsBillingAddress":false
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Updated successfully",
    "data":null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */



  /**
 * @api {get} /companies/all Get all companies data
 * @apiName GetCompaniesList
 * @apiGroup Companies
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.

 * @apiParam {String} searchQuery(queryParam) search query string.
 * @apiParam {String} type(queryParam) type for filter (company,reseller).
 * @apiParam {String} status(queryParam) status for filter (active,inactive).
 * @apiParam {String} requiredFields(queryParam) to get required fields only in data
 * @apiParam {Number} perPage(queryParam) records count per page.
 * @apiParam {Number} pageNumber(queryParam) page number
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Companies data Fetched",
    "data": [
        {
            "_id": "5c135257346f6e142980a869",
            "primaryContact": {
                "name": "ROhith",
                "phone": "9876543211",
                "extention": ""
            },
            "billingContact": {
                "name": "Maruthi",
                "phone": "987654322",
                "email": "maruthi@mtwlabs.com",
                "extention": ""
            },
            "address": {
                "address1": "Madhapur",
                "address2": "Ayyappa society",
                "city": "Hyderabad",
                "state": "Telangana",
                "zipcode": "500081",
                "country": "India"
            },
            "createdDate": "2018-12-14T06:48:28.876Z",
            "updatedDate": "2018-12-14T06:48:28.876Z",
            "email": "company3@gmail.com",
            "name": "company3",
            "tradeName": "New Trade",
            "employeCount": 10,
            "type": "company",
            "isPrimaryContactIsBillingContact": false,
            "isPrimaryAddressIsBillingAddress": false,
            "createdBy": "5c126409d84721019cf167be",
            "__v": 0,
            "userId": "5c135257346f6e142980a868"
        },
        {
            "_id": "5c13533e3e32ff14fb4311dd",
            "primaryContact": {
                "name": "ROhith",
                "phone": "9876543211",
                "extention": ""
            },
            "billingContact": {
                "name": "Maruthi",
                "phone": "987654322",
                "email": "maruthi@mtwlabs.com",
                "extention": ""
            },
            "address": {
                "address1": "Madhapur",
                "address2": "Ayyappa society",
                "city": "Hyderabad",
                "state": "Telangana",
                "zipcode": "500081",
                "country": "India"
            },
            "createdDate": "2018-12-14T06:52:40.186Z",
            "updatedDate": "2018-12-14T06:52:40.186Z",
            "email": "rajesh4.ramesh@gmail.com",
            "name": "company3",
            "tradeName": "New Trade",
            "employeCount": 10,
            "type": "company",
            "isPrimaryContactIsBillingContact": false,
            "isPrimaryAddressIsBillingAddress": false,
            "createdBy": "5c126409d84721019cf167be",
            "__v": 0,
            "userId": "5c13533e3e32ff14fb4311dc"
        }
    ],
    "count": 2
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /companies/getGeneralSettings Get General Settings
 * @apiName getGeneralSettings
 * @apiGroup Companies
 *
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Success",
    "companyDetails": {
        "primaryContact": {
            "isEmailVerified": false,
            "name": "njuig",
            "phone": "2124548782"
        },
        "secondaryContact": {
            "isEmailVerified": false
        },
        "_id": "5c9e3dd42bdee465121c80df",
        "name": "nkojjj"
    },
    "settings": {
        "general": {
            "lockAccessNeeded": false,
            "bootAllUsersNeeded": false,
            "messageOnFrontPageNeeded": false,
            "messageOnFrontPage": ""
        },
        "_id": "5c9e3dd42bdee465121c80e0"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */


/**
 * @api {put} /companies/updateGeneralSettings Update General Settings
 * @apiName updateGeneralSettings
 * @apiGroup Companies
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
 *
 {
    "companyDetails": {
        "primaryContact": {
            "isEmailVerified": false,
            "name": "Maruthi Kurrak",
            "phone": "8686062024",
            "extention": "12"
        },
        "secondaryContact": {
            "isEmailVerified": false,
            "name": "Maruthi Kurrak",
            "phone": "8686062024",
            "extention": "12"
        },
        "primaryAddress": {
            "address1": "ayyappa society",
            "address2": "lane 5",
            "city": "Hyderabad",
            "state": "washington",
            "zipcode": "50008",
            "country": "United States"
        },
        "name": "Mtw Labs"
    },
    "settings": {
        "general": {
            "lockAccessNeeded": false,
            "bootAllUsersNeeded": false,
            "messageOnFrontPageNeeded": true,
            "messageOnFrontPage": "Hello Team"
        }
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Settings updated successfully",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */


/**
 * @api {post} /companies/sendVerificationEmail Send Verification email
 * @apiName sendVerificationEmail
 * @apiGroup Companies
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
 *
 {
	"email":"sai@mtwlabs.com",
	"name":"MTW Labs"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Verification mail sent",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */


/**
 * @api {post} /companies/verifyEmail Verifying email
 * @apiName verifyEmail
 * @apiGroup Companies
 *
 * @apiParamExample {json} Request-Example:
 *
 {
	"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.YzllMTE4OTRiMWQ3MjYzNzViMjMwNTgiLCJlbWFpbCI6InNhaUBtdHdsYWJzLmNvbSIsIm5hbWUiOiJNVFcgTGFicyIsImlhdCI6MTU1NDIwNjU0MSwiZXhwIjoxNTU0MjA4MzQxfQ.JFvzutDPjTknO8-XD2l2NWEwIDyA"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Successfully updated"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

//============================================================================================

/**
 * @api {put} /companies/updateCompanyDetails Update Company Settings
 * @apiName updateCompanyDetails
 * @apiGroup Companies
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
 *
{
	"companyDetails": {
        "primaryContact" : {
	        "name" : "bhaskar", 
	        "phone" : "1234567897"
    	}, 
        "employeCount" : 120, 
        "name": "Mtw Labs"
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Company details updated successfully",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */
