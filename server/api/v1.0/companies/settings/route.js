let express = require('express');

let router = express.Router();

let authMiddleware = require('../../middlewares/auth');

let CompanySettingsController = require('./controller');

let validationsMiddleware = require('../../middlewares/validations');

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();


router.post('/empIdentitySettings', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, authMiddleware.isCompany, CompanySettingsController.empIdentitySettings);


router.get('/getStandardSettings', authMiddleware.isUserLogin, authMiddleware.isCompany,
    CompanySettingsController.getStandardSettings);

router.put('/updateStandardSettings', authMiddleware.isUserLogin, authMiddleware.isCompany, CompanySettingsController.updateStandardSettings);

router.get(
    '/settingsOfMyInfoSections', 
    authMiddleware.isUserLogin, 
    // authMiddleware.isCompany, 
    CompanySettingsController.settingsOfMyInfoSections
);

router.get(
    '/settingsOfMyInfoTab/:tab', 
    authMiddleware.isUserLogin, 
    // authMiddleware.isCompany, 
    CompanySettingsController.settingsOfMyInfoTab
);


//policies
router.post('/uploadPolicy', authMiddleware.isUserLogin, authMiddleware.isCompany, multipartMiddleware, CompanySettingsController.uploadPolicy);

router.get('/singlePolicy/:_id', authMiddleware.isUserLogin, authMiddleware.isCompany, multipartMiddleware, CompanySettingsController.singlePolicy);

router.post('/editUploadPolicy', authMiddleware.isUserLogin, authMiddleware.isCompany, multipartMiddleware, CompanySettingsController.editUploadPolicy);

router.get('/getUploadPolicies', authMiddleware.isUserLogin, authMiddleware.isCompany, multipartMiddleware, CompanySettingsController.getUploadPolicies);

router.post('/deletePolicies', authMiddleware.isUserLogin, authMiddleware.isCompany, multipartMiddleware, CompanySettingsController.deletePolicies);


//templates
router.post('/uploadEmailTemplate', authMiddleware.isUserLogin, authMiddleware.isCompany, multipartMiddleware, CompanySettingsController.uploadEmailTemplate);

router.get('/singleEmailTemplate/:_id', authMiddleware.isUserLogin, authMiddleware.isCompany, multipartMiddleware, CompanySettingsController.singleEmailTemplate);

router.post('/editEmailTemplate', authMiddleware.isUserLogin, authMiddleware.isCompany, multipartMiddleware, CompanySettingsController.editEmailTemplate);

router.get('/getEmailTemplates', authMiddleware.isUserLogin, authMiddleware.isCompany, multipartMiddleware, CompanySettingsController.getEmailTemplates);

router.post('/deleteEmailTemplates', authMiddleware.isUserLogin, authMiddleware.isCompany, multipartMiddleware, CompanySettingsController.deleteEmailTemplates);





router.post('/addUpdateCustomField', authMiddleware.isUserLogin, authMiddleware.isCompany, CompanySettingsController.addUpdateCustomField);

router.get(
    '/getAllCustomFields', 
    authMiddleware.isUserLogin, 
    // authMiddleware.isCompany, 
    CompanySettingsController.getAllCustomFields
);

router.get('/getCustomField/:_id', authMiddleware.isUserLogin, authMiddleware.isCompany, CompanySettingsController.getCustomField);

router.post('/deleteCustomField', authMiddleware.isUserLogin, authMiddleware.isCompany, CompanySettingsController.deleteCustomField);


router.get(
    '/:companyId', 
    authMiddleware.isUserLogin, 
    // authMiddleware.isCompany, 
    CompanySettingsController.getSettingsByCompanyId
);

router.put('/:companyId', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, authMiddleware.isCompany, CompanySettingsController.updateSettingsByCompanyId);

module.exports = router;
