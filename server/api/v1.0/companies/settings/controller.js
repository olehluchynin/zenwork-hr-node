let mongoose = require('mongoose');
let CompanySettingsModel = require('./model');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../../config/config');
let utils = require('./../../../../common/utils');
let helpers = require('./../../../../common/helpers');
let constants = require('./../../../../common/constants');
let _ = require('underscore');

let s3 = require('./../../../../common/s3/s3');

let mailer = require('./../../../../common/mailer');

let getSettingsByCompanyId = (req,res)=>{
    var companyId  = req.params.companyId;
    let queryParams = req.query;
    queryParams.requiredFields = {};
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let options = {};
        if(queryParams && queryParams.requiredFields){
            // queryParams.requiredFields = JSON.parse(queryParams.requiredFields);
            options = queryParams.requiredFields;
            options.createdDate = 1;
            options.updatedDate = 1;
            options.companyId = 1;
        }
        return CompanySettingsModel.findOne({companyId:companyId},{});
    }).then((data) => {
        res.status(200).json({ status: true, message: "Fetched company settings successfully", data: data });
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

let updateSettingsByCompanyId = (req,res)=>{
    var companyId  = req.params.companyId;
    var body = req.body;
    return new Promise((resolve, reject) => {
        if (!companyId) reject("Invalid companyId");
        else resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOne({companyId:companyId});
    }).then(settings=>{
        if(!settings){
            res.status(400).json({ status: false, message: "Data not found", data: null });
            return null;
        }else{
            body.udpateById  = req.jwt.companyId;
            body.updatedDate = new Date();
            return CompanySettingsModel.update({companyId:companyId},body); 
        }
    }).then(updated=>{
        if(updated){
            res.status(200).json({status:true,message:"Updated successfully",data:null});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
};


/**
 * Author:Sai Reddy  Date:04/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To Update Employee Identity Settings
 */
let empIdentitySettings = (req, res) => {
    console.log(req.body);
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOneAndUpdate({companyId:req.jwt.companyId},{$set:req.body}).exec();
    }).then((data) => {
        if(data) {
            res.status(200).json({status: true, message: "Successfully updated"});
        } else {
            res.status(404).json({status: false, message: "Compnay not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:08/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To GET Standard Settings
 */
let getStandardSettings = (req, res) => {
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOne({companyId:req.jwt.companyId},{myInfo_sections:1,companyId:1}).exec();
    }).then((data) => {
        if(data) {
            res.status(200).json({status: true, message: "Success", data});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:08/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To Update Standard Settings
 */
let updateStandardSettings = (req, res) => {
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        for(let key in req.body.myInfo_sections) {
            // console.log(key);
            for(let field in req.body.myInfo_sections[key].fields) {
                // console.log('=========> ',field);
                req.body.myInfo_sections[key].fields[field].show = false;
            }
        }
        // console.log(req.body.myInfo_sections.Emergency_Contact.fields);
        return CompanySettingsModel.findOneAndUpdate({companyId:req.jwt.companyId},{myInfo_sections:req.body.myInfo_sections}).exec();
    }).then((data) => {
        if(data) {
            res.status(200).json({status: true, message: "Updated successfully"});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:08/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To GET Settings of all sections
 */
let settingsOfMyInfoSections = (req, res) => {
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOne({companyId:req.jwt.companyId},{myInfo_sections:1}).lean().exec();
    }).then((data) => {
        if(data) {
            let output = {};
            delete data._id;
            for(let key in data.myInfo_sections) {
                if(data.myInfo_sections.hasOwnProperty(key)) {
                    // console.log(key);
                    delete data.myInfo_sections[key].fields;
                    // for(let field in req.body.myInfo_sections[key].fields) {
                    // console.log('=========> ',field);
                    // req.body.myInfo_sections[key].fields[field].show = false;
                    // }
                }
            }
            res.status(200).json({status: true, message: "Success", data});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/*
let obj = {
    myInfo_sections: {
        "Personal": {
            "fields":{
                "First_Name":{
                    show: true
                }
            },
            hide: false
        }
    }
}

let toChange = 'First_Name';
// let toChange = 'Personal';

obj.myInfo_sections.Personal.fields[toChange].show = !obj.myInfo_sections.Personal.fields[toChange].show;
// obj.myInfo_sections[toChange].hide= !obj.myInfo_sections.[toChange].hide;

console.log(obj.myInfo_sections.Personal.fields);*/


/**
 * Author:Sai Reddy  Date:08/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To GET Settings of single tab
 */
let settingsOfMyInfoTab = (req, res) => {
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        let str = 'myInfo_sections.'+req.params.tab;
        return CompanySettingsModel.findOne({companyId:req.jwt.companyId},{myInfo_sections:1}).exec();
    }).then((data) => {
        if(data) {
            data = data['myInfo_sections'][req.params.tab];
            res.status(200).json({status: true, message: "Success",data});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let uploadPolicy = (req, res) => {
    console.log(req.body);
    console.log(req.files);
    let _id;
    let file;
    let s3Path;

    return new Promise( (resolve, reject) => {
        if(!req.files.file) reject("File not found.");
        else resolve(CompanySettingsModel.findOne({companyId: req.jwt.companyId, 'companyPolicy.givenFileName': req.body.givenFileName}));
    }).then(company_policy_with_name => {
        if (company_policy_with_name) {
            throw 'Company policy already exists with this name';
        }
        else {
            _id = mongoose.Types.ObjectId();
            file = req.files.file;
            s3Path = 'company_policies/'+_id+'__'+file.originalFilename;
            return s3.uploadFile(s3Path,file.path);
        }
    }).then(() => {
        let body = req.body;
        let doc = {
            _id: _id,
            originalFilename: file.originalFilename,
            givenFileName: body.givenFileName,
            s3Path: _id+'__'+file.originalFilename
        };
        return CompanySettingsModel.findOneAndUpdate({companyId:req.jwt.companyId},{$push:{companyPolicy:doc}}).exec()
    }).then((data) => {
        if(data) {
            res.status(200).json({status: true, message: "Updated Successfully"});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:10/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To get single Policy document
 */
let singlePolicy = (req, res) => {
    let body = req.body;
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOne({companyId:req.jwt.companyId},{companyPolicy:{ $elemMatch: { _id: req.params._id } }}).exec();
    }).then((data) => {
        if(data) {
            data = data.companyPolicy[0];
            res.status(200).json({status: true, message: "Success",data});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let editUploadPolicy = (req, res) => {
    // console.log(req.body);
    // console.log(req.files);
    let body = req.body;
    let file;
    let s3Path;

    return new Promise( (resolve, reject) => {
        /*if(!req.files.file) reject("File not found.");
        else */resolve(CompanySettingsModel.findOne({companyId: req.jwt.companyId}));
    }).then(settings => {
        let policies_with_name = settings.companyPolicy.filter(x => x.givenFileName == body.givenFileName)
        if (policies_with_name.length > 1) {
            throw 'Company policy already exists with this name'
        }
        else if(policies_with_name.length == 1){
            if(String(policies_with_name[0]._id) != String(body._id)) {
                throw 'Company policy already exists with this name'
            }
        }

        if(req.files.file) {
            file = req.files.file;
            s3Path = 'company_policies/'+body._id+'__'+file.originalFilename;
            body.originalFilename = file.originalFilename;
            body.s3Path = body._id+'__'+file.originalFilename;
            return Promise.all([s3.deleteFile('company_policies/' + body.s3Path),s3.uploadFile(s3Path,file.path)]);
        } else {
            return null;
        }
    }).then(updated => {
        // console.log('body ',body);
        return CompanySettingsModel.updateOne({companyId:req.jwt.companyId,'companyPolicy._id':body._id},{$set:{'companyPolicy.$':body}}).exec();
    }).then((data) => {
        if(data) {
            // console.log('data', data);
            res.status(200).json({status: true, message: "Updated Successfully"});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:15/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-
 * Login API To get all Upload Policies
 */
let getUploadPolicies = (req, res) => {
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOne({companyId:req.jwt.companyId},{companyPolicy:1}).lean().exec();
    }).then((data) => {
        let queries = [];
        data.companyPolicy.forEach(function (each) {
            each.isChecked = false;
            queries.push(s3.getPresignedUrlOfAfile('zenworkhr-docs', 'company_policies', each))
        });
        return Promise.all(queries);
        // return [data,Promise.all(queries)];
    }).then((data) => {
        if(data) {
            res.status(200).json({status: true, message: "Success",data});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:15/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-
 * Login API To delete Policy
 */
let deletePolicies = (req, res) => {
    let body = req.body;
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOneAndUpdate({companyId:req.jwt.companyId},{$pull:{companyPolicy:{_id:{$in:body}}}}).exec();
    }).then((data) => {
        let queries = [];
        data.companyPolicy.forEach(function (each) {
            queries.push(s3.deleteFile('company_policies/'+each.s3Path))
        });
        return Promise.all(queries);
        // return [data,Promise.all(queries)];
    }).then((data) => {
        if(data) {
            res.status(200).json({status: true, message: "Success"});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};


/**
 * Author:Sai Reddy  Date:10/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-
 * Login API To get single Email template
 */
let uploadEmailTemplate = (req, res) => {
    console.log(req.body);
    console.log(req.files);
    let _id;
    let file;
    let s3Path;

    return new Promise( (resolve, reject) => {
        let filter = {companyId: req.jwt.companyId, 'emailTemplates.module': req.body.module, 'emailTemplates.givenFileName': req.body.givenFileName}
        console.log('filter', filter)
        if(!req.files.file) reject("File not found.");
        else resolve(CompanySettingsModel.findOne({companyId: req.jwt.companyId}));
        // else resolve(null);
    }).then(settings => {
        let email_templates_with_module = settings.emailTemplates.filter(x => x.module == req.body.module)
        let email_templates_with_name = email_templates_with_module.filter(x => x.givenFileName == req.body.givenFileName)
        if (email_templates_with_name.length) {
            throw 'Email template already exists with this name';
        }
        else {
            _id = mongoose.Types.ObjectId();
            file = req.files.file;
            s3Path = 'email_templates/'+_id+'__'+file.originalFilename;
            return s3.uploadFile(s3Path,file.path);
        }
    }).then(() => {
        let body = req.body;
        let doc = {
            _id: _id,
            originalFilename: file.originalFilename,
            givenFileName: body.givenFileName,
            module: body.module,
            s3Path: _id+'__'+file.originalFilename
        };
        return CompanySettingsModel.findOneAndUpdate({companyId:req.jwt.companyId},{$push:{emailTemplates:doc}}).exec()
    }).then((data) => {
        if(data) {
            res.status(200).json({status: true, message: "Updated Successfully"});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:10/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-
 * Login API To get single Policy document
 */
let singleEmailTemplate = (req, res) => {
    let body = req.body;
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOne({companyId:req.jwt.companyId},{emailTemplates:{ $elemMatch: { _id: req.params._id } }}).exec();
    }).then((data) => {
        if(data) {
            data = data.emailTemplates[0];
            res.status(200).json({status: true, message: "Success",data});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let editEmailTemplate = (req, res) => {
    let body = req.body;
    let file;
    let s3Path;

    return new Promise( (resolve, reject) => {
        let filter  = {
            companyId: req.jwt.companyId, 
            'emailTemplates.module': body.module, 
            'emailTemplates.givenFileName': body.givenFileName, 
            'emailTemplates._id': {$ne: ObjectID(body._id)}    
        }
        console.log('filter - ', filter)
        /*if(!req.files.file) reject("File not found.");
        else */resolve(CompanySettingsModel.findOne({companyId: req.jwt.companyId}));
    }).then(settings => {
        let email_templates_with_module = settings.emailTemplates.filter(x => x.module == body.module)
        let email_templates_with_name = email_templates_with_module.filter(x => x.givenFileName == body.givenFileName)

        if (email_templates_with_name.length > 1) {
            throw 'Email template already exists with this name'
        }
        else if(email_templates_with_name.length == 1){
            if(String(email_templates_with_name[0]._id) != String(body._id)) {
                throw 'Email template already exists with this name'
            }
        }

        if(req.files.file) {
            file = req.files.file;
            s3Path = 'email_templates/'+body._id+'__'+file.originalFilename;
            body.originalFilename = file.originalFilename;
            body.s3Path = body._id+'__'+file.originalFilename;
            return Promise.all([s3.deleteFile('email_templates/' + body.s3Path),s3.uploadFile(s3Path,file.path)]);
        } else {
            return null;
        }
    }).then(updated => {
        console.log('updated ',updated);
        return CompanySettingsModel.updateOne({companyId:req.jwt.companyId,'emailTemplates._id':body._id},{$set:{'emailTemplates.$':body}}).exec();
    }).then((data) => {
        if(data) {
            // console.log('data', data);
            res.status(200).json({status: true, message: "Updated Successfully"});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:15/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-
 * Login API To get all Upload Policies
 */
let getEmailTemplates  = (req, res) => {
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOne({companyId:req.jwt.companyId},{emailTemplates:1}).lean().exec();
    }).then((data) => {
        let queries = [];
        data.emailTemplates.forEach(function (each) {
            each.isChecked = false;
            queries.push(s3.getPresignedUrlOfAfile('zenworkhr-docs', 'email_templates', each))
        });
        return Promise.all(queries);
        // return [data,Promise.all(queries)];
    }).then((data) => {
        if(data) {
            let grouped = _.groupBy(data, function (each) {
                return each.module;
            });
            res.status(200).json({status: true, message: "Success",grouped});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:15/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-
 * Login API To delete Policy
 */
let deleteEmailTemplates = (req, res) => {
    let body = req.body;
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOneAndUpdate({companyId:req.jwt.companyId},{$pull:{emailTemplates:{_id:{$in:body}}}}).exec();
    }).then((data) => {
        let queries = [];
        data.companyPolicy.forEach(function (each) {
            queries.push(s3.deleteFile('email_templates/'+each.s3Path))
        });
        return Promise.all(queries);
        // return [data,Promise.all(queries)];
    }).then((data) => {
        if(data) {
            res.status(200).json({status: true, message: "Success"});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};




/**
 * Author:Sai Reddy  Date:10/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To Add or Update Custom fields
 */
let addUpdateCustomField = (req, res) => {
    let body = req.body;
    return new Promise( (resolve, reject) => {
        if (!body.fieldName) reject("Enter a Field Name");
        else if (!body.fieldType) reject("Enter a Field Type");
        else resolve(null);
    }).then(() => {
        let queries = [];
        body.show = false;
        queries.push(new Promise((resolve, reject) => {
            if(body._id) {
                CompanySettingsModel.updateOne({companyId:req.jwt.companyId,'customFields._id':body._id},{$set:{'customFields.$':body}}).exec()
                    .then((response) => {
                        resolve(response);
                    }).catch((err) => {
                    console.log("==> ",err);
                    reject(err);
                });
            } else {
                body.createdAt = new Date();
                CompanySettingsModel.findOneAndUpdate({companyId:req.jwt.companyId},{$push:{customFields:body}}).exec()
                    .then((response) => {
                        resolve(response);
                    }).catch((err) => {
                    console.log("==> ",err);
                    reject(err);
                });
            }
        }));
        return Promise.all(queries);
        // return CompanySettingsModel.findOneAndUpdate({companyId:req.jwt.userId},{$push:{customFields:req.body}}).exec();
    }).then((data) => {
        if(data) {
            // data = data['myInfo_sections'][req.params.tab];
            res.status(200).json({status: true, message: "Updated Successfully"});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:10/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To get all Custom fields
 */
let getAllCustomFields = (req, res) => {
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOne({companyId:req.jwt.companyId},{customFields:1}).exec();
    }).then((data) => {
        if(data) {
            res.status(200).json({status: true, message: "Success",data});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:10/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To get single Custom field details
 */
let getCustomField = (req, res) => {
    let body = req.body;
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOne({companyId:req.jwt.companyId},{customFields:{ $elemMatch: { _id: req.params._id } }}).exec();
    }).then((data) => {
        if(data) {
            data = data.customFields[0];
            res.status(200).json({status: true, message: "Success",data});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:10/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To delete Custom fields
 */
let deleteCustomField = (req, res) => {
    let body = req.body;
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return CompanySettingsModel.findOneAndUpdate({companyId:req.jwt.companyId},{$pull:{customFields:{_id:{$in:body}}}}).exec();
    }).then((data) => {
        if(data) {
            // data = data['myInfo_sections'][req.params.tab];
            res.status(200).json({status: true, message: "Successfully deleted"});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

module.exports = {
    getSettingsByCompanyId,
    updateSettingsByCompanyId,
    empIdentitySettings,
    getStandardSettings,
    updateStandardSettings,
    settingsOfMyInfoSections,
    settingsOfMyInfoTab,

    uploadPolicy,
    singlePolicy,
    editUploadPolicy,
    getUploadPolicies,
    deletePolicies,

    uploadEmailTemplate,
    singleEmailTemplate,
    editEmailTemplate,
    getEmailTemplates,
    deleteEmailTemplates,

    addUpdateCustomField,
    getAllCustomFields,
    getCustomField,
    deleteCustomField
};
