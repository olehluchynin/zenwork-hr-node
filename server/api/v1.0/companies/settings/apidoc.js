/**
 * @apiDefine DataNotFoundError
 *
 * @apiError DataNotFound The data the you are requested was not found.
 *
 * @apiErrorExample Data Not Found Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "message": "Data Not Found"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine InternalServerError
 *
 * @apiError InternalServerError Internal Server error.
 *
 * @apiErrorExample Internal Server Error:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "status": false,
 *       "message": "Internal server error"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine TokenNotFound
 *
 * @apiError TokenNotFound User Token Not Found.
 *
 * @apiErrorExample Token Not Found:
 *     HTTP/1.1 400 Token Not Found
 *     {
 *       "status": false,
 *       "message": "Token Not Found"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine BadRequest
 *
 * @apiError BadRequest Bad request.
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": false,
 *       "message": "Invalid key"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine AccessDeneid
 *
 * @apiError AccessDenied Access denied for this operation.
 *
 * @apiErrorExample Access Deneid:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "status": false,
 *       "message": "Access Denied for this operation"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine DataAlreadyExists
 *
 * @apiError DataConflict Date already exists.
 *
 * @apiErrorExample Data Conflict Error:
 *     HTTP/1.1 402 Data Conflict
 *     {
 *       "status": false,
 *       "message": "Data already exist"
 *       "data": null 
 *     }
 */

 /**
 * @api {get} /companies/settings/:companyId Get company settings
 * @apiName GetCompanySettingsData
 * @apiGroup Company Settings
 * @apiHeader {String} x-access-token User login token.
*
 * @apiParam {String} companyId company unique _id.
 * @apiParam {String} requiredFields key value pair of required keys (each with values 1) *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Fetched company settings successfully",
    "data": {
        "general": {
            "lockAccessNeeded": false,
            "bootAllUsersNeeded": false,
            "messageOnFrontPageNeeded": false,
            "messageOnFrontPage": "Sample message"
        },
        "passwordRequirement": {
            "numeric": false,
            "symbol": false,
            "uppercase": false,
            "minimumCharacters": 3
        },
        "security": {
            "twoFactorAuthNeeded": false,
            "maxAttemsToLockId": 3,
            "enableForgotId": false,
            "enableForgotPassword": false
        },
        "administratorContact": {
            "workPhone": "987654321",
            "extention": "012",
            "mobile": "9876543211",
            "homePhone": "9123456789"
        },
        "administratorAddress": {
            "address1": "add1",
            "address2": "add2",
            "city": "Hyd",
            "state": "Tg",
            "zipcode": "502267",
            "country": "India"
        },
        "createdDate": "2018-12-31T07:09:43.292Z",
        "updatedDate": "2018-12-31T10:06:02.573Z",
        "_id": "5c29c46fc9aa8a1b2c08a689",
        "createdBy": "5c126409d84721019cf167be",
        "__v": 0,
        "companyId": "5c29c46fc9aa8a1b2c08a688",
        "homeEmail": "test@gmail.com",
        "workEmail": "test@gmail.com"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

 /**
 * @api {put} /companies/settings/:companyId Update company settings
 * @apiName UpdateCompanySettings
 * @apiGroup Company Settings
 * 
 * @apiParam {String} companyId company unique _id.
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
 {
    "general": {
        "lockAccessNeeded": false,
        "bootAllUsersNeeded": false,
        "messageOnFrontPageNeeded": false,
        "messageOnFrontPage":"Sample message"
    },
    "passwordRequirement": {
        "minimumCharacters": 3,
        "numeric": false,
        "symbol": false,
        "uppercase": false
    },
    "security": {
        "maxAttemsToLockId": 3,
        "twoFactorAuthNeeded": false,
        "enableForgotId": false,
        "enableForgotPassword": false
    },
    //Contact information
    "administratorContact": {
        "workPhone": "987654321",
        "extention": "012",
        "mobile": "9876543211",
        "homePhone": "9123456789"
    },
    "workEmail": "test@gmail.com",
    "homeEmail": "test@gmail.com",
    "administratorAddress": {
        "address1": "add1",
        "address2": "add2",
        "city": "Hyd",
        "state": "Tg",
        "zipcode": "502267",
        "country": "India"
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Settings updated successfully",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /companies/settings/empIdentitySettings Update Identity settings
 * @apiName empIdentitySettings
 * @apiGroup Company Settings
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
 *
 {
	"maskSSN":false,                        //[true, false]
	"idNumber":{
		"numberType":"System Generated",    //["System Generated", "Manual"]
		"digits":5,                         //[3,4,5,6]
		"firstNumber":16000
	}
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Successfully updated"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

/**
 * @api {get} /companies/settings/getStandardSettings Get Standard settings
 * @apiName getStandardSettings
 * @apiGroup Company Settings
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Success",
    "data": {
        "myInfo_sections": {
            "Personal": {
                "fields": {
                    "First_Name": {
                        "show": false,
                        "format": "Alpha",
                        "hide": true,
                        "required": true
                    },
                    "Middle_Name": {
                        "show": false,
                        "format": "Alpha",
                        "hide": true,
                        "required": true
                    }
                },
                "hide": true
            },
            "Time_Off": {
                "hide": true
            }
        },
        "_id": "5c9e11894b1d726375b23059",
        "companyId": "5c9e11894b1d726375b23058"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

/**
 * @api {put} /companies/settings/updateStandardSettings Update Standard settings
 * @apiName updateStandardSettings
 * @apiGroup Company Settings
 *
 * @apiParam {String} companyId company unique _id.
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
 *
    {
    "myInfo_sections": {
        "Personal": {
            "fields": {
                "First_Name": {
                    "show": false,
                    "format": "Alpha",
                    "hide": true,
                    "required": true
                },
                "Middle_Name": {
                    "show": false,
                    "format": "Alpha",
                    "hide": true,
                    "required": true
                }
            },
            "hide": true
        },
        "Time_Off": {
            "hide": true
        }
    }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Updated successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

/**
 * @api {get} /companies/settings/settingsOfMyInfoSections Get settings of my info sections
 * @apiName settingsOfMyInfoSections
 * @apiGroup Company Settings
 * @apiHeader {String} x-access-token User login token.
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Success",
    "data": {
        "myInfo_sections": {
            "Personal": {
                "hide": true
            },
            "Job": {
                "hide": true
            },
            "Time_Off": {
                "hide": true
            },
            "Emergency_Contact": {
                "hide": true
            },
            "Performance": {
                "hide": true
            },
            "Compensation": {
                "hide": true
            },
            "Notes": {
                "hide": true
            },
            "Benefits": {
                "hide": true
            },
            "Training": {
                "hide": true
            },
            "Documents": {
                "hide": true
            },
            "Assets": {
                "hide": true
            },
            "Onboarding": {
                "hide": true
            },
            "Offboarding": {
                "hide": true
            }
        }
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

/**
 * @api {get} /companies/settings/settingsOfMyInfoTab/:tab Get single tab settings for my info
 * @apiName settingsOfMyInfoTab
 * @apiGroup Company Settings
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParam {String} tab myinfo tab name.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Success",
    "data": {
        "fields": {
            "First_Name": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            },
            "Middle_Name": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            },
            "Last_Name": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            },
            "Preferred_Name": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            },
            "Salutation": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            },
            "Date_of_Birth": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            },
            "Gender": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            },
            "Maritial_Status": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            },
            "Maritial_Status_Effective_Date": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            },
            "Veteran_Status": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            },
            "SSN": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            },
            "Race": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            },
            "Ethnicity": {
                "show": false,
                "format": "Alpha",
                "hide": true,
                "required": true
            }
        },
        "hide": true
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */


/**
 * @api {post} /companies/settings/addUpdateCustomField Add/Update Custom Fields
 * @apiName addUpdateCustomField
 * @apiGroup Company Settings
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
 *
 {
	"fieldName":"Name!!",
	"fieldType":"Text!!",   //any one in ['Text','Numeric','Yes/No','Dropdown', 'Date']
	"dropDownOptions":[],
	"fieldCode":"c1!!",
	"hide":false,
	"required":true
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Updated Successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

/**
 * @api {get} /companies/settings/getAllCustomFields Get all Custom Fields
 * @apiName getAllCustomFields
 * @apiGroup Company Settings
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Success",
    "data": {
        "_id": "5c9e11894b1d726375b23059",
        "customFields": [
            {
                "dropDownOptions": [],
                "fieldLocation": "Custom",
                "show": false,
                "hide": false,
                "required": true,
                "_id": "5cadcddf23f5b94ea6bdd80f",
                "fieldName": "Name!!",
                "fieldType": "Text!!",
                "fieldCode": "c1!!",
                "createdAt": "2019-04-10T11:05:03.796Z"
            }
        ]
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

/**
 * @api {get} /companies/settings/getCustomField/:_id Get Single Custom Field Details
 * @apiName getCustomField
 * @apiGroup Company Settings
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParam {String} _id field _id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Success",
    "data": {
        "dropDownOptions": [],
        "fieldLocation": "Custom",
        "show": false,
        "hide": false,
        "required": true,
        "_id": "5cadcddf23f5b94ea6bdd80f",
        "fieldName": "Name!!",
        "fieldType": "Text!!",
        "fieldCode": "c1!!",
        "createdAt": "2019-04-10T11:05:03.796Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

/**
 * @api {post} /companies/settings/deleteCustomField/ Delete Custom Fields
 * @apiName deleteCustomFields
 * @apiGroup Company Settings
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
 *
 * ["5cb16ec3ca37e91ab629c2ef","5cb16ec5ca37e91ab629c2f0"]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Successfully deleted"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */
