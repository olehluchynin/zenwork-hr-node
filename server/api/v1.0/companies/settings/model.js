let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants = require('./../../../../common/constants');

let Schema = new mongoose.Schema({
    companyId: { type: ObjectID, ref: 'companies' },
    createdBy: { type: ObjectID, ref: 'companies' },

    //General settings
    general: {
        lockAccessNeeded: { type: Boolean,default:false },
        bootAllUsersNeeded: { type: Boolean,default:false },
        messageOnFrontPageNeeded: { type: Boolean,default:false },
        messageOnFrontPage: { type: String, default:"" }
    },
    passwordRequirement: {
        minimumCharacters: { type: Number },
        numeric: { type: Boolean,default:false },
        symbol: { type: Boolean,default:false },
        uppercase: { type: Boolean,default:false }
    },
    security: {
        maxAttemsToLockId: { type: Number },
        twoFactorAuthNeeded: { type: Boolean,default:false },
        enableForgotId: { type: Boolean },
        enableForgotPassword: { type: Boolean },
    },
    //Contact information
    details: {
        companyName: String,
        fein: String,
        companyWebsite: String,
        adminContactName: String
    },
    administratorContact: {
        workPhone: String,
        extention: String,
        mobile: String,
        homePhone: String
    },
    workEmail: String,
    homeEmail: String,
    administratorAddress: {
        address1: String,
        address2: String,
        city: String,
        state: String,
        zipcode: String,
        country: String
    },

    maskSSN: {type: Boolean},
    idNumber: {
        numberType: {type: String, enum:constants.idNumberType},
        digits: {type: Number},
        // firstNumber: {type: Number}
        firstNumber: {type: String}
    },

    companyPolicy: [{
        originalFilename: {type: String},   //reference name
        givenFileName: {type: String},      //file name
        s3Path: {type: String}
    }],

    emailTemplates: [{
        originalFilename: {type: String},   //reference name
        givenFileName: {type: String},      //file name
        s3Path: {type: String},
        module: {type: String, enum:['Employee Management','Recruitment','Benefits Administration','Performance Management', 'Leave Management']}
    }],

    customFields: [{
        fieldName: {type: String},
        fieldType: {type: String, enum:['Text','Numeric','Yes/No','Dropdown', 'Date']},
        dropDownOptions: {type: Array, default:[]},
        fieldLocation: {type: String, default:"Custom", enum:['Custom']},
        fieldCode: {type: String},

        show: {type: Boolean, default: false},  //for frontend usage
        hide: {type: Boolean, default: false},
        required: {type: Boolean, default: true},

        createdAt: {type: Date}
    }],

    myInfo_sections: {
        Personal: {
            hide: {type: Boolean, default: false},
            fields: {
                First_Name: {
                    show: {type: Boolean, default: false},  //for frontend usage
                    format: {type: String, default: 'Alpha', enum:['Alpha', 'Numeric', 'AlphaNumeric', 'Dropdown']},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Middle_Name: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Last_Name: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Preferred_Name: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Salutation: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Date_of_Birth: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Gender: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Maritial_Status: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Maritial_Status_Effective_Date: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Veteran_Status: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                SSN: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Race: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Ethnicity: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                }
            }
        },
        Job: {
            hide: {type: Boolean, default: false},
            fields: {
                Employee_Id: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Employee_Type: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Employee_Self_Service_Id: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Work_Location: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Reporting_Location: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Hire_Date: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Rehire_Date: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Termination_Date: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Termination_Reason: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Rehire_Status: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Archive_Status: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                FLSA_Code: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                EEO_Job_Category: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Vendor_Id: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Specific_Workflow_Approval: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Site_AccessRole: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Type_of_User_Role_type: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Custom_1: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Custom_2: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                }
            }
        },
        Time_Off: {
            hide: {type: Boolean, default: false}
        },
        Emergency_Contact: {
            hide: {type: Boolean, default: false},
            fields: {
                Name: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Relationship: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Work_Phone: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Ext: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Mobile_Phone: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Home_Phone: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Work_Email: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Home_Email: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Street_1: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Street_2: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                City: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                State: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Zip: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
            }
        },
        Performance: {
            hide: {type: Boolean, default: false}
        },
        Compensation: {
            hide: {type: Boolean, default: false},
            fields: {
                Nonpaid_Record: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Highly_compensated_Employee_type: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Pay_group: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Pay_frequency: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Salary_Grade: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                },
                Compensation_ratio: {
                    show: {type: Boolean, default: false},
                    format: {type: String, default: 'Alpha'},
                    hide: {type: Boolean, default: false},
                    required: {type: Boolean, default: false}
                }
            }
        },
        Notes: {
            hide: {type: Boolean, default: false}
        },
        Benefits: {
            hide: {type: Boolean, default: false}
        },
        Training: {
            hide: {type: Boolean, default: false}
        },
        Documents: {
            hide: {type: Boolean, default: false}
        },
        Assets: {
            hide: {type: Boolean, default: false}
        },
        Onboarding: {
            hide: {type: Boolean, default: false}
        },
        Offboarding: {
            hide: {type: Boolean, default: false}
        }
    }

    // createdDate: { type: Date, default: Date.now() },
    // updatedDate: { type: Date, default: Date.now() },
},{
    timestamps: true,
    versionKey: false
});
Schema.plugin(autoIncrement.plugin, {model:'company_settings', field:'company_settingsId', startAt: 1, incrementBy: 1});


let CampanySettingsModel = mongoose.model('company_settings', Schema);


module.exports = CampanySettingsModel;

// {
//     "general": {
//         "lockAccessNeeded": false,
//         "bootAllUsersNeeded": false,
//         "messageOnFrontPageNeeded": false,
//         "messageOnFrontPage":"Sample message"
//     },
//     "passwordRequirement": {
//         "minimumCharacters": 3,
//         "numeric": false,
//         "symbol": false,
//         "uppercase": false
//     },
//     "security": {
//         "maxAttemsToLockId": 3,
//         "twoFactorAuthNeeded": false,
//         "enableForgotId": false,
//         "enableForgotPassword": false
//     },
//     //Contact information
//     "administratorContact": {
//         "workPhone": "987654321",
//         "extention": "012",
//         "mobile": "9876543211",
//         "homePhone": "9123456789"
//     },
//     "workEmail": "test@gmail.com",
//     "homeEmail": "test@gmail.com",
//     "administratorAddress": {
//         "address1": "add1",
//         "address2": "add2",
//         "city": "Hyd",
//         "state": "Tg",
//         "zipcode": "502267",
//         "country": "India"
//     }
// }
