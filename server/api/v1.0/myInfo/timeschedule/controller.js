/**
 * This file contains methods for schedule time off in my-info
 * 
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso (08-07-2019)
 */

let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;

let scheduleTimeOffCollection = require('./model').schedule_timeoffModel;
let leaveEliGroupCollection = require('./../../leaveManagement/leaveEligibilityGroup/model').leaveEligibility;
let userCollection = require('./../../users/model');
let timeOffPoliciesCollection = require('./../../leaveManagement/TimeOffPolicies/model').timePolicy;

var timeoffStatusOptions = {
    "pending": "Pending",
    "approved": "Approved",
    "rejected": "Rejected"
};

/**
 * Function to Get current Leave Eligibility group applicable for employee
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso(08 July 2019)
 */
let getCurrentLeaveElibGroup = ((req, res) => {
    var allParams = req.params;

    return new Promise((resolve, reject) => {
        if (!allParams.userId) {
            reject("User Id is mandatory");
        }

        var allFilters = {
            _id: ObjectID(allParams.userId)
        };

        var valuesToSelect = {
            leave_Eligibility_id: true,
            leave_EligGrp_EffectDate: true
        }
        userCollection.findOne(allFilters, valuesToSelect).populate("leave_Eligibility_id")
            .then((employeeDetails) => {
                if (!employeeDetails) {
                    reject("Leave Eligibility group not found");
                } else {
                    res.status(200).json({ status: true, message: "Leave Eligibility group details", data: employeeDetails });
                }
            })

    })
        .catch((err) => {
            res.status(400).json({ status: false, message: err, data: null });
        })
});

/**
* Function to change current Leave Eligibility group for employee and add old to history
* @param {*} req 
* @param {*} res 
* @author : Surendra Waso(08 July 2019)
*/
let changeEligibilityGroup = ((req, res) => {
    var allParams = req.body;

    return new Promise((resolve, reject) => {
        if (!allParams.userId) {
            reject("userId is mandatory.");
        }

        if (!allParams.newEligibilityGroup) {
            reject("newEligibilityGroup is mandatory");
        }

        if (!allParams.updatedBy) {
            reject("updatedBy is mandatory");
        }

        var allFilters = {
            _id: allParams.userId
        };

        userCollection.findOne(allFilters)
            .then((employeeDetails) => {
                if (!employeeDetails) {
                    reject("Requested employee does not exist");
                } else {
                    if (employeeDetails.leave_Eligibility_id == allParams.newEligibilityGroup) {
                        reject("Requested Eligibility group is already assigned to this employee");
                    } else {
                        allFilters = {
                            _id: allParams.newEligibilityGroup
                        }

                        leaveEliGroupCollection.findOne(allFilters)
                            .then((leaveEliGroupDetails) => {
                                if (!leaveEliGroupDetails) {
                                    reject("Requested leave eligibilty group does not exist");
                                } else {
                                    /**
                                     * ToDo : Check all conditions for validation like effective date,etc
                                     */
                                    userCollection.findByIdAndUpdate(employeeDetails._id, {
                                        $set: {
                                            leave_Eligibility_id: ObjectID(allParams.newEligibilityGroup),
                                            leave_EligGrp_EffectDate: new Date(),
                                            updatedBy: allParams.updatedBy
                                        },
                                        $push: {
                                            leave_Eligibility_group_history: {
                                                leave_Eligibility_id: ObjectID(allParams.newEligibilityGroup),
                                                leave_EligGrp_EffectDate: employeeDetails.leave_EligGrp_EffectDate,
                                                leave_EligGrp_EndDate: new Date()
                                            }
                                        }
                                    })
                                        .then((updateDetails) => {
                                            res.status(200).json({ status: true, message: "Leave Eligibility group is updated", data: updateDetails })
                                        })
                                }
                            })
                    }
                }
            })
    })
        .catch((err) => {
            res.status(400).json({ status: false, message: err, data: null });
        })
});

/**
 * Function to get Leave Eligibility group history for employee
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso(08 July 2019)
 */
let getLeaveEligGroupHistory = ((req, res) => {
    var allParams = req.params;

    return new Promise((resolve, reject) => {
        if (!allParams.userId) {
            reject("userId is mandatory");
        }

        var allFilters = {
            _id: allParams.userId
        };

        var valuesToSelect = {
            leave_Eligibility_group_history: true,
            _id: false
        }
        userCollection.findById(allFilters, valuesToSelect).populate("leave_Eligibility_group_history.leave_Eligibility_id")
            .then((leaveEligGrpHistory) => {
                if (leaveEligGrpHistory) {
                    res.status(200).json({ status: true, message: "Leave Eligibility group history ", data: leaveEligGrpHistory });
                } else {
                    res.status(200).json({ status: true, message: "History is not available for requested employee ", data: leaveEligGrpHistory })
                }
            })
    })
        .catch((err) => {
            res.status(400).json({ status: false, message: err, data: null });
        })

});

/**
* Function to Get Policy types applicable for an employee
* @param {*} req 
* @param {*} res 
* @author : Surendra Waso(08 July 2019)
*/
let getScheduleTimeoffPolicies = ((req, res) => {
    var allParams = req.params;

    return new Promise((resolve, reject) => {
        if (!allParams.userId) {
            reject("User Id is mandatory");
        }

        var allFilters = {
            _id: ObjectID(allParams.userId)
        }
        userCollection.findOne(allFilters)
            .then((employeeDetails) => {
                if (!employeeDetails) {
                    reject("Employee details not found");
                } else {
                    if (!employeeDetails.leave_Eligibility_id) {
                        reject("Employee doesn't have any leave Eligibility group attached");
                    } else {
                        console.log('employeeDetails.leave_Eligibility_id - ', employeeDetails.leave_Eligibility_id)
                        allFilters = {
                            leave_Eligibility_group: ObjectID(employeeDetails.leave_Eligibility_id),
                            policy_effective_date: {
                                $lte: new Date(new Date().setHours(0, 0, 0, 0))
                            }
                        };
                        var valuesToSelect = {
                            policytype: true,
                            expirationoptions: true
                        }
                        console.log("allFilters ", allFilters, valuesToSelect);
                        timeOffPoliciesCollection.find(allFilters, valuesToSelect)
                            .then((policyDetails) => {
                                console.log('policyDetails - ', policyDetails)
                                if (policyDetails.length <= 0) {
                                    res.status(200).json({ status: true, message: "No policy details available for the group", data: [] });
                                } else {
                                    /**
                                     * ToDo: Check Validations for effective date, hire date, negative balance, etc
                                     */
                                    var applicablePolicies = [];
                                    for (let i = 0; i < policyDetails.length; i++) {
                                        var isValidPolicy = true;
                                        if (new Date(policyDetails[i].policy_effective_date) >= new Date(new Date().setHours(0, 0, 0, 0))) {
                                            isValidPolicy = false;
                                        }
                                        if (policyDetails[i].expirationoptions.expiretime && new Date(policyDetails[i].expirationoptions.expiredate) < new Date(new Date().setHours(0, 0, 0, 0))) {
                                            isValidPolicy = false;
                                        }
                                        if (policyDetails[i].employee_policy_period.new_hire.hire_date.selected == true && new Date(policyDetails[i].employee_policy_period.new_hire.hire_date.date) >= new Date(employeeDetails.job.hireDate)) {
                                            isValidPolicy = false;
                                        }

                                        // if(policyDetails[i].employee_policy_period.new_hire.first_month)

                                        if (isValidPolicy) {
                                            applicablePolicies.push(policyDetails[i]);
                                        }

                                    }
                                    res.status(200).json({ status: true, message: "Policy details available for the group", data: policyDetails });
                                }
                            })
                    }
                }
            })

    })
        .catch((err) => {
            console.log(err)
            res.status(400).json({ status: false, message: err, data: null });
        })
})

/**
* Function to save schedule time-off request for an employee
* @param {*} req 
* @param {*} res 
* @author : Surendra Waso(09 July 2019)
*/
let saveScheduleTimeOffDetails = ((req, res) => {
    var allParams = req.body;

    return new Promise((resolve, reject) => {

        if (!allParams.timeoffEmployee) {
            reject("timeoffEmployee is mandatory");
        }
        if (!allParams.selectedPolicy) {
            reject("selectedPolicyType is mandatory");
        }
        if (!allParams.timeoffPolicyType) {
            reject("timeoffPolicyType is mandatory");
        }
        if (!allParams.timeoffStartDate) {
            reject("timeoffStartDate is mandatory");
        }
        if (!allParams.timeoffEndDate) {
            reject("timeoffEndDate is mandatory");
        }
        if (!allParams.timeOffStartTime) {
            reject("timeOffStartTime is mandatory");
        }
        if (!allParams.timeoffEndTime) {
            reject("timeoffEndTime is mandatory");
        }
        if (!allParams.createdBy) {
            reject("createdBy is mandatory");
        }

        var allFilters = {
            _id: ObjectID(allParams.timeoffEmployee)
        };
        userCollection.findOne(allFilters)
            .then((employeeDetails) => {
                if (!employeeDetails) {
                    reject("Employee not found");
                } else {
                    allFilters = {
                        timeoffEmployee: ObjectID(allParams.timeoffEmployee),
                        selectedPolicy: ObjectID(allParams.selectedPolicy),
                        timeoffStartDate: new Date(allParams.timeoffStartDate),
                        timeoffEndDate: new Date(allParams.timeoffEndDate)

                    };
                    scheduleTimeOffCollection.findOne(allFilters)
                        .then((existingRequest) => {
                            if (existingRequest) {
                                reject("Request already exist");
                            } else {
                                allFilters = {
                                    _id: ObjectID(allParams.selectedPolicy)
                                };
                                timeOffPoliciesCollection.findOne(allFilters)
                                    .then((policyDetails) => {
                                        if (!policyDetails) {
                                            reject("policy details not found");
                                        } else {
                                            var timeoffRequest = {
                                                timeoffEmployee: ObjectID(allParams.timeoffEmployee),
                                                selectedPolicy: ObjectID(allParams.selectedPolicy),
                                                timeoffPolicyType: allParams.timeoffPolicyType,
                                                timeoffStartDate: new Date(allParams.timeoffStartDate),
                                                timeoffEndDate: new Date(allParams.timeoffEndDate),
                                                timeOffStartTime: new Date(allParams.timeOffStartTime),
                                                timeoffEndTime: new Date(allParams.timeoffEndTime),
                                                managerId: ObjectID(employeeDetails.job.ReportsTo),
                                                companyId: ObjectID(employeeDetails.companyId),
                                                timeoffStatus: timeoffStatusOptions.pending,
                                                createdBy: ObjectID(allParams.createdBy),
                                                updatedBy: ObjectID(allParams.createdBy)
                                            };
                                            return Promise.all([scheduleTimeOffCollection.create(timeoffRequest)]);
                                        }
                                    })
                                    .then(([savedTimeoffDetails]) => {
                                        if (savedTimeoffDetails) {
                                            res.status(200).json({ status: true, message: "Schedule time-off saved successfully", data: savedTimeoffDetails });
                                        } else {
                                            res.status(200).json({ status: true, message: "Schedule time-off save failed", data: savedTimeoffDetails });
                                        }
                                    })
                            }
                        })

                }//old
            })
    })
        .catch((err) => {
            res.status(400).json({ status: false, message: err, data: null });
        })
})

/**
 * Function to update scheduled timeoff details or to Approve or Reject timeoff request for requested an employee
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso(09 July 2019)
 */
let updateScheduledTimeoffDetails = ((req, res) => {

    var allParams = req.body;

    return new Promise((resolve, reject) => {
        if (!allParams.timeoffRequestId) {
            reject("timeoffRequestId is mandatory");
        }
        if (!allParams.timeoffEmployee) {
            reject("timeoffEmployee is mandatory");
        }
        if (!allParams.timeoffStartDate) {
            reject("timeoffStartDate is mandatory");
        }
        if (!allParams.timeoffEndDate) {
            reject("timeoffEndDate is mandatory");
        }
        if (!allParams.timeOffStartTime) {
            reject("timeOffStartTime is mandatory");
        }
        if (!allParams.timeoffEndTime) {
            reject("timeoffEndTime is mandatory");
        }
        if (!allParams.timeoffStatus) {
            reject("timeoffStatus is mandatory");
        }
        if (!allParams.updatedBy) {
            reject("updatedBy is mandatory");
        }

        if (allParams.timeoffStatus && allParams.timeoffStatus !== timeoffStatusOptions.pending && allParams.timeoffStatus !== timeoffStatusOptions.rejected && allParams.timeoffStatus !== timeoffStatusOptions.approved) {
            reject("Invalid option for timeoffStatus");
            return;
        }

        if (new Date(allParams.timeoffStartDate) < new Date() || new Date(allParams.timeoffEndDate) < new Date()) {
            reject("scheduled time-off date is in past, not acceptable");
            return;
        }

        var allFilters = {
            _id: allParams.timeoffEmployee
        };
        userCollection.findOne(allFilters)
            .then((employeeDetails) => {
                if (!employeeDetails) {
                    reject("Employee not found");
                } else {
                    allFilters = {
                        _id: allParams.timeoffRequestId,
                        timeoffEmployee: allParams.timeoffEmployee,

                    }
                    scheduleTimeOffCollection.findOne(allFilters)
                        .then((scheduledTimeoffDetails) => {
                            if (!scheduledTimeoffDetails) {
                                reject("Request not found.");
                            } else {
                                if (new Date(scheduledTimeoffDetails.timeoffStartDate) < new Date() || new Date(scheduledTimeoffDetails.timeoffEndDate) < new Date()) {
                                    reject("scheduled time-off date is in past, not acceptable");
                                } else {
                                    var updateObj = {
                                        timeoffStartDate: allParams.timeoffStartDate,
                                        timeoffEndDate: allParams.timeoffEndDate,
                                        timeOffStartTime: allParams.timeOffStartTime,
                                        timeoffEndTime: allParams.timeoffEndTime,
                                        timeoffStatus: allParams.timeoffStatus,
                                        updatedBy: allParams.updatedBy
                                    }
                                    if (updateObj.timeoffStatus != timeoffStatusOptions.pending) {
                                        updateObj.approvedRejectedBy = allParams.updatedBy;
                                        updateObj.approvedRejectedOnDate = new Date();
                                    }

                                    scheduleTimeOffCollection.findOneAndUpdate(allFilters, updateObj)
                                        .then((updateDetails) => {
                                            if (!updateDetails) {
                                                reject("Something went wrong update failed.")
                                            } else {
                                                res.status(200).json({ status: true, message: "Update successful", data: updateDetails });
                                            }
                                        })
                                }
                            }
                        })

                }
            })
    })
        .catch((err) => {
            res.status(400).json({ status: false, message: err, data: null });
        })
})


/**
 * Function to get accrual leave balance for requested an employee
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso(09 July 2019)
 */
let getAccrualLeaveBalance = ((req, res) => {
    var allParams = req.params;

    return new Promise((resolve, reject) => {
        if (!allParams.userId) {
            reject("userId is mandatory");
        }

        var allFilters = {
            _id: ObjectID(allParams.userId)
        };

        userCollection.findOne(allFilters).populate("leave_Eligibility_id")
            .then((employeeDetails) => {
                if (!employeeDetails) {
                    reject("Employee not found");
                } else {
                    if (!employeeDetails.leave_Eligibility_id) {
                        reject("No leave Eligibility group attached to this employee");
                    } else {
                        allFilters = {
                            "leave_Eligibility_group": employeeDetails.leave_Eligibility_id._id
                        }
                        timeOffPoliciesCollection.find(allFilters)
                            .then((allPoliciesForEmployee) => {
                                if (allPoliciesForEmployee.length == 0) {
                                    reject("Policy details are not available");
                                } else {
                                    /**
                                     * ToDo: Check all conditions like effective date, hire date, first month 
                                     */

                                    var applicablePolicies = [];
                                    for (let i = 0; i < policyDetails.length; i++) {
                                        var isValidPolicy = true;
                                        if (new Date(policyDetails[i].policy_effective_date) >= new Date(new Date().setHours(0, 0, 0, 0))) {
                                            isValidPolicy = false;
                                        }
                                        if (policyDetails[i].expirationoptions.expiretime && new Date(policyDetails[i].expirationoptions.expiredate) < new Date(new Date().setHours(0, 0, 0, 0))) {
                                            isValidPolicy = false;
                                        }
                                        if (policyDetails[i].employee_policy_period.new_hire.hire_date.selected == true && new Date(policyDetails[i].employee_policy_period.new_hire.hire_date.date) >= new Date(employeeDetails.job.hireDate)) {
                                            isValidPolicy = false;
                                        }

                                        if (policyDetails[i].employee_policy_period.new_hire.first_month.selected && policyDetails[i].employee_policy_period.new_hire.first_month.after_days.selected) {
                                            //notCompleted
                                            if (policyDetails[i].employee_policy_period.new_hire.first_month.after_days.days > 0) {
                                                isValidPolicy = false;
                                            }
                                        }

                                        if (isValidPolicy) {
                                            applicablePolicies.push(policyDetails[i]);
                                        }

                                    }

                                    //Check leaves for whole year and check difference for balance

                                    res.status(200).json({ status: true, message: "Accrual balance", data: applicablePolicies });
                                }
                            })
                    }
                }
            })
    })
        .catch((err) => {
            res.status(400).json({ status: true, message: err, data: null });
        })

})

/**
 * Function to get list of scheduled time-off requests for requested an employee
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso(11 July 2019)
 */
let getRequestedTimeOffs = ((req, res) => {

    let allParams = req.body;

    return new Promise((resolve, reject) => {
        if (!allParams.employeeId) {
            reject("employeeId is mandatory.");
        }
        if (!allParams.requestType) {
            reject("requestType is mandatory.");
        }

        if (!allParams.perPageCount) {
            reject("perPageCount is mandatory.");
        }

        if (!allParams.currentPageNumber && allParams.currentPageNumber != 0) {
            reject("currentPageNumber is mandatory.");
        }

        var allFilters = {
            _id: ObjectID(allParams.employeeId)
        };

        userCollection.findOne(allFilters)
            .then((employeeDetails) => {
                if (!employeeDetails) {
                    reject("Employee not found.")
                } else {

                    let timeoffStart = new Date(new Date().getFullYear(), 0, 1);
                    let timeoffEnd = new Date(new Date().getFullYear(), 11, 31);

                    allFilters = {
                        timeoffEmployee: ObjectID(allParams.employeeId)
                    }

                    if (allParams.requestType == "Pending") {
                        allFilters["timeoffStartDate"] = {
                            $gte: new Date(new Date().setHours(0, 0, 0, 0))
                        }
                    } else {
                        allFilters["timeoffEndDate"] = {
                            $lte: new Date(new Date().setHours(0, 0, 0, 0))
                        }
                    }
                    scheduleTimeOffCollection.find(allFilters).populate({ path: 'timeoffEmployee', select: 'personal.name job' }).limit(Number(allParams.perPageCount)).skip((Number(allParams.perPageCount) * Number(allParams.currentPageNum)))
                        .then((timeOffList) => {
                            var returnMessage = "No time-off requests available. "
                            if (timeOffList.length > 0) {
                                returnMessage = "Time-off request list"
                            }
                            res.status(200).json({ status: true, message: returnMessage, data: timeOffList });
                        })
                }
            })
    })
        .catch((err) => {
            res.status(400).json({ status: false, message: err, data: null });
        })
})

/**
 * Function to get list of all employees who are out on specific date based on selected filter
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso(11 July 2019)
 */
let getWhoisOutList = ((req, res) => {

    var allParams = req.body;

    return new Promise((resolve, reject) => {
        if (!allParams.employeeId) {
            reject("employeeId is mandatory");
        }

        if (!allParams.filterFromDate) {
            reject("Filter from date is mandatory");
        }

        if (!allParams.filterToDate) {
            reject("Filter todate is mandatory");
        }

        if (!allParams.selectedCategory) {
            reject("Selected category is mandatory");
        }

        var allFilters = {
            _id: ObjectID(allParams.employeeId)
        }

        userCollection.findOne(allFilters)
            .then((employeeDetails) => {
                if (!employeeDetails) {
                    reject("Employee not found");
                } else {

                    allFilters = {};

                    if (allParams.selectedCategory == "All") {
                        allFilters["companyId"] = employeeDetails.companyId;
                    } else if (allParams.selectedCategory == "Teammates") {
                        allFilters["job.ReportsTo"] = employeeDetails.job.ReportsTo;
                    } else if (allParams.selectedCategory == "Managers") {
                        allFilters["_id"] = ObjectID(allParams.employeeId);
                    } else {
                        allFilters["job.ReportsTo"] = employeeDetails._id;
                    }

                    var valuesToSelect = {
                        _id: true
                    };

                    userCollection.find(allFilters, valuesToSelect)
                        .then((userList) => {
                            if (userList.length <= 0) {
                                reject("No user with selected filters");
                            } else {
                                let tempUserList = [];

                                for (let m = 0; m < userList.length; m++) {
                                    tempUserList.push(userList[m]._id);
                                }

                                allFilters = {
                                    "$and": [{
                                        $and: [{
                                            "timeoffStartDate": {
                                                "$gte": new Date(allParams.filterFromDate)
                                            }
                                        },
                                        {
                                            "timeoffStartDate": {
                                                "$lte": new Date(allParams.filterToDate)
                                            }
                                        }
                                        ]
                                    },
                                    {
                                        $and: [{
                                            "timeoffEndDate": {
                                                "$gte": new Date(allParams.filterFromDate)
                                            }
                                        }, {
                                            "timeoffEndDate": {
                                                "$lte": new Date(allParams.filterToDate)
                                            }
                                        }]
                                    }]
                                }

                                if (allParams.selectedCategory !== "Managers") {
                                    allFilters["timeoffEmployee"] = {
                                        $in: tempUserList
                                    }
                                } else {
                                    allFilters["timeoffEmployee"] = ObjectID(employeeDetails.job.ReportsTo);
                                }

                                console.log("allFilters ", JSON.stringify(allFilters))
                                scheduleTimeOffCollection.find(allFilters).populate({ path: 'timeoffEmployee', select: 'personal.name job' })
                                    .then((timeOffList) => {
                                        var returnMessage = "No timeoff requests in given dates."
                                        if (timeOffList.length > 0) {
                                            returnMessage = "Timeoff request data "
                                        }
                                        res.status(200).json({ status: true, message: returnMessage, data: timeOffList })
                                    })
                            }
                        })
                }
            })
    })
        .catch((err) => {
            res.status(400).json({ status: false, message: err, data: null });
        })
})

module.exports = {
    getCurrentLeaveElibGroup,//To get current leave Eligibility group applicable to requested employee
    changeEligibilityGroup,//To change leave Eligibility group of requested employee
    getLeaveEligGroupHistory,//To get history of leave Eligibility group
    getScheduleTimeoffPolicies,//To get Schedule Timeoff Policies for requested user
    saveScheduleTimeOffDetails,//To apply an schedule timeoff request to senior
    updateScheduledTimeoffDetails,//To approve/Reject/Change timeoff details
    getRequestedTimeOffs,//To get Scheduled time off requests  
    getWhoisOutList,//To get who is out in selected date range
};