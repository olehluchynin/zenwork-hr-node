let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;

var timeoffStatusEnum   =   ["Pending","Approved","Rejected"]

let schedule_timeoffSchema = new mongoose.Schema({
    timeoffEmployee         :   { type: ObjectID, ref: 'users',required:true },
    selectedPolicy      :   { type: ObjectID, ref: 'time_off_policies',required:true },
    timeoffPolicyType       :   {type : String, required:   true},
    timeoffStartDate        :   {type : Date,   required:   true},
    timeoffEndDate          :   {type : Date,   required:   true},
    timeOffStartTime        :   {type : Date,   required:   true},
    timeoffEndTime          :   {type : Date,   required:   true},
    managerId               :   { type: ObjectID, ref: 'users',required:    true },
    companyId               :   { type: ObjectID, ref: 'companies',required:    true},
    timeoffStatus           :   {type:  String, enum:   timeoffStatusEnum,required: true},
    approvedRejectedBy      :   { type: ObjectID, ref: 'users' },
    approvedRejectedOnDate  :   Date,
    createdBy               :   { type: ObjectID, ref: 'users',required:    true},
    updatedBy               :   { type: ObjectID, ref: 'users',required:    true},
},{
    timestamps: true,
    versionKey: false,
    strict: false
})


let schedule_timeoffModel = mongoose.model('schedule_timeoffs', schedule_timeoffSchema);

module.exports  =   {
    schedule_timeoffModel
}