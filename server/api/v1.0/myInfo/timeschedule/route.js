let express =   require("express");
let router  =   express.Router();

let authMiddleware = require('../../middlewares/auth');
let timescheduleController  =   require('./controller');

router.get('/getcurrentleaveeligroup/:userId',authMiddleware.isUserLogin,timescheduleController.getCurrentLeaveElibGroup);
router.post('/changeleaveeligibgroup',authMiddleware.isUserLogin,timescheduleController.changeEligibilityGroup);
router.get('/getleaveeligrouphistory/:userId',authMiddleware.isUserLogin,timescheduleController.getLeaveEligGroupHistory);
router.get('/gettimeoffpolicies/:userId',authMiddleware.isUserLogin,timescheduleController.getScheduleTimeoffPolicies);
router.post('/applyfortimeoff',authMiddleware.isUserLogin,timescheduleController.saveScheduleTimeOffDetails);
router.post('/updatedscheduledtimeoff',authMiddleware.isUserLogin,timescheduleController.updateScheduledTimeoffDetails);
router.post('/gettimeoffrequests',authMiddleware.isUserLogin,timescheduleController.getRequestedTimeOffs);
router.post('/getwhoisoutlist',authMiddleware.isUserLogin,timescheduleController.getWhoisOutList);

module.exports  =   router;