/**
 * @api {get} /myinfotimeschedule/getcurrentleaveeligroup/:userId GetCurrentLeaveEligGroup
 * @apiName Get Current Leave Eligibility Group
 * @apiGroup MyInfo Time Schedule
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {String} userId _id of employee whose current leave Eligibility group you want
 * @apiParamExample {json} Request-Example: 
 * http://localhost:3003/api/v1.0/myinfo/myinfotimeschedule/getcurrentleaveeligroup/5cd91cc3f308c21c43e5054d
 *

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Leave Eligibility group details",
    "data": {
        "leave_Eligibility_id": [
            {
                "Eligibility_criteria": {
                    "sub_fields": [
                        "Banglore"
                    ],
                    "selected_field": "Location"
                },
                "grand_father": {
                    "include": [
                        {
                            "item_id": "5cd91cc3f308c21c43e5054d",
                            "item_text": "vipin"
                        },
                        {
                            "item_id": "5cd92313f308c21c43e50599",
                            "item_text": "ravi"
                        },
                        {
                            "item_id": "5cd91d94f308c21c43e50553",
                            "item_text": "raj"
                        },
                        {
                            "item_id": "5cda377f51f0be25b3adceb4",
                            "item_text": "Flor"
                        },
                        {
                            "item_id": "5cda3ad10e57b225e1240c9d",
                            "item_text": "Slyvia"
                        },
                        {
                            "item_id": "5cda3d4a2fec3c2610219939",
                            "item_text": "Lina"
                        }
                    ],
                    "exclude": []
                },
                "emp_effective_date": {
                    "date_selection": "Custom Date"
                },
                "_id": "5d0ce6a8c213d57fd5a1e347",
                "companyId": "5c9e11894b1d726375b23058",
                "name": "Leave 1",
                "effectiveDate": "2019-07-26T18:30:00.000Z",
                "benefitRule": false,
                "benefitType": "Part Time",
                "createdAt": "2019-06-21T14:16:08.419Z",
                "updatedAt": "2019-06-21T14:16:08.419Z"
            }
        ],
        "_id": "5cd91cc3f308c21c43e5054d",
        "leave_EligGrp_EffectDate": "2019-03-29T00:00:00.000Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */




/**
 * @api {post} /myinfo/myinfotimeschedule/changeleaveeligibgroup ChangeLeaveEligibilityGroup
 * @apiName Change leave Eligibility group
 * @apiGroup MyInfo Time Schedule
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * @apiParam {String} newEligibilityGroup  New Leave Eligibility group
 * @apiParam {String} userId		_id of employee whose leave Eligibility group has to be changed
 * @apiParam {String} updatedBy		_id of current logged in employee 
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"newEligibilityGroup" : "5d0cdcbdc213d57fd5a1e33c",
	"userId" : "5cd91cc3f308c21c43e5054d",
	"updatedBy":"5c9e11894b1d726375b23057"
}
*
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Leave Eligibility group is updated",
    "data": {
        "personal": {
            "name": {
                "firstName": "vipin",
                "middleName": "M",
                "lastName": "reddy",
                "preferredName": "vipin"
            },
            "address": {
                "street1": "hyd",
                "street2": "hyd",
                "city": "hyd",
                "state": "NewYork",
                "zipcode": "121222222",
                "country": "United States"
            },
            "contact": {
                "workPhone": "1234567654",
                "extention": "2345",
                "mobilePhone": "2345654345",
                "homePhone": "6543321234",
                "workMail": "info@gmail.com",
                "personalMail": "vipin.reddy@mtwlabs.com"
            },
            "dob": "1992-06-10T18:30:00.000Z",
            "gender": "male",
            "maritalStatus": "Single",
            "effectiveDate": "2019-05-13T18:30:00.000Z",
            "veteranStatus": "Single",
            "ssn": "123321233",
            "ethnicity": "Ethnicity11",
            "education": [],
            "languages": [],
            "visa_information": []
        },
        "job": {
            "current_employment_status": {
                "status": "Active",
                "effective_date": "2019-06-06T18:30:00.000Z"
            },
            "Specific_Workflow_Approval": "No",
            "employeeId": "1234",
            "employeeType": "Full Time",
            "hireDate": "2018-08-12T18:30:00.000Z",
            "jobTitle": "Software Engineer",
            "department": "Tech",
            "location": "Banglore",
            "businessUnit": "Unit 1",
            "unionFields": "Union 1",
            "FLSA_Code": "Exempt",
            "EEO_Job_Category": "Professionals",
            "Site_AccessRole": "5cd91346f308c21c43e50511",
            "VendorId": "234532",
            "employment_status_history": [
                {
                    "_id": "5cd91cc3f308c21c43e5054e",
                    "status": "Active",
                    "effective_date": "2019-05-13T07:29:07.042Z",
                    "end_date": "2019-05-23T08:08:22.872Z"
                },
                {
                    "_id": "5ce654f6bee9b42bc9f15f31",
                    "status": "Active",
                    "effective_date": "2019-05-06T18:30:00.000Z",
                    "end_date": "2019-05-23T08:34:58.834Z"
                },
                {
                    "_id": "5ce65b3220c97f2d35a2801d",
                    "status": "Terminated",
                    "effective_date": "2019-05-06T18:30:00.000Z",
                    "end_date": "2019-05-24T02:01:22.631Z"
                },
                {
                    "_id": "5ce750723db9243152d77797",
                    "status": "Terminated",
                    "effective_date": "2019-05-07T18:30:00.000Z",
                    "end_date": "2019-06-07T04:32:33.257Z"
                },
                {
                    "_id": "5cf9e8e1912a72225bb3fb25",
                    "status": "Active",
                    "effective_date": "2019-06-06T18:30:00.000Z"
                }
            ],
            "job_information_history": [
                {
                    "_id": "5cd91cc3f308c21c43e5054f",
                    "jobTitle": "Software Engineer",
                    "department": "Tech",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "effective_date": "2019-05-13T07:29:07.042Z"
                }
            ],
            "eligibleForRehire": true,
            "last_day_of_work": "2019-06-20T18:30:00.000Z",
            "off_boarding_template_assigned": "5cf9e8e1912a72225bb3fb27",
            "terminationDate": "2019-06-06T18:30:00.000Z",
            "terminationReason": "Attendance",
            "terminationType": "Voluntary",
            "termination_comments": "ghhj",
            "ReportsTo": "5cda377f51f0be25b3adceb4"
        },
        "compensation": {
            "NonpaidPosition": true,
            "Pay_group": "Group2",
            "Pay_frequency": "Semi-Monthly",
            "compensationRatio": "2",
            "Salary_Grade": "high",
            "compensation_history": [],
            "current_compensation": []
        },
        "module_settings": {
            "eligible_for_benifits": false,
            "assign_leave_management_rules": false,
            "assign_performance_management": false,
            "assaign_employee_number": "Manual",
            "onboarding_template": "5cd91cc3f308c21c43e50552"
        },
        "status": "active",
        "groups": [
            "5cd922b7f308c21c43e50560",
            "5cdabfd9af231c1ca23a208a",
            "5cdac0aaaf231c1ca23a20fd",
            "5cdac212107c831d51cd05ee",
            null
        ],
        "signUpType": "manual",
        "_id": "5cd91cc3f308c21c43e5054d",
        "email": "info@gmail.com",
        "companyId": "5c9e11894b1d726375b23058",
        "type": "employee",
        "password": "MV7Xx9MJ",
        "createdAt": "2019-05-13T07:29:07.060Z",
        "updatedAt": "2019-07-08T07:09:09.879Z",
        "userId": 88,
        "offboardedBy": "5c9e11894b1d726375b23057",
        "leave_Eligibility_group_history": [],
        "leave_Eligibility_id": "5d0ce6a8c213d57fd5a1e347",
        "leave_EligGrp_EffectDate": "2019-07-08T07:09:09.879Z",
        "blackout_EffectDate": "2019-03-29T00:00:00.000Z",
        "blackout_GrpName": "Test",
        "blackout_emp_grp_id": "5d021d90cce65213e4d1395e",
        "positionworkschedule_id": "5d0ce119c213d57fd5a1e344",
        "updatedBy": "5c9e11894b1d726375b23057"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {get} /myinfotimeschedule/getleaveeligrouphistory/:userId GetLeaveEligGroupHistory
 * @apiName Get Current Leave Eligibility Group History
 * @apiGroup MyInfo Time Schedule
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {String} userId _id of employee whose current leave Eligibility group history you want
 * @apiParamExample {json} Request-Example: 
 * http://localhost:3003/api/v1.0/myinfo/myinfotimeschedule/getleaveeligrouphistory/5cda377f51f0be25b3adceb4
 *

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Leave Eligibility group history ",
    "data": {
        "leave_Eligibility_group_history": [
            {
                "_id": "5d23100fdaa75e22ac67d89f",
                "leave_Eligibility_id": {
                    "Eligibility_criteria": {
                        "sub_fields": [
                            "Unit 1"
                        ],
                        "selected_field": "Business Unit"
                    },
                    "grand_father": {
                        "include": [
                            {
                                "item_id": "5cd91cc3f308c21c43e5054d",
                                "item_text": "vipin"
                            },
                            {
                                "item_id": "5cd91d94f308c21c43e50553",
                                "item_text": "raj"
                            },
                            {
                                "item_id": "5cd91e68f308c21c43e50559",
                                "item_text": "sai"
                            },
                            {
                                "item_id": "5cd92313f308c21c43e50599",
                                "item_text": "ravi"
                            },
                            {
                                "item_id": "5cd93e4a8e9dea1f4bb987dc",
                                "item_text": "asma"
                            },
                            {
                                "item_id": "5cda377f51f0be25b3adceb4",
                                "item_text": "Flor"
                            },
                            {
                                "item_id": "5cda3ad10e57b225e1240c9d",
                                "item_text": "Slyvia"
                            },
                            {
                                "item_id": "5cda3d4a2fec3c2610219939",
                                "item_text": "Lina"
                            },
                            {
                                "item_id": "5cdeaa4c3c5c5b67e3533423",
                                "item_text": "suri"
                            },
                            {
                                "item_id": "5cdec1d5df4fd06b83354eb8",
                                "item_text": "Smith"
                            },
                            {
                                "item_id": "5cdf0dc1df4fd06b83354f2a",
                                "item_text": "nick112233"
                            },
                            {
                                "item_id": "5ce6275ee2c6ab2b5061ae88",
                                "item_text": "charan"
                            },
                            {
                                "item_id": "5cf6095fecc3dd104e07cd7d",
                                "item_text": "Diana"
                            },
                            {
                                "item_id": "5cf60c35ecc3dd104e07cda2",
                                "item_text": "kabir"
                            },
                            {
                                "item_id": "5cf61c6ecd193f11e6a25a72",
                                "item_text": "Cortez"
                            },
                            {
                                "item_id": "5cf634aa810f561230a5158c",
                                "item_text": "vinnu"
                            },
                            {
                                "item_id": "5cf63b954122c722605fc34a",
                                "item_text": "vinnu"
                            },
                            {
                                "item_id": "5cf63da992e28543f4489cd8",
                                "item_text": "vinnu"
                            },
                            {
                                "item_id": "5cf64d906a9bd83844c8b905",
                                "item_text": "vinnu"
                            },
                            {
                                "item_id": "5cf653208b742314ef8ad2cd",
                                "item_text": "dfd"
                            },
                            {
                                "item_id": "5cf666cd617d2e1918997509",
                                "item_text": "vinnu"
                            },
                            {
                                "item_id": "5cf8bc49f098d90ff0f10bde",
                                "item_text": "vinnu"
                            },
                            {
                                "item_id": "5cf9059412bccb31e028e038",
                                "item_text": "vinnu"
                            },
                            {
                                "item_id": "5cff696e386d686297b5132d",
                                "item_text": "Roy"
                            },
                            {
                                "item_id": "5d0082410cb9b86a2eaf2bb4",
                                "item_text": "kira"
                            },
                            {
                                "item_id": "5d00a12e2b285670bf28944d",
                                "item_text": "one"
                            },
                            {
                                "item_id": "5d00a2442b285670bf28946c",
                                "item_text": "second"
                            },
                            {
                                "item_id": "5d00a3632b285670bf28948b",
                                "item_text": "Third"
                            },
                            {
                                "item_id": "5d00a5602b285670bf2894aa",
                                "item_text": "Fourth"
                            },
                            {
                                "item_id": "5d00a6442b285670bf2894b3",
                                "item_text": "fifth"
                            },
                            {
                                "item_id": "5d00ade42b285670bf2894d2",
                                "item_text": "sixth"
                            },
                            {
                                "item_id": "5d00ae122b285670bf2894f1",
                                "item_text": "sixth"
                            },
                            {
                                "item_id": "5d00ae2f2b285670bf289510",
                                "item_text": "sixth"
                            },
                            {
                                "item_id": "5d00aed22b285670bf28952f",
                                "item_text": "sixth"
                            },
                            {
                                "item_id": "5d00afbf2b285670bf28954e",
                                "item_text": "sixth"
                            },
                            {
                                "item_id": "5d00b42c2ef87a28e41d61a8",
                                "item_text": "ria"
                            },
                            {
                                "item_id": "5d00c3b92b285670bf289575",
                                "item_text": "raj"
                            },
                            {
                                "item_id": "5d0cd60b9f139654dab05538",
                                "item_text": "vipin"
                            },
                            {
                                "item_id": "5d0cd6269f139654dab05557",
                                "item_text": "vipin"
                            },
                            {
                                "item_id": "5d0cd7d39f139654dab0557a",
                                "item_text": "uday"
                            },
                            {
                                "item_id": "5d0cd8959f139654dab05599",
                                "item_text": "uday"
                            },
                            {
                                "item_id": "5d0cda339f139654dab055ba",
                                "item_text": "qwe"
                            }
                        ],
                        "exclude": []
                    },
                    "emp_effective_date": {
                        "date_selection": "Date of Hire"
                    },
                    "_id": "5d0cdcbdc213d57fd5a1e33c",
                    "companyId": "5c9e11894b1d726375b23058",
                    "name": "Sick",
                    "effectiveDate": "2019-05-07T18:30:00.000Z",
                    "benefitRule": false,
                    "benefitType": "Full Time",
                    "createdAt": "2019-06-21T13:33:49.083Z",
                    "updatedAt": "2019-06-21T13:35:10.425Z"
                },
                "leave_EligGrp_EndDate": "2019-07-08T09:42:39.029Z"
            },
            {
                "_id": "5d231078daa75e22ac67d8a0",
                "leave_Eligibility_id": {
                    "Eligibility_criteria": {
                        "sub_fields": [
                            "Banglore"
                        ],
                        "selected_field": "Location"
                    },
                    "grand_father": {
                        "include": [
                            {
                                "item_id": "5cd91cc3f308c21c43e5054d",
                                "item_text": "vipin"
                            },
                            {
                                "item_id": "5cd92313f308c21c43e50599",
                                "item_text": "ravi"
                            },
                            {
                                "item_id": "5cd91d94f308c21c43e50553",
                                "item_text": "raj"
                            },
                            {
                                "item_id": "5cda377f51f0be25b3adceb4",
                                "item_text": "Flor"
                            },
                            {
                                "item_id": "5cda3ad10e57b225e1240c9d",
                                "item_text": "Slyvia"
                            },
                            {
                                "item_id": "5cda3d4a2fec3c2610219939",
                                "item_text": "Lina"
                            }
                        ],
                        "exclude": []
                    },
                    "emp_effective_date": {
                        "date_selection": "Custom Date"
                    },
                    "_id": "5d0ce6a8c213d57fd5a1e347",
                    "companyId": "5c9e11894b1d726375b23058",
                    "name": "Leave 1",
                    "effectiveDate": "2019-07-26T18:30:00.000Z",
                    "benefitRule": false,
                    "benefitType": "Part Time",
                    "createdAt": "2019-06-21T14:16:08.419Z",
                    "updatedAt": "2019-06-21T14:16:08.419Z"
                },
                "leave_EligGrp_EffectDate": "2019-07-08T09:42:39.029Z",
                "leave_EligGrp_EndDate": "2019-07-08T09:44:24.286Z"
            }
        ]
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


 /**
 * @api {get} /myinfotimeschedule/gettimeoffpolicies/:userId GetAllTimeoffPolicies
 * @apiName Get All Timeoff Policies
 * @apiGroup MyInfo Time Schedule
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {String} userId _id of employee whose current leave Eligibility group you want
 * @apiParamExample {json} Request-Example: 
 * http://localhost:3003/api/v1.0/myinfo/myinfotimeschedule/gettimeoffpolicies/5cda377f51f0be25b3adceb4
 *

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Policy details available for the group",
    "data": [
        {
            "expirationoptions": {
                "expiretime": true,
                "expireoptions": "Expiration Date",
                "expiredate": "2019-07-30T18:30:00.000Z"
            },
            "_id": "5d23227784d011475d5e8fb9",
            "policytype": "Sick"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /myinfo/myinfotimeschedule/applyfortimeoff Apply for scheduled timeoff
 * @apiName Apply for scheduled timeoff
 * @apiGroup MyInfo Time Schedule
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * @apiParam {String} selectedPolicy  _id of time-off policy
 * @apiParam {String} timeoffPolicyType		Name of time-off policy
 * @apiParam {String} timeoffEmployee	_id of requesting employee 
 * 
 * @apiParamExample {json} Request-Example:
 *
{
                "timeoffEmployee"         :   "5cd91cc3f308c21c43e5054d",
                "selectedPolicy"	      :   "5d23227784d011475d5e8fb9",
                "timeoffPolicyType"       :   "Sick",
                "timeoffStartDate"        :   "Sat Jul 13 2019 00:00:00 GMT+0530 (India Standard Time)",
                "timeoffEndDate"          :   "Sat Jul 13 2019 00:00:00 GMT+0530 (India Standard Time)",
                "timeOffStartTime"        :   "Sat Jul 13 2019 00:00:00 GMT+0530 (India Standard Time)",
                "timeoffEndTime"          :   "Sat Jul 13 2019 23:59:00 GMT+0530 (India Standard Time)",
                "createdBy"               :   "5cd91cc3f308c21c43e5054d"
 }
*
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Schedule time-off saved successfully",
    "data": {
        "_id": "5d258a2f52e9bf16e0b32849",
        "timeoffEmployee": "5cd91cc3f308c21c43e5054d",
        "selectedPolicy": "5d23227784d011475d5e8fb9",
        "timeoffPolicyType": "Sick",
        "timeoffStartDate": "2019-07-13T18:30:00.000Z",
        "timeoffEndDate": "2019-07-13T18:30:00.000Z",
        "timeOffStartTime": "2019-07-13T18:30:00.000Z",
        "timeoffEndTime": "2019-07-14T18:29:00.000Z",
        "managerId": "5cda377f51f0be25b3adceb4",
        "companyId": "5c9e11894b1d726375b23058",
        "timeoffStatus": "Pending",
        "createdBy": "5cd91cc3f308c21c43e5054d",
        "updatedBy": "5cd91cc3f308c21c43e5054d",
        "createdAt": "2019-07-10T06:48:15.678Z",
        "updatedAt": "2019-07-10T06:48:15.678Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /myinfo/myinfotimeschedule/updatedscheduledtimeoff Update scheduled time-off
 * @apiName Update scheduled time-off
 * @apiGroup MyInfo Time Schedule
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * @apiParam {String} selectedPolicy  _id of time-off policy
 * @apiParam {String} timeoffStatus		Available options: "Pending","Approved","Rejected"
 * @apiParam {String} timeoffEmployee	_id of requesting employee 
 * @apiParam {String} updatedBy			_id of manager or request employee
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"timeoffRequestId"		:	"5d25794512ba8410189f126d",
	"timeoffEmployee"         :   "5cd91cc3f308c21c43e5054d",
	"timeoffStartDate"        :   "Thu Jul 11 2019 00:00:00 GMT+0530 (India Standard Time)",
    "timeoffEndDate"          :   "Sat Jul 13 2019 00:00:00 GMT+0530 (India Standard Time)",
    "timeOffStartTime"        :   "Thu Jul 11 2019 00:00:00 GMT+0530 (India Standard Time)",
    "timeoffEndTime"          :   "Sat Jul 13 2019 23:59:00 GMT+0530 (India Standard Time)",
	"timeoffStatus"           :   "Rejected",
	"updatedBy"               :   "5cda377f51f0be25b3adceb4"
}
*
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Update successful",
    "data": {
        "_id": "5d25794512ba8410189f126d",
        "timeoffEmployee": "5cd91cc3f308c21c43e5054d",
        "selectedPolicy": "5d23227784d011475d5e8fb9",
        "timeoffPolicyType": "Sick",
        "timeoffStartDate": "2019-07-10T18:30:00.000Z",
        "timeoffEndDate": "2019-07-12T18:30:00.000Z",
        "timeOffStartTime": "2019-07-10T18:30:00.000Z",
        "timeoffEndTime": "2019-07-13T18:29:00.000Z",
        "managerId": "5cda377f51f0be25b3adceb4",
        "companyId": "5c9e11894b1d726375b23058",
        "timeoffStatus": "Rejected",
        "createdBy": "5cd91cc3f308c21c43e5054d",
        "updatedBy": "5cda377f51f0be25b3adceb4",
        "createdAt": "2019-07-10T05:36:05.446Z",
        "updatedAt": "2019-07-10T06:57:12.427Z",
        "approvedRejectedBy": "5cda377f51f0be25b3adceb4",
        "approvedRejectedOnDate": "2019-07-10T06:57:12.426Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /myinfo/myinfotimeschedule/gettimeoffrequests Get Timeoff requests
 * @apiName Get Timeoff requests
 * @apiGroup MyInfo Time Schedule
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * @apiParam {String} requestType  Available options: "Pending","Completed"
 * @apiParam {String} employeeId  _id of employee whose timeoff details are required
 * 
 * @apiParamExample {json} Request-Example:
 *
{
 "employeeId":"5cd91cc3f308c21c43e5054d",
 "requestType":"Completed",
 "perPageCount":3,
 "currentPageNumber":0

}
*
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Time-off request list",
    "data": [
        {
            "_id": "5d245a2e2dbd371d48ece125",
            "timeoffEmployee": {
                "personal": {
                    "name": {
                        "firstName": "vipin",
                        "middleName": "M",
                        "lastName": "reddy",
                        "preferredName": "vipin"
                    }
                },
                "job": {
                    "current_employment_status": {
                        "status": "Active",
                        "effective_date": "2019-06-06T18:30:00.000Z"
                    },
                    "Specific_Workflow_Approval": "No",
                    "employeeId": "1234",
                    "employeeType": "Full Time",
                    "hireDate": "2018-08-12T18:30:00.000Z",
                    "jobTitle": "Software Engineer",
                    "department": "Tech",
                    "location": "Banglore",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "FLSA_Code": "Exempt",
                    "EEO_Job_Category": "Professionals",
                    "Site_AccessRole": "5cd91346f308c21c43e50511",
                    "VendorId": "234532",
                    "employment_status_history": [
                        {
                            "_id": "5cd91cc3f308c21c43e5054e",
                            "status": "Active",
                            "effective_date": "2019-05-13T07:29:07.042Z",
                            "end_date": "2019-05-23T08:08:22.872Z"
                        },
                        {
                            "_id": "5ce654f6bee9b42bc9f15f31",
                            "status": "Active",
                            "effective_date": "2019-05-06T18:30:00.000Z",
                            "end_date": "2019-05-23T08:34:58.834Z"
                        },
                        {
                            "_id": "5ce65b3220c97f2d35a2801d",
                            "status": "Terminated",
                            "effective_date": "2019-05-06T18:30:00.000Z",
                            "end_date": "2019-05-24T02:01:22.631Z"
                        },
                        {
                            "_id": "5ce750723db9243152d77797",
                            "status": "Terminated",
                            "effective_date": "2019-05-07T18:30:00.000Z",
                            "end_date": "2019-06-07T04:32:33.257Z"
                        },
                        {
                            "_id": "5cf9e8e1912a72225bb3fb25",
                            "status": "Active",
                            "effective_date": "2019-06-06T18:30:00.000Z"
                        }
                    ],
                    "job_information_history": [
                        {
                            "_id": "5cd91cc3f308c21c43e5054f",
                            "jobTitle": "Software Engineer",
                            "department": "Tech",
                            "businessUnit": "Unit 1",
                            "unionFields": "Union 1",
                            "effective_date": "2019-05-13T07:29:07.042Z"
                        }
                    ],
                    "eligibleForRehire": true,
                    "last_day_of_work": "2019-06-20T18:30:00.000Z",
                    "off_boarding_template_assigned": "5cf9e8e1912a72225bb3fb27",
                    "terminationDate": "2019-06-06T18:30:00.000Z",
                    "terminationReason": "Attendance",
                    "terminationType": "Voluntary",
                    "termination_comments": "ghhj",
                    "ReportsTo": "5cda377f51f0be25b3adceb4"
                },
                "_id": "5cd91cc3f308c21c43e5054d"
            },
            "selectedPolicy": "5d23227784d011475d5e8fb9",
            "timeoffPolicyType": "Sick",
            "timeoffStartDate": "2019-07-09T18:30:00.000Z",
            "timeoffEndDate": "2019-07-11T18:30:00.000Z",
            "timeOffStartTime": "2019-07-09T18:30:00.000Z",
            "timeoffEndTime": "2019-07-12T18:29:00.000Z",
            "managerId": "5cda377f51f0be25b3adceb4",
            "companyId": "5c9e11894b1d726375b23058",
            "timeoffStatus": "Pending",
            "createdBy": "5cd91cc3f308c21c43e5054d",
            "updatedBy": "5cda377f51f0be25b3adceb4",
            "createdAt": "2019-07-09T09:11:10.012Z",
            "updatedAt": "2019-07-10T05:27:05.834Z"
        },
        {
            "_id": "5d245ad91d4b3b3174570346",
            "timeoffEmployee": {
                "personal": {
                    "name": {
                        "firstName": "vipin",
                        "middleName": "M",
                        "lastName": "reddy",
                        "preferredName": "vipin"
                    }
                },
                "job": {
                    "current_employment_status": {
                        "status": "Active",
                        "effective_date": "2019-06-06T18:30:00.000Z"
                    },
                    "Specific_Workflow_Approval": "No",
                    "employeeId": "1234",
                    "employeeType": "Full Time",
                    "hireDate": "2018-08-12T18:30:00.000Z",
                    "jobTitle": "Software Engineer",
                    "department": "Tech",
                    "location": "Banglore",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "FLSA_Code": "Exempt",
                    "EEO_Job_Category": "Professionals",
                    "Site_AccessRole": "5cd91346f308c21c43e50511",
                    "VendorId": "234532",
                    "employment_status_history": [
                        {
                            "_id": "5cd91cc3f308c21c43e5054e",
                            "status": "Active",
                            "effective_date": "2019-05-13T07:29:07.042Z",
                            "end_date": "2019-05-23T08:08:22.872Z"
                        },
                        {
                            "_id": "5ce654f6bee9b42bc9f15f31",
                            "status": "Active",
                            "effective_date": "2019-05-06T18:30:00.000Z",
                            "end_date": "2019-05-23T08:34:58.834Z"
                        },
                        {
                            "_id": "5ce65b3220c97f2d35a2801d",
                            "status": "Terminated",
                            "effective_date": "2019-05-06T18:30:00.000Z",
                            "end_date": "2019-05-24T02:01:22.631Z"
                        },
                        {
                            "_id": "5ce750723db9243152d77797",
                            "status": "Terminated",
                            "effective_date": "2019-05-07T18:30:00.000Z",
                            "end_date": "2019-06-07T04:32:33.257Z"
                        },
                        {
                            "_id": "5cf9e8e1912a72225bb3fb25",
                            "status": "Active",
                            "effective_date": "2019-06-06T18:30:00.000Z"
                        }
                    ],
                    "job_information_history": [
                        {
                            "_id": "5cd91cc3f308c21c43e5054f",
                            "jobTitle": "Software Engineer",
                            "department": "Tech",
                            "businessUnit": "Unit 1",
                            "unionFields": "Union 1",
                            "effective_date": "2019-05-13T07:29:07.042Z"
                        }
                    ],
                    "eligibleForRehire": true,
                    "last_day_of_work": "2019-06-20T18:30:00.000Z",
                    "off_boarding_template_assigned": "5cf9e8e1912a72225bb3fb27",
                    "terminationDate": "2019-06-06T18:30:00.000Z",
                    "terminationReason": "Attendance",
                    "terminationType": "Voluntary",
                    "termination_comments": "ghhj",
                    "ReportsTo": "5cda377f51f0be25b3adceb4"
                },
                "_id": "5cd91cc3f308c21c43e5054d"
            },
            "selectedPolicy": "5d23227784d011475d5e8fb9",
            "timeoffPolicyType": "Sick",
            "timeoffStartDate": "2019-07-09T18:30:00.000Z",
            "timeoffEndDate": "2019-07-11T18:30:00.000Z",
            "timeOffStartTime": "2019-07-09T18:30:00.000Z",
            "timeoffEndTime": "2019-07-12T18:29:00.000Z",
            "managerId": "5cda377f51f0be25b3adceb4",
            "companyId": "5c9e11894b1d726375b23058",
            "timeoffStatus": "Pending",
            "createdBy": "5cd91cc3f308c21c43e5054d",
            "updatedBy": "5cd91cc3f308c21c43e5054d",
            "createdAt": "2019-07-09T09:14:01.827Z",
            "updatedAt": "2019-07-09T09:14:01.827Z"
        },
        {
            "_id": "5d25794512ba8410189f126d",
            "timeoffEmployee": {
                "personal": {
                    "name": {
                        "firstName": "vipin",
                        "middleName": "M",
                        "lastName": "reddy",
                        "preferredName": "vipin"
                    }
                },
                "job": {
                    "current_employment_status": {
                        "status": "Active",
                        "effective_date": "2019-06-06T18:30:00.000Z"
                    },
                    "Specific_Workflow_Approval": "No",
                    "employeeId": "1234",
                    "employeeType": "Full Time",
                    "hireDate": "2018-08-12T18:30:00.000Z",
                    "jobTitle": "Software Engineer",
                    "department": "Tech",
                    "location": "Banglore",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "FLSA_Code": "Exempt",
                    "EEO_Job_Category": "Professionals",
                    "Site_AccessRole": "5cd91346f308c21c43e50511",
                    "VendorId": "234532",
                    "employment_status_history": [
                        {
                            "_id": "5cd91cc3f308c21c43e5054e",
                            "status": "Active",
                            "effective_date": "2019-05-13T07:29:07.042Z",
                            "end_date": "2019-05-23T08:08:22.872Z"
                        },
                        {
                            "_id": "5ce654f6bee9b42bc9f15f31",
                            "status": "Active",
                            "effective_date": "2019-05-06T18:30:00.000Z",
                            "end_date": "2019-05-23T08:34:58.834Z"
                        },
                        {
                            "_id": "5ce65b3220c97f2d35a2801d",
                            "status": "Terminated",
                            "effective_date": "2019-05-06T18:30:00.000Z",
                            "end_date": "2019-05-24T02:01:22.631Z"
                        },
                        {
                            "_id": "5ce750723db9243152d77797",
                            "status": "Terminated",
                            "effective_date": "2019-05-07T18:30:00.000Z",
                            "end_date": "2019-06-07T04:32:33.257Z"
                        },
                        {
                            "_id": "5cf9e8e1912a72225bb3fb25",
                            "status": "Active",
                            "effective_date": "2019-06-06T18:30:00.000Z"
                        }
                    ],
                    "job_information_history": [
                        {
                            "_id": "5cd91cc3f308c21c43e5054f",
                            "jobTitle": "Software Engineer",
                            "department": "Tech",
                            "businessUnit": "Unit 1",
                            "unionFields": "Union 1",
                            "effective_date": "2019-05-13T07:29:07.042Z"
                        }
                    ],
                    "eligibleForRehire": true,
                    "last_day_of_work": "2019-06-20T18:30:00.000Z",
                    "off_boarding_template_assigned": "5cf9e8e1912a72225bb3fb27",
                    "terminationDate": "2019-06-06T18:30:00.000Z",
                    "terminationReason": "Attendance",
                    "terminationType": "Voluntary",
                    "termination_comments": "ghhj",
                    "ReportsTo": "5cda377f51f0be25b3adceb4"
                },
                "_id": "5cd91cc3f308c21c43e5054d"
            },
            "selectedPolicy": "5d23227784d011475d5e8fb9",
            "timeoffPolicyType": "Sick",
            "timeoffStartDate": "2019-07-10T18:30:00.000Z",
            "timeoffEndDate": "2019-07-10T18:30:00.000Z",
            "timeOffStartTime": "2019-07-10T18:30:00.000Z",
            "timeoffEndTime": "2019-07-10T18:29:00.000Z",
            "managerId": "5cda377f51f0be25b3adceb4",
            "companyId": "5c9e11894b1d726375b23058",
            "timeoffStatus": "Rejected",
            "createdBy": "5cd91cc3f308c21c43e5054d",
            "updatedBy": "5cda377f51f0be25b3adceb4",
            "createdAt": "2019-07-10T05:36:05.446Z",
            "updatedAt": "2019-07-10T06:57:25.487Z",
            "approvedRejectedBy": "5cda377f51f0be25b3adceb4",
            "approvedRejectedOnDate": "2019-07-10T06:57:25.486Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /myinfo/myinfotimeschedule/getwhoisoutlist Get who is out list
 * @apiName Get who is out list
 * @apiGroup MyInfo Time Schedule
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * @apiParam {String} selectedCategory  Available options: "Direct", "All", "Teammates", "Managers"
 * @apiParam {String} employeeId  _id of employee whose timeoff details are required
 * 
 * @apiParamExample {json} Request-Example:
 *
{
"employeeId":"5cda377f51f0be25b3adceb4",
"filterFromDate":"Wed Jul 14 2019 00:00:00 GMT+0530 (India Standard Time)",
"filterToDate":"Wed Jul 14 2019 00:00:00 GMT+0530 (India Standard Time)",
"selectedCategory":"Direct"
}
*
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Timeoff request data ",
    "data": [
        {
            "_id": "5d258a2f52e9bf16e0b32849",
            "timeoffEmployee": {
                "personal": {
                    "name": {
                        "firstName": "vipin",
                        "middleName": "M",
                        "lastName": "reddy",
                        "preferredName": "vipin"
                    }
                },
                "job": {
                    "current_employment_status": {
                        "status": "Active",
                        "effective_date": "2019-06-06T18:30:00.000Z"
                    },
                    "Specific_Workflow_Approval": "No",
                    "employeeId": "1234",
                    "employeeType": "Full Time",
                    "hireDate": "2018-08-12T18:30:00.000Z",
                    "jobTitle": "Software Engineer",
                    "department": "Tech",
                    "location": "Banglore",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "FLSA_Code": "Exempt",
                    "EEO_Job_Category": "Professionals",
                    "Site_AccessRole": "5cd91346f308c21c43e50511",
                    "VendorId": "234532",
                    "employment_status_history": [
                        {
                            "_id": "5cd91cc3f308c21c43e5054e",
                            "status": "Active",
                            "effective_date": "2019-05-13T07:29:07.042Z",
                            "end_date": "2019-05-23T08:08:22.872Z"
                        },
                        {
                            "_id": "5ce654f6bee9b42bc9f15f31",
                            "status": "Active",
                            "effective_date": "2019-05-06T18:30:00.000Z",
                            "end_date": "2019-05-23T08:34:58.834Z"
                        },
                        {
                            "_id": "5ce65b3220c97f2d35a2801d",
                            "status": "Terminated",
                            "effective_date": "2019-05-06T18:30:00.000Z",
                            "end_date": "2019-05-24T02:01:22.631Z"
                        },
                        {
                            "_id": "5ce750723db9243152d77797",
                            "status": "Terminated",
                            "effective_date": "2019-05-07T18:30:00.000Z",
                            "end_date": "2019-06-07T04:32:33.257Z"
                        },
                        {
                            "_id": "5cf9e8e1912a72225bb3fb25",
                            "status": "Active",
                            "effective_date": "2019-06-06T18:30:00.000Z"
                        }
                    ],
                    "job_information_history": [
                        {
                            "_id": "5cd91cc3f308c21c43e5054f",
                            "jobTitle": "Software Engineer",
                            "department": "Tech",
                            "businessUnit": "Unit 1",
                            "unionFields": "Union 1",
                            "effective_date": "2019-05-13T07:29:07.042Z"
                        }
                    ],
                    "eligibleForRehire": true,
                    "last_day_of_work": "2019-06-20T18:30:00.000Z",
                    "off_boarding_template_assigned": "5cf9e8e1912a72225bb3fb27",
                    "terminationDate": "2019-06-06T18:30:00.000Z",
                    "terminationReason": "Attendance",
                    "terminationType": "Voluntary",
                    "termination_comments": "ghhj",
                    "ReportsTo": "5cda377f51f0be25b3adceb4"
                },
                "_id": "5cd91cc3f308c21c43e5054d"
            },
            "selectedPolicy": "5d23227784d011475d5e8fb9",
            "timeoffPolicyType": "Sick",
            "timeoffStartDate": "2019-07-13T18:30:00.000Z",
            "timeoffEndDate": "2019-07-13T18:30:00.000Z",
            "timeOffStartTime": "2019-07-13T18:30:00.000Z",
            "timeoffEndTime": "2019-07-14T18:29:00.000Z",
            "managerId": "5cda377f51f0be25b3adceb4",
            "companyId": "5c9e11894b1d726375b23058",
            "timeoffStatus": "Pending",
            "createdBy": "5cd91cc3f308c21c43e5054d",
            "updatedBy": "5cd91cc3f308c21c43e5054d",
            "createdAt": "2019-07-10T06:48:15.678Z",
            "updatedAt": "2019-07-10T06:48:15.678Z"
        },
        {
            "_id": "5d270a4a01ff7d0f38ff7cb0",
            "timeoffEmployee": {
                "personal": {
                    "name": {
                        "firstName": "Lina",
                        "lastName": "Dunker",
                        "middleName": "James",
                        "preferredName": "Lina"
                    }
                },
                "job": {
                    "current_employment_status": {
                        "status": "Terminated",
                        "effective_date": "2019-05-13T18:30:00.000Z"
                    },
                    "Specific_Workflow_Approval": "No",
                    "employeeId": "16099574",
                    "employeeType": "Full Time",
                    "hireDate": "2019-05-14T18:30:00.000Z",
                    "jobTitle": "Software Engineer",
                    "department": "Tech",
                    "location": "Banglore",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "FLSA_Code": "Exempt",
                    "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                    "Site_AccessRole": "5cd922b7f308c21c43e50560",
                    "VendorId": "4346404",
                    "employment_status_history": [
                        {
                            "_id": "5cda3d4a2fec3c261021993a",
                            "status": "Active",
                            "effective_date": "2019-05-14T04:00:10.410Z",
                            "end_date": "2019-05-17T03:23:07.896Z"
                        },
                        {
                            "_id": "5cde291b4cdb8624bcc28075",
                            "status": "Leave",
                            "effective_date": "2019-05-17T03:05:56.342Z",
                            "end_date": "2019-05-17T03:29:53.021Z"
                        },
                        {
                            "_id": "5cde2ab1a286b927accea5f0",
                            "status": "Active",
                            "effective_date": "2019-05-17T03:05:56.342Z",
                            "end_date": "2019-05-17T03:37:25.999Z"
                        },
                        {
                            "_id": "5cde2c765e2e562494b8187e",
                            "status": "Leave",
                            "effective_date": "2019-05-17T03:40:56.342Z",
                            "end_date": "2019-05-17T03:39:00.627Z"
                        },
                        {
                            "_id": "5cde2cd4e83d4e082c7ff95c",
                            "status": "Active",
                            "effective_date": "2019-05-17T04:40:56.342Z",
                            "end_date": "2019-05-17T03:41:08.613Z"
                        },
                        {
                            "_id": "5cde2d541352531830068e82",
                            "status": "Leave",
                            "effective_date": "2019-05-17T06:40:56.342Z",
                            "end_date": "2019-05-17T03:42:49.391Z"
                        },
                        {
                            "_id": "5cde2db9bb4f582cf8013065",
                            "status": "Active",
                            "effective_date": "2019-05-17T07:40:56.342Z",
                            "end_date": "2019-05-23T09:06:11.926Z"
                        },
                        {
                            "_id": "5ce6628320c97f2d35a2802b",
                            "status": "Leave of Absence",
                            "effective_date": "2019-05-14T18:30:00.000Z",
                            "end_date": "2019-05-23T09:51:37.374Z"
                        },
                        {
                            "_id": "5ce66d297732592f983702ac",
                            "status": "Terminated",
                            "effective_date": "2019-05-13T18:30:00.000Z"
                        }
                    ],
                    "job_information_history": [
                        {
                            "_id": "5cda3d4a2fec3c261021993b",
                            "jobTitle": "Software Engineer",
                            "department": "Tech",
                            "businessUnit": "Unit 1",
                            "unionFields": "Union 1",
                            "effective_date": "2019-05-14T04:00:10.410Z",
                            "end_date": "2019-05-17T04:43:15.819Z"
                        },
                        {
                            "_id": "5cde3be3f2285524c04b21e2",
                            "jobTitle": "Senior Software Engineer",
                            "department": "Tech",
                            "businessUnit": "Unit 1",
                            "unionFields": "Union 1",
                            "effective_date": "2019-05-17T04:05:44.350Z"
                        }
                    ],
                    "ReportsTo": "5cda377f51f0be25b3adceb4",
                    "eligibleForRehire": false,
                    "last_day_of_work": "2019-05-19T18:30:00.000Z",
                    "off_boarding_template_assigned": "5ce66d297732592f983702b0",
                    "terminationDate": "2019-05-13T18:30:00.000Z",
                    "terminationReason": "Abandonment",
                    "terminationType": "Voluntary",
                    "termination_comments": "jhgjh",
                    "HR_Contact": "5d0cfcb4e16c4a5dcb9094d0"
                },
                "_id": "5cda3d4a2fec3c2610219939"
            },
            "selectedPolicy": "5d23227784d011475d5e8fb9",
            "timeoffPolicyType": "Sick",
            "timeoffStartDate": "2019-07-13T18:30:00.000Z",
            "timeoffEndDate": "2019-07-13T18:30:00.000Z",
            "timeOffStartTime": "2019-07-13T18:30:00.000Z",
            "timeoffEndTime": "2019-07-14T18:29:00.000Z",
            "managerId": "5cda377f51f0be25b3adceb4",
            "companyId": "5c9e11894b1d726375b23058",
            "timeoffStatus": "Pending",
            "createdBy": "5cda377f51f0be25b3adceb4",
            "updatedBy": "5cda377f51f0be25b3adceb4",
            "createdAt": "2019-07-11T10:07:06.813Z",
            "updatedAt": "2019-07-11T10:07:06.813Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */
