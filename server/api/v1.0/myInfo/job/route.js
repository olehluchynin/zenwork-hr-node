let express = require('express');

let router = express.Router();

let authMiddleware = require('../../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../../middlewares/validations');

router.post(
    "/updateEmploymentStatus",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.addEmploymentStatus
);

router.post(
    "/updateJobInformation",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.addJobInformation
);

module.exports = router;