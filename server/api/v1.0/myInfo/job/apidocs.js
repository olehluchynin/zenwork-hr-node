/**
 * @api {post} /myinfo/job/updateEmploymentStatus UpdateEmploymentStatus
 * @apiName UpdateEmploymentStatus
 * @apiGroup Job
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cda3d4a2fec3c2610219939",
	"status": "Leave",
	"effective_date": "2019-05-17T03:05:56.342Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Updated employment status successfully",
    "data": {
        "personal": {
            "name": {
                "firstName": "Lina",
                "lastName": "Dunker",
                "middleName": "James",
                "preferredName": "Lina"
            },
            "address": {
                "street1": "shdgioidvknkv",
                "street2": "sdhiyfirnknckbk",
                "city": "Manhattan",
                "state": "NewYork",
                "zipcode": "235365263",
                "country": "United States"
            },
            "contact": {
                "workPhone": "7745365555",
                "extention": "3746",
                "mobilePhone": "23652536649",
                "homePhone": "233446546025",
                "workMail": "lina@gmail.com",
                "personalMail": "lina@gmail.com"
            },
            "social_links": {
                "linkedin": "",
                "twitter": "",
                "facebook": ""
            },
            "dob": "1995-06-07T18:30:00.000Z",
            "gender": "female",
            "maritalStatus": "Single",
            "effectiveDate": "2019-05-14T18:30:00.000Z",
            "veteranStatus": "Single",
            "ssn": "3655694769330",
            "ethnicity": "Ethnicity11",
            "education": [
                {
                    "_id": "5cdba1018ed19a301ececd1e",
                    "university": "",
                    "degree": "",
                    "specialization": "",
                    "gpa": null,
                    "start_date": null,
                    "end_date": null
                }
            ],
            "languages": [
                {
                    "_id": "5cdba1018ed19a301ececd1d",
                    "language": "",
                    "level_of_fluency": "Native proficiency"
                }
            ],
            "visa_information": [
                {
                    "_id": "5cdba1018ed19a301ececd1c",
                    "visa_type": "",
                    "issuing_country": "",
                    "issued_date": null,
                    "expiration_date": null,
                    "status": "",
                    "notes": ""
                }
            ]
        },
        "job": {
            "current_employment_status": {
                "status": "Active",
                "effective_date": "2019-05-17T03:05:56.342Z"
            },
            "Specific_Workflow_Approval": "No",
            "employeeId": "16099574",
            "employeeType": "Full Time",
            "hireDate": "2019-05-14T18:30:00.000Z",
            "jobTitle": "Software Engineer",
            "department": "Tech",
            "location": "Banglore",
            "businessUnit": "Unit 1",
            "unionFields": "Union 1",
            "FLSA_Code": "Exempt",
            "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
            "Site_AccessRole": "5cd922b7f308c21c43e50560",
            "VendorId": "4346404",
            "employment_status_history": [
                {
                    "_id": "5cda3d4a2fec3c261021993a",
                    "status": "Active",
                    "effective_date": "2019-05-14T04:00:10.410Z",
                    "end_date": "2019-05-17T03:23:07.896Z"
                },
                {
                    "_id": "5cde291b4cdb8624bcc28075",
                    "status": "Leave",
                    "effective_date": "2019-05-17T03:05:56.342Z",
                    "end_date": "2019-05-17T03:29:53.021Z"
                },
                {
                    "_id": "5cde2ab1a286b927accea5f0",
                    "status": "Active",
                    "effective_date": "2019-05-17T03:05:56.342Z"
                }
            ],
            "job_information_history": [
                {
                    "_id": "5cda3d4a2fec3c261021993b",
                    "jobTitle": "Software Engineer",
                    "department": "Tech",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "effective_date": "2019-05-14T04:00:10.410Z"
                }
            ]
        },
        "compensation": {
            "NonpaidPosition": true,
            "HighlyCompensatedEmployee": true,
            "Pay_group": "Group2",
            "Pay_frequency": "Weekly",
            "Salary_Grade": "09",
            "compensationRatio": "60%",
            "current_compensation": [
                {
                    "payRate": {
                        "unit": ""
                    },
                    "_id": "5cdb94f68ed19a301ececd18"
                },
                {
                    "payRate": {
                        "amount": 123,
                        "unit": ""
                    },
                    "_id": "5cdb96b98ed19a301ececd19",
                    "effective_date": "2019-05-25T18:30:00.000Z",
                    "payType": "Monthly",
                    "Pay_frequency": "Bi-Weekly",
                    "notes": "sdfgsdfg",
                    "changeReason": "A-Promotion"
                }
            ],
            "compensation_history": []
        },
        "module_settings": {
            "eligible_for_benifits": true,
            "assign_leave_management_rules": true,
            "assign_performance_management": true,
            "assaign_employee_number": "System Generated",
            "onboarding_template": "5cda3d4a2fec3c261021993f"
        },
        "status": "active",
        "groups": [],
        "_id": "5cda3d4a2fec3c2610219939",
        "email": "lina@gmail.com",
        "companyId": "5c9e11894b1d726375b23058",
        "type": "employee",
        "createdAt": "2019-05-14T04:00:10.460Z",
        "updatedAt": "2019-05-17T03:29:53.049Z",
        "userId": 95
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /myinfo/job/updateJobInformation UpdateJobInformation
 * @apiName UpdateJobInformation
 * @apiGroup Job
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cda3d4a2fec3c2610219939",
	"jobTitle" : "5cda3d4a2fec3c261021998", 
    "department" : "Tech", 
    "businessUnit" : "Unit 1", 
    "unionFields" : "Union 1", 
    "effective_date" : "2019-05-17T04:05:44.350Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Updated job information successfully",
    "data": {
        "personal": {
            "name": {
                "firstName": "Lina",
                "lastName": "Dunker",
                "middleName": "James",
                "preferredName": "Lina"
            },
            "address": {
                "street1": "shdgioidvknkv",
                "street2": "sdhiyfirnknckbk",
                "city": "Manhattan",
                "state": "NewYork",
                "zipcode": "235365263",
                "country": "United States"
            },
            "contact": {
                "workPhone": "7745365555",
                "extention": "3746",
                "mobilePhone": "23652536649",
                "homePhone": "233446546025",
                "workMail": "lina@gmail.com",
                "personalMail": "lina@gmail.com"
            },
            "social_links": {
                "linkedin": "",
                "twitter": "",
                "facebook": ""
            },
            "dob": "1995-06-07T18:30:00.000Z",
            "gender": "female",
            "maritalStatus": "Single",
            "effectiveDate": "2019-05-14T18:30:00.000Z",
            "veteranStatus": "Single",
            "ssn": "3655694769330",
            "ethnicity": "Ethnicity11",
            "education": [
                {
                    "_id": "5cdba1018ed19a301ececd1e",
                    "university": "",
                    "degree": "",
                    "specialization": "",
                    "gpa": null,
                    "start_date": null,
                    "end_date": null
                }
            ],
            "languages": [
                {
                    "_id": "5cdba1018ed19a301ececd1d",
                    "language": "",
                    "level_of_fluency": "Native proficiency"
                }
            ],
            "visa_information": [
                {
                    "_id": "5cdba1018ed19a301ececd1c",
                    "visa_type": "",
                    "issuing_country": "",
                    "issued_date": null,
                    "expiration_date": null,
                    "status": "",
                    "notes": ""
                }
            ]
        },
        "job": {
            "current_employment_status": {
                "status": "Active",
                "effective_date": "2019-05-17T07:40:56.342Z"
            },
            "Specific_Workflow_Approval": "No",
            "employeeId": "16099574",
            "employeeType": "Full Time",
            "hireDate": "2019-05-14T18:30:00.000Z",
            "jobTitle": "Software Engineer",
            "department": "Tech",
            "location": "Banglore",
            "businessUnit": "Unit 1",
            "unionFields": "Union 1",
            "FLSA_Code": "Exempt",
            "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
            "Site_AccessRole": "5cd922b7f308c21c43e50560",
            "VendorId": "4346404",
            "employment_status_history": [
                {
                    "_id": "5cda3d4a2fec3c261021993a",
                    "status": "Active",
                    "effective_date": "2019-05-14T04:00:10.410Z",
                    "end_date": "2019-05-17T03:23:07.896Z"
                },
                {
                    "_id": "5cde291b4cdb8624bcc28075",
                    "status": "Leave",
                    "effective_date": "2019-05-17T03:05:56.342Z",
                    "end_date": "2019-05-17T03:29:53.021Z"
                },
                {
                    "_id": "5cde2ab1a286b927accea5f0",
                    "status": "Active",
                    "effective_date": "2019-05-17T03:05:56.342Z",
                    "end_date": "2019-05-17T03:37:25.999Z"
                },
                {
                    "_id": "5cde2c765e2e562494b8187e",
                    "status": "Leave",
                    "effective_date": "2019-05-17T03:40:56.342Z",
                    "end_date": "2019-05-17T03:39:00.627Z"
                },
                {
                    "_id": "5cde2cd4e83d4e082c7ff95c",
                    "status": "Active",
                    "effective_date": "2019-05-17T04:40:56.342Z",
                    "end_date": "2019-05-17T03:41:08.613Z"
                },
                {
                    "_id": "5cde2d541352531830068e82",
                    "status": "Leave",
                    "effective_date": "2019-05-17T06:40:56.342Z",
                    "end_date": "2019-05-17T03:42:49.391Z"
                },
                {
                    "_id": "5cde2db9bb4f582cf8013065",
                    "status": "Active",
                    "effective_date": "2019-05-17T07:40:56.342Z"
                }
            ],
            "job_information_history": [
                {
                    "_id": "5cda3d4a2fec3c261021993b",
                    "jobTitle": "Software Engineer",
                    "department": "Tech",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "effective_date": "2019-05-14T04:00:10.410Z",
                    "end_date": "2019-05-17T04:43:15.819Z"
                },
                {
                    "_id": "5cde3be3f2285524c04b21e2",
                    "jobTitle": "Senior Software Engineer",
                    "department": "Tech",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "effective_date": "2019-05-17T04:05:44.350Z"
                }
            ]
        },
        "compensation": {
            "NonpaidPosition": true,
            "HighlyCompensatedEmployee": true,
            "Pay_group": "Group2",
            "Pay_frequency": "Weekly",
            "Salary_Grade": "09",
            "compensationRatio": "60%",
            "current_compensation": [
                {
                    "payRate": {
                        "unit": ""
                    },
                    "_id": "5cdb94f68ed19a301ececd18"
                },
                {
                    "payRate": {
                        "amount": 123,
                        "unit": ""
                    },
                    "_id": "5cdb96b98ed19a301ececd19",
                    "effective_date": "2019-05-25T18:30:00.000Z",
                    "payType": "Monthly",
                    "Pay_frequency": "Bi-Weekly",
                    "notes": "sdfgsdfg",
                    "changeReason": "A-Promotion"
                }
            ],
            "compensation_history": []
        },
        "module_settings": {
            "eligible_for_benifits": true,
            "assign_leave_management_rules": true,
            "assign_performance_management": true,
            "assaign_employee_number": "System Generated",
            "onboarding_template": "5cda3d4a2fec3c261021993f"
        },
        "status": "active",
        "groups": [],
        "_id": "5cda3d4a2fec3c2610219939",
        "email": "lina@gmail.com",
        "companyId": "5c9e11894b1d726375b23058",
        "type": "employee",
        "createdAt": "2019-05-14T04:00:10.460Z",
        "updatedAt": "2019-05-17T04:43:15.855Z",
        "userId": 95
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */