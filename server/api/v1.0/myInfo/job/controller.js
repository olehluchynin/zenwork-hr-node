let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../../config/config');
let _ = require('underscore');
let User = require('./../../users/model');

/**
*@author Asma
*@date 17/05/2019 07:32
API to add employment status -- UpdateEmploymentStatus button in my info job tab
*/
let addEmploymentStatus = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!companyId) {
            reject('companyId is required')
        }
        else if (!userId) {
            reject('userId is required')
        }
        else {
            resolve(null)
        }
    })
    .then(() => {
        delete body.companyId
        delete body.userId
        let filter = {
            _id: userId,
            companyId: companyId
        }
        User.findOne(filter)
            .then(user => {
                user.job.employment_status_history[user.job.employment_status_history.length - 1].end_date = new Date()
                user.job.employment_status_history.push({
                    'status': body.status,
                    "effective_date": body.effective_date
                })
                user.job.current_employment_status = {
                    'status': body.status,
                    "effective_date": body.effective_date
                }
                user.save(function(err, response) {
                    if (!err) {
                        response = response.toJSON()
                        delete response.password
                        res.status(200).json({
                            status: true,
                            message: "Updated employment status successfully",
                            data: response
                        })
                    }
                    else {
                        console.log(err)
                        res.status(400).json({
                            status: false,
                            message: "Error while updating employment status",
                            error: err
                        })
                    }
                });
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while updating employment status",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

/**
*@author Asma
*@date 17/05/2019 09:14
API to add job informaton -- Update Job Information button in MyInfo Job tab
*/
let addJobInformation = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!companyId) {
            reject('companyId is required')
        }
        else if (!userId) {
            reject('userId is required')
        }
        else {
            resolve(null)
        }
    })
    .then(() => {
        delete body.companyId
        delete body.userId
        console.log(body)
        let filter = {
            _id: userId,
            companyId: companyId
        }
        User.findOne(filter)
            .then(user => {
                user.job.job_information_history[user.job.job_information_history.length - 1].end_date = new Date()
                user.job.job_information_history.push(body)
                // jobTitle: String,
                // department: String,
                // businessUnit: String,
                // unionFields: String,
                // location: String,
                // ReportsTo: { type: ObjectID, ref: 'users' },
                for (let key in body) {
                    if (['jobTitle', "department", 'businessUnit', 'unionFields', 'location', 'ReportsTo'].includes(key)) {
                        user.job[key] = body[key]
                    }
                }
                // user.job.jobTitle = body.jobTitle
                // user.job.department = body.department
                // user.job.businessUnit = body.businessUnit
                // user.job.unionFields = body.unionFields
                // user.job.location = body.location
                // user.job.ReportsTo = body.ReportsTo
                user.save(function(err, response) {
                    if (!err) {
                        response = response.toJSON()
                        delete response.password
                        res.status(200).json({
                            status: true,
                            message: "Updated job information successfully",
                            data: response
                        })
                    }
                    else {
                        console.log(err)
                        res.status(400).json({
                            status: false,
                            message: "Error while updating job information",
                            error: err
                        })
                    }
                });
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while updating job information",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

module.exports = {
    addEmploymentStatus,
    addJobInformation
}