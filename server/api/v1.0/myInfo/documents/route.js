let express = require('express');

let router = express.Router();

let authMiddleware = require('../../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../../middlewares/validations');

var multipart = require('connect-multiparty');

var multipartMiddleware = multipart();

router.post(
    "/upload",
    authMiddleware.isUserLogin,
    multipartMiddleware,
    Controller.uploadDocument
);

router.get(
    "/get/:company_id/:user_id/:_id",
    authMiddleware.isUserLogin,
    Controller.getDocumentDetails
);

router.post(
    "/list",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.getUploadedDocuments
);

router.post(
    "/delete",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deleteDocuments
);

router.post(
    "/edit",
    authMiddleware.isUserLogin,
    multipartMiddleware,
    Controller.editDocuments
);

module.exports = router;