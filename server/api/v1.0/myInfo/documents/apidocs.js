/**
 * @api {post} /myinfo/documents/upload UploadDocument
 * @apiName UploadDocument
 * @apiGroup Documents
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {multipart/form-data} Request-Example:
 *
{
    file=@<file path>
    data={
    "companyId": "5c9e11894b1d726375b23058",
    "userId": "5cda3d4a2fec3c2610219939",
    "documentName": "My Resume 1",
    "documentType": "Resumes and Applications"
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Document uploaded successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /myinfo/documents/get/:company_id/:user_id/:_id GetDocumentDetails
 * @apiName GetDocumentDetails
 * @apiGroup Documents
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} user_id Users unique _id
 * @apiParam {ObjectId} _id Documents unique _id
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched document details",
    "data": {
        "_id": "5ce21b898abd1d312c39d14f",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cda3d4a2fec3c2610219939",
        "originalFilename": "order_invoice.pdf",
        "documentName": "My Resume 1",
        "documentType": "Resumes and Applications",
        "s3Path": "5ce21b888abd1d312c39d14e__order_invoice.pdf",
        "createdAt": "2019-05-20T03:14:17.455Z",
        "updatedAt": "2019-05-20T03:14:17.455Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {post} /myinfo/documents/list ListDocuments
 * @apiName ListDocuments
 * @apiGroup Documents
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "userId": "5cda3d4a2fec3c2610219939",
    "per_page": 10,
    "page_no": 1,
    "sort": {
        "documentName": 1
    },
    "filter": {
        "documentType": "Resumes and Applications"
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Success",
    "count": 1,
    "data": [
        {
            "_id": "5ce21b898abd1d312c39d14f",
            "companyId": "5c9e11894b1d726375b23058",
            "userId": "5cda3d4a2fec3c2610219939",
            "originalFilename": "order_invoice.pdf",
            "documentName": "My Resume 1",
            "documentType": "Resumes and Applications",
            "s3Path": "5ce21b888abd1d312c39d14e__order_invoice.pdf",
            "createdAt": "2019-05-20T03:14:17.455Z",
            "updatedAt": "2019-05-20T03:14:17.455Z",
            "isChecked": false,
            "url": "https://zenworkhr-docs.s3.amazonaws.com/documents/5ce21b888abd1d312c39d14e__order_invoice.pdf?AWSAccessKeyId=AKIAQQWFVNX2IASWO5S7&Expires=1558413136&Signature=zQKjLL79jwkr%2F7vlrnsuffyMegQ%3D"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /myinfo/documents/delete DeleteDocuments
 * @apiName DeleteDocuments
 * @apiGroup Documents
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cda3d4a2fec3c2610219939",
	"_ids": [
		"5ce21b898abd1d312c39d14f",
		"5ce28cf51b4a170f16e7375c"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Success"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /myinfo/documents/edit EditDocuments
 * @apiName EditDocuments
 * @apiGroup Documents
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {multipart/form-data} Request-Example:
 *
{
	-F file=@/C:/Users/Downloads/invoice2.pdf \
    -F 'data={
        "documentId": "5ce28e211b4a170f16e7375e",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cdec1d5df4fd06b83354eb8",
        "originalFilename": "Structure Import.xlsx",
        "documentName": "Personal Resume2",
        "documentType": "Resumes and Applications",
        "s3Path": "5ce28e211b4a170f16e7375d__Structure Import.xlsx",
        "createdAt": "2019-05-20T11:23:13.813Z",
        "updatedAt": "2019-05-20T11:23:13.813Z"
    }'
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Document updated successfully",
    "data": {
        "_id": "5ce28e211b4a170f16e7375e",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cdec1d5df4fd06b83354eb8",
        "originalFilename": "invoice2.pdf",
        "documentName": "Personal Resume2",
        "documentType": "Resumes and Applications",
        "s3Path": "5ce371824b16882bdca9efcd__invoice2.pdf",
        "createdAt": "2019-05-20T11:23:13.813Z",
        "updatedAt": "2019-05-21T03:33:23.569Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

