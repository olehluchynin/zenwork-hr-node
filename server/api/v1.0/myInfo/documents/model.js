let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('../../../../common/constants');

let Schema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    userId: {type: ObjectID, ref: 'users', required: true},
    
    originalFilename: {type: String},
    documentName: {type: String},      
    documentType: {
        type: String
        // enum: ['New Hire Documents', 'Task-list Attachments', 'Workflow Attachments', 'Resumes and Applications', 'Signed Documents']
    },
    s3Path: {type: String}
    
},{
    timestamps: true,
    versionKey: false,
    strict: false
});

Schema.index({ _id: 1, companyId: 1, userId: 1 });

Schema.index({ companyId: 1, userId: 1 });

Schema.index({companyId: 1, userId: 1, documentName: 1}, {unique: true})

let UserDocumentsModel = mongoose.model('documents',Schema);

module.exports = UserDocumentsModel;