let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../../config/config');
let _ = require('underscore');
let User = require('./../../users/model');
let DocumentsModel = require('./model');
let s3 = require('./../../../../common/s3/s3');

/**
*@author Asma
*@date 20/05/2019 08:19
API to upload document to s3 save the details with companyId and userId
*/
let uploadDocument = (req, res) => {
    console.log(req.body);
    console.log(req.files);
    let _id;
    let file;
    let s3Path;
    let body = JSON.parse(req.body.data);
    let companyId = body.companyId
    let userId = body.userId
    return new Promise((resolve, reject) => {
        if(!req.files.file) reject("File not found.");
        else if (!companyId) reject("companyId is required");
        else if (!userId) reject("userId is required");
        else resolve(DocumentsModel.findOne({companyId: companyId, userId: userId, documentName: body.documentName}))
    }).then(document_with_name => {
        if(document_with_name) {
            throw 'Document already exists with this name';
        }  
        else {
            _id = mongoose.Types.ObjectId();
            file = req.files.file;
            s3Path = 'documents/'+_id+'__'+file.originalFilename;
            return s3.uploadFile(s3Path,file.path);
        }   
    }).then(() => {
        let doc = {
            companyId: companyId,
            userId: userId,
            originalFilename: file.originalFilename,
            documentName: body.documentName,
            documentType: body.documentType,
            s3Path: _id+'__'+file.originalFilename
        };
        DocumentsModel.create(doc)
            .then(document => {
                document.save(function(err, response) {
                    if (!err) {
                        res.status(200).json({
                            status: true,
                            message: "Document uploaded successfully"
                        })
                    }
                    else {
                        res.status(400).json({
                            status: false,
                            message: "Error while adding document",
                            error: err
                        })
                    } 
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while adding document",
                    error: err
                })
            })
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
*@author Asma
*@date 21/05/2019 15:21
Method to get document's details
*/
let getDocumentDetails = (req, res) => {
    
    let companyId = req.params.company_id
    let userId = req.params.user_id
    let documentId = req.params._id
    return new Promise( (resolve, reject) => {
        if (!companyId) reject("company_id is required");
        else if (!userId) reject("user_id is required");
        else if (!documentId) reject("_id is required");
        else resolve(null);
    }).then(() => {
        let find_filter = {
            companyId: companyId,
            userId: userId,
            _id: documentId
        }
        DocumentsModel.findOne(find_filter)
            .then(document => {
                if (document) {
                    res.status(200).json({
                        status: true,
                        message: "Successfully fetched document details",
                        data: document
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "Document Not found",
                        data: null
                    })
                }
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while fetching document details",
                    error: err
                })
            })
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
*@author Asma
*@date 21/05/2019 15:21
method to edit document
*/
let editDocuments = (req, res) => {
    // console.log(req.body);
    // console.log(req.files);
    let body = JSON.parse(req.body.data);
    let companyId = body.companyId
    let userId = body.userId
    let documentId = body.documentId
    let file;
    let newS3Path;
    let updateObj = {};
    updateObj['documentName'] = body.documentName
    updateObj['documentType'] = body.documentType
    let s3_id = mongoose.Types.ObjectId();

    return new Promise( (resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else if (!userId) reject('userId is required');
        else if (!documentId) reject('documentId is required');
        else resolve(DocumentsModel.findOne({companyId: companyId, userId: userId, documentName: body.documentName, _id: {$ne: body.documentId}}))
    }).then(document_with_name => {
        if(document_with_name) {
            throw 'Document already exists with this name';
        }
        if(req.files.file) {
            file = req.files.file;
            newS3Path = 'documents/'+ s3_id +'__'+file.originalFilename;
            updateObj['originalFilename'] = file.originalFilename
            updateObj['s3Path'] = s3_id +'__'+file.originalFilename;
            return Promise.all([s3.deleteFile('documents/' + body.s3Path),s3.uploadFile(newS3Path,file.path)]);
        } else {
            return null;
        }
    }).then(updated => {
        console.log(updated);
        let update_filter = {
            companyId : companyId,
            userId: userId,
            _id: documentId
        }
        DocumentsModel.findOneAndUpdate(
            update_filter,
            {
                $set: updateObj
            },
            {
                new: true, useFindAndModify: false
            }
        )
        .then(response => {
            console.log(response)
            res.status(200).json({
                status: true,
                message: "Document updated successfully",
                data: response
            })
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({
                status: false,
                message: "Error while updating document",
                error: err
            })
        })
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
*@author Asma
*@date 21/05/2019 15:21
Method to get list of user's documents
*/
let getUploadedDocuments = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let userId = body.userId
    return new Promise( (resolve, reject) => {
        if (!companyId) reject("companyId is required");
        else if (!userId) reject("userId is required");
        else resolve(null);
    }).then(() => {
        let per_page = body.per_page ? body.per_page : 10
        let page_no = body.page_no ? body.page_no : 1
        let query = {
            companyId: companyId,
            userId: userId
        }
        let sort_obj = {
            createdAt: -1
        }

        if (body.filter && body.filter.hasOwnProperty('documentType')) {
            query['documentType'] = body.filter.documentType
        }

        // if (body.sort) {
        //     sort_obj = Object.assign(sort_obj, body.sort);
        // }
        if ( body.sort && Object.keys(body.sort).length > 0) {
            sort_obj = body.sort
        }

        return Promise.all([
            DocumentsModel.find(query).sort(sort_obj).skip((page_no - 1) * per_page).limit(per_page).lean().exec(),
            DocumentsModel.countDocuments(query)
        ])
        
    }).then(async ([data, total_count]) => {
        let queries = [];
        data.forEach(function (each) {
            each.isChecked = false;
            queries.push(s3.getPresignedUrlOfAfile('zenworkhr-docs', 'documents', each))
        });
        return {
            documentsWithURLs: await Promise.all(queries),
            total_count: total_count
        }
        // return [data,Promise.all(queries)];
    }).then(data => {
        res.status(200).json({
            status: true, 
            message: "Success",
            count: data.total_count,
            data: data.documentsWithURLs
        });
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
*@author Asma
*@date 21/05/2019 15:22
Method to delete documents
*/
let deleteDocuments = (req, res) => {
    let body = req.body;
    let companyId = body.companyId
    let userId = body.userId
    return new Promise( (resolve, reject) => {
        if (!companyId) reject("companyId is required");
        else if (!userId) reject("userId is required");
        else resolve(null);
    }).then(() => {
        return DocumentsModel.find({companyId: companyId, userId: userId, _id : {$in: body._ids}}).lean().exec()
    }).then((data) => {
        let queries = [];
        data.forEach(function (each) {
            queries.push(s3.deleteFile('documents/'+each.s3Path))
        });
        return Promise.all(queries);
    }).then(data => {
        console.log(data)
        DocumentsModel.deleteMany({companyId: companyId, userId: userId, _id : {$in: body._ids}})
            .then(response => {
                res.status(200).json({status: true, message: "Success"});
            })
            .catch(err => {
                res.status(400).json({status: false, message: "Error while deleting documents", error : err});
            });
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};


module.exports = {
    uploadDocument,
    getDocumentDetails,
    getUploadedDocuments,
    deleteDocuments,
    editDocuments
}