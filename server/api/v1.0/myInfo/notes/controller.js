let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../../config/config');
let utils = require('./../../../../common/utils');
let helpers = require('./../../../../common/helpers');
let _ = require('underscore');
let NotesModel = require('./model');

/**
*@author Asma
*@date 11/05/2019 09:35
API to add note on a user's profile by another user
*/
let addNote = (req,res) => {
    let body = req.body
    return new Promise((resolve,reject) => {
        if (body.note && body.note.length == 0 ) reject('Please enter a note');
        resolve(null);
    })
    .then(() => {
        NotesModel.create(body)
            .then(note => {
                note.save(function (err, response) {
                    if (!err) {
                        res.status(200).json({
                            status: true,
                            message: "Added note successfully",
                            data: response
                        })
                    }
                    else {
                        res.status(400).json({
                            status: false,
                            message: "Error while adding note",
                            error: err
                        })
                    }
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while adding note",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while adding note",
            error: err
        })
    });
}

/**
*@author Asma
*@date 11/05/2019 09:36
API to get one note's details
*/
let getNote = (req, res) => {
    let companyId = req.params.company_id
    let userId = req.params.user_id
    let note_id = req.params.note_id
    return new Promise((resolve, reject) => {
        if (!companyId) {
            reject('company_id is required')
        }
        else if (!userId) {
            reject('user_id is required')
        }
        else if (!note_id) {
            reject('note_id is required')
        }
        else {
            resolve(null);
        }
    })
    .then(()=> {
        NotesModel.findOne({
            _id: note_id,
            companyId: companyId,
            userId : userId
        })
        .then(result => {
            if(result) {
                res.status(200).json({
                    status: true,
                    message: "Successfully fecthed note",
                    data: result
                })
            }
            else {
                res.status(404).json({
                    status: false,
                    message: "note _id not found",
                    data: null
                })
            }
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while fetching note",
                error: err
            })
        });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while fetching note",
            error: err
        })
    })
}

/**
*@author Asma
*@date 11/05/2019 09:38
API to get list of notes of user
*/
let listNotes = (req, res ) => {
    let body = req.body
    return new Promise((resolve, reject) => {
        if (!body.companyId) {
            reject('companyId is required')
        }
        else if (!body.userId) {
            reject('userId is required')
        }
        else {
            resolve(null);
        }
    })
    .then(() => {
        // let per_page = body.per_page ? body.per_page : 10
        // let page_no = body.page_no ? body.page_no : 1
        let sort = {
            updatedAt : -1
        }
        let filter = {
            companyId: body.companyId,
            userId : body.userId,
            // archive: body.archive
        }

        if (body.hasOwnProperty('archive')) {
            filter['archive'] = body.archive
        }
        // else {
        //     filter['archive'] = false
        // }

        NotesModel.find(filter).populate('addedBy')
            .sort(sort)
            // .skip((page_no - 1) * per_page)
            // .limit(per_page)
            .lean().exec()
            .then(notes => {

                NotesModel.count(filter)
                    .then(count => {
                        res.status(200).json({
                            status: true, message: "Success", total_count: count, data: notes
                        })
                    })
                    .catch(err => {
                        res.status(400).json({
                            status: false,
                            message: "Error while fetching notes",
                            error: err
                        })
                    })
                
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while fetching notes",
                    error: err
                })
            });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while fetching notes",
            error: err
        })
    })
}

/**
*@author Asma
*@date 11/05/2019 09:40
API to edit a note
*/
let editNote = (req,res) => {
    let body = req.body
    let noteId = req.params.note_id
    let companyId = body.companyId
    let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!noteId) {
            reject('noteId is required')
        }
        else if (!body.note) {
            reject('note is required')
        }
        else if (!companyId) {
            reject('companyId is required')
        }
        else if (!userId) {
            reject('userId is required')
        }
        else {
            resolve(null);
        }
    })
    .then(() => {
        delete body.companyId
        delete body.userId
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        delete body.AddedBy
        delete body.archive
        console.log(body)
        let filter = {
            _id: ObjectID(noteId), 
            companyId : ObjectID(companyId), 
            userId: ObjectID(userId)
        }
        console.log(filter)
        NotesModel.findOneAndUpdate(filter, {$set: body}, {new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                res.status(200).json({
                    status: true,
                    message: "Note updated successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while updating note",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while updating note",
            error: err
        })
    });
}

/**
*@author Asma
*@date 11/05/2019 09:40
API to delete a note
*/
let deleteNote = (req, res) => {
    let noteId = req.params.note_id
    let companyId = req.params.company_id
    let userId = req.params.user_id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        NotesModel.findOneAndDelete({
            companyId: companyId,
            userId: userId,
            _id: noteId
        })
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting notes",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while deleting notes",
            error: err
        })
    })
}

/**
*@author Asma
*@date 11/05/2019 09:40
API to change archive status of a note
*/
let changeStatus = ( req, res) => {
    let body = req.body
    let noteId = req.params.note_id
    let companyId = body.companyId
    let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!noteId) {
            reject('noteId is required')
        }
        else if (!body.hasOwnProperty("archive")) {
            reject('archive field is required')
        }
        else if (!companyId) {
            reject('companyId is required')
        }
        else if (!userId) {
            reject('userId is required')
        }
        else {
            resolve(null);
        }
    })
    .then(()=> {
        delete body.companyId
        delete body.userId
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        delete body.AddedBy
        delete body.note
        let filter = {
            _id: ObjectID(noteId), 
            companyId : ObjectID(companyId), 
            userId: ObjectID(userId)
        }
        console.log(filter)
        NotesModel.findOneAndUpdate(filter, {$set: body}, {new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                res.status(200).json({
                    status: true,
                    message: "Changed status successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while changing status",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while changing status",
            error: err
        })
    })
}


module.exports = {
    addNote,
    getNote,
    editNote,
    deleteNote,
    listNotes,
    changeStatus
}