let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('../../../../common/constants');

let Schema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    userId: {type: ObjectID, ref: 'users', required: true},
    addedBy: { type: ObjectID, ref: 'users', required: true },
    note: {
        type: String,
        required: true
    },
    archive: {
        type: Boolean,
        default: false
    }
},{
    timestamps: true,
    versionKey: false,
    strict: false
});

Schema.index({ _id: 1, companyId: 1, userId: 1 });

Schema.index({ companyId: 1, userId: 1 });

let NotesModel = mongoose.model('notes',Schema);

module.exports = NotesModel;