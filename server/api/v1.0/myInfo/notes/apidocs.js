/**
 * @api {post} /myinfo/notes/add AddNotes
 * @apiName AddNotes
 * @apiGroup Notes
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cd10c7ff301f75b4276f408",
	"addedBy": "5cd10f7c3eb4985b72e8206e",
	"note": "The meeting is postponed to 5PM"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Added note successfully",
    "data": {
        "archive": false,
        "_id": "5cd63e85a7b22917b45e2a85",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd10c7ff301f75b4276f408",
        "addedBy": "5cd10f7c3eb4985b72e8206e",
        "note": "The meeting is postponed to 5PM",
        "createdAt": "2019-05-11T03:16:21.825Z",
        "updatedAt": "2019-05-11T03:16:21.825Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /myinfo/notes/get/:company_id/:user_id/:note_id GetNote
 * @apiName GetNote
 * @apiGroup Notes
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} user_id Users unique _id
 * @apiParam {ObjectId} note_id Notes unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fecthed note",
    "data": {
        "archive": false,
        "_id": "5cd63e85a7b22917b45e2a85",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd10c7ff301f75b4276f408",
        "addedBy": "5cd10f7c3eb4985b72e8206e",
        "note": "The meeting is postponed to 5PM",
        "createdAt": "2019-05-11T03:16:21.825Z",
        "updatedAt": "2019-05-11T03:16:21.825Z"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /myinfo/notes/getAll GetAllNotes
 * @apiName GetAllNotes
 * @apiGroup Notes
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cd10c7ff301f75b4276f408"
}
 * @apiParamExample {json} Request-Example2:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "userId": "5cd10c7ff301f75b4276f408",
    "archive": true
}
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Success",
    "total_count": 1,
    "data": [
        {
            "_id": "5cd63e85a7b22917b45e2a85",
            "archive": false,
            "companyId": "5c9e11894b1d726375b23058",
            "userId": "5cd10c7ff301f75b4276f408",
            "addedBy": "5cd10f7c3eb4985b72e8206e",
            "note": "The meeting is postponed to 5PM",
            "createdAt": "2019-05-11T03:16:21.825Z",
            "updatedAt": "2019-05-11T03:16:21.825Z"
        }
    ]
}
 * @apiSuccessExample Success-Response2:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Success",
    "total_count": 0,
    "data": []
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /myinfo/notes/edit/:note_id EditNote
 * @apiName EditNote
 * @apiGroup Notes
 * 
 * @apiParam {ObjectId} note_id Notes unique _id
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "userId": "5cd10c7ff301f75b4276f408",
    "note": "The meeting is postponed to 6PM"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Note updated successfully",
    "data": {
        "archive": false,
        "_id": "5cd63e85a7b22917b45e2a85",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd10c7ff301f75b4276f408",
        "addedBy": "5cd10f7c3eb4985b72e8206e",
        "note": "The meeting is postponed to 6PM",
        "createdAt": "2019-05-11T03:16:21.825Z",
        "updatedAt": "2019-05-11T04:45:15.271Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {delete} /myinfo/notes/delete/:company_id/:user_id/:note_id DeleteNote
 * @apiName DeleteNote
 * @apiGroup Notes
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} user_id Users unique _id
 * @apiParam {ObjectId} note_id Notes unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "archive": false,
        "_id": "5cd657765806c708587344f2",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd10c7ff301f75b4276f408",
        "addedBy": "5cd10f7c3eb4985b72e8206e",
        "note": "Please enter all the details in your profile",
        "createdAt": "2019-05-11T05:02:46.904Z",
        "updatedAt": "2019-05-11T05:02:46.904Z"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /myinfo/notes/change-status/:note_id ChangeStatus
 * @apiName ChangeStatus
 * @apiGroup Notes
 * 
 * @apiParam {ObjectId} note_id Notes unique _id
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"archive": true,
    "companyId": "5c9e11894b1d726375b23058",
    "userId": "5cd10c7ff301f75b4276f408"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Changed status successfully",
    "data": {
        "archive": true,
        "_id": "5cd657905806c708587344f3",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd10c7ff301f75b4276f408",
        "addedBy": "5cd10f7c3eb4985b72e8206e",
        "note": "Lets go for a team outing next week?",
        "createdAt": "2019-05-11T05:03:12.148Z",
        "updatedAt": "2019-05-11T05:14:44.433Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */
