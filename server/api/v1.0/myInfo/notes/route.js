let express = require('express');

let router = express.Router();

let authMiddleware = require('../../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../../middlewares/validations');

router.post(
    '/add', 
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.addNote
);

router.get(
    "/get/:company_id/:user_id/:note_id",
    authMiddleware.isUserLogin,
    Controller.getNote
);

router.post(
    "/getAll",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.listNotes
);

router.put(
    "/edit/:note_id",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editNote
);

router.delete(
    "/delete/:company_id/:user_id/:note_id",
    authMiddleware.isUserLogin,
    Controller.deleteNote
);

router.put(
    "/change-status/:note_id",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.changeStatus
);


module.exports = router;