let express = require('express');

let router = express.Router();

router.use('/emergencyContact', require('./emergencyContacts/route'));

router.use('/notes', require('./notes/route'));

router.use('/assets', require('./assets/route'));

router.use('/compensation', require('./compensation/route'));

router.use('/job', require('./job/route'));

router.use('/onboarding', require('./onboarding/route'));

router.use('/documents', require('./documents/route'));

router.use('/personal', require('./personal/route'));

router.use('/custom', require('./custom/route'));

router.use('/offboarding', require('./offboarding/route'));

router.use('/myinfotimeschedule',require('./timeschedule/route'));

module.exports = router;