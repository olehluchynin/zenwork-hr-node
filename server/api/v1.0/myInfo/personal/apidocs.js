/**
 * @api {post} /myinfo/personal/deleteVisaInformation DeleteVisaInformation
 * @apiName DeleteVisaInformation
 * @apiGroup Personal
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cdec1d5df4fd06b83354eb8",
	"_ids": [
		"5ce397ad9ee35d233019523e"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted visa information successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

//======================================================================================================

/**
 * @api {put} /myinfo/personal/updateVisaInformation UpdateVisaInformation
 * @apiName UpdateVisaInformation
 * @apiGroup Personal
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cdf0dc1df4fd06b83354f2a",
	"visaId": "5ce2d2ca1b4a170f16e73777",
	"visa_type": "V-1",
    "issuing_country": "USA",
    "issued_date": "2019-05-21T06:13:36.495Z",
    "expiration_date": "2020-06-21T06:13:36.495Z",
    "status": "Active",
    "notes": "I will need sponsorship for visa next year"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Updated visa information successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

//================================================================================================================

/**
 * @api {post} /myinfo/personal/deleteEducation DeleteEducation
 * @apiName DeleteEducation
 * @apiGroup Personal
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cdec1d5df4fd06b83354eb8",
	"_ids": [
		"5ce3c4473ce7631a2c3d2ca5",
		"5ce3da033ce7631a2c3d2d3d"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted education successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

//========================================================================================================

/**
 * @api {put} /myinfo/personal/updateEducation UpdateEducation
 * @apiName UpdateEducation
 * @apiGroup Personal
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cdec1d5df4fd06b83354eb8",
	"eduId": "5cded1eedf4fd06b83354f29",
    "university": "Wolverhampton",
    "degree": "Bachelors",
    "specialization": "Electronics",
    "gpa": 9,
    "start_date": "2014-01-04T18:30:00.000Z",
    "end_date": "2018-06-12T18:30:00.000Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Updated education successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

//================================================================================================================

/**
 * @api {post} /myinfo/personal/deleteLanguages DeleteLanguages
 * @apiName DeleteLanguages
 * @apiGroup Personal
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cdec1d5df4fd06b83354eb8",
	"_ids": [
		"5ce3dae33ce7631a2c3d2d43",
		"5ce2dd041b4a170f16e7377b"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted languages successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

//========================================================================================================

/**
 * @api {put} /myinfo/personal/updateLanguage UpdateLanguage
 * @apiName UpdateLanguage
 * @apiGroup Personal
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cdec1d5df4fd06b83354eb8",
	"languageId": "5cded1eedf4fd06b83354f28",
	"language": "German",
    "level_of_fluency": "Intermediate"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Updated languages successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {post} /myinfo/personal/upload UploadPhoto
 * @apiName UploadPhoto
 * @apiGroup Personal
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {multipart/form-data} Request-Example:
 *
{
    file=@<file path>
    data={
    "companyId": "5c9e11894b1d726375b23058",
    "userId": "5cda3d4a2fec3c2610219939"
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Profile pic uploaded successfully",
    "data": {
        "originalFilename": "Capture.JPG",
        "s3Path": "5d079dc0211e63024813e590__Capture.JPG",
        "url": "https://zenworkhr-docs.s3.amazonaws.com/profile_photos/5d079dc0211e63024813e590__Capture.JPG?AWSAccessKeyId=AKIAQQWFVNX2IASWO5S7&Expires=1560866625&Signature=qWOIyGVsKlZWnuazWLDWLviacMA%3D"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */