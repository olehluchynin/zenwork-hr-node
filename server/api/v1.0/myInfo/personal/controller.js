let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../../config/config');
let _ = require('underscore');
let User = require('./../../users/model');
let s3 = require('./../../../../common/s3/s3');


/**
*@author Asma
*@date 23/05/2019 10:32
Method to edit visa details - individual sub document
*/
let editVisaDetails = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let userId = body.userId
    let visaId = body.visaId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else if (!userId) reject('userId is required');
        else if (!visaId) reject('visaId is requried')
        else resolve(null);
    })
    .then(() => {
        delete body.companyId
        delete body.userId
        delete body.visaId
        let update_filter = {
            companyId: companyId,
            _id: userId,
            "personal.visa_information._id" : visaId
        }
        console.log(body)
        var updateObj = {};
        for (let key in body) {
            updateObj['personal.visa_information.$.' + key] = body[key]
        }
        console.log(updateObj)
        User.updateOne(update_filter, {
            $set: updateObj
        }, {new: true})
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Updated visa information successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while updating visa information",
                error : err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err,
            data: null
        })
    })
}

/**
*@author Asma
*@date 23/05/2019 10:33
Method to delete visa info - individual sub document
*/
let deleteVisaInformation = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else if (!userId) reject('userId is required');
        else resolve(null);
    })
    .then(() => {
        let update_filter = {
            companyId: companyId,
            _id: userId
        }
        User.updateOne(update_filter, {
            $pull: {
                'personal.visa_information' : {_id: {$in : body._ids}}
            }
        }, {new: true})
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted visa information successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting visa information",
                error : err
            })
        });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err,
            data: null
        })
    })
}

/**
*@author Asma
*@date 23/05/2019 10:33
Method to edit education info - individual sub document
*/
let editEducation = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let userId = body.userId
    let eduId = body.eduId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else if (!userId) reject('userId is required');
        else if (!eduId) reject('eduId is requried')
        else resolve(null);
    })
    .then(() => {
        delete body.companyId
        delete body.userId
        delete body.eduId
        let update_filter = {
            companyId: companyId,
            _id: userId,
            "personal.education._id" : eduId
        }
        console.log(body)
        var updateObj = {};
        for (let key in body) {
            updateObj['personal.education.$.' + key] = body[key]
        }
        console.log(updateObj)
        User.updateOne(update_filter, {
            $set: updateObj
        }, {new: true})
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Updated education successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while updating education",
                error : err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err,
            data: null
        })
    })
}

/**
*@author Asma
*@date 23/05/2019 10:33
Method to delete education info - individual sub document
*/
let deleteEducation = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else if (!userId) reject('userId is required');
        else resolve(null);
    })
    .then(() => {
        let update_filter = {
            companyId: companyId,
            _id: userId
        }
        User.updateOne(update_filter, {
            $pull: {
                'personal.education' : {_id: {$in : body._ids}}
            }
        }, {new: true})
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted education successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting education",
                error : err
            })
        });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err,
            data: null
        })
    })
}

/**
*@author Asma
*@date 23/05/2019 10:34
Method to edit languages info - individual sub document
*/
let editLanguage = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let userId = body.userId
    let languageId = body.languageId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else if (!userId) reject('userId is required');
        else if (!languageId) reject('languageId is requried')
        else resolve(null);
    })
    .then(() => {
        delete body.companyId
        delete body.userId
        delete body.languageId
        let update_filter = {
            companyId: companyId,
            _id: userId,
            "personal.languages._id" : languageId
        }
        console.log(body)
        var updateObj = {};
        for (let key in body) {
            updateObj['personal.languages.$.' + key] = body[key]
        }
        console.log(updateObj)
        User.updateOne(update_filter, {
            $set: updateObj
        }, {new: true})
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Updated languages successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while updating languages",
                error : err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err,
            data: null
        })
    })
}

/**
*@author Asma
*@date 23/05/2019 10:34
Method to delete languages info - individual sub document
*/
let deleteLanguages = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else if (!userId) reject('userId is required');
        else resolve(null);
    })
    .then(() => {
        let update_filter = {
            companyId: companyId,
            _id: userId
        }
        User.updateOne(update_filter, {
            $pull: {
                'personal.languages' : {_id: {$in : body._ids}}
            }
        }, {new: true})
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted languages successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting languages",
                error : err
            })
        });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err,
            data: null
        })
    })
}

let uploadPhoto = (req, res) => {
    console.log(req.body);
    console.log(req.files);
    let _id;
    let file;
    let s3Path;
    let body = JSON.parse(req.body.data);
    let companyId = body.companyId
    let userId = body.userId
    return new Promise( (resolve, reject) => {
        if(!req.files.file) reject("File not found.");
        else if (!companyId) reject("companyId is required");
        else if (!userId) reject("userId is required");
        // else if (req.files.file && req.files.file.type)
        else {
            let find_query = {
                _id: userId,
                companyId: companyId
            }
            resolve(User.findOne(find_query));
        }
    }).then(user => {
        _id = mongoose.Types.ObjectId();
        file = req.files.file;
        s3Path = 'profile_photos/'+_id+'__'+file.originalFilename;
        if(user.personal.profile_pic_details.s3Path) {
            console.log('here1')
            return Promise.all([s3.deleteFile('profile_photos/' + user.personal.profile_pic_details.s3Path),s3.uploadFile(s3Path,file.path)]);
        }
        else {
            console.log('here2')
            return s3.uploadFile(s3Path,file.path);
        }
    }).then(data => {
        let find_query = {
            _id: userId,
            companyId: companyId
        }
        let doc = {
            "personal.profile_pic_details": {
                originalFilename: file.originalFilename,
                s3Path: _id+'__'+file.originalFilename
            }
        };
        User.findOneAndUpdate(find_query, {$set: doc}, {new: true})
            .then(async user => {
                if (user) {
                    user = user.toJSON()
                    // console.log(user)
                    console.log(user.personal.profile_pic_details)
                    let user_profile_pic_details_with_url = await s3.getPresignedUrlOfAfile('zenworkhr-docs', 'profile_photos', user.personal.profile_pic_details)
                    res.status(200).json({
                        status: true,
                        message: "Profile pic uploaded successfully",
                        data: user_profile_pic_details_with_url
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "user not found"
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while uploading profile pic",
                    error: err
                })
            })
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
}

module.exports = {
    editVisaDetails,
    deleteVisaInformation,
    editEducation,
    deleteEducation,
    editLanguage,
    deleteLanguages,

    uploadPhoto
}