let express = require('express');

let router = express.Router();

let authMiddleware = require('../../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../../middlewares/validations');

var multipart = require('connect-multiparty');

var multipartMiddleware = multipart();

router.put(
    "/updateVisaInformation",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editVisaDetails
);

router.post(
    "/deleteVisaInformation",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deleteVisaInformation
);

router.put(
    "/updateEducation",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editEducation
);

router.post(
    "/deleteEducation",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deleteEducation
);

router.put(
    "/updateLanguage",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editLanguage
);

router.post(
    "/deleteLanguages",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deleteLanguages
);

router.post(
    "/upload",
    authMiddleware.isUserLogin,
    multipartMiddleware,
    Controller.uploadPhoto
);

module.exports = router;