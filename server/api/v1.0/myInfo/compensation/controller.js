let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../../config/config');
let _ = require('underscore');
let User = require('./../../users/model');

/**
*@author Asma
*@date 17/05/2019 07:36
API to edit current compensation of an employee
*/
let editCurrentCompensation = (req, res) => {
    let body = req.body
    let userId = body.userId
    let companyId = body.companyId
    let cur_comp_id = body.cur_comp_id
    return new Promise((resolve, reject) => {
        if (!userId) {
            reject('userId is required');
        }
        else if (!cur_comp_id) {
            reject('current compensation Id "cur_comp_id" is requried');
        }
        else if (!companyId) {
            reject('companyId is requried');
        }
        else {
            resolve(null);
        }
    })
        .then(() => {
            delete body._id
            delete body.cur_comp_id
            delete body.companyId
            delete body.userId
            console.log(body)
            var updateObj = {};
            for (let key in body) {
                updateObj['compensation.current_compensation.$.' + key] = body[key]
            }
            console.log(updateObj)
            User.findOneAndUpdate(
                {
                    _id: ObjectID(userId),
                    companyId: ObjectID(companyId),
                    'compensation.current_compensation._id': ObjectID(cur_comp_id)
                }, 
                {
                    $set: updateObj
                }, 
                { new: true }
            )
            .then(response => {
                delete response.password
                res.status(200).json({
                    status: true,
                    message: "updated current compensation successfully",
                    data: response
                })
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while updating current compensation",
                    error: err
                });
            })
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err,
                data: null
            });
        })
}

module.exports = {
    editCurrentCompensation
}