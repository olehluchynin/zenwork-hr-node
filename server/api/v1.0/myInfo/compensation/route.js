let express = require('express');

let router = express.Router();

let authMiddleware = require('../../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../../middlewares/validations');

router.put(
    "/updateCurrentCompensation",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editCurrentCompensation
);

module.exports = router;