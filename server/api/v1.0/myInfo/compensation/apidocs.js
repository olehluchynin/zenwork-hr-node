/**
 * @api {put} /myinfo/compensation/updateCurrentCompensation UpdateCurrentCompensation
 * @apiName UpdateCurrentCompensation
 * @apiGroup Compensation
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cda3ad10e57b225e1240c9d",
	"cur_comp_id": "5cda3ad10e57b225e1240ca0",
	"effective_date" : "2019-05-19T18:30:00.000+0000", 
    "payRate" : {
        "amount" : 18
    }, 
    "Pay_frequency" : "Monthly", 
    "changeReason" : "--", 
    "notes" : "--"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "updated current compensation successfully",
    "data": {
        "personal": {
            "name": {
                "firstName": "Slyvia",
                "middleName": "Marie",
                "lastName": "Claus",
                "preferredName": "Slyvia"
            },
            "address": {
                "street1": "wejhtuhiothiefnvk",
                "street2": "lfpg9tuyu9u9tjv",
                "city": "Manhattan",
                "state": "NewYork",
                "zipcode": "465493",
                "country": "United States"
            },
            "contact": {
                "workPhone": "277935475934679",
                "extention": "43564",
                "mobilePhone": "74325443659034",
                "homePhone": "4373469491",
                "workMail": "Slyvia@gmail.com",
                "personalMail": "Slyvia@gmail.com"
            },
            "dob": "1994-02-09T18:30:00.000Z",
            "gender": "female",
            "maritalStatus": "Single",
            "effectiveDate": "2019-05-14T18:30:00.000Z",
            "veteranStatus": "Single",
            "ssn": "37344245266623",
            "ethnicity": "Ethnicity11",
            "education": [],
            "languages": [],
            "visa_information": []
        },
        "job": {
            "current_employment_status": {
                "status": "Leave",
                "effective_date": "2019-05-15T03:49:37.209Z"
            },
            "Specific_Workflow_Approval": "No",
            "employeeId": "99573",
            "employeeType": "Full Time",
            "hireDate": "2019-05-14T18:30:00.000Z",
            "jobTitle": "Software Engineer",
            "department": "Tech",
            "location": "Banglore",
            "businessUnit": "Unit 1",
            "unionFields": "Union 1",
            "FLSA_Code": "Exempt",
            "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
            "Site_AccessRole": "5cd922b7f308c21c43e50560",
            "VendorId": "325567950",
            "employment_status_history": [
                {
                    "_id": "5cda3ad10e57b225e1240c9e",
                    "status": "Active",
                    "effective_date": "2019-05-14T03:49:37.209Z"
                },
                {
                    "_id": "5cdba6a58ed19a301ececd22",
                    "status": "Leave",
                    "effective_date": "2019-05-15T03:49:37.209Z"
                }
            ],
            "job_information_history": [
                {
                    "_id": "5cda3ad10e57b225e1240c9f",
                    "jobTitle": "Software Engineer",
                    "department": "Tech",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "effective_date": "2019-05-14T03:49:37.209Z"
                }
            ]
        },
        "compensation": {
            "NonpaidPosition": false,
            "Pay_group": "Group2",
            "Pay_frequency": "Weekly",
            "Salary_Grade": "A",
            "compensationRatio": "23",
            "current_compensation": [
                {
                    "payRate": {
                        "amount": 18
                    },
                    "_id": "5cda3ad10e57b225e1240ca0",
                    "effective_date": "2019-05-19T18:30:00.000Z",
                    "Pay_frequency": "Monthly",
                    "changeReason": "--",
                    "notes": "--"
                }
            ],
            "compensation_history": []
        },
        "module_settings": {
            "eligible_for_benifits": false,
            "assign_leave_management_rules": false,
            "assign_performance_management": false,
            "assaign_employee_number": "System Generated",
            "onboarding_template": "5cda3ad10e57b225e1240ca3"
        },
        "status": "active",
        "groups": [],
        "_id": "5cda3ad10e57b225e1240c9d",
        "email": "Slyvia@gmail.com",
        "companyId": "5c9e11894b1d726375b23058",
        "type": "employee",
        "password": "Dt5ZZM8R",
        "createdAt": "2019-05-14T03:49:37.243Z",
        "updatedAt": "2019-05-16T02:38:39.610Z",
        "userId": 94
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */