let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../../config/config');
let _ = require('underscore');
let User = require('./../../users/model');
const OffBoardingModel = require('./../../offboarding/model').OffBoardingModel;

let getUserOffboardingDetails = (req, res) => {
    let companyId = req.params.company_id
    let templateId = req.params.template_id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        OffBoardingModel.findOne({
            _id: ObjectID(templateId),
            companyId: ObjectID(companyId),
            templateType: "derived"
        })
        .lean()
        .exec()
        .then(template => {
            
            let categories = Object.keys(template['offBoardingTasks'])
            if (!template.assigned_date) {
                template.assigned_date = template['offBoardingTasks'][categories[0]][0]['task_assigned_date']
            }
            if (!template.offboarding_complete) {
                template.offboarding_complete = 'No'
            }
            res.status(200).json({
                status: true,
                message: "Successfully fetched offboarding template",
                data: template
            });
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: "Error while fetching offboarding template",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let offboardingAdminOverride = (req, res) => {
    let companyId = req.jwt.companyId
    let userId = ObjectID(req.params.user_id)
    return new Promise((resolve, reject) => {
        resolve(User.findOne({companyId: companyId, _id: userId}, {email: 1, 'job.off_boarding_template_assigned': 1}).lean().exec());
    })
    .then(async user_details => {
        // set offboarding complete to yes
        // set all offboarding tasks statuses to completed
        console.log('user_details - ', user_details)
        let promises = []
        let filter = {
            companyId: companyId, 
            _id: user_details.job.off_boarding_template_assigned
        }
        let template = await OffBoardingModel.findOne(filter).lean().exec()
        console.log('template - ', template)
        if (template) {
            promises.push(
                OffBoardingModel.findOneAndUpdate(filter, {$set: {
                        offboarding_complete: 'Yes',
                        completion_date: new Date()
                    }
                })
            )
    
            for (let [task_category, tasks_array] of Object.entries(template['offBoardingTasks'])) {
                for (let task of tasks_array) {
                    let update_task_filter = {
                        companyId: companyId,
                        _id: user_details.job.off_boarding_template_assigned
                    }
                    update_task_filter['offBoardingTasks.' + task_category + '.' + '_id'] = task._id
                    let updateObj = {}
                    updateObj['$set'] = {}
                    updateObj['$set']['offBoardingTasks.' + task_category + ".$.status"] = 'Completed'
                    updateObj['$set']['offBoardingTasks.' + task_category + ".$.task_completion_date"] = new Date()
                    promises.push(
                        OffBoardingModel.updateOne(update_task_filter, updateObj, {new: true})
                    )
                }
            }
    
            Promise.all(promises)
                .then(responses => {
                    console.log('offboarding admin override - ', responses)
                    res.status(200).json({
                        status: true,
                        message: "Admin override success"
                    })
                })
                .catch(err => {
                    console.log(err)
                    res.status(400).json({
                        status: false,
                        message: "Error",
                        error: err
                    })
                })
        }
        else {  
            res.status(404).json({
                status: false,
                message: "Offboarding template is not yet assigned to this employee"
            })
        }
        
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: "Error",
            error: err
        })
    })
}

module.exports = {
    getUserOffboardingDetails,
    offboardingAdminOverride
}