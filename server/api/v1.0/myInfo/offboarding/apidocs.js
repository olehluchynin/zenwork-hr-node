/**
 * @api {get} /myinfo/offboarding/get-offboarding-details/:company_id/:template_id GetOffboardingDetails
 * @apiName GetOffboardingDetails
 * @apiGroup MyInfoOffboarding
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} template_id Offboarding template unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched offboarding template",
    "data": {
        "_id": "5ce654f6bee9b42bc9f15f35",
        "templateType": "derived",
        "offBoardingTasks": {
            "HrTasks": [
                {
                    "_id": "5ce654f6bee9b42bc9f15f32",
                    "timeToComplete": {
                        "time": 4,
                        "unit": "Day"
                    },
                    "taskName": "HR Task",
                    "taskAssignee": "5cd91e68f308c21c43e50559",
                    "taskDesription": "Sample Description",
                    "category": "HR",
                    "task_assigned_date": "2019-05-23T08:08:22.876Z",
                    "Date": "2019-05-01T09:48:50.507Z",
                    "isChecked": false,
                    "status": "To do"
                }
            ],
            "ManagerTasks": [
                {
                    "_id": "5ce654f6bee9b42bc9f15f33",
                    "timeToComplete": {
                        "time": 4,
                        "unit": "Day"
                    },
                    "taskName": "Manager type",
                    "taskAssignee": "5cd93e4a8e9dea1f4bb987dc",
                    "taskDesription": "Sample Description",
                    "category": "Manager",
                    "task_assigned_date": "2019-05-23T08:08:22.876Z",
                    "Date": "2019-05-01T09:48:50.507Z",
                    "assigneeEmail": "info@asma.com",
                    "status": "To do"
                }
            ],
            "ITSetup": [
                {
                    "_id": "5ce654f6bee9b42bc9f15f34",
                    "timeToComplete": {
                        "time": 4,
                        "unit": "Day"
                    },
                    "taskName": "IT Task",
                    "taskAssignee": "5cd91e68f308c21c43e50559",
                    "taskDesription": "Sample Description",
                    "category": "IT",
                    "task_assigned_date": "2019-05-23T08:08:22.876Z",
                    "Date": "2019-05-01T09:48:50.507Z",
                    "isChecked": false,
                    "status": "To do"
                }
            ]
        },
        "companyId": "5c9e11894b1d726375b23058",
        "templateName": "sample template",
        "createdBy": "5cd91e68f308c21c43e50559",
        "updatedBy": "5cd91e68f308c21c43e50559",
        "assigned_date": "2019-05-23T08:08:22.875Z",
        "offboarding_complete": "No",
        "createdAt": "2019-05-23T08:08:22.881Z",
        "updatedAt": "2019-05-23T08:08:22.881Z",
        "otId": 66
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /myinfo/offboarding/adminOverride/:user_id AdminOverride
 * @apiName AdminOverride
 * @apiGroup MyInfoOffboarding
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} user_id Users unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    status: true,
    message: "Admin override success"
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */