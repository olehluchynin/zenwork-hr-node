let express = require('express');

let router = express.Router();

let authMiddleware = require('../../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../../middlewares/validations');

router.get(
    "/get-offboarding-details/:company_id/:template_id",
    authMiddleware.isUserLogin,
    Controller.getUserOffboardingDetails
);

router.get(
    '/adminOverride/:user_id',
    authMiddleware.isUserLogin,
    authMiddleware.isCompany,
    Controller.offboardingAdminOverride
);

module.exports = router;