let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('../../../../common/constants');

let Schema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    userId: {type: ObjectID, ref: 'users', required: true},
    category: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true 
    },
    serial_number: {
        type: String,
        required: true
    },
    date_loaned: {
        type: Date,
        required: true
    },
    date_returned: {
        type: Date
    }
},{
    timestamps: true,
    versionKey: false,
    strict: false
});

Schema.index({ _id: 1, companyId: 1, userId: 1 });

Schema.index({ companyId: 1, userId: 1 });

// Schema.index({companyId: 1, userId: 1, serial_number: 1}, {unique: true})

let AssetsModel = mongoose.model('assets',Schema);

module.exports = AssetsModel;