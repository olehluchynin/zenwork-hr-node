/**
 * @api {post} /myinfo/assets/add AddAssets
 * @apiName AddAssets
 * @apiGroup Assets
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cd10c7ff301f75b4276f408",
	"category": "Computer Accessories",
	"description": "Keyboard",
	"serial_number": "243450",
	"date_loaned": "2019-05-12T06:08:32.432Z",
	"date_returned": "2019-05-12T06:08:32.432Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Added asset successfully",
    "data": {
        "_id": "5cd7b8891e9b5134a085710e",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd10c7ff301f75b4276f408",
        "category": "Computer Accessories",
        "description": "Keyboard",
        "serial_number": "243450",
        "date_loaned": "2019-05-12T06:08:32.432Z",
        "date_returned": "2019-05-12T06:08:32.432Z",
        "createdAt": "2019-05-12T06:09:13.154Z",
        "updatedAt": "2019-05-12T06:09:13.154Z"
    }
}
 * @apiError SerialNumber serial number already exists
 * @apiErrorExample Serial number already exists:
 * HTTP/1.1 400 Bad Request
{
    "status": false,
    "message": "Asset already exists with this serial number"
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /myinfo/assets/get/:company_id/:user_id/:asset_id GetAsset
 * @apiName GetAsset
 * @apiGroup Assets
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} user_id Users unique _id
 * @apiParam {ObjectId} asset_id Assets unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fecthed asset",
    "data": {
        "_id": "5cd7b8891e9b5134a085710e",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd10c7ff301f75b4276f408",
        "category": "Computer Accessories",
        "description": "Keyboard",
        "serial_number": "243450",
        "date_loaned": "2019-05-12T06:08:32.432Z",
        "date_returned": "2019-05-12T06:08:32.432Z",
        "createdAt": "2019-05-12T06:09:13.154Z",
        "updatedAt": "2019-05-12T06:09:13.154Z"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /myinfo/assets/edit/:asset_id EditAsset
 * @apiName EditAsset
 * @apiGroup Assets
 * 
 * @apiParam {ObjectId} asset_id Assets unique _id
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "_id": "5cd7b8891e9b5134a085710e",
    "companyId": "5c9e11894b1d726375b23058",
    "userId": "5cd10c7ff301f75b4276f408",
    "category": "Computer Accessories",
    "description": "Mouse",
    "serial_number": "243450",
    "date_loaned": "2019-05-12T06:08:32.432Z",
    "date_returned": "2019-05-12T06:08:32.432Z",
    "createdAt": "2019-05-12T06:09:13.154Z",
    "updatedAt": "2019-05-12T06:09:13.154Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Asset updated successfully",
    "data": {
        "_id": "5cd7b8891e9b5134a085710e",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd10c7ff301f75b4276f408",
        "category": "Computer Accessories",
        "description": "Mouse",
        "serial_number": "243450",
        "date_loaned": "2019-05-12T06:08:32.432Z",
        "date_returned": "2019-05-12T06:08:32.432Z",
        "createdAt": "2019-05-12T06:09:13.154Z",
        "updatedAt": "2019-05-12T06:20:04.669Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /myinfo/assets/getAll GetAllAssets
 * @apiName GetAllAssets
 * @apiGroup Assets
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "userId": "5cd10c7ff301f75b4276f408",
    "per_page": 10,
    "page_no": 1
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Success",
    "total_count": 1,
    "data": [
        {
            "_id": "5cd7b8891e9b5134a085710e",
            "companyId": "5c9e11894b1d726375b23058",
            "userId": "5cd10c7ff301f75b4276f408",
            "category": "Computer Accessories",
            "description": "Mouse",
            "serial_number": "243450",
            "date_loaned": "2019-05-12T06:08:32.432Z",
            "date_returned": "2019-05-12T06:08:32.432Z",
            "createdAt": "2019-05-12T06:09:13.154Z",
            "updatedAt": "2019-05-12T06:20:04.669Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /myinfo/assets/delete DeleteAssets
 * @apiName DeleteAssets
 * @apiGroup Assets
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "userId": "5cd10c7ff301f75b4276f408",
    "_ids": [
    	"5cd7b8891e9b5134a085710e"
    	]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted assets successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */