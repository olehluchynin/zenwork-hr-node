let express = require('express');

let router = express.Router();

let authMiddleware = require('../../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../../middlewares/validations');

router.post(
    '/add', 
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.addAsset
);

router.get(
    "/get/:company_id/:user_id/:asset_id",
    authMiddleware.isUserLogin,
    Controller.getAsset
);

router.post(
    "/getAll",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.listAssets
);

router.put(
    "/edit/:asset_id",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editAsset
);

router.post(
    "/delete",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deleteAssets
);


module.exports = router;