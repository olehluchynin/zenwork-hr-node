let express = require('express');

let router = express.Router();

let authMiddleware = require('../../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../../middlewares/validations');

router.get(
    "/get-onboarding-details/:company_id/:template_id",
    authMiddleware.isUserLogin,
    Controller.getUserOnboardingDetails
);

router.get(
    '/adminOverride/:user_id',
    authMiddleware.isUserLogin,
    authMiddleware.isCompany,
    Controller.onboardingAdminOverride
);

module.exports = router;