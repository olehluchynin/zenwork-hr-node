/**
 * @api {get} /myinfo/onboarding/get-onboarding-details/:company_id/:template_id GetOnboardingDetails
 * @apiName GetOnboardingDetails
 * @apiGroup MyInfoOnboarding
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} template_id Onboarding template unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched template",
    "data": {
        "_id": "5cda3d4a2fec3c261021993f",
        "templateType": "derived",
        "templateName": "India Sales team",
        "Onboarding Tasks": {
            "HR+sales": [
                {
                    "taskName": "Issue Bike ",
                    "taskAssignee": "asdfghjk@xcvbn.com",
                    "assigneeEmail": "asdfghjk@xcvbn.com",
                    "taskDesription": "The bike will be used by the sales team to visit the client locations",
                    "timeToComplete": {
                        "time": 2,
                        "unit": "Day"
                    },
                    "category": "HR+sales",
                    "_id": "5cda3d4a2fec3c261021993d",
                    "status": "To do",
                    "task_assigned_date": "2019-05-14T04:00:10.503Z"
                }
            ],
            "Company property": [
                {
                    "taskName": "Mobile phone",
                    "taskAssignee": "asma.mubeen@mtwlabs.com",
                    "assigneeEmail": "asma.mubeen@mtwlabs.com",
                    "taskDesription": "Issue mobile phone",
                    "timeToComplete": {
                        "time": 3,
                        "unit": "Week"
                    },
                    "category": "Company property",
                    "_id": "5cda3d4a2fec3c261021993e",
                    "status": "To do",
                    "task_assigned_date": "2019-05-14T04:00:10.504Z"
                }
            ]
        },
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-05-14T04:00:10.508Z",
        "updatedAt": "2019-05-14T04:00:10.508Z",
        "otId": 70,
        "assigned_date": "2019-05-14T04:00:10.503Z",
        "onboarding_complete": "No"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /myinfo/onboarding/adminOverride/:user_id AdminOverride
 * @apiName AdminOverride
 * @apiGroup MyInfoOnboarding
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} user_id Users unique _id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    status: true,
    message: "Admin override success"
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */