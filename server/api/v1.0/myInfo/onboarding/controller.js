let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../../config/config');
let _ = require('underscore');
let User = require('./../../users/model');
const OnboardingModel = require('./../../employee-management/onboarding/onboard-template/model');


/**
*@author Asma
*@date 23/05/2019 10:35
Method to get derived onboarding template/tasks info and other onboarding info of a user
*/
let getUserOnboardingDetails = (req, res) => {
    let companyId = req.params.company_id
    let templateId = req.params.template_id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        OnboardingModel.findOne({
            _id: ObjectID(templateId),
            companyId: ObjectID(companyId)
        })
        .lean()
        .exec()
        .then(template => {
            for(let category in template['Onboarding Tasks']) {
                if(template['Onboarding Tasks'].hasOwnProperty(category)) {
                    if(template['Onboarding Tasks'][category].length === 0) {
                        delete template['Onboarding Tasks'][category];
                    }
                }
            }
            let categories = Object.keys(template['Onboarding Tasks'])
            if (!template.assigned_date) {
                template.assigned_date = template['Onboarding Tasks'][categories[0]][0]['task_assigned_date']
            }
            if (!template.onboarding_complete) {
                template.onboarding_complete = 'No'
            }
            res.status(200).json({
                status: true,
                message: "Successfully fetched template",
                data: template
            });
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: "Error while fetching template",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let onboardingAdminOverride = (req, res) => {
    let companyId = req.jwt.companyId
    let userId = ObjectID(req.params.user_id)
    return new Promise((resolve, reject) => {
        resolve(User.findOne({companyId: companyId, _id: userId}, {email: 1, 'module_settings.onboarding_template': 1}).lean().exec());
    })
    .then(async user_details => {
        // set onboarding complete to yes
        // set all onboarding tasks statuses to completed
        console.log('user_details - ', user_details)
        let promises = []
        let filter = {
            companyId: companyId, 
            _id: user_details.module_settings.onboarding_template
        }
        let template = await OnboardingModel.findOne(filter).lean().exec()
        console.log('template - ', template)
        if (template) {
            promises.push(
                OnboardingModel.findOneAndUpdate(filter, {$set: {
                        onboarding_complete: 'Yes',
                        completion_date: new Date()
                    }
                })
            )
    
            for (let [task_category, tasks_array] of Object.entries(template['Onboarding Tasks'])) {
                for (let task of tasks_array) {
                    let update_task_filter = {
                        companyId: companyId,
                        _id: user_details.module_settings.onboarding_template
                    }
                    update_task_filter['Onboarding Tasks.' + task_category + '.' + '_id'] = task._id
                    let updateObj = {}
                    updateObj['$set'] = {}
                    updateObj['$set']['Onboarding Tasks.' + task_category + ".$.status"] = 'Completed'
                    updateObj['$set']['Onboarding Tasks.' + task_category + ".$.task_completion_date"] = new Date()
                    promises.push(
                        OnboardingModel.updateOne(update_task_filter, updateObj, {new: true})
                    )
                }
            }
    
            Promise.all(promises)
                .then(responses => {
                    console.log('onboarding admin override - ', responses)
                    res.status(200).json({
                        status: true,
                        message: "Admin override success"
                    })
                })
                .catch(err => {
                    console.log(err)
                    res.status(400).json({
                        status: false,
                        message: "Error",
                        error: err
                    })
                })
        }
        else {
            res.status(404).json({
                status: false,
                message: "Onboarding template is not yet assigned to this employee"
            })
        }
        
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: "Error",
            error: err
        })
    })
}

module.exports = {
    getUserOnboardingDetails,
    onboardingAdminOverride
}