let express = require('express');

let router = express.Router();

let authMiddleware = require('../../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../../middlewares/validations');

router.post(
    '/addOrUpdate',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.addOrUpdateCustomData
);

router.get(
    '/:company_id/:user_id',
    authMiddleware.isUserLogin,
    Controller.getCustomData
);

module.exports = router;