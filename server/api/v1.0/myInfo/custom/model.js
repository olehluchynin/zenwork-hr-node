let mongoose = require('mongoose');
let ObjectID = mongoose.Schema.ObjectId;

let CustomTabSchema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    userId: {type: ObjectID, ref: 'users', required: true}

}, {
    timestamps: true,
    versionKey: false,
    strict: false
})

CustomTabSchema.index({userId: 1, companyId: 1}, {unique: true})

let UserCustomDataModel = mongoose.model('user_custom_data', CustomTabSchema, 'user_custom_data');


module.exports = UserCustomDataModel;