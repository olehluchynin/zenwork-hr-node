/**
 * @api {post} /myinfo/custom/addOrUpdate AddOrUpdateCustomData
 * @apiName AddOrUpdateCustomData
 * @apiGroup Custom
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cdeb987c8b3566a2c99d450",
	"birth": "July"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Custom data updated successfully",
    "data": {
        "_id": "5d248281c8f4768976db55f1",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cdeb987c8b3566a2c99d450",
        "birth": "July",
        "createdAt": "2019-07-09T12:03:12.751Z",
        "updatedAt": "2019-07-09T12:03:12.751Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /myinfo/custom/:company_id/:user_id GetCustomData
 * @apiName GetCustomData
 * @apiGroup Custom
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} user_id Users unique _id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Fetched user custom data successfully",
    "data": {
        "_id": "5d248281c8f4768976db55f1",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cdeb987c8b3566a2c99d450",
        "birth": "July",
        "createdAt": "2019-07-09T12:03:12.751Z",
        "updatedAt": "2019-07-09T12:03:12.751Z"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */