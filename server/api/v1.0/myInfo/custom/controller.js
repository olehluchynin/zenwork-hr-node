let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../../config/config');
let _ = require('underscore');
let UserCustomDataModel = require('./model');

let addOrUpdateCustomData = (req,res) => {
    let body = req.body
    let companyId = body.companyId
    let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!companyId) {
            reject('companyId is required')
        }
        else if (!userId) {
            reject('userId is required')
        }
        else {
            resolve(null);
        }
    })
    .then(() => {
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        console.log(body)
        let filter = {
            companyId : ObjectID(companyId), 
            userId: ObjectID(userId)
        }
        console.log(filter)
        UserCustomDataModel.findOneAndUpdate(filter, {$set: body}, {upsert: true, new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                res.status(200).json({
                    status: true,
                    message: "Custom data updated successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while updating custom data",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    });
}

let getCustomData = (req,res)=> {
    let companyId = req.params.company_id
    let userId = req.params.user_id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        let find_query = {
            userId: ObjectID(userId),
            companyId: ObjectID(companyId)
        }
        UserCustomDataModel.findOne(find_query).lean().exec()
            .then(user_custom_data => {
                if(user_custom_data) {
                    res.status(200).json({
                        status: true,
                        message: "Fetched user custom data successfully",
                        data: user_custom_data
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "User custom data not found",
                        data: null
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while fecthing user custom data",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err);
        res.status(400).json({
            status: false,
            message: "Error while fecthing user custom data",
            error: err
        })
    })
}

module.exports = {
    addOrUpdateCustomData,
    getCustomData
}