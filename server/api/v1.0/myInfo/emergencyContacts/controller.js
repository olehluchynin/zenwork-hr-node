let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../../config/config');
let utils = require('./../../../../common/utils');
let helpers = require('./../../../../common/helpers');
let _ = require('underscore');
let EmergencyContacts = require('./model');

/**
*@author Asma
*@date 11/05/2019 09:32
API to add emergency contact of a user/ employee
*/
let addEC = (req,res) => {
    let body = req.body
    return new Promise((resolve,reject) => {
        resolve(null);
    })
    .then(() => {
        EmergencyContacts.create(body)
            .then(EC => {
                EC.save(function (err, response) {
                    if (!err) {
                        res.status(200).json({
                            status: true,
                            message: "Added emergency contact successfully",
                            data: response
                        })
                    }
                    else {
                        res.status(400).json({
                            status: false,
                            message: "Error while adding emergency contact",
                            error: err
                        })
                    }
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while adding emergency contact",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while adding emergency contact",
            error: err
        })
    });
}

/**
*@author Asma
*@date 11/05/2019 09:32
API to get one emergency contact details
*/
let getEC = (req, res) => {
    let companyId = req.params.company_id
    let userId = req.params.user_id
    let EC_Id = req.params.ec_id
    return new Promise((resolve, reject) => {
        if (!companyId) {
            reject('company_id is required')
        }
        else if (!userId) {
            reject('user_id is required')
        }
        else if (!EC_Id) {
            reject('ec_id is required')
        }
        else {
            resolve(null);
        }
    })
    .then(()=> {
        EmergencyContacts.findOne({
            _id: EC_Id,
            companyId: companyId,
            userId : userId
        })
        .then(result => {
            if(result) {
                res.status(200).json({
                    status: true,
                    message: "Successfully fecthed emergency contact",
                    data: result
                })
            }
            else {
                res.status(404).json({
                    status: false,
                    message: "emergency contact _id not found",
                    data: null
                })
            }
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while fetching emergency contact",
                error: err
            })
        });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while fetching emergency contact",
            error: err
        })
    })
}

/**
*@author Asma
*@date 11/05/2019 09:33
API to get list of employees with sort, search, filter functionalitites
*/
let listECs = (req, res ) => {
    let body = req.body
    return new Promise((resolve, reject) => {
        if (!body.companyId) {
            reject('companyId is required')
        }
        else if (!body.userId) {
            reject('userId is required')
        }
        else {
            resolve(null);
        }
    })
    .then(() => {
        let per_page = body.per_page ? body.per_page : 10
        let page_no = body.page_no ? body.page_no : 1
        let sort = {
            updatedAt : -1
        }
        let filter = {
            companyId: body.companyId,
            userId : body.userId
        }
        EmergencyContacts.find(filter)
            .sort(sort)
            .skip((page_no - 1) * per_page)
            .limit(per_page)
            .lean().exec()
            .then(ECsOfUser => {

                EmergencyContacts.count(filter)
                    .then(count => {
                        res.status(200).json({
                            status: true, message: "Success", total_count: count, data: ECsOfUser
                        })
                    })
                    .catch(err => {
                        res.status(400).json({
                            status: false,
                            message: "Error while fetching emergency contacts",
                            error: err
                        })
                    })
                
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while fetching emergency contacts",
                    error: err
                })
            });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while fetching emergency contacts",
            error: err
        })
    })
}

/**
*@author Asma
*@date 11/05/2019 09:34
API to edit emergency contact
*/
let editEC = (req,res) => {
    let body = req.body
    let ec_id = req.params.ec_id
    let companyId = body.companyId
    let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!ec_id) {
            reject('ec_id is required')
        }
        else if (!companyId) {
            reject('companyId is required')
        }
        else if (!userId) {
            reject('userId is required')
        }
        else {
            resolve(null);
        }
    })
    .then(() => {
        delete body.companyId
        delete body.userId
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        console.log(body)
        let filter = {
            _id: ObjectID(ec_id), 
            companyId : ObjectID(companyId), 
            userId: ObjectID(userId)
        }
        console.log(filter)
        EmergencyContacts.findOneAndUpdate(filter, {$set: body}, {new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                res.status(200).json({
                    status: true,
                    message: "Emergency contact updated successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while updating emergency contact",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while updating emergency contact",
            error: err
        })
    });
}

/**
*@author Asma
*@date 11/05/2019 09:34
API to delete one or multiple emergency contacts of a user
*/
let deleteEC = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let userId = body.userId
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        EmergencyContacts.deleteMany({
            companyId: companyId,
            userId: userId,
            _id: {$in : body._ids}
        })
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting emergency contact(s)",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while deleting emergency contact(s)",
            error: err
        })
    })
}

module.exports = {
    addEC,
    getEC,
    editEC,
    deleteEC,
    listECs
}