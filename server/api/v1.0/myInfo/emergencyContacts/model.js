let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('../../../../common/constants');

let Schema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    userId: {type: ObjectID, ref: 'users', required: true},
    fullName: {
        type: String, required: true
    },
    relationship: {type : String},
    primaryContact: {type: Boolean, default: false},
    contact: {
        workPhone: String,
        extention: String,
        mobilePhone: String,
        homePhone: String,
        workMail: String,
        personalMail: String
    },
    address: {
        street1: String,
        street2: String,
        city: String,
        state: String,
        zipcode: String,
        country: String
    },

    isChecked : {
        type: Boolean, default: false
    }
},{
    timestamps: true,
    versionKey: false,
    strict: false
});

Schema.index({ _id: 1, companyId: 1, userId: 1 });

Schema.index({ companyId: 1, userId: 1 });

let EmergencyContactsModel = mongoose.model('emergency_contacts',Schema);

module.exports = EmergencyContactsModel;