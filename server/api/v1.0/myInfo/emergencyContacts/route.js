let express = require('express');

let router = express.Router();

let authMiddleware = require('../../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../../middlewares/validations');

router.post(
    '/add', 
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.addEC
);

router.get(
    "/get/:company_id/:user_id/:ec_id",
    authMiddleware.isUserLogin,
    Controller.getEC
);

router.post(
    "/getAll",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.listECs
);

router.put(
    "/edit/:ec_id",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editEC
);

router.post(
    "/delete",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deleteEC
);


module.exports = router;