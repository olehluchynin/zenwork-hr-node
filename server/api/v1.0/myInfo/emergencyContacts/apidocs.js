/**
 * @api {post} /myinfo/emergencyContact/add AddEmergencyContact
 * @apiName AddEmergencyContact
 * @apiGroup EmergencyContact
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cd108877ff3945b0c1dbb1e",
	"fullName": "Rachel James",
    "relationship": "Mother",
    
    "contact": {
        "workPhone": "9090909090",
        "extention": "91",
        "mobilePhone": "2323232323",
        "homePhone": "3365656565",
        "workMail": "xcvdf@vxcorp.com",
        "personalMail": "rachel@gmail.com"
    },
    "address": {
        "street1": "block2",
        "street2": "road no 5",
        "city": "manhattan",
        "state": "New york",
        "zipcode": "123131",
        "country": "United States"
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Added emergency contact successfully",
    "data": {
        "primaryContact": false,
        "_id": "5cd4f40ef605610758d15e27",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd108877ff3945b0c1dbb1e",
        "fullName": "Rachel James",
        "relationship": "Mother",
        "contact": {
            "workPhone": "9090909090",
            "extention": "91",
            "mobilePhone": "2323232323",
            "homePhone": "3365656565",
            "workMail": "xcvdf@vxcorp.com",
            "personalMail": "rachel@gmail.com"
        },
        "address": {
            "street1": "block2",
            "street2": "road no 5",
            "city": "manhattan",
            "state": "New york",
            "zipcode": "123131",
            "country": "United States"
        },
        "createdAt": "2019-05-10T03:46:22.669Z",
        "updatedAt": "2019-05-10T03:46:22.669Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /myinfo/emergencyContact/get/:company_id/:user_id/:ec_id GetEmergencyContact
 * @apiName GetEmergencyContact
 * @apiGroup EmergencyContact
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} user_id Users unique _id
 * @apiParam {ObjectId} ec_id Emergency contact unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fecthed emergency contact",
    "data": {
        "contact": {
            "workPhone": "9090909090",
            "extention": "91",
            "mobilePhone": "2323232323",
            "homePhone": "3365656565",
            "workMail": "xcvdf@vxcorp.com",
            "personalMail": "rachel@gmail.com"
        },
        "address": {
            "street1": "block2",
            "street2": "road no 5",
            "city": "manhattan",
            "state": "New york",
            "zipcode": "123131",
            "country": "United States"
        },
        "primaryContact": false,
        "isChecked": false,
        "_id": "5cd4f40ef605610758d15e27",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd108877ff3945b0c1dbb1e",
        "fullName": "Rachel James",
        "relationship": "Mother",
        "createdAt": "2019-05-10T03:46:22.669Z",
        "updatedAt": "2019-05-10T03:46:22.669Z"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /myinfo/emergencyContact/getAll GetAllEmergencyContacts
 * @apiName GetAllEmergencyContacts
 * @apiGroup EmergencyContact
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "userId": "5ccaf70395a1a7354dded7d9",
    "per_page": 10,
    "page_no": 1
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Success",
    "total_count": 4,
    "data": [
        {
            "_id": "5cd51a79b8cd8b7fbecbfed0",
            "primaryContact": true,
            "isChecked": false,
            "companyId": "5c9e11894b1d726375b23058",
            "userId": "5ccaf70395a1a7354dded7d9",
            "fullName": "Saiprakash g",
            "relationship": "Spouse",
            "contact": {
                "workPhone": "8099843874",
                "extention": "1122",
                "mobilePhone": "123424",
                "homePhone": "12341234",
                "workMail": "sai@gmail.coom",
                "personalMail": "saiprakash@gmail.com"
            },
            "address": {
                "street1": "Plot no 339/353 door no 201 dwarakamaye apartment",
                "street2": "Pragathi nagar",
                "city": "HYDERABAD",
                "state": "NewYork",
                "zipcode": "500090",
                "country": "India"
            },
            "createdAt": "2019-05-10T06:30:17.079Z",
            "updatedAt": "2019-05-10T06:30:17.079Z"
        },
        {
            "_id": "5cd507a2c5a9e37e8e43c771",
            "primaryContact": false,
            "companyId": "5c9e11894b1d726375b23058",
            "userId": "5ccaf70395a1a7354dded7d9",
            "fullName": "Asma",
            "relationship": "Spouse",
            "contact": {
                "workPhone": "1234123",
                "extention": "123",
                "mobilePhone": "12341234",
                "homePhone": "12341234",
                "workMail": "asma@gmail.com",
                "personalMail": "asmamubin@gmail.com"
            },
            "address": {
                "street1": "madhapur",
                "street2": "Hitech",
                "city": "HYDERABAD",
                "state": "NewYork",
                "zipcode": "500090",
                "country": "India"
            },
            "createdAt": "2019-05-10T05:09:54.607Z",
            "updatedAt": "2019-05-10T05:09:54.607Z"
        },
        {
            "_id": "5cd50667c5a9e37e8e43c770",
            "primaryContact": true,
            "companyId": "5c9e11894b1d726375b23058",
            "userId": "5ccaf70395a1a7354dded7d9",
            "fullName": "vipin",
            "relationship": "Spouse",
            "contact": {
                "workPhone": "23452123",
                "extention": "123",
                "mobilePhone": "23452134",
                "homePhone": "23452345",
                "workMail": "vipin@gmail.com",
                "personalMail": "vipinreddy@gmail.com"
            },
            "address": {
                "street1": "Ameerpet",
                "street2": "Srnagar",
                "city": "HYDERABAD",
                "state": "NewYork",
                "zipcode": "500090",
                "country": "India"
            },
            "createdAt": "2019-05-10T05:04:39.416Z",
            "updatedAt": "2019-05-10T05:04:39.416Z"
        },
        {
            "_id": "5cd50123c5a9e37e8e43c76e",
            "primaryContact": true,
            "companyId": "5c9e11894b1d726375b23058",
            "userId": "5ccaf70395a1a7354dded7d9",
            "fullName": "saiprakash",
            "relationship": "Spouse",
            "contact": {
                "workPhone": "8099843874",
                "extention": "91",
                "mobilePhone": "809984387",
                "homePhone": "08099843874",
                "workMail": "saiprakash@gmail.com",
                "personalMail": "gollaprakash@gmail.com"
            },
            "address": {
                "street1": "Plot no 339/353 door no 201 dwarakamaye apartment",
                "street2": "Pragathi nagar",
                "city": "HYDERABAD",
                "state": "NewYork",
                "zipcode": "500090",
                "country": "India"
            },
            "createdAt": "2019-05-10T04:42:11.509Z",
            "updatedAt": "2019-05-10T04:42:11.509Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {put} /myinfo/emergencyContact/edit/:ec_id EditEmergencyContact
 * @apiName EditEmergencyContact
 * @apiGroup EmergencyContact
 * 
 * @apiParam {ObjectId} ec_id Emergency contact unique _id
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "userId": "5cd108877ff3945b0c1dbb1e",
    "relationship": "Mother"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Emergency contact updated successfully",
    "data": {
        "contact": {
            "workPhone": "9090909090",
            "extention": "91",
            "mobilePhone": "2323232323",
            "homePhone": "3365656565",
            "workMail": "xcvdf@vxcorp.com",
            "personalMail": "rachel@gmail.com"
        },
        "address": {
            "street1": "block2",
            "street2": "road no 5",
            "city": "manhattan",
            "state": "New york",
            "zipcode": "123131",
            "country": "United States"
        },
        "primaryContact": false,
        "isChecked": false,
        "_id": "5cd4f40ef605610758d15e27",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd108877ff3945b0c1dbb1e",
        "fullName": "Rachel James",
        "relationship": "Mother",
        "createdAt": "2019-05-10T03:46:22.669Z",
        "updatedAt": "2019-05-10T09:18:51.944Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /myinfo/emergencyContact/delete DeleteEmergencyContact
 * @apiName DeleteEmergencyContact
 * @apiGroup EmergencyContact
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "userId": "5cd108877ff3945b0c1dbb1e",
    "_ids": [
    	"5cd4f40ef605610758d15e27"
    	]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */
