let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;

let ClientDemoRequestDetails = new mongoose.Schema({
    firstName: String,
    lastName: String,
    companyName: String,
    companySize: String,
    email: String,
    phone : Number
}, {
        timestamps: true,
        versionKey: false
    });
Schema.plugin(autoIncrement.plugin, {model:'demorequests', field:'demorequestsId', startAt: 1, incrementBy: 1});

let ClientDemoRequest = mongoose.model('demorequests', ClientDemoRequestDetails);
module.exports = ClientDemoRequest;
