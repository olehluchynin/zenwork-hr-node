let mongoose = require('mongoose');
let _ = require("underscore");

let clientDemoCollection = require('./model');
let Mailer = require("./../../../common/mailer");
let registerForDemo = (req, res) => {

    let body = req.body;
    return new Promise((resolve, reject) => {
        if (!body.firstName) {
            reject({
                firstName: "Enter first name"
            });
        } else if (!body.lastName) {
            reject({
                lastName: "Enter last name"
            });
        } else if (!body.companyName) {
            reject({
                companyName: "Enter company name"
            });
        } else if (!body.companySize) {
            reject({
                companySize: "Select number of employees"
            });
        }
        else if (!body.email || !(_.isString(body.email) && /^\S+@\S+\.\S+/.test(body.email))) {
            reject({
                email: "Enter a valid email address"
            });
        }
        else if (!body.phone || !(/[0-9]{10}/.test(body.phone))) {
            reject({
                phone: "Enter a valid phone number"
            });
        } else {
            resolve(body)
        };
    }).then((data) => {
        console.log("Hello")
        clientDemoCollection.findOne({ email: body.email })
            .then(
                (response) => {
                    if (!response) {
                        clientDemoCollection.create(body)
                            .then(
                                (response) => {
                                    Mailer.sendClientDemo(body.email, body.firstName + " " + body.lastName);
                                    res.status(200).json({ status: true, message: "Successfully saved Client Details", data: response });
                                }
                            ).catch(
                                (err) => {
                                    console.log(err);
                                    res.status(400).json({ status: false, message: "Insufficient Data", err: err });
                                }
                            )
                    } else {
                        res.status(200).json({ status: false, message: "Email already registered", err: { email : "Email already registered" } });
                    }
                }
            )
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: "Insufficient Data", err: err });
    });
};



module.exports = {
    registerForDemo
}
