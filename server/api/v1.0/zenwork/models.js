let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');

// mongoose.set('debug', true);

let Schema = new mongoose.Schema({
    tax_rate: {
        type: Number,
        default: 8
    }
},{
    timestamps: true,
    versionKey: false
});



let TaxRateModel = mongoose.model('taxrate',Schema);

module.exports = {
    TaxRateModel
};
