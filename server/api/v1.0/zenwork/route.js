let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.get(
    '/getTaxRate/:tax_rate_id',
    authMiddleware.isUserLogin,
    Controller.getTaxRateById
);

router.get(
    '/getTaxRate',
    // authMiddleware.isUserLogin,
    Controller.getTaxRate
);

router.put(
    '/updateTaxRate',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.updateTaxRate
);

router.post(
    '/addTaxRate',
    authMiddleware.isUserLogin,
    Controller.addTaxRate
);

module.exports = router;