/**
 * @api {put} /zenwork/updateTaxRate UpdateTaxRate
 * @apiName UpdateTaxRate
 * @apiGroup ZenworkRelated
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "tax_rate": 8,
    "_id": "5cefc80116c14409c00a0748",
    "createdAt": "2019-05-30T12:09:37.468Z",
    "updatedAt": "2019-05-30T12:09:37.468Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Tax rate updated successfully",
    "data": {
        "tax_rate": 8,
        "_id": "5cefc80116c14409c00a0748",
        "createdAt": "2019-05-30T12:09:37.468Z",
        "updatedAt": "2019-05-30T12:13:03.844Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /zenwork/getTaxRate/:tax_rate_id GetTaxRateById
 * @apiName GetTaxRateById
 * @apiGroup ZenworkRelated
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} tax_rate_id Tax rate uniue _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched tax_rate",
    "data": {
        "tax_rate": 8,
        "_id": "5cefc80116c14409c00a0748",
        "createdAt": "2019-05-30T12:09:37.468Z",
        "updatedAt": "2019-05-30T12:13:03.844Z"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /zenwork/getTaxRate GetOneTaxRate
 * @apiName GetOneTaxRate
 * @apiGroup ZenworkRelated
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched tax_rate",
    "data": {
        "tax_rate": 8,
        "_id": "5cefc80116c14409c00a0748",
        "createdAt": "2019-05-30T12:09:37.468Z",
        "updatedAt": "2019-05-30T12:13:03.844Z"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */