let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../config/config');
let _ = require('underscore');
let TaxRateModel = require('./models').TaxRateModel;

let addTaxRate = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        TaxRateModel.create({})
            .then(tax_rate => {
                res.status(200).json({
                    status: true,
                    message: "Tax rate doc added successfully",
                    data: tax_rate
                })
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while adding tax rate",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let updateTaxRate = ( req, res) => {
    let body = req.body
    let tax_rate_id = body._id
    return new Promise((resolve, reject) => {
        if(!tax_rate_id) reject('_id is required');
        else resolve(null);
    })
    .then(() => {
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        TaxRateModel.findOneAndUpdate({_id: tax_rate_id}, {$set: body}, {new: true})
            .then(tax_rate => {
                res.status(200).json({
                    status: true,
                    message: "Tax rate updated successfully",
                    data: tax_rate
                })
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while updating tax rate",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let getTaxRateById = ( req, res) => {
    
    let tax_rate_id = req.params.tax_rate_id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        TaxRateModel.findOne({_id: tax_rate_id})
            .then(tax_rate => {
                res.status(200).json({
                    status: true,
                    message: "Successfully fetched tax_rate",
                    data: tax_rate
                })
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while fetching tax rate",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let getTaxRate = ( req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        TaxRateModel.findOne({})
            .then(tax_rate => {
                res.status(200).json({
                    status: true,
                    message: "Successfully fetched tax_rate",
                    data: tax_rate
                })
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while fetching tax rate",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

module.exports = {
    updateTaxRate,
    getTaxRateById,
    addTaxRate,
    getTaxRate
}