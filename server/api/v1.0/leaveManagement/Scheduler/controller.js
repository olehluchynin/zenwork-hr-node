let mongoose = require('mongoose');
let moment = require('moment');
var xlsx = require('node-xlsx');

var json2xls = require('json2xls');
var fs = require('fs');

let ObjectID = mongoose.Types.ObjectId;
let scheduler_model = require("./model");
let holidayModel = scheduler_model.scheduler_holiday;
let blackOutEmpGrp = scheduler_model.BlackoutEmployeeGroupModel;
let blackOutModel = scheduler_model.BlackoutModel;
let positionScheduleModel = scheduler_model.positionWorkSchedule;
let departmentScheduleModel = scheduler_model.departmentSchedule;
let departmentEmployeesScheduleInfoModel = scheduler_model.departmentEmployeesScheduleInfoModel
let departmentEmployeesScheduleInfoModel2 = scheduler_model.departmentEmployeesScheduleInfoModel2
// let employeeModel = scheduler_model.employeeSchema;

let userModel = require('../../users/model');

/**
*@author Asma
*@date 03/09/2019 11:54
Function to convert 12 hour format to 24 hour format
*/
let timeConvertor = (time) => {
    time = time.toUpperCase().replace(' ', '')
    var PM = time.match('PM') ? true : false
    
    time = time.split(':')
    var min = parseInt(time[1],10)
    
    if (PM) {
        var hour = parseInt(time[0],10)
        if (hour < 12) {
            hour += 12 
        }
    } else {
        var hour = parseInt(time[0], 10)
        if(hour == 12) {
            hour = 0
        }    
    }
    
    // console.log(hour + ':' + min)
    return {
        hours: hour,
        minutes: min
    }
}

/**
*@author Asma
*@date 03/09/2019 17:27
Method to convert 24h time to 12hr string
*/
let convertTo12Format = (timeDict) => {
    let time = ['', ':', '', ' ', '']
    if (timeDict.hours < 12) {
        time[4] = 'AM'
    }
    else {
        time[4] = 'PM'
    }

    time[2] = String(timeDict.minutes)

    if (time[2].length == 1) {
        time[2] = '0' + time[2]
    }

    time[0] = timeDict.hours % 12 || 12

    return time.join("")
}

/**
*@author Bhaskar Keelu
*@date 12/06/2019
Method to add the Holiday List based on the location.
*/

let createHolidays = (req, res) => {
    var body = req.body;
    return new Promise((response, reject) => {
        if (!body.location_id) reject("Please enter the location.");
        if (!body.year) reject("Please enter the Year.");
        if (!body.holiday_name) reject("Please enter the Holiday name");
        if (!body.holiday_date) reject("Please enter the Holiday date");
        response(holidayModel.findOne({companyId: req.jwt.companyId, year: body.year, holiday_name: body.holiday_name}).lean().exec());
    }).then(holiday => {
        if(holiday) {
            res.status(400).json({
                status: false,
                message: "Holiday already exists with this name"
            })
        }
        body.companyId = req.jwt.companyId
        holidayModel.create(body)
            .then(resp => {
                if (resp) {
                    res.status(200).json({
                        "status": true,
                        "message": "Successfully added holiday",
                        "result": resp 
                    })
                }
            }).catch((error) => {
                console.log(error)
                res.status(400).json({
                    status: false,
                    message: "Failed to execute the Query",
                    data: null
                })
            });
    }).catch((err) => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err            
        })
    })
}

/**
*@author Asma
*@date 23/08/2019 15:53

*/
let addHolidays = (req, res) => {
    var body = req.body;
    console.log(body)
    return new Promise((resolve, reject) => {
        if (!body.location_ids) reject("Please enter the location.");
        if (!body.year) reject("Please enter the Year.");
        if (!body.holiday_name) reject("Please enter the Holiday name");
        if (!body.holiday_date) reject("Please enter the Holiday date");
        resolve(null);
    }).then(async () => {
        let holidays = []
        for (let location_id of body.location_ids) {
            let holiday_exists = await holidayModel.findOne({companyId: req.jwt.companyId, location_id: location_id, year: body.year, holiday_name: body.holiday_name}).lean().exec()
            if (holiday_exists) {
                throw 'Holiday already exists'                
            }
            let holiday = {
                companyId: req.jwt.companyId,
                year: body.year,
                holiday_name: body.holiday_name,
                holiday_date: body.holiday_date,
                location_id: location_id
            }
            holidays.push(holiday)
        }
        holidayModel.insertMany(holidays)
            .then(resp => {
                if (resp) {
                    res.status(200).json({
                        "status": true,
                        "message": "Successfully added holidays",
                        "result": resp 
                    })
                }
            }).catch((error) => {
                console.log(error)
                res.status(400).json({
                    status: false,
                    message: "Failed to execute the Query",
                    error: error
                })
            });
    }).catch((err) => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err            
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 12/06/2019
Method to update the Holiday List based on the location.
*/
/**
*@author Asma
*@date 05/08/2019 17:07
changed API according to new model
*/
let updateHolidaysDetails = (req, res) => {
    let body = req.body;
    let _id = body._id
    return new Promise((response, reject) => {
        if (!_id) reject("_id is required");
        response(null);
    }).then(() => {
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        let filter = {
            _id: ObjectID(_id), 
            companyId : ObjectID(req.jwt.companyId)
        }
        console.log(filter)
        body.updatedBy = ObjectID(req.jwt.userId)
        holidayModel.findOneAndUpdate(filter, {$set: body}, {new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                res.status(200).json({
                    status: true,
                    message: "Holiday updated successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while updating holiday",
                    error: err
                })
            })
    }).catch((err) => {
        console.log(err)
        res.status(400).json({
            status: true,
            message: err
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 13/06/2019
Method to delete the Holiday List based on the location.
*/

let deleteHolidayDetails = (req, res) => {
    var body = req.body;
    var params = body.params
    return new Promise((response, reject) => {
        if (!params[0]) reject("Please enter the details to delete the data.");
        response(null)
    }).then(() => {
        var toRemove = [];
        for (var i = 0; i < params.length; i++) {
            var obj = {};
            obj.location_id = params[i].location_id;
            obj.holiday_id = params[i].holiday_id;
            toRemove.push(obj);
        }
        return toRemove;
    }).then(async toRemove => {
        // console.log(toRemove)
        for (const each of toRemove) {
            console.log(each)
            await holidayModel.update({ "location_id": each.location_id }, { $pull: { "holiday_details": { "_id": { $in: each.holiday_id } } } })
        }
        return toRemove;
    }).then((data) => {
        if (data) {
            res.status(200).json({
                status: true,
                message: "Successfully removed the data."
            })
        } else {
            res.status(200).json({
                status: false,
                message: "Data not found...",
                error: data
            })
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: err });
    });
}

/**
*@author Asma
*@date 05/08/2019 18:00
method to delete multiple holidays
*/
let deleteHolidays = (req, res) => {
    let body = req.body
    return new Promise((resolve, reject) => {
        if( !body._ids) reject('_ids are required')
        resolve(null);
    })
    .then(() => {
        holidayModel.deleteMany({
            companyId: req.jwt.companyId,            
            _id: {$in : body._ids}
        })
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting holidays",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while deleting holidays",
            error: err
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 13/06/2019
Method to create a Employee Group Name for BlackOut Days.
*/

let createBlackoutEmployeeGroup = (req, res) => {
    var body = req.body;
    let employee_ids = body.employee_ids
    delete body.employee_ids
    return new Promise((resolve, reject) => {
        if (!body.name) reject("Please enter the name.");
        if (!body.effectiveDate) reject("Please enter the Effective Date.");
        if(!employee_ids) reject('employee_ids are required')
        if(!employee_ids.length) reject('Include atleast one employee')
        resolve(blackOutEmpGrp.findOne({companyId: body.companyId, name: body.name}));
    }).then(blackoutEmployeeGroup => {
        if (blackoutEmployeeGroup) {
            res.status(400).json({
                status: false,
                message: "Blackout group already exists with this name"
            })
        }
        blackOutEmpGrp.create(body).then(newBlackoutGroup => {
            let promises = []
            console.log('newBlackoutGroup', newBlackoutGroup)
            for (let user_id of  employee_ids) {
                promises.push(userModel.findOneAndUpdate({companyId: body.companyId, _id: user_id}, {
                    $push: {
                        blackout_groups: newBlackoutGroup._id
                    }
                }))
            }
            Promise.all(promises)
                .then(responses => {
                    res.status(200).json({
                        status: true,
                        message: "Successfully created blackout employee group",
                        data: newBlackoutGroup
                    })
                })
                .catch(err => {
                    console.log(err)
                    res.status(400).json({
                        status: false,
                        message: "Failed to execute the Query",
                        error: err
                    })
                })
        }).catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: "Failed to execute the Query",
                error: err
            })
        });
    }).catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: "Failed to Execute the Query.",
            error: err
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 13/06/2019
Method to create a Block Days for a list of employees.
*/

let addBlackoutDates = (req, res) => {
    var body = req.body;
    return new Promise((resolve, reject) => {
        for (let blackoutDetails of body) {
            if(!blackoutDetails.location_id) reject('Select location');
            if (!blackoutDetails.blackoutName) reject("Enter name.");
            if(!blackoutDetails.blackoutEmployeeGroup) reject('Select employee group');
            if (!blackoutDetails.startDate) reject("Select the Start Date.");
            if (!blackoutDetails.endDate) reject("Select the End Date.");
            if (!blackoutDetails.blockType) reject("Select the Block Type.");
            if (blackoutDetails.startDate > blackoutDetails.endDate) reject("Start Date cannot be greater than End Date");
        }
        let names = body.map(x => x.blackoutName) 
        if(names.length != new Set(names).size) {
            reject('Duplicate names are not allowed')
        }
        resolve(null);
    }).then(() => {
        body = body.map(x => {
            x.companyId = req.jwt.companyId
            return x;
        })
        blackOutModel.insertMany(body).then((resp) => {
            res.status(200).json({
                "status": true,
                "message": "Successfully added blackout dates",
                "result": resp
            })
        }).catch((error) => {
            console.log(error)
            res.status(400).json({
                status: false,
                message: "Failed to execute the Query",
                error: error
            })
        });
    }).catch((err) => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 13/06/2019
Method to delete the Block Days for a list of employees.
*/

let deleteBlackOut = (req, res) => {
    var body = req.body;
    return new Promise((response, reject) => {
        if (!body._id[0]) reject("Please enter the details to delete the data.");
        response(null)
    }).then(() => {
        blackOutModel.remove({ _id: { $in: body._id } })
            .then((result) => {
                res.status(200).json({
                    status: true,
                    message: "Successfully deleted the data.",
                    result: result ? result : []
                })
            }).catch((error) => {
                res.status(400).json({
                    status: false,
                    message: "Failed to delete the Data...",
                    error: error
                })
            })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "Failed to delete the Data.",
            error: error
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 13/06/2019
Method to update the BlockOut Days based on the location.
*/

let updateBlackoutDetails = (req, res) => {
    var body = req.body;
    let _id = body._id
    let companyId = req.jwt.companyId
    return new Promise((resolve, reject) => {
        if (!_id) reject("_id is required");
        resolve(null);
    }).then(() => {
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        blackOutModel.findOneAndUpdate({ companyId: companyId, _id: body._id }, { $set: body }, {new: true})
            .then(result => {
                res.status(200).json({
                    status: true,
                    message: "Successfully updated blackout dates",
                    data: result
                })
            }).catch((err) => {
                res.status(400).json({
                    status: false,
                    message: "Failed to update the data",
                    error: err
                })
            })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "Failed to update the data",
            error: error
        })
    })

}

/**
*@author Bhaskar Keelu
*@date 13/06/2019
Method to fetch the Holiday List.
*/

let getholidayDetails = (req, res) => {
    let year = parseInt(req.params.year)
    let companyId = req.jwt.companyId
    return new Promise((response, reject) => {
        response(null);
    }).then(() => {
        holidayModel.find({companyId: companyId, year: year}).populate('location_id', 'name').lean().exec()
            .then(holidays => {
                res.status(200).json({
                    status: true,
                    message: "Successfully fetched holidays",
                    data: holidays
                })
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while fetching holidays",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: "Error while fetching holidays",
            error: err
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 13/06/2019
Method to fetch the Holiday List.
*/

let getBlackOutDetails = (req, res) => {
    let body = req.body;
    let per_page, page_no
    return new Promise((resolve, reject) => {
        if (body.per_page < body.page_no) {
            res.status(200).json({
                "status": false,
                "message": "Page_Number should not be greater than per_page"
            })
        }
        per_page = body.per_page ? body.per_page : 10
        page_no = body.page_no ? body.page_no : 1
        resolve(null);
    }).then(() => {
        blackOutModel.find({companyId: req.jwt.companyId}).sort({updatedAt: -1}).skip((page_no - 1) * per_page).limit(per_page).then((result) => {
            res.status(200).json({
                status: true,
                message: "Successfully fetched the data.",
                result: result 
            })
        }).catch((err) => {
            res.status(400).json({
                status: false,
                message: "Failed to Execute the Query",
                error: err
            })
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 14/06/2019
Method to fetch the particular holiday details.
*/

let getHoliday = (req, res) => {
    let companyId = req.jwt.companyId
    let holiday_id = ObjectID(req.params._id)
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        holidayModel.findOne({companyId: companyId, _id: holiday_id}).populate('location_id', 'name').lean().exec()
            .then(holiday => {
                if(holiday) {
                    res.status(200).json({
                        status: true,
                        message: "Successfully fetched holiday",
                        data: holiday
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "Holiday not found",
                        data: null
                    })
                }
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while fetching holiday",
                    error: error
                })
            })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "Error while fetching holiday",
            error: error
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 14/06/2019
Method to import files for BlackOut Employee Groups.
*/

let importXlsx = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let file;
        if (req.files) file = req.files.file;
        // console.log('body ==> ',req.body);
        // console.log('files ==> ',req.files);
        // console.log(file);

        var data = xlsx.parse(file.path)[0].data;
        // console.log("===========>", data);
        // console.log("===========>", data[0].data);
        let toAdd = [];
        for (let i = 1; i < data.length; i++) {
            let obj = {};
            if (data[i][0] && data[i][1] && data[i][2]) {
                obj.empId = data[i][0];
                obj.blackoutName = data[i][1];
                obj.effectiveDate = new Date((data[i][2] - (25567 + 2)) * 86400 * 1000);
                obj.company_id = req.body.companyId
                toAdd.push(obj);
            }
        }
        return toAdd;
        // console.log(toAdd);
        // return StructureModel.create(toAdd);
    }).then(async toAdd => {
        console.log(toAdd, "toAdd")
        for (const each of toAdd) {
            let doc = await userModel.findOne({ "job.employeeId": each.empId, "companyId": each.company_id }, { _id: 1 })
                .sort({ _id: -1 }).then((result) => {
                    if (result != null) {
                        // console.log(result)
                        blackOutEmpGrp.findOne({ "companyId": each.company_id, "name": each.blackoutName })
                            .then((blackOutResult) => {
                                console.log(blackOutResult, "black");
                                if (blackOutResult != null) {
                                    userModel.findOneAndUpdate(
                                        { "job.employeeId": each.empId, "companyId": each.company_id },
                                        { "blackout_EffectDate": each.effectiveDate, "blackout_GrpName": each.blackoutName, "blackout_emp_grp_id": blackOutResult._id })
                                        .sort({ _id: -1 }).then((result) => {
                                            console.log(result.blackout_EffectDate, "blackout_EffectDate");
                                        })
                                }
                            })
                    } else {
                        res.status(200).json({
                            status: false,
                            message: "No data...",
                            result: result
                        })
                    }
                });
        }
        console.log('Done!');
        // }
        return toAdd;
    }).then((data) => {
        console.log("data", data);
        if (data) {
            res.status(200).json({ status: true, message: "Successfully Added " + data.length + ' Records' });
        } else {
            res.status(200).json({ status: true, message: "Data not found in the file" });
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: err });
    });
};

/**
*@author Bhaskar Keelu
*@date 15/06/2019
Method to import files for BlackOut Employee Groups.
*/
let getBlackOutEmpGrp = (req, res) => {
    var company_id = req.params.company_id
    console.log(company_id);
    return new Promise((response, reject) => {
        if (!company_id) reject("Please provide the companyID")
        response(null);
    }).then(() => {
        console.log(company_id)
        blackOutEmpGrp.find({ companyId: company_id }).then((result) => {
            res.status(200).json({
                status: true,
                message: "Successfully fetched the data.",
                result: result ? result : []
            })
        }).catch((err) => {
            res.status(400).json({
                status: false,
                message: "Failed to fetch the data.",
                result: err
            })
        })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "Failed to fetch the data.",
            result: error
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 15/06/2019
Method to import files for BlackOut Employee Groups.
*/
let createPositionSchedule = (req, res) => {
    var body = req.body;
    var weekApplicable = body.weekApplicable
    body.companyId = req.jwt.companyId
    return new Promise((resolve, reject) => {
        // shiftStartTimeDecimal = shiftStartTime.hours + (shiftStartTime.minutes/60)
        // var regex = new RegExp(':', 'g');
        if (!body.scheduleTitle) reject('scheduleTitle is required')
        if (!body.scheduleStartDate) reject('scheduleStartDate is required')
        if (!body.type) reject('type is required')
        if (body.type == 'Standard' && !body.jobTitle) reject('jobTitle is required')
        if (!body.daysOfWeek) reject('select days')
        if (!body.daysOfWeek.length) reject('select days')
        if (!body.totalHrsPerDay) reject('Enter totalHrsPerDay')
        if (!body.totalHrsPerWeek) reject('totalHrsPerWeek is required')

        if (body.lunchBreakTime) {
            for (let day of weekApplicable) {
                if (!day.day) reject("Please select the day");
                if (!day.shiftStartTime && !day.lunchStartTime && !day.lunchEndTime && !day.shiftEndTime) reject("Please enter the time for day : " + day.day)
                let shiftStartTime = day.shiftStartTime;
                shiftStartTime = timeConvertor(shiftStartTime)
                let shiftStartTimeDecimal = shiftStartTime.hours + (shiftStartTime.minutes/60)
                console.log('shiftStartTime hours -', typeof shiftStartTime.hours)
                console.log('shiftStartTime minutes -', typeof shiftStartTime.minutes)
                console.log('shiftStartTimeDecimal -', shiftStartTimeDecimal)
                day.shiftStartTime = shiftStartTime
                let lunchStartTime = day.lunchStartTime;
                lunchStartTime = timeConvertor(lunchStartTime)
                let lunchStartTimeDecimal = lunchStartTime.hours + (lunchStartTime.minutes/60)
                console.log('lunchStartTimeDecimal -', lunchStartTimeDecimal)
                day.lunchStartTime = lunchStartTime
                let lunchEndTime = day.lunchEndTime;
                lunchEndTime = timeConvertor(lunchEndTime)
                let lunchEndTimeDecimal = lunchEndTime.hours + (lunchEndTime.minutes/60)
                console.log('lunchEndTimeDecimal -', lunchEndTimeDecimal)
                day.lunchEndTime = lunchEndTime
                let shiftEndTime = day.shiftEndTime;
                shiftEndTime = timeConvertor(shiftEndTime)
                let shiftEndTimeDecimal = shiftEndTime.hours + (shiftEndTime.minutes/60)
                console.log('shiftEndTimeDecimal -', shiftEndTimeDecimal)
                day.shiftEndTime = shiftEndTime
                if (shiftStartTimeDecimal > lunchStartTimeDecimal) reject("Lunch Start Time should not be less than Start Time in day : " + day.day)
                if (lunchStartTimeDecimal > lunchEndTimeDecimal) reject("Lunch End Time should not be less than Lunch Start Time in day : " + day.day)
                if (lunchEndTimeDecimal > shiftEndTimeDecimal) reject("End Time should not be less than Lunch End Time in day : " + day.day)            
            }
        }
        else {
            for (let day of weekApplicable) {
                if (!day.day) reject("Please select the day");
                if (!day.shiftStartTime && !day.shiftEndTime) reject("Please enter the time for day : " + day.day)
                let shiftStartTime = day.shiftStartTime;
                shiftStartTime = timeConvertor(shiftStartTime)
                let shiftStartTimeDecimal = shiftStartTime.hours + (shiftStartTime.minutes/60)
                console.log('shiftStartTime hours -', typeof shiftStartTime.hours)
                console.log('shiftStartTime minutes -', typeof shiftStartTime.minutes)
                console.log('shiftStartTimeDecimal -', shiftStartTimeDecimal)
                day.shiftStartTime = shiftStartTime
                
                let shiftEndTime = day.shiftEndTime;
                shiftEndTime = timeConvertor(shiftEndTime)
                let shiftEndTimeDecimal = shiftEndTime.hours + (shiftEndTime.minutes/60)
                console.log('shiftEndTimeDecimal -', shiftEndTimeDecimal)
                day.shiftEndTime = shiftEndTime
                if (shiftStartTimeDecimal > shiftEndTimeDecimal) reject("Shift End Time should not be less than Start Time in day : " + day.day)
            }
        }
        
        resolve(positionScheduleModel.findOne({companyId: body.companyId, scheduleTitle: body.scheduleTitle}).lean().exec())
    }).then(positionScheduleExists => {
        if (positionScheduleExists) {
            res.status(400).json({
                status: false,
                message: 'Position schedule already exists with this title'
            })
        }
        else {
            positionScheduleModel.create(body)
                .then(positionSchedule => {
                    res.status(200).json({
                        status: true,
                        message: 'Successfully created position schedule',
                        data: positionSchedule
                    })
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).json({
                        status: false,
                        message: 'Error while creating position schedule',
                        error: err
                    })
                })
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err});
    });
}

/**
*@author Bhaskar Keelu
*@date 09/07/2019
Method to Create a schedule for particular employees.
*/

// let createEmployeeSpecificSchedule = (req, res) => {
//     var body = req.body;
//     return new Promise((response, reject) => {
//         var week_applicable = body.week_applicable
//         var regex = new RegExp(':', 'g');
//         for (var i = 0; i < week_applicable.length; i++) {
//             var start_time = week_applicable[i].starttime;
//             var end_time = week_applicable[i].end_time;
//             if (!week_applicable[i].day) reject("Please select the day");
//             if (!week_applicable[i].starttime && !week_applicable[i].end_time) reject("Please enter the time for day : " + week_applicable[i].day)
//         }
//         response(null)
//     }).then(() => {
//         employeeModel.create(body).then((result) => {
//             if (result) {
//                 res.status(200).json({
//                     status: true,
//                     message: "Successfully inserted the data.",
//                     result: result ? result : []
//                 })
//             }
//         }).catch((err) => {
//             res.status(400).json({
//                 status: true,
//                 message: "Failed to execute the data.",
//                 result: err
//             })
//         })
//     }).catch((error) => {
//         res.status(402).json({
//             status: false,
//             message: "Failed to execute the Query",
//             data: error
//         })
//     });
// }

/**
*@author Bhaskar Keelu
*@date 15/06/2019
Method to import files for BlackOut Employee Groups.
*/

let deletePosition = (req, res) => {
    var body = req.body;
    return new Promise((response, reject) => {
        if (!body._id[0]) reject("Please provide the Id's to delete the data.");
        response(null);
    }).then(() => {
        positionScheduleModel.remove({ _id: { $in: body._id } })
            .then((result) => {
                res.status(200).json({
                    status: true,
                    message: "Successfully deleted the data.",
                    result: result ? result : []
                })
            }).catch((error) => {
                res.status(400).json({
                    status: false,
                    message: "Failed to delete the Data...",
                    error: error
                })
            })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "Failed to delete the Data.",
            error: error
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 16/06/2019
Method to import files for BlackOut Employee Groups.
*/

let ListPositionSchedules = (req, res) => {
    let companyId = req.jwt.companyId;
    let jobTitle;
    if (req.query && req.query.jobTitle) {
        console.log(req.query.jobTitle)
        jobTitle = ObjectID(req.query.jobTitle)
    }
    return new Promise((resolve, reject) => {
        if (!companyId) reject("CompanyId not found");
        resolve(null);
    }).then(() => {
        let filter = {
            companyId: companyId
        }
        if (jobTitle) {
            filter['jobTitle'] = jobTitle
        }
        console.log(filter)
        positionScheduleModel.find(filter).sort({updatedAt: -1}).lean().exec()
            .then(async positionSchedules => {
                for (let positionSchedule of positionSchedules) {
                    let employees = await userModel.find({companyId: companyId, position_work_schedule_id: positionSchedule._id}, {_id:1, 'personal.name': 1}).lean().exec()
                    let employeeIds = employees.map(x => x._id)
                    positionSchedule.employeeIds = employeeIds
                }
                res.status(200).json({
                    status: false,
                    message: "Successfully fetched position work schedules",
                    data: positionSchedules
                })
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({
                    status: false,
                    message: "Error while fecthing position work schedules",
                    error: err
                })
            })
    })
    .catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err});
    });
}
/**
*@author Bhaskar Keelu
*@date 16/06/2019
Method to import files for BlackOut Employee Groups.
*/
let getPositionSchedule = (req, res) => {
    let _id = req.params._id;
    let companyId = req.jwt.companyId
    return new Promise((resolve, reject) => {
        if (!_id) reject("_id is required");
        resolve(null);
    }).then(() => {
        Promise.all([
            positionScheduleModel.findOne({ _id: _id, companyId: companyId }).lean().exec(),
            userModel.find({companyId: companyId, position_work_schedule_id: _id}).lean().exec()    
        ])
        .then(([positionSchedule, employees]) => {
            let employeeIds = employees.map(x => x._id)
            if (positionSchedule) {
                positionSchedule.employeeIds = employeeIds
                for(let dayDetails of positionSchedule.weekApplicable) {
                    dayDetails.shiftStartTime = convertTo12Format(dayDetails.shiftStartTime)
                    dayDetails.shiftEndTime = convertTo12Format(dayDetails.shiftEndTime)
                    if (dayDetails.lunchStartTime) {
                        dayDetails.lunchStartTime = convertTo12Format(dayDetails.lunchStartTime)
                    }
                    if (dayDetails.lunchEndTime) {
                        dayDetails.lunchEndTime = convertTo12Format(dayDetails.lunchEndTime)
                    }
                }
                res.status(200).json({
                    status: true,
                    message: "Successfully fetched position work schedule",
                    data: positionSchedule
                })
            }
            else {
                res.status(404).json({
                    status: false,
                    message: "Position work schedule not found"
                })
            }
        })
        .catch((err) => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: "failed to execute",
                error: err
            })
        })
    }).catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: "failed to execute",
            error: err
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 16/06/2019
Method to update the BlockOut Days based on the location.
*/

let updatePositionSchedule = (req, res) => {
    var body = req.body;
    let _id = body._id
    let companyId = req.jwt.companyId
    let employeeIds = body.employeeIds
    delete body.employeeIds
    var weekApplicable = body.weekApplicable
    return new Promise((resolve, reject) => {
        if (!_id) reject("_id is required");
        if (!body.scheduleTitle) reject('scheduleTitle is required')
        if (!body.scheduleStartDate) reject('scheduleStartDate is required')
        if (!body.type) reject('type is required')
        if (body.type == 'Standard' && !body.jobTitle) reject('jobTitle is required')
        if (!body.daysOfWeek) reject('select days')
        if (!body.daysOfWeek.length) reject('select days')
        if (!body.totalHrsPerDay) reject('Enter totalHrsPerDay')
        if (!body.totalHrsPerWeek) reject('totalHrsPerWeek is required')

        if (body.lunchBreakTime) {
            for (let day of weekApplicable) {
                if (!day.day) reject("Please select the day");
                if (!day.shiftStartTime && !day.lunchStartTime && !day.lunchEndTime && !day.shiftEndTime) reject("Please enter the time for day : " + day.day)
                let shiftStartTime = day.shiftStartTime;
                shiftStartTime = timeConvertor(shiftStartTime)
                let shiftStartTimeDecimal = shiftStartTime.hours + (shiftStartTime.minutes/60)
                console.log('shiftStartTime hours -', typeof shiftStartTime.hours)
                console.log('shiftStartTime minutes -', typeof shiftStartTime.minutes)
                console.log('shiftStartTimeDecimal -', shiftStartTimeDecimal)
                day.shiftStartTime = shiftStartTime
                let lunchStartTime = day.lunchStartTime;
                lunchStartTime = timeConvertor(lunchStartTime)
                let lunchStartTimeDecimal = lunchStartTime.hours + (lunchStartTime.minutes/60)
                console.log('lunchStartTimeDecimal -', lunchStartTimeDecimal)
                day.lunchStartTime = lunchStartTime
                let lunchEndTime = day.lunchEndTime;
                lunchEndTime = timeConvertor(lunchEndTime)
                let lunchEndTimeDecimal = lunchEndTime.hours + (lunchEndTime.minutes/60)
                console.log('lunchEndTimeDecimal -', lunchEndTimeDecimal)
                day.lunchEndTime = lunchEndTime
                let shiftEndTime = day.shiftEndTime;
                shiftEndTime = timeConvertor(shiftEndTime)
                let shiftEndTimeDecimal = shiftEndTime.hours + (shiftEndTime.minutes/60)
                console.log('shiftEndTimeDecimal -', shiftEndTimeDecimal)
                day.shiftEndTime = shiftEndTime
                if (shiftStartTimeDecimal > lunchStartTimeDecimal) reject("Lunch Start Time should not be less than Start Time in day : " + day.day)
                if (lunchStartTimeDecimal > lunchEndTimeDecimal) reject("Lunch End Time should not be less than Lunch Start Time in day : " + day.day)
                if (lunchEndTimeDecimal > shiftEndTimeDecimal) reject("End Time should not be less than Lunch End Time in day : " + day.day)            
            }
        }
        else {
            for (let day of weekApplicable) {
                if (!day.day) reject("Please select the day");
                if (!day.shiftStartTime && !day.shiftEndTime) reject("Please enter the time for day : " + day.day)
                let shiftStartTime = day.shiftStartTime;
                shiftStartTime = timeConvertor(shiftStartTime)
                let shiftStartTimeDecimal = shiftStartTime.hours + (shiftStartTime.minutes/60)
                console.log('shiftStartTime hours -', typeof shiftStartTime.hours)
                console.log('shiftStartTime minutes -', typeof shiftStartTime.minutes)
                console.log('shiftStartTimeDecimal -', shiftStartTimeDecimal)
                day.shiftStartTime = shiftStartTime
                
                let shiftEndTime = day.shiftEndTime;
                shiftEndTime = timeConvertor(shiftEndTime)
                let shiftEndTimeDecimal = shiftEndTime.hours + (shiftEndTime.minutes/60)
                console.log('shiftEndTimeDecimal -', shiftEndTimeDecimal)
                day.shiftEndTime = shiftEndTime
                if (shiftStartTimeDecimal > shiftEndTimeDecimal) reject("Shift End Time should not be less than Start Time in day : " + day.day)
            }
        }
        resolve(positionScheduleModel.findOne({companyId: body.companyId, scheduleTitle: body.scheduleTitle, _id: {$ne: ObjectID(_id)}}).lean().exec())
    }).then(positionScheduleExists => {
        if (positionScheduleExists) {
            res.status(400).json({
                status: false,
                message: 'Position schedule already exists with this title'
            })
        }
        else {
            delete body._id
            delete body.companyId
            delete body.createdAt
            delete body.updatedAt
            let filter = {
                companyId: companyId,
                _id: _id
            }
            positionScheduleModel.findOneAndUpdate(filter, {
                $set: body
            }, {
                new: true, useFindAndModify: false
            })
                .then(response => {
                    if (response) {
                        res.status(200).json({
                            status: true,
                            message: "Successfully updated the data",
                            data: response
                        })
                    }
                    else {
                        res.status(404).json({
                            status: false,
                            message: "Position schedule not found"
                        })
                    }
                })
                .catch(err => {
                    console.log(err)
                    res.status(400).json({
                        status: false,
                        message: "Error while updating position schedule",
                        error: err
                    })
                })
        }
    }).catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 19/06/2019
Method to import files for BlackOut Employee Groups.
*/

let createDepartmentSchedule = (req, res) => {
    var body = req.body;
    return new Promise((response, reject) => {
        var openTime = body.Open_Time;
        var closeTime = body.Close_Time;
        var StartDate = body.StartDate;
        var EndDate = body.EndDate;
        var regex = new RegExp(':', 'g');
        if (parseInt(openTime.replace(regex, ''), 10) > parseInt(closeTime.replace(regex, ''), 10)) reject("Department Close Time should not be less than Open Time.");
        if (StartDate > EndDate) reject("Department End Time should not be less than Start Time.");
        if (body.Schedule_StartDate > body.Schedule_EndDate) reject("Department End Time should not be less than Start Time.");
        response(null)
    }).then(() => {
        departmentModel.create(body).then((resp) => {
            if (resp) {
                res.status(200).json({
                    "status": true,
                    "message": "Successfully inserted the Department Work Schedule.",
                    "result": resp ? resp : []
                })
            } else {
                res.status(200).json({
                    status: false,
                    message: "Failed to execute the Query.",
                    result: result ? result : []
                })
            }
        }).catch((error) => {
            res.status(402).json({
                status: false,
                message: "Failed to execute the Query",
                data: null
            })
        });
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "Failed to execute the query",
            result: error
        })
    })
}

/**
*@author Asma
*@date 04/09/2019 17:58
Method to create department work schedule
*/
let createDepartmentScheduleNew = (req, res) => {
    let body = req.body
    let companyId = req.jwt.companyId
    body.companyId = companyId
    let departmentEmployeesScheduleInfo = body.departmentEmployeesScheduleInfo
    delete body.departmentEmployeesScheduleInfo
    return new Promise((resolve, reject) => {
        if(!body.departmentScheduleName) reject('Enter Department Schedule Name')
        if(!body.openTime) reject('openTime is required');
        if(!body.closeTime) reject('closeTime  is required');
        if(!body.intervalSpan) reject('intervalSpan  is required');
        if(!body.resourceType) reject('resourceType  is required');
        if(!body.daysOfWeekApplicable) reject('daysOfWeekApplicable  is required');
        if (!body.daysOfWeekApplicable.length) reject('select days')
        if(!body.startDate) reject('startDate  is required');
        if(!body.endDate) reject('endDate  is required');
        if(!body.intervalResourcesInformation) reject('intervalResourcesInformation  is required')
        if(!body.intervalResourcesInformation.length) reject('intervalResourcesInformation  is required')
        if(!departmentEmployeesScheduleInfo) reject('departmentEmployeesScheduleInfo  is required')
        if(!departmentEmployeesScheduleInfo.length) reject('departmentEmployeesScheduleInfo  is required')
        resolve(departmentScheduleModel.findOne({companyId: companyId, departmentScheduleName: body.departmentScheduleName}).lean().exec())
    })
    .then(departmentSchedule => {
        if (departmentSchedule) {
            res.status(400).json({
                status: false,
                message: "Department schedule already exists with this name"
            })
        }
        else {
            body.openTime = timeConvertor(body.openTime)
            body.closeTime = timeConvertor(body.closeTime)
            body.intervalResourcesInformation = body.intervalResourcesInformation.map(x => {
                x.scheduledTime = timeConvertor(x.scheduledTime)
                return x
            })
            departmentScheduleModel.create(body)
                .then(departmentSchedule => {
                    departmentEmployeesScheduleInfo = departmentEmployeesScheduleInfo.map(x => {
                        x.companyId = companyId
                        x.departmentSchedule = departmentSchedule._id
                        x.intervalsInfo = x.intervalsInfo.map(y => {
                            y.scheduledTime = timeConvertor(y.scheduledTime)
                            return y
                        })
                        return x;
                    })
                    departmentEmployeesScheduleInfoModel2.insertMany(departmentEmployeesScheduleInfo)
                        .then(response => {
                            res.status(200).json({
                                status: true,
                                message: 'Successfully created department work schedule',
                                data: {
                                    departmentSchedule: departmentSchedule,
                                    departmentEmployeesScheduleInfo: response
                                }
                            })
                        })
                        .catch(err => {
                            console.log(err)
                            res.status(500).json({
                                status: false,
                                message: "Error while creating department work schedule",
                                error: err
                            })
                        })
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).json({
                        status: false,
                        message: "Error while creating department work schedule",
                        error: err
                    })
                })
        }
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

/**
*@author Asma
*@date 05/09/2019 18:56
Method to get a list of department schedules
*/
let listDepartmentSchedules = (req, res) => {
    var companyId = req.jwt.companyId;
    return new Promise((resolve, reject) => {
        if (!companyId) reject("companyId not found");
        resolve(null);
    }).then(() => {
        departmentScheduleModel.find({ companyId: companyId }, {intervalResourcesInformation : 0}).lean().exec()
            .then(async result => {
                for (let schedule of result) {
                    schedule.totalHrsPerDay = schedule.closeTime.hours - schedule.openTime.hours
                    let assignedEmployees = await departmentEmployeesScheduleInfoModel2.distinct("userId", {companyId: companyId, departmentSchedule: schedule._id}).exec()
                    // console.log(assignedEmployees)
                    schedule.assignedEmployeesCount = assignedEmployees.length
                }
                res.status(200).json({
                    status: true,
                    message: "Successfully fetched department schedules",
                    result: result
                })
            }).catch((err) => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while fetching department schedules",
                    error: err
                })
            })
    }).catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: "Error while fetching department schedules",
            error: err
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 19/06/2019
Method to get details of one department work schedule
*/
let getDepartmentSchedule = (req, res) => {
    let _id = req.params._id;
    let companyId = req.jwt.companyId
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let filter = { companyId: companyId, _id: ObjectID(_id) }
        console.log(filter)
        departmentScheduleModel.findOne(filter).lean().exec()
            .then(async departmentSchedule => {
                console.log(departmentSchedule)
                if (departmentSchedule) {
                    departmentSchedule.openTime = convertTo12Format(departmentSchedule.openTime)
                    departmentSchedule.closeTime = convertTo12Format(departmentSchedule.closeTime)
                    departmentSchedule.intervalResourcesInformation = departmentSchedule.intervalResourcesInformation.map(x => {
                        x.scheduledTime = convertTo12Format(x.scheduledTime)
                        return x
                    })
                    let departmentEmployeesScheduleInfo = await departmentEmployeesScheduleInfoModel2.find({companyId, departmentSchedule: ObjectID(_id)}).populate('userId', 'personal.name').lean().exec()
                    for (let statusInfo of departmentEmployeesScheduleInfo) {
                        statusInfo.intervalsInfo = statusInfo.intervalsInfo.map(x => {
                            x.scheduledTime = convertTo12Format(x.scheduledTime)
                            return x
                        })
                    }
                    let assignedEmployees = await departmentEmployeesScheduleInfoModel2.distinct("userId", {companyId: companyId, departmentSchedule: ObjectID(_id)}).exec()
                    res.status(200).json({
                        status: true,
                        message: "Successfully fetched department work schedule",
                        data: {
                            departmentSchedule: departmentSchedule,
                            departmentEmployeesScheduleInfo : departmentEmployeesScheduleInfo,
                            assignedEmployees: assignedEmployees
                        }
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "Department schedule not found"
                    })
                }
            }).catch(err => {
                console.log(err)
                res.status(500).json({
                    status: false,
                    message: "Error while fetching department work schedule",
                    error: err
                })
            })
    }).catch(err => {
        console.log(err)
        res.status(500).json({
            status: false,
            message: "Error while fetching department work schedule",
            error: err
        })
    })
}


/**
*@author Asma
*@date 09/09/2019 14:11
Method to edit department schedule
*/
let editDepartmentSchedule = (req, res) => {
    let body = req.body
    let _id = body._id
    let companyId = req.jwt.companyId
    let departmentEmployeesScheduleInfo = body.departmentEmployeesScheduleInfo
    delete body.departmentEmployeesScheduleInfo
    delete body.createdBy
    delete body.createdAt
    delete body.updatedBy
    delete body.updatedAt
    return new Promise((resolve, reject) => {
        if(!body.departmentScheduleName) reject('Enter Department Schedule Name')
        if(!body.openTime) reject('openTime is required');
        if(!body.closeTime) reject('closeTime  is required');
        if(!body.intervalSpan) reject('intervalSpan  is required');
        if(!body.resourceType) reject('resourceType  is required');
        if(!body.daysOfWeekApplicable) reject('daysOfWeekApplicable  is required');
        if (!body.daysOfWeekApplicable.length) reject('select days')
        if(!body.startDate) reject('startDate  is required');
        if(!body.endDate) reject('endDate  is required');
        if(!body.intervalResourcesInformation) reject('intervalResourcesInformation  is required')
        if(!body.intervalResourcesInformation.length) reject('intervalResourcesInformation  is required')
        if(!departmentEmployeesScheduleInfo) reject('departmentEmployeesScheduleInfo  is required')
        if(!departmentEmployeesScheduleInfo.length) reject('departmentEmployeesScheduleInfo  is required')
        resolve(departmentScheduleModel.findOne({companyId: companyId, departmentScheduleName: body.departmentScheduleName, _id: {$ne: ObjectID(_id)}}).lean().exec())
    })
    .then(departmentSchedule => {
        if (departmentSchedule) {
            res.status(400).json({
                status: false,
                message: "Department schedule already exists with this name"
            })
        }
        else {
            body.openTime = timeConvertor(body.openTime)
            body.closeTime = timeConvertor(body.closeTime)
            body.intervalResourcesInformation = body.intervalResourcesInformation.map(x => {
                x.scheduledTime = timeConvertor(x.scheduledTime)
                return x
            })
            let promises = [
                departmentScheduleModel.findOneAndUpdate({companyId: companyId, _id: body._id}, {$set: body}, {new: true, useFindAndModify: false})
            ]
            departmentEmployeesScheduleInfo = departmentEmployeesScheduleInfo.map(x => {
                x.companyId = companyId
                x.departmentSchedule = _id
                x.intervalsInfo = x.intervalsInfo.map(y => {
                    y.scheduledTime = timeConvertor(y.scheduledTime)
                    return y
                })
                return x;
            })
            for (let x of departmentEmployeesScheduleInfo) {
                if (x._id) {
                    console.log(x)
                    delete x.createdAt
                    delete x.updatedAt
                    promises.push(departmentEmployeesScheduleInfoModel2.findOneAndUpdate({companyId: companyId, _id: x._id}, {$set: x}, {new: true, useFindAndModify: false}))
                }
                else {
                    promises.push(departmentEmployeesScheduleInfoModel2.create(x))
                }
            }
            console.log(promises.length)
            Promise.all(promises)
                .then(responses => {
                    res.status(200).json({
                        status: true,
                        message: 'Successfully updated department work schedule',
                        data: responses
                    })
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).json({
                        status: false,
                        message: "Error while updating department work schedule",
                        error: err
                    })
                })
        }
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 19/06/2019
Method to import files for BlackOut Employee Groups.
*/

let deleteDepartment = (req, res) => {
    var body = req.body;
    return new Promise((resolve, reject) => {
        if (!body._id) reject("_ids not found");
        if (!body._id.length) reject("_ids not found");
        resolve(null);
    }).then(() => {
        departmentScheduleModel.deleteMany({ _id: { $in: body._id } })
            .then((result) => {
                res.status(200).json({
                    status: true,
                    message: "Successfully deleted the data.",
                    result: result
                })
            }).catch((error) => {
                res.status(400).json({
                    status: false,
                    message: "Failed to delete the Data...",
                    error: error
                })
            })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "Failed to delete the Data.",
            error: error
        })
    })
}

let loadHoliday = (req, res) => {
    let year = req.params.year
    var holidayList = [{
        "name": "New Year's Day",
        "date": new Date("01-01-" + year)
    }, {
        "name": "Martin Luther King Day",
        "date": new Date("01-21-" + year)
    }, {
        "name": "Presidents' Day",
        "date": new Date("02-18-" + year)
    }, {
        "name": "Memorial Day",
        "date": new Date("04-27-" + year)
    }, {
        "name": "Independence Day",
        "date": new Date("07-04-" + year)
    }, {
        "name": "Labor Day",
        "date": new Date("09-02-" + year)
    }, {
        "name": "Columbus Day",
        "date": new Date("10-14-" + year)
    }, {
        "name": "Veterans Day",
        "date": new Date("11-11-" + year)
    }, {
        "name": "Thanksgiving Day",
        "date": new Date("11-28-" + year)
    }, {
        "name": "Christmas Day",
        "date": new Date("12-25-" + year)
    }]
    res.status(200).json({
        status: true,
        result: holidayList
    })
}

/**
*@author Bhaskar Keelu
*@date 26/06/2019
Method to update the Department Schedule.
*/

let updateDepartment = (req, res) => {
    var body = req.body;
    return new Promise((response, reject) => {
        if (!body._id) reject("Please enter the update details.");
        response(null);
    }).then(() => {
        departmentModel.update({ _id: body._id }, { $set: body.params })
            .then((result) => {
                res.status(200).json({
                    status: true,
                    message: "Successfully updated the data"
                })
            }).catch((err) => {
                res.status(400).json({
                    status: false,
                    message: "Failed to update the data",
                    data: err
                })
            })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "Failed to update the data",
            data: error
        })
    })

}

let getEmployeeTimeSchedule = (req, res) => {
    var body = req.body;
    return new Promise((response, reject) => {
        if (!body.Employee_id) reject("Please enter the Employee ID.");
        response(null);
    }).then(() => {
        positionScheduleModel.find({ positionScheduleSchema: { $in: body.Employee_id } }).then((result) => {
            res.status(200).json({
                status: true,
                message: "Successfully fetched the data",
                result: result ? result : []
            })
        }).catch((err) => {
            res.status(200).json({
                status: false,
                message: "Failed to Execute the Query",
                result: err
            })
        })
    }).catch((err) => {
        res.status(200).json({
            status: false,
            message: "Failed to Execute the Query",
            result: err
        })
    })
}

let getEmployeeWorkSchedule = (req, res) => {
    let body = req.body;
    return new Promise((response, reject) => {
        if (!body.schedulename) reject("Please enter the Schedule name.");
        if (!body.scheduletype) reject("Please enter the Schedule type.");
        response(null);
    }).then(() => {
        positionScheduleModel.find({ "scheduletitle": body.schedulename, "type": body.scheduletype }).then((result) => {
            res.status(200).json({
                status: true,
                message: "Successfully fetched the data.",
                result: result ? result : []
            })
        }).then((err) => {
            res.status(400).json({
                status: false,
                message: "Failed to fetch the data",
                error: err
            })
        })
    })
}

let getEmpWorkTime = (req, res) => {
    let body = req.body;
    let per_page, page_no;
    return new Promise((response, reject) => {
        if (!body.employeelist) reject("Please enter the employee.");
        if (body.per_page < body.page_no) {
            res.status(200).json({
                "status": false,
                "message": "Page_Number should not be greater than per_page"
            })
        }
        per_page = body.per_page ? body.per_page : 10
        page_no = body.page_no ? body.page_no : 1
        response(null);
    }).then(() => {
        userModel.find({ "_id": { $in: body.employeelist } }, { personal: 1 }).skip((page_no - 1) * per_page).limit(per_page).populate("positionworkschedule_id").then((result) => {
            res.status(200).json({
                status: true,
                message: "Successfully fetched the data.",
                result: result ? result : []
            })
        }).catch((err) => {
            res.status(400).json({
                status: false,
                message: "Failed to fetch the data",
                error: err
            })
        })
    })
}

let getYearBasedHolidays = (req, res) => {
    let year = req.params.year;
    return new Promise((response, reject) => {
        if (!year) reject("Please enter the year.");
        response(null);
    }).then(() => {
        holidayModel.find({ "year": year }).then((result) => {
            res.status(200).json({
                status: true,
                message: "Successfully fetched the data.",
                result: result ? result : []
            })
        }).catch((err) => {
            res.status(400).json({
                status: false,
                message: "Failed to fetch the data",
                error: err
            })
        })
    })
}

let getPositionHistory = (req, res) => {
    let empId = req.params.empId;
    let positionId = req.params.positionId;
    return new Promise((response, reject) => {
        if (!empId) reject("Please enter the empId.");
        response(null);
    }).then(() => {
        userModel.find({ "_id": empId, "position_work_schedule_history.positionschedule_id": positionId }).populate("positionworkschedule_id").then((result) => {
            if (result.length != 0) {
                res.status(200).json({
                    status: true,
                    message: "Successfully fetched the data...",
                    result: result ? result : []
                })
            } else {
                userModel.find({ "_id": empId }).populate("positionworkschedule_id").then((Idresult) => {
                    if (Idresult.length != 0) {
                        res.status(200).json({
                            status: true,
                            message: "Successfully fetched the data.",
                            result: Idresult ? Idresult : []
                        })
                    } else {
                        res.status(200).json({
                            status: true,
                            message: "No data.",
                            result: result
                        })
                    }
                })
            }
        }).catch((err) => {
            res.status(400).json({
                status: false,
                message: "Failed to fetch the data",
                error: err
            })
        })
    })
}

module.exports = {
    createHolidays,
    addHolidays,
    updateHolidaysDetails,
    // deleteHolidayDetails,
    deleteHolidays,
    getholidayDetails,
    getHoliday,
    loadHoliday,

    createBlackoutEmployeeGroup,
    getBlackOutEmpGrp,
    addBlackoutDates,
    deleteBlackOut,
    updateBlackoutDetails,
    getBlackOutDetails,
    
    importXlsx,

    createPositionSchedule,
    deletePosition,
    ListPositionSchedules,
    getPositionSchedule,
    updatePositionSchedule,

    createDepartmentSchedule,
    createDepartmentScheduleNew,
    getDepartmentSchedule,
    listDepartmentSchedules,
    deleteDepartment,
    editDepartmentSchedule,
    updateDepartment,

    getEmployeeTimeSchedule,
    getEmployeeWorkSchedule,
    getEmpWorkTime,
    // createEmployeeSpecificSchedule,
    getYearBasedHolidays,
    getPositionHistory,

    convertTo12Format
}

// if (parseInt(shiftStartTime.replace(regex, ''), 10) > parseInt(lunchStartTime.replace(regex, ''), 10)) reject("Lunch Start Time should not be less than Start Time in day : " + day.day)
// if (parseInt(lunchStartTime.replace(regex, ''), 10) > parseInt(lunchEndTime.replace(regex, ''), 10)) reject("Lunch End Time should not be less than Lunch Start Time in day : " + day.day)
// if (parseInt(lunchEndTime.replace(regex, ''), 10) > parseInt(shiftEndTime.replace(regex, ''), 10)) reject("End Time should not be less than Lunch End Time in day : " + day.day)