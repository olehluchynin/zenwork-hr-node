let express = require('express');

let router = express.Router();

var schedulerController = require('./controller');

var multipart = require('connect-multiparty');

var multipartMiddleware = multipart();

let authMiddleware = require('../../middlewares/auth');

let validationsMiddleware = require('../../middlewares/validations');

router.post(
    '/createHoliday',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    schedulerController.createHolidays
);

router.post(
    '/addHolidays',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    schedulerController.addHolidays
);


router.post(
    '/updateHoliday',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    schedulerController.updateHolidaysDetails
);

router.post(
    '/deleteHolidays',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    schedulerController.deleteHolidays
);

router.get(
    '/getHolidayList/:year',
    authMiddleware.isUserLogin,
    schedulerController.getholidayDetails
);

router.get(
    '/getHoliday/:_id',
    authMiddleware.isUserLogin,
    schedulerController.getHoliday
);

router.get(
    '/loadholidays/:year',
    schedulerController.loadHoliday
);

router.post(
    '/createBlackoutEmployeeGroup',
    authMiddleware.isUserLogin,
    schedulerController.createBlackoutEmployeeGroup
);

router.get(
    '/getempgrp/:company_id',
    authMiddleware.isUserLogin,
    schedulerController.getBlackOutEmpGrp
);

router.post(
    '/addBlackoutDates',
    authMiddleware.isUserLogin,
    schedulerController.addBlackoutDates
);

router.post('/deleteblackout',schedulerController.deleteBlackOut);

router.post(
    '/updateblackout',
    authMiddleware.isUserLogin,
    schedulerController.updateBlackoutDetails
);

router.post(
    '/getblackout',
    authMiddleware.isUserLogin,
    schedulerController.getBlackOutDetails
);

router.post('/import',multipartMiddleware,schedulerController.importXlsx);

router.post(
    '/createPositionSchedule',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    schedulerController.createPositionSchedule
);

router.post('/deleteposition',schedulerController.deletePosition);

router.get(
    '/listPositionSchedules',
    authMiddleware.isUserLogin,
    schedulerController.ListPositionSchedules
);

router.get(
    '/getPositionSchedule/:_id',
    authMiddleware.isUserLogin,
    schedulerController.getPositionSchedule
);

router.put(
    '/updatePositionSchedule',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    schedulerController.updatePositionSchedule
);
router.post('/createdepartment',schedulerController.createDepartmentSchedule);

router.post(
    '/createDepartmentSchedule',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    schedulerController.createDepartmentScheduleNew
);

router.get(
    '/listDepartmentSchedules',
    authMiddleware.isUserLogin,
    schedulerController.listDepartmentSchedules
);

router.get(
    '/getDepartmentSchedule/:_id',
    authMiddleware.isUserLogin,
    schedulerController.getDepartmentSchedule
);

router.post(
    '/deletedepartment',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    schedulerController.deleteDepartment
);

router.post('/updatedepartment',schedulerController.updateDepartment);

router.post(
    '/editDepartmentSchedule', 
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    schedulerController.editDepartmentSchedule
);

router.post('/getemptime',schedulerController.getEmployeeTimeSchedule);
router.post('/getempschedule',schedulerController.getEmployeeWorkSchedule);
router.post('/getempworktime',schedulerController.getEmpWorkTime);
// router.post('/employeespecific',schedulerController.createEmployeeSpecificSchedule);
router.get('/yearbasedholiday/:year',schedulerController.getYearBasedHolidays)
router.get('/postionhistory/:empId/:positionId',schedulerController.getPositionHistory)

module.exports = router;