/**
 * @api {post} /scheduler/createHoliday Create Holiday
 * @apiName Create Holiday
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "location_id": "5cb1d02318598b356a883bc3",
    "country": "India",
    "year": 2019,
    "holiday_name": "Independence Day",
    "holiday_date": "2019-08-15T13:20:12.015Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully added holiday",
    "result": {
        "_id": "5d482d47feaf2b3ec0e146a6",
        "location_id": "5cb1d02318598b356a883bc3",
        "year": 2019,
        "holiday_name": "Independence Day",
        "holiday_date": "2019-08-15T13:20:12.015Z",
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-08-05T13:21:11.365Z",
        "updatedAt": "2019-08-05T13:21:11.365Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /scheduler/addHolidays AddHolidays
 * @apiName AddHolidays
 * @apiGroup Scheduler
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "location_ids": [
        "5cb1d02318598b356a883bc3",
        "5d023dfd65807b01afd238a3"
    ],
    "country": "India",
    "year": 2019,
    "holiday_name": "IT Day",
    "holiday_date": "2019-12-31T13:20:12.015Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully added holidays",
    "result": [
        {
            "_id": "5d5fc28aaec57b3ef024b1ab",
            "companyId": "5c9e11894b1d726375b23058",
            "year": 2019,
            "holiday_name": "IT Day",
            "holiday_date": "2019-12-31T13:20:12.015Z",
            "location_id": "5cb1d02318598b356a883bc3",
            "createdAt": "2019-08-23T10:40:10.525Z",
            "updatedAt": "2019-08-23T10:40:10.525Z"
        },
        {
            "_id": "5d5fc28aaec57b3ef024b1ac",
            "companyId": "5c9e11894b1d726375b23058",
            "year": 2019,
            "holiday_name": "IT Day",
            "holiday_date": "2019-12-31T13:20:12.015Z",
            "location_id": "5d023dfd65807b01afd238a3",
            "createdAt": "2019-08-23T10:40:10.525Z",
            "updatedAt": "2019-08-23T10:40:10.525Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/updateHoliday Update Holiday
 * @apiName Update Holiday
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "_id": "5d482d47feaf2b3ec0e146a6",
    "location_id": "5cb1d02318598b356a883bc3",
    "year": 2019,
    "holiday_name": "Independence Day",
    "holiday_date": "2019-08-15T13:10:12.015Z",
    "companyId": "5c9e11894b1d726375b23058",
    "createdAt": "2019-08-05T13:21:11.365Z",
    "updatedAt": "2019-08-05T13:21:11.365Z"
}    
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Holiday updated successfully",
    "data": {
        "_id": "5d482d47feaf2b3ec0e146a6",
        "location_id": "5cb1d02318598b356a883bc3",
        "year": 2019,
        "holiday_name": "Independence Day",
        "holiday_date": "2019-08-15T13:10:12.015Z",
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-08-05T13:21:11.365Z",
        "updatedAt": "2019-08-06T05:21:59.297Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/deleteHolidays Delete Holiday
 * @apiName Delete Holiday
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "_ids": [
    	"5d482d2bfeaf2b3ec0e146a5"
    	]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {get} /scheduler/getHoliday/:_id Get Holiday
 * @apiName Get Holiday
 * @apiGroup Scheduler
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} _id Holidays unique _id
 *    
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched holiday",
    "data": {
        "_id": "5d482d47feaf2b3ec0e146a6",
        "location_id": {
            "_id": "5cb1d02318598b356a883bc3",
            "name": "Banglore"
        },
        "year": 2019,
        "holiday_name": "Independence Day",
        "holiday_date": "2019-08-15T13:10:12.015Z",
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-08-05T13:21:11.365Z",
        "updatedAt": "2019-08-06T05:21:59.297Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/createBlackoutEmployeeGroup CreateBlackoutEmployeeGroup
 * @apiName CreateBlackoutEmployeeGroup
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
	"companyId" : "5c9e11894b1d726375b23058",
	"name" : "Testing",
	"effectiveDate" : "2019-08-23T06:03:17.704Z",
	"initial_group_effective_date_details" : {
        "date_selection" : "date_of_hire"
    },
    "employee_ids" : [
        "5d281a8504cfec208474dea9",
        "5d281a9804cfec208474deba"
    ]
}   
 * @apiParamExample {json} Request-Example2:
 *
{
	"companyId" : "5c9e11894b1d726375b23057",
	"name" : "Test",
	"effectiveDate" : "2019-02-11T06:29:57.924Z",
	"initial_group_effective_date_details" : {
        "date_selection" : "custom_date",
        "custom_date_field": "Blackout effective date"
    }
    "employee_ids" : [
        "5c9e11894b1d726375b23056",
        "5c9e11894b1d726375b23055"
    ]
}    
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully created blackout employee group",
    "data": {
        "_id": "5d5f82b4dc08d14214322091",
        "companyId": "5c9e11894b1d726375b23058",
        "name": "Testing",
        "effectiveDate": "2019-08-23T06:03:17.704Z",
        "initial_group_effective_date_details": {
            "date_selection": "date_of_hire"
        },
        "createdAt": "2019-08-23T06:07:48.875Z",
        "updatedAt": "2019-08-23T06:07:48.875Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/addBlackoutDates AddBlackoutDates
 * @apiName AddBlackoutDates
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
[
    {
        "location_id" : "5cb1d02318598b356a883bc3",
        "blackoutName" : "Department",
        "blackoutEmployeeGroup" : "5d5f8b06c6004237674abd8a",
        "startDate" : "2019-02-11T06:29:57.924Z",
        "endDate" : "2019-05-01T06:29:57.924Z",
        "blockType" : "Block"
    },
    {
        "location_id" : "5cb1d02318598b356a883bc3",
        "blackoutName" : "zenworkRequirement",
        "blackoutEmployeeGroup" : "5d5f82b4dc08d14214322091",
        "startDate" : "2019-03-11T06:29:57.924Z",
        "endDate" : "2019-06-01T06:29:57.924Z",
        "blockType" : "Single Day Request Only"
    }
]     
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully added blackout dates",
    "result": [
        {
            "_id": "5d5fa9a04e51f5222ca0bb56",
            "location_id": "5cb1d02318598b356a883bc3",
            "blackoutName": "Department",
            "blackoutEmployeeGroup": "5d5f8b06c6004237674abd8a",
            "startDate": "2019-02-11T06:29:57.924Z",
            "endDate": "2019-05-01T06:29:57.924Z",
            "blockType": "Block",
            "companyId": "5c9e11894b1d726375b23058",
            "createdAt": "2019-08-23T08:53:52.279Z",
            "updatedAt": "2019-08-23T08:53:52.279Z"
        },
        {
            "_id": "5d5fa9a04e51f5222ca0bb57",
            "location_id": "5cb1d02318598b356a883bc3",
            "blackoutName": "zenworkRequirement",
            "blackoutEmployeeGroup": "5d5f82b4dc08d14214322091",
            "startDate": "2019-03-11T06:29:57.924Z",
            "endDate": "2019-06-01T06:29:57.924Z",
            "blockType": "Single Day Request Only",
            "companyId": "5c9e11894b1d726375b23058",
            "createdAt": "2019-08-23T08:53:52.279Z",
            "updatedAt": "2019-08-23T08:53:52.279Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/updateblackout Update Blackout
 * @apiName Update Blackout
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"_id" : "5d021e6ccce65213e4d1395f",
	"blackoutName" : "Tax"
}    
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully updated the data"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/deleteblackout Delete BlackOut
 * @apiName Delete BlackOut
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"_id" : ["5d0220279460fd199c1070c1","5d021fcc9460fd199c1070c0"]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully deleted the data.",
    "result": {
        "n": 2,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {get} /scheduler/getHolidayList/:year GetHolidayList
 * @apiName GetHolidayList
 * @apiGroup Scheduler
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {Number} year year(2019, 2020)
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched holidays",
    "data": [
        {
            "_id": "5d482d47feaf2b3ec0e146a6",
            "location_id": {
                "_id": "5cb1d02318598b356a883bc3",
                "name": "Banglore"
            },
            "year": 2019,
            "holiday_name": "Independence Day",
            "holiday_date": "2019-08-15T13:10:12.015Z",
            "companyId": "5c9e11894b1d726375b23058",
            "createdAt": "2019-08-05T13:21:11.365Z",
            "updatedAt": "2019-08-06T05:21:59.297Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/getblackout Get BlackOutList
 * @apiName Get BlackOutList
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"country" : "India"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched the data.",
    "result": [
        {
            "_id": "5d021e6ccce65213e4d1395f",
            "country": "India",
            "companyId": "5c9e11894b1d726375b23057",
            "location_id": "5cb1d02318598b356a883bc3",
            "blackoutName": "Tax",
            "employeeGrpName": "5d021d90cce65213e4d1395e",
            "startDate": "2019-02-11T06:29:57.924Z",
            "endDate": "2019-05-01T06:29:57.924Z",
            "blockType": "Block",
            "createdAt": "2019-06-13T09:59:08.053Z",
            "updatedAt": "2019-06-13T10:04:25.365Z"
        },
        {
            "_id": "5d0220b19460fd199c1070c2",
            "country": "India",
            "companyId": "5c9e11894b1d726375b23057",
            "location_id": "5cb1d02318598b356a883bc3",
            "blackoutName": "Financial Department",
            "employeeGrpName": "5d021d90cce65213e4d1395e",
            "startDate": "2019-02-11T06:29:57.924Z",
            "endDate": "2019-05-01T06:29:57.924Z",
            "blockType": "Block",
            "createdAt": "2019-06-13T10:08:49.734Z",
            "updatedAt": "2019-06-13T10:08:49.734Z"
        },
        {
            "_id": "5d0220c29460fd199c1070c3",
            "country": "India",
            "companyId": "5c9e11894b1d726375b23057",
            "location_id": "5cb1d02318598b356a883bc3",
            "blackoutName": "Department",
            "employeeGrpName": "5d021d90cce65213e4d1395e",
            "startDate": "2019-02-11T06:29:57.924Z",
            "endDate": "2019-05-01T06:29:57.924Z",
            "blockType": "Block",
            "createdAt": "2019-06-13T10:09:06.490Z",
            "updatedAt": "2019-06-13T10:09:06.490Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/import Upload Xlsx
 * @apiName Upload Xlsx
 * @apiGroup Scheduler
 * 
 * 
 * @apiHeader {String} x-access-token User login token.

 * @apiParam {file} file file.xlsx
 * @apiParam {companyId} companyId CompanyID
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Successfully Added 1 Records"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

 /**
 * @api {get} /getempgrp/company_id Get Employee Group
 * @apiName Get Employee Group
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
   
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched the data.",
    "result": [
        {
            "Eligibility_criteria": {
                "sub_fields": [
                    "Tech",
                    "Sales"
                ],
                "selected_field": "Department"
            },
            "specific_employee": {
                "include": [
                    3254,
                    54654
                ],
                "exclude": [
                    465
                ]
            },
            "emp_effective_date": {
                "date_selection": "date_of_hire",
                "custom_date_field": ""
            },
            "_id": "5d021d90cce65213e4d1395e",
            "companyId": "5c9e11894b1d726375b23058",
            "name": "Test",
            "effectiveDate": "2019-02-11T06:29:57.924Z",
            "createdAt": "2019-06-13T09:55:28.119Z",
            "updatedAt": "2019-06-13T09:55:28.119Z"
        },
        {
            "Eligibility_criteria": {
                "sub_fields": [
                    "Unit 1"
                ],
                "selected_field": "Business Unit"
            },
            "specific_employee": {
                "include": [],
                "exclude": []
            },
            "emp_effective_date": {
                "date_selection": "Date of Hire",
                "custom_date_field": ""
            },
            "_id": "5d049c78b8e344200ee168a1",
            "companyId": "5c9e11894b1d726375b23058",
            "name": "Employee group 1",
            "effectiveDate": "2019-07-11T18:30:00.000Z",
            "createdAt": "2019-06-15T07:21:28.514Z",
            "updatedAt": "2019-06-15T07:21:28.514Z"
        },
        {
            "Eligibility_criteria": {
                "sub_fields": [
                    "Unit 2",
                    "Unit 1"
                ],
                "selected_field": "Business Unit"
            },
            "specific_employee": {
                "include": [
                    "5cdf0dc1df4fd06b83354f2a",
                    "5cdec1d5df4fd06b83354eb8",
                    "5cd91cc3f308c21c43e5054d",
                    "5cd91e68f308c21c43e50559",
                    "5cd91d94f308c21c43e50553"
                ],
                "exclude": []
            },
            "emp_effective_date": {
                "date_selection": "Date of Hire",
                "custom_date_field": ""
            },
            "_id": "5d049d19b8e344200ee168a2",
            "companyId": "5c9e11894b1d726375b23058",
            "name": "Group 2",
            "effectiveDate": "2019-07-10T18:30:00.000Z",
            "createdAt": "2019-06-15T07:24:09.663Z",
            "updatedAt": "2019-06-15T07:24:09.663Z"
        },
        {
            "Eligibility_criteria": {
                "sub_fields": [
                    "Unit 1",
                    "Unit 2"
                ],
                "selected_field": "Business Unit"
            },
            "specific_employee": {
                "include": [
                    "5cd91cc3f308c21c43e5054d",
                    "5cd91d94f308c21c43e50553",
                    "5cd91e68f308c21c43e50559",
                    "5cda3d4a2fec3c2610219939",
                    "5cda3ad10e57b225e1240c9d",
                    "5cda377f51f0be25b3adceb4",
                    "5cd93e4a8e9dea1f4bb987dc"
                ],
                "exclude": []
            },
            "emp_effective_date": {
                "date_selection": "Date of Hire",
                "custom_date_field": ""
            },
            "_id": "5d04c7a15269352c8083a586",
            "companyId": "5c9e11894b1d726375b23058",
            "name": "Business Unit",
            "effectiveDate": "2019-06-27T18:30:00.000Z",
            "createdAt": "2019-06-15T10:25:37.783Z",
            "updatedAt": "2019-06-15T10:25:37.783Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/createPositionSchedule CreatePositionSchedule
 * @apiName CreatePositionSchedule
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
    "scheduleTitle": "TestOne",
    "scheduleStartDate": "2019-09-03T08:43:46.971Z",
    "type": "Standard",
    "jobTitle": "5cbdae55e1c6df4dba2ea51e",
    "daysOfWeek": [
        "Monday",
        "Tuesday",
        "Wednesday"
    ],
    "totalHrsPerDay": 8,
    "totalHrsPerWeek": 24,
    "lunchBreakTime": true,
    "weekApplicable": [
        {
            "day": "Monday",
            "shiftStartTime": "9:30 AM",
            "lunchStartTime": "1:30 PM",
            "lunchEndTime": "2:30 PM",
            "shiftEndTime": "6:30 PM"
        },
        {
            "day": "Monday",
            "shiftStartTime": "9:30 AM",
            "lunchStartTime": "1:30 PM",
            "lunchEndTime": "2:30 PM",
            "shiftEndTime": "6:30 PM"
        },
        {
            "day": "Monday",
            "shiftStartTime": "9:30 AM",
            "lunchStartTime": "1:30 PM",
            "lunchEndTime": "2:30 PM",
            "shiftEndTime": "6:30 PM"
        }
    ]
}
* @apiParamExample {json} Request-Example2:
{
    "scheduleTitle": "TestOne",
    "scheduleStartDate": "2019-09-03T08:43:46.971Z",
    "type": "Standard",
    "jobTitle": "5cbdae55e1c6df4dba2ea51e",
    "daysOfWeek": [
        "Monday",
        "Tuesday",
        "Wednesday"
    ],
    "totalHrsPerDay": 8,
    "totalHrsPerWeek": 24,
    "lunchBreakTime": false,
    "weekApplicable": [
        {
            "day": "Monday",
            "shiftStartTime": "9:30 AM",
            "shiftEndTime": "6:30 PM"
        },
        {
            "day": "Monday",
            "shiftStartTime": "9:30 AM",
            "shiftEndTime": "6:30 PM"
        },
        {
            "day": "Monday",
            "shiftStartTime": "9:30 AM",
            "shiftEndTime": "6:30 PM"
        }
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully created position schedule",
    "data": {
        "daysOfWeek": [
            "Monday",
            "Tuesday",
            "Wednesday"
        ],
        "_id": "5d6e2dc037a25e248c0c287a",
        "scheduleTitle": "TestOne",
        "scheduleStartDate": "2019-09-03T08:43:46.971Z",
        "type": "Standard",
        "jobTitle": "5cbdae55e1c6df4dba2ea51e",
        "totalHrsPerDay": 8,
        "totalHrsPerWeek": 24,
        "lunchBreakTime": true,
        "weekApplicable": [
            {
                "_id": "5d6e2dc037a25e248c0c287d",
                "day": "Monday",
                "shiftStartTime": {
                    "hours": 9,
                    "minutes": 30
                },
                "lunchStartTime": {
                    "hours": 13,
                    "minutes": 30
                },
                "lunchEndTime": {
                    "hours": 14,
                    "minutes": 30
                },
                "shiftEndTime": {
                    "hours": 18,
                    "minutes": 30
                }
            },
            {
                "_id": "5d6e2dc037a25e248c0c287c",
                "day": "Monday",
                "shiftStartTime": {
                    "hours": 9,
                    "minutes": 30
                },
                "lunchStartTime": {
                    "hours": 13,
                    "minutes": 30
                },
                "lunchEndTime": {
                    "hours": 14,
                    "minutes": 30
                },
                "shiftEndTime": {
                    "hours": 18,
                    "minutes": 30
                }
            },
            {
                "_id": "5d6e2dc037a25e248c0c287b",
                "day": "Monday",
                "shiftStartTime": {
                    "hours": 9,
                    "minutes": 30
                },
                "lunchStartTime": {
                    "hours": 13,
                    "minutes": 30
                },
                "lunchEndTime": {
                    "hours": 14,
                    "minutes": 30
                },
                "shiftEndTime": {
                    "hours": 18,
                    "minutes": 30
                }
            }
        ],
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-09-03T09:09:20.772Z",
        "updatedAt": "2019-09-03T09:09:20.772Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/deleteposition Delete Position
 * @apiName Delete Position
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"_id" : ["5d0662b3326a8715bc8daea8"]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully deleted the data.",
    "result": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /scheduler/listPositionSchedules? ListPositionSchedules
 * @apiName ListPositionSchedules
 * @apiGroup Scheduler
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * @apiParam {ObjectId} jobTitle 5cbdae55e1c6df4dba2ea51e - query param
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": false,
    "message": "Successfully fetched position work schedules",
    "data": [
        {
            "_id": "5d6e37cd8f0b521227c04da7",
            "daysOfWeek": [
                "Monday"
            ],
            "scheduleTitle": "Test1",
            "scheduleStartDate": "2019-09-19T18:30:00.000Z",
            "type": "Standard",
            "jobTitle": "6a6f625469746c652e5f6964",
            "weekApplicable": [
                {
                    "_id": "5d6e37cd8f0b521227c04da8",
                    "day": "Monday",
                    "shiftStartTime": {
                        "hours": 9,
                        "minutes": 0
                    },
                    "lunchStartTime": {
                        "hours": 13,
                        "minutes": 0
                    },
                    "lunchEndTime": {
                        "hours": 14,
                        "minutes": 0
                    },
                    "shiftEndTime": {
                        "hours": 18,
                        "minutes": 0
                    }
                }
            ],
            "totalHrsPerDay": 9,
            "totalHrsPerWeek": 9,
            "lunchBreakTime": true,
            "companyId": "5c9e11894b1d726375b23058",
            "createdAt": "2019-09-03T09:52:13.269Z",
            "updatedAt": "2019-09-03T09:52:13.269Z",
            "employeeIds": []
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {get} /scheduler/getPositionSchedule/:_id GetPositionSchedule
 * @apiName GetPositionSchedule
 * @apiGroup Scheduler
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched position work schedule",
    "data": {
        "_id": "5d6e37cd8f0b521227c04da7",
        "daysOfWeek": [
            "Monday"
        ],
        "scheduleTitle": "Test1",
        "scheduleStartDate": "2019-09-19T18:30:00.000Z",
        "type": "Standard",
        "jobTitle": "6a6f625469746c652e5f6964",
        "weekApplicable": [
            {
                "_id": "5d6e37cd8f0b521227c04da8",
                "day": "Monday",
                "shiftStartTime": {
                    "hours": 9,
                    "minutes": 0
                },
                "lunchStartTime": {
                    "hours": 13,
                    "minutes": 0
                },
                "lunchEndTime": {
                    "hours": 14,
                    "minutes": 0
                },
                "shiftEndTime": {
                    "hours": 18,
                    "minutes": 0
                }
            }
        ],
        "totalHrsPerDay": 9,
        "totalHrsPerWeek": 9,
        "lunchBreakTime": true,
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-09-03T09:52:13.269Z",
        "updatedAt": "2019-09-03T09:52:13.269Z",
        "employeeIds": []
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {put} /scheduler/updatePositionSchedule UpdatePositionSchedule
 * @apiName UpdatePositionSchedule
 * @apiGroup Scheduler
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "_id": "5d6e37cd8f0b521227c04da7",
    "daysOfWeek": [
        "Monday",
        "Tuesday"
    ],
    "scheduleTitle": "Test1",
    "scheduleStartDate": "2019-09-19T18:30:00.000Z",
    "type": "Standard",
    "jobTitle": "6a6f625469746c652e5f6964",
    "weekApplicable": [
        {
            "_id": "5d6e37cd8f0b521227c04da8",
            "day": "Monday",
            "shiftStartTime": "9:00 AM",
            "lunchStartTime": "1:00 PM",
            "lunchEndTime": "2:00 PM",
            "shiftEndTime": "6:00 PM"
        },
        {
            "day": "Tuesday",
            "shiftStartTime": "12:00 AM",
            "lunchStartTime": "12:00 PM",
            "lunchEndTime": "2:00 PM",
            "shiftEndTime": "6:00 PM"
        }
    ],
    "totalHrsPerDay": 9,
    "totalHrsPerWeek": 9,
    "lunchBreakTime": true,
    "companyId": "5c9e11894b1d726375b23058",
    "createdAt": "2019-09-03T09:52:13.269Z",
    "updatedAt": "2019-09-03T09:52:13.269Z",
    "employeeIds": []
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully updated the data",
    "data": {
        "daysOfWeek": [
            "Monday",
            "Tuesday"
        ],
        "_id": "5d6e37cd8f0b521227c04da7",
        "scheduleTitle": "Test1",
        "scheduleStartDate": "2019-09-19T18:30:00.000Z",
        "type": "Standard",
        "jobTitle": "6a6f625469746c652e5f6964",
        "weekApplicable": [
            {
                "shiftStartTime": {
                    "hours": 9,
                    "minutes": 0
                },
                "lunchStartTime": {
                    "hours": 13,
                    "minutes": 0
                },
                "lunchEndTime": {
                    "hours": 14,
                    "minutes": 0
                },
                "shiftEndTime": {
                    "hours": 18,
                    "minutes": 0
                },
                "_id": "5d6e37cd8f0b521227c04da8",
                "day": "Monday"
            },
            {
                "shiftStartTime": {
                    "hours": 0,
                    "minutes": 0
                },
                "lunchStartTime": {
                    "hours": 12,
                    "minutes": 0
                },
                "lunchEndTime": {
                    "hours": 14,
                    "minutes": 0
                },
                "shiftEndTime": {
                    "hours": 18,
                    "minutes": 0
                },
                "_id": "5d6e63a13e49c642d8317ad5",
                "day": "Tuesday"
            }
        ],
        "totalHrsPerDay": 9,
        "totalHrsPerWeek": 9,
        "lunchBreakTime": true,
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-09-03T09:52:13.269Z",
        "updatedAt": "2019-09-03T12:59:13.324Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/createDepartmentSchedule CreateDepartmentSchedule
 * @apiName CreateDepartmentSchedule
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
    "departmentScheduleName": "TestOne",
    "openTime": "9:30 am",
    "closeTime": "6:30 pm",
    "intervalSpan": 60,
    "resourceType": "Dynamic",
    "daysOfWeekApplicable": [
        "Monday",
        "Tuesday"
    ],
    "startDate": "2019-09-04T13:03:13.117Z",
    "endDate": "2019-10-04T13:03:13.117Z",
    "intervalResourcesInformation": [
        {
            "interval": 1,
            "scheduledTime": "9:30 am",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 2,
            "scheduledTime": "10:30 am",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 3,
            "scheduledTime": "11:30 am",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 4,
            "scheduledTime": "12:30 pm",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 5,
            "scheduledTime": "1:30 pm",
            "resourcesNeeded": 0,
            "resourcesAvailable": 0
        },
        {
            "interval": 6,
            "scheduledTime": "2:30 pm",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 7,
            "scheduledTime": "3:30 pm",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 8,
            "scheduledTime": "4:30 pm",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 9,
            "scheduledTime": "5:30 pm",
            "resourcesNeeded": 5,
            
        }
    ],
    "departmentEmployeesScheduleInfo": [
        {
            "userId": "5d6f80580b16171924f1a0b9",
            "day": "Monday",
            "intervalsInfo": [
                {
                    "interval": 1,
                    "scheduledTime": "9:30 am",
                    "status": "Working"
                },
                {
                    "interval": 2,
                    "scheduledTime": "10:30 am",
                    "status": "Working"
                },
                {
                    "interval": 3,
                    "scheduledTime": "11:30 am",
                    "status": "Working"
                },
                {
                    "interval": 4,
                    "scheduledTime": "12:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 5,
                    "scheduledTime": "1:30 pm",
                    "status": "Lunch"
                },
                {
                    "interval": 6,
                    "scheduledTime": "2:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 7,
                    "scheduledTime": "3:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 8,
                    "scheduledTime": "4:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 9,
                    "scheduledTime": "5:30 pm",
                    "status": "Working"
                }
            ]
        },
        {
            "userId": "5d6f80580b16171924f1a0b9",
            "day": "Tuesday",
            "intervalsInfo": [
                {
                    "interval": 1,
                    "scheduledTime": "9:30 am",
                    "status": "Working"
                },
                {
                    "interval": 2,
                    "scheduledTime": "10:30 am",
                    "status": "Working"
                },
                {
                    "interval": 3,
                    "scheduledTime": "11:30 am",
                    "status": "Working"
                },
                {
                    "interval": 4,
                    "scheduledTime": "12:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 5,
                    "scheduledTime": "1:30 pm",
                    "status": "Lunch"
                },
                {
                    "interval": 6,
                    "scheduledTime": "2:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 7,
                    "scheduledTime": "3:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 8,
                    "scheduledTime": "4:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 9,
                    "scheduledTime": "5:30 pm",
                    "status": "Working"
                }
            ]
        },
        {
            "userId": "5d6f8a8781042129e8c270ee",
            "day": "Monday",
            "intervalsInfo": [
                {
                    "interval": 1,
                    "scheduledTime": "9:30 am",
                    "status": "Working"
                },
                {
                    "interval": 2,
                    "scheduledTime": "10:30 am",
                    "status": "Working"
                },
                {
                    "interval": 3,
                    "scheduledTime": "11:30 am",
                    "status": "Working"
                },
                {
                    "interval": 4,
                    "scheduledTime": "12:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 5,
                    "scheduledTime": "1:30 pm",
                    "status": "Lunch"
                },
                {
                    "interval": 6,
                    "scheduledTime": "2:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 7,
                    "scheduledTime": "3:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 8,
                    "scheduledTime": "4:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 9,
                    "scheduledTime": "5:30 pm",
                    "status": "Working"
                }
            ]
        },
        {
            "userId": "5d6f8a8781042129e8c270ee",
            "day": "Tuesday",
            "intervalsInfo": [
                {
                    "interval": 1,
                    "scheduledTime": "9:30 am",
                    "status": "Working"
                },
                {
                    "interval": 2,
                    "scheduledTime": "10:30 am",
                    "status": "Working"
                },
                {
                    "interval": 3,
                    "scheduledTime": "11:30 am",
                    "status": "Working"
                },
                {
                    "interval": 4,
                    "scheduledTime": "12:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 5,
                    "scheduledTime": "1:30 pm",
                    "status": "Lunch"
                },
                {
                    "interval": 6,
                    "scheduledTime": "2:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 7,
                    "scheduledTime": "3:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 8,
                    "scheduledTime": "4:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 9,
                    "scheduledTime": "5:30 pm",
                    "status": "Working"
                }
            ]
        }
    ]
}
 * @apiParamExample {json} Request-Example2:
 *
{
    "departmentScheduleName": "TestOne",
    "openTime": "9:30 am",
    "closeTime": "6:30 pm",
    "intervalSpan": 60,
    "resourceType": "Standard",
    "standardNoOfResourcesPerInterval": "5",
    "daysOfWeekApplicable": [
        "Monday",
        "Tuesday"
    ],
    "startDate": "2019-09-04T13:03:13.117Z",
    "endDate": "2019-10-04T13:03:13.117Z",
    "intervalResourcesInformation": [
        {
            "interval": 1,
            "scheduledTime": "9:30 am",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 2,
            "scheduledTime": "10:30 am",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 3,
            "scheduledTime": "11:30 am",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 4,
            "scheduledTime": "12:30 pm",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 5,
            "scheduledTime": "1:30 pm",
            "resourcesNeeded": 0,
            "resourcesAvailable": 0
        },
        {
            "interval": 6,
            "scheduledTime": "2:30 pm",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 7,
            "scheduledTime": "3:30 pm",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 8,
            "scheduledTime": "4:30 pm",
            "resourcesNeeded": 5,
            
        },
        {
            "interval": 9,
            "scheduledTime": "5:30 pm",
            "resourcesNeeded": 5,
            
        }
    ],
    "departmentEmployeesScheduleInfo": [
        {
            "userId": "5d6f80580b16171924f1a0b9",
            "day": "Monday",
            "intervalsInfo": [
                {
                    "interval": 1,
                    "scheduledTime": "9:30 am",
                    "status": "Working"
                },
                {
                    "interval": 2,
                    "scheduledTime": "10:30 am",
                    "status": "Working"
                },
                {
                    "interval": 3,
                    "scheduledTime": "11:30 am",
                    "status": "Working"
                },
                {
                    "interval": 4,
                    "scheduledTime": "12:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 5,
                    "scheduledTime": "1:30 pm",
                    "status": "Lunch"
                },
                {
                    "interval": 6,
                    "scheduledTime": "2:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 7,
                    "scheduledTime": "3:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 8,
                    "scheduledTime": "4:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 9,
                    "scheduledTime": "5:30 pm",
                    "status": "Working"
                }
            ]
        },
        {
            "userId": "5d6f80580b16171924f1a0b9",
            "day": "Tuesday",
            "intervalsInfo": [
                {
                    "interval": 1,
                    "scheduledTime": "9:30 am",
                    "status": "Working"
                },
                {
                    "interval": 2,
                    "scheduledTime": "10:30 am",
                    "status": "Working"
                },
                {
                    "interval": 3,
                    "scheduledTime": "11:30 am",
                    "status": "Working"
                },
                {
                    "interval": 4,
                    "scheduledTime": "12:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 5,
                    "scheduledTime": "1:30 pm",
                    "status": "Lunch"
                },
                {
                    "interval": 6,
                    "scheduledTime": "2:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 7,
                    "scheduledTime": "3:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 8,
                    "scheduledTime": "4:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 9,
                    "scheduledTime": "5:30 pm",
                    "status": "Working"
                }
            ]
        },
        {
            "userId": "5d6f8a8781042129e8c270ee",
            "day": "Monday",
            "intervalsInfo": [
                {
                    "interval": 1,
                    "scheduledTime": "9:30 am",
                    "status": "Working"
                },
                {
                    "interval": 2,
                    "scheduledTime": "10:30 am",
                    "status": "Working"
                },
                {
                    "interval": 3,
                    "scheduledTime": "11:30 am",
                    "status": "Working"
                },
                {
                    "interval": 4,
                    "scheduledTime": "12:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 5,
                    "scheduledTime": "1:30 pm",
                    "status": "Lunch"
                },
                {
                    "interval": 6,
                    "scheduledTime": "2:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 7,
                    "scheduledTime": "3:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 8,
                    "scheduledTime": "4:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 9,
                    "scheduledTime": "5:30 pm",
                    "status": "Working"
                }
            ]
        },
        {
            "userId": "5d6f8a8781042129e8c270ee",
            "day": "Tuesday",
            "intervalsInfo": [
                {
                    "interval": 1,
                    "scheduledTime": "9:30 am",
                    "status": "Working"
                },
                {
                    "interval": 2,
                    "scheduledTime": "10:30 am",
                    "status": "Working"
                },
                {
                    "interval": 3,
                    "scheduledTime": "11:30 am",
                    "status": "Working"
                },
                {
                    "interval": 4,
                    "scheduledTime": "12:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 5,
                    "scheduledTime": "1:30 pm",
                    "status": "Lunch"
                },
                {
                    "interval": 6,
                    "scheduledTime": "2:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 7,
                    "scheduledTime": "3:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 8,
                    "scheduledTime": "4:30 pm",
                    "status": "Working"
                },
                {
                    "interval": 9,
                    "scheduledTime": "5:30 pm",
                    "status": "Working"
                }
            ]
        }
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully created department work schedule",
    "data": {
        "departmentSchedule": {
            "daysOfWeekApplicable": [
                "Monday",
                "Tuesday"
            ],
            "_id": "5d724cedf119be3f90f602e1",
            "departmentScheduleName": "TestOne",
            "openTime": {
                "hours": 9,
                "minutes": 30
            },
            "closeTime": {
                "hours": 18,
                "minutes": 30
            },
            "intervalSpan": 60,
            "resourceType": "Dynamic",
            "startDate": "2019-09-04T13:03:13.117Z",
            "endDate": "2019-10-04T13:03:13.117Z",
            "intervalResourcesInformation": [
                {
                    "_id": "5d724cedf119be3f90f602ea",
                    "interval": 1,
                    "scheduledTime": {
                        "hours": 9,
                        "minutes": 30
                    },
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e9",
                    "interval": 2,
                    "scheduledTime": {
                        "hours": 10,
                        "minutes": 30
                    },
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e8",
                    "interval": 3,
                    "scheduledTime": {
                        "hours": 11,
                        "minutes": 30
                    },
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e7",
                    "interval": 4,
                    "scheduledTime": {
                        "hours": 12,
                        "minutes": 30
                    },
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e6",
                    "interval": 5,
                    "scheduledTime": {
                        "hours": 13,
                        "minutes": 30
                    },
                    "resourcesNeeded": 0,
                    "resourcesAvailable": 0
                },
                {
                    "_id": "5d724cedf119be3f90f602e5",
                    "interval": 6,
                    "scheduledTime": {
                        "hours": 14,
                        "minutes": 30
                    },
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e4",
                    "interval": 7,
                    "scheduledTime": {
                        "hours": 15,
                        "minutes": 30
                    },
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e3",
                    "interval": 8,
                    "scheduledTime": {
                        "hours": 16,
                        "minutes": 30
                    },
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e2",
                    "interval": 9,
                    "scheduledTime": {
                        "hours": 17,
                        "minutes": 30
                    },
                    "resourcesNeeded": 5,
                    
                }
            ],
            "companyId": "5c9e11894b1d726375b23058",
            "createdAt": "2019-09-06T12:11:25.552Z",
            "updatedAt": "2019-09-06T12:11:25.552Z"
        },
        "departmentEmployeesScheduleInfo": [
            {
                "_id": "5d724ceef119be3f90f602eb",
                "userId": "5d6f80580b16171924f1a0b9",
                "day": "Monday",
                "intervalsInfo": [
                    {
                        "_id": "5d724ceef119be3f90f602f4",
                        "interval": 1,
                        "scheduledTime": {
                            "hours": 9,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f3",
                        "interval": 2,
                        "scheduledTime": {
                            "hours": 10,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f2",
                        "interval": 3,
                        "scheduledTime": {
                            "hours": 11,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f1",
                        "interval": 4,
                        "scheduledTime": {
                            "hours": 12,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f0",
                        "interval": 5,
                        "scheduledTime": {
                            "hours": 13,
                            "minutes": 30
                        },
                        "status": "Lunch"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602ef",
                        "interval": 6,
                        "scheduledTime": {
                            "hours": 14,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602ee",
                        "interval": 7,
                        "scheduledTime": {
                            "hours": 15,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602ed",
                        "interval": 8,
                        "scheduledTime": {
                            "hours": 16,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602ec",
                        "interval": 9,
                        "scheduledTime": {
                            "hours": 17,
                            "minutes": 30
                        },
                        "status": "Working"
                    }
                ],
                "companyId": "5c9e11894b1d726375b23058",
                "departmentSchedule": "5d724cedf119be3f90f602e1",
                "createdAt": "2019-09-06T12:11:26.064Z",
                "updatedAt": "2019-09-06T12:11:26.064Z"
            },
            {
                "_id": "5d724ceef119be3f90f602f5",
                "userId": "5d6f80580b16171924f1a0b9",
                "day": "Tuesday",
                "intervalsInfo": [
                    {
                        "_id": "5d724ceef119be3f90f602fe",
                        "interval": 1,
                        "scheduledTime": {
                            "hours": 9,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602fd",
                        "interval": 2,
                        "scheduledTime": {
                            "hours": 10,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602fc",
                        "interval": 3,
                        "scheduledTime": {
                            "hours": 11,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602fb",
                        "interval": 4,
                        "scheduledTime": {
                            "hours": 12,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602fa",
                        "interval": 5,
                        "scheduledTime": {
                            "hours": 13,
                            "minutes": 30
                        },
                        "status": "Lunch"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f9",
                        "interval": 6,
                        "scheduledTime": {
                            "hours": 14,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f8",
                        "interval": 7,
                        "scheduledTime": {
                            "hours": 15,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f7",
                        "interval": 8,
                        "scheduledTime": {
                            "hours": 16,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f6",
                        "interval": 9,
                        "scheduledTime": {
                            "hours": 17,
                            "minutes": 30
                        },
                        "status": "Working"
                    }
                ],
                "companyId": "5c9e11894b1d726375b23058",
                "departmentSchedule": "5d724cedf119be3f90f602e1",
                "createdAt": "2019-09-06T12:11:26.066Z",
                "updatedAt": "2019-09-06T12:11:26.066Z"
            },
            {
                "_id": "5d724ceef119be3f90f602ff",
                "userId": "5d6f8a8781042129e8c270ee",
                "day": "Monday",
                "intervalsInfo": [
                    {
                        "_id": "5d724ceef119be3f90f60308",
                        "interval": 1,
                        "scheduledTime": {
                            "hours": 9,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60307",
                        "interval": 2,
                        "scheduledTime": {
                            "hours": 10,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60306",
                        "interval": 3,
                        "scheduledTime": {
                            "hours": 11,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60305",
                        "interval": 4,
                        "scheduledTime": {
                            "hours": 12,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60304",
                        "interval": 5,
                        "scheduledTime": {
                            "hours": 13,
                            "minutes": 30
                        },
                        "status": "Lunch"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60303",
                        "interval": 6,
                        "scheduledTime": {
                            "hours": 14,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60302",
                        "interval": 7,
                        "scheduledTime": {
                            "hours": 15,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60301",
                        "interval": 8,
                        "scheduledTime": {
                            "hours": 16,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60300",
                        "interval": 9,
                        "scheduledTime": {
                            "hours": 17,
                            "minutes": 30
                        },
                        "status": "Working"
                    }
                ],
                "companyId": "5c9e11894b1d726375b23058",
                "departmentSchedule": "5d724cedf119be3f90f602e1",
                "createdAt": "2019-09-06T12:11:26.068Z",
                "updatedAt": "2019-09-06T12:11:26.068Z"
            },
            {
                "_id": "5d724ceef119be3f90f60309",
                "userId": "5d6f8a8781042129e8c270ee",
                "day": "Tuesday",
                "intervalsInfo": [
                    {
                        "_id": "5d724ceef119be3f90f60312",
                        "interval": 1,
                        "scheduledTime": {
                            "hours": 9,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60311",
                        "interval": 2,
                        "scheduledTime": {
                            "hours": 10,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60310",
                        "interval": 3,
                        "scheduledTime": {
                            "hours": 11,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f6030f",
                        "interval": 4,
                        "scheduledTime": {
                            "hours": 12,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f6030e",
                        "interval": 5,
                        "scheduledTime": {
                            "hours": 13,
                            "minutes": 30
                        },
                        "status": "Lunch"
                    },
                    {
                        "_id": "5d724ceef119be3f90f6030d",
                        "interval": 6,
                        "scheduledTime": {
                            "hours": 14,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f6030c",
                        "interval": 7,
                        "scheduledTime": {
                            "hours": 15,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f6030b",
                        "interval": 8,
                        "scheduledTime": {
                            "hours": 16,
                            "minutes": 30
                        },
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f6030a",
                        "interval": 9,
                        "scheduledTime": {
                            "hours": 17,
                            "minutes": 30
                        },
                        "status": "Working"
                    }
                ],
                "companyId": "5c9e11894b1d726375b23058",
                "departmentSchedule": "5d724cedf119be3f90f602e1",
                "createdAt": "2019-09-06T12:11:26.069Z",
                "updatedAt": "2019-09-06T12:11:26.069Z"
            }
        ]
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {get} /scheduler/listDepartmentSchedules ListDepartmentSchedules
 * @apiName ListDepartmentSchedules
 * @apiGroup Scheduler
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
   
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched department schedules",
    "result": [
        {
            "_id": "5d6fbeeb6b87f92db4739e1c",
            "daysOfWeekApplicable": [
                "Monday",
                "Tuesday"
            ],
            "departmentScheduleName": "TestOne",
            "openTime": {
                "hours": 9,
                "minutes": 30
            },
            "closeTime": {
                "hours": 18,
                "minutes": 30
            },
            "intervalSpan": 60,
            "resourceType": "Dynamic",
            "startDate": "2019-09-04T13:03:13.117Z",
            "endDate": "2019-10-04T13:03:13.117Z",
            "companyId": "5c9e11894b1d726375b23058",
            "createdAt": "2019-09-04T13:40:59.678Z",
            "updatedAt": "2019-09-04T13:40:59.678Z",
            "totalHrsPerDay": 9,
            "assignedEmployeesCount": 2
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {get} /scheduler/getDepartmentSchedule/:_id GetDepartmentSchedule
 * @apiName GetDepartmentSchedule
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * @apiParam {ObjectId} _id Department schedule unique _id
   
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched department work schedule",
    "data": {
        "departmentSchedule": {
            "_id": "5d724cedf119be3f90f602e1",
            "daysOfWeekApplicable": [
                "Monday",
                "Tuesday"
            ],
            "departmentScheduleName": "TestOne",
            "openTime": "9:30 AM",
            "closeTime": "6:30 PM",
            "intervalSpan": 60,
            "resourceType": "Dynamic",
            "startDate": "2019-09-04T13:03:13.117Z",
            "endDate": "2019-10-04T13:03:13.117Z",
            "intervalResourcesInformation": [
                {
                    "_id": "5d724cedf119be3f90f602ea",
                    "interval": 1,
                    "scheduledTime": "9:30 AM",
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e9",
                    "interval": 2,
                    "scheduledTime": "10:30 AM",
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e8",
                    "interval": 3,
                    "scheduledTime": "11:30 AM",
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e7",
                    "interval": 4,
                    "scheduledTime": "12:30 PM",
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e6",
                    "interval": 5,
                    "scheduledTime": "1:30 PM",
                    "resourcesNeeded": 0,
                    "resourcesAvailable": 0
                },
                {
                    "_id": "5d724cedf119be3f90f602e5",
                    "interval": 6,
                    "scheduledTime": "2:30 PM",
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e4",
                    "interval": 7,
                    "scheduledTime": "3:30 PM",
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e3",
                    "interval": 8,
                    "scheduledTime": "4:30 PM",
                    "resourcesNeeded": 5,
                    
                },
                {
                    "_id": "5d724cedf119be3f90f602e2",
                    "interval": 9,
                    "scheduledTime": "5:30 PM",
                    "resourcesNeeded": 5,
                    
                }
            ],
            "companyId": "5c9e11894b1d726375b23058",
            "createdAt": "2019-09-06T12:11:25.552Z",
            "updatedAt": "2019-09-06T12:11:25.552Z"
        },
        "departmentEmployeesScheduleInfo": [
            {
                "_id": "5d724ceef119be3f90f602eb",
                "userId": {
                    "_id": "5d6f80580b16171924f1a0b9",
                    "personal": {
                        "name": {
                            "firstName": "Rekesh",
                            "middleName": "Rakee",
                            "lastName": "Golla",
                            "preferredName": "Rakesh"
                        }
                    }
                },
                "day": "Monday",
                "intervalsInfo": [
                    {
                        "_id": "5d724ceef119be3f90f602f4",
                        "interval": 1,
                        "scheduledTime": "9:30 AM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f3",
                        "interval": 2,
                        "scheduledTime": "10:30 AM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f2",
                        "interval": 3,
                        "scheduledTime": "11:30 AM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f1",
                        "interval": 4,
                        "scheduledTime": "12:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f0",
                        "interval": 5,
                        "scheduledTime": "1:30 PM",
                        "status": "Lunch"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602ef",
                        "interval": 6,
                        "scheduledTime": "2:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602ee",
                        "interval": 7,
                        "scheduledTime": "3:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602ed",
                        "interval": 8,
                        "scheduledTime": "4:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602ec",
                        "interval": 9,
                        "scheduledTime": "5:30 PM",
                        "status": "Working"
                    }
                ],
                "companyId": "5c9e11894b1d726375b23058",
                "departmentSchedule": "5d724cedf119be3f90f602e1",
                "createdAt": "2019-09-06T12:11:26.064Z",
                "updatedAt": "2019-09-06T12:11:26.064Z"
            },
            {
                "_id": "5d724ceef119be3f90f602f5",
                "userId": {
                    "_id": "5d6f80580b16171924f1a0b9",
                    "personal": {
                        "name": {
                            "firstName": "Rekesh",
                            "middleName": "Rakee",
                            "lastName": "Golla",
                            "preferredName": "Rakesh"
                        }
                    }
                },
                "day": "Tuesday",
                "intervalsInfo": [
                    {
                        "_id": "5d724ceef119be3f90f602fe",
                        "interval": 1,
                        "scheduledTime": "9:30 AM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602fd",
                        "interval": 2,
                        "scheduledTime": "10:30 AM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602fc",
                        "interval": 3,
                        "scheduledTime": "11:30 AM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602fb",
                        "interval": 4,
                        "scheduledTime": "12:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602fa",
                        "interval": 5,
                        "scheduledTime": "1:30 PM",
                        "status": "Lunch"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f9",
                        "interval": 6,
                        "scheduledTime": "2:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f8",
                        "interval": 7,
                        "scheduledTime": "3:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f7",
                        "interval": 8,
                        "scheduledTime": "4:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f602f6",
                        "interval": 9,
                        "scheduledTime": "5:30 PM",
                        "status": "Working"
                    }
                ],
                "companyId": "5c9e11894b1d726375b23058",
                "departmentSchedule": "5d724cedf119be3f90f602e1",
                "createdAt": "2019-09-06T12:11:26.066Z",
                "updatedAt": "2019-09-06T12:11:26.066Z"
            },
            {
                "_id": "5d724ceef119be3f90f602ff",
                "userId": {
                    "_id": "5d6f8a8781042129e8c270ee",
                    "personal": {
                        "name": {
                            "firstName": "Harish",
                            "middleName": "Chanda",
                            "lastName": "CH",
                            "preferredName": "Harish"
                        }
                    }
                },
                "day": "Monday",
                "intervalsInfo": [
                    {
                        "_id": "5d724ceef119be3f90f60308",
                        "interval": 1,
                        "scheduledTime": "9:30 AM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60307",
                        "interval": 2,
                        "scheduledTime": "10:30 AM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60306",
                        "interval": 3,
                        "scheduledTime": "11:30 AM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60305",
                        "interval": 4,
                        "scheduledTime": "12:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60304",
                        "interval": 5,
                        "scheduledTime": "1:30 PM",
                        "status": "Lunch"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60303",
                        "interval": 6,
                        "scheduledTime": "2:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60302",
                        "interval": 7,
                        "scheduledTime": "3:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60301",
                        "interval": 8,
                        "scheduledTime": "4:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60300",
                        "interval": 9,
                        "scheduledTime": "5:30 PM",
                        "status": "Working"
                    }
                ],
                "companyId": "5c9e11894b1d726375b23058",
                "departmentSchedule": "5d724cedf119be3f90f602e1",
                "createdAt": "2019-09-06T12:11:26.068Z",
                "updatedAt": "2019-09-06T12:11:26.068Z"
            },
            {
                "_id": "5d724ceef119be3f90f60309",
                "userId": {
                    "_id": "5d6f8a8781042129e8c270ee",
                    "personal": {
                        "name": {
                            "firstName": "Harish",
                            "middleName": "Chanda",
                            "lastName": "CH",
                            "preferredName": "Harish"
                        }
                    }
                },
                "day": "Tuesday",
                "intervalsInfo": [
                    {
                        "_id": "5d724ceef119be3f90f60312",
                        "interval": 1,
                        "scheduledTime": "9:30 AM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60311",
                        "interval": 2,
                        "scheduledTime": "10:30 AM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f60310",
                        "interval": 3,
                        "scheduledTime": "11:30 AM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f6030f",
                        "interval": 4,
                        "scheduledTime": "12:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f6030e",
                        "interval": 5,
                        "scheduledTime": "1:30 PM",
                        "status": "Lunch"
                    },
                    {
                        "_id": "5d724ceef119be3f90f6030d",
                        "interval": 6,
                        "scheduledTime": "2:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f6030c",
                        "interval": 7,
                        "scheduledTime": "3:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f6030b",
                        "interval": 8,
                        "scheduledTime": "4:30 PM",
                        "status": "Working"
                    },
                    {
                        "_id": "5d724ceef119be3f90f6030a",
                        "interval": 9,
                        "scheduledTime": "5:30 PM",
                        "status": "Working"
                    }
                ],
                "companyId": "5c9e11894b1d726375b23058",
                "departmentSchedule": "5d724cedf119be3f90f602e1",
                "createdAt": "2019-09-06T12:11:26.069Z",
                "updatedAt": "2019-09-06T12:11:26.069Z"
            }
        ],
        "assignedEmployees": [
            "5d6f80580b16171924f1a0b9",
            "5d6f8a8781042129e8c270ee"
        ]
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/deletedepartment Delete Department
 * @apiName Delete Department
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"_id" : ["5d0a02bc2800f02bacaacc19"]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully deleted the data.",
    "result": {
        "n": 0,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {get} /scheduler/loadholidays/:year Load Holidays
 * @apiName Load Holidays
 * @apiGroup Scheduler
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * @apiParam {Number} year year(2019,2020 etc)
   
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "result": [
        {
            "name": "New Year's Day",
            "date": "2018-12-31T18:30:00.000Z"
        },
        {
            "name": "Martin Luther King Day",
            "date": "2019-01-20T18:30:00.000Z"
        },
        {
            "name": "Presidents' Day",
            "date": "2019-02-17T18:30:00.000Z"
        },
        {
            "name": "Memorial Day",
            "date": "2019-04-26T18:30:00.000Z"
        },
        {
            "name": "Independence Day",
            "date": "2019-07-03T18:30:00.000Z"
        },
        {
            "name": "Labor Day",
            "date": "2019-09-01T18:30:00.000Z"
        },
        {
            "name": "Columbus Day",
            "date": "2019-10-13T18:30:00.000Z"
        },
        {
            "name": "Veterans Day",
            "date": "2019-11-10T18:30:00.000Z"
        },
        {
            "name": "Thanksgiving Day",
            "date": "2019-11-27T18:30:00.000Z"
        },
        {
            "name": "Christmas Day",
            "date": "2019-12-24T18:30:00.000Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/updatedepartment Update Department
 * @apiName Update Department
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"_id" : "5d0a07795b144e1254b1c1ea",	
	"params" : {
	    "Department_Name": "Service Department 1234",
	    "Open_Time": "09:00",
	    "Close_Time": "18:00",
	    "Interval_Definition": "30Min",
	    "Resource_Type": "Dynamic",
	    "Resource_Needed_PerInterval": "11",
	    "Days_Week_Applicable": [
	        "Mon-Fri",
	        "Sat",
	        "Sun"
	    ],
	    "StartDate": "2019-06-16T06:29:57.924Z",
	    "EndDate": "2019-06-19T06:29:57.924Z",
	    "companyId": "5c9e11894b1d726375b23058",
	    "Eligibility_criteria": {
	        "selected_field": "Business Units",
	        "sub_fields": [
	            "Business Unit 1",
	            "Business Unit 5",
	            "Business Unit 10"
	        ]
	    },
	    "specific_employee": {
	        "include": [
	            "5cda377f51f0be25b3adceb4"
	        ],
	        "exclude": []
	    },
	    "ScheduleDate": "2019-06-16T06:29:57.924Z",
	    "Scedule_Resource_Type": "Dynamic",
	    "Schedule_StartDate": "2019-06-16T06:29:57.924Z",
	    "Schedule_EndDate": "2019-06-26T06:29:57.924Z",
	    "Schedule_Builder": [
	        {
	            "Interval_Time": "10:00",
	            "Resource_Needed": 2,
	            "Resource_Available": 3,
	            "EmployeeList": [
	            	{
                    "_id" : "5d0a07795b144e1254b1c1ee",
                    "empID" : "5cda377f51f0be25b3adceb4",
                    "status" : "Lunch"
                }, 
                {
                    "_id" : "5d0a07795b144e1254b1c1ed",
                    "empID" : "5cda3ad10e57b225e1240c9d",
                    "status" : "Sick"
                }, 
                {
                    "_id" : "5d0a07795b144e1254b1c1ec",
                    "status" : "Working"
                },
	                {
	                    "empID": "5cda377f51f0be25b3adceb4",
	                    "status": "Working"
	                },
	                {
	                    "empID": "5cda3ad10e57b225e1240c9d",
	                    "status": "Working"
	                },
	                {
	                    "empId": "5cda3d4a2fec3c2610219939",
	                    "status": "Working"
	                }
	            ]
	        },
	        {
	            "Interval_Time": "10:30",
	            "Resource_Needed": 2,
	            "Resource_Available": 2,
	            "EmployeeList": [
	                {
	                    "empID": "5cda377f51f0be25b3adceb4",
	                    "status": "Working"
	                },
	                {
	                    "empID": "5cda3ad10e57b225e1240c9d",
	                    "status": "Sick"
	                },
	                {
	                    "empId": "5cda3d4a2fec3c2610219939",
	                    "status": "Working"
	                }
	            ]
	        },
	        {
	            "Interval_Time": "17:33",
	            "Resource_Needed": 3,
	            "Resource_Available": 3,
	            "EmployeeList": [
	                {
	                    "empID": "5cda377f51f0be25b3adceb4",
	                    "status": "Working"
	                },
	                {
	                    "empID": "5cda3ad10e57b225e1240c9d",
	                    "status": "Working"
	                },
	                {
	                    "empId": "5cda3d4a2fec3c2610219939",
	                    "status": "Working"
	                }
	            ]
	        }
	    ]
	}
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully updated the data"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/getempworktime Get Employee WorkTime
 * @apiName Get Employee WorkTime
 * @apiGroup Scheduler
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"employeelist" : ["5cd91d94f308c21c43e50553"],
	"page_no" : 1,
	"per_page" : 10
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched the data.",
    "result": [
        {
            "personal": {
                "name": {
                    "firstName": "raju",
                    "middleName": "king",
                    "lastName": "raju",
                    "preferredName": "raj"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "123456754",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1234567898",
                    "extention": "8765",
                    "mobilePhone": "9876543234",
                    "homePhone": "9876543212",
                    "workMail": "info@raju.com",
                    "personalMail": "raju@gmail.com"
                },
                "dob": "1997-07-15T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-05-13T18:30:00.000Z",
                "veteranStatus": "Married",
                "ssn": "1232123",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "_id": "5cd91d94f308c21c43e50553",
            "positionworkschedule_id": {
                "employeelist": [
                    "5cd91cc3f308c21c43e5054d",
                    "5cd91d94f308c21c43e50553",
                    "5cd91e68f308c21c43e50559",
                    "5cd92313f308c21c43e50599",
                    "5cda377f51f0be25b3adceb4",
                    "5cdeaa4c3c5c5b67e3533423",
                    "5cdec1d5df4fd06b83354eb8",
                    "5cdf0dc1df4fd06b83354f2a",
                    "5ce6275ee2c6ab2b5061ae88",
                    "5cf6095fecc3dd104e07cd7d",
                    "5cf60c35ecc3dd104e07cda2",
                    "5cf60d14ecc3dd104e07cdc1",
                    "5cf61c6ecd193f11e6a25a72",
                    "5cf6246d810f561230a5156c",
                    "5cf634aa810f561230a5158c",
                    "5cf63b954122c722605fc34a",
                    "5cf63da992e28543f4489cd8",
                    "5cf64d906a9bd83844c8b905",
                    "5cf653208b742314ef8ad2cd",
                    "5cf666cd617d2e1918997509",
                    "5cf8bc49f098d90ff0f10bde",
                    "5cf9059412bccb31e028e038",
                    "5cff696e386d686297b5132d",
                    "5d0082410cb9b86a2eaf2bb4",
                    "5d00a12e2b285670bf28944d",
                    "5d00a2442b285670bf28946c",
                    "5d00a3632b285670bf28948b",
                    "5d00a5602b285670bf2894aa",
                    "5d00a6442b285670bf2894b3",
                    "5d00ade42b285670bf2894d2",
                    "5d00ae122b285670bf2894f1",
                    "5d00ae2f2b285670bf289510",
                    "5d00aed22b285670bf28952f",
                    "5d00afbf2b285670bf28954e",
                    "5d00b42c2ef87a28e41d61a8",
                    "5d00c3b92b285670bf289575",
                    "5d0cd60b9f139654dab05538",
                    "5d0cd6269f139654dab05557",
                    "5d0cd7d39f139654dab0557a",
                    "5d0cd8959f139654dab05599",
                    "5d0cda339f139654dab055ba"
                ],
                "days_of_week": [
                    "Monday",
                    "Tuesday"
                ],
                "_id": "5d0ce119c213d57fd5a1e344",
                "companyId": "5c9e11894b1d726375b23058",
                "scheduletitle": "Clerk3",
                "schedulestartdate": "2019-06-26T12:33:55.000Z",
                "type": "Standard",
                "assign_hire": true,
                "assign_job_title": true,
                "manually_assign": true,
                "job_title_name": "jobTitle.name",
                "week_applicable": [
                    {
                        "_id": "5d0ce119c213d57fd5a1e346",
                        "day": "Monday",
                        "starttime": "09:00",
                        "lunch_starttime": "13:00",
                        "lunch_endtime": "14:00",
                        "end_time": "18:00"
                    },
                    {
                        "_id": "5d0ce119c213d57fd5a1e345",
                        "day": "Tuesday",
                        "starttime": "10:00",
                        "lunch_starttime": "14:00",
                        "lunch_endtime": "15:00",
                        "end_time": "19:00"
                    }
                ],
                "total_hrs_day": "08:00",
                "total_hrs_week": "40:00",
                "lunch_break_time": true,
                "__v": 0
            }
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */
/**
 * @api {post} /scheduler/getempschedule Get EmployeeSchedule
 * @apiName Get EmployeeSchedule
 * @apiGroup Position Work Schedule
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"schedulename" : "Customer Service 5",
	"scheduletype" : "Standard"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched the data.",
    "result": [
        {
            "employeelist": [],
            "days_of_week": [
                "Mon,Fri,Sat"
            ],
            "_id": "5d066995c720451fc4ec1ef1",
            "companyId": "5c9e11894b1d726375b23057",
            "scheduletitle": "Customer Service 5",
            "schedulestartdate": "2019-06-16T06:29:57.924Z",
            "type": "Standard",
            "assign_hire": true,
            "assign_job_title": true,
            "manually_assign": true,
            "job_title_name": "Software Engineer",
            "week_applicable": [
                {
                    "_id": "5d066995c720451fc4ec1ef4",
                    "day": "Monday",
                    "starttime": "08:00:00",
                    "lunch_starttime": "12:00:00",
                    "lunch_endtime": "12:30:00",
                    "end_time": "17:00:00"
                },
                {
                    "_id": "5d066995c720451fc4ec1ef3",
                    "day": "Friday",
                    "starttime": "08:00:00",
                    "lunch_starttime": "12:00:00",
                    "lunch_endtime": "12:30:00",
                    "end_time": "17:00:00"
                },
                {
                    "_id": "5d066995c720451fc4ec1ef2",
                    "day": "Saturday",
                    "starttime": "08:00:00",
                    "lunch_starttime": "12:00:00",
                    "lunch_endtime": "12:30:00",
                    "end_time": "17:00:00"
                }
            ],
            "total_hrs_day": "08:00",
            "total_hrs_week": "40:00",
            "lunch_break_time": true,
            "__v": 0
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/employeespecific Create Employee Specific
 * @apiName Create Employee Specific
 * @apiGroup Position Work Schedule
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
  "companyId": "5c9e11894b1d726375b23058",
  "scheduletitle": "Test ABC",
  "schedulestartdate": "2019-07-10T18:30:00.000Z",
  "type": "Standard",
  "total_hrs_day": "8:00",
  "total_hrs_week": "45:00",
  "week_applicable": [
    {
      "day": "Monday",
      "starttime": "08:00",
      "end_time": "17:00",
      "lunch_starttime" : "12:00",
      "lunch_endtime" : "14:00"
    },
    {
      "day": "Tuesday",
      "starttime": "10:00",
      "end_time": "18:00",
      "lunch_starttime" : "12:00",
      "lunch_endtime" : "14:00"
    }
  ],
  "days_of_week": [
    "Mon,Tue"
  ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully inserted the data.",
    "result": {
        "days_of_week": [
            "Mon,Tue"
        ],
        "_id": "5d242a8be42a7437f014406c",
        "companyId": "5c9e11894b1d726375b23058",
        "scheduletitle": "Test ABC",
        "schedulestartdate": "2019-07-10T18:30:00.000Z",
        "type": "Standard",
        "total_hrs_day": "8:00",
        "total_hrs_week": "45:00",
        "week_applicable": [
            {
                "_id": "5d242a8be42a7437f014406e",
                "day": "Monday",
                "starttime": "08:00",
                "end_time": "17:00"
            },
            {
                "_id": "5d242a8be42a7437f014406d",
                "day": "Tuesday",
                "starttime": "10:00",
                "end_time": "18:00"
            }
        ]
        "__v": 0
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {get} /scheduler/postionhistory/EmpId/PostionID Position Schedule history
 * @apiName Position Schedule history
 * @apiGroup Scheduler
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched the data...",
    "result": [
        {
            "personal": {
                "name": {
                    "firstName": "Nisar",
                    "lastName": "Mohd",
                    "middleName": "M",
                    "preferredName": "nisar"
                },
                "address": {
                    "street1": "Ayyappa Society, Chanda Naik Nag",
                    "street2": "lane 3",
                    "city": "Alaska",
                    "state": "Alaska",
                    "zipcode": "43556",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "9876543210",
                    "extention": "123",
                    "mobilePhone": "7685456789",
                    "homePhone": "0449876589",
                    "workMail": "nisarforcad@gmail.com",
                    "personalMail": "nisar@gmail.com"
                },
                "social_links": {
                    "linkedin": "https://www.linkedin.com/feed/",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1992-01-06T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-06-30T18:30:00.000Z",
                "veteranStatus": "No Military Service",
                "race": "",
                "ssn": "123345678987",
                "ethnicity": "Not Hispanic or Latino",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-07-11T13:44:54.032Z"
                },
                "Specific_Workflow_Approval": "Yes",
                "employeeId": "12345678",
                "employeeType": "Full Time",
                "hireDate": "2019-07-31T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Mumbai",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Professionals",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "32145676",
                "HR_Contact": "5cd91cc3f308c21c43e5054d",
                "ReportsTo": "5cd91e68f308c21c43e50559",
                "employment_status_history": [
                    {
                        "_id": "5d273d56b79a0a653d8f92d9",
                        "status": "Active",
                        "effective_date": "2019-07-11T13:44:54.032Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5d273d56b79a0a653d8f92da",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "ReportsTo": "5cd91e68f308c21c43e50559",
                        "effective_date": "2019-07-11T13:44:54.032Z"
                    }
                ]
            },
            "compensation": {
                "NonpaidPosition": true,
                "current_compensation": [
                    {
                        "_id": "5d273d56b79a0a653d8f92db",
                        "changeReason": "--",
                        "notes": "--"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": false,
                "assign_performance_management": true,
                "onboarding_template": "5d273d56b79a0a653d8f92df"
            },
            "status": "active",
            "groups": [],
            "signUpType": "manual",
            "leave_Eligibility_id": [],
            "_id": "5d273d56b79a0a653d8f92d8",
            "positionworkschedule_id": {
                "employeelist": [
                    "5d286ea50718b71638380648",
                    "5d273d56b79a0a653d8f92d8"
                ],
                "days_of_week": [
                    "Mon,Fri"
                ],
                "_id": "5d066995c720451fc4ec1ef1",
                "companyId": "5c9e11894b1d726375b23057",
                "scheduletitle": "Customer Service 7",
                "schedulestartdate": "2019-10-30T06:29:57.924Z",
                "type": "Standard",
                "assign_hire": true,
                "assign_job_title": true,
                "manually_assign": true,
                "job_title_name": "Software Engineer",
                "week_applicable": [
                    {
                        "_id": "5d066b12ec99530dd8fbe365",
                        "day": "Monday",
                        "starttime": "08:00:00",
                        "lunch_starttime": "12:00:00",
                        "lunch_endtime": "12:30:00",
                        "end_time": "17:00:00"
                    }
                ],
                "total_hrs_day": "08:00",
                "total_hrs_week": "40:00",
                "lunch_break_time": true,
                "__v": 0
            },
            "email": "nisarforcad@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "onboardedBy": "5c9e11894b1d726375b23057",
            "createdBy": "5c9e11894b1d726375b23057",
            "updatedBy": "5c9e11894b1d726375b23057",
            "password": "7vONlC4M",
            "leave_Eligibility_group_history": [],
            "createdAt": "2019-07-11T13:44:54.043Z",
            "updatedAt": "2019-07-19T10:39:59.721Z",
            "userId": 395,
            "position_work_schedule_history": [
                {
                    "_id": "5d319ddf56fe350c80db69d0",
                    "positionschedule_id": "5d066995c720451fc4ec1ef1",
                    "positionschedule_startdate": "2019-06-20T06:29:57.924Z",
                    "positionschedule_enddate": "2019-06-20T06:29:57.924Z",
                    "position_schedule_history": [
                        {
                            "employeelist": [
                                "5d286ea50718b71638380648",
                                "5d273d56b79a0a653d8f92d8"
                            ],
                            "days_of_week": [
                                "Mon,Fri"
                            ],
                            "_id": "5d066995c720451fc4ec1ef1",
                            "companyId": "5c9e11894b1d726375b23057",
                            "scheduletitle": "Customer Service 8",
                            "schedulestartdate": "2019-06-20T06:29:57.924Z",
                            "type": "Standard",
                            "assign_hire": true,
                            "assign_job_title": true,
                            "manually_assign": true,
                            "job_title_name": "Software Engineer",
                            "week_applicable": [
                                {
                                    "_id": "5d066b12ec99530dd8fbe365",
                                    "day": "Monday",
                                    "starttime": "08:00:00",
                                    "lunch_starttime": "12:00:00",
                                    "lunch_endtime": "12:30:00",
                                    "end_time": "17:00:00"
                                }
                            ],
                            "total_hrs_day": "08:00",
                            "total_hrs_week": "40:00",
                            "lunch_break_time": true,
                            "__v": 0
                        }
                    ]
                },
                {
                    "_id": "5d319dff56fe350c80db69d2",
                    "positionschedule_id": "5d066995c720451fc4ec1ef1",
                    "positionschedule_startdate": "2019-06-20T06:29:57.924Z",
                    "positionschedule_enddate": "2019-10-30T06:29:57.924Z",
                    "position_schedule_history": [
                        {
                            "employeelist": [
                                "5d286ea50718b71638380648",
                                "5d273d56b79a0a653d8f92d8"
                            ],
                            "days_of_week": [
                                "Mon,Fri"
                            ],
                            "_id": "5d066995c720451fc4ec1ef1",
                            "companyId": "5c9e11894b1d726375b23057",
                            "scheduletitle": "Customer Service 8",
                            "schedulestartdate": "2019-06-20T06:29:57.924Z",
                            "type": "Standard",
                            "assign_hire": true,
                            "assign_job_title": true,
                            "manually_assign": true,
                            "job_title_name": "Software Engineer",
                            "week_applicable": [
                                {
                                    "_id": "5d066b12ec99530dd8fbe365",
                                    "day": "Monday",
                                    "starttime": "08:00:00",
                                    "lunch_starttime": "12:00:00",
                                    "lunch_endtime": "12:30:00",
                                    "end_time": "17:00:00"
                                }
                            ],
                            "total_hrs_day": "08:00",
                            "total_hrs_week": "40:00",
                            "lunch_break_time": true,
                            "__v": 0
                        }
                    ]
                }
            ]
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /scheduler/editDepartmentSchedule EditDepartmentSchedule
 * @apiName EditDepartmentSchedule
 * @apiGroup Scheduler
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "_id": "5d724cedf119be3f90f602e1",
    "daysOfWeekApplicable": [
        "Monday",
        "Tuesday"
    ],
    "departmentScheduleName": "TestOne1",
    "openTime": "9:30 AM",
    "closeTime": "7:30 PM",
    "intervalSpan": 60,
    "resourceType": "Dynamic",
    "startDate": "2019-09-04T13:03:13.117Z",
    "endDate": "2019-10-04T13:03:13.117Z",
    "intervalResourcesInformation": [
        {
            "_id": "5d724cedf119be3f90f602ea",
            "interval": 1,
            "scheduledTime": "9:30 AM",
            "resourcesNeeded": 5,
            "resourcesAvailable": 2
        },
        {
            "_id": "5d724cedf119be3f90f602e9",
            "interval": 2,
            "scheduledTime": "10:30 AM",
            "resourcesNeeded": 5,
            "resourcesAvailable": 2
        },
        {
            "_id": "5d724cedf119be3f90f602e8",
            "interval": 3,
            "scheduledTime": "11:30 AM",
            "resourcesNeeded": 5,
            "resourcesAvailable": 2
        },
        {
            "_id": "5d724cedf119be3f90f602e7",
            "interval": 4,
            "scheduledTime": "12:30 PM",
            "resourcesNeeded": 5,
            "resourcesAvailable": 2
        },
        {
            "_id": "5d724cedf119be3f90f602e6",
            "interval": 5,
            "scheduledTime": "1:30 PM",
            "resourcesNeeded": 0,
            "resourcesAvailable": 0
        },
        {
            "_id": "5d724cedf119be3f90f602e5",
            "interval": 6,
            "scheduledTime": "2:30 PM",
            "resourcesNeeded": 5,
            "resourcesAvailable": 2
        },
        {
            "_id": "5d724cedf119be3f90f602e4",
            "interval": 7,
            "scheduledTime": "3:30 PM",
            "resourcesNeeded": 5,
            "resourcesAvailable": 2
        },
        {
            "_id": "5d724cedf119be3f90f602e3",
            "interval": 8,
            "scheduledTime": "4:30 PM",
            "resourcesNeeded": 5,
            "resourcesAvailable": 2
        },
        {
            "_id": "5d724cedf119be3f90f602e2",
            "interval": 9,
            "scheduledTime": "5:30 PM",
            "resourcesNeeded": 5,
            "resourcesAvailable": 2
        },
        {
            "interval": 10,
            "scheduledTime": "6:30 PM",
            "resourcesNeeded": 5,
            "resourcesAvailable": 2
        }
    ],
    "companyId": "5c9e11894b1d726375b23058",
    "createdAt": "2019-09-06T12:11:25.552Z",
    "updatedAt": "2019-09-06T12:11:25.552Z",
    "departmentEmployeesScheduleInfo": [
        {
            "_id": "5d724ceef119be3f90f602eb",
            "userId": "5d6f80580b16171924f1a0b9",
            "day": "Monday",
            "intervalsInfo": [
                {
                    "_id": "5d724ceef119be3f90f602f4",
                    "interval": 1,
                    "scheduledTime": "9:30 AM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602f3",
                    "interval": 2,
                    "scheduledTime": "10:30 AM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602f2",
                    "interval": 3,
                    "scheduledTime": "11:30 AM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602f1",
                    "interval": 4,
                    "scheduledTime": "12:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602f0",
                    "interval": 5,
                    "scheduledTime": "1:30 PM",
                    "status": "Lunch"
                },
                {
                    "_id": "5d724ceef119be3f90f602ef",
                    "interval": 6,
                    "scheduledTime": "2:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602ee",
                    "interval": 7,
                    "scheduledTime": "3:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602ed",
                    "interval": 8,
                    "scheduledTime": "4:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602ec",
                    "interval": 9,
                    "scheduledTime": "5:30 PM",
                    "status": "Working"
                },
                {
                	"interval": 10,
                	"scheduledTime": "6:30 PM",
                	"status": "Working"
                }
            ],
            "companyId": "5c9e11894b1d726375b23058",
            "departmentSchedule": "5d724cedf119be3f90f602e1",
            "createdAt": "2019-09-06T12:11:26.064Z",
            "updatedAt": "2019-09-06T12:11:26.064Z"
        },
        {
            "_id": "5d724ceef119be3f90f602f5",
            "userId": "5d6f80580b16171924f1a0b9",
            "day": "Tuesday",
            "intervalsInfo": [
                {
                    "_id": "5d724ceef119be3f90f602fe",
                    "interval": 1,
                    "scheduledTime": "9:30 AM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602fd",
                    "interval": 2,
                    "scheduledTime": "10:30 AM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602fc",
                    "interval": 3,
                    "scheduledTime": "11:30 AM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602fb",
                    "interval": 4,
                    "scheduledTime": "12:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602fa",
                    "interval": 5,
                    "scheduledTime": "1:30 PM",
                    "status": "Lunch"
                },
                {
                    "_id": "5d724ceef119be3f90f602f9",
                    "interval": 6,
                    "scheduledTime": "2:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602f8",
                    "interval": 7,
                    "scheduledTime": "3:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602f7",
                    "interval": 8,
                    "scheduledTime": "4:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f602f6",
                    "interval": 9,
                    "scheduledTime": "5:30 PM",
                    "status": "Working"
                },
                {
                	"interval": 10,
                	"scheduledTime": "6:30 PM",
                	"status": "Working"
                }
            ],
            "companyId": "5c9e11894b1d726375b23058",
            "departmentSchedule": "5d724cedf119be3f90f602e1",
            "createdAt": "2019-09-06T12:11:26.066Z",
            "updatedAt": "2019-09-06T12:11:26.066Z"
        },
        {
            "_id": "5d724ceef119be3f90f602ff",
            "userId": "5d6f8a8781042129e8c270ee",
            "day": "Monday",
            "intervalsInfo": [
                {
                    "_id": "5d724ceef119be3f90f60308",
                    "interval": 1,
                    "scheduledTime": "9:30 AM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f60307",
                    "interval": 2,
                    "scheduledTime": "10:30 AM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f60306",
                    "interval": 3,
                    "scheduledTime": "11:30 AM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f60305",
                    "interval": 4,
                    "scheduledTime": "12:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f60304",
                    "interval": 5,
                    "scheduledTime": "1:30 PM",
                    "status": "Lunch"
                },
                {
                    "_id": "5d724ceef119be3f90f60303",
                    "interval": 6,
                    "scheduledTime": "2:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f60302",
                    "interval": 7,
                    "scheduledTime": "3:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f60301",
                    "interval": 8,
                    "scheduledTime": "4:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f60300",
                    "interval": 9,
                    "scheduledTime": "5:30 PM",
                    "status": "Working"
                },
                {
                	"interval": 10,
                	"scheduledTime": "6:30 PM",
                	"status": "Working"
                }
            ],
            "companyId": "5c9e11894b1d726375b23058",
            "departmentSchedule": "5d724cedf119be3f90f602e1",
            "createdAt": "2019-09-06T12:11:26.068Z",
            "updatedAt": "2019-09-06T12:11:26.068Z"
        },
        {
            "_id": "5d724ceef119be3f90f60309",
            "userId": "5d6f8a8781042129e8c270ee",
            "day": "Tuesday",
            "intervalsInfo": [
                {
                    "_id": "5d724ceef119be3f90f60312",
                    "interval": 1,
                    "scheduledTime": "9:30 AM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f60311",
                    "interval": 2,
                    "scheduledTime": "10:30 AM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f60310",
                    "interval": 3,
                    "scheduledTime": "11:30 AM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f6030f",
                    "interval": 4,
                    "scheduledTime": "12:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f6030e",
                    "interval": 5,
                    "scheduledTime": "1:30 PM",
                    "status": "Lunch"
                },
                {
                    "_id": "5d724ceef119be3f90f6030d",
                    "interval": 6,
                    "scheduledTime": "2:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f6030c",
                    "interval": 7,
                    "scheduledTime": "3:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f6030b",
                    "interval": 8,
                    "scheduledTime": "4:30 PM",
                    "status": "Working"
                },
                {
                    "_id": "5d724ceef119be3f90f6030a",
                    "interval": 9,
                    "scheduledTime": "5:30 PM",
                    "status": "Working"
                },
                {
                	"interval": 10,
                	"scheduledTime": "6:30 PM",
                	"status": "Working"
                }
            ],
            "companyId": "5c9e11894b1d726375b23058",
            "departmentSchedule": "5d724cedf119be3f90f602e1",
            "createdAt": "2019-09-06T12:11:26.069Z",
            "updatedAt": "2019-09-06T12:11:26.069Z"
        }
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully updated department work schedule",
    "data": [
        {
            "openTime": {
                "hours": 9,
                "minutes": 30
            },
            "closeTime": {
                "hours": 19,
                "minutes": 30
            },
            "daysOfWeekApplicable": [
                "Monday",
                "Tuesday"
            ],
            "_id": "5d724cedf119be3f90f602e1",
            "departmentScheduleName": "TestOne1",
            "intervalSpan": 60,
            "resourceType": "Dynamic",
            "startDate": "2019-09-04T13:03:13.117Z",
            "endDate": "2019-10-04T13:03:13.117Z",
            "intervalResourcesInformation": [
                {
                    "scheduledTime": {
                        "hours": 9,
                        "minutes": 30
                    },
                    "_id": "5d724cedf119be3f90f602ea",
                    "interval": 1,
                    "resourcesNeeded": 5
                },
                {
                    "scheduledTime": {
                        "hours": 10,
                        "minutes": 30
                    },
                    "_id": "5d724cedf119be3f90f602e9",
                    "interval": 2,
                    "resourcesNeeded": 5
                },
                {
                    "scheduledTime": {
                        "hours": 11,
                        "minutes": 30
                    },
                    "_id": "5d724cedf119be3f90f602e8",
                    "interval": 3,
                    "resourcesNeeded": 5
                },
                {
                    "scheduledTime": {
                        "hours": 12,
                        "minutes": 30
                    },
                    "_id": "5d724cedf119be3f90f602e7",
                    "interval": 4,
                    "resourcesNeeded": 5
                },
                {
                    "scheduledTime": {
                        "hours": 13,
                        "minutes": 30
                    },
                    "_id": "5d724cedf119be3f90f602e6",
                    "interval": 5,
                    "resourcesNeeded": 0
                },
                {
                    "scheduledTime": {
                        "hours": 14,
                        "minutes": 30
                    },
                    "_id": "5d724cedf119be3f90f602e5",
                    "interval": 6,
                    "resourcesNeeded": 5
                },
                {
                    "scheduledTime": {
                        "hours": 15,
                        "minutes": 30
                    },
                    "_id": "5d724cedf119be3f90f602e4",
                    "interval": 7,
                    "resourcesNeeded": 5
                },
                {
                    "scheduledTime": {
                        "hours": 16,
                        "minutes": 30
                    },
                    "_id": "5d724cedf119be3f90f602e3",
                    "interval": 8,
                    "resourcesNeeded": 5
                },
                {
                    "scheduledTime": {
                        "hours": 17,
                        "minutes": 30
                    },
                    "_id": "5d724cedf119be3f90f602e2",
                    "interval": 9,
                    "resourcesNeeded": 5
                },
                {
                    "scheduledTime": {
                        "hours": 18,
                        "minutes": 30
                    },
                    "_id": "5d7625fff57bdf3a3806a6ae",
                    "interval": 10,
                    "resourcesNeeded": 5
                }
            ],
            "companyId": "5c9e11894b1d726375b23058",
            "createdAt": "2019-09-06T12:11:25.552Z",
            "updatedAt": "2019-09-09T10:14:23.430Z"
        },
        {
            "_id": "5d724ceef119be3f90f602eb",
            "userId": "5d6f80580b16171924f1a0b9",
            "day": "Monday",
            "intervalsInfo": [
                {
                    "scheduledTime": {
                        "hours": 9,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602f4",
                    "interval": 1,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 10,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602f3",
                    "interval": 2,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 11,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602f2",
                    "interval": 3,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 12,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602f1",
                    "interval": 4,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 13,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602f0",
                    "interval": 5,
                    "status": "Lunch"
                },
                {
                    "scheduledTime": {
                        "hours": 14,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602ef",
                    "interval": 6,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 15,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602ee",
                    "interval": 7,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 16,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602ed",
                    "interval": 8,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 17,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602ec",
                    "interval": 9,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 18,
                        "minutes": 30
                    },
                    "_id": "5d7625fff57bdf3a3806a6b8",
                    "interval": 10,
                    "status": "Working"
                }
            ],
            "companyId": "5c9e11894b1d726375b23058",
            "departmentSchedule": "5d724cedf119be3f90f602e1",
            "createdAt": "2019-09-06T12:11:26.064Z",
            "updatedAt": "2019-09-09T10:14:23.433Z"
        },
        {
            "_id": "5d724ceef119be3f90f602f5",
            "userId": "5d6f80580b16171924f1a0b9",
            "day": "Tuesday",
            "intervalsInfo": [
                {
                    "scheduledTime": {
                        "hours": 9,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602fe",
                    "interval": 1,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 10,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602fd",
                    "interval": 2,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 11,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602fc",
                    "interval": 3,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 12,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602fb",
                    "interval": 4,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 13,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602fa",
                    "interval": 5,
                    "status": "Lunch"
                },
                {
                    "scheduledTime": {
                        "hours": 14,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602f9",
                    "interval": 6,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 15,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602f8",
                    "interval": 7,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 16,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602f7",
                    "interval": 8,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 17,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f602f6",
                    "interval": 9,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 18,
                        "minutes": 30
                    },
                    "_id": "5d7625fff57bdf3a3806a6c2",
                    "interval": 10,
                    "status": "Working"
                }
            ],
            "companyId": "5c9e11894b1d726375b23058",
            "departmentSchedule": "5d724cedf119be3f90f602e1",
            "createdAt": "2019-09-06T12:11:26.066Z",
            "updatedAt": "2019-09-09T10:14:23.434Z"
        },
        {
            "_id": "5d724ceef119be3f90f602ff",
            "userId": "5d6f8a8781042129e8c270ee",
            "day": "Monday",
            "intervalsInfo": [
                {
                    "scheduledTime": {
                        "hours": 9,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f60308",
                    "interval": 1,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 10,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f60307",
                    "interval": 2,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 11,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f60306",
                    "interval": 3,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 12,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f60305",
                    "interval": 4,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 13,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f60304",
                    "interval": 5,
                    "status": "Lunch"
                },
                {
                    "scheduledTime": {
                        "hours": 14,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f60303",
                    "interval": 6,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 15,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f60302",
                    "interval": 7,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 16,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f60301",
                    "interval": 8,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 17,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f60300",
                    "interval": 9,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 18,
                        "minutes": 30
                    },
                    "_id": "5d7625fff57bdf3a3806a6cc",
                    "interval": 10,
                    "status": "Working"
                }
            ],
            "companyId": "5c9e11894b1d726375b23058",
            "departmentSchedule": "5d724cedf119be3f90f602e1",
            "createdAt": "2019-09-06T12:11:26.068Z",
            "updatedAt": "2019-09-09T10:14:23.434Z"
        },
        {
            "_id": "5d724ceef119be3f90f60309",
            "userId": "5d6f8a8781042129e8c270ee",
            "day": "Tuesday",
            "intervalsInfo": [
                {
                    "scheduledTime": {
                        "hours": 9,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f60312",
                    "interval": 1,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 10,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f60311",
                    "interval": 2,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 11,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f60310",
                    "interval": 3,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 12,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f6030f",
                    "interval": 4,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 13,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f6030e",
                    "interval": 5,
                    "status": "Lunch"
                },
                {
                    "scheduledTime": {
                        "hours": 14,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f6030d",
                    "interval": 6,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 15,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f6030c",
                    "interval": 7,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 16,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f6030b",
                    "interval": 8,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 17,
                        "minutes": 30
                    },
                    "_id": "5d724ceef119be3f90f6030a",
                    "interval": 9,
                    "status": "Working"
                },
                {
                    "scheduledTime": {
                        "hours": 18,
                        "minutes": 30
                    },
                    "_id": "5d7625fff57bdf3a3806a6d6",
                    "interval": 10,
                    "status": "Working"
                }
            ],
            "companyId": "5c9e11894b1d726375b23058",
            "departmentSchedule": "5d724cedf119be3f90f602e1",
            "createdAt": "2019-09-06T12:11:26.069Z",
            "updatedAt": "2019-09-09T10:14:23.434Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */