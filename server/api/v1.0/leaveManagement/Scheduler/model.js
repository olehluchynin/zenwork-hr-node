let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants = require('./../../../../common/constants');

let holiday = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    year: Number,
    location_id : {type: ObjectID, ref:'structures', required: true},
    holiday_name : {type: String, required: true},
    holiday_date : {type: Date, required: true}
},{
    timestamps: true,
    versionKey: false
});

holiday.index({year: 1, location_id: 1, holiday_name: 1}, {unique: true})

// let blackOutEmpGrpSchema = new mongoose.Schema({
//     name: String,
//     effectiveDate: Date,
//     companyId: {type: ObjectID, ref:'companies', required: true},
//     Eligibility_criteria: {
//         selected_field : String,
//         sub_fields : Array
//     },
//     specific_employee:{
//         include: Array,
//         exclude: Array
//     },
//     emp_effective_date : {
//         date_selection : String,
//         custom_date_field : String
//     }
// }, {
//     timestamps: true,
//     versionKey: false
// });

// Eligibility_criteria: [{
    //     selected_field : {type: String},
    //     sub_fields_codes : [{type: Number}]
    // }],

let BlackoutEmployeeGroupSchema = new mongoose.Schema({
    name: {type: String, required: true},
    effectiveDate: Date,
    companyId: {type: ObjectID, ref:'companies', required: true},
    initial_group_effective_date_details : {
        date_selection : {
            type: String,
            enum: ['date_of_hire', 'custom_date']
        },
        custom_date_field: {type: String}
    }
}, {
    timestamps: true,
    versionKey: false
});

BlackoutEmployeeGroupSchema.index({companyId: 1, name: 1}, {unique: true})

let BlackoutSchema = new mongoose.Schema({
    // country : String,
    companyId : {type:ObjectID,ref:'companies',required:true},
    location_id : {type: ObjectID, ref:'structures', required: true},
    blackoutName : {type: String, required: true},
    blackoutEmployeeGroup : {type: ObjectID, ref:'blackoutempgrp', required: true},
    startDate : {type: Date, required: true},
    endDate: {type: Date, required: true},
    blockType : {type: String, required: true, 'enum' : ['Block', 'Single Day Request Only']}
},{
    timestamps: true,
    versionKey: false
})

BlackoutSchema.index({companyId: 1, blackoutName: 1}, {unique: true})

let positionScheduleSchema = new mongoose.Schema({
    companyId : {type:ObjectID, ref:'companies', required:true},
    scheduleTitle : {type: String, required: true},
    scheduleStartDate : Date,
    type : {type: String, enum: ['Standard', 'Employee Specific']},
    jobTitle : {type: ObjectID, ref: 'structures'},
    daysOfWeek : Array,
    totalHrsPerDay : Number,
    totalHrsPerWeek : Number,
    lunchBreakTime : Boolean,
    weekApplicable : [{
        day : {type: String, enum: Object.keys(constants.dayOfWeek)},
        shiftStartTime : {hours: Number, minutes: Number},
        lunchStartTime : {hours: Number, minutes: Number},
        lunchEndTime : {hours: Number, minutes: Number},
        shiftEndTime : {hours: Number, minutes: Number},
    }]
}, {
    timestamps: true,
    versionKey: false
});

positionScheduleSchema.index({companyId: 1, scheduleTitle: 1}, {unique: true})

// let employeeSpecificSchema = new mongoose.Schema({
//     companyId : {type:ObjectID,ref:'companies',required:true},
//     scheduletitle : String,
//     schedulestartdate : Date,
//     type : String,
//     days_of_week : Array,
//     week_applicable : [{
//         day : String,
//         starttime : String,
//         end_time : String
//     }],
//     total_hrs_day : String,
//     total_hrs_week : String
// });

let departmentScheduleSchema = new mongoose.Schema({
    departmentScheduleName : {type: String, required: true },
    openTime : {hours: Number, minutes: Number},
    closeTime : {hours: Number, minutes: Number},
    intervalSpan : Number,
    resourceType : {type: String, enum: ['Dynamic', 'Standard']},
    standardNoOfResourcesPerInterval : Number,
    daysOfWeekApplicable : Array,
    startDate : Date,
    endDate : Date,
    companyId: {type: ObjectID, ref:'companies', required: true},
    intervalResourcesInformation: [{
        interval: {type: Number, required:true},
        scheduledTime: {hours: Number, minutes: Number},
        resourcesNeeded: {type: Number, required:true},
        // resourcesAvailable: {type: Number, required:true}
    }]
}, {
    timestamps: true,
    versionKey: false
})

departmentScheduleSchema.index({companyId: 1, departmentScheduleName: 1}, {unique: true})

let departmentEmployeesScheduleInfoSchema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    departmentSchedule : {type: ObjectID, ref: 'Department_Work_Schedule', required: true},
    userId: {type: ObjectID, ref: 'users', required: true},
    day: {type: String, enum: Object.keys(constants.dayOfWeek), required: true},
    interval: {type: Number, required:true},
    status: {type: String, enum: ['Not Working', 'Working', 'Lunch', 'Working/Lunch', 'Vacation', 'Sick', 'On Leave'], required: true} 
}, {
    timestamps: true,
    versionKey: false
})

let departmentEmployeesScheduleInfoSchema2 = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    departmentSchedule : {type: ObjectID, ref: 'Department_Work_Schedule', required: true},
    userId: {type: ObjectID, ref: 'users', required: true},
    day: {type: String, enum: Object.keys(constants.dayOfWeek), required: true},
    intervalsInfo: [{
        interval: {type: Number, required:true},
        scheduledTime: {hours: Number, minutes: Number},
        status: {type: String, enum: ['Not Working', 'Working', 'Lunch', 'Working/Lunch', 'Vacation', 'Sick', 'On Leave'], required: true}
    }]
}, {
    timestamps: true,
    versionKey: false
})

let BlackoutModel = mongoose.model("blackout",BlackoutSchema);
let BlackoutEmployeeGroupModel = mongoose.model("blackoutempgrp",BlackoutEmployeeGroupSchema);
let scheduler_holiday = mongoose.model("holiday",holiday);
let positionWorkSchedule = mongoose.model("Position_Work_Schedule", positionScheduleSchema);
let departmentSchedule = mongoose.model("Department_Work_Schedule",departmentScheduleSchema);
let departmentEmployeesScheduleInfoModel = mongoose.model('department_employees_schedule_info', departmentEmployeesScheduleInfoSchema, 'department_employees_schedule_info')
let departmentEmployeesScheduleInfoModel2 = mongoose.model('department_employees_schedule_info2', departmentEmployeesScheduleInfoSchema2, 'department_employees_schedule_info2')
// let employeeSchema = mongoose.model("Employee_Specific_Schedule",employeeSpecificSchema);

module.exports = {
    scheduler_holiday,
    BlackoutEmployeeGroupModel,
    BlackoutModel,
    positionWorkSchedule,
    departmentSchedule,
    // employeeSchema,
    departmentEmployeesScheduleInfoModel,
    departmentEmployeesScheduleInfoModel2
};