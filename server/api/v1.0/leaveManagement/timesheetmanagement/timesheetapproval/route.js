let express = require('express');

let router = express.Router();

let authMiddleware = require('../../../middlewares/auth');

let timesheetApprovController = require('./controller');

router.get('/getallpaygroups/:userId', authMiddleware.isUserLogin,  timesheetApprovController.getAllPayGroups);
router.post('/requesttimesheetapproval',authMiddleware.isUserLogin,  timesheetApprovController.employeeRequestForApproval);
router.post('/updatetimesheet',authMiddleware.isUserLogin,  timesheetApprovController.updateApprovalRequests);
router.post('/gettimesheetapprovalrequests',authMiddleware.isUserLogin,  timesheetApprovController.getApprovalRequests);
router.get('/gettimesheetdetails/:payCycle/:employeeId',authMiddleware.isUserLogin,  timesheetApprovController.getTimesheetDetails);
router.post('/getmissingpunches',timesheetApprovController.getMissingPunches);
router.post('/gettimesheetsummary',authMiddleware.isUserLogin,  timesheetApprovController.getTimesheetSummary);

module.exports = router;


