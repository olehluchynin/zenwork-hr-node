/**
 * @api {post} /timeSchedule/timesheetapproval/requesttimesheetapproval RequestForTimesheetApproval
 * @apiName EmployeeRequestForTimesheetApproval
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
        "userId":"5cdeaa4c3c5c5b67e3533423"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "User Request",
    "data": {
        "isApproved": false,
        "_id": "5d00cb6b3778642bb07427ce",
        "userId": "5cda3d4a2fec3c2610219939",
        "employee_fullname": "Lina Dunker",
        "manager_id": "5cda377f51f0be25b3adceb4",
        "manager_full_name": "Flor  Dunker",
        "bussiness_unit": "Unit 1",
        "department": "Tech",
        "pay_group": "Group2",
        "pay_frequency": "Weekly",
        "pay_cycle": 24,
        "pay_cycle_start": "2019-06-09T18:30:00.000Z",
        "pay_cycle_end": "2019-06-15T18:30:00.000Z",
        "companyId": "5c9e11894b1d726375b23058",
        "createdBy": "5cda3d4a2fec3c2610219939",
        "createdAt": "2019-06-12T09:52:43.722Z",
        "updatedAt": "2019-06-12T09:52:43.722Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /timeSchedule/timesheetapproval/updatetimesheet ApproveOrRejectTimesheetApprovalRequest
 * @apiName Approve or Reject request
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * @apiParam {String} approveType  Available options: admin,manager,employee
 * 
 * @apiParamExample {json} Request-Example:
 *
{
        "employeeId"    :   "5cd91cc3f308c21c43e5054d",
        "payCycle"      :   13,
        "approveType"   :   "admin",
        "isApproved"    :   true,
        "comments"      :   "test comment",
        "approvedBy"    :   "5c9e11894b1d726375b23057"
}
*
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Update timesheet",
    "data": {
        "isApproved": false,
        "_id": "5cfb668075cc171bf4faadf3",
        "userId": "5cdeaa4c3c5c5b67e3533423",
        "employee_fullname": "suresh m",
        "manager_id": "5cda377f51f0be25b3adceb4",
        "manager_full_name": "Flor  m",
        "bussiness_unit": "Unit 1",
        "department": "Tech",
        "pay_group": "Group2",
        "pay_frequency": "Weekly",
        "pay_cycle": 23,
        "pay_cycle_start": "2019-06-03T07:40:48.357Z",
        "pay_cycle_end": "2019-06-07T07:40:48.359Z",
        "companyId": "5c9e11894b1d726375b23058",
        "createdBy": "5cdeaa4c3c5c5b67e3533423",
        "createdAt": "2019-06-08T07:40:48.364Z",
        "updatedAt": "2019-06-08T07:40:48.364Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {post} /timeSchedule/timesheetapproval/getmissingpunches GetMissingPunches
 * @apiName GetMissingPunches
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "startdate": "2019-05-21",
    "enddate": "2019-05-31",
    "employeetype": {
        "type": {
            "businesunit": [
                "Unit 1"
            ],
            "location": [
                "Banglore"
            ],
            "department": [
                "Tech"
            ],
            "paygroup": [],
            "All": false
        }
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Please check the missing punches",
    "missingpunches": [
        {
            "EmployeeDetails": {
                "_id": "5cdeaa4c3c5c5b67e3533423",
                "personal": {
                    "name": {
                        "firstName": "suresh",
                        "lastName": "m",
                        "middleName": "reddy",
                        "preferredName": "suri",
                        "salutation": "Mr"
                    }
                },
                "job": {
                    "ReportsTo": "5cda377f51f0be25b3adceb4"
                },
                "details": {
                    "name": {
                        "firstName": "Flor ",
                        "lastName": "Houchins",
                        "middleName": "Chantal",
                        "preferredName": "Flor"
                    }
                }
            },
            "MissingPunchType": "OUT",
            "MissingDate": "2019-05-29"
        },
        {
            "EmployeeDetails": {
                "_id": "5cdeaa4c3c5c5b67e3533423",
                "personal": {
                    "name": {
                        "firstName": "suresh",
                        "lastName": "m",
                        "middleName": "reddy",
                        "preferredName": "suri",
                        "salutation": "Mr"
                    }
                },
                "job": {
                    "ReportsTo": "5cda377f51f0be25b3adceb4"
                },
                "details": {
                    "name": {
                        "firstName": "Flor ",
                        "lastName": "Houchins",
                        "middleName": "Chantal",
                        "preferredName": "Flor"
                    }
                }
            },
            "MissingPunchType": "IN",
            "MissingDate": "2019-05-30"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {post} /timeSchedule/timesheetapproval/gettimesheetapprovalrequests Get Timesheet approvalRequests
 * @apiName Get timesheet approval requests
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {String} employeeId current employee's unique _id
 * @apiParam {String} selectedCategory  Selected filter(Available options: AllEmployees,Approved,NotApproved,DirectReportiesOnly)
 * @apiParam {String} selectedPayGroup  selected paygroup
 * @apiParam {String} selectedPayCycle Pay Cycle entered by user
 * @apiParam {String} perPageCount  selected page count
 * @apiParam {String} currentPageNum  current page number
 *
 * @apiParamExample {json} Request-Example:
 * {
	"employeeId":"5cda377f51f0be25b3adceb4",
	"selectedCategory":"Approved",
	"selectedPayGroup":"Semi-Monthly",
	"selectedPayCycle":"24",
	"perPageCount":1,
	"currentPageNum":0
}
 * 

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Request data",
    "data": {
        "timesheetReqList": [
            {
                "isEmployeeApproved": false,
                "isManagerApproved": true,
                "isAdminApproved": false,
                "_id": "5d065967d290c308a8336f55",
                "userId": "5cd91cc3f308c21c43e5054d",
                "employee_fullname": "suresh m",
                "manager_id": "5cda377f51f0be25b3adceb4",
                "manager_full_name": "Flor  m",
                "bussiness_unit": "Unit 1",
                "department": "Tech",
                "pay_group": "Group2",
                "pay_frequency": "Weekly",
                "pay_cycle": 24,
                "pay_cycle_start": "2019-06-16T18:30:00.000Z",
                "pay_cycle_end": "2019-06-22T18:30:00.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "admin_id": "5c9e11894b1d726375b23057",
                "admin_full_name": " ",
                "createdBy": "5cdeaa4c3c5c5b67e3533423",
                "createdAt": "2019-06-16T14:59:51.162Z",
                "updatedAt": "2019-06-16T15:35:57.149Z",
                "comments": "test reject for admin",
                "employeeApprovalTimestamp": "2019-06-16T15:31:48.432Z",
                "updatedBy": "5c9e11894b1d726375b23057",
                "managerApprovalTimestamp": "2019-06-16T15:33:47.355Z",
                "adminApprovalTimestamp": "2019-06-16T15:35:57.149Z"
            }
        ],
        "totalCount": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */
 
 /**
 * @api {get} /timeSchedule/timesheetapproval/gettimesheetdetails/:payCycle/:employeeId Get timesheet details
 * @apiName Get timesheet details
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {String} payCycle    selected pay cycle
 * @apiParam {String} employeeId  _id of timesheet employee
 *

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    {
    "status": true,
    "message": "Attendence timesheetDetails",
    "data": [
        {
            "firstIn": "2019-05-29T10:25:36.166Z",
            "punchDate": "2019-05-28T18:30:00.000Z",
            "employeeName": "Lina Dunker",
            "timesheetId": "5cf77aac27b94e0b2c717319",
            "userId": "5cd91cc3f308c21c43e5054d",
            "employee_fullname": "Lina Dunker",
            "manager_id": "5cda377f51f0be25b3adceb4",
            "manager_full_name": "Flor  Dunker",
            "bussiness_unit": "Unit 1",
            "department": "Tech",
            "pay_group": "Group2",
            "pay_frequency": "Weekly",
            "pay_cycle": 25,
            "pay_cycle_start": "2019-05-27T00:17:48.888Z",
            "pay_cycle_end": "2019-06-15T08:17:48.889Z",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5cda3d4a2fec3c2610219939",
            "createdAt": "2019-06-05T08:17:48.894Z",
            "updatedAt": "2019-06-05T08:17:48.894Z",
            "dailyTotal": {
                "hours": 0,
                "minutes": 2,
                "seconds": 27
            },
            "weeklyTotalTime": {
                "hours": 0,
                "minutes": 2,
                "seconds": 27
            },
            "payCycleTotalTime": {
                "hours": 0,
                "minutes": 2,
                "seconds": 27
            }
        },
        {
            "firstIn": "2019-05-30T08:55:21.437Z",
            "punchDate": "2019-05-29T18:30:00.000Z",
            "employeeName": "Lina Dunker",
            "timesheetId": "5cf77aac27b94e0b2c717319",
            "userId": "5cd91cc3f308c21c43e5054d",
            "employee_fullname": "Lina Dunker",
            "manager_id": "5cda377f51f0be25b3adceb4",
            "manager_full_name": "Flor  Dunker",
            "bussiness_unit": "Unit 1",
            "department": "Tech",
            "pay_group": "Group2",
            "pay_frequency": "Weekly",
            "pay_cycle": 25,
            "pay_cycle_start": "2019-05-27T00:17:48.888Z",
            "pay_cycle_end": "2019-06-15T08:17:48.889Z",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5cda3d4a2fec3c2610219939",
            "createdAt": "2019-06-05T08:17:48.894Z",
            "updatedAt": "2019-06-05T08:17:48.894Z",
            "dailyTotal": {
                "hours": 2,
                "minutes": 39,
                "seconds": 50
            },
            "weeklyTotalTime": {
                "hours": 2,
                "minutes": 42,
                "seconds": 17
            },
            "payCycleTotalTime": {
                "hours": 2,
                "minutes": 42,
                "seconds": 17
            }
        },
        {
            "firstIn": "2019-06-06T16:50:48.470Z",
            "punchDate": "2019-06-05T18:30:00.000Z",
            "employeeName": "Lina Dunker",
            "timesheetId": "5cf77aac27b94e0b2c717319",
            "userId": "5cd91cc3f308c21c43e5054d",
            "employee_fullname": "Lina Dunker",
            "manager_id": "5cda377f51f0be25b3adceb4",
            "manager_full_name": "Flor  Dunker",
            "bussiness_unit": "Unit 1",
            "department": "Tech",
            "pay_group": "Group2",
            "pay_frequency": "Weekly",
            "pay_cycle": 25,
            "pay_cycle_start": "2019-05-27T00:17:48.888Z",
            "pay_cycle_end": "2019-06-15T08:17:48.889Z",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5cda3d4a2fec3c2610219939",
            "createdAt": "2019-06-05T08:17:48.894Z",
            "updatedAt": "2019-06-05T08:17:48.894Z",
            "dailyTotal": {
                "hours": 2,
                "minutes": 0,
                "seconds": 18
            },
            "weeklyTotalTime": {
                "hours": 4,
                "minutes": 42,
                "seconds": 35
            },
            "payCycleTotalTime": {
                "hours": 4,
                "minutes": 42,
                "seconds": 35
            }
        },
        {
            "firstIn": "2019-06-07T13:28:20.577Z",
            "punchDate": "2019-06-06T18:30:00.000Z",
            "employeeName": "Lina Dunker",
            "timesheetId": "5cf77aac27b94e0b2c717319",
            "userId": "5cd91cc3f308c21c43e5054d",
            "employee_fullname": "Lina Dunker",
            "manager_id": "5cda377f51f0be25b3adceb4",
            "manager_full_name": "Flor  Dunker",
            "bussiness_unit": "Unit 1",
            "department": "Tech",
            "pay_group": "Group2",
            "pay_frequency": "Weekly",
            "pay_cycle": 25,
            "pay_cycle_start": "2019-05-27T00:17:48.888Z",
            "pay_cycle_end": "2019-06-15T08:17:48.889Z",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5cda3d4a2fec3c2610219939",
            "createdAt": "2019-06-05T08:17:48.894Z",
            "updatedAt": "2019-06-05T08:17:48.894Z",
            "dailyTotal": {
                "hours": 0,
                "minutes": 7,
                "seconds": 45
            },
            "weeklyTotalTime": {
                "hours": 4,
                "minutes": 50,
                "seconds": 20
            },
            "payCycleTotalTime": {
                "hours": 4,
                "minutes": 50,
                "seconds": 20
            }
        },
        {
            "firstIn": "2019-06-08T03:51:14.179Z",
            "punchDate": "2019-06-07T18:30:00.000Z",
            "employeeName": "Lina Dunker",
            "timesheetId": "5cf77aac27b94e0b2c717319",
            "userId": "5cd91cc3f308c21c43e5054d",
            "employee_fullname": "Lina Dunker",
            "manager_id": "5cda377f51f0be25b3adceb4",
            "manager_full_name": "Flor  Dunker",
            "bussiness_unit": "Unit 1",
            "department": "Tech",
            "pay_group": "Group2",
            "pay_frequency": "Weekly",
            "pay_cycle": 25,
            "pay_cycle_start": "2019-05-27T00:17:48.888Z",
            "pay_cycle_end": "2019-06-15T08:17:48.889Z",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5cda3d4a2fec3c2610219939",
            "createdAt": "2019-06-05T08:17:48.894Z",
            "updatedAt": "2019-06-05T08:17:48.894Z",
            "dailyTotal": {
                "hours": 4,
                "minutes": 11,
                "seconds": 4
            },
            "weeklyTotalTime": {
                "hours": 9,
                "minutes": 1,
                "seconds": 24
            },
            "payCycleTotalTime": {
                "hours": 9,
                "minutes": 1,
                "seconds": 24
            }
        },
        {
            "firstIn": "2019-06-10T04:10:25.000Z",
            "punchDate": "2019-06-09T18:30:00.000Z",
            "employeeName": "Lina Dunker",
            "timesheetId": "5cf77aac27b94e0b2c717319",
            "userId": "5cd91cc3f308c21c43e5054d",
            "employee_fullname": "Lina Dunker",
            "manager_id": "5cda377f51f0be25b3adceb4",
            "manager_full_name": "Flor  Dunker",
            "bussiness_unit": "Unit 1",
            "department": "Tech",
            "pay_group": "Group2",
            "pay_frequency": "Weekly",
            "pay_cycle": 25,
            "pay_cycle_start": "2019-05-27T00:17:48.888Z",
            "pay_cycle_end": "2019-06-15T08:17:48.889Z",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5cda3d4a2fec3c2610219939",
            "createdAt": "2019-06-05T08:17:48.894Z",
            "updatedAt": "2019-06-05T08:17:48.894Z",
            "dailyTotal": {
                "hours": 11,
                "minutes": 58,
                "seconds": 0
            },
            "weeklyTotalTime": {
                "hours": 11,
                "minutes": 58,
                "seconds": 0
            },
            "payCycleTotalTime": {
                "hours": 20,
                "minutes": 59,
                "seconds": 24
            }
        },
        {
            "firstIn": "2019-06-11T03:00:25.000Z",
            "punchDate": "2019-06-10T18:30:00.000Z",
            "employeeName": "Lina Dunker",
            "timesheetId": "5cf77aac27b94e0b2c717319",
            "userId": "5cd91cc3f308c21c43e5054d",
            "employee_fullname": "Lina Dunker",
            "manager_id": "5cda377f51f0be25b3adceb4",
            "manager_full_name": "Flor  Dunker",
            "bussiness_unit": "Unit 1",
            "department": "Tech",
            "pay_group": "Group2",
            "pay_frequency": "Weekly",
            "pay_cycle": 25,
            "pay_cycle_start": "2019-05-27T00:17:48.888Z",
            "pay_cycle_end": "2019-06-15T08:17:48.889Z",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5cda3d4a2fec3c2610219939",
            "createdAt": "2019-06-05T08:17:48.894Z",
            "updatedAt": "2019-06-05T08:17:48.894Z",
            "dailyTotal": {
                "hours": 2,
                "minutes": 2,
                "seconds": 0
            },
            "weeklyTotalTime": {
                "hours": 14,
                "minutes": 0,
                "seconds": 0
            },
            "payCycleTotalTime": {
                "hours": 23,
                "minutes": 1,
                "seconds": 24
            }
        },
        {
            "firstIn": "2019-06-12T02:00:25.000Z",
            "punchDate": "2019-06-11T18:30:00.000Z",
            "employeeName": "Lina Dunker",
            "timesheetId": "5cf77aac27b94e0b2c717319",
            "userId": "5cd91cc3f308c21c43e5054d",
            "employee_fullname": "Lina Dunker",
            "manager_id": "5cda377f51f0be25b3adceb4",
            "manager_full_name": "Flor  Dunker",
            "bussiness_unit": "Unit 1",
            "department": "Tech",
            "pay_group": "Group2",
            "pay_frequency": "Weekly",
            "pay_cycle": 25,
            "pay_cycle_start": "2019-05-27T00:17:48.888Z",
            "pay_cycle_end": "2019-06-15T08:17:48.889Z",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5cda3d4a2fec3c2610219939",
            "createdAt": "2019-06-05T08:17:48.894Z",
            "updatedAt": "2019-06-05T08:17:48.894Z",
            "dailyTotal": {
                "hours": 0,
                "minutes": 54,
                "seconds": 17
            },
            "weeklyTotalTime": {
                "hours": 14,
                "minutes": 54,
                "seconds": 17
            },
            "payCycleTotalTime": {
                "hours": 23,
                "minutes": 55,
                "seconds": 41
            }
        },
        {
            "firstIn": "2019-06-13T03:00:25.000Z",
            "punchDate": "2019-06-12T18:30:00.000Z",
            "employeeName": "Lina Dunker",
            "timesheetId": "5cf77aac27b94e0b2c717319",
            "userId": "5cd91cc3f308c21c43e5054d",
            "employee_fullname": "Lina Dunker",
            "manager_id": "5cda377f51f0be25b3adceb4",
            "manager_full_name": "Flor  Dunker",
            "bussiness_unit": "Unit 1",
            "department": "Tech",
            "pay_group": "Group2",
            "pay_frequency": "Weekly",
            "pay_cycle": 25,
            "pay_cycle_start": "2019-05-27T00:17:48.888Z",
            "pay_cycle_end": "2019-06-15T08:17:48.889Z",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5cda3d4a2fec3c2610219939",
            "createdAt": "2019-06-05T08:17:48.894Z",
            "updatedAt": "2019-06-05T08:17:48.894Z",
            "dailyTotal": {
                "hours": 1,
                "minutes": 10,
                "seconds": 0
            },
            "weeklyTotalTime": {
                "hours": 16,
                "minutes": 4,
                "seconds": 17
            },
            "payCycleTotalTime": {
                "hours": 25,
                "minutes": 5,
                "seconds": 41
            }
        }
    ]
}
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /timeSchedule/timesheetapproval/gettimesheetsummary Get timesheet summary
 * @apiName Get timesheet summary
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {String} selectedCategoryType    selected category type (Available options: "AllEmployees","BusinessUnit","Location","Department","PayGroup") 
 * @apiParam {String} selectedCategory  selected category
 * @apiParam {String} payCycle selected pay cycle
 * @apiParam {String} userId _id of current employee
 * @apiParam {String} perPageCount selected page count
 * @apiParam {String} currentPageNum current page number
 *
 * * @apiParamExample {json} Request-Example: 
 * {
	"selectedCategoryType":"PayGroup",
	"selectedCategory":"Semi-Monthly",
	"payCycle":13,
	"userId":"5cda377f51f0be25b3adceb4",
	"perPageCount":10,
	"currentPageNum":0
}
*

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Timesheet summary",
    "data": {
        "attendenceList": [
            {
                "timesheetDetails": {
                    "isEmployeeApproved": false,
                    "isManagerApproved": true,
                    "isAdminApproved": true,
                    "_id": "5d199ac34263f850ea8b56d8",
                    "userId": "5cd91cc3f308c21c43e5054d",
                    "employee_fullname": "vipin reddy",
                    "manager_id": "5cda377f51f0be25b3adceb4",
                    "manager_full_name": "Flor  reddy",
                    "bussiness_unit": "Unit 1",
                    "department": "Tech",
                    "pay_group": "Group2",
                    "pay_frequency": "Semi-Monthly",
                    "pay_cycle": 13,
                    "pay_cycle_start": "2019-07-01T00:00:00.000Z",
                    "pay_cycle_end": "2019-07-07T00:00:00.000Z",
                    "companyId": "5c9e11894b1d726375b23058",
                    "admin_id": "5c9e11894b1d726375b23057",
                    "admin_full_name": " ",
                    "createdBy": "5cd91cc3f308c21c43e5054d",
                    "createdAt": "2019-07-01T05:31:47.311Z",
                    "updatedAt": "2019-07-05T10:48:22.770Z",
                    "adminApprovalTimestamp": "2019-07-05T10:48:22.770Z",
                    "comments": "test comment",
                    "updatedBy": "5c9e11894b1d726375b23057",
                    "managerApprovalTimestamp": "2019-07-05T10:48:22.770Z",
                    "employeeApprovalTimestamp": "2019-07-04T06:46:23.263Z"
                },
                "timedata": {
                    "totalDaysForDaily": 6,
                    "dailyTotal": {
                        "hours": 0,
                        "minutes": 0,
                        "seconds": 0
                    },
                    "weeklyTotal": {
                        "hours": 0,
                        "minutes": 0,
                        "seconds": 1
                    },
                    "payCyleTotal": {
                        "hours": 0,
                        "minutes": 0,
                        "seconds": 1
                    }
                }
            }
        ],
        "totalCount": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {get} /timeSchedule/timesheetapproval/getallpaygroups/:userId Get all pay groups
 * @apiName Get all pay groups
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {String} userId current employee's unique _id
 * @apiParamExample {json} Request-Example: 
 * http://localhost:3003/api/v1.0/timeSchedule/timesheetapproval/getallpaygroups/5cd91cc3f308c21c43e5054d
 *

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "All pay groups",
    "data": [
        "Semi-Monthly",
        "Weekly",
        "Bi-Weekly"
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */
