/**
 * This file contains methods for timesheet approval.
 * 
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso
 */

let mongoose = require('mongoose');
let moment = require('moment');
let ObjectID = mongoose.Types.ObjectId;

let async = require('async')

let userCollection = require('../../../users/model');
let companyCollection = require('../../../companies/model');
let companySettingsCollection = require('../../../companies/settings/model');
let timesheetRequestCollection = require('./model').TimesheetRequestModel;
let attendanceCollection = require('./../../model').AttendanceModel;

const employeeTypes = {
    "company": "company",
    "employee": "employee"
};

//Surendra(08-June-2019):Const for punch in and punch out
const punchTypeOptions = {
    "punchin": "IN",
    "punchout": "OUT"
};

/**
     * This method is used to get all pay groups available in requested user's company.
     * 
     * @param {*} req 
     * @param {*} res 
     * @author : Surendra Waso
     */
let getAllPayGroups = (req, res) => {
    let body = req.params;
    return new Promise((resolve, reject) => {
        if (!body.userId) {
            reject("userId is mandatory");
            return;
        }
        userCollection.findOne({ _id: body.userId })
            .then((employee) => {
                if (!employee) {
                    reject("Employee Not Found");
                } else {
                    userCollection.distinct("compensation.Pay_frequency", { companyId: employee.companyId })
                        .then((allPayGroups) => {
                            res.status(200).json({ status: true, message: "All pay groups", data: allPayGroups });
                        })
                }
            })
    })
        .catch((err) => {
            console.log(err);
            res.status(400).json({ status: false, message: err, data: null });
        });
}



/**
 * This method is used to add an approval request to the manager for current pay cycle timesheet
 * 
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso
 */
let employeeRequestForApproval = (req, res) => {
    let body = req.body;

    return new Promise(async (resolve, reject) => {
        console.log("Req received");
        if (!body.userId) {
            reject("userId is mandatory");
            return;
        }
        var isDuplicateRequest = await checkIfDuplicateRequest(body.userId);
        if (isDuplicateRequest) {
            reject("You have already requested for current pay cycle");
        } else {
            userCollection.findOne({ _id: body.userId }).populate('job.ReportsTo').exec(async (err, employeeDetails) => {
                console.log("Populated User " + employeeDetails);
                if (!employeeDetails) {
                    reject("Employee not found");
                }
                /**
                 * ToDo:validation part
                 */
                if (!employeeDetails.job.ReportsTo || !employeeDetails.job.ReportsTo._id) {
                    //Manager Details not available
                    reject("Manager details are not available");
                }
                if (!employeeDetails.compensation || !employeeDetails.compensation.Pay_group) {
                    //Pay Group not available
                    reject("Pay group details are not available");
                }
                var adminDetails = await userCollection.findOne({ companyId: employeeDetails.companyId, type: "company" }).exec()
                let approvalRequest = new timesheetRequestCollection({
                    userId: body.userId,
                    employee_fullname: employeeDetails.personal.name.firstName + " " + employeeDetails.personal.name.lastName,
                    manager_id: employeeDetails.job.ReportsTo._id,
                    manager_full_name: employeeDetails.job.ReportsTo.personal.name.firstName + " " + employeeDetails.personal.name.lastName,
                    bussiness_unit: employeeDetails.job.businessUnit,
                    department: employeeDetails.job.department,
                    pay_group: employeeDetails.compensation.Pay_group,
                    pay_frequency: employeeDetails.compensation.Pay_frequency,
                    pay_cycle: getCurrentPayCycle(new Date()),
                    pay_cycle_start: getCurrentPayCycleStartEnd(employeeDetails.compensation.Pay_group, "Monday").firstday,
                    pay_cycle_end: getCurrentPayCycleStartEnd(employeeDetails.compensation.Pay_group, "Monday").lastday,
                    companyId: employeeDetails.companyId,
                    admin_id: adminDetails._id,
                    admin_full_name: (adminDetails && adminDetails.personal && adminDetails.personal.name && adminDetails.personal.name.firstName) ? (adminDetails.personal.name.firstName + " " + adminDetails.personal.name.lastName) : " ",
                    createdBy: body.userId
                });

                approvalRequest.save(function (err, requestData) {
                    if (!err) {
                        res.status(200).json({ status: true, message: "User Request", data: requestData });
                    } else {
                        res.status(200).json({ status: false, message: "User Request Failed", data: err });
                    }
                });
            })
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });

}

/**
 * This method is used to get all timesheet approval requests for requested user
 * 
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso
 */
let getApprovalRequests = (req, res) => {
    let data = req.body;
    employeeId = data.employeeId;
    selectedCategory = data.selectedCategory;
    selectedPayGroup = data.selectedPayGroup;
    selectedPayCycle = data.selectedPayCycle;

    var allConditions = {};//For filters

    return new Promise((resolve, reject) => {
        if (!employeeId) {
            reject("Employee ID is mandatory");
        }

        if (!selectedCategory) {
            reject("Category is mandatory");
        }

        // if(selectedCategory && selectedCategory !== "AllEmployees" && !selectedPayGroup){
        //     reject("PayGroup is mandatory");
        // }

        userCollection.findOne({ _id: employeeId })
            .then((employee) => {
                if (!employee) {
                    reject("Employee not found");
                } else {
                    if (employee.type == employeeTypes.company) {
                        allConditions = { "companyId": employee.companyId };
                    } else {
                        allConditions = { "job.ReportsTo": employeeId };
                    }

                    if (selectedCategory == "AllEmployees") {
                        /**
                         * ToDo: Check how to take all Employee for managers, currently only direct are shown
                         */
                    }

                    if (selectedPayGroup) {
                        allConditions["compensation.Pay_frequency"] = selectedPayGroup;
                    }

                    userCollection.find(allConditions, { _id: true })
                        .then((employeeList) => {
                            if (employeeList.length == 0) {
                                reject("No one is reporting to requested employee");
                            } else {

                                allConditions = { userId: { $in: employeeList } };

                                if (selectedPayCycle) {
                                    allConditions["pay_cycle"] = selectedPayCycle;
                                }
                                /**
                                 * ToDo: Change approved conditions for admin,employee or manager, confirm from Nisar
                                 */
                                if (selectedCategory == "Approved") {
                                    allConditions.isManagerApproved = true;
                                } else if (selectedCategory == "NotApproved") {
                                    allConditions.isManagerApproved = false;
                                }
                                console.log(JSON.stringify(allConditions));
                                timesheetRequestCollection.find(allConditions).limit(Number(data.perPageCount)).skip((Number(data.perPageCount) * Number(data.currentPageNum)))
                                    .then(async (timesheetReqList) => {
                                        if (timesheetReqList.length == 0) {
                                            res.status(200).json({ status: true, message: "Request data", data: { "timesheetReqList": [], "totalCount": 0 } });
                                        } else {
                                            var totalCount = await timesheetRequestCollection.find(allConditions).countDocuments();
                                            res.status(200).json({ status: true, message: "Request data", data: { "timesheetReqList": timesheetReqList, "totalCount": totalCount } });
                                        }
                                    })


                            }
                        })
                }
            })

    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });

}

/**
  * This method is used to approve or reject the timesheet approval requests
  * 
  * @param {*} req 
  * @param {*} res 
  * @author : Surendra Waso
  */
let updateApprovalRequests = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        if (!body.employeeId) {
            reject("Employee ID is mandatory");
        }

        if (!body.approveType) {
            reject("Approve type is mandatory");
        }

        if (!body.payCycle) {
            reject("Pay cycle is mandatory");
        }


        if (!body.isApproved && body.isApproved != false) {
            reject("isApproved is mandatory");
        } else if (body.isApproved != true && body.isApproved != false) {
            console.log("Invalid value for isApproved");
            reject("Invalid value for isApproved");//"Invalid value for isApproved");
        }

        userCollection.findOne({ _id: body.employeeId })
            .then((employee) => {
                if (!employee) {
                    reject("Employee not found");
                } else {
                    console.log("employee full details " + JSON.stringify(body));
                    var allFilters = {
                        userId: body.employeeId,
                        pay_cycle: body.payCycle
                    };

                    timesheetRequestCollection.findOne(allFilters)
                        .then(async (timesheet) => {
                            console.log("timesheet full details " + JSON.stringify(timesheet));
                            if (!timesheet) {
                                reject("Timesheet not found");
                            } else {
                                if (timesheet.userId == body.employeeId) {
                                    // var adminDetails    =  await userCollection.findOne({ companyId: body.employeeDetails.companyId,type:employeeTypes.company}).exec()
                                    var updateData = {};
                                    if (body.approveType == "employee") {
                                        updateData.isEmployeeApproved = body.isApproved ? true : false;
                                        updateData.employeeApprovalTimestamp = new Date();

                                    } else if (body.approveType == "admin") {
                                        updateData.isAdminApproved = body.isApproved ? true : false;
                                        updateData.adminApprovalTimestamp = new Date();
                                        updateData.isManagerApproved = body.isApproved ? true : false;
                                        updateData.managerApprovalTimestamp = new Date();
                                    } else if (body.approveType == "manager") {
                                        updateData.isManagerApproved = body.isApproved ? true : false;
                                        updateData.managerApprovalTimestamp = new Date();
                                    } else {
                                        reject("Something went wrong with request data.")
                                    }
                                    updateData.updatedBy = body.approvedBy;
                                    updateData.comments = body.comments;

                                    timesheetRequestCollection.findByIdAndUpdate(timesheet._id, updateData)
                                        .then((updateResult) => {
                                            res.status(200).json({ status: true, message: "Update timesheet", data: updateResult });
                                        })
                                } else {
                                    res.status(200).json({ status: true, message: "Requested timesheet belongs to other employee", data: null });

                                }

                            }
                        })
                }

            })


    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    })
}

/**
  * This method is used to get all the details of timesheet on per day basis.
  * 
  * @param {*} req 
  * @param {*} res 
  * @author : Surendra Waso
  */
let getTimesheetDetails = (req, res) => {
    let data = req.params;

    return new Promise((resolve, reject) => {
        if (!data.employeeId) {
            reject("Employee ID is mandatory.");
        }

        if (!data.payCycle) {
            reject("payCycle is mandatory.")
        }
        var employeeId = data.employeeId;
        userCollection.findOne({ _id: employeeId })
            .then((employee) => {
                if (!employee) {
                    reject("Employee not found");
                } else {
                    resolve(employee);
                }
            })
            .then((employee) => {
                timesheetRequestCollection.findOne({ "pay_cycle": data.payCycle, "userId": data.employeeId })
                    .then(async (timesheetDetails) => {
                        var attendenceList = [];

                        var attendenceListForIn = await attendanceCollection.aggregate([{ $match: { userId: ObjectID(timesheetDetails.userId), punchType: punchTypeOptions.punchin, punchDate: { $gte: new Date(new Date(timesheetDetails.pay_cycle_start).setHours(0, 0, 0, 0)), $lte: new Date(new Date(timesheetDetails.pay_cycle_end).setHours(23, 59, 59, 59)) } } }, { $group: { _id: { "userId": "$userId", "punchDate": "$punchDate" }, firstIn: { $first: "$punchTime" } } }]).exec();
                        console.log("\n ****** \n" + JSON.stringify({ userId: ObjectID(timesheetDetails.userId), punchType: punchTypeOptions.punchin, punchDate: { $gte: new Date(new Date(timesheetDetails.pay_cycle_start).setHours(0, 0, 0, 0)), $lte: new Date(new Date(timesheetDetails.pay_cycle_end).setHours(23, 59, 59, 59)) } }));
                        var attendenceListForOut = await attendanceCollection.aggregate([{ $match: { userId: ObjectID(timesheetDetails.userId), punchType: punchTypeOptions.punchout, punchDate: { $gte: new Date(new Date(timesheetDetails.pay_cycle_start).setHours(0, 0, 0, 0)), $lte: new Date(new Date(timesheetDetails.pay_cycle_end).setHours(23, 59, 59, 59)) } } }, { $group: { _id: { "userId": "$userId", "punchDate": "$punchDate" }, lastOut: { $first: "$punchTime" } } }]).exec();
                        if (attendenceListForIn.length > 0 && attendenceListForOut.length > 0) {
                            // console.log("attendenceList calling await = "+JSON.stringify(attendenceList));
                            attendenceList = await mergeByDate(attendenceListForIn, attendenceListForOut, timesheetDetails);
                            attendenceList = await addWeeklyTime(attendenceList);
                            // console.log("attendenceList sending result await = "+JSON.stringify(attendenceList));
                            res.status(200).json({ status: true, message: "Attendence timesheetDetails", data: attendenceList });
                        } else {
                            res.status(200).json({ status: true, message: "Attendence ", data: attendenceList });
                        }

                        // res.status(200).json({ status: true, message: "Attendence list", data:  attendenceList});
                    })
            })
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    })
}

/**
     * This method is used to get the summary of all employees reporting with daily,weekly,paycycle hours.
     * 
     * @param {*} req 
     * @param {*} res 
     * @author : Surendra Waso
     */
let getTimesheetSummary = (req, res) => {

    var allParams = req.body;

    return new Promise((resolve, reject) => {

        if (!allParams.selectedCategoryType) {
            reject("Category type is mandatory");
        }
        if (allParams.selectedCategoryType !== "AllEmployees" && !allParams.selectedCategory) {
            reject("Selected category type is mandatory");
        }
        if (!allParams.userId) {
            reject("User Id is mandatory");
        }
        if (!allParams.payCycle) {
            reject("payCycle is mandatory");
        }
        if (!allParams.perPageCount) {
            reject("perPageCount is mandatory");
        }

        if (!allParams.currentPageNum && allParams.currentPageNum !== 0) {
            reject("currentPageNum is mandatory");
        }

        console.log("allParams - " + JSON.stringify(allParams));
        userCollection.find({ _id: ObjectID(allParams.userId) })
            .then((employee) => {
                if (employee.length <= 0) {
                    reject("Employee not found");
                } else {
                    var allFilters = {};
                    var valuesToSelect = {
                        "_id": true
                    };

                    employee = employee[0];

                    if (allParams.selectedCategoryType == "AllEmployees" && employee.type == employeeTypes.company) {
                        allFilters.companyId = ObjectID(employee.companyId);
                        allFilters.type = { $ne: 'company' };
                    } else if (allParams.selectedCategoryType == "AllEmployees" && employee.type == employeeTypes.employee) {
                        allFilters["job.ReportsTo"] = ObjectID(employee._id);
                    } else if (allParams.selectedCategoryType == "BusinessUnit") {
                        allFilters["job.businessUnit"] = allParams.selectedCategory;
                    } else if (allParams.selectedCategoryType == "Location") {
                        allFilters["job.location"] = allParams.selectedCategory;
                    } else if (allParams.selectedCategoryType == "Department") {
                        allFilters["job.department"] = allParams.selectedCategory;
                    } else if (allParams.selectedCategoryType == "PayGroup") {
                        allFilters["compensation.Pay_frequency"] = allParams.selectedCategory;
                    }

                    if (employee.type == employeeTypes.company) {
                        allFilters.companyId = ObjectID(employee.companyId);
                        allFilters.type = { $ne: 'company' };
                    } else if (employee.type == employeeTypes.employee) {
                        /**
                         * ToDo: Check how to get all employees for managers
                         */
                        allFilters["job.ReportsTo"] = ObjectID(employee._id);
                    }


                    userCollection.find(allFilters, valuesToSelect)
                        .then((employeeList) => {
                            if (employeeList.length <= 0) {
                                res.status(400).json({ status: true, message: "No employee is reporting to requested user", data: null })
                            } else {
                                var allEmployeeId = [];
                                employeeList.forEach(function (element) {
                                    var id = element._id;
                                    allEmployeeId.push(id);
                                })
                                var filter = {};
                                filter.userId = {
                                    "$in": allEmployeeId
                                };
                                filter.pay_cycle = allParams.payCycle;
                                console.log("filter - " + JSON.stringify(filter));
                                timesheetRequestCollection.find(filter).limit(Number(allParams.perPageCount)).skip((Number(allParams.perPageCount) * Number(allParams.currentPageNum)))
                                    .then(async (timesheetList) => {

                                        if (timesheetList.length <= 0) {
                                            res.status(200).json({ status: true, message: "Timesheet not available", data: null })
                                        } else {
                                            var attendenceList = [];
                                            for (var j = 0; j < timesheetList.length; j++) {

                                                var timedata = await getTimesheetDetailsForIndividual(timesheetList[j]);

                                                attendenceList.push({ "timesheetDetails": timesheetList[j], "timedata": timedata });
                                            }

                                            var totalCount = await timesheetRequestCollection.find(filter).countDocuments();

                                            // res.status(200).json({status:true,message: "Timesheet summary",data:attendenceList})
                                            res.status(200).json({ status: true, message: "Timesheet summary", data: { "attendenceList": attendenceList, "totalCount": totalCount } });
                                        }//else of timesheetList.length = 0

                                    })
                            }
                        })
                }
            })

    }).catch((err) => {
        res.status(400).json({ status: false, message: err, data: null });
    })
}

async function getTimesheetDetailsForIndividual(timesheetDetail, res) {
    if (timesheetDetail.pay_cycle_start && timesheetDetail.pay_cycle_end) {
        var allDayData = [];
        var dailyTotalForOne = { "hours": 0, "minutes": 0, "seconds": 0 };
        var weeklyTotalForOne = { "hours": 0, "minutes": 0, "seconds": 0 };
        var payCyleTotalForOne = { "hours": 0, "minutes": 0, "seconds": 0 };

        for (var i = new Date(timesheetDetail.pay_cycle_start); i <= new Date(timesheetDetail.pay_cycle_end); i.setDate(i.getDate() + 1)) {
            var attendenceList = await attendanceCollection.aggregate([{ $match: { userId: ObjectID(timesheetDetail.userId), punchDate: { $gte: new Date(new Date(i).setHours(0, 0, 0, 0)), $lte: new Date(new Date(i).setHours(23, 59, 59, 59)) } } }, { $sort: { punchTime: 1 } }]).exec();

            if (attendenceList.length > 0) {
                var isPunchMissing = await checkMissingPunchesAndGetDailyTotal(attendenceList);
                if (typeof isPunchMissing !== "string") {

                    dailyTotalForOne = addTime(dailyTotalForOne, isPunchMissing);
                    weeklyTotalForOne = addTime(weeklyTotalForOne, isPunchMissing);
                    payCyleTotalForOne = addTime(payCyleTotalForOne, isPunchMissing);

                    allDayData.push({ "Date": new Date(i), "data": isPunchMissing });
                }

            }

        }
        // return testTimesheet;
        var totalDaysForDaily = (new Date(timesheetDetail.pay_cycle_end).setHours(0, 0, 0, 0) - new Date(timesheetDetail.pay_cycle_start).setHours(0, 0, 0, 0)) / (1000 * 60 * 60 * 24)
        var checkDiff = ((dailyTotalForOne.hours * 60 * 60 * 1000) + (dailyTotalForOne.minutes * 60 * 1000) + (dailyTotalForOne.seconds * 1000)) / totalDaysForDaily;
        dailyTotalForOne = getMillisecondToHhMmSs(checkDiff);
        weeklyTotalForOne = getMillisecondToHhMmSs(((weeklyTotalForOne.hours * 60 * 60 * 1000) + (weeklyTotalForOne.minutes * 60 * 1000) + (weeklyTotalForOne.seconds * 1000)) / ((totalDaysForDaily + 1) / 7));

        return { "totalDaysForDaily": totalDaysForDaily, "dailyTotal": dailyTotalForOne, "weeklyTotal": weeklyTotalForOne, "payCyleTotal": payCyleTotalForOne };
        // res.status(200).json({status:true,message: "Timesheet summary",data:{"totalDaysForDaily":totalDaysForDaily,"dailyTotalForOne":dailyTotalForOne,"weeklyTotalForOne":weeklyTotalForOne,"payCyleTotalForOne":payCyleTotalForOne}})

    } else {
        testTimesheet.testmessage = "Something went wrong with pay_cycle_start or pay_cycle_end";
        return testTimesheet;
    }
}

function getMillisecondToHhMmSs(diff) {

    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);
    diff -= minutes * (1000 * 60);
    var seconds = Math.floor(diff / 1000);

    return { "hours": hours, "minutes": minutes, "seconds": seconds }
}


/**
  * This method is used to calculate weekly and pay cycle total time for given attendanceList, it should contain daily total
  * 
  * @param {*} req 
  * @param {*} res 
  * @author : Surendra Waso
  */
async function addWeeklyTime(attendenceList) {
    var weeklyTotalTimeLocal = { "hours": 0, "minutes": 0, "seconds": 0 };
    var payCycleTotalTimeLocal = { "hours": 0, "minutes": 0, "seconds": 0 };

    for (var i = 0; i < attendenceList.length; i++) {
        var currentCalcDate = new Date(attendenceList[i].punchDate);
        if (currentCalcDate.getDay() == 1) {
            weeklyTotalTimeLocal = { "hours": 0, "minutes": 0, "seconds": 0 };
        }
        weeklyTotalTimeLocal = await addTime(weeklyTotalTimeLocal, attendenceList[i].dailyTotal);
        payCycleTotalTimeLocal = await addTime(payCycleTotalTimeLocal, attendenceList[i].dailyTotal);

        attendenceList[i].weeklyTotalTime = weeklyTotalTimeLocal;
        attendenceList[i].payCycleTotalTime = payCycleTotalTimeLocal;

        // console.log("attendenceList[ = "+i+"] = "+JSON.stringify(attendenceList));
    }
    // console.log("attendenceList after await = "+JSON.stringify(attendenceList));
    return attendenceList;
}

/**
 * This method is used to merge two arrays and sort them according to time
 * 
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso
 */
function mergeByDate(inArray, outArray, timesheetDetails) {
    var mergedArray = inArray;
    var maxLength = inArray.length > outArray.length ? inArray.length : outArray.length;

    mergedArray.sort(sortByDate);
    outArray.sort(sortByDate);

    console.log("\n mergedArray + " + JSON.stringify(mergedArray))
    console.log("\n ---------\n outArray + " + JSON.stringify(outArray))

    for (var i = 0; i < maxLength; i++) {
        var inDate = new Date(mergedArray[i]._id.punchDate);
        var outDate = new Date(outArray[i]._id.punchDate);
        if (inDate.getFullYear() == outDate.getFullYear() && inDate.getMonth() == outDate.getMonth() && inDate.getDate() == outDate.getDate()) {
            mergedArray[i]["lastOut"] = outArray[i].lastOut;
        }
    }
    console.log("\n after for lastout mergedArray = " + JSON.stringify(mergedArray) + "\n");
    return mergeTimesheetDetails(mergedArray, timesheetDetails);
}

async function mergeTimesheetDetails(mergedDateArray, timesheetDetails) {
    // console.log("timesheetDetails - "+JSON.stringify(timesheetDetails)+"\n");
    // console.log("mergedArray b4 await = "+JSON.stringify(mergedDateArray)+"\n");
    for (var i = 0; i < mergedDateArray.length; i++) {
        mergedDateArray[i].punchDate = mergedDateArray[i]._id.punchDate;
        mergedDateArray[i].employeeName = timesheetDetails.employee_fullname;
        mergedDateArray[i].isApproved = timesheetDetails.isApproved;
        mergedDateArray[i].timesheetId = timesheetDetails._id;
        mergedDateArray[i].userId = timesheetDetails.userId;
        mergedDateArray[i].employee_fullname = timesheetDetails.employee_fullname;
        mergedDateArray[i].manager_id = timesheetDetails.manager_id;
        mergedDateArray[i].manager_full_name = timesheetDetails.manager_full_name;
        mergedDateArray[i].bussiness_unit = timesheetDetails.bussiness_unit;
        mergedDateArray[i].department = timesheetDetails.department;
        mergedDateArray[i].pay_group = timesheetDetails.pay_group;
        mergedDateArray[i].pay_frequency = timesheetDetails.pay_frequency;
        mergedDateArray[i].pay_cycle = timesheetDetails.pay_cycle;
        mergedDateArray[i].pay_cycle_start = timesheetDetails.pay_cycle_start;
        mergedDateArray[i].pay_cycle_end = timesheetDetails.pay_cycle_end;
        mergedDateArray[i].companyId = timesheetDetails.companyId;
        mergedDateArray[i].createdBy = timesheetDetails.createdBy;
        mergedDateArray[i].createdAt = timesheetDetails.createdAt;
        mergedDateArray[i].updatedAt = timesheetDetails.updatedAt;
        mergedDateArray[i].dailyTotal = await getDailyHours(mergedDateArray[i].punchDate, mergedDateArray[i].userId);//,res

        delete mergedDateArray[i]._id;

    }
    // console.log("mergedArray after await = "+JSON.stringify(mergedDateArray));
    return mergedDateArray;
}

function sortByDate(date1, date2) {
    return new Date(date1._id.punchDate).getTime() - new Date(date2._id.punchDate).getTime();
}

async function getDailyHours(date, userId) {//,res

    var dailyData;
    var attendenceDetails = await attendanceCollection.aggregate([{ $match: { userId: ObjectID(userId), punchDate: new Date(date) } }, { $sort: { punchTime: 1 } }]).exec();//aggregate([{$match:{userId:ObjectID(userId),punchDate:new Date(date)}},{$sort:{punchTime:1}}])
    // console.log("attendenceDetails 335- "+JSON.stringify({"match":{userId:ObjectID(userId),punchDate:new Date(date)}},{"sort":{punchTime:1}}));
    if (attendenceDetails) {
        dailyData = await checkMissingPunchesAndGetDailyTotal(attendenceDetails)
        // console.log("dailyData 355- "+JSON.stringify(dailyData));
        return dailyData;
    } else {
        return { "hours": 0, "minutes": 0, "seconds": 0 };
    }
}

function checkMissingPunchesAndGetDailyTotal(allPunches) {
    var isPunchMissing = false;
    var dailyTotal = { "hours": 0, "minutes": 0, "seconds": 0 };

    for (var i = 0; i < (allPunches.length - 1); i++) {
        //Check if any punch is missing
        console.log("allPunches[i+1].punchType - " + allPunches[i + 1].punchType)
        if (!(allPunches[i].punchType === "IN" && allPunches[i + 1].punchType === "OUT")) {
            isPunchMissing = "Some punches are missing";
        }
        i++;
    }
    if (isPunchMissing) {
        return isPunchMissing;
    } else {
        for (var j = 0; j < (allPunches.length - 1); j++) {
            dailyTotal = calculateTimeDifference(allPunches[j].punchTime, allPunches[j + 1].punchTime, dailyTotal);
            j++;
        }
        // var date    =   allPunches[0].punchDate;
        //console.log("dailyTotal"+JSON.stringify(allPunches));
        if (allPunches) {
            return dailyTotal;
        } else {
            return { date: [] };
        }

    }

}


function calculateTimeDifference(startDate, endDate, dailyTotal) {

    var diff = new Date(endDate).getTime() - new Date(startDate).getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);
    diff -= minutes * (1000 * 60);
    var seconds = Math.floor(diff / 1000);

    // If using time pickers with 24 hours format, add the below line get exact hours
    if (hours < 0)
        hours = hours + 24;

    return (addTime(dailyTotal, { "hours": Number((hours <= 9 ? "0" : "") + hours), "minutes": Number((minutes <= 9 ? "0" : "") + minutes), "seconds": Number((seconds <= 9 ? "0" : "") + seconds) }));
}

function addTime(totalNew, newAdd) {
    var total = {
        "hours": totalNew.hours,
        "minutes": totalNew.minutes,
        "seconds": totalNew.seconds
    };
    total.seconds += newAdd.seconds;

    if ((total.seconds) >= 60) {
        total.seconds = total.seconds - 60;
        total.minutes += 1;
    }

    total.minutes += newAdd.minutes;

    if ((total.minutes) >= 60) {
        total.minutes = total.minutes - 60;
        total.hours += 1;
    }

    total.hours = total.hours + newAdd.hours;
    console.log(total);
    return total;
}

async function checkIfDuplicateRequest(userId) {
    var requestExist = false;
    // var employeeDetails =   await userCollection.findOne({ _id: userId }) .populate('job.ReportsTo').exec()

    // var pay_cycle_start =   await getCurrentPayCycleStartEnd(employeeDetails.compensation.Pay_group,"Monday").firstday;
    // var pay_cycle_end   =   await getCurrentPayCycleStartEnd(employeeDetails.compensation.Pay_group,"Monday").lastday;

    var timesheetRequest = await timesheetRequestCollection.find({ userId: userId, pay_cycle: getCurrentPayCycle(new Date()) }).exec()

    console.log("timesheetRequest - " + JSON.stringify(timesheetRequest) + " {userId:userId,pay_cycle:getCurrentPayCycle(new Date())}" + JSON.stringify({ "userId": userId, "pay_cycle": getCurrentPayCycle(new Date()) }))
    if (timesheetRequest.length <= 0) {
        console.log("No request " + JSON.stringify(timesheetRequest))
        requestExist = false;
    } else {
        console.log("yes request " + JSON.stringify(timesheetRequest))
        requestExist = true;
    }

    return requestExist;
}

let getCurrentPayCycleStartEnd = function (payCycleType, weekStart) {
    var weekStartDay = weekStart == "Monday" ? 1 : 0;//If Week start is monday +1 else sunday 0
    var weekEndDay = weekStart == "Monday" ? 6 : 5;//If Week start is monday +4 else saturday 5

    var curr = new Date();//Today
    var first = (curr.getDate() - curr.getDay()) + weekStartDay;//First day(Monday if 5 day working and week start monday) of week
    var last = first + weekEndDay;//Last day(Friday if 5 day working and week start monday) of week
    var firstday = new Date(curr.setDate(first));
    var lastday = new Date(curr.setDate(last));

    console.log("\n" + "firstday - " + firstday + "\n" + " and lastday - " + lastday);
    return {
        "firstday": new Date(new Date(firstday).setHours(0, 0, 0, 0)),
        "lastday": new Date(new Date(lastday).setHours(0, 0, 0, 0))
    };
}

let getCurrentPayCycle = function (d) {

    // Create a copy of this date object
    var target = new Date(d.valueOf());
    // ISO week date weeks start on monday
    // so correct the day number
    var dayNr = (d.getDay() + 6) % 7;
    // Set the target to the thursday of this week so the
    // target date is in the right year
    target.setDate(target.getDate() - dayNr + 3);
    // ISO 8601 states that week 1 is the week
    // with january 4th in it
    var jan4 = new Date(target.getFullYear(), 0, 4);
    // Number of days between target date and january 4th
    var dayDiff = (target - jan4) / 86400000;
    // Calculate week number: Week 1 (january 4th) plus the
    // number of weeks between target date and january 4th
    var weekNr = 1 + Math.ceil(dayDiff / 7);
    return weekNr;
}

/**
*@author Bhaskar Keelu
*@date 11/06/2019
Method to fetch the employee missing punches.
*/
let getMissingPunches = (req, res) => {
    let body = req.body;
    // let final_result = [];
    var start_date = body.startdate; // yyyy-mm-dd
    var end_date = body.enddate; //yyyy-mm-dd
    var employee_type = body.employeetype;
    return new Promise((resolve, reject) => {
        if (!body.startdate && !body.enddate) reject("Please enter the start date and end datz");
        if (!body.employeetype) reject("Please select the employee type category.")
        resolve(null);
    }).then(() => {
        console.log({
            "and": [
                { "createdAt": { $gt: new Date(start_date) } },
                { "createdAt": { $lt: new Date(end_date) } }
            ]
        });
        attendanceCollection.find({
            $and: [
                { "createdAt": { $gt: new Date(start_date) } },
                { "createdAt": { $lt: new Date(end_date) } }
            ]
        }).then((result) => {
            var user_id = []
            result.forEach(function (element) {
                var id = element.userId;
                user_id.push(id);
            })
            var query = { _id: { $in: user_id } }
            var typeName = employee_type.type;
            var business_unit = typeName.businesunit ? typeName.businesunit : [];
            var location = typeName.location ? typeName.location : [];
            var department = typeName.department ? typeName.department : [];
            var pay_group = typeName.paygroup ? typeName.paygroup : [];
            // console.log(business_unit.length,"business_unit.length")
            if (business_unit.length != 0) {
                query['job.businessUnit'] = { $in: business_unit }
            } else
                if (location.length != 0) {
                    query['job.location'] = { $in: location }
                } else
                    if (department.length != 0) {
                        query['job.department'] = { $in: department }
                    } else
                        if (pay_group.length != 0) {
                            query['compensation.Pay_group'] = { $in: pay_group }
                        }

            if (typeName.All == true) {
                query = {
                    _id: { $in: user_id }
                }
            }
            // console.log(query)
            userCollection.find(query, { "personal.name": 1, "job.ReportsTo": 1 }).lean().exec()
                .then((response) => {
                    async.map(response, function (list, callback) {
                        userCollection.findOne({ _id: list.job.ReportsTo }, { "personal.name": 1 })
                            .then((managerresp) => {
                                if (managerresp) {
                                    list.details = managerresp.personal;
                                    callback(error, null);
                                } else {
                                    callback(error, null);
                                }
                            }).catch((error) => {
                                callback(false, { status: false, message: "err" });
                            })
                    }, function (error, resutls) {
                        if (error) {
                            res.status(500).json({ "data": error, message: "try again" })
                        } else {
                            result.sort((a, b) => (a.punchTime > b.punchTime) ? 1 : ((b.punchTime > a.punchTime) ? -1 : 0));
                            //  var final_missing_punches = 
                            var missingPunches = checkMissingPunches(response, result)
                            if (missingPunches != "") {
                                res.status(200).json({
                                    "status": true,
                                    "message": "Please check the missing punches",
                                    "missingpunches": missingPunches
                                })
                            } else {
                                res.status(200).json({
                                    "status": true,
                                    "message": "There is no missing punches"
                                })
                            }
                        }
                    })
                }).catch((error) => {
                    console.log(error)
                    res.status(500).json({
                        "error": error
                    })
                })
        }).catch((err) => {
            res.status(500).json({
                "error": err
            })
        })
    }).catch((err) => {
        res.status(500).json({
            "error": err
        })
    })
}


function checkMissingPunches(employeeData, allPunches) {
    console.log(JSON.stringify(employeeData), "employeeData");
    console.log(JSON.stringify(allPunches), "allPunches");
    var isPunchMissing = false;
    var missingPunches = [];
    for (var i = 0; i < (allPunches.length - 1); i++) {
        var parse_date = moment.utc(allPunches[i].punchTime).format('YYYY-MM-DD');
        var isNextPunch = allPunches[i + 1] ? true : false;
        if (isNextPunch) {
            var parse_date2 = moment.utc(allPunches[i + 1].punchTime).format('YYYY-MM-DD');
        }
        if (allPunches[i].punchType === "OUT") {
            isPunchMissing = true;
            var miss_punchType = "IN";
            for (var j = 0; j < employeeData.length; j++) {
                var count = 0;
                if ((employeeData[j]._id).toString() == (allPunches[i].userId).toString() && count == 0) {
                    var missingData = { "EmployeeDetails": employeeData[j], "MissingPunchType": miss_punchType, "MissingDate": parse_date, "PunchData": allPunches[i] }
                    missingPunches.push(missingData);
                }
            }
        } else
            if (!(allPunches[i].punchType === "IN" && allPunches[i + 1].punchType === "OUT") && (allPunches[i].userId).toString() === (allPunches[i + 1].userId).toString() && isNextPunch) {
                if (parse_date === parse_date2) {
                    isPunchMissing = true;
                    var miss_punchType = allPunches[i].punchType == "IN" ? "OUT" : "IN";
                    for (var j = 0; j < employeeData.length; j++) {
                        var count = 0;
                        if ((employeeData[j]._id).toString() == (allPunches[i].userId).toString() && count == 0) {
                            var missingData = { "EmployeeDetails": employeeData[j], "MissingPunchType": miss_punchType, "MissingDate": parse_date, "PunchData": allPunches[i] }
                            missingPunches.push(missingData);
                        }
                    }
                } else {
                    isPunchMissing = true;
                    var miss_punchType = "OUT";
                    for (var j = 0; j < employeeData.length; j++) {
                        var count = 0;
                        if ((employeeData[j]._id).toString() == (allPunches[i].userId).toString() && count == 0) {
                            var missingData = { "EmployeeDetails": employeeData[j], "MissingPunchType": miss_punchType, "MissingDate": parse_date, "PunchData": allPunches[i] }
                            missingPunches.push(missingData);
                        }
                    }
                }
            } else {
                if (!isNextPunch) {
                    isPunchMissing = true;
                    var miss_punchType = allPunches[i].punchType == "IN" ? "OUT" : "IN";
                    for (var j = 0; j < employeeData.length; j++) {
                        var count = 0;
                        if ((employeeData[j]._id).toString() == (allPunches[i].userId).toString() && count == 0) {
                            var missingData = { "EmployeeDetails": employeeData[j], "MissingPunchType": miss_punchType, "MissingDate": parse_date, "PunchData": allPunches[i] }
                            missingPunches.push(missingData);
                        }
                    }
                } else
                    if (allPunches[i].punchType === "IN" && allPunches[i + 1].punchType === "OUT" && (allPunches[i].userId).toString() === (allPunches[i + 1].userId).toString() && parse_date === parse_date2) {
                        i++;
                    } else {
                        isPunchMissing = true;
                        var miss_punchType = "OUT";
                        for (var j = 0; j < employeeData.length; j++) {
                            var count = 0;
                            if ((employeeData[j]._id).toString() == (allPunches[i].userId).toString() && count == 0) {
                                var missingData = { "EmployeeDetails": employeeData[j], "MissingPunchType": miss_punchType, "MissingDate": parse_date, "PunchData": allPunches[i] }
                                missingPunches.push(missingData);
                            }
                        }
                    }
            }
    }
    if (isPunchMissing) {
        return missingPunches;
    } else {
        return "";
    }
}

module.exports = {
    getAllPayGroups,//Get all pay groups from users collection
    employeeRequestForApproval,//Save the timesheet approval request to DB
    getApprovalRequests,//Get all approval according to selected options and dates
    updateApprovalRequests,//Update timesheet details
    getTimesheetDetails,//Get Timesheet details based on ID
    getTimesheetSummary,//Get timesheet summary for all for requested payCycle
    getMissingPunches
}