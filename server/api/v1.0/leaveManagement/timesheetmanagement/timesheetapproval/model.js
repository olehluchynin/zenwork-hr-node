let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
// let constants = require('./../../../../common/constants');

let TimesheetRequestSchema = new mongoose.Schema({
    userId: { type: ObjectID, ref: 'users', required:true},
    employee_fullname:{type:String, required:true },
    manager_id:{type:ObjectID,ref:'users',required:true},
    manager_full_name:{type:String,required:true},
    bussiness_unit:{type:String},
    department:{type:String},
    pay_group:{type:String,required:true},
    pay_frequency:{type:String,required:true},
    pay_cycle:{type:Number,required:true},
    pay_cycle_start:{type:Date,required:true},
    pay_cycle_end:{type:Date,required:true},
    admin_id:{type:ObjectID,ref:'users',required:true},
    admin_full_name:{type:String},
    isEmployeeApproved:{type: Boolean, default: false},
    isManagerApproved:{type: Boolean, default: false},
    isAdminApproved:{type:Boolean,default:false},
    employeeApprovalTimestamp:{type:Date},
    managerApprovalTimestamp:{type:Date},
    adminApprovalTimestamp:{type:Date},
    comments:{type:String},
    companyId: { type: ObjectID, ref: 'companies',required:true},
    createdBy: { type: ObjectID, ref: 'users' },
    updatedBy: { type: ObjectID, ref: 'users' },
},{
    timestamps: true,
    versionKey: false,
    strict: false
})

//Schema.plugin(autoIncrement.plugin, {model:'timesheetapproval_requests', field:'timesheetId', startAt: 1, incrementBy: 1});
let TimesheetRequestModel = mongoose.model('timesheetapproval_requests', TimesheetRequestSchema)

module.exports = {
    TimesheetRequestModel : TimesheetRequestModel
}