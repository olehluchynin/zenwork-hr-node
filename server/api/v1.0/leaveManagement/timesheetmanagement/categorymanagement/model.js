/***
 * ToDo: Comments
 */

let mongoose       = require('mongoose'),
    autoIncrement  = require('mongoose-auto-increment');
let ObjectID       = mongoose.Schema.ObjectId;
let constants      = require('./../../../common/constants');


let Schema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies'},
    categoryName:String,
    createdBy: {type:ObjectID,ref:'users'},
    updatedBy:{type:ObjectID,ref:'users'},
},{
    timestamps: true,
    versionKey: false,
    strict: false
});

Schema.plugin(autoIncrement.plugin, {model:'approvalcategories', field:'categoryId', startAt: 1, incrementBy: 1});

let ApprovalCategoriesModel = mongoose.model('approvalcategories', Schema);

module.exports = ApprovalCategoriesModel;
