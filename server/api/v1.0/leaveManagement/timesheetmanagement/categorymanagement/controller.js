/**
 * ToDo: Comments
 */

let mongoose                = require('mongoose');
let ApprovalCategoriesModel = require('./model');

let getAllCategories  =   (req,res) => {
    let body = req.body;
    ApprovalCategoriesModel.find({}).then((result)=>{
        
        res.status(200).json({ status: true, message: "Categories data Fetched", data: result ? result : [] });
    }).catch((err)=> {
        if (typeof err == 'object') {
            res.status(500).json({ status: false, message: "Internal server error", data: null });
        } else {
            res.status(400).json({ status: false, message: err, data: null });
        }
    })

}

let addCategory =   (req,res) => {
    let body = {
        categoryName:req.body.categoryName,
        createdBy: req.body.createdBy
    };

    ApprovalCategoriesModel.create(body)
    .then(
        (category) => {
            res.status(200).json({ status: true, message: "Category Successfully Added", data: category });
        }
    ).catch(
        (err) => {
            console.log(err);
            if (typeof err == 'object') {
                res.status(500).json({ status: false, message: "Internal server error", data: null });
            } else {
                res.status(400).json({ status: false, message: err, data: null });
            }
        }
    )

}

let updateCategory = (req, res) => {
    let body = {
        description: req.body.category,
        updatedBy: req.jwt.userId
    };

    ApprovalCategoriesModel.findOneAndUpdate({ _id: req.body._id }, { $set: body })
        .then(
            (category) => {
                res.status(200).json({ status: true, message: "Category Successfully Updated", data: category });
            }
        ).catch(
            (err) => {
                console.log(err);
                if (typeof err == 'object') {
                    res.status(500).json({ status: false, message: "Internal server error", data: null });
                } else {
                    res.status(400).json({ status: false, message: err, data: null });
                }
            }
        )
}

let deleteCategoryById  =   (req,res) => {
    let _id = ObjectID(req.body._id);
    return new Promise((resolve, reject) => {
        if (!_id) reject("Invalid categoryId");
        else resolve(null);
    }).then(() => {
        return ApprovalCategoriesModel.findOne({ _id: _id });
    }).then(category => {
        if (!category) {
            res.status(400).json({ status: false, message: "Category not found", data: null });
            return null;
        } else 
        {
            return category;
        }
        /*else {
            if (body.password) delete body.password;
            body.udpateById = req.jwt._id;
            return EmployeeModel.findOneAndUpdate({ _id: _id }, { $pull: { "emergencyContact": { "$elemMatch": { "workMail": body.workMail } } } });
        }*/
    }).then(category => {
        if (category) {
            ApprovalCategoriesModel.findByIdAndDelete(req.params.id)
            res.status(200).json({ status: true, message: "Deleted successfully", data: null });
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}