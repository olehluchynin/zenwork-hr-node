let express = require('express');

let router = express.Router();

let authMiddleware = require('../../../middlewares/auth');

let timesheetMaintenanceController  =   require('./controller');

router.post('/searchemployee',authMiddleware.isUserLogin,timesheetMaintenanceController.searchEmployee);
router.post('/searchapprovalemployee',authMiddleware.isUserLogin,timesheetMaintenanceController.searchApprovalEmployee);
router.post('/sendmissingapproval',authMiddleware.isUserLogin,timesheetMaintenanceController.sendEmailNotificationForMissingApproval);
router.post('/gethistroricaltimesheetdetails',authMiddleware.isUserLogin,timesheetMaintenanceController.getHistroricalTimesheet);
router.get('/getaccuralleavebalance/:userId',authMiddleware.isUserLogin,timesheetMaintenanceController.getAccuralLeaveBalanceDetails);

router.get('/test/:userId/:payCycle/:missingApprovalType',authMiddleware.isUserLogin,timesheetMaintenanceController.testQuery);
module.exports = router;