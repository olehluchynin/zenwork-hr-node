/**
 * @api {post} /timeSchedule/timesheetmaintenance/searchemployee Search employee in timesheet maintenance
 * @apiName Search employee in timesheet maintenance
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {String} selectedCategoryType    selected category type 
 * @apiParam {String} selectedCategory    selected category
 * @apiParam {String} searchString  name,etc from search box
 * @apiParam {String} userId _id of current employee
 * @apiParam {String} perPageCount selected page count
 * @apiParam {String} currentPageNum current page number
 *
 
 * @apiParamExample {json} Request-Example:
 *{
    "selectedCategoryType"  :   "BusinessUnit",
    "selectedCategory"      :   "Unit 1",
    "searchString"          :   "vip",
    "userId"                :   "5cda377f51f0be25b3adceb4",
    "perPageCount"          :   10,
    "currentPageNum"        :   0
}

 *

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Users found",
    "data": {
        "searchResult": [
            {
                "personal": {
                    "name": {
                        "firstName": "vipin",
                        "middleName": "M",
                        "lastName": "reddy",
                        "preferredName": "vipin"
                    },
                    "address": {
                        "street1": "hyd",
                        "street2": "hyd",
                        "city": "hyd",
                        "state": "NewYork",
                        "zipcode": "121222222",
                        "country": "United States"
                    },
                    "contact": {
                        "workPhone": "1234567654",
                        "extention": "2345",
                        "mobilePhone": "2345654345",
                        "homePhone": "6543321234",
                        "workMail": "info@gmail.com",
                        "personalMail": "vipin.reddy@mtwlabs.com"
                    },
                    "dob": "1992-06-10T18:30:00.000Z",
                    "gender": "male",
                    "maritalStatus": "Single",
                    "effectiveDate": "2019-05-13T18:30:00.000Z",
                    "veteranStatus": "Single",
                    "ssn": "123321233",
                    "ethnicity": "Ethnicity11",
                    "education": [],
                    "languages": [],
                    "visa_information": []
                },
                "job": {
                    "current_employment_status": {
                        "status": "Active",
                        "effective_date": "2019-06-06T18:30:00.000Z"
                    },
                    "Specific_Workflow_Approval": "No",
                    "employeeId": "1234",
                    "employeeType": "Full Time",
                    "hireDate": "2018-08-12T18:30:00.000Z",
                    "jobTitle": "Software Engineer",
                    "department": "Tech",
                    "location": "Banglore",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "FLSA_Code": "Exempt",
                    "EEO_Job_Category": "Professionals",
                    "Site_AccessRole": "5cd91346f308c21c43e50511",
                    "VendorId": "234532",
                    "employment_status_history": [
                        {
                            "_id": "5cd91cc3f308c21c43e5054e",
                            "status": "Active",
                            "effective_date": "2019-05-13T07:29:07.042Z",
                            "end_date": "2019-05-23T08:08:22.872Z"
                        },
                        {
                            "_id": "5ce654f6bee9b42bc9f15f31",
                            "status": "Active",
                            "effective_date": "2019-05-06T18:30:00.000Z",
                            "end_date": "2019-05-23T08:34:58.834Z"
                        },
                        {
                            "_id": "5ce65b3220c97f2d35a2801d",
                            "status": "Terminated",
                            "effective_date": "2019-05-06T18:30:00.000Z",
                            "end_date": "2019-05-24T02:01:22.631Z"
                        },
                        {
                            "_id": "5ce750723db9243152d77797",
                            "status": "Terminated",
                            "effective_date": "2019-05-07T18:30:00.000Z",
                            "end_date": "2019-06-07T04:32:33.257Z"
                        },
                        {
                            "_id": "5cf9e8e1912a72225bb3fb25",
                            "status": "Active",
                            "effective_date": "2019-06-06T18:30:00.000Z"
                        }
                    ],
                    "job_information_history": [
                        {
                            "_id": "5cd91cc3f308c21c43e5054f",
                            "jobTitle": "Software Engineer",
                            "department": "Tech",
                            "businessUnit": "Unit 1",
                            "unionFields": "Union 1",
                            "effective_date": "2019-05-13T07:29:07.042Z"
                        }
                    ],
                    "eligibleForRehire": true,
                    "last_day_of_work": "2019-06-20T18:30:00.000Z",
                    "off_boarding_template_assigned": "5cf9e8e1912a72225bb3fb27",
                    "terminationDate": "2019-06-06T18:30:00.000Z",
                    "terminationReason": "Attendance",
                    "terminationType": "Voluntary",
                    "termination_comments": "ghhj",
                    "ReportsTo": "5cda377f51f0be25b3adceb4"
                },
                "compensation": {
                    "NonpaidPosition": true,
                    "Pay_group": "Group2",
                    "Pay_frequency": "Semi-Monthly",
                    "compensationRatio": "2",
                    "Salary_Grade": "high",
                    "compensation_history": [],
                    "current_compensation": []
                },
                "_id": "5cd91cc3f308c21c43e5054d",
                "userId": 88
            }
        ],
        "totalCount": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /timeSchedule/timesheetmaintenance/searchapprovalemployee Search employee for approval
 * @apiName Search employee for approval
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {String} selectedCategory    selected category (Available options: AllEmployees,ManagerApprovedOnly,EmployeeApprovedOnly,NoApprovals)
 * @apiParam {String} payCycle    selected Pay Cycle
 * @apiParam {String} searchString  name,etc from search box
 * @apiParam {String} userId _id of current employee
 * @apiParam {String} perPageCount selected page count
 * @apiParam {String} currentPageNum current page number
 *
 
 * @apiParamExample {json} Request-Example:
 *{

"selectedCategory"      :   "AllEmployees",
"searchString"          :   "vip",
"userId"                :   "5cda377f51f0be25b3adceb4",
"perPageCount"          :   10,
"currentPageNum"        :   0,
"payCycle"              :   13
}
 *

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Searched timesheet details",
    "data": {
        "timesheetList": [
            {
                "isEmployeeApproved": false,
                "isManagerApproved": false,
                "isAdminApproved": false,
                "_id": "5d199ac34263f850ea8b56d8",
                "userId": "5cd91cc3f308c21c43e5054d",
                "employee_fullname": "vipin reddy",
                "manager_id": "5cda377f51f0be25b3adceb4",
                "manager_full_name": "Flor  reddy",
                "bussiness_unit": "Unit 1",
                "department": "Tech",
                "pay_group": "Group2",
                "pay_frequency": "Semi-Monthly",
                "pay_cycle": 13,
                "pay_cycle_start": "2019-07-01T00:00:00.000Z",
                "pay_cycle_end": "2019-07-07T00:00:00.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "admin_id": "5c9e11894b1d726375b23057",
                "admin_full_name": " ",
                "createdBy": "5cd91cc3f308c21c43e5054d",
                "createdAt": "2019-07-01T05:31:47.311Z",
                "updatedAt": "2019-07-04T06:47:36.277Z",
                "adminApprovalTimestamp": "2019-07-04T06:47:36.277Z",
                "comments": "test comment",
                "updatedBy": "5c9e11894b1d726375b23057",
                "managerApprovalTimestamp": "2019-07-04T06:46:56.321Z",
                "employeeApprovalTimestamp": "2019-07-04T06:46:23.263Z"
            }
        ],
        "totalCount": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {delete} /timeSchedule/deleteattendancefordate/:userId/:timesheetEmployee/:punchDate/:payCycle Delete attendance for one date
 * @apiName deleteattendanceforonedate
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
* @apiParam {String} userId    current employee
* @apiParam {String} timesheetEmployee    _id of employee to delete attendance
* @apiParam {String} punchDate    date for which data has to be deleted
* @apiParam {String} payCycle    selected Pay Cycle
*

 * @apiParamExample {json} Request-Example:
 *http://localhost:3003/api/v1.0/timeSchedule/deleteattendancefordate/5cda377f51f0be25b3adceb4/5cdeaa4c3c5c5b67e3533423/2019-06-03T18:30:00.000Z/23

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Attendance data deleted",
    "data": {
        "n": 3,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /timeSchedule/timesheetmaintenance/sendmissingapproval Send notification for missing approval
 * @apiName Send notification for missing approval
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * 
 * @apiParam {String} userId _id of current employee
 * @apiParam {String} payCycle    selected Pay Cycle
 * @apiParam {String} missingApprovalType Missing Approval Type(Available options: employeeMissingApproval, managerMissingApproval,'')
 *
 
 * @apiParamExample {json} Request-Example:
 *http://localhost:3003/api/v1.0/timeSchedule/timesheetmaintenance/sendmissingapproval
 {
     userId                 : "5cda377f51f0be25b3adceb4",
     payCycle               : "24",
     missingApprovalType    : "employeeMissingApproval"
 }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Missing approval list",
    "data": {
        "allMissingApprovalResult": [
            {
                "isEmployeeApproved": false,
                "isManagerApproved": true,
                "isAdminApproved": false,
                "_id": "5d00cb6b3778642bb07427ce",
                "userId": "5cda3d4a2fec3c2610219939",
                "employee_fullname": "Lina Dunker",
                "manager_id": "5cda377f51f0be25b3adceb4",
                "manager_full_name": "Flor  Dunker",
                "bussiness_unit": "Unit 1",
                "department": "Tech",
                "pay_group": "Group2",
                "pay_frequency": "Weekly",
                "pay_cycle": 24,
                "pay_cycle_start": "2019-06-09T18:30:00.000Z",
                "pay_cycle_end": "2019-06-15T18:30:00.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "createdBy": "5cda3d4a2fec3c2610219939",
                "createdAt": "2019-06-12T09:52:43.722Z",
                "updatedAt": "2019-06-12T09:52:43.722Z"
            },
            {
                "isEmployeeApproved": false,
                "isManagerApproved": true,
                "isAdminApproved": false,
                "_id": "5d065967d290c308a8336f55",
                "userId": "5cd91cc3f308c21c43e5054d",
                "employee_fullname": "suresh m",
                "manager_id": "5cda377f51f0be25b3adceb4",
                "manager_full_name": "Flor  m",
                "bussiness_unit": "Unit 1",
                "department": "Tech",
                "pay_group": "Group2",
                "pay_frequency": "Weekly",
                "pay_cycle": 24,
                "pay_cycle_start": "2019-06-16T18:30:00.000Z",
                "pay_cycle_end": "2019-06-22T18:30:00.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "admin_id": "5c9e11894b1d726375b23057",
                "admin_full_name": " ",
                "createdBy": "5cdeaa4c3c5c5b67e3533423",
                "createdAt": "2019-06-16T14:59:51.162Z",
                "updatedAt": "2019-06-16T15:35:57.149Z",
                "comments": "test reject for admin",
                "employeeApprovalTimestamp": "2019-06-16T15:31:48.432Z",
                "updatedBy": "5c9e11894b1d726375b23057",
                "managerApprovalTimestamp": "2019-06-16T15:33:47.355Z",
                "adminApprovalTimestamp": "2019-06-16T15:35:57.149Z"
            }
        ],
        "managerEmailArray": [],
        "employeeEmailArray": [
            "5cda3d4a2fec3c2610219939",
            "5cd91cc3f308c21c43e5054d"
        ]
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /timeSchedule/timesheetmaintenance/gethistroricaltimesheetdetails Get histrorical timesheet
 * @apiName Get histrorical timesheet
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * 
 * @apiParam {String} userId _id of timesheet employee
 * @apiParam {String} timesheetStartDate    selected start date
 * @apiParam {String} timesheetEndDate selected end date
 *
 
 * @apiParamExample {json} Request-Example:
 *{
    "userId":"5cdeaa4c3c5c5b67e3533423",
    "timesheetStartDate": "2019-07-16T18:30:00.000Z",
    "timesheetEndDate":"2019-07-22T18:30:00.000Z"
}
 *

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Timesheet data found",
    "data": {
        "ArrivalDetails": {
            "ontimeArrival": 0,
            "lateArrivalCount": 5,
            "earlyArrivalCount": 2,
            "totalHoursWorked": {
                "hours": 23,
                "minutes": 48,
                "seconds": 30
            },
            "averageHours": {
                "hours": 1,
                "minutes": 29,
                "seconds": 16
            }
        },
        "employeeWeekData": {
            "Sunday": {
                "day": "Sunday",
                "starttime": "08:00:00",
                "lunch_starttime": "12:00:00",
                "lunch_endtime": "12:30:00",
                "end_time": "17:00:00"
            },
            "Monday": {
                "day": "Monday",
                "starttime": "08:00:00",
                "lunch_starttime": "12:00:00",
                "lunch_endtime": "12:30:00",
                "end_time": "17:00:00"
            },
            "Tuesday": {
                "day": "Tuesday",
                "starttime": "08:00:00",
                "lunch_starttime": "12:00:00",
                "lunch_endtime": "12:30:00",
                "end_time": "17:00:00"
            },
            "Wednesday": {
                "day": "Wednesday",
                "starttime": "08:00:00",
                "lunch_starttime": "12:00:00",
                "lunch_endtime": "12:30:00",
                "end_time": "17:00:00"
            },
            "Thursday": {
                "day": "Thursday",
                "starttime": "08:00:00",
                "lunch_starttime": "12:00:00",
                "lunch_endtime": "12:30:00",
                "end_time": "17:00:00"
            },
            "Friday": {
                "day": "Friday",
                "starttime": "08:00:00",
                "lunch_starttime": "12:00:00",
                "lunch_endtime": "12:30:00",
                "end_time": "17:00:00"
            },
            "Saturday": {
                "day": "Saturday",
                "starttime": "08:00:00",
                "lunch_starttime": "12:00:00",
                "lunch_endtime": "12:30:00",
                "end_time": "17:00:00"
            }
        },
        "dateWiseAttendanceList": {
            "Tue May 28 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "hoursworked": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "grosshours": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "isWeekOff": false
            },
            "Wed May 29 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "hoursworked": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "grosshours": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "isWeekOff": false
            },
            "Thu May 30 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "attendanceDetails": [
                    {
                        "_id": "5cef9a79ef0f7d19a4db53c2",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Thursday",
                        "punchTime": "2019-05-30T08:55:21.437Z",
                        "punchDate": "2019-05-29T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-05-30T08:55:21.671Z",
                        "updatedAt": "2019-05-30T08:55:21.671Z"
                    },
                    {
                        "_id": "5cefbe34ef0f7d19a4db53c3",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Thursday",
                        "punchTime": "2019-05-30T11:27:48.472Z",
                        "punchDate": "2019-05-29T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-05-30T11:27:48.703Z",
                        "updatedAt": "2019-05-30T11:27:48.703Z"
                    },
                    {
                        "_id": "5cefe0642f475b0aac595746",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Thursday",
                        "punchTime": "2019-05-30T13:53:39.788Z",
                        "punchDate": "2019-05-29T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-05-30T13:53:40.013Z",
                        "updatedAt": "2019-05-30T13:53:40.013Z"
                    },
                    {
                        "_id": "5cefe1034a89fb1250313338",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Thursday",
                        "punchTime": "2019-05-30T13:56:19.472Z",
                        "punchDate": "2019-05-29T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-05-30T13:56:19.719Z",
                        "updatedAt": "2019-05-30T13:56:19.719Z"
                    }
                ],
                "hoursworked": {
                    "hours": 2,
                    "minutes": 35,
                    "seconds": 6
                },
                "grosshours": {
                    "hours": 5,
                    "minutes": 0,
                    "seconds": 58
                },
                "isWeekOff": false,
                "lateByTime": {
                    "hours": 6,
                    "minutes": 25,
                    "seconds": 21
                }
            },
            "Fri May 31 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "hoursworked": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "grosshours": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "isWeekOff": false
            },
            "Sat Jun 01 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "hoursworked": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "grosshours": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "isWeekOff": false
            },
            "Sun Jun 02 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "hoursworked": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "grosshours": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "isWeekOff": false
            },
            "Mon Jun 03 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "hoursworked": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "grosshours": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "isWeekOff": false
            },
            "Tue Jun 04 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "hoursworked": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "grosshours": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "isWeekOff": false
            },
            "Wed Jun 05 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "hoursworked": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "grosshours": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "isWeekOff": false
            },
            "Thu Jun 06 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "attendanceDetails": [
                    {
                        "_id": "5cf944689b25641368886511",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Thursday",
                        "punchTime": "2019-06-06T16:50:48.470Z",
                        "punchDate": "2019-06-05T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-06-06T16:50:48.724Z",
                        "updatedAt": "2019-06-06T16:50:48.724Z"
                    },
                    {
                        "_id": "5cf9447c9b25641368886512",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Thursday",
                        "punchTime": "2019-06-06T17:51:08.430Z",
                        "punchDate": "2019-06-05T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-06-06T17:51:08.661Z",
                        "updatedAt": "2019-06-06T17:51:08.661Z"
                    },
                    {
                        "_id": "5cf9448d9b25641368886513",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Thursday",
                        "punchTime": "2019-06-06T17:52:25.430Z",
                        "punchDate": "2019-06-05T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-06-06T17:52:25.668Z",
                        "updatedAt": "2019-06-06T17:52:25.668Z"
                    },
                    {
                        "_id": "5cff4d29d8571614505fe8f2",
                        "isApproved": false,
                        "day": "Thursday",
                        "punchTime": "2019-06-06T18:52:25.000Z",
                        "punchDate": "2019-06-05T18:30:00.000Z",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "createdAt": "2019-06-11T06:41:45.208Z",
                        "updatedAt": "2019-06-11T06:41:45.208Z"
                    }
                ],
                "hoursworked": {
                    "hours": 2,
                    "minutes": 0,
                    "seconds": 18
                },
                "grosshours": {
                    "hours": 2,
                    "minutes": 1,
                    "seconds": 36
                },
                "isWeekOff": false,
                "lateByTime": {
                    "hours": 14,
                    "minutes": 20,
                    "seconds": 48
                }
            },
            "Fri Jun 07 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "attendanceDetails": [
                    {
                        "_id": "5cfa6674baacdc157c94abbb",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Friday",
                        "punchTime": "2019-06-07T13:28:20.577Z",
                        "punchDate": "2019-06-06T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-06-07T13:28:20.804Z",
                        "updatedAt": "2019-06-07T13:28:20.804Z"
                    },
                    {
                        "_id": "5cfa6846baacdc157c94abbc",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Friday",
                        "punchTime": "2019-06-07T13:36:05.920Z",
                        "punchDate": "2019-06-06T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-06-07T13:36:06.143Z",
                        "updatedAt": "2019-06-07T13:36:06.143Z"
                    }
                ],
                "hoursworked": {
                    "hours": 0,
                    "minutes": 7,
                    "seconds": 45
                },
                "grosshours": {
                    "hours": 0,
                    "minutes": 7,
                    "seconds": 45
                },
                "isWeekOff": false,
                "lateByTime": {
                    "hours": 10,
                    "minutes": 58,
                    "seconds": 20
                }
            },
            "Sat Jun 08 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "attendanceDetails": [
                    {
                        "_id": "5cfb30b25e7af90ed03eb156",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Saturday",
                        "punchTime": "2019-06-08T03:51:14.179Z",
                        "punchDate": "2019-06-07T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-06-08T03:51:14.421Z",
                        "updatedAt": "2019-06-08T03:51:14.421Z"
                    },
                    {
                        "_id": "5cfb30d55e7af90ed03eb157",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Saturday",
                        "punchTime": "2019-06-08T03:51:48.968Z",
                        "punchDate": "2019-06-07T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-06-08T03:51:49.191Z",
                        "updatedAt": "2019-06-08T03:51:49.191Z"
                    },
                    {
                        "_id": "5cfb32345e7af90ed03eb158",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Saturday",
                        "punchTime": "2019-06-08T03:57:40.523Z",
                        "punchDate": "2019-06-07T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-06-08T03:57:40.755Z",
                        "updatedAt": "2019-06-08T03:57:40.755Z"
                    },
                    {
                        "_id": "5cfb6ceb75cc171bf4faadf4",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Saturday",
                        "punchTime": "2019-06-08T08:08:10.922Z",
                        "punchDate": "2019-06-07T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-06-08T08:08:11.136Z",
                        "updatedAt": "2019-06-08T08:08:11.136Z"
                    }
                ],
                "hoursworked": {
                    "hours": 4,
                    "minutes": 11,
                    "seconds": 4
                },
                "grosshours": {
                    "hours": 4,
                    "minutes": 16,
                    "seconds": 56
                },
                "isWeekOff": false,
                "lateByTime": {
                    "hours": 1,
                    "minutes": 21,
                    "seconds": 14
                }
            },
            "Sun Jun 09 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "hoursworked": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "grosshours": {
                    "hours": 0,
                    "minutes": 0,
                    "seconds": 0
                },
                "isWeekOff": false
            },
            "Mon Jun 10 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "attendanceDetails": [
                    {
                        "_id": "5cff763c411ce417a896919b",
                        "isApproved": false,
                        "day": "Tuesday",
                        "punchTime": "2019-06-10T02:52:25.000Z",
                        "punchDate": "2019-06-09T18:30:00.000Z",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "createdBy": "5cda377f51f0be25b3adceb4",
                        "createdAt": "2019-06-11T09:37:00.822Z",
                        "updatedAt": "2019-06-11T09:37:00.822Z"
                    },
                    {
                        "_id": "5cff763c411ce417a896919c",
                        "isApproved": false,
                        "day": "Tuesday",
                        "punchTime": "2019-06-10T03:00:25.000Z",
                        "punchDate": "2019-06-09T18:30:00.000Z",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "createdBy": "5cda377f51f0be25b3adceb4",
                        "createdAt": "2019-06-11T09:37:00.822Z",
                        "updatedAt": "2019-06-11T09:37:00.822Z"
                    },
                    {
                        "_id": "5cff763c411ce417a896919d",
                        "isApproved": false,
                        "day": "Tuesday",
                        "punchTime": "2019-06-10T04:10:25.000Z",
                        "punchDate": "2019-06-09T18:30:00.000Z",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "createdBy": "5cda377f51f0be25b3adceb4",
                        "createdAt": "2019-06-11T09:37:00.822Z",
                        "updatedAt": "2019-06-11T09:37:00.822Z"
                    },
                    {
                        "_id": "5cff763c411ce417a896919a",
                        "isApproved": false,
                        "day": "Tuesday",
                        "punchTime": "2019-06-10T16:00:25.000Z",
                        "punchDate": "2019-06-09T18:30:00.000Z",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "createdBy": "5cda377f51f0be25b3adceb4",
                        "createdAt": "2019-06-11T09:37:00.822Z",
                        "updatedAt": "2019-06-11T09:37:00.822Z"
                    }
                ],
                "hoursworked": {
                    "hours": 11,
                    "minutes": 58,
                    "seconds": 0
                },
                "grosshours": {
                    "hours": 13,
                    "minutes": 8,
                    "seconds": 0
                },
                "isWeekOff": false,
                "lateByTime": {
                    "hours": 0,
                    "minutes": 22,
                    "seconds": 25
                }
            },
            "Tue Jun 11 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "attendanceDetails": [
                    {
                        "_id": "5cff787ca570562faca6c754",
                        "isApproved": false,
                        "day": "Tuesday",
                        "punchTime": "2019-06-11T02:00:25.000Z",
                        "punchDate": "2019-06-10T18:30:00.000Z",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "createdBy": "5cda377f51f0be25b3adceb4",
                        "createdAt": "2019-06-11T09:46:36.108Z",
                        "updatedAt": "2019-06-11T09:46:36.108Z"
                    },
                    {
                        "_id": "5cff787ca570562faca6c755",
                        "isApproved": false,
                        "day": "Tuesday",
                        "punchTime": "2019-06-11T02:52:25.000Z",
                        "punchDate": "2019-06-10T18:30:00.000Z",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "createdBy": "5cda377f51f0be25b3adceb4",
                        "createdAt": "2019-06-11T09:46:36.108Z",
                        "updatedAt": "2019-06-11T09:46:36.108Z"
                    },
                    {
                        "_id": "5cff787ca570562faca6c756",
                        "isApproved": false,
                        "day": "Tuesday",
                        "punchTime": "2019-06-11T03:00:25.000Z",
                        "punchDate": "2019-06-10T18:30:00.000Z",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "createdBy": "5cda377f51f0be25b3adceb4",
                        "createdAt": "2019-06-11T09:46:36.108Z",
                        "updatedAt": "2019-06-11T09:46:36.108Z"
                    },
                    {
                        "_id": "5cff787ca570562faca6c757",
                        "isApproved": false,
                        "day": "Tuesday",
                        "punchTime": "2019-06-11T04:10:25.000Z",
                        "punchDate": "2019-06-10T18:30:00.000Z",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "createdBy": "5cda377f51f0be25b3adceb4",
                        "createdAt": "2019-06-11T09:46:36.108Z",
                        "updatedAt": "2019-06-11T09:46:36.108Z"
                    }
                ],
                "hoursworked": {
                    "hours": 2,
                    "minutes": 2,
                    "seconds": 0
                },
                "grosshours": {
                    "hours": 2,
                    "minutes": 10,
                    "seconds": 0
                },
                "isWeekOff": false
            },
            "Wed Jun 12 2019 00:00:00 GMT+0530 (India Standard Time)": {
                "attendanceDetails": [
                    {
                        "_id": "5cff78c3a570562faca6c758",
                        "isApproved": false,
                        "day": "Tuesday",
                        "punchTime": "2019-06-12T02:00:25.000Z",
                        "punchDate": "2019-06-11T18:30:00.000Z",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "createdBy": "5cda377f51f0be25b3adceb4",
                        "createdAt": "2019-06-11T09:47:47.423Z",
                        "updatedAt": "2019-06-11T09:47:47.423Z"
                    },
                    {
                        "_id": "5cff78c3a570562faca6c759",
                        "isApproved": false,
                        "day": "Tuesday",
                        "punchTime": "2019-06-12T02:52:25.000Z",
                        "punchDate": "2019-06-11T18:30:00.000Z",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "createdBy": "5cda377f51f0be25b3adceb4",
                        "createdAt": "2019-06-11T09:47:47.423Z",
                        "updatedAt": "2019-06-11T09:47:47.423Z"
                    },
                    {
                        "_id": "5d00c3e880442831c41b882e",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Wednesday",
                        "punchTime": "2019-06-12T09:20:39.789Z",
                        "punchDate": "2019-06-11T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "IN",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-06-12T09:20:40.039Z",
                        "updatedAt": "2019-06-12T09:20:40.039Z"
                    },
                    {
                        "_id": "5d00c47180442831c41b882f",
                        "isApproved": false,
                        "userId": "5cd91cc3f308c21c43e5054d",
                        "day": "Wednesday",
                        "punchTime": "2019-06-12T09:22:57.324Z",
                        "punchDate": "2019-06-11T18:30:00.000Z",
                        "createdBy": "5cdeaa4c3c5c5b67e3533423",
                        "punchType": "OUT",
                        "companyId": "5c9e11894b1d726375b23058",
                        "createdAt": "2019-06-12T09:22:57.544Z",
                        "updatedAt": "2019-06-12T09:22:57.544Z"
                    }
                ],
                "hoursworked": {
                    "hours": 0,
                    "minutes": 54,
                    "seconds": 17
                },
                "grosshours": {
                    "hours": 7,
                    "minutes": 22,
                    "seconds": 32
                },
                "isWeekOff": false
            }
        }
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */