/**
     * This file contains methods for timesheet maintenance.
     * 
     * @param {*} req 
     * @param {*} res 
     * @author : Surendra Waso
     */

let mongoose = require('mongoose');
let moment = require('moment');
let ObjectID = mongoose.Types.ObjectId;

let userCollection = require('../../../users/model');
let companyCollection = require('../../../companies/model');
let companySettingsCollection = require('../../../companies/settings/model');
let timesheetRequestCollection = require('./../timesheetapproval/model').TimesheetRequestModel;
let attendanceCollection = require('./../../model').AttendanceModel;
let holidayCollection = require('./../../Scheduler/model').scheduler_holiday;
let structureCollection = require('./../../../settings-structure/model');
let config = require('./../../../../../config/config');
var Handlebars = require('handlebars');
let fs = require('fs');
let mailer = require('./../../../../../common/mailer');

const employeeTypes = {
    "company": "company",
    "employee": "employee"
};

//Surendra(08-June-2019):Const for punch in and punch out
const punchTypeOptions = {
    "punchin": "IN",
    "punchout": "OUT"
};

const allDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

/**
 * This method is used to search the employes for selected filters in timesheet maintenance
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso(13 June 2019)
 */
let searchEmployee = (req, res) => {
    var allParams = req.body;

    return new Promise((resolve, reject) => {
        if (!allParams.selectedCategoryType) {
            reject("Category type is mandatory");
        }
        if (allParams.selectedCategoryType !== "AllEmployees" && !allParams.selectedCategory) {
            reject("Selected category type is mandatory");
        }
        if (!allParams.userId) {
            reject("User Id is mandatory");
        }
        // if(!allParams.searchString){
        //     reject("searchStringValue is mandatory");
        // }
        if (!allParams.perPageCount) {
            reject("perPageCount is mandatory");
        }

        if (!allParams.currentPageNum && allParams.currentPageNum != 0) {
            reject("currentPageNum is mandatory");
        }
        console.log("allParams - " + JSON.stringify(allParams));
        userCollection.find({ _id: ObjectID(allParams.userId) })
            .then((employee) => {
                if (!employee) {
                    reject("Employee not found");
                } else {
                    console.log("employee - " + JSON.stringify(employee));

                    var allFilters = {};
                    let valuesToSelect = {
                        "personal": true,
                        "userId": true,
                        "job": true,
                        "compensation": true
                    };

                    employee = employee[0];

                    if (allParams.selectedCategoryType == "AllEmployees" && employee.type == employeeTypes.company) {
                        allFilters.companyId = ObjectID(employee.companyId);
                        allFilters.type = { $ne: 'company' };
                    } else if (allParams.selectedCategoryType == "AllEmployees" && employee.type == employeeTypes.employee) {
                        allFilters["job.ReportsTo"] = ObjectID(employee._id);
                    } else if (allParams.selectedCategoryType == "BusinessUnit") {
                        allFilters["job.businessUnit"] = allParams.selectedCategory;
                    } else if (allParams.selectedCategoryType == "Location") {
                        allFilters["job.location"] = allParams.selectedCategory;
                    } else if (allParams.selectedCategoryType == "Department") {
                        allFilters["job.location"] = allParams.selectedCategory;
                    }

                    if (employee.type == employeeTypes.company) {
                        allFilters.companyId = ObjectID(employee.companyId);
                        allFilters.type = { $ne: 'company' };
                    } else if (employee.type == employeeTypes.employee) {
                        allFilters["job.ReportsTo"] = ObjectID(employee._id);
                    }

                    if (allParams.searchString) {
                        allFilters['$or'] = [
                            {
                                'personal.name.firstName': { '$regex': allParams.searchString, '$options': 'i' }
                            },
                            {
                                'personal.name.middleName': { '$regex': allParams.searchString, '$options': 'i' }
                            },
                            {
                                'personal.name.lastName': { '$regex': allParams.searchString, '$options': 'i' }
                            },
                            {
                                'personal.name.preferredName': { '$regex': allParams.searchString, '$options': 'i' }
                            },
                            {
                                'job.jobTitle': { '$regex': allParams.searchString, '$options': 'i' }
                            }
                        ];
                    }
                    console.log("allFilters - " + JSON.stringify(allFilters));
                    userCollection.find(allFilters, valuesToSelect).limit(Number(allParams.perPageCount)).skip((Number(allParams.perPageCount) * Number(allParams.currentPageNum)))
                        .then(async (searchResult) => {
                            console.log("\n searchResult ", searchResult)
                            var returnMessage = ""
                            var totalCount = 0;
                            if (searchResult.length == 0) {
                                returnMessage = "No users for selected combination";
                            } else {
                                totalCount = await userCollection.find(allFilters, valuesToSelect).countDocuments();
                                returnMessage = "Users found";
                            }

                            res.status(200).json({ status: true, message: returnMessage, data: { "searchResult": searchResult, "totalCount": totalCount } })
                        })
                }
            })


    }).catch((err) => {
        res.status(400).json({ status: false, message: err, data: null });
    })
}

async function getAllReportingEmployees(empType, empId) {
    var searchValue = {};
    if (empType == employeeTypes.company) {
        searchValue = { "companyId": empId }
    } else {
        searchValue = { "job.ReportsTo": empId }
    }
    var allReportingUser = await userCollection.find(searchValue).exec();
    return allReportingUser;
}

/**
 * This method is used to search the employes for approvaal data using selected filters
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso(17 June 2019)
 */
let searchApprovalEmployee = (req, res) => {
    var allParams = req.body;

    return new Promise((resolve, reject) => {
        if (!allParams.selectedCategory) {
            reject("Category is mandatory");
        }

        if (!allParams.userId) {
            reject("User Id is mandatory");
        }
        if (!allParams.payCycle) {
            reject("payCycle is mandatory");
        }
        // if(!allParams.searchString){
        //     reject("searchStringValue is mandatory");
        // }
        if (!allParams.perPageCount) {
            reject("perPageCount is mandatory");
        }

        if (!allParams.currentPageNum && allParams.currentPageNum != 0) {
            reject("currentPageNum is mandatory");
        }

        userCollection.findOne({ _id: ObjectID(allParams.userId) })
            .then((employee) => {
                if (!employee) {
                    reject("Employee not found");
                } else {
                    console.log("employee - " + JSON.stringify(employee));

                    var allFilters = {};
                    let valuesToSelect = {
                        "_id": true
                    };


                    if (allParams.selectedCategoryType == "AllEmployees" && employee.type == employeeTypes.company) {
                        allFilters.companyId = ObjectID(employee.companyId);
                        allFilters.type = { $ne: 'company' };
                    } else if (allParams.selectedCategoryType == "AllEmployees" && employee.type == employeeTypes.employee) {
                        allFilters["job.ReportsTo"] = ObjectID(employee._id);
                    }

                    if (employee.type == employeeTypes.company) {
                        allFilters.companyId = ObjectID(employee.companyId);
                        allFilters.type = { $ne: 'company' };
                    } else if (employee.type == employeeTypes.employee) {
                        allFilters["job.ReportsTo"] = ObjectID(employee._id);
                    }
                    if (allParams.searchString) {
                        allFilters['$or'] = [
                            {
                                'personal.name.firstName': { '$regex': allParams.searchString, '$options': 'i' }
                            },
                            {
                                'personal.name.middleName': { '$regex': allParams.searchString, '$options': 'i' }
                            },
                            {
                                'personal.name.lastName': { '$regex': allParams.searchString, '$options': 'i' }
                            },
                            {
                                'personal.name.preferredName': { '$regex': allParams.searchString, '$options': 'i' }
                            },
                            {
                                'job.jobTitle': { '$regex': allParams.searchString, '$options': 'i' }
                            }
                        ];
                    }
                    console.log("allFilters - " + JSON.stringify(allFilters));
                    userCollection.find(allFilters, valuesToSelect)
                        .then((searchResult) => {
                            var returnMessage = ""
                            if (searchResult.length == 0) {
                                reject("No users for selected combination");
                            } else {
                                var timesheetFilter = {};
                                if (allParams.selectedCategory == "ManagerApprovedOnly") {
                                    timesheetFilter.isManagerApproved = true;
                                    timesheetFilter.isEmployeeApproved = false;
                                } else if (allParams.selectedCategory == "EmployeeApprovedOnly") {
                                    timesheetFilter.isEmployeeApproved = true;
                                    timesheetFilter.isManagerApproved = false;
                                } else if (allParams.selectedCategory == "NoApprovals") {
                                    timesheetFilter.isEmployeeApproved = false;
                                    timesheetFilter.isManagerApproved = false;
                                } else {
                                    // timesheetFilter.isEmployeeApproved  =   false;
                                    // timesheetFilter.isManagerApproved   =   false;
                                }



                                timesheetFilter.pay_cycle = allParams.payCycle;
                                timesheetFilter.userId = {
                                    "$in": searchResult
                                }
                                console.log("timesheetFilter  " + JSON.stringify(timesheetFilter));
                                timesheetRequestCollection.find(timesheetFilter).limit(Number(allParams.perPageCount)).skip((Number(allParams.perPageCount) * Number(allParams.currentPageNum)))
                                    .then(async (timesheetList) => {
                                        var totalCount = 0;
                                        var returnMessage = "Searched timesheet details";
                                        if (timesheetList.length > 0) {
                                            totalCount = await timesheetRequestCollection.find(timesheetFilter).countDocuments();
                                        } else {
                                            returnMessage = "No data found";
                                        }

                                        res.status(200).json({ status: true, message: returnMessage, data: { "timesheetList": timesheetList, "totalCount": totalCount } })
                                    })


                            }

                        })
                }
            })
    }).catch((err) => {
        res.status(400).json({ status: false, message: err, data: null });
    })
}

/**
 * This method is used to send email notifications to employee or manager according to selection by Admin
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso(18 June 2019)
 */
let sendEmailNotificationForMissingApproval = (req, res) => {
    var allParams = req.body;

    return new Promise((resolve, reject) => {
        //userId,missingapproval,payCycle
        if (!allParams.userId) {
            reject("userId is mandatory");
        }
        if (!allParams.payCycle) {
            reject("payCycle is mandatory");
        }
        if (!allParams.missingApprovalType) {
            reject("missingApprovalType is mandatory");
        }

        userCollection.findOne({ _id: allParams.userId })
            .then((employee) => {
                if (!employee) {
                    reject("Employee not found");
                } else {
                    var empFilter = {};
                    if (employee.type == employeeTypes.company) {
                        empFilter.companyId = ObjectID(employee.companyId);
                        empFilter.type = { $ne: 'company' };
                    } else if (employee.type == employeeTypes.employee) {
                        empFilter["job.ReportsTo"] = ObjectID(employee._id);
                    }

                    var valuesToSelect = {
                        _id: true
                    };

                    userCollection.find(empFilter, valuesToSelect)
                        .then((allReportingEmployees) => {
                            var allFilters = {};
                            if (allParams.missingApprovalType == "employeeMissingApproval") {

                                allFilters.isEmployeeApproved = false;
                            } else if (allParams.missingApprovalType == "managerMissingApproval") {
                                allFilters.isManagerApproved = false;

                            } else {
                                allFilters["$or"] = [
                                    {
                                        'isEmployeeApproved': false
                                    },
                                    {
                                        'isManagerApproved': false
                                    }
                                ]
                            }
                            allFilters.pay_cycle = allParams.payCycle;
                            allFilters.userId = {
                                "$in": allReportingEmployees
                            };

                            timesheetRequestCollection.find(allFilters)
                                .then((allMissingApprovalResult) => {
                                    if (allMissingApprovalResult.length <= 0) {
                                        res.status(200).json({ status: true, message: "No missing approvals found", data: [] });
                                    } else {

                                        var employeeEmailArray = [];
                                        var managerEmailArray = [];

                                        for (var j = 0; j < allMissingApprovalResult.length; j++) {
                                            if (allMissingApprovalResult[j].isEmployeeApproved == false) {
                                                employeeEmailArray.push(ObjectID(allMissingApprovalResult[j].userId));
                                            }

                                            if (allMissingApprovalResult[j].isManagerApproved == false) {
                                                managerEmailArray.push(ObjectID(allMissingApprovalResult[j].manager_id));
                                            }
                                        }
                                        if (employeeEmailArray.length > 0) {
                                            allFilters = {
                                                _id: {
                                                    "$in": employeeEmailArray
                                                }
                                            };

                                            var valuesToSelect = {
                                                "personal.name": true,
                                                email: true
                                            };
                                            userCollection.find(allFilters, valuesToSelect)
                                                .then((allEmployeeForEmail) => {
                                                    if (allEmployeeForEmail.length > 0) {
                                                        /**
                                                         * ToDo: Send email 
                                                         */
                                                    }
                                                })
                                        }

                                        if (managerEmailArray.length > 0) {
                                            allFilters = {
                                                _id: {
                                                    "$in": managerEmailArray
                                                }
                                            };

                                            var valuesToSelect = {
                                                "personal.name": true,
                                                email: true
                                            };
                                            userCollection.find(allFilters, valuesToSelect)
                                                .then((allManagerForEmail) => {
                                                    var emailSent = sendEmailToAllForNoti(allManagerForEmail, allParams.missingApprovalType, res)
                                                    // res.status(200).json({status : true, message : "Missing approval list",data : {"allManagerForEmail":allManagerForEmail,"allMissingApprovalResult":allMissingApprovalResult,"managerEmailArray":managerEmailArray,"employeeEmailArray":employeeEmailArray}});
                                                })
                                        }
                                        // res.status(200).json({status : true, message : "Missing approval list",data : {"allMissingApprovalResult":allMissingApprovalResult,"managerEmailArray":managerEmailArray,"employeeEmailArray":employeeEmailArray}});
                                    }
                                })
                        })
                }
            })
    })
        .catch((err) => {
            res.status(400).json({ status: false, message: err, data: null });
        })
}

/**
 * This method is used to send email notifications to selected  employee or manager according to selection by Admin
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso(18 June 2019)
 */
let sendEmailNotification = (req, res) => {
    var body = req.body;

    return new Promise((resolve, reject) => {
        if (!body.userId) {
            reject("userId is mandatory");
        }
        if (!body.payCycle) {
            reject("payCycle is mandatory");
        }
        if (!body.employeeList) {
            reject("employeeList is mandatory");
        }
        if (body.employeeList && body.employeeList.length <= 0) {
            reject("Atleast one employee is required to send email");
        }

        userCollection.findOne({ _id: body.userId })
            .then((employee) => {
                if (!employee) {
                    reject("Requested employee not found");
                } else {
                    //ToDo: check with Nisar for exact action

                    var employeeIdArray = [];
                    for (var i = 0; i < employeeList.length; i++) {
                        employeeIdArray.push({ _id: ObjectID(employeeList[i].userId) });
                    }

                    var allFilters = {};
                    allFilters["$or"] = [
                        {
                            'isEmployeeApproved': false
                        },
                        {
                            'isManagerApproved': false
                        }
                    ]

                    allFilters.pay_cycle = allParams.payCycle;
                    allFilters.userId = {
                        "$in": employeeIdArray
                    };

                    console.log("allReportingEmployees - " + JSON.stringify(employeeIdArray));
                    console.log("\n allFilters - " + JSON.stringify(allFilters));
                    timesheetRequestCollection.find(allFilters)
                        .then((allMissingApprovalResult) => {
                            var employeeEmailArray = [];
                            var managerEmailArray = [];

                            for (var j = 0; j < allMissingApprovalResult.length; j++) {
                                if (allMissingApprovalResult[j].isEmployeeApproved == false) {
                                    employeeEmailArray.push(allMissingApprovalResult[j].userId);
                                }

                                if (allMissingApprovalResult[j].isManagerApproved == false) {
                                    managerEmailArray.push(allMissingApprovalResult[j].manager_id);
                                }
                            }

                            res.status(200).json({ status: true, message: "Missing approval list", data: { "allMissingApprovalResult": allMissingApprovalResult, "managerEmailArray": managerEmailArray, "employeeEmailArray": employeeEmailArray } });
                        })


                }
            })
    })
        .catch((err) => {
            res.status(400).json({ status: false, message: err, data: null });
        })
}

/**
 * This method is used to get historical timesheet of single employee 
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso(18 June 2019)
 */
let getHistroricalTimesheet = (req, res) => {
    var allParams = req.body;

    return new Promise((resolve, reject) => {
        if (!allParams.userId) {
            reject("userId is mandatory");
        }

        if (!allParams.timesheetStartDate) {
            reject("timesheetStartDate is mandatory");
        }
        if (!allParams.timesheetEndDate) {
            reject("timesheetEndDate is mandatory");
        }

        userCollection.findOne({ _id: allParams.userId })
            .then(async (employee) => {
                if (!employee) {
                    console.log(employee);
                    reject("Timesheet employee not found");
                } else {
                    var lateArrivalCount = 0;
                    var earlyArrivalCount = 0;
                    var ontimeArrival = 0;
                    var totalHoursWorked = { "hours": 0, "minutes": 0, "seconds": 0 };
                    /**
                     * ToDo: Check work schedule time,day, 
                     */
                    /**
                     * ToDo: Check leave applied by employee for current date range.
                     */
                    /**
                     * ToDo: Check Holiday applicable for employee for current date range
                     */
                    let locationFilterObj = { "name": "Banglore", "parentRef": "Location", "companyId": ObjectID("5c9e11894b1d726375b23058") };
                    let locationFieldsToSelect = { "_id": true };
                    var locationId = await structureCollection.find(locationFilterObj, locationFieldsToSelect);
                    console.log("allHolidays - " + allHolidays)
                    var allHolidays = await holidayCollection.find({ "companyId": ObjectID("5c9e11894b1d726375b23058"), "location_id": ObjectID("5d023dfd65807b01afd238a3") })
                    console.log("allHolidays - " + allHolidays)
                    /**
                     * End of : Check Holiday applicable for employee for current date range
                     */
                    var employeeWeekData = convertEmployeeWeekArrayToObject([
                        {
                            "day": "Sunday",
                            "starttime": "08:00:00",
                            "lunch_starttime": "12:00:00",
                            "lunch_endtime": "12:30:00",
                            "end_time": "17:00:00"
                        }, {
                            "day": "Monday",
                            "starttime": "08:00:00",
                            "lunch_starttime": "12:00:00",
                            "lunch_endtime": "12:30:00",
                            "end_time": "17:00:00"
                        },
                        {
                            "day": "Tuesday",
                            "starttime": "08:00:00",
                            "lunch_starttime": "12:00:00",
                            "lunch_endtime": "12:30:00",
                            "end_time": "17:00:00"
                        },
                        {
                            "day": "Wednesday",
                            "starttime": "08:00:00",
                            "lunch_starttime": "12:00:00",
                            "lunch_endtime": "12:30:00",
                            "end_time": "17:00:00"
                        },
                        {
                            "day": "Thursday",
                            "starttime": "08:00:00",
                            "lunch_starttime": "12:00:00",
                            "lunch_endtime": "12:30:00",
                            "end_time": "17:00:00"
                        },
                        {
                            "day": "Friday",
                            "starttime": "08:00:00",
                            "lunch_starttime": "12:00:00",
                            "lunch_endtime": "12:30:00",
                            "end_time": "17:00:00"
                        },
                        {
                            "day": "Saturday",
                            "starttime": "08:00:00",
                            "lunch_starttime": "12:00:00",
                            "lunch_endtime": "12:30:00",
                            "end_time": "17:00:00"
                        }]);
                    var allAttendanceData = await attendanceCollection.aggregate([{ $match: { userId: ObjectID(allParams.userId), punchDate: { $gte: new Date(allParams.timesheetStartDate), $lte: new Date(allParams.timesheetEndDate) } } }, { $sort: { punchTime: 1 } }]).exec();
                    // var allAttendanceData   = await   attendanceCollection.aggregate([{$match:{userId:ObjectID("5cd91cc3f308c21c43e5054d"),punchDate:{$gte:new Date("2019-05-27T18:30:00.000Z"),$lte:new Date("2019-06-22T18:30:00.000Z")}}},{$sort:{punchTime:1}}]).exec();

                    var dateWiseAttendanceList = {};
                    for (var date = new Date(new Date(allParams.timesheetStartDate).toISOString()); date < new Date(new Date(allParams.timesheetEndDate).toISOString()); date.setDate(date.getDate() + 1)) {

                        dateWiseAttendanceList[date] = {};

                        for (var i = 0; i < allAttendanceData.length; i++) {
                            if (dateWiseAttendanceList[allAttendanceData[i].punchDate] && !dateWiseAttendanceList[allAttendanceData[i].punchDate]["attendanceDetails"]) {
                                dateWiseAttendanceList[allAttendanceData[i].punchDate]["attendanceDetails"] = []
                            }
                            if (new Date(date).setHours(0, 0, 0, 0) == new Date(allAttendanceData[i].punchDate).setHours(0, 0, 0, 0) && dateWiseAttendanceList[allAttendanceData[i].punchDate]) {
                                dateWiseAttendanceList[allAttendanceData[i].punchDate]["attendanceDetails"].push(allAttendanceData[i]);
                            }
                        }
                    }

                    //Check daily hours and Gross hours, late arrival, early arrival
                    var allDates = Object.keys(dateWiseAttendanceList);
                    for (var j = 0; j < allDates.length; j++) {
                        if (!dateWiseAttendanceList[allDates[j]]["hoursworked"]) {
                            dateWiseAttendanceList[allDates[j]]["hoursworked"] = { "hours": 0, "minutes": 0, "seconds": 0 };
                            dateWiseAttendanceList[allDates[j]]["grosshours"] = { "hours": 0, "minutes": 0, "seconds": 0 };
                        }
                        dateWiseAttendanceList[allDates[j]].hoursworked = await checkMissingPunchesAndGetDailyTotal(dateWiseAttendanceList[allDates[j]].attendanceDetails);
                        totalHoursWorked = addTime(totalHoursWorked, dateWiseAttendanceList[allDates[j]].hoursworked);
                        if (!isAttendancePunchMissing(dateWiseAttendanceList[allDates[j]].attendanceDetails)) {
                            dateWiseAttendanceList[allDates[j]].grosshours = calculateTimeDifference(dateWiseAttendanceList[allDates[j]].attendanceDetails[0].punchTime, dateWiseAttendanceList[allDates[j]].attendanceDetails[dateWiseAttendanceList[allDates[j]].attendanceDetails.length - 1].punchTime, dateWiseAttendanceList[allDates[j]]["grosshours"])
                        }

                        //Check Late arrival or early arrival
                        if (employeeWeekData[allDays[new Date(allDates[j]).getDay()]] && employeeWeekData[allDays[new Date(allDates[j]).getDay()]].starttime) {
                            //Check if ontime arrival or not for first entry
                            dateWiseAttendanceList[allDates[j]].isWeekOff = false;
                            if (dateWiseAttendanceList[allDates[j]].attendanceDetails && dateWiseAttendanceList[allDates[j]].attendanceDetails.length > 0) {
                                var startTimeArr = employeeWeekData[allDays[new Date(allDates[j]).getDay()]].starttime.split(":");

                                if (dateWiseAttendanceList[allDates[j]].attendanceDetails[0].punchType == punchTypeOptions.punchin && (new Date(dateWiseAttendanceList[allDates[j]].attendanceDetails[0].punchTime) > new Date(new Date(dateWiseAttendanceList[allDates[j]].attendanceDetails[0].punchTime).setHours(Number(startTimeArr[0]), Number(startTimeArr[1]), Number(startTimeArr[2]))))) {
                                    lateArrivalCount++;
                                    dateWiseAttendanceList[allDates[j]].lateByTime = getMillisecondToHhMmSs(new Date(dateWiseAttendanceList[allDates[j]].attendanceDetails[0].punchTime).getTime() - new Date(dateWiseAttendanceList[allDates[j]].attendanceDetails[0].punchTime).setHours(Number(startTimeArr[0]), Number(startTimeArr[1]), Number(startTimeArr[2])));

                                } else if (dateWiseAttendanceList[allDates[j]].attendanceDetails && dateWiseAttendanceList[allDates[j]].attendanceDetails.length > 0 && new Date(dateWiseAttendanceList[allDates[j]].attendanceDetails[0].punchTime) < new Date(new Date(dateWiseAttendanceList[allDates[j]].attendanceDetails[0].punchTime).setHours(Number(startTimeArr[0]), Number(startTimeArr[1]), Number(startTimeArr[2])))) {
                                    earlyArrivalCount++;
                                } else {
                                    ontimeArrival++;
                                }
                            }

                        } else {
                            //No schedule for this day or week off 
                            dateWiseAttendanceList[allDates[j]].isWeekOff = true;
                        }

                    }



                    var totalAverageHours = await getAverageTime(totalHoursWorked, allDates.length);
                    res.status(200).json({ status: true, message: "Timesheet data found", data: { "ArrivalDetails": { "ontimeArrival": ontimeArrival, "lateArrivalCount": lateArrivalCount, "earlyArrivalCount": earlyArrivalCount, "totalHoursWorked": totalHoursWorked, "averageHours": totalAverageHours }, "employeeWeekData": employeeWeekData, "dateWiseAttendanceList": dateWiseAttendanceList } });
                }
            })

    })
        .catch((err) => {
            res.status(400).json({ status: false, message: err, data: null });
        })
}

/**
 * This method is used to get balance leave details
 * @param {*} req 
 * @param {*} res 
 * @author : Surendra Waso(28 June 2019)
 */
let getAccuralLeaveBalanceDetails = (req, res) => {
    var allParams = req.params;
    return new Promise((resolve, reject) => {
        if (!allParams.userId) {
            reject("userId is mandatory");
        }

        userCollection.find({ _id: ObjectID(allParams.userId) })
            .then((employee) => {
                if (!employee) {
                    reject("Employee not found");
                } else {
                    //Get all leave applicable
                    let allFilters = {
                        _id: ObjectID(allParams.userId)
                    };

                    let valuesToSelect = {
                        leave_Eligibility_id: true,
                        _id: false
                    };
                    console.log("allFilters " + JSON.stringify(allFilters) + " \n valuesToSelect " + JSON.stringify(valuesToSelect));

                    userCollection.find(allFilters, valuesToSelect).populate("leave_Eligibility_id")
                        .then((allLeaveGroups) => {
                            if (!allLeaveGroups) {
                                reject("No leaves available for requested employee");
                            } else {


                                res.status(200).json({ status: true, message: "allLeaveGroups", data: allLeaveGroups })
                            }
                        })
                }
            })


    })
        .catch((err) => {
            res.status(400).json({ status: false, message: err, data: null });
        })
}

function getAverageTime(totalHours, totalDays) {
    var avgHours = {
        "hours": totalHours.hours * 60 * 60,
        "minutes": totalHours.minutes * 60,
        "seconds": totalHours.seconds
    };
    var total = (avgHours.hours + avgHours.minutes + avgHours.seconds) / totalDays;
    return getMillisecondToHhMmSs(total * 1000);

}

function getMillisecondToHhMmSs(diff) {

    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);
    diff -= minutes * (1000 * 60);
    var seconds = Math.floor(diff / 1000);

    return { "hours": hours, "minutes": minutes, "seconds": seconds }
}

function convertEmployeeWeekArrayToObject(weekArray) {
    var weekObject = {};
    for (var i = 0; i < weekArray.length; i++) {
        if (weekArray[i].day) {
            weekObject[weekArray[i].day] = weekArray[i];
        }
    }
    return weekObject;
}

function isAttendancePunchMissing(allPunches) {
    var isPunchMissing = false;

    if (!allPunches || !allPunches.length) {
        return true;
    }
    for (var i = 0; i < (allPunches.length - 1); i++) {
        //Check if any punch is missing
        console.log("allPunches[i+1].punchType - " + allPunches[i + 1].punchType)
        if (!(allPunches[i].punchType === "IN" && allPunches[i + 1].punchType === "OUT")) {
            isPunchMissing = "Some punches are missing";
        }
        i++;
    }

    return isPunchMissing;

}


function checkMissingPunchesAndGetDailyTotal(allPunches) {
    var isPunchMissing = false;
    var dailyTotal = { "hours": 0, "minutes": 0, "seconds": 0 };
    if (!allPunches || !allPunches.length) {
        return dailyTotal;
    }
    for (var i = 0; i < (allPunches.length - 1); i++) {
        //Check if any punch is missing
        console.log("allPunches[i+1].punchType - " + allPunches[i + 1].punchType)
        if (!(allPunches[i].punchType === "IN" && allPunches[i + 1].punchType === "OUT")) {
            isPunchMissing = "Some punches are missing";
        }
        i++;
    }
    if (isPunchMissing) {
        return isPunchMissing;
    } else {
        for (var j = 0; j < (allPunches.length - 1); j++) {
            dailyTotal = calculateTimeDifference(allPunches[j].punchTime, allPunches[j + 1].punchTime, dailyTotal);
            j++;
        }
        // var date    =   allPunches[0].punchDate;
        //console.log("dailyTotal"+JSON.stringify(allPunches));
        if (allPunches) {
            return dailyTotal;
        } else {
            return { date: [] };
        }

    }

}

function calculateTimeDifference(startDate, endDate, dailyTotal) {

    var diff = new Date(endDate).getTime() - new Date(startDate).getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);
    diff -= minutes * (1000 * 60);
    var seconds = Math.floor(diff / 1000);

    // If using time pickers with 24 hours format, add the below line get exact hours
    if (hours < 0)
        hours = hours + 24;

    return (addTime(dailyTotal, { "hours": Number((hours <= 9 ? "0" : "") + hours), "minutes": Number((minutes <= 9 ? "0" : "") + minutes), "seconds": Number((seconds <= 9 ? "0" : "") + seconds) }));
}

function addTime(totalNew, newAdd) {
    var total = {
        "hours": totalNew.hours,
        "minutes": totalNew.minutes,
        "seconds": totalNew.seconds
    };
    total.seconds += newAdd.seconds;

    if ((total.seconds) >= 60) {
        total.seconds = total.seconds - 60;
        total.minutes += 1;
    }

    total.minutes += newAdd.minutes;

    if ((total.minutes) >= 60) {
        total.minutes = total.minutes - 60;
        total.hours += 1;
    }

    total.hours = total.hours + newAdd.hours;
    console.log(total);
    return total;
}

function sendEmailToAllForNoti(allEmailData, emailFor, res) {

    try {
        let to = [{ email: "surendra.w@mtwlabs.com" }];//'info@zenworkhr.com';
        let from = "info@zenworkhr.com";
        let subject = "ZenworkHR - " + "Test approval notification";
        let text = "ZenworkHR";

        fs.readFile(__dirname + '/empapprovalremindertemplate.hbs', 'utf8', async (err, file_data) => {
            if (!err) {
                let html_data = (Handlebars.compile(file_data))(allEmailData)
                console.log(to, from, subject, text, html_data)
                try {
                    var temp = await mailer.sendApprovalReminderMail(to, from, subject, text, html_data)
                    return temp;
                    // res.status(200).json({
                    //     status: true,
                    //     message: "Message sent successfully"
                    // })   
                }
                catch (e) {
                    console.log(e)
                    res.status(400).json({
                        status: false,
                        message: "Error while sending message",
                        error: err
                    })
                }
            }
            else {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while sending message",
                    error: err
                })
            }

        })

    }
    catch (err) {
        console.log("\n Error in Email notification - " + err);

    }

}

function getWeekDayDataForEmployee(allDayData, dayOfWeek) {
    var dataForDay = {};
    for (var i = 0; i < allDayData.length; i++) {
        if (allDayData[i].day == dayOfWeek) {
            dataForDay = allDayData[i];
            break;
        }
    }

    return dataForDay;
}

let testQuery = ((req, res) => {
    var allParams = req.params;
    return new Promise((resolve, reject) => {
        //userId,missingapproval,payCycle
        console.log("\n test success \n")
        console.log("allParams ", allParams)
        if (!allParams.userId) {
            reject("userId is mandatory");
        }
        if (!allParams.payCycle) {
            reject("payCycle is mandatory");
        }
        if (!allParams.missingApprovalType) {
            reject("missingApprovalType is mandatory");
        }
        console.log("allParams ", allParams)
        userCollection.findOne({ _id: allParams.userId })
            .then((employee) => {
                if (!employee) {
                    reject("Employee not found");
                } else {
                    var empFilter = {};
                    if (employee.type == employeeTypes.company) {
                        empFilter.companyId = ObjectID(employee.companyId);
                        empFilter.type = { $ne: 'company' };
                    } else if (employee.type == employeeTypes.employee) {
                        empFilter["job.ReportsTo"] = ObjectID(employee._id);
                    }

                    var valuesToSelect = {
                        _id: true
                    };

                    userCollection.find(empFilter, valuesToSelect)
                        .then((allReportingEmployees) => {
                            console.log("allReportingEmployees ", allReportingEmployees)
                            var allFilters = {};
                            if (allParams.missingApprovalType == "employeeMissingApproval") {

                                allFilters.isEmployeeApproved = false;
                            } else if (allParams.missingApprovalType == "managerMissingApproval") {
                                allFilters.isManagerApproved = false;

                            } else {
                                allFilters["$or"] = [
                                    {
                                        'isEmployeeApproved': false
                                    },
                                    {
                                        'isManagerApproved': false
                                    }
                                ]
                            }
                            allFilters.pay_cycle = allParams.payCycle;
                            allFilters.userId = {
                                "$in": allReportingEmployees
                            };

                            timesheetRequestCollection.find(allFilters).populate({ path: 'userId manager_id', select: 'personal.name job.HR_Contact email', populate: { path: 'job.HR_Contact', select: 'personal.name' } })
                                .then(async (allMissingApprovalResult) => {
                                    console.log("allMissingApprovalResult " + JSON.stringify(allMissingApprovalResult))
                                    if (allMissingApprovalResult.length <= 0) {
                                        res.status(200).json({ status: true, message: "No missing approvals found", data: [] });
                                    } else {

                                        var employeeEmailArray = [];
                                        var managerEmailArray = [];

                                        for (var j = 0; j < allMissingApprovalResult.length; j++) {
                                            if (allParams.missingApprovalType !== "managerMissingApproval" && allMissingApprovalResult[j].isEmployeeApproved == false) {
                                                employeeEmailArray.push(allMissingApprovalResult[j]);
                                            }

                                            if (allParams.missingApprovalType !== "employeeMissingApproval" && allMissingApprovalResult[j].isManagerApproved == false) {
                                                managerEmailArray.push(allMissingApprovalResult[j]);
                                            }
                                        }
                                        var emailResult = [];
                                        for (var k = 0; k < employeeEmailArray.length; k++) {
                                            var emailSent = await sendEmailToAllForNoti(employeeEmailArray[k], allParams.missingApprovalType, res);
                                            emailResult.push(emailSent);
                                        }

                                        // var emailSent   =  await sendEmailToAllForNoti(employeeEmailArray,allParams.missingApprovalType,res)
                                        res.status(200).json({ status: true, message: "Missing approval list", data: { "emailResult": emailResult, "allMissingApprovalResult": allMissingApprovalResult, "managerEmailArray": managerEmailArray, "employeeEmailArray": employeeEmailArray } });

                                    }
                                    // res.status(200).json({status : true, message : "Missing approval list",data : {"allMissingApprovalResult":allMissingApprovalResult,"managerEmailArray":managerEmailArray,"employeeEmailArray":employeeEmailArray}});

                                })
                        })
                }
            })
    })
        .catch((err) => {
            res.status(400).json({ status: false, message: err, data: null });
        })
})

module.exports = {
    searchEmployee,//Search employee for special data in timesheet maintenance
    searchApprovalEmployee,//Search employee for approval data in timesheet approval
    sendEmailNotificationForMissingApproval,//Send email notification for missing approval
    sendEmailNotification,//Send email notification for requested list of employees
    getHistroricalTimesheet,//Get histrorical timesheet data
    getAccuralLeaveBalanceDetails,//Get leave balance of single user for all types of leaves
    testQuery,//Test query delete this befor final update
}