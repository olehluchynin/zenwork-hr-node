/**
 * @apiDefine DataNotFoundError
 *
 * @apiError DataNotFound The data the you are requested was not found.
 *
 * @apiErrorExample Data Not Found Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "message": "Data Not Found"
 *       "data": null 
 *     }
 */


 /**
 * @apiDefine InternalServerError
 *
 * @apiError InternalServerError Internal Server error.
 *
 * @apiErrorExample Internal Server Error:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "status": false,
 *       "message": "Internal server error"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine TokenNotFound
 *
 * @apiError TokenNotFound User Token Not Found.
 *
 * @apiErrorExample Token Not Found:
 *     HTTP/1.1 400 Token Not Found
 *     {
 *       "status": false,
 *       "message": "Token Not Found"
 *       "data": null 
 *     }
 */

  /**
 * @apiDefine BadRequest
 *
 * @apiError BadRequest Bad request.
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": false,
 *       "message": "Invalid key"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine AccessDeneid
 *
 * @apiError AccessDenied Access denied for this operation.
 *
 * @apiErrorExample Access Deneid:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "status": false,
 *       "message": "Access Denied for this operation"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine DataAlreadyExists
 *
 * @apiError DataConflict Date already exists.
 *
 * @apiErrorExample Data Conflict Error:
 *     HTTP/1.1 402 Data Conflict
 *     {
 *       "status": false,
 *       "message": "Data already exist"
 *       "data": null 
 *     }
 */

/**
 * @api {post} /timeSchedule/punchIn Punch In
 * @apiName PunchIn
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
        "userId":"5cd91cc3f308c21c43e5054d"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Punch request added successfully",
    "data": {
        "isApproved": false,
        "_id": "5d146ad5b5008f2f48547cd2",
        "userId": "5cd91cc3f308c21c43e5054d",
        "day": "Thursday",
        "punchTime": "2019-06-27T07:05:56.929Z",
        "punchDate": "2019-06-26T18:30:00.000Z",
        "createdBy": "5cd91cc3f308c21c43e5054d",
        "punchType": "IN",
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-06-27T07:05:57.168Z",
        "updatedAt": "2019-06-27T07:05:57.168Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /timeSchedule/punchOut Punch Out
 * @apiName PunchOut
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
        "userId":"5cd91cc3f308c21c43e5054d"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Punch request added successfully",
    "data": {
        "isApproved": false,
        "_id": "5d146b23b5008f2f48547cd3",
        "userId": "5cd91cc3f308c21c43e5054d",
        "day": "Thursday",
        "punchTime": "2019-06-27T07:07:15.699Z",
        "punchDate": "2019-06-26T18:30:00.000Z",
        "createdBy": "5cd91cc3f308c21c43e5054d",
        "punchType": "OUT",
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-06-27T07:07:15.931Z",
        "updatedAt": "2019-06-27T07:07:15.931Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


 /**
 * @api {post} /timeSchedule/addmissingpunches Add missing punches
 * @apiName Add missing punches
 * @apiGroup Time Sheet Approval and Maintenance
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
"addedBy":"5cda377f51f0be25b3adceb4",        
"userId":"5cd91cc3f308c21c43e5054d",
"companyId"  : "5c9e11894b1d726375b23058",
"punchArray":[
{
                "day"        : "Tuesday",
                "punchTime"  : "Tue Jun 11 2019 07:30:25 GMT+0530 (India Standard Time)",
                "punchType"  : "IN"
                
},{
                "day"        : "Tuesday",
                "punchTime"  : "Fri Jun 11 2019 09:40:25 GMT+0530 (India Standard Time)",
                "punchType"  : "OUT"
                
},{
                "day"        : "Friday",
                "punchTime"  : "Fri Jun 14 2019 08:22:25 GMT+0530 (India Standard Time)",
                "punchType"  : "IN"
                
},{
                "day"        : "Friday",
                "punchTime"  : "Fri Jun 14 2019 08:30:25 GMT+0530 (India Standard Time)",
                "punchType"  : "OUT"
                
}]

}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Punches added successfully",
    "data": {
        "success": [
            {
                "isApproved": false,
                "_id": "5d148438a483d63060af1336",
                "day": "Tuesday",
                "punchTime": "2019-06-14T02:00:25.000Z",
                "punchDate": "2019-06-13T18:30:00.000Z",
                "punchType": "IN",
                "companyId": "5c9e11894b1d726375b23058",
                "userId": "5cd91cc3f308c21c43e5054d",
                "createdBy": "5cda377f51f0be25b3adceb4",
                "createdAt": "2019-06-27T08:54:16.416Z",
                "updatedAt": "2019-06-27T08:54:16.416Z"
            },
            {
                "isApproved": false,
                "_id": "5d148438a483d63060af1337",
                "day": "Tuesday",
                "punchTime": "2019-06-14T02:52:25.000Z",
                "punchDate": "2019-06-13T18:30:00.000Z",
                "punchType": "OUT",
                "companyId": "5c9e11894b1d726375b23058",
                "userId": "5cd91cc3f308c21c43e5054d",
                "createdBy": "5cda377f51f0be25b3adceb4",
                "createdAt": "2019-06-27T08:54:16.416Z",
                "updatedAt": "2019-06-27T08:54:16.416Z"
            },
            {
                "isApproved": false,
                "_id": "5d148438a483d63060af1338",
                "day": "Friday",
                "punchTime": "2019-06-15T03:00:25.000Z",
                "punchDate": "2019-06-14T18:30:00.000Z",
                "punchType": "IN",
                "companyId": "5c9e11894b1d726375b23058",
                "userId": "5cd91cc3f308c21c43e5054d",
                "createdBy": "5cda377f51f0be25b3adceb4",
                "createdAt": "2019-06-27T08:54:16.416Z",
                "updatedAt": "2019-06-27T08:54:16.416Z"
            },
            {
                "isApproved": false,
                "_id": "5d148438a483d63060af1339",
                "day": "Friday",
                "punchTime": "2019-06-15T04:10:25.000Z",
                "punchDate": "2019-06-14T18:30:00.000Z",
                "punchType": "OUT",
                "companyId": "5c9e11894b1d726375b23058",
                "userId": "5cd91cc3f308c21c43e5054d",
                "createdBy": "5cda377f51f0be25b3adceb4",
                "createdAt": "2019-06-27T08:54:16.416Z",
                "updatedAt": "2019-06-27T08:54:16.416Z"
            }
        ],
        "rejected": []
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */
