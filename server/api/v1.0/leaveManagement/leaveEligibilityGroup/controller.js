let mongoose = require('mongoose');
let moment = require('moment');
var xlsx = require('node-xlsx');

var json2xls = require('json2xls');
var fs = require('fs');

let ObjectID = mongoose.Types.ObjectId;

let leaveEligibilityModel = require('./model').leaveEligibility;
let userModel = require('../../users/model');

/**
*@author Asma
*@date 11/09/2019 10:34
Method to create a new leave eligibility group
*/
let createEligibilityGroup = (req, res) => {
    let body = req.body;
    let empIds = body.empIds;
    let companyId = req.jwt.companyId
    body.companyId = companyId
    delete body.empIds;
    return new Promise((resolve, reject) => {
        if (!body.name) reject('name is required')
        if (!body.effectiveDate) reject('effectiveDate is required')
        if (!empIds) reject('select employees')
        if (!empIds.length) reject('select employees')
        resolve(leaveEligibilityModel.findOne({ companyId: companyId, name: body.name }).lean().exec())
    })
        .then(group => {
            if (group) {
                res.status(400).json({
                    status: false,
                    message: "Leave Eligibility group already exists with this name"
                })
            }
            else {
                leaveEligibilityModel.create(body)
                    .then(result => {
                        Promise.all([
                            userModel.updateMany(
                                { _id: { $in: empIds } },
                                { $addToSet: { leave_eligibility_ids: result._id } }
                            ),
                            userModel.updateMany({_id: { $in: empIds }}, {$push: {leave_eligibility_group_history: {leave_eligibility_id: result._id}}})
                        ])
                        .then((resp) => {
                            res.status(200).json({
                                status: true,
                                message: "Successfully created leave eligilibility group",
                                data: resp
                            })
                        }).catch((err) => {
                            console.log(err)
                            res.status(500).json({
                                status: false,
                                message: "Error while creating leave eligilibility group",
                                error: err
                            })
                        })
                    }).catch((err) => {
                        console.log(err)
                        res.status(500).json({
                            status: false,
                            message: "Error while creating leave eligilibility group",
                            error: err
                        })
                    });
            }
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })

};

/**
*@author Asma
*@date 12/09/2019 10:35
Method to get a leave eligibility group
*/
let getEligibilityGroup = (req, res) => {
    let companyId = req.jwt.companyId
    let groupId = req.params._id
    return new Promise((resolve, reject) => {
        resolve(null)
    })
    .then(() => {
        leaveEligibilityModel.findOne({companyId: companyId, _id: ObjectID(groupId)}).lean().exec()
            .then(async group => {
                if (group) {
                    let users = await userModel.find({companyId: companyId, leave_eligibility_ids: ObjectID(groupId)}, {_id: 1}).lean().exec()
                    let userIds = users.map(x => x._id)
                    group.userIds = userIds
                    res.status(200).json({
                        status: true,
                        message: "Successfully fetched leave eligibility group details",
                        data: group
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "Leave eligibility group not found"
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({
                    status: false,
                    message: "Error while fetching leave eligibility group",
                    error : err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(500).json({
            status: false,
            message: "Error while fetching leave eligibility group",
            error : err
        })
    })
}

/**
*@author Asma
*@date 12/09/2019 12:27
Method to list leave eligibility groups of a company 
*/
let listLeaveEligibilityGroups = (req, res) => {
    let companyId = req.jwt.companyId
    return new Promise((resolve, reject) => {
        resolve(null)
    })
    .then(() => {
        leaveEligibilityModel.find({companyId: companyId}, {name: 1}).lean().exec()
            .then(groups => {
                res.status(200).json({
                    status: true,
                    message: "Successfully fetched leave eligibility groups",
                    data: groups
                })
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({
                    status: false,
                    message: "Error while fetching leave eligibility groups",
                    error : err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(500).json({
            status: false,
            message: "Error while fetching leave eligibility group",
            error : err
        })
    })
}


/**
*@author Asma
*@date 12/09/2019 12:54
Method to delete leave eligibility groups
*/
let deleteEligibilityGroups = (req, res) => {
    let body = req.body;
    let companyId = req.jwt.companyId
    return new Promise((resolve, reject) => {
        if (!body._ids) reject('_ids are required')
        if (!body._ids.length) reject('_ids are required')
        resolve(null);
    })
    .then(() => {
        let promises = []
        promises.push(leaveEligibilityModel.deleteMany({companyId: companyId, _id: {$in: body._ids}}))
        promises.push(userModel.updateMany({companyId: companyId, leave_eligibility_ids: {$in: body._ids}}, {$pull: {leave_eligibility_ids: {$in: body._ids}}}))
        // #TODO add end dates to lave eligbility history
        Promise.all(promises)
            .then(responses => {
                res.status(200).json({
                    status: true,
                    message: "Deleted leave eligibility groups successfully",
                    data: responses
                })
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({
                    status: false,
                    message: "Error while deleting leave eligibility groups",
                    error : err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(500).json({
            status: false,
            message: "Error while deleting leave eligibility groups",
            error : err
        })
    })
}


/**
*@author Asma
*@date 13/09/2019 12:16
Method to update leave eligibility group
*/
let updateEligibilityGroup = (req, res) => {
    let body = req.body;
    let empIds = body.empIds;
    let companyId = req.jwt.companyId
    let _id = body._id;
    delete body._id;
    delete body.empIds;
    delete body.createdAt;
    delete body.updatedAt;
    delete body.companyId;
    return new Promise((resolve, reject) => {
        if (!_id) reject('_id is required')
        if (!body.name) reject('name is required')
        if (!body.effectiveDate) reject('effectiveDate is required')
        if (!empIds) reject('select employees')
        if (!empIds.length) reject('select employees')
        resolve(leaveEligibilityModel.findOne({ companyId: companyId, name: body.name, _id: {$ne: ObjectID(_id)} }).lean().exec())
    })
    .then(group => {
        if(group) {
            res.status(409).json({
                status: true,
                message: "Leave eligibility group already exists with this name"
            })
        }
        else {
            leaveEligibilityModel.findOneAndUpdate({companyId: companyId, _id: _id}, {$set: body}, {new: true})
                .then(response => {
                    if (response) {
                        userModel.updateMany({companyId: companyId, leave_eligibility_ids: ObjectID(_id)}, {$pull: {leave_eligibility_ids: ObjectID(_id)}})
                            .then(user_pull_response => {
                                // #TODO leave eligibility history update 
                                userModel.updateMany(
                                    { _id: { $in: empIds } },
                                    { $addToSet: { leave_eligibility_ids: _id } }
                                )
                                .then(user_update_response => {
                                    res.status(200).json({
                                        status: true,
                                        message: "Successfully updated leave eligibility group",
                                        data: response
                                    })
                                })
                                .catch(err => {
                                    console.log(err)
                                    res.status(500).json({
                                        status: false,
                                        message: "Error while updating leave eligibility group",
                                        error: err
                                    })
                                })
                                
                            })
                            .catch(err => {
                                console.log(err)
                                res.status(500).json({
                                    status: false,
                                    message: "Error while updating leave eligibility group",
                                    error: err
                                })
                            })
                    }
                    else {
                        res.status(404).json({
                            status: false,
                            message: "Leave eligibility group not found"
                        })
                    }
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).json({
                        status: false,
                        message: "Error while updating leave eligibility group",
                        error: err
                    })
                })
        }
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}


// /**
// *@author Bhaskar Keelu
// *@date 28/05/2019
// Method to fetch the employee list based on benefit type
// */

// let getEmployeeList = (req, res) => {
//     let body = req.body;
//     leaveEligibilityModel.find(
//         {
//             "benefitType": { $in: body.benefitType }
//         }).distinct('_id').then((result) => {
//             if (result.length != 0) {
//                 userModel.find({ "leave_Eligibility_id": result }).then((empList) => {
//                     res.status(200).json({
//                         "Status": "Success",
//                         "Message":
//                             "Successfully fetched the data.",
//                         "Result": result ? result : [],
//                         "EmployeeList": empList ? empList : []
//                     })
//                 }).catch((error) => {
//                     res.status(400).json({
//                         "Status": false,
//                         "Message": "Error Exception while fetch Employee List",
//                         "Err": error
//                     })
//                 })
//             } else {
//                 res.status(200).json({
//                     "Status": false,
//                     "Message": "There are no assigned employees"
//                 })
//             }
//         })
// }

// /**
// *@author Bhaskar Keelu
// *@date 29/05/2019
// Method to fetch the employee list based on benefit type
// */
// let employeelistpaginate = (req, res) => {
//     let body = req.body;
//     if (body.per_page < body.page_no) {
//         res.status(200).json({
//             "status": false,
//             "message": "Page_Number should not be greater than per_page"
//         })
//     }
//     let per_page = body.per_page ? body.per_page : 10
//     let page_no = body.page_no ? body.page_no : 1
//     leaveEligibilityModel.find(
//         { "_id": { $in: body.benefitType } }).distinct('_id').then((result) => {
//             if (result.length != 0) {
//                 userModel.count({ "leave_Eligibility_id": result }).then((countemp) => {
//                     userModel.find({ "leave_Eligibility_id": result }).skip((page_no - 1) * per_page).limit(per_page).then((empList) => {
//                         res.status(200).json({
//                             "Status": "Success",
//                             "Message": "Successfully fetched the Employee List.",
//                             "EmployeeList": empList ? empList : [],
//                             "count": countemp
//                         })
//                     }).catch((error) => {
//                         res.status(400).json({
//                             "Status": false,
//                             "Message": "Error Exception while fetch Employee List",
//                             "Err": error
//                         })
//                     })
//                 })
//             } else {
//                 res.status(200).json({
//                     "Status": false,
//                     "Message": "There are no assigned employees"
//                 })
//             }
//         })
// }
// /**
// *@author Bhaskar Keelu
// *@date 03/06/2019
// Method to upload the employees list through excel file.
// */
// let uploadxlsx = (req, res) => {
//     return new Promise((resolve, reject) => {
//         resolve(null);
//     }).then(() => {
//         let file;
//         if (req.files) file = req.files.file;
//         // console.log('body ==> ',req.body);
//         // console.log('files ==> ',req.files);
//         // console.log(file);

//         var data = xlsx.parse(file.path)[0].data;
//         // console.log("===========>", data);
//         // console.log("===========>", data[0].data);
//         let toAdd = [];
//         for (let i = 1; i < data.length; i++) {
//             let obj = {};
//             if (data[i][0] && data[i][1] && data[i][2] && data[i][3]) {
//                 obj.empId = data[i][0];
//                 obj.empName = data[i][1];
//                 obj.groupName = data[i][2];
//                 obj.effectiveDate = new Date((data[i][3] - (25567 + 2)) * 86400 * 1000);
//                 obj.company_id = req.body.companyid
//                 toAdd.push(obj);
//             }
//         }
//         return toAdd;
//         // console.log(toAdd);
//         // return StructureModel.create(toAdd);
//     }).then(async toAdd => {
//         // async function processArray(array) {
//         for (const each of toAdd) {
//             console.log(each);

//             let doc = await userModel.findOneAndUpdate({ "job.employeeId": each.empId, "companyId": each.company_id }, { "leave_EligGrp_EffectDate": each.effectiveDate, "leave_EligGrp_GrpName": each.groupName }).sort({ _id: -1 });
//         }
//         console.log('Done!');
//         // }
//         return toAdd;
//     }).then((data) => {
//         // console.log(data);
//         if (data) {
//             res.status(200).json({ status: true, message: "Successfully Added " + data.length + ' Records' });
//         } else {
//             res.status(200).json({ status: true, message: "Data not found in the file" });
//         }
//     }).catch((err) => {
//         console.log(err);
//         res.status(400).json({ status: false, message: err, data: null });
//     });
// };

// /**
// *@author Bhaskar Keelu
// *@date 03/06/2019
// Method to download the sample employees list through excel file.
// */
// let exportExcel = (req, res) => {
//     var jsonArr = req.body.data;
//     var xls = json2xls(jsonArr);

//     fs.writeFileSync('file.xlsx', xls, 'binary');
//     res.download('file.xlsx');
// }

module.exports = {
    createEligibilityGroup,
    getEligibilityGroup,
    listLeaveEligibilityGroups,
    deleteEligibilityGroups,
    updateEligibilityGroup,
    
    // getEmployeeList,
    // employeelistpaginate,
    // uploadxlsx,
    // exportExcel
}