let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
//let constants = require('./../../../common/constants');

let leaveEligibiltySchema = new mongoose.Schema({
    name: {type: String, required: true},
    effectiveDate: {type: Date, required: true},
    useBenefitGroupRules: Boolean,
    benifitGroup: {type: ObjectID},
    companyId: {type: ObjectID, ref:'companies', required: true},
    initial_group_effective_date_details : {
        date_selection : {
            type: String,
            enum: ['date_of_hire', 'custom_date']
        },
        custom_date_field: {type: String}
    },
    timeOffPolicies: [{type: ObjectID, ref: "time_off_policies"}]
}, {
    timestamps: true,
    versionKey: false
});

leaveEligibiltySchema.index({companyId: 1, name: 1}, {unique: true})

// let leaveEligGrpHistorySchema = new mongoose.Schema({
//     companyId : {type: ObjectID, ref:'companies', required: true},
//     userId: {type: ObjectID, ref:'users', required: true},
//     leave_eligibility_id: { type: ObjectID, ref: 'leave_eligility_groups' }, 
//     effective_date: Date, 
//     end_date: Date
// }, {
//     timestamps: true,
//     versionKey: false
// })

let leaveEligibility = mongoose.model("leave_eligility_groups",leaveEligibiltySchema);

module.exports = {
    leaveEligibility
};