/**
 * @api {post} /leave-Eligibility-group/create CreateLeaveEligibityGroup
 * @apiName CreateLeaveEligibityGroup
 * @apiGroup LeaveEligibityGroup
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
	"name": "TestOne",
    "effectiveDate": "2019-09-11T12:25:33.950Z",
    "useBenefitGroupRules": false,
    "initial_group_effective_date_details" : {
        "date_selection" : "date_of_hire"
    },
    "empIds": [
    		"5d76382665b3a10ab0551178",
    		"5d761ebb0db0b007748f5222"
    	]
}
 * @apiParamExample {json} Request-Example2:
{
	"name": "TestTwo",
    "effectiveDate": "2019-09-11T12:25:33.950Z",
    "useBenefitGroupRules": true,
    "benifitGroup": "5d78b5aa585e7760e3397cc1",
    "initial_group_effective_date_details" : {
        "date_selection" : "custom_date",
        "custom_date_field": "LE_Group_Date"
    },
    "empIds": [
    		"5d78b5aa585e7760e3397cc0",
    		"5d76382665b3a10ab0551178"
    	]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "Status": true,
    "Message": "Successfully inserted the data.",
    "data": {
        "n": 2,
        "nModified": 2,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {get} /leaveEligibilityGroup/get/:_id GetLeaveEligibityGroup
 * @apiName GetLeaveEligibityGroup
 * @apiGroup LeaveEligibityGroup
 * 
 *   
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} _id LeaveEligibityGroups unique _id
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched leave eligibility group details",
    "data": {
        "_id": "5d78ead479b54b13680f623f",
        "name": "TestOne",
        "effectiveDate": "2019-09-11T12:25:33.950Z",
        "initial_group_effective_date_details": {
            "date_selection": "date_of_hire"
        },
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-09-11T12:38:44.619Z",
        "updatedAt": "2019-09-11T12:38:44.619Z",
        "userIds": [
            "5d761ebb0db0b007748f5222",
            "5d76382665b3a10ab0551178"
        ]
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /leaveEligibilityGroup/list ListLeaveEligibityGroups
 * @apiName ListLeaveEligibityGroups
 * @apiGroup LeaveEligibityGroup
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched leave eligibility groups",
    "data": [
        {
            "_id": "5d78ead479b54b13680f623f",
            "name": "TestOne"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /leaveEligibilityGroup/delete DeleteLeaveEligibityGroups
 * @apiName DeleteLeaveEligibityGroups
 * @apiGroup LeaveEligibityGroup
 * 
 *   
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"_ids": [
			"5d78ead479b54b13680f623f",
			"5d79fa91fb7c0a31a08ab171"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted leave eligibility groups successfully",
    "data": [
        {
            "n": 0,
            "ok": 1
        },
        {
            "n": 3,
            "nModified": 3,
            "ok": 1
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /leaveEligibilityGroup/update UpdateLeaveEligibityGroup
 * @apiName UpdateLeaveEligibityGroup
 * @apiGroup LeaveEligibityGroup
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
* @apiParamExample {json} Request-Example:
 *
{
    "_id": "5d79faadfb7c0a31a08ab173",
    "name": "TestFour",
    "effectiveDate": "2019-09-11T12:25:33.950Z",
    "initial_group_effective_date_details": {
        "date_selection": "custom_date",
        "custom_date_field": "LE_Group_Date"
    },
    "companyId": "5c9e11894b1d726375b23058",
    "createdAt": "2019-09-12T07:58:37.515Z",
    "updatedAt": "2019-09-12T07:58:37.515Z",
    "empIds": [
        "5d76382665b3a10ab0551178",
        "5d78b5aa585e7760e3397cc0",
        "5d70f08524239162c3722bb4"
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully updated leave eligibility group",
    "data": {
        "initial_group_effective_date_details": {
            "date_selection": "custom_date",
            "custom_date_field": "LE_Group_Date"
        },
        "_id": "5d79faadfb7c0a31a08ab173",
        "name": "TestFour",
        "effectiveDate": "2019-09-11T12:25:33.950Z",
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-09-12T07:58:37.515Z",
        "updatedAt": "2019-09-13T07:12:11.204Z"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


//===================================================================================================================
//===================================================================================================================
//===================================================================================================================

 /**
 * @api {post} /leave-Eligibility-group/getemplist GetEmployeeList
 * @apiName GetEmployeeList
 * @apiGroup Leave Eligibity Group
 * 
 *   
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"benefitType"  : ["Full Time","Retirees"]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "Status": "Success",
    "Message": "Successfully fetched the data.",
    "Result": [
        "5cebddf60ef791519fcd00f7",
        "5cecda34f9c29e149c54d8ad",
        "5cecda45f9c29e149c54d8ae",
        "5cecda4df9c29e149c54d8af",
        "5cece0cd0ef791519fcd0120"
    ],
    "EmployeeList": [
        {
            "personal": {
                "name": {
                    "firstName": "vipin",
                    "middleName": "M",
                    "lastName": "reddy",
                    "preferredName": "vipin"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "121222222",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1234567654",
                    "extention": "2345",
                    "mobilePhone": "2345654345",
                    "homePhone": "6543321234",
                    "workMail": "info@gmail.com",
                    "personalMail": "vipin.reddy@mtwlabs.com"
                },
                "dob": "1992-06-10T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-13T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "123321233",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Terminated",
                    "effective_date": "2019-05-07T18:30:00.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "1234",
                "employeeType": "Full Time",
                "hireDate": "2018-08-12T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Professionals",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "234532",
                "employment_status_history": [
                    {
                        "_id": "5cd91cc3f308c21c43e5054e",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:29:07.042Z",
                        "end_date": "2019-05-23T08:08:22.872Z"
                    },
                    {
                        "_id": "5ce654f6bee9b42bc9f15f31",
                        "status": "Active",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "end_date": "2019-05-23T08:34:58.834Z"
                    },
                    {
                        "_id": "5ce65b3220c97f2d35a2801d",
                        "status": "Terminated",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "end_date": "2019-05-24T02:01:22.631Z"
                    },
                    {
                        "_id": "5ce750723db9243152d77797",
                        "status": "Terminated",
                        "effective_date": "2019-05-07T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd91cc3f308c21c43e5054f",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:29:07.042Z"
                    }
                ],
                "eligibleForRehire": false,
                "last_day_of_work": "2019-05-14T18:30:00.000Z",
                "off_boarding_template_assigned": "5ce750723db9243152d7779b",
                "terminationDate": "2019-05-07T18:30:00.000Z",
                "terminationReason": "Conduct",
                "terminationType": "Voluntary",
                "termination_comments": "daksd"
            },
            "compensation": {
                "NonpaidPosition": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Semi-Monthly",
                "compensationRatio": "2",
                "Salary_Grade": "high",
                "compensation_history": [],
                "current_compensation": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cd91cc3f308c21c43e50552"
            },
            "status": "active",
            "groups": [
                "5cd922b7f308c21c43e50560",
                "5cdabfd9af231c1ca23a208a",
                "5cdac0aaaf231c1ca23a20fd",
                "5cdac212107c831d51cd05ee"
            ],
            "_id": "5cd91cc3f308c21c43e5054d",
            "email": "info@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "MV7Xx9MJ",
            "createdAt": "2019-05-13T07:29:07.060Z",
            "updatedAt": "2019-05-27T12:54:14.547Z",
            "userId": 88,
            "offboardedBy": "5c9e11894b1d726375b23057",
            "leave_Eligibility_id": "5cebddf60ef791519fcd00f7"
        },
        {
            "personal": {
                "name": {
                    "firstName": "raju",
                    "middleName": "king",
                    "lastName": "raju",
                    "preferredName": "raj"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "123456754",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1234567898",
                    "extention": "8765",
                    "mobilePhone": "9876543234",
                    "homePhone": "9876543212",
                    "workMail": "info@raju.com",
                    "personalMail": "raju@gmail.com"
                },
                "dob": "1997-07-15T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-05-13T18:30:00.000Z",
                "veteranStatus": "Married",
                "ssn": "1232123",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-13T07:32:36.683Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "123445",
                "employeeType": "Full Time",
                "hireDate": "2019-05-13T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Craft Workers",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "32223",
                "employment_status_history": [
                    {
                        "_id": "5cd91d94f308c21c43e50554",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:32:36.683Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd91d94f308c21c43e50555",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:32:36.683Z"
                    }
                ]
            },
            "compensation": {
                "payRate": {
                    "amount": 2
                },
                "NonpaidPosition": false,
                "Pay_group": "Group1",
                "Pay_frequency": "Weekly",
                "First_Pay_Date": "2019-05-14T18:30:00.000Z",
                "compensationRatio": "23",
                "Salary_Grade": "3g",
                "current_compensation": [],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cd91d94f308c21c43e50558"
            },
            "status": "active",
            "groups": [
                "5cd922b7f308c21c43e50560",
                "5cdabfd9af231c1ca23a208a",
                "5cdac017af231c1ca23a20c3",
                "5cdac0aaaf231c1ca23a20fd",
                "5cdac212107c831d51cd05ee"
            ],
            "_id": "5cd91d94f308c21c43e50553",
            "email": "info@raju.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "kqXfm2rX",
            "createdAt": "2019-05-13T07:32:36.693Z",
            "updatedAt": "2019-05-27T12:54:14.547Z",
            "userId": 89,
            "leave_Eligibility_id": "5cebddf60ef791519fcd00f7"
        },
        {
            "personal": {
                "name": {
                    "firstName": "sai",
                    "middleName": "r",
                    "lastName": "koth",
                    "preferredName": "sai"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "121123243",
                    "country": "India"
                },
                "contact": {
                    "workPhone": "2134564322",
                    "extention": "2342",
                    "mobilePhone": "2342344354",
                    "homePhone": "4234324546",
                    "workMail": "info@sai.com",
                    "personalMail": "sai@gmail.com"
                },
                "dob": "1992-02-23T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-05-06T18:30:00.000Z",
                "veteranStatus": "Married",
                "ssn": "2321321",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-13T07:36:08.077Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "123123",
                "employeeType": "Full Time",
                "hireDate": "2019-05-06T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Nick Location",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Non-Exempt",
                "EEO_Job_Category": "Administrative Support Workers",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "123123",
                "employment_status_history": [
                    {
                        "_id": "5cd91e68f308c21c43e5055a",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:36:08.077Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd91e68f308c21c43e5055b",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:36:08.077Z"
                    }
                ]
            },
            "compensation": {
                "payRate": {
                    "amount": 1
                },
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "First_Pay_Date": "1998-03-12T18:30:00.000Z",
                "compensationRatio": "1",
                "Salary_Grade": "high",
                "compensation_history": [],
                "current_compensation": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cd91e68f308c21c43e5055e"
            },
            "status": "active",
            "groups": [
                "5cdac017af231c1ca23a20c3"
            ],
            "_id": "5cd91e68f308c21c43e50559",
            "email": "info@sai.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "IWU0vANB",
            "createdAt": "2019-05-13T07:36:08.085Z",
            "updatedAt": "2019-05-27T12:54:14.547Z",
            "userId": 90,
            "leave_Eligibility_id": "5cebddf60ef791519fcd00f7"
        },
        {
            "personal": {
                "name": {
                    "firstName": "ravi",
                    "middleName": "kumar",
                    "lastName": "singam",
                    "preferredName": "ravi"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "223331232",
                    "country": "India"
                },
                "contact": {
                    "workPhone": "2353246273",
                    "extention": "9879",
                    "mobilePhone": "9876652334",
                    "homePhone": "6867823840",
                    "workMail": "vipin@gmail.com",
                    "personalMail": "info@ravi.com"
                },
                "dob": "1993-06-07T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2016-05-01T18:30:00.000Z",
                "veteranStatus": "Married",
                "ssn": "12321",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-13T07:56:03.886Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "99090",
                "employeeType": "Full Time",
                "hireDate": "2018-02-05T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Sales",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "1232",
                "employment_status_history": [
                    {
                        "_id": "5cd92313f308c21c43e5059a",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:56:03.886Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd92313f308c21c43e5059b",
                        "jobTitle": "Software Engineer",
                        "department": "Sales",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:56:03.886Z"
                    }
                ]
            },
            "compensation": {
                "payRate": {
                    "amount": 1
                },
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "First_Pay_Date": "2019-02-11T18:30:00.000Z",
                "compensationRatio": "1",
                "Salary_Grade": "low",
                "current_compensation": [],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cd92313f308c21c43e5059e"
            },
            "status": "active",
            "groups": [],
            "_id": "5cd92313f308c21c43e50599",
            "email": "vipin@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "EtmOV70m",
            "createdAt": "2019-05-13T07:56:03.905Z",
            "updatedAt": "2019-05-28T07:18:37.219Z",
            "userId": 91,
            "leave_Eligibility_id": "5cece0cd0ef791519fcd0120"
        },
        {
            "personal": {
                "name": {
                    "firstName": "asma",
                    "lastName": "a",
                    "middleName": "mubeen",
                    "preferredName": "asma"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "212212121",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1232189379",
                    "extention": "9732",
                    "mobilePhone": "2387434783",
                    "homePhone": "1231231233",
                    "workMail": "info@asma.com",
                    "personalMail": "asma@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1993-02-09T18:30:00.000Z",
                "gender": "female",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "2312312",
                "ethnicity": "Ethnicity11",
                "education": [
                    {
                        "_id": "5ce1525cc201f76fdcaab18b",
                        "university": "ddghdfgh",
                        "degree": "Bachelors",
                        "specialization": "IT",
                        "gpa": 5,
                        "start_date": "2019-05-23T18:30:00.000Z",
                        "end_date": "2019-06-28T18:30:00.000Z"
                    }
                ],
                "languages": [
                    {
                        "_id": "5ce1525cc201f76fdcaab18a",
                        "language": "Chinese",
                        "level_of_fluency": "Intermediate"
                    }
                ],
                "visa_information": [
                    {
                        "_id": "5ce1525cc201f76fdcaab189",
                        "visa_type": "EB-3",
                        "issuing_country": "India",
                        "issued_date": "2019-05-21T18:30:00.000Z",
                        "expiration_date": "2019-05-30T18:30:00.000Z",
                        "status": "Active",
                        "notes": "etwertwer"
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-13T09:52:10.102Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "99251",
                "employeeType": "Full Time",
                "hireDate": "2019-03-13T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "c edit loc",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Professionals",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "1111223",
                "employment_status_history": [
                    {
                        "_id": "5cd93e4a8e9dea1f4bb987dd",
                        "status": "Active",
                        "effective_date": "2019-05-13T09:52:10.102Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd93e4a8e9dea1f4bb987de",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T09:52:10.102Z"
                    }
                ]
            },
            "compensation": {
                "NonpaidPosition": false,
                "HighlyCompensatedEmployee": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "B",
                "compensationRatio": "23",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": null,
                            "unit": ""
                        },
                        "_id": "5ce1528ac201f76fdcaab18c",
                        "effective_date": null,
                        "payType": "",
                        "Pay_frequency": "",
                        "notes": "",
                        "changeReason": ""
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cd93e4a8e9dea1f4bb987e1"
            },
            "status": "active",
            "groups": [],
            "_id": "5cd93e4a8e9dea1f4bb987dc",
            "email": "info@asma.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "eQdrFU81",
            "createdAt": "2019-05-13T09:52:10.135Z",
            "updatedAt": "2019-05-28T06:50:45.260Z",
            "userId": 92,
            "leave_Eligibility_id": "5cecda45f9c29e149c54d8ae"
        },
        {
            "personal": {
                "name": {
                    "firstName": "Flor ",
                    "lastName": "Houchins",
                    "middleName": "Chantal",
                    "preferredName": "Flor"
                },
                "address": {
                    "street1": "khdfdjjsvb",
                    "street2": "kdfahdsoigi",
                    "city": "Manhattan",
                    "state": "NewYork",
                    "zipcode": "25332",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "39256649394",
                    "extention": "3264",
                    "mobilePhone": "237856549",
                    "homePhone": "36253532302",
                    "workMail": "flor@gmail.com",
                    "personalMail": "flor@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1992-01-06T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "12432361361246",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": [
                    {
                        "_id": "5ce42f0a1532171a8b5948e8",
                        "visa_type": "",
                        "issuing_country": "",
                        "issued_date": null,
                        "expiration_date": null,
                        "status": "",
                        "notes": ""
                    },
                    {
                        "_id": "5ce42f0a1532171a8b5948e7",
                        "visa_type": "",
                        "issuing_country": "",
                        "issued_date": null,
                        "expiration_date": null,
                        "status": "",
                        "notes": ""
                    },
                    {
                        "_id": "5ce42f0a1532171a8b5948e6",
                        "visa_type": "EB-1",
                        "issuing_country": "USA",
                        "issued_date": "2019-05-22T18:30:00.000Z",
                        "expiration_date": "2019-05-30T18:30:00.000Z",
                        "status": "Active",
                        "notes": "wefasdfasdfasd"
                    },
                    {
                        "_id": "5ce42f0a1532171a8b5948e5",
                        "visa_type": "EB-1",
                        "issuing_country": "USA",
                        "issued_date": "2019-05-22T18:30:00.000Z",
                        "expiration_date": "2019-05-30T18:30:00.000Z",
                        "status": "Active",
                        "notes": "wefasdfasdfasd"
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Leave",
                    "effective_date": "2019-05-17T03:05:56.342Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "99412",
                "employeeType": "Full Time",
                "hireDate": "2019-05-14T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "23565608395",
                "employment_status_history": [
                    {
                        "_id": "5cda377f51f0be25b3adceb5",
                        "status": "Active",
                        "effective_date": "2019-05-14T03:35:27.299Z"
                    },
                    {
                        "_id": "5cde251e7139a81c906b5344",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:05:56.342Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cda377f51f0be25b3adceb6",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-14T03:35:27.299Z"
                    }
                ]
            },
            "compensation": {
                "NonpaidPosition": false,
                "HighlyCompensatedEmployee": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "A",
                "compensationRatio": "233",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 123,
                            "unit": ""
                        },
                        "_id": "5cdbd2fd2d755332c3632a05",
                        "effective_date": "2019-05-25T18:30:00.000Z",
                        "payType": "Salary",
                        "Pay_frequency": "Weekly",
                        "notes": "asdasd",
                        "changeReason": "A-Promotion"
                    },
                    {
                        "payRate": {
                            "amount": 456,
                            "unit": ""
                        },
                        "_id": "5cdbd39b2d755332c3632a06",
                        "effective_date": "2019-05-30T08:52:45.000Z",
                        "payType": "Hourly",
                        "Pay_frequency": "Weekly",
                        "notes": "sdetrrewrt",
                        "changeReason": "A-Promotion"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": true,
                "assign_performance_management": true,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cda377f51f0be25b3adceba"
            },
            "status": "active",
            "groups": [],
            "_id": "5cda377f51f0be25b3adceb4",
            "email": "flor@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "396IgZZT",
            "createdAt": "2019-05-14T03:35:27.334Z",
            "updatedAt": "2019-05-28T06:50:45.260Z",
            "userId": 93,
            "leave_Eligibility_id": "5cecda45f9c29e149c54d8ae"
        },
        {
            "personal": {
                "name": {
                    "firstName": "Slyvia",
                    "middleName": "Marie",
                    "lastName": "Claus",
                    "preferredName": "Slyvia"
                },
                "address": {
                    "street1": "wejhtuhiothiefnvk",
                    "street2": "lfpg9tuyu9u9tjv",
                    "city": "Manhattan",
                    "state": "NewYork",
                    "zipcode": "465493",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "277935475934679",
                    "extention": "43564",
                    "mobilePhone": "74325443659034",
                    "homePhone": "4373469491",
                    "workMail": "Slyvia@gmail.com",
                    "personalMail": "Slyvia@gmail.com"
                },
                "dob": "1994-02-09T18:30:00.000Z",
                "gender": "female",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "37344245266623",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Terminated",
                    "effective_date": "2019-05-04T18:30:00.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "99573",
                "employeeType": "Full Time",
                "hireDate": "2019-05-14T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "325567950",
                "employment_status_history": [
                    {
                        "_id": "5cda3ad10e57b225e1240c9e",
                        "status": "Active",
                        "effective_date": "2019-05-14T03:49:37.209Z",
                        "end_date": "2019-05-23T09:02:59.816Z"
                    },
                    {
                        "_id": "5ce661c320c97f2d35a28022",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-14T18:30:00.000Z",
                        "end_date": "2019-05-23T13:01:10.788Z"
                    },
                    {
                        "_id": "5ce699964581392fcfc99164",
                        "status": "Terminated",
                        "effective_date": "2019-05-04T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cda3ad10e57b225e1240c9f",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-14T03:49:37.209Z"
                    }
                ],
                "eligibleForRehire": false,
                "last_day_of_work": "2019-05-05T18:30:00.000Z",
                "off_boarding_template_assigned": "5ce699964581392fcfc99168",
                "terminationDate": "2019-05-04T18:30:00.000Z",
                "terminationReason": "Attendance",
                "terminationType": "Voluntary",
                "termination_comments": "askjdksa"
            },
            "compensation": {
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "A",
                "compensationRatio": "23",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 18
                        },
                        "_id": "5cda3ad10e57b225e1240ca0",
                        "effective_date": "2019-05-19T18:30:00.000Z",
                        "Pay_frequency": "Monthly",
                        "changeReason": "--",
                        "notes": "--"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cda3ad10e57b225e1240ca3"
            },
            "status": "active",
            "groups": [],
            "_id": "5cda3ad10e57b225e1240c9d",
            "email": "Slyvia@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "Dt5ZZM8R",
            "createdAt": "2019-05-14T03:49:37.243Z",
            "updatedAt": "2019-05-27T12:54:14.547Z",
            "userId": 94,
            "offboardedBy": "5c9e11894b1d726375b23057",
            "leave_Eligibility_id": "5cebddf60ef791519fcd00f7"
        },
        {
            "personal": {
                "name": {
                    "firstName": "Lina",
                    "lastName": "Dunker",
                    "middleName": "James",
                    "preferredName": "Lina"
                },
                "address": {
                    "street1": "shdgioidvknkv",
                    "street2": "sdhiyfirnknckbk",
                    "city": "Manhattan",
                    "state": "NewYork",
                    "zipcode": "235365263",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "7745365555",
                    "extention": "3746",
                    "mobilePhone": "23652536649",
                    "homePhone": "233446546025",
                    "workMail": "lina@gmail.com",
                    "personalMail": "lina@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1995-06-07T18:30:00.000Z",
                "gender": "female",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "3655694769330",
                "ethnicity": "Ethnicity11",
                "education": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d2",
                        "university": "",
                        "degree": "",
                        "specialization": "",
                        "gpa": null,
                        "start_date": null,
                        "end_date": null
                    }
                ],
                "languages": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d1",
                        "language": "",
                        "level_of_fluency": "Native proficiency"
                    }
                ],
                "visa_information": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d0",
                        "visa_type": "",
                        "issuing_country": "",
                        "issued_date": null,
                        "expiration_date": null,
                        "status": "",
                        "notes": ""
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Terminated",
                    "effective_date": "2019-05-13T18:30:00.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "16099574",
                "employeeType": "Full Time",
                "hireDate": "2019-05-14T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "4346404",
                "employment_status_history": [
                    {
                        "_id": "5cda3d4a2fec3c261021993a",
                        "status": "Active",
                        "effective_date": "2019-05-14T04:00:10.410Z",
                        "end_date": "2019-05-17T03:23:07.896Z"
                    },
                    {
                        "_id": "5cde291b4cdb8624bcc28075",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:05:56.342Z",
                        "end_date": "2019-05-17T03:29:53.021Z"
                    },
                    {
                        "_id": "5cde2ab1a286b927accea5f0",
                        "status": "Active",
                        "effective_date": "2019-05-17T03:05:56.342Z",
                        "end_date": "2019-05-17T03:37:25.999Z"
                    },
                    {
                        "_id": "5cde2c765e2e562494b8187e",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:40:56.342Z",
                        "end_date": "2019-05-17T03:39:00.627Z"
                    },
                    {
                        "_id": "5cde2cd4e83d4e082c7ff95c",
                        "status": "Active",
                        "effective_date": "2019-05-17T04:40:56.342Z",
                        "end_date": "2019-05-17T03:41:08.613Z"
                    },
                    {
                        "_id": "5cde2d541352531830068e82",
                        "status": "Leave",
                        "effective_date": "2019-05-17T06:40:56.342Z",
                        "end_date": "2019-05-17T03:42:49.391Z"
                    },
                    {
                        "_id": "5cde2db9bb4f582cf8013065",
                        "status": "Active",
                        "effective_date": "2019-05-17T07:40:56.342Z",
                        "end_date": "2019-05-23T09:06:11.926Z"
                    },
                    {
                        "_id": "5ce6628320c97f2d35a2802b",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-14T18:30:00.000Z",
                        "end_date": "2019-05-23T09:51:37.374Z"
                    },
                    {
                        "_id": "5ce66d297732592f983702ac",
                        "status": "Terminated",
                        "effective_date": "2019-05-13T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cda3d4a2fec3c261021993b",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-14T04:00:10.410Z",
                        "end_date": "2019-05-17T04:43:15.819Z"
                    },
                    {
                        "_id": "5cde3be3f2285524c04b21e2",
                        "jobTitle": "Senior Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-17T04:05:44.350Z"
                    }
                ],
                "ReportsTo": "5cda377f51f0be25b3adceb4",
                "eligibleForRehire": false,
                "last_day_of_work": "2019-05-19T18:30:00.000Z",
                "off_boarding_template_assigned": "5ce66d297732592f983702b0",
                "terminationDate": "2019-05-13T18:30:00.000Z",
                "terminationReason": "Abandonment",
                "terminationType": "Voluntary",
                "termination_comments": "jhgjh"
            },
            "compensation": {
                "NonpaidPosition": true,
                "HighlyCompensatedEmployee": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "B",
                "compensationRatio": "60%",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 123456,
                            "unit": ""
                        },
                        "_id": "5cdb94f68ed19a301ececd18",
                        "Pay_frequency": "Bi-Weekly",
                        "changeReason": "A-Promotion",
                        "effective_date": "2019-05-21T11:56:01.000Z",
                        "notes": "dfghjdfgh",
                        "payType": "Test"
                    },
                    {
                        "payRate": {
                            "amount": 123,
                            "unit": ""
                        },
                        "_id": "5cdb96b98ed19a301ececd19",
                        "effective_date": "2019-05-25T18:30:00.000Z",
                        "payType": "Monthly",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "sdfgsdfg",
                        "changeReason": "A-Promotion"
                    },
                    {
                        "payRate": {
                            "amount": 678,
                            "unit": ""
                        },
                        "_id": "5cde972b3c5c5b67e353341b",
                        "effective_date": "2019-05-30T11:14:16.000Z",
                        "payType": "Test",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "sfdgsdfg",
                        "changeReason": "A-Promotion"
                    },
                    {
                        "payRate": {
                            "amount": 4563,
                            "unit": ""
                        },
                        "_id": "5cde9af23c5c5b67e353341f",
                        "effective_date": "2019-05-14T18:30:00.000Z",
                        "payType": "Hourly",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "ertetrte",
                        "changeReason": "A-Promotion"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": true,
                "assign_performance_management": true,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cda3d4a2fec3c261021993f"
            },
            "status": "active",
            "groups": [],
            "_id": "5cda3d4a2fec3c2610219939",
            "email": "lina@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "s9rI7Pt7",
            "createdAt": "2019-05-14T04:00:10.460Z",
            "updatedAt": "2019-05-28T06:50:54.107Z",
            "userId": 95,
            "offboardedBy": "5c9e11894b1d726375b23057",
            "leave_Eligibility_id": "5cecda4df9c29e149c54d8af"
        },
        {
            "personal": {
                "name": {
                    "firstName": "Smith ",
                    "lastName": "ford",
                    "middleName": "Dunkun ",
                    "preferredName": "Smith"
                },
                "address": {
                    "street1": "Rugby street",
                    "street2": "lane 2",
                    "city": "Newyork",
                    "state": "NewYork",
                    "zipcode": "45675",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "7653456785",
                    "extention": "1234",
                    "mobilePhone": "7702265585",
                    "homePhone": "5566445678",
                    "workMail": "gopikrishna.annaldas@hotmail.com",
                    "personalMail": "gopikrishna.annaldas@hotmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1997-02-10T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-05-20T18:30:00.000Z",
                "veteranStatus": "Other Veteran",
                "ssn": "123-32-2343",
                "ethnicity": "Hispanic or Latino",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Leave of Absence",
                    "effective_date": "2019-05-06T12:36:46.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "16099577",
                "employeeType": "Full Time",
                "hireDate": "2019-05-21T18:30:00.000Z",
                "jobTitle": "Test",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Technicians",
                "Site_AccessRole": "5cdac017af231c1ca23a20c3",
                "VendorId": "123456",
                "employment_status_history": [
                    {
                        "_id": "5cdec1d5df4fd06b83354eb9",
                        "status": "Active",
                        "effective_date": "2019-05-17T14:14:45.767Z",
                        "end_date": "2019-05-21T12:25:08.108Z"
                    },
                    {
                        "_id": "5ce3ee241532171a8b5948cc",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-07T12:24:39.000Z",
                        "end_date": "2019-05-21T12:33:26.043Z"
                    },
                    {
                        "_id": "5ce3f0161532171a8b5948cd",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-12T12:33:04.000Z",
                        "end_date": "2019-05-21T12:34:08.513Z"
                    },
                    {
                        "_id": "5ce3f0401532171a8b5948ce",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-01T12:33:55.000Z",
                        "end_date": "2019-05-21T12:37:04.532Z"
                    },
                    {
                        "_id": "5ce3f0f01532171a8b5948cf",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-06T12:36:46.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cdec1d5df4fd06b83354eba",
                        "jobTitle": "Internship",
                        "department": "Sales",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-17T14:14:45.767Z",
                        "end_date": "2019-05-22T06:36:59.768Z"
                    },
                    {
                        "_id": "5ce4ee0b1532171a8b5949f6",
                        "jobTitle": "Internship",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "location": "Banglore",
                        "ReportsTo": "5cd91cc3f308c21c43e5054d",
                        "changeReason": "A-Promotion",
                        "remoteWork": "Yes",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "notes": "sample",
                        "end_date": "2019-05-22T06:37:45.600Z"
                    },
                    {
                        "_id": "5ce4ee391532171a8b594a00",
                        "jobTitle": "Test",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "location": "Banglore",
                        "ReportsTo": "5cd91cc3f308c21c43e5054d",
                        "changeReason": "A-Promotion",
                        "remoteWork": "Yes",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "notes": "sample"
                    }
                ],
                "ReportsTo": "5cdeb987c8b3566a2c99d450"
            },
            "compensation": {
                "NonpaidPosition": true,
                "HighlyCompensatedEmployee": true,
                "Pay_group": "Group1",
                "Pay_frequency": "Bi-Weekly",
                "Salary_Grade": "B",
                "compensationRatio": "30",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 557,
                            "unit": ""
                        },
                        "_id": "5cdfada6c201f76fdcaab181",
                        "effective_date": "2019-05-21T07:10:23.000Z",
                        "payType": "Test",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "tyutyui",
                        "changeReason": "A-Promotion"
                    },
                    {
                        "payRate": {
                            "amount": 890,
                            "unit": ""
                        },
                        "_id": "5cdfb31fc201f76fdcaab183",
                        "effective_date": "2019-05-30T07:23:26.000Z",
                        "payType": "Salary",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "zxcv",
                        "changeReason": "A-Promotion"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": true,
                "assign_performance_management": true,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cdec1d5df4fd06b83354ec9"
            },
            "status": "active",
            "groups": [],
            "_id": "5cdec1d5df4fd06b83354eb8",
            "email": "gopikrishna.annaldas@hotmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "zakLK1yh",
            "createdAt": "2019-05-17T14:14:45.801Z",
            "updatedAt": "2019-05-28T06:50:45.260Z",
            "userId": 98,
            "leave_Eligibility_id": "5cecda45f9c29e149c54d8ae"
        },
        {
            "personal": {
                "name": {
                    "firstName": "nick1234",
                    "lastName": "bondurant1234",
                    "middleName": "l1234",
                    "preferredName": "nick112233"
                },
                "address": {
                    "street1": "asdf",
                    "street2": "asdf",
                    "city": "COLUMBIA",
                    "state": "NewYork",
                    "zipcode": "21045",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "8771111111",
                    "extention": "1234",
                    "mobilePhone": "8771111111",
                    "homePhone": "8771111111",
                    "workMail": "asdf@adfa.com",
                    "personalMail": "asdf@aasf.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "2019-05-17T05:00:00.000Z",
                "gender": "male",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-17T05:00:00.000Z",
                "veteranStatus": "No Military Service",
                "ssn": "asdfasdfasfd",
                "ethnicity": "Not Hispanic or Latino",
                "education": [
                    {
                        "_id": "5ce2d2ca1b4a170f16e73779",
                        "university": "",
                        "degree": "",
                        "specialization": "",
                        "gpa": null,
                        "start_date": null,
                        "end_date": null
                    }
                ],
                "languages": [
                    {
                        "_id": "5ce2d2ca1b4a170f16e73778",
                        "language": "",
                        "level_of_fluency": ""
                    }
                ],
                "visa_information": [
                    {
                        "_id": "5ce2d2ca1b4a170f16e73777",
                        "visa_type": "V-1",
                        "issuing_country": "USA",
                        "issued_date": "2019-05-21T06:13:36.495Z",
                        "expiration_date": "2020-06-21T06:13:36.495Z",
                        "status": "Active",
                        "notes": "I will need sponsorship for visa next year"
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-17T19:38:41.036Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "16099578",
                "employeeType": "Full Time",
                "hireDate": "2019-05-08T05:00:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "c edit loc",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "First/Mid Level Officials and Managers",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "5",
                "employment_status_history": [
                    {
                        "_id": "5cdf0dc1df4fd06b83354f2b",
                        "status": "Active",
                        "effective_date": "2019-05-17T19:38:41.036Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cdf0dc1df4fd06b83354f2c",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-17T19:38:41.036Z"
                    }
                ]
            },
            "compensation": {
                "NonpaidPosition": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "21",
                "compensationRatio": "12",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 1123
                        },
                        "_id": "5cdf0dc1df4fd06b83354f2d",
                        "effective_date": "2019-05-30T05:00:00.000Z",
                        "Pay_frequency": "Weekly",
                        "changeReason": "--",
                        "notes": "--"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": true,
                "assign_performance_management": true,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cdf0dc1df4fd06b83354f30"
            },
            "status": "active",
            "groups": [],
            "_id": "5cdf0dc1df4fd06b83354f2a",
            "email": "asdf@adfa.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "BB3Pypso",
            "createdAt": "2019-05-17T19:38:41.047Z",
            "updatedAt": "2019-05-28T06:50:45.260Z",
            "userId": 99,
            "leave_Eligibility_id": "5cecda45f9c29e149c54d8ae"
        },
        {
            "personal": {
                "name": {
                    "firstName": "vidya",
                    "middleName": "charan",
                    "lastName": "reddy",
                    "preferredName": "charan"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "hyd",
                    "zipcode": "56876",
                    "country": "hyd"
                },
                "contact": {
                    "workPhone": "8987654324",
                    "extention": "3456",
                    "mobilePhone": "9876543456",
                    "homePhone": "8765432345",
                    "workMail": "charan@outlook.com",
                    "personalMail": "charan@gmail.com"
                },
                "dob": "1993-03-03T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-04-10T18:30:00.000Z",
                "veteranStatus": "No Military Service",
                "ssn": "123456789765",
                "ethnicity": "Hispanic or Latino",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-23T04:53:50.181Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "16099579",
                "employeeType": "Full Time",
                "hireDate": "2018-07-04T18:30:00.000Z",
                "jobTitle": "Internship",
                "department": "Sales",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cdac017af231c1ca23a20c3",
                "VendorId": "1234565432",
                "HR_Contact": "5cd91e68f308c21c43e50559",
                "ReportsTo": "5cd93e4a8e9dea1f4bb987dc",
                "employment_status_history": [
                    {
                        "_id": "5ce6275ee2c6ab2b5061ae89",
                        "status": "Active",
                        "effective_date": "2019-05-23T04:53:50.181Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5ce6275ee2c6ab2b5061ae8a",
                        "jobTitle": "Internship",
                        "department": "Sales",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "ReportsTo": "5cd91cc3f308c21c43e5054d",
                        "effective_date": "2019-05-23T04:53:50.181Z"
                    }
                ]
            },
            "compensation": {
                "NonpaidPosition": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "high",
                "compensationRatio": "3",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 33
                        },
                        "_id": "5ce6275ee2c6ab2b5061ae8b",
                        "effective_date": "2019-03-18T18:30:00.000Z",
                        "Pay_frequency": "Weekly",
                        "changeReason": "--",
                        "notes": "--"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5ce6275ee2c6ab2b5061ae8e"
            },
            "status": "active",
            "groups": [],
            "_id": "5ce6275ee2c6ab2b5061ae88",
            "email": "charan@outlook.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "tLRpfc4l",
            "createdAt": "2019-05-23T04:53:50.218Z",
            "updatedAt": "2019-05-28T07:18:37.219Z",
            "userId": 138,
            "leave_Eligibility_id": "5cece0cd0ef791519fcd0120"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /leave-Eligibility-group/paginate Paginate
 * @apiName Paginate
 * @apiGroup Leave Eligibity Group
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"benefitType"  : ["Full Time","Retirees"],
	"page_no" : 1,
	"per_page" : 10
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "Status": "Success",
    "Message": "Successfully fetched the Employee List.",
    "EmployeeList": [
        {
            "personal": {
                "name": {
                    "firstName": "vipin",
                    "middleName": "M",
                    "lastName": "reddy",
                    "preferredName": "vipin"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "121222222",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1234567654",
                    "extention": "2345",
                    "mobilePhone": "2345654345",
                    "homePhone": "6543321234",
                    "workMail": "info@gmail.com",
                    "personalMail": "vipin.reddy@mtwlabs.com"
                },
                "dob": "1992-06-10T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-13T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "123321233",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Terminated",
                    "effective_date": "2019-05-07T18:30:00.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "1234",
                "employeeType": "Full Time",
                "hireDate": "2018-08-12T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Professionals",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "234532",
                "employment_status_history": [
                    {
                        "_id": "5cd91cc3f308c21c43e5054e",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:29:07.042Z",
                        "end_date": "2019-05-23T08:08:22.872Z"
                    },
                    {
                        "_id": "5ce654f6bee9b42bc9f15f31",
                        "status": "Active",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "end_date": "2019-05-23T08:34:58.834Z"
                    },
                    {
                        "_id": "5ce65b3220c97f2d35a2801d",
                        "status": "Terminated",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "end_date": "2019-05-24T02:01:22.631Z"
                    },
                    {
                        "_id": "5ce750723db9243152d77797",
                        "status": "Terminated",
                        "effective_date": "2019-05-07T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd91cc3f308c21c43e5054f",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:29:07.042Z"
                    }
                ],
                "eligibleForRehire": false,
                "last_day_of_work": "2019-05-14T18:30:00.000Z",
                "off_boarding_template_assigned": "5ce750723db9243152d7779b",
                "terminationDate": "2019-05-07T18:30:00.000Z",
                "terminationReason": "Conduct",
                "terminationType": "Voluntary",
                "termination_comments": "daksd"
            },
            "compensation": {
                "NonpaidPosition": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Semi-Monthly",
                "compensationRatio": "2",
                "Salary_Grade": "high",
                "compensation_history": [],
                "current_compensation": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cd91cc3f308c21c43e50552"
            },
            "status": "active",
            "groups": [
                "5cd922b7f308c21c43e50560",
                "5cdabfd9af231c1ca23a208a",
                "5cdac0aaaf231c1ca23a20fd",
                "5cdac212107c831d51cd05ee"
            ],
            "_id": "5cd91cc3f308c21c43e5054d",
            "email": "info@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "MV7Xx9MJ",
            "createdAt": "2019-05-13T07:29:07.060Z",
            "updatedAt": "2019-05-27T12:54:14.547Z",
            "userId": 88,
            "offboardedBy": "5c9e11894b1d726375b23057",
            "leave_Eligibility_id": "5cebddf60ef791519fcd00f7"
        },
        {
            "personal": {
                "name": {
                    "firstName": "raju",
                    "middleName": "king",
                    "lastName": "raju",
                    "preferredName": "raj"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "123456754",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1234567898",
                    "extention": "8765",
                    "mobilePhone": "9876543234",
                    "homePhone": "9876543212",
                    "workMail": "info@raju.com",
                    "personalMail": "raju@gmail.com"
                },
                "dob": "1997-07-15T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-05-13T18:30:00.000Z",
                "veteranStatus": "Married",
                "ssn": "1232123",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-13T07:32:36.683Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "123445",
                "employeeType": "Full Time",
                "hireDate": "2019-05-13T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Craft Workers",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "32223",
                "employment_status_history": [
                    {
                        "_id": "5cd91d94f308c21c43e50554",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:32:36.683Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd91d94f308c21c43e50555",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:32:36.683Z"
                    }
                ]
            },
            "compensation": {
                "payRate": {
                    "amount": 2
                },
                "NonpaidPosition": false,
                "Pay_group": "Group1",
                "Pay_frequency": "Weekly",
                "First_Pay_Date": "2019-05-14T18:30:00.000Z",
                "compensationRatio": "23",
                "Salary_Grade": "3g",
                "current_compensation": [],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cd91d94f308c21c43e50558"
            },
            "status": "active",
            "groups": [
                "5cd922b7f308c21c43e50560",
                "5cdabfd9af231c1ca23a208a",
                "5cdac017af231c1ca23a20c3",
                "5cdac0aaaf231c1ca23a20fd",
                "5cdac212107c831d51cd05ee"
            ],
            "_id": "5cd91d94f308c21c43e50553",
            "email": "info@raju.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "kqXfm2rX",
            "createdAt": "2019-05-13T07:32:36.693Z",
            "updatedAt": "2019-05-27T12:54:14.547Z",
            "userId": 89,
            "leave_Eligibility_id": "5cebddf60ef791519fcd00f7"
        },
        {
            "personal": {
                "name": {
                    "firstName": "sai",
                    "middleName": "r",
                    "lastName": "koth",
                    "preferredName": "sai"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "121123243",
                    "country": "India"
                },
                "contact": {
                    "workPhone": "2134564322",
                    "extention": "2342",
                    "mobilePhone": "2342344354",
                    "homePhone": "4234324546",
                    "workMail": "info@sai.com",
                    "personalMail": "sai@gmail.com"
                },
                "dob": "1992-02-23T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-05-06T18:30:00.000Z",
                "veteranStatus": "Married",
                "ssn": "2321321",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-13T07:36:08.077Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "123123",
                "employeeType": "Full Time",
                "hireDate": "2019-05-06T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Nick Location",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Non-Exempt",
                "EEO_Job_Category": "Administrative Support Workers",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "123123",
                "employment_status_history": [
                    {
                        "_id": "5cd91e68f308c21c43e5055a",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:36:08.077Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd91e68f308c21c43e5055b",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:36:08.077Z"
                    }
                ]
            },
            "compensation": {
                "payRate": {
                    "amount": 1
                },
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "First_Pay_Date": "1998-03-12T18:30:00.000Z",
                "compensationRatio": "1",
                "Salary_Grade": "high",
                "compensation_history": [],
                "current_compensation": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cd91e68f308c21c43e5055e"
            },
            "status": "active",
            "groups": [
                "5cdac017af231c1ca23a20c3"
            ],
            "_id": "5cd91e68f308c21c43e50559",
            "email": "info@sai.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "IWU0vANB",
            "createdAt": "2019-05-13T07:36:08.085Z",
            "updatedAt": "2019-05-27T12:54:14.547Z",
            "userId": 90,
            "leave_Eligibility_id": "5cebddf60ef791519fcd00f7"
        },
        {
            "personal": {
                "name": {
                    "firstName": "ravi",
                    "middleName": "kumar",
                    "lastName": "singam",
                    "preferredName": "ravi"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "223331232",
                    "country": "India"
                },
                "contact": {
                    "workPhone": "2353246273",
                    "extention": "9879",
                    "mobilePhone": "9876652334",
                    "homePhone": "6867823840",
                    "workMail": "vipin@gmail.com",
                    "personalMail": "info@ravi.com"
                },
                "dob": "1993-06-07T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2016-05-01T18:30:00.000Z",
                "veteranStatus": "Married",
                "ssn": "12321",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-13T07:56:03.886Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "99090",
                "employeeType": "Full Time",
                "hireDate": "2018-02-05T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Sales",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "1232",
                "employment_status_history": [
                    {
                        "_id": "5cd92313f308c21c43e5059a",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:56:03.886Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd92313f308c21c43e5059b",
                        "jobTitle": "Software Engineer",
                        "department": "Sales",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:56:03.886Z"
                    }
                ]
            },
            "compensation": {
                "payRate": {
                    "amount": 1
                },
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "First_Pay_Date": "2019-02-11T18:30:00.000Z",
                "compensationRatio": "1",
                "Salary_Grade": "low",
                "current_compensation": [],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cd92313f308c21c43e5059e"
            },
            "status": "active",
            "groups": [],
            "_id": "5cd92313f308c21c43e50599",
            "email": "vipin@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "EtmOV70m",
            "createdAt": "2019-05-13T07:56:03.905Z",
            "updatedAt": "2019-05-28T07:18:37.219Z",
            "userId": 91,
            "leave_Eligibility_id": "5cece0cd0ef791519fcd0120"
        },
        {
            "personal": {
                "name": {
                    "firstName": "asma",
                    "lastName": "a",
                    "middleName": "mubeen",
                    "preferredName": "asma"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "212212121",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1232189379",
                    "extention": "9732",
                    "mobilePhone": "2387434783",
                    "homePhone": "1231231233",
                    "workMail": "info@asma.com",
                    "personalMail": "asma@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1993-02-09T18:30:00.000Z",
                "gender": "female",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "2312312",
                "ethnicity": "Ethnicity11",
                "education": [
                    {
                        "_id": "5ce1525cc201f76fdcaab18b",
                        "university": "ddghdfgh",
                        "degree": "Bachelors",
                        "specialization": "IT",
                        "gpa": 5,
                        "start_date": "2019-05-23T18:30:00.000Z",
                        "end_date": "2019-06-28T18:30:00.000Z"
                    }
                ],
                "languages": [
                    {
                        "_id": "5ce1525cc201f76fdcaab18a",
                        "language": "Chinese",
                        "level_of_fluency": "Intermediate"
                    }
                ],
                "visa_information": [
                    {
                        "_id": "5ce1525cc201f76fdcaab189",
                        "visa_type": "EB-3",
                        "issuing_country": "India",
                        "issued_date": "2019-05-21T18:30:00.000Z",
                        "expiration_date": "2019-05-30T18:30:00.000Z",
                        "status": "Active",
                        "notes": "etwertwer"
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-13T09:52:10.102Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "99251",
                "employeeType": "Full Time",
                "hireDate": "2019-03-13T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "c edit loc",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Professionals",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "1111223",
                "employment_status_history": [
                    {
                        "_id": "5cd93e4a8e9dea1f4bb987dd",
                        "status": "Active",
                        "effective_date": "2019-05-13T09:52:10.102Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd93e4a8e9dea1f4bb987de",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T09:52:10.102Z"
                    }
                ]
            },
            "compensation": {
                "NonpaidPosition": false,
                "HighlyCompensatedEmployee": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "B",
                "compensationRatio": "23",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": null,
                            "unit": ""
                        },
                        "_id": "5ce1528ac201f76fdcaab18c",
                        "effective_date": null,
                        "payType": "",
                        "Pay_frequency": "",
                        "notes": "",
                        "changeReason": ""
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cd93e4a8e9dea1f4bb987e1"
            },
            "status": "active",
            "groups": [],
            "_id": "5cd93e4a8e9dea1f4bb987dc",
            "email": "info@asma.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "eQdrFU81",
            "createdAt": "2019-05-13T09:52:10.135Z",
            "updatedAt": "2019-05-28T06:50:45.260Z",
            "userId": 92,
            "leave_Eligibility_id": "5cecda45f9c29e149c54d8ae"
        },
        {
            "personal": {
                "name": {
                    "firstName": "Flor ",
                    "lastName": "Houchins",
                    "middleName": "Chantal",
                    "preferredName": "Flor"
                },
                "address": {
                    "street1": "khdfdjjsvb",
                    "street2": "kdfahdsoigi",
                    "city": "Manhattan",
                    "state": "NewYork",
                    "zipcode": "25332",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "39256649394",
                    "extention": "3264",
                    "mobilePhone": "237856549",
                    "homePhone": "36253532302",
                    "workMail": "flor@gmail.com",
                    "personalMail": "flor@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1992-01-06T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "12432361361246",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": [
                    {
                        "_id": "5ce42f0a1532171a8b5948e8",
                        "visa_type": "",
                        "issuing_country": "",
                        "issued_date": null,
                        "expiration_date": null,
                        "status": "",
                        "notes": ""
                    },
                    {
                        "_id": "5ce42f0a1532171a8b5948e7",
                        "visa_type": "",
                        "issuing_country": "",
                        "issued_date": null,
                        "expiration_date": null,
                        "status": "",
                        "notes": ""
                    },
                    {
                        "_id": "5ce42f0a1532171a8b5948e6",
                        "visa_type": "EB-1",
                        "issuing_country": "USA",
                        "issued_date": "2019-05-22T18:30:00.000Z",
                        "expiration_date": "2019-05-30T18:30:00.000Z",
                        "status": "Active",
                        "notes": "wefasdfasdfasd"
                    },
                    {
                        "_id": "5ce42f0a1532171a8b5948e5",
                        "visa_type": "EB-1",
                        "issuing_country": "USA",
                        "issued_date": "2019-05-22T18:30:00.000Z",
                        "expiration_date": "2019-05-30T18:30:00.000Z",
                        "status": "Active",
                        "notes": "wefasdfasdfasd"
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Leave",
                    "effective_date": "2019-05-17T03:05:56.342Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "99412",
                "employeeType": "Full Time",
                "hireDate": "2019-05-14T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "23565608395",
                "employment_status_history": [
                    {
                        "_id": "5cda377f51f0be25b3adceb5",
                        "status": "Active",
                        "effective_date": "2019-05-14T03:35:27.299Z"
                    },
                    {
                        "_id": "5cde251e7139a81c906b5344",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:05:56.342Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cda377f51f0be25b3adceb6",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-14T03:35:27.299Z"
                    }
                ]
            },
            "compensation": {
                "NonpaidPosition": false,
                "HighlyCompensatedEmployee": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "A",
                "compensationRatio": "233",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 123,
                            "unit": ""
                        },
                        "_id": "5cdbd2fd2d755332c3632a05",
                        "effective_date": "2019-05-25T18:30:00.000Z",
                        "payType": "Salary",
                        "Pay_frequency": "Weekly",
                        "notes": "asdasd",
                        "changeReason": "A-Promotion"
                    },
                    {
                        "payRate": {
                            "amount": 456,
                            "unit": ""
                        },
                        "_id": "5cdbd39b2d755332c3632a06",
                        "effective_date": "2019-05-30T08:52:45.000Z",
                        "payType": "Hourly",
                        "Pay_frequency": "Weekly",
                        "notes": "sdetrrewrt",
                        "changeReason": "A-Promotion"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": true,
                "assign_performance_management": true,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cda377f51f0be25b3adceba"
            },
            "status": "active",
            "groups": [],
            "_id": "5cda377f51f0be25b3adceb4",
            "email": "flor@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "396IgZZT",
            "createdAt": "2019-05-14T03:35:27.334Z",
            "updatedAt": "2019-05-28T06:50:45.260Z",
            "userId": 93,
            "leave_Eligibility_id": "5cecda45f9c29e149c54d8ae"
        },
        {
            "personal": {
                "name": {
                    "firstName": "Slyvia",
                    "middleName": "Marie",
                    "lastName": "Claus",
                    "preferredName": "Slyvia"
                },
                "address": {
                    "street1": "wejhtuhiothiefnvk",
                    "street2": "lfpg9tuyu9u9tjv",
                    "city": "Manhattan",
                    "state": "NewYork",
                    "zipcode": "465493",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "277935475934679",
                    "extention": "43564",
                    "mobilePhone": "74325443659034",
                    "homePhone": "4373469491",
                    "workMail": "Slyvia@gmail.com",
                    "personalMail": "Slyvia@gmail.com"
                },
                "dob": "1994-02-09T18:30:00.000Z",
                "gender": "female",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "37344245266623",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Terminated",
                    "effective_date": "2019-05-04T18:30:00.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "99573",
                "employeeType": "Full Time",
                "hireDate": "2019-05-14T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "325567950",
                "employment_status_history": [
                    {
                        "_id": "5cda3ad10e57b225e1240c9e",
                        "status": "Active",
                        "effective_date": "2019-05-14T03:49:37.209Z",
                        "end_date": "2019-05-23T09:02:59.816Z"
                    },
                    {
                        "_id": "5ce661c320c97f2d35a28022",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-14T18:30:00.000Z",
                        "end_date": "2019-05-23T13:01:10.788Z"
                    },
                    {
                        "_id": "5ce699964581392fcfc99164",
                        "status": "Terminated",
                        "effective_date": "2019-05-04T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cda3ad10e57b225e1240c9f",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-14T03:49:37.209Z"
                    }
                ],
                "eligibleForRehire": false,
                "last_day_of_work": "2019-05-05T18:30:00.000Z",
                "off_boarding_template_assigned": "5ce699964581392fcfc99168",
                "terminationDate": "2019-05-04T18:30:00.000Z",
                "terminationReason": "Attendance",
                "terminationType": "Voluntary",
                "termination_comments": "askjdksa"
            },
            "compensation": {
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "A",
                "compensationRatio": "23",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 18
                        },
                        "_id": "5cda3ad10e57b225e1240ca0",
                        "effective_date": "2019-05-19T18:30:00.000Z",
                        "Pay_frequency": "Monthly",
                        "changeReason": "--",
                        "notes": "--"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cda3ad10e57b225e1240ca3"
            },
            "status": "active",
            "groups": [],
            "_id": "5cda3ad10e57b225e1240c9d",
            "email": "Slyvia@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "Dt5ZZM8R",
            "createdAt": "2019-05-14T03:49:37.243Z",
            "updatedAt": "2019-05-27T12:54:14.547Z",
            "userId": 94,
            "offboardedBy": "5c9e11894b1d726375b23057",
            "leave_Eligibility_id": "5cebddf60ef791519fcd00f7"
        },
        {
            "personal": {
                "name": {
                    "firstName": "Lina",
                    "lastName": "Dunker",
                    "middleName": "James",
                    "preferredName": "Lina"
                },
                "address": {
                    "street1": "shdgioidvknkv",
                    "street2": "sdhiyfirnknckbk",
                    "city": "Manhattan",
                    "state": "NewYork",
                    "zipcode": "235365263",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "7745365555",
                    "extention": "3746",
                    "mobilePhone": "23652536649",
                    "homePhone": "233446546025",
                    "workMail": "lina@gmail.com",
                    "personalMail": "lina@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1995-06-07T18:30:00.000Z",
                "gender": "female",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "3655694769330",
                "ethnicity": "Ethnicity11",
                "education": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d2",
                        "university": "",
                        "degree": "",
                        "specialization": "",
                        "gpa": null,
                        "start_date": null,
                        "end_date": null
                    }
                ],
                "languages": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d1",
                        "language": "",
                        "level_of_fluency": "Native proficiency"
                    }
                ],
                "visa_information": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d0",
                        "visa_type": "",
                        "issuing_country": "",
                        "issued_date": null,
                        "expiration_date": null,
                        "status": "",
                        "notes": ""
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Terminated",
                    "effective_date": "2019-05-13T18:30:00.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "16099574",
                "employeeType": "Full Time",
                "hireDate": "2019-05-14T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "4346404",
                "employment_status_history": [
                    {
                        "_id": "5cda3d4a2fec3c261021993a",
                        "status": "Active",
                        "effective_date": "2019-05-14T04:00:10.410Z",
                        "end_date": "2019-05-17T03:23:07.896Z"
                    },
                    {
                        "_id": "5cde291b4cdb8624bcc28075",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:05:56.342Z",
                        "end_date": "2019-05-17T03:29:53.021Z"
                    },
                    {
                        "_id": "5cde2ab1a286b927accea5f0",
                        "status": "Active",
                        "effective_date": "2019-05-17T03:05:56.342Z",
                        "end_date": "2019-05-17T03:37:25.999Z"
                    },
                    {
                        "_id": "5cde2c765e2e562494b8187e",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:40:56.342Z",
                        "end_date": "2019-05-17T03:39:00.627Z"
                    },
                    {
                        "_id": "5cde2cd4e83d4e082c7ff95c",
                        "status": "Active",
                        "effective_date": "2019-05-17T04:40:56.342Z",
                        "end_date": "2019-05-17T03:41:08.613Z"
                    },
                    {
                        "_id": "5cde2d541352531830068e82",
                        "status": "Leave",
                        "effective_date": "2019-05-17T06:40:56.342Z",
                        "end_date": "2019-05-17T03:42:49.391Z"
                    },
                    {
                        "_id": "5cde2db9bb4f582cf8013065",
                        "status": "Active",
                        "effective_date": "2019-05-17T07:40:56.342Z",
                        "end_date": "2019-05-23T09:06:11.926Z"
                    },
                    {
                        "_id": "5ce6628320c97f2d35a2802b",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-14T18:30:00.000Z",
                        "end_date": "2019-05-23T09:51:37.374Z"
                    },
                    {
                        "_id": "5ce66d297732592f983702ac",
                        "status": "Terminated",
                        "effective_date": "2019-05-13T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cda3d4a2fec3c261021993b",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-14T04:00:10.410Z",
                        "end_date": "2019-05-17T04:43:15.819Z"
                    },
                    {
                        "_id": "5cde3be3f2285524c04b21e2",
                        "jobTitle": "Senior Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-17T04:05:44.350Z"
                    }
                ],
                "ReportsTo": "5cda377f51f0be25b3adceb4",
                "eligibleForRehire": false,
                "last_day_of_work": "2019-05-19T18:30:00.000Z",
                "off_boarding_template_assigned": "5ce66d297732592f983702b0",
                "terminationDate": "2019-05-13T18:30:00.000Z",
                "terminationReason": "Abandonment",
                "terminationType": "Voluntary",
                "termination_comments": "jhgjh"
            },
            "compensation": {
                "NonpaidPosition": true,
                "HighlyCompensatedEmployee": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "B",
                "compensationRatio": "60%",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 123456,
                            "unit": ""
                        },
                        "_id": "5cdb94f68ed19a301ececd18",
                        "Pay_frequency": "Bi-Weekly",
                        "changeReason": "A-Promotion",
                        "effective_date": "2019-05-21T11:56:01.000Z",
                        "notes": "dfghjdfgh",
                        "payType": "Test"
                    },
                    {
                        "payRate": {
                            "amount": 123,
                            "unit": ""
                        },
                        "_id": "5cdb96b98ed19a301ececd19",
                        "effective_date": "2019-05-25T18:30:00.000Z",
                        "payType": "Monthly",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "sdfgsdfg",
                        "changeReason": "A-Promotion"
                    },
                    {
                        "payRate": {
                            "amount": 678,
                            "unit": ""
                        },
                        "_id": "5cde972b3c5c5b67e353341b",
                        "effective_date": "2019-05-30T11:14:16.000Z",
                        "payType": "Test",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "sfdgsdfg",
                        "changeReason": "A-Promotion"
                    },
                    {
                        "payRate": {
                            "amount": 4563,
                            "unit": ""
                        },
                        "_id": "5cde9af23c5c5b67e353341f",
                        "effective_date": "2019-05-14T18:30:00.000Z",
                        "payType": "Hourly",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "ertetrte",
                        "changeReason": "A-Promotion"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": true,
                "assign_performance_management": true,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cda3d4a2fec3c261021993f"
            },
            "status": "active",
            "groups": [],
            "_id": "5cda3d4a2fec3c2610219939",
            "email": "lina@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "s9rI7Pt7",
            "createdAt": "2019-05-14T04:00:10.460Z",
            "updatedAt": "2019-05-28T06:50:54.107Z",
            "userId": 95,
            "offboardedBy": "5c9e11894b1d726375b23057",
            "leave_Eligibility_id": "5cecda4df9c29e149c54d8af"
        },
        {
            "personal": {
                "name": {
                    "firstName": "kabir",
                    "lastName": "arjun",
                    "middleName": "singh",
                    "preferredName": "Arjun",
                    "salutation": "Dr."
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "American Samoa",
                    "zipcode": "123456",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1234567876",
                    "extention": "1234",
                    "mobilePhone": "1234567876",
                    "homePhone": "1234567897",
                    "workMail": "kabir.singh@gmail.com",
                    "personalMail": "vipinarjun@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "2019-05-07T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "No Military Service",
                "race": "Asian",
                "ssn": "1234",
                "ethnicity": "Not Hispanic or Latino",
                "education": [
                    {
                        "_id": "5ce4da271532171a8b594962",
                        "university": "OSmania",
                        "degree": "Bachelors",
                        "specialization": "Electronics",
                        "gpa": 3,
                        "start_date": "2019-05-01T18:30:00.000Z",
                        "end_date": "2019-05-15T18:30:00.000Z"
                    },
                    {
                        "_id": "5ce4e36d1532171a8b5949f4",
                        "university": "JNTUH",
                        "degree": "Bachelors",
                        "specialization": "IT",
                        "gpa": 10,
                        "start_date": "2019-05-06T18:30:00.000Z",
                        "end_date": "2019-06-28T18:30:00.000Z"
                    }
                ],
                "languages": [
                    {
                        "_id": "5ce4d8eb1532171a8b594954",
                        "language": "Hindi",
                        "level_of_fluency": "Intermediate"
                    },
                    {
                        "_id": "5ce4d8eb1532171a8b594953",
                        "language": "Japanese",
                        "level_of_fluency": "Basic"
                    }
                ],
                "visa_information": [
                    {
                        "_id": "5ce4d8eb1532171a8b594952",
                        "visa_type": "EB-2",
                        "issuing_country": "USA",
                        "issued_date": "2019-05-22T18:30:00.000Z",
                        "expiration_date": "2019-05-24T18:30:00.000Z",
                        "status": "Active",
                        "notes": "ertyeryterty"
                    },
                    {
                        "_id": "5ce4d8eb1532171a8b594951",
                        "visa_type": "L-1",
                        "issuing_country": "India",
                        "issued_date": "2019-05-07T18:30:00.000Z",
                        "expiration_date": "2019-05-30T18:30:00.000Z",
                        "status": "Active",
                        "notes": "asdf"
                    },
                    {
                        "_id": "5ce4d8eb1532171a8b594950",
                        "visa_type": "H-1B",
                        "issuing_country": "USA",
                        "issued_date": "2019-05-06T18:30:00.000Z",
                        "expiration_date": "2019-07-19T18:30:00.000Z",
                        "status": "Expired",
                        "notes": ""
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Terminated",
                    "effective_date": "2019-05-06T18:30:00.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "16099576",
                "employeeType": "Full Time",
                "hireDate": "2019-05-07T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Craft Workers",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "1245",
                "employment_status_history": [
                    {
                        "_id": "5cdeb987c8b3566a2c99d451",
                        "status": "Active",
                        "effective_date": "2019-05-17T13:39:19.107Z",
                        "end_date": "2019-05-23T09:14:15.841Z"
                    },
                    {
                        "_id": "5ce6646720c97f2d35a28036",
                        "status": "Terminated",
                        "effective_date": "2019-05-06T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cdeb987c8b3566a2c99d452",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-17T13:39:19.107Z"
                    }
                ],
                "eligibleForRehire": true,
                "last_day_of_work": "2019-05-19T18:30:00.000Z",
                "off_boarding_template_assigned": "5ce6646720c97f2d35a2803a",
                "terminationDate": "2019-05-06T18:30:00.000Z",
                "terminationReason": "Sleeping in Office",
                "terminationType": "Involuntary",
                "termination_comments": "zxczxcxzc"
            },
            "compensation": {
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "AA",
                "compensationRatio": "1",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 2,
                            "unit": ""
                        },
                        "_id": "5cdeb987c8b3566a2c99d453",
                        "effective_date": "2019-05-07T18:30:00.000Z",
                        "Pay_frequency": "Weekly",
                        "changeReason": "Job Reclassification",
                        "notes": "dfghdfg",
                        "payType": "Test"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cdeb987c8b3566a2c99d456"
            },
            "status": "active",
            "groups": [],
            "_id": "5cdeb987c8b3566a2c99d450",
            "email": "kabir.singh@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "bgavZ4Md",
            "createdAt": "2019-05-17T13:39:19.131Z",
            "updatedAt": "2019-05-28T07:27:53.090Z",
            "userId": 97,
            "leave_Eligibility_id": "5cece2f86027a01268d7cf4c"
        },
        {
            "personal": {
                "name": {
                    "firstName": "Smith ",
                    "lastName": "ford",
                    "middleName": "Dunkun ",
                    "preferredName": "Smith"
                },
                "address": {
                    "street1": "Rugby street",
                    "street2": "lane 2",
                    "city": "Newyork",
                    "state": "NewYork",
                    "zipcode": "45675",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "7653456785",
                    "extention": "1234",
                    "mobilePhone": "7702265585",
                    "homePhone": "5566445678",
                    "workMail": "gopikrishna.annaldas@hotmail.com",
                    "personalMail": "gopikrishna.annaldas@hotmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1997-02-10T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-05-20T18:30:00.000Z",
                "veteranStatus": "Other Veteran",
                "ssn": "123-32-2343",
                "ethnicity": "Hispanic or Latino",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Leave of Absence",
                    "effective_date": "2019-05-06T12:36:46.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "16099577",
                "employeeType": "Full Time",
                "hireDate": "2019-05-21T18:30:00.000Z",
                "jobTitle": "Test",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Technicians",
                "Site_AccessRole": "5cdac017af231c1ca23a20c3",
                "VendorId": "123456",
                "employment_status_history": [
                    {
                        "_id": "5cdec1d5df4fd06b83354eb9",
                        "status": "Active",
                        "effective_date": "2019-05-17T14:14:45.767Z",
                        "end_date": "2019-05-21T12:25:08.108Z"
                    },
                    {
                        "_id": "5ce3ee241532171a8b5948cc",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-07T12:24:39.000Z",
                        "end_date": "2019-05-21T12:33:26.043Z"
                    },
                    {
                        "_id": "5ce3f0161532171a8b5948cd",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-12T12:33:04.000Z",
                        "end_date": "2019-05-21T12:34:08.513Z"
                    },
                    {
                        "_id": "5ce3f0401532171a8b5948ce",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-01T12:33:55.000Z",
                        "end_date": "2019-05-21T12:37:04.532Z"
                    },
                    {
                        "_id": "5ce3f0f01532171a8b5948cf",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-06T12:36:46.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cdec1d5df4fd06b83354eba",
                        "jobTitle": "Internship",
                        "department": "Sales",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-17T14:14:45.767Z",
                        "end_date": "2019-05-22T06:36:59.768Z"
                    },
                    {
                        "_id": "5ce4ee0b1532171a8b5949f6",
                        "jobTitle": "Internship",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "location": "Banglore",
                        "ReportsTo": "5cd91cc3f308c21c43e5054d",
                        "changeReason": "A-Promotion",
                        "remoteWork": "Yes",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "notes": "sample",
                        "end_date": "2019-05-22T06:37:45.600Z"
                    },
                    {
                        "_id": "5ce4ee391532171a8b594a00",
                        "jobTitle": "Test",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "location": "Banglore",
                        "ReportsTo": "5cd91cc3f308c21c43e5054d",
                        "changeReason": "A-Promotion",
                        "remoteWork": "Yes",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "notes": "sample"
                    }
                ],
                "ReportsTo": "5cdeb987c8b3566a2c99d450"
            },
            "compensation": {
                "NonpaidPosition": true,
                "HighlyCompensatedEmployee": true,
                "Pay_group": "Group1",
                "Pay_frequency": "Bi-Weekly",
                "Salary_Grade": "B",
                "compensationRatio": "30",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 557,
                            "unit": ""
                        },
                        "_id": "5cdfada6c201f76fdcaab181",
                        "effective_date": "2019-05-21T07:10:23.000Z",
                        "payType": "Test",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "tyutyui",
                        "changeReason": "A-Promotion"
                    },
                    {
                        "payRate": {
                            "amount": 890,
                            "unit": ""
                        },
                        "_id": "5cdfb31fc201f76fdcaab183",
                        "effective_date": "2019-05-30T07:23:26.000Z",
                        "payType": "Salary",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "zxcv",
                        "changeReason": "A-Promotion"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": true,
                "assign_performance_management": true,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cdec1d5df4fd06b83354ec9"
            },
            "status": "active",
            "groups": [],
            "_id": "5cdec1d5df4fd06b83354eb8",
            "email": "gopikrishna.annaldas@hotmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "zakLK1yh",
            "createdAt": "2019-05-17T14:14:45.801Z",
            "updatedAt": "2019-05-28T06:50:45.260Z",
            "userId": 98,
            "leave_Eligibility_id": "5cecda45f9c29e149c54d8ae"
        }
    ],
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /leave-Eligibility-group/paginate Paginate
 * @apiName Paginate
 * @apiGroup Leave Eligibity Group
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"benefitType"  : ["Full Time","Retirees"],
	"page_no" : 1,
	"per_page" : 10
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "Status": "Success",
    "Message": "Successfully fetched the Employee List.",
    "EmployeeList": [
        {
            "personal": {
                "name": {
                    "firstName": "vipin",
                    "middleName": "M",
                    "lastName": "reddy",
                    "preferredName": "vipin"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "121222222",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1234567654",
                    "extention": "2345",
                    "mobilePhone": "2345654345",
                    "homePhone": "6543321234",
                    "workMail": "info@gmail.com",
                    "personalMail": "vipin.reddy@mtwlabs.com"
                },
                "dob": "1992-06-10T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-13T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "123321233",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Terminated",
                    "effective_date": "2019-05-07T18:30:00.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "1234",
                "employeeType": "Full Time",
                "hireDate": "2018-08-12T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Professionals",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "234532",
                "employment_status_history": [
                    {
                        "_id": "5cd91cc3f308c21c43e5054e",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:29:07.042Z",
                        "end_date": "2019-05-23T08:08:22.872Z"
                    },
                    {
                        "_id": "5ce654f6bee9b42bc9f15f31",
                        "status": "Active",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "end_date": "2019-05-23T08:34:58.834Z"
                    },
                    {
                        "_id": "5ce65b3220c97f2d35a2801d",
                        "status": "Terminated",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "end_date": "2019-05-24T02:01:22.631Z"
                    },
                    {
                        "_id": "5ce750723db9243152d77797",
                        "status": "Terminated",
                        "effective_date": "2019-05-07T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd91cc3f308c21c43e5054f",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:29:07.042Z"
                    }
                ],
                "eligibleForRehire": false,
                "last_day_of_work": "2019-05-14T18:30:00.000Z",
                "off_boarding_template_assigned": "5ce750723db9243152d7779b",
                "terminationDate": "2019-05-07T18:30:00.000Z",
                "terminationReason": "Conduct",
                "terminationType": "Voluntary",
                "termination_comments": "daksd"
            },
            "compensation": {
                "NonpaidPosition": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Semi-Monthly",
                "compensationRatio": "2",
                "Salary_Grade": "high",
                "compensation_history": [],
                "current_compensation": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cd91cc3f308c21c43e50552"
            },
            "status": "active",
            "groups": [
                "5cd922b7f308c21c43e50560",
                "5cdabfd9af231c1ca23a208a",
                "5cdac0aaaf231c1ca23a20fd",
                "5cdac212107c831d51cd05ee"
            ],
            "_id": "5cd91cc3f308c21c43e5054d",
            "email": "info@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "MV7Xx9MJ",
            "createdAt": "2019-05-13T07:29:07.060Z",
            "updatedAt": "2019-05-27T12:54:14.547Z",
            "userId": 88,
            "offboardedBy": "5c9e11894b1d726375b23057",
            "leave_Eligibility_id": "5cebddf60ef791519fcd00f7"
        },
        {
            "personal": {
                "name": {
                    "firstName": "raju",
                    "middleName": "king",
                    "lastName": "raju",
                    "preferredName": "raj"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "123456754",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1234567898",
                    "extention": "8765",
                    "mobilePhone": "9876543234",
                    "homePhone": "9876543212",
                    "workMail": "info@raju.com",
                    "personalMail": "raju@gmail.com"
                },
                "dob": "1997-07-15T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-05-13T18:30:00.000Z",
                "veteranStatus": "Married",
                "ssn": "1232123",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-13T07:32:36.683Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "123445",
                "employeeType": "Full Time",
                "hireDate": "2019-05-13T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Craft Workers",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "32223",
                "employment_status_history": [
                    {
                        "_id": "5cd91d94f308c21c43e50554",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:32:36.683Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd91d94f308c21c43e50555",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:32:36.683Z"
                    }
                ]
            },
            "compensation": {
                "payRate": {
                    "amount": 2
                },
                "NonpaidPosition": false,
                "Pay_group": "Group1",
                "Pay_frequency": "Weekly",
                "First_Pay_Date": "2019-05-14T18:30:00.000Z",
                "compensationRatio": "23",
                "Salary_Grade": "3g",
                "current_compensation": [],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cd91d94f308c21c43e50558"
            },
            "status": "active",
            "groups": [
                "5cd922b7f308c21c43e50560",
                "5cdabfd9af231c1ca23a208a",
                "5cdac017af231c1ca23a20c3",
                "5cdac0aaaf231c1ca23a20fd",
                "5cdac212107c831d51cd05ee"
            ],
            "_id": "5cd91d94f308c21c43e50553",
            "email": "info@raju.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "kqXfm2rX",
            "createdAt": "2019-05-13T07:32:36.693Z",
            "updatedAt": "2019-05-27T12:54:14.547Z",
            "userId": 89,
            "leave_Eligibility_id": "5cebddf60ef791519fcd00f7"
        },
        {
            "personal": {
                "name": {
                    "firstName": "sai",
                    "middleName": "r",
                    "lastName": "koth",
                    "preferredName": "sai"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "121123243",
                    "country": "India"
                },
                "contact": {
                    "workPhone": "2134564322",
                    "extention": "2342",
                    "mobilePhone": "2342344354",
                    "homePhone": "4234324546",
                    "workMail": "info@sai.com",
                    "personalMail": "sai@gmail.com"
                },
                "dob": "1992-02-23T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-05-06T18:30:00.000Z",
                "veteranStatus": "Married",
                "ssn": "2321321",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-13T07:36:08.077Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "123123",
                "employeeType": "Full Time",
                "hireDate": "2019-05-06T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Nick Location",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Non-Exempt",
                "EEO_Job_Category": "Administrative Support Workers",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "123123",
                "employment_status_history": [
                    {
                        "_id": "5cd91e68f308c21c43e5055a",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:36:08.077Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd91e68f308c21c43e5055b",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:36:08.077Z"
                    }
                ]
            },
            "compensation": {
                "payRate": {
                    "amount": 1
                },
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "First_Pay_Date": "1998-03-12T18:30:00.000Z",
                "compensationRatio": "1",
                "Salary_Grade": "high",
                "compensation_history": [],
                "current_compensation": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cd91e68f308c21c43e5055e"
            },
            "status": "active",
            "groups": [
                "5cdac017af231c1ca23a20c3"
            ],
            "_id": "5cd91e68f308c21c43e50559",
            "email": "info@sai.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "IWU0vANB",
            "createdAt": "2019-05-13T07:36:08.085Z",
            "updatedAt": "2019-05-27T12:54:14.547Z",
            "userId": 90,
            "leave_Eligibility_id": "5cebddf60ef791519fcd00f7"
        },
        {
            "personal": {
                "name": {
                    "firstName": "ravi",
                    "middleName": "kumar",
                    "lastName": "singam",
                    "preferredName": "ravi"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "223331232",
                    "country": "India"
                },
                "contact": {
                    "workPhone": "2353246273",
                    "extention": "9879",
                    "mobilePhone": "9876652334",
                    "homePhone": "6867823840",
                    "workMail": "vipin@gmail.com",
                    "personalMail": "info@ravi.com"
                },
                "dob": "1993-06-07T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2016-05-01T18:30:00.000Z",
                "veteranStatus": "Married",
                "ssn": "12321",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-13T07:56:03.886Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "99090",
                "employeeType": "Full Time",
                "hireDate": "2018-02-05T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Sales",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "1232",
                "employment_status_history": [
                    {
                        "_id": "5cd92313f308c21c43e5059a",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:56:03.886Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd92313f308c21c43e5059b",
                        "jobTitle": "Software Engineer",
                        "department": "Sales",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:56:03.886Z"
                    }
                ]
            },
            "compensation": {
                "payRate": {
                    "amount": 1
                },
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "First_Pay_Date": "2019-02-11T18:30:00.000Z",
                "compensationRatio": "1",
                "Salary_Grade": "low",
                "current_compensation": [],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cd92313f308c21c43e5059e"
            },
            "status": "active",
            "groups": [],
            "_id": "5cd92313f308c21c43e50599",
            "email": "vipin@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "EtmOV70m",
            "createdAt": "2019-05-13T07:56:03.905Z",
            "updatedAt": "2019-05-28T07:18:37.219Z",
            "userId": 91,
            "leave_Eligibility_id": "5cece0cd0ef791519fcd0120"
        },
        {
            "personal": {
                "name": {
                    "firstName": "asma",
                    "lastName": "a",
                    "middleName": "mubeen",
                    "preferredName": "asma"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "212212121",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1232189379",
                    "extention": "9732",
                    "mobilePhone": "2387434783",
                    "homePhone": "1231231233",
                    "workMail": "info@asma.com",
                    "personalMail": "asma@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1993-02-09T18:30:00.000Z",
                "gender": "female",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "2312312",
                "ethnicity": "Ethnicity11",
                "education": [
                    {
                        "_id": "5ce1525cc201f76fdcaab18b",
                        "university": "ddghdfgh",
                        "degree": "Bachelors",
                        "specialization": "IT",
                        "gpa": 5,
                        "start_date": "2019-05-23T18:30:00.000Z",
                        "end_date": "2019-06-28T18:30:00.000Z"
                    }
                ],
                "languages": [
                    {
                        "_id": "5ce1525cc201f76fdcaab18a",
                        "language": "Chinese",
                        "level_of_fluency": "Intermediate"
                    }
                ],
                "visa_information": [
                    {
                        "_id": "5ce1525cc201f76fdcaab189",
                        "visa_type": "EB-3",
                        "issuing_country": "India",
                        "issued_date": "2019-05-21T18:30:00.000Z",
                        "expiration_date": "2019-05-30T18:30:00.000Z",
                        "status": "Active",
                        "notes": "etwertwer"
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-13T09:52:10.102Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "99251",
                "employeeType": "Full Time",
                "hireDate": "2019-03-13T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "c edit loc",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Professionals",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "1111223",
                "employment_status_history": [
                    {
                        "_id": "5cd93e4a8e9dea1f4bb987dd",
                        "status": "Active",
                        "effective_date": "2019-05-13T09:52:10.102Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd93e4a8e9dea1f4bb987de",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T09:52:10.102Z"
                    }
                ]
            },
            "compensation": {
                "NonpaidPosition": false,
                "HighlyCompensatedEmployee": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "B",
                "compensationRatio": "23",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": null,
                            "unit": ""
                        },
                        "_id": "5ce1528ac201f76fdcaab18c",
                        "effective_date": null,
                        "payType": "",
                        "Pay_frequency": "",
                        "notes": "",
                        "changeReason": ""
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cd93e4a8e9dea1f4bb987e1"
            },
            "status": "active",
            "groups": [],
            "_id": "5cd93e4a8e9dea1f4bb987dc",
            "email": "info@asma.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "eQdrFU81",
            "createdAt": "2019-05-13T09:52:10.135Z",
            "updatedAt": "2019-05-28T06:50:45.260Z",
            "userId": 92,
            "leave_Eligibility_id": "5cecda45f9c29e149c54d8ae"
        },
        {
            "personal": {
                "name": {
                    "firstName": "Flor ",
                    "lastName": "Houchins",
                    "middleName": "Chantal",
                    "preferredName": "Flor"
                },
                "address": {
                    "street1": "khdfdjjsvb",
                    "street2": "kdfahdsoigi",
                    "city": "Manhattan",
                    "state": "NewYork",
                    "zipcode": "25332",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "39256649394",
                    "extention": "3264",
                    "mobilePhone": "237856549",
                    "homePhone": "36253532302",
                    "workMail": "flor@gmail.com",
                    "personalMail": "flor@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1992-01-06T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "12432361361246",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": [
                    {
                        "_id": "5ce42f0a1532171a8b5948e8",
                        "visa_type": "",
                        "issuing_country": "",
                        "issued_date": null,
                        "expiration_date": null,
                        "status": "",
                        "notes": ""
                    },
                    {
                        "_id": "5ce42f0a1532171a8b5948e7",
                        "visa_type": "",
                        "issuing_country": "",
                        "issued_date": null,
                        "expiration_date": null,
                        "status": "",
                        "notes": ""
                    },
                    {
                        "_id": "5ce42f0a1532171a8b5948e6",
                        "visa_type": "EB-1",
                        "issuing_country": "USA",
                        "issued_date": "2019-05-22T18:30:00.000Z",
                        "expiration_date": "2019-05-30T18:30:00.000Z",
                        "status": "Active",
                        "notes": "wefasdfasdfasd"
                    },
                    {
                        "_id": "5ce42f0a1532171a8b5948e5",
                        "visa_type": "EB-1",
                        "issuing_country": "USA",
                        "issued_date": "2019-05-22T18:30:00.000Z",
                        "expiration_date": "2019-05-30T18:30:00.000Z",
                        "status": "Active",
                        "notes": "wefasdfasdfasd"
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Leave",
                    "effective_date": "2019-05-17T03:05:56.342Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "99412",
                "employeeType": "Full Time",
                "hireDate": "2019-05-14T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "23565608395",
                "employment_status_history": [
                    {
                        "_id": "5cda377f51f0be25b3adceb5",
                        "status": "Active",
                        "effective_date": "2019-05-14T03:35:27.299Z"
                    },
                    {
                        "_id": "5cde251e7139a81c906b5344",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:05:56.342Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cda377f51f0be25b3adceb6",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-14T03:35:27.299Z"
                    }
                ]
            },
            "compensation": {
                "NonpaidPosition": false,
                "HighlyCompensatedEmployee": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "A",
                "compensationRatio": "233",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 123,
                            "unit": ""
                        },
                        "_id": "5cdbd2fd2d755332c3632a05",
                        "effective_date": "2019-05-25T18:30:00.000Z",
                        "payType": "Salary",
                        "Pay_frequency": "Weekly",
                        "notes": "asdasd",
                        "changeReason": "A-Promotion"
                    },
                    {
                        "payRate": {
                            "amount": 456,
                            "unit": ""
                        },
                        "_id": "5cdbd39b2d755332c3632a06",
                        "effective_date": "2019-05-30T08:52:45.000Z",
                        "payType": "Hourly",
                        "Pay_frequency": "Weekly",
                        "notes": "sdetrrewrt",
                        "changeReason": "A-Promotion"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": true,
                "assign_performance_management": true,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cda377f51f0be25b3adceba"
            },
            "status": "active",
            "groups": [],
            "_id": "5cda377f51f0be25b3adceb4",
            "email": "flor@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "396IgZZT",
            "createdAt": "2019-05-14T03:35:27.334Z",
            "updatedAt": "2019-05-28T06:50:45.260Z",
            "userId": 93,
            "leave_Eligibility_id": "5cecda45f9c29e149c54d8ae"
        },
        {
            "personal": {
                "name": {
                    "firstName": "Slyvia",
                    "middleName": "Marie",
                    "lastName": "Claus",
                    "preferredName": "Slyvia"
                },
                "address": {
                    "street1": "wejhtuhiothiefnvk",
                    "street2": "lfpg9tuyu9u9tjv",
                    "city": "Manhattan",
                    "state": "NewYork",
                    "zipcode": "465493",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "277935475934679",
                    "extention": "43564",
                    "mobilePhone": "74325443659034",
                    "homePhone": "4373469491",
                    "workMail": "Slyvia@gmail.com",
                    "personalMail": "Slyvia@gmail.com"
                },
                "dob": "1994-02-09T18:30:00.000Z",
                "gender": "female",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "37344245266623",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Terminated",
                    "effective_date": "2019-05-04T18:30:00.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "99573",
                "employeeType": "Full Time",
                "hireDate": "2019-05-14T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "325567950",
                "employment_status_history": [
                    {
                        "_id": "5cda3ad10e57b225e1240c9e",
                        "status": "Active",
                        "effective_date": "2019-05-14T03:49:37.209Z",
                        "end_date": "2019-05-23T09:02:59.816Z"
                    },
                    {
                        "_id": "5ce661c320c97f2d35a28022",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-14T18:30:00.000Z",
                        "end_date": "2019-05-23T13:01:10.788Z"
                    },
                    {
                        "_id": "5ce699964581392fcfc99164",
                        "status": "Terminated",
                        "effective_date": "2019-05-04T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cda3ad10e57b225e1240c9f",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-14T03:49:37.209Z"
                    }
                ],
                "eligibleForRehire": false,
                "last_day_of_work": "2019-05-05T18:30:00.000Z",
                "off_boarding_template_assigned": "5ce699964581392fcfc99168",
                "terminationDate": "2019-05-04T18:30:00.000Z",
                "terminationReason": "Attendance",
                "terminationType": "Voluntary",
                "termination_comments": "askjdksa"
            },
            "compensation": {
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "A",
                "compensationRatio": "23",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 18
                        },
                        "_id": "5cda3ad10e57b225e1240ca0",
                        "effective_date": "2019-05-19T18:30:00.000Z",
                        "Pay_frequency": "Monthly",
                        "changeReason": "--",
                        "notes": "--"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cda3ad10e57b225e1240ca3"
            },
            "status": "active",
            "groups": [],
            "_id": "5cda3ad10e57b225e1240c9d",
            "email": "Slyvia@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "Dt5ZZM8R",
            "createdAt": "2019-05-14T03:49:37.243Z",
            "updatedAt": "2019-05-27T12:54:14.547Z",
            "userId": 94,
            "offboardedBy": "5c9e11894b1d726375b23057",
            "leave_Eligibility_id": "5cebddf60ef791519fcd00f7"
        },
        {
            "personal": {
                "name": {
                    "firstName": "Lina",
                    "lastName": "Dunker",
                    "middleName": "James",
                    "preferredName": "Lina"
                },
                "address": {
                    "street1": "shdgioidvknkv",
                    "street2": "sdhiyfirnknckbk",
                    "city": "Manhattan",
                    "state": "NewYork",
                    "zipcode": "235365263",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "7745365555",
                    "extention": "3746",
                    "mobilePhone": "23652536649",
                    "homePhone": "233446546025",
                    "workMail": "lina@gmail.com",
                    "personalMail": "lina@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1995-06-07T18:30:00.000Z",
                "gender": "female",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "3655694769330",
                "ethnicity": "Ethnicity11",
                "education": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d2",
                        "university": "",
                        "degree": "",
                        "specialization": "",
                        "gpa": null,
                        "start_date": null,
                        "end_date": null
                    }
                ],
                "languages": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d1",
                        "language": "",
                        "level_of_fluency": "Native proficiency"
                    }
                ],
                "visa_information": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d0",
                        "visa_type": "",
                        "issuing_country": "",
                        "issued_date": null,
                        "expiration_date": null,
                        "status": "",
                        "notes": ""
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Terminated",
                    "effective_date": "2019-05-13T18:30:00.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "16099574",
                "employeeType": "Full Time",
                "hireDate": "2019-05-14T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "4346404",
                "employment_status_history": [
                    {
                        "_id": "5cda3d4a2fec3c261021993a",
                        "status": "Active",
                        "effective_date": "2019-05-14T04:00:10.410Z",
                        "end_date": "2019-05-17T03:23:07.896Z"
                    },
                    {
                        "_id": "5cde291b4cdb8624bcc28075",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:05:56.342Z",
                        "end_date": "2019-05-17T03:29:53.021Z"
                    },
                    {
                        "_id": "5cde2ab1a286b927accea5f0",
                        "status": "Active",
                        "effective_date": "2019-05-17T03:05:56.342Z",
                        "end_date": "2019-05-17T03:37:25.999Z"
                    },
                    {
                        "_id": "5cde2c765e2e562494b8187e",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:40:56.342Z",
                        "end_date": "2019-05-17T03:39:00.627Z"
                    },
                    {
                        "_id": "5cde2cd4e83d4e082c7ff95c",
                        "status": "Active",
                        "effective_date": "2019-05-17T04:40:56.342Z",
                        "end_date": "2019-05-17T03:41:08.613Z"
                    },
                    {
                        "_id": "5cde2d541352531830068e82",
                        "status": "Leave",
                        "effective_date": "2019-05-17T06:40:56.342Z",
                        "end_date": "2019-05-17T03:42:49.391Z"
                    },
                    {
                        "_id": "5cde2db9bb4f582cf8013065",
                        "status": "Active",
                        "effective_date": "2019-05-17T07:40:56.342Z",
                        "end_date": "2019-05-23T09:06:11.926Z"
                    },
                    {
                        "_id": "5ce6628320c97f2d35a2802b",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-14T18:30:00.000Z",
                        "end_date": "2019-05-23T09:51:37.374Z"
                    },
                    {
                        "_id": "5ce66d297732592f983702ac",
                        "status": "Terminated",
                        "effective_date": "2019-05-13T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cda3d4a2fec3c261021993b",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-14T04:00:10.410Z",
                        "end_date": "2019-05-17T04:43:15.819Z"
                    },
                    {
                        "_id": "5cde3be3f2285524c04b21e2",
                        "jobTitle": "Senior Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-17T04:05:44.350Z"
                    }
                ],
                "ReportsTo": "5cda377f51f0be25b3adceb4",
                "eligibleForRehire": false,
                "last_day_of_work": "2019-05-19T18:30:00.000Z",
                "off_boarding_template_assigned": "5ce66d297732592f983702b0",
                "terminationDate": "2019-05-13T18:30:00.000Z",
                "terminationReason": "Abandonment",
                "terminationType": "Voluntary",
                "termination_comments": "jhgjh"
            },
            "compensation": {
                "NonpaidPosition": true,
                "HighlyCompensatedEmployee": true,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "B",
                "compensationRatio": "60%",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 123456,
                            "unit": ""
                        },
                        "_id": "5cdb94f68ed19a301ececd18",
                        "Pay_frequency": "Bi-Weekly",
                        "changeReason": "A-Promotion",
                        "effective_date": "2019-05-21T11:56:01.000Z",
                        "notes": "dfghjdfgh",
                        "payType": "Test"
                    },
                    {
                        "payRate": {
                            "amount": 123,
                            "unit": ""
                        },
                        "_id": "5cdb96b98ed19a301ececd19",
                        "effective_date": "2019-05-25T18:30:00.000Z",
                        "payType": "Monthly",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "sdfgsdfg",
                        "changeReason": "A-Promotion"
                    },
                    {
                        "payRate": {
                            "amount": 678,
                            "unit": ""
                        },
                        "_id": "5cde972b3c5c5b67e353341b",
                        "effective_date": "2019-05-30T11:14:16.000Z",
                        "payType": "Test",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "sfdgsdfg",
                        "changeReason": "A-Promotion"
                    },
                    {
                        "payRate": {
                            "amount": 4563,
                            "unit": ""
                        },
                        "_id": "5cde9af23c5c5b67e353341f",
                        "effective_date": "2019-05-14T18:30:00.000Z",
                        "payType": "Hourly",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "ertetrte",
                        "changeReason": "A-Promotion"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": true,
                "assign_performance_management": true,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cda3d4a2fec3c261021993f"
            },
            "status": "active",
            "groups": [],
            "_id": "5cda3d4a2fec3c2610219939",
            "email": "lina@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "s9rI7Pt7",
            "createdAt": "2019-05-14T04:00:10.460Z",
            "updatedAt": "2019-05-28T06:50:54.107Z",
            "userId": 95,
            "offboardedBy": "5c9e11894b1d726375b23057",
            "leave_Eligibility_id": "5cecda4df9c29e149c54d8af"
        },
        {
            "personal": {
                "name": {
                    "firstName": "kabir",
                    "lastName": "arjun",
                    "middleName": "singh",
                    "preferredName": "Arjun",
                    "salutation": "Dr."
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "American Samoa",
                    "zipcode": "123456",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1234567876",
                    "extention": "1234",
                    "mobilePhone": "1234567876",
                    "homePhone": "1234567897",
                    "workMail": "kabir.singh@gmail.com",
                    "personalMail": "vipinarjun@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "2019-05-07T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "No Military Service",
                "race": "Asian",
                "ssn": "1234",
                "ethnicity": "Not Hispanic or Latino",
                "education": [
                    {
                        "_id": "5ce4da271532171a8b594962",
                        "university": "OSmania",
                        "degree": "Bachelors",
                        "specialization": "Electronics",
                        "gpa": 3,
                        "start_date": "2019-05-01T18:30:00.000Z",
                        "end_date": "2019-05-15T18:30:00.000Z"
                    },
                    {
                        "_id": "5ce4e36d1532171a8b5949f4",
                        "university": "JNTUH",
                        "degree": "Bachelors",
                        "specialization": "IT",
                        "gpa": 10,
                        "start_date": "2019-05-06T18:30:00.000Z",
                        "end_date": "2019-06-28T18:30:00.000Z"
                    }
                ],
                "languages": [
                    {
                        "_id": "5ce4d8eb1532171a8b594954",
                        "language": "Hindi",
                        "level_of_fluency": "Intermediate"
                    },
                    {
                        "_id": "5ce4d8eb1532171a8b594953",
                        "language": "Japanese",
                        "level_of_fluency": "Basic"
                    }
                ],
                "visa_information": [
                    {
                        "_id": "5ce4d8eb1532171a8b594952",
                        "visa_type": "EB-2",
                        "issuing_country": "USA",
                        "issued_date": "2019-05-22T18:30:00.000Z",
                        "expiration_date": "2019-05-24T18:30:00.000Z",
                        "status": "Active",
                        "notes": "ertyeryterty"
                    },
                    {
                        "_id": "5ce4d8eb1532171a8b594951",
                        "visa_type": "L-1",
                        "issuing_country": "India",
                        "issued_date": "2019-05-07T18:30:00.000Z",
                        "expiration_date": "2019-05-30T18:30:00.000Z",
                        "status": "Active",
                        "notes": "asdf"
                    },
                    {
                        "_id": "5ce4d8eb1532171a8b594950",
                        "visa_type": "H-1B",
                        "issuing_country": "USA",
                        "issued_date": "2019-05-06T18:30:00.000Z",
                        "expiration_date": "2019-07-19T18:30:00.000Z",
                        "status": "Expired",
                        "notes": ""
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Terminated",
                    "effective_date": "2019-05-06T18:30:00.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "16099576",
                "employeeType": "Full Time",
                "hireDate": "2019-05-07T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Craft Workers",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "1245",
                "employment_status_history": [
                    {
                        "_id": "5cdeb987c8b3566a2c99d451",
                        "status": "Active",
                        "effective_date": "2019-05-17T13:39:19.107Z",
                        "end_date": "2019-05-23T09:14:15.841Z"
                    },
                    {
                        "_id": "5ce6646720c97f2d35a28036",
                        "status": "Terminated",
                        "effective_date": "2019-05-06T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cdeb987c8b3566a2c99d452",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-17T13:39:19.107Z"
                    }
                ],
                "eligibleForRehire": true,
                "last_day_of_work": "2019-05-19T18:30:00.000Z",
                "off_boarding_template_assigned": "5ce6646720c97f2d35a2803a",
                "terminationDate": "2019-05-06T18:30:00.000Z",
                "terminationReason": "Sleeping in Office",
                "terminationType": "Involuntary",
                "termination_comments": "zxczxcxzc"
            },
            "compensation": {
                "NonpaidPosition": false,
                "Pay_group": "Group2",
                "Pay_frequency": "Weekly",
                "Salary_Grade": "AA",
                "compensationRatio": "1",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 2,
                            "unit": ""
                        },
                        "_id": "5cdeb987c8b3566a2c99d453",
                        "effective_date": "2019-05-07T18:30:00.000Z",
                        "Pay_frequency": "Weekly",
                        "changeReason": "Job Reclassification",
                        "notes": "dfghdfg",
                        "payType": "Test"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": false,
                "assign_leave_management_rules": false,
                "assign_performance_management": false,
                "assaign_employee_number": "System Generated",
                "onboarding_template": "5cdeb987c8b3566a2c99d456"
            },
            "status": "active",
            "groups": [],
            "_id": "5cdeb987c8b3566a2c99d450",
            "email": "kabir.singh@gmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "bgavZ4Md",
            "createdAt": "2019-05-17T13:39:19.131Z",
            "updatedAt": "2019-05-28T07:27:53.090Z",
            "userId": 97,
            "leave_Eligibility_id": "5cece2f86027a01268d7cf4c"
        },
        {
            "personal": {
                "name": {
                    "firstName": "Smith ",
                    "lastName": "ford",
                    "middleName": "Dunkun ",
                    "preferredName": "Smith"
                },
                "address": {
                    "street1": "Rugby street",
                    "street2": "lane 2",
                    "city": "Newyork",
                    "state": "NewYork",
                    "zipcode": "45675",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "7653456785",
                    "extention": "1234",
                    "mobilePhone": "7702265585",
                    "homePhone": "5566445678",
                    "workMail": "gopikrishna.annaldas@hotmail.com",
                    "personalMail": "gopikrishna.annaldas@hotmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "dob": "1997-02-10T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-05-20T18:30:00.000Z",
                "veteranStatus": "Other Veteran",
                "ssn": "123-32-2343",
                "ethnicity": "Hispanic or Latino",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Leave of Absence",
                    "effective_date": "2019-05-06T12:36:46.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "16099577",
                "employeeType": "Full Time",
                "hireDate": "2019-05-21T18:30:00.000Z",
                "jobTitle": "Test",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Technicians",
                "Site_AccessRole": "5cdac017af231c1ca23a20c3",
                "VendorId": "123456",
                "employment_status_history": [
                    {
                        "_id": "5cdec1d5df4fd06b83354eb9",
                        "status": "Active",
                        "effective_date": "2019-05-17T14:14:45.767Z",
                        "end_date": "2019-05-21T12:25:08.108Z"
                    },
                    {
                        "_id": "5ce3ee241532171a8b5948cc",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-07T12:24:39.000Z",
                        "end_date": "2019-05-21T12:33:26.043Z"
                    },
                    {
                        "_id": "5ce3f0161532171a8b5948cd",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-12T12:33:04.000Z",
                        "end_date": "2019-05-21T12:34:08.513Z"
                    },
                    {
                        "_id": "5ce3f0401532171a8b5948ce",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-01T12:33:55.000Z",
                        "end_date": "2019-05-21T12:37:04.532Z"
                    },
                    {
                        "_id": "5ce3f0f01532171a8b5948cf",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-06T12:36:46.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cdec1d5df4fd06b83354eba",
                        "jobTitle": "Internship",
                        "department": "Sales",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-17T14:14:45.767Z",
                        "end_date": "2019-05-22T06:36:59.768Z"
                    },
                    {
                        "_id": "5ce4ee0b1532171a8b5949f6",
                        "jobTitle": "Internship",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "location": "Banglore",
                        "ReportsTo": "5cd91cc3f308c21c43e5054d",
                        "changeReason": "A-Promotion",
                        "remoteWork": "Yes",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "notes": "sample",
                        "end_date": "2019-05-22T06:37:45.600Z"
                    },
                    {
                        "_id": "5ce4ee391532171a8b594a00",
                        "jobTitle": "Test",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "location": "Banglore",
                        "ReportsTo": "5cd91cc3f308c21c43e5054d",
                        "changeReason": "A-Promotion",
                        "remoteWork": "Yes",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "notes": "sample"
                    }
                ],
                "ReportsTo": "5cdeb987c8b3566a2c99d450"
            },
            "compensation": {
                "NonpaidPosition": true,
                "HighlyCompensatedEmployee": true,
                "Pay_group": "Group1",
                "Pay_frequency": "Bi-Weekly",
                "Salary_Grade": "B",
                "compensationRatio": "30",
                "current_compensation": [
                    {
                        "payRate": {
                            "amount": 557,
                            "unit": ""
                        },
                        "_id": "5cdfada6c201f76fdcaab181",
                        "effective_date": "2019-05-21T07:10:23.000Z",
                        "payType": "Test",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "tyutyui",
                        "changeReason": "A-Promotion"
                    },
                    {
                        "payRate": {
                            "amount": 890,
                            "unit": ""
                        },
                        "_id": "5cdfb31fc201f76fdcaab183",
                        "effective_date": "2019-05-30T07:23:26.000Z",
                        "payType": "Salary",
                        "Pay_frequency": "Bi-Weekly",
                        "notes": "zxcv",
                        "changeReason": "A-Promotion"
                    }
                ],
                "compensation_history": []
            },
            "module_settings": {
                "eligible_for_benifits": true,
                "assign_leave_management_rules": true,
                "assign_performance_management": true,
                "assaign_employee_number": "Manual",
                "onboarding_template": "5cdec1d5df4fd06b83354ec9"
            },
            "status": "active",
            "groups": [],
            "_id": "5cdec1d5df4fd06b83354eb8",
            "email": "gopikrishna.annaldas@hotmail.com",
            "companyId": "5c9e11894b1d726375b23058",
            "type": "employee",
            "password": "zakLK1yh",
            "createdAt": "2019-05-17T14:14:45.801Z",
            "updatedAt": "2019-05-28T06:50:45.260Z",
            "userId": 98,
            "leave_Eligibility_id": "5cecda45f9c29e149c54d8ae"
        }
    ],
    "count": 12
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /leave-Eligibility-group/uploadxlsx Upload Xlsx
 * @apiName Upload Xlsx
 * @apiGroup Leave Eligibity Group
 * 
 * 
 * @apiHeader {String} x-access-token User login token.

 * @apiParam {file} file file.xlsx
 * @apiParam {companyId} _id CompanyID
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Successfully Added 1 Records"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

 /**
 * @api {post} /leave-Eligibility-group/exportexcel Export Excel
 * @apiName Export Excel
 * @apiGroup Leave Eligibity Group
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "data": [
        {
            "empId": "2432534",
            "empName": "Alex",
            "groupName": "Team Titans",
            "effectiveDate": "2019-03-29(yyyy-mm-dd)"
        }
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */