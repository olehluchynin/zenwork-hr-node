let express = require('express');

let router = express.Router();

var leaveEligibiltyController = require('./controller');

let authMiddleware = require('../../middlewares/auth');

let validationsMiddleware = require('../../middlewares/validations');

var multipart = require('connect-multiparty');

var multipartMiddleware = multipart();

router.post(
    '/create',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    leaveEligibiltyController.createEligibilityGroup
);

router.get(
    '/get/:_id',
    authMiddleware.isUserLogin,
    leaveEligibiltyController.getEligibilityGroup
);

router.get(
    '/list',
    authMiddleware.isUserLogin,
    leaveEligibiltyController.listLeaveEligibilityGroups
);

router.post(
    '/delete',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    leaveEligibiltyController.deleteEligibilityGroups
);

router.put(
    '/update',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    leaveEligibiltyController.updateEligibilityGroup
);

// router.post('/getemplist',leaveEligibiltyController.getEmployeeList);
// router.post('/paginate',leaveEligibiltyController.employeelistpaginate);
// router.post('/uploadxlsx',multipartMiddleware,leaveEligibiltyController.uploadxlsx);
// router.post('/exportexcel',leaveEligibiltyController.exportExcel);

module.exports = router;