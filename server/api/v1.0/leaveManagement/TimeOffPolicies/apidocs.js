 /**
 * @api {post} /timeOffPolicies/createTierLevels CreateTierLevels
 * @apiName CreateTierLevels
 * @apiGroup TimeOffPolicies
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "dateForServiceLengthCalculation": "Hire Date",
    "tierLevelEntryPoint": "Beginning of Pay Cycle",
    "tierLevels": [
        {
            "level": 1,
            "serviceLength": {
                "length": 5,
                "unit": "Years"
            },
            "hoursEarnedMax": 40,
            "hoursEarnedPerCycle": 5,
            "carryOverAllowed": true,
            "carryOverMax": 20
        },
        {
            "level": 2,
            "serviceLength": {
                "length": 10,
                "unit": "Years"
            },
            "hoursEarnedMax": 54,
            "hoursEarnedPerCycle": 6,
            "carryOverAllowed": true,
            "carryOverMax": 30
        }
    ]
}

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully created tier levels",
    "data": {
        "_id": "5d7f296af35f261c3ca87464",
        "dateForServiceLengthCalculation": "Hire Date",
        "tierLevelEntryPoint": "Beginning of Pay Cycle",
        "numberOfTierLevels": 2,
        "tierLevels": [
            {
                "_id": "5d7f296af35f261c3ca87466",
                "level": 1,
                "serviceLength": {
                    "length": 5,
                    "unit": "Years"
                },
                "hoursEarnedMax": 40,
                "hoursEarnedPerCycle": 5,
                "carryOverAllowed": true,
                "carryOverMax": 20
            },
            {
                "_id": "5d7f296af35f261c3ca87465",
                "level": 2,
                "serviceLength": {
                    "length": 10,
                    "unit": "Years"
                },
                "hoursEarnedMax": 54,
                "hoursEarnedPerCycle": 6,
                "carryOverAllowed": true,
                "carryOverMax": 30
            }
        ],
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-09-16T06:19:22.041Z",
        "updatedAt": "2019-09-16T06:19:22.041Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {get} /timeOffPolicies/getTierLevels/:_id GetTierLevels
 * @apiName GetTierLevels
 * @apiGroup TimeOffPolicies
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} _id TierLevels unique _id

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched tier levels",
    "data": {
        "_id": "5d7f296af35f261c3ca87464",
        "dateForServiceLengthCalculation": "Hire Date",
        "tierLevelEntryPoint": "Beginning of Pay Cycle",
        "numberOfTierLevels": 2,
        "tierLevels": [
            {
                "_id": "5d7f296af35f261c3ca87466",
                "level": 1,
                "serviceLength": {
                    "length": 5,
                    "unit": "Years"
                },
                "hoursEarnedMax": 40,
                "hoursEarnedPerCycle": 5,
                "carryOverAllowed": true,
                "carryOverMax": 20
            },
            {
                "_id": "5d7f296af35f261c3ca87465",
                "level": 2,
                "serviceLength": {
                    "length": 10,
                    "unit": "Years"
                },
                "hoursEarnedMax": 54,
                "hoursEarnedPerCycle": 6,
                "carryOverAllowed": true,
                "carryOverMax": 30
            }
        ],
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-09-16T06:19:22.041Z",
        "updatedAt": "2019-09-16T06:19:22.041Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {post} /timeOffPolicies/createpolicy CreatePolicy
 * @apiName CreatePolicy
 * @apiGroup TimeOffPolicies
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
  "selectall_Eligibilitygroup" : true,
  "companyId" : "5cc936f98b746b2564532c16",
  "leave_Eligibility_group": ["Full Time","Part Time"],
  "policytype": "Sick",
  "policy_effective_date": "2019-06-28T06:29:57.924Z",
  "employee_count" : 20,
  "employee_policy_period": {
    "new_hire": {
      "hire_date": {
        "selected": false,
        "date": "2019-06-28T06:29:57.924Z"
      },
      "first_month": {
        "selected": true,
        "following_DOH": true,
        "after_days": {
          "selected": true,
          "days": "25",
          "timerbased": "Weekly"
        }
      },
      "following_DOT": {
        "selected": true,
        "days": "90",
        "timerbased": "Day(s)"
      },
        "others" : false
    },
    "life_event": {
      "hire_date": {
        "selected": false,
        "date": "2019-06-28T06:29:57.924Z"
      },
      "first_month": {
        "selected": true,
        "following_DOH": true,
        "after_days": {
          "selected": true,
          "days": "25",
          "timerbased": "Weekly"
        }
      },
      "following_DOT": {
        "selected": true,
        "days": "90",
        "timerbased": "Day(s)"
      },
        "others" : false
    },
    "employee_usage": {
      "policyterm": "Number of Days",
      "policytermdays": "15"
    }
  },
  "time_earned": {
    "timeearned": "Each pay period",
    "pay_period_freq": "Weekly",
    "dayofweek": "Monday",
    "dateofMonth": "First Day of Month",
    "dayofmonth": "Last Day of Month",
    "tireslevel": {
      "yearsofservices": true,
      "tier_level_id": ["5cee30600d4d232aec6c9caa"]
    },
    "negativebalance": true,
    "neg_max_hrs": "-200"
  },
  "expirationoptions": {
    "expiretime": true,
    "expireoptions": "Expiration Date",
    "expiredate": "2019-06-28T06:29:57.924Z"
  }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "Status": true,
    "Message": "Successfully inserted the data.",
    "data": {
        "employee_policy_period": {
            "new_hire": {
                "hire_date": {
                    "selected": false,
                    "date": "2019-06-28T06:29:57.924Z"
                },
                "first_month": {
                    "after_days": {
                        "days": "25",
                        "timerbased": "Weekly"
                    },
                    "selected": true,
                    "following_DOH": true
                },
                "following_DOT": {
                    "days": "90",
                    "timerbased": "Day(s)"
                },
                "others": false
            },
            "employee_usage": {
                "policyterm": "Number of Days",
                "policytermdays": "15"
            }
        },
        "time_earned": {
            "tireslevel": {
                "tier_level_id": [
                    "5cee30600d4d232aec6c9caa"
                ],
                "yearsofservices": true
            },
            "timeearned": "Each pay period",
            "pay_period_freq": "Weekly",
            "dayofweek": "Monday",
            "dateofMonth": "First Day of Month",
            "dayofmonth": "Last Day of Month",
            "negativebalance": true,
            "neg_max_hrs": "-200"
        },
        "leave_Eligibility_group": [
            "Full Time",
            "Part Time"
        ],
        "_id": "5cee30a60d4d232aec6c9cad",
        "selectall_Eligibilitygroup": true,
        "companyId": "5cc936f98b746b2564532c16",
        "employee_count" : 20,
        "policytype": "Sick",
        "policy_effective_date": "2019-06-28T06:29:57.924Z",
        "expirationoptions": {
            "expiretime": true,
            "expireoptions": "Expiration Date",
            "expiredate": "2019-06-28T06:29:57.924Z"
        },
        "createdAt": "2019-05-29T07:11:34.264Z",
        "updatedAt": "2019-05-29T07:11:34.264Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */




 /**
 * @api {get} /timeOffPolicies/getpolicydetails/:companyId GetPolicy
 * @apiName GetPolicy
 * @apiGroup TimeOffPolicies
 * 
 * @apiHeader {String} x-access-token User login token.

 * @apiParam {ObjectId} company_id Company unique _id

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "Status": true,
    "data": [
        {
            "time_earned": {
                "tireslevel": {
                    "tier_level_id": [
                        "5cee30600d4d232aec6c9caa"
                    ],
                    "yearsofservices": true
                },
                "timeearned": "Each pay period",
                "pay_period_freq": "Weekly",
                "dayofweek": "Monday",
                "dateofMonth": "First Day of Month",
                "dayofmonth": "Last Day of Month",
                "negativebalance": true,
                "neg_max_hrs": "-200"
            },
            "expirationoptions": {
                "expiretime": true,
                "expireoptions": "Expiration Date",
                "expiredate": "2019-06-28T06:29:57.924Z"
            },
            "leave_Eligibility_group": [
                "Full Time"
            ],
            "policytype": "Jury Duty",
            "policy_effective_date": "2019-06-28T06:29:57.924Z"
        },
        {
            "time_earned": {
                "tireslevel": {
                    "tier_level_id": [
                        "5cee30600d4d232aec6c9caa"
                    ],
                    "yearsofservices": true
                },
                "timeearned": "Each pay period",
                "pay_period_freq": "Weekly",
                "dayofweek": "Monday",
                "dateofMonth": "First Day of Month",
                "dayofmonth": "Last Day of Month",
                "negativebalance": true,
                "neg_max_hrs": "-200"
            },
            "expirationoptions": {
                "expiretime": true,
                "expireoptions": "Expiration Date",
                "expiredate": "2019-06-28T06:29:57.924Z"
            },
            "leave_Eligibility_group": [
                "Full Time"
            ],
            "policytype": "Sick",
            "policy_effective_date": "2019-06-28T06:29:57.924Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */



 /**
 * @api {put} /timeOffPolicies/updateTierLevels UpdateTierLevels
 * @apiName UpdateTierLevels
 * @apiGroup TimeOffPolicies
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "_id": "5d7f296af35f261c3ca87464",
    "dateForServiceLengthCalculation": "Hire Date",
    "tierLevelEntryPoint": "Beginning of Pay Cycle",
    "tierLevels": [
        {
            "_id": "5d7f296af35f261c3ca87466",
            "level": 1,
            "serviceLength": {
                "length": 5,
                "unit": "Years"
            },
            "hoursEarnedMax": 40,
            "hoursEarnedPerCycle": 5,
            "carryOverAllowed": true,
            "carryOverMax": 20
        },
        {
            "_id": "5d7f296af35f261c3ca87465",
            "level": 2,
            "serviceLength": {
                "length": 10,
                "unit": "Years"
            },
            "hoursEarnedMax": 54,
            "hoursEarnedPerCycle": 6,
            "carryOverAllowed": true,
            "carryOverMax": 30
        }
    ],
    "companyId": "5c9e11894b1d726375b23058",
    "createdAt": "2019-09-16T06:19:22.041Z",
    "updatedAt": "2019-09-16T06:19:22.041Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully updated tier levels",
    "data": {
        "_id": "5d7f296af35f261c3ca87464",
        "dateForServiceLengthCalculation": "Hire Date",
        "tierLevelEntryPoint": "Beginning of Pay Cycle",
        "numberOfTierLevels": 2,
        "tierLevels": [
            {
                "serviceLength": {
                    "length": 5,
                    "unit": "Years"
                },
                "_id": "5d7f296af35f261c3ca87466",
                "level": 1,
                "hoursEarnedMax": 40,
                "hoursEarnedPerCycle": 5,
                "carryOverAllowed": true,
                "carryOverMax": 20
            },
            {
                "serviceLength": {
                    "length": 10,
                    "unit": "Years"
                },
                "_id": "5d7f296af35f261c3ca87465",
                "level": 2,
                "hoursEarnedMax": 54,
                "hoursEarnedPerCycle": 6,
                "carryOverAllowed": true,
                "carryOverMax": 30
            }
        ],
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-09-16T06:19:22.041Z",
        "updatedAt": "2019-09-16T09:53:14.891Z"
    }
}

 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /timeOffPolicies/deleteTierLevels DeleteTierLevels
 * @apiName DeleteTierLevels
 * @apiGroup TimeOffPolicies
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"_id": "5d7f296af35f261c3ca87464",
	"levelIds": [
		"5d7f296af35f261c3ca87466",
		"5d7f296af35f261c3ca87465"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully deleted levels",
    "data": {
        "_id": "5d7f296af35f261c3ca87464",
        "dateForServiceLengthCalculation": "Hire Date",
        "tierLevelEntryPoint": "Beginning of Pay Cycle",
        "tierLevels": [],
        "companyId": "5c9e11894b1d726375b23058",
        "createdAt": "2019-09-16T06:19:22.041Z",
        "updatedAt": "2019-09-16T08:56:30.781Z"
    }
}

 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {get} /timeOffPolicies/get/:_id Get
 * @apiName Get
 * @apiGroup TimeOffPolicies
 * 
 * @apiHeader {String} x-access-token User login token.

 * @apiParam {ObjectId} _id Id unique _id

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched the Time Policy Data.",
    "policyData": {
        "employee_policy_period": {
            "new_hire": {
                "hire_date": {
                    "selected": false,
                    "date": "2019-06-28T06:29:57.924Z"
                },
                "first_month": {
                    "after_days": {
                        "days": "25",
                        "timerbased": "Weekly"
                    },
                    "selected": true,
                    "following_DOH": true
                },
                "following_DOT": {
                    "days": "90",
                    "timerbased": "Day(s)"
                },
                "others": false
            },
            "employee_usage": {
                "policyterm": "Number of Days",
                "policytermdays": "15"
            }
        },
        "time_earned": {
            "tireslevel": {
                "tier_level_id": [
                    "5cee30600d4d232aec6c9caa"
                ],
                "yearsofservices": true
            },
            "timeearned": "Each pay period",
            "pay_period_freq": "Weekly",
            "dayofweek": "Monday",
            "dateofMonth": "First Day of Month",
            "dayofmonth": "Last Day of Month",
            "negativebalance": true,
            "neg_max_hrs": "-200"
        },
        "expirationoptions": {
            "expiretime": true,
            "expireoptions": "Expiration Date",
            "expiredate": "2019-06-28T06:29:57.924Z"
        },
        "leave_Eligibility_group": [
            "Full Time"
        ],
        "_id": "5cee65f20e79d1262cce49ad",
        "selectall_Eligibilitygroup": true,
        "companyId": "5cc936f98b746b2564532c16",
        "policytype": "Jury Duty",
        "policy_effective_date": "2019-06-28T06:29:57.924Z",
        "employee_count": 20,
        "createdAt": "2019-05-29T10:58:58.124Z",
        "updatedAt": "2019-05-29T10:58:58.124Z"
    },
    "tierLevelData": [
        {
            "_id": "5cee30600d4d232aec6c9caa",
            "datetype_Of_Servicecalc": "Hire Date",
            "tierLevel_Entry": "Beginnning of Pay cycle",
            "TierLevel_Ln": "2",
            "companyId": "5cc936f98b746b2564532c16",
            "tierLevel": [
                {
                    "serviceLength": {
                        "date": "5",
                        "duration": "Years"
                    },
                    "_id": "5cee30600d4d232aec6c9cac",
                    "level": "Level 1",
                    "hoursEarnedMax": "40:00",
                    "hoursEarnedPerCycle": "05:00",
                    "carryOverAllowed": "Yes",
                    "carryOverMax": "20"
                },
                {
                    "serviceLength": {
                        "date": "20",
                        "duration": "Months"
                    },
                    "_id": "5cee30600d4d232aec6c9cab",
                    "level": "Level 2",
                    "hoursEarnedMax": "80:00",
                    "hoursEarnedPerCycle": "10:00",
                    "carryOverAllowed": "Yes",
                    "carryOverMax": "20"
                }
            ],
            "createdAt": "2019-05-29T07:10:24.686Z",
            "updatedAt": "2019-05-29T07:10:24.686Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /timeOffPolicies/updatetimepolicy UpdateTimePolicy
 * @apiName UpdateTimePolicy
 * @apiGroup TimeOffPolicies
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
  "_id": "5cee65f20e79d1262cce49ad",
  "params": {
    "selectall_Eligibilitygroup": true,
    "companyId": "5cc936f98b746b2564532c16",
    "leave_Eligibility_group": [
      "Full Time","Part Time"
    ],
    "policytype": "Vacation",
    "policy_effective_date": "2019-06-28T06:29:57.924Z",
    "employee_count": 20,
    "employee_policy_period": {
      "new_hire": {
        "hire_date": {
          "selected": false,
          "date": "2019-06-28T06:29:57.924Z"
        },
        "first_month": {
          "selected": true,
          "following_DOH": true,
          "after_days": {
            "days": "25",
            "timerbased": "Weekly"
          }
        },
        "following_DOT": {
          "days": "90",
          "timerbased": "Day(s)"
        },
        "others": false
      },
      "employee_usage": {
        "policyterm": "Number of Days",
        "policytermdays": "15"
      }
    },
    "time_earned": {
      "timeearned": "Each pay period",
      "pay_period_freq": "Weekly",
      "dayofweek": "Monday",
      "dateofMonth": "First Day of Month",
      "dayofmonth": "Last Day of Month",
      "tireslevel": {
        "yearsofservices": true,
        "tier_level_id": [
          "5cee30600d4d232aec6c9caa"
        ]
      },
      "negativebalance": true,
      "neg_max_hrs": "-200"
    },
    "expirationoptions": {
      "expiretime": true,
      "expireoptions": "Expiration Date",
      "expiredate": "2019-06-28T06:29:57.924Z"
    }
  }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully updated the Time Off Policy.",
    "data": {
        "employee_policy_period": {
            "new_hire": {
                "hire_date": {
                    "selected": false,
                    "date": "2019-06-28T06:29:57.924Z"
                },
                "first_month": {
                    "after_days": {
                        "days": "25",
                        "timerbased": "Weekly"
                    },
                    "selected": true,
                    "following_DOH": true
                },
                "following_DOT": {
                    "days": "90",
                    "timerbased": "Day(s)"
                },
                "others": false
            },
            "employee_usage": {
                "policyterm": "Number of Days",
                "policytermdays": "15"
            }
        },
        "time_earned": {
            "tireslevel": {
                "tier_level_id": [
                    "5cee30600d4d232aec6c9caa"
                ],
                "yearsofservices": true
            },
            "timeearned": "Each pay period",
            "pay_period_freq": "Weekly",
            "dayofweek": "Monday",
            "dateofMonth": "First Day of Month",
            "dayofmonth": "Last Day of Month",
            "negativebalance": true,
            "neg_max_hrs": "-200"
        },
        "expirationoptions": {
            "expiretime": true,
            "expireoptions": "Expiration Date",
            "expiredate": "2019-06-28T06:29:57.924Z"
        },
        "leave_Eligibility_group": [
            "Full Time"
        ],
        "_id": "5cee65f20e79d1262cce49ad",
        "selectall_Eligibilitygroup": true,
        "companyId": "5cc936f98b746b2564532c16",
        "policytype": "Jury Duty",
        "policy_effective_date": "2019-06-28T06:29:57.924Z",
        "employee_count": 20,
        "createdAt": "2019-05-29T10:58:58.124Z",
        "updatedAt": "2019-05-29T10:58:58.124Z"
    }
}

 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /timeOffPolicies/delete Delete
 * @apiName Delete
 * @apiGroup TimeOffPolicies
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"_id" : ["5cf0eb78b056d110f0d4efb5"]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully deleted the Time Off Policy Data.",
    "data": {
        "n": 1,
        "ok": 1
    }
}

 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

 /**
 * @api {post} /timeOffPolicies/upload Import Xlsx
 * @apiName Import Xlsx
 * @apiGroup TimeOffPolicies
 * 
 * 
 * @apiHeader {String} x-access-token User login token.

 * @apiParam {file} file file.xlsx
 * @apiParam {companyId} companyId CompanyID
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Successfully Added 1 Records"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

 /**
 * @api {post} /leave-Eligibility-group/exportexcel Export Excel
 * @apiName Export Excel
 * @apiGroup TimeOffPolicies
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "data": [
        {
            "EmployeeID": "2432534",
            "EmployeeName": "Alex",
            "PolicyName": "Sick",
            "AccuralBalance" : "40.00.00",
            "effectiveDate": "2019-03-29(yyyy-mm-dd)",
            "TierLevel" : "level 1"
        }
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */