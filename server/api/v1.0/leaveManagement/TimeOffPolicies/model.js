let mongoose = require('mongoose');
let ObjectID = mongoose.Schema.ObjectId;

let timeOffPoliciesSchema = new mongoose.Schema({
    policyType: String,
    policyEffectiveDate: Date,
    // leaveEligibilityGroups: [{type: ObjectID, ref:'leave_eligility_groups'}],
    companyId: {type: ObjectID, ref:'companies', required: true},

    policyWaitingPeriod: {
        newHire: {
            selection: {type: String, enum: ['Date of hire', 'First of month', 'Time following date of hire', 'Other'], required: true},
            firstOfMonthSelectionDetails: {
                followingDateOfHire: {type: Boolean},
                duration: {
                    length : {type: Number},
                    unit : {type: String, enum: ['Years', 'Months', 'Days']}
                }
            },
            timeFollowingDateOfHireDetails: {
                length : {type: Number},
                unit : {type: String, enum: ['Years', 'Months', 'Days']}
            }
        },
        lifeEvent: {
            selection: {type: String, enum: ['Date of event', 'First of month', 'Time following date of event', 'Other'], required: true},
            firstOfMonthSelectionDetails: {
                followingDateOfEvent: {type: Boolean},
                duration: {
                    length : {type: Number},
                    unit : {type: String, enum: ['Years', 'Months', 'Days']}
                }
            },
            timeFollowingDateOfEventDetails: {
                length : {type: Number},
                unit : {type: String, enum: ['Years', 'Months', 'Days']}
            }
        }
    },

    employeeUsage: {
        selection: {type: String, enum: ['Immediately', 'Number of Days', 'Number of Months'], required: true},
        length: { type: Number, required: true }
    },
    
    timeEarned: {type: String, enum: ['January 1st', 'Each pay period', 'Each year', 'Over X intervals each year', 'For each hour worked']},
    payFrequency: {type: String},
    dayofweek: String,
    dateofMonth: String,
    dayofmonth: String,

    useTierLevelsBasedOnYearsOfService: {type: Boolean},
    tierLevelsId: {type: ObjectID, ref: 'tier_levels'},
    
    negativeBalance: {type: Boolean},
    negativeBalanceMaxHrs: {type: Number},
    
    expires: Boolean,
    expirationOptions: {type: String, enum: ['December 31st', 'Expiration Date']},
    expirationDate : {type: Date},
    
    // import_flag: Boolean,
    // accural_balance : String,
    // tier_level_import_id : Array,
    // userId: {type: ObjectID, ref:'users'},
}, {
    timestamps: true,
    versionKey: false
});

let tierLevelsSchema = mongoose.Schema({
    dateForServiceLengthCalculation : {type: String, enum: ['Hire Date', 'RehireDate', 'Custom'], required: true},
    tierLevelEntryPoint : {type: String, enum: ['Beginning of Pay Cycle', 'End of Pay Cycle', 'Manual Adjustment'], required: true},
    // numberOfTierLevels : {type: Number, required: true},
    companyId: {type: ObjectID, ref:'companies', required: true},
    tierLevels : [{
        level : {type: Number, required: true},
        serviceLength : {
            length : {type: Number, required: true},
            unit : {type: String, enum: ['Years', 'Months', 'Days'], required: true}
        },
        hoursEarnedMax : {type: Number, required: true},
        hoursEarnedPerCycle : {type: Number, required: true},
        carryOverAllowed : {type: Boolean, required: true},
        carryOverMax : {type: Number, required: true},
    }]
}, {
    timestamps: true,
    versionKey: false
});



let timeOffPoliciesModel = mongoose.model("time_off_policies", timeOffPoliciesSchema, 'time_off_policies');
let tierLevelsModel = mongoose.model("tier_levels", tierLevelsSchema, 'tier_levels');

module.exports = {
    timeOffPoliciesModel,
    tierLevelsModel
};