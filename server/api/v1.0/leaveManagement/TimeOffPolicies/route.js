let express = require('express');

let router = express.Router();

var timePolicyController = require('./controller');

let authMiddleware = require('../../middlewares/auth');

let validationsMiddleware = require('../../middlewares/validations');

var multipart = require('connect-multiparty');

var multipartMiddleware = multipart();

router.post(
    '/createTierLevels',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    timePolicyController.createTierLevels
);

router.get(
    '/getTierLevels/:_id',
    authMiddleware.isUserLogin,
    timePolicyController.getTierLevels
);

router.post(
    '/deleteTierLevels',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    timePolicyController.deleteTierLevels
);

router.put(
    '/updateTierLevels',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    timePolicyController.updateTierLevels
);

router.post('/createpolicy',timePolicyController.createTimePolicies);

router.get('/getpolicydetails/:company_id',timePolicyController.getPolicyDetails);



router.get('/get/:_id',timePolicyController.getPolicy);
router.post('/updatetimepolicy',timePolicyController.updateTimeOffPolicy);
router.post('/delete',timePolicyController.deletePolicyAndTier);
router.post('/upload',multipartMiddleware,timePolicyController.importXlsx);

module.exports = router;