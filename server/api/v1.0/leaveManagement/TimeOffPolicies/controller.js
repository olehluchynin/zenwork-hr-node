let mongoose = require('mongoose');
let moment = require('moment');
let ObjectID = mongoose.Types.ObjectId;

var xlsx = require('node-xlsx');

var json2xls = require('json2xls');
var fs = require('fs');


let Model = require('./model');
let timeOffPoliciesModel = Model.timeOffPoliciesModel;
let tierLevelsModel = Model.tierLevelsModel;
let userModel = require('../../users/model');

/**
*@author Asma
*@date 13/09/2019 17:46
Method to create tier levels
*/
let createTierLevels = (req, res) => {
    let body = req.body;
    let companyId = req.jwt.companyId
    body.companyId = companyId
    return new Promise((resolve, reject) => {
        // if (!body.numberOfTierLevels) reject('numberOfTierLevels required')
        if (!body.tierLevels) reject('tierLevels array is required')
        if (!body.tierLevels.length) reject('tierLevels array is required')
        // if (body.numberOfTierLevels != body.tierLevels.length) reject('tierLevels array length does not match number of tier levels')
        resolve(null);
    })
    .then(() => {
        tierLevelsModel.create(body)
            .then(tierLevels => {
                res.status(200).json({
                    status: true,
                    message: "Successfully created tier levels",
                    data: tierLevels
                })
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({
                    status: false,
                    message: "Error while creating tier levels",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
};

/**
*@author Asma
*@date 16/09/2019 12:25
Method to get tier levels
*/
let getTierLevels = (req, res) => {
    let companyId = req.jwt.companyId;
    let tierLevelsId = req.params._id;
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        tierLevelsModel.findOne({companyId: companyId, _id: ObjectID(tierLevelsId)}).lean().exec()
            .then(tierLevels => {
                if (tierLevels) {
                    res.status(200).json({
                        status: true,
                        message: "Successfully fetched tier levels",
                        data: tierLevels
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "Tier levels not found"
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({
                    status: false,
                    message: "Error while fetching tier levels",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(500).json({
            status: false,
            message: "Error while fetching tier levels",
            error: err
        })
    })
}

/**
*@author Asma
*@date 16/09/2019 12:54
Method to delete tier levels
*/
let deleteTierLevels = (req, res) => {
    let body = req.body;
    let _id = body._id;
    let companyId = req.jwt.companyId
    let levelIds = body.levelIds;
    return new Promise((resolve, reject) => {
        if (!body._id) reject('_id is required')
        if (!body.levelIds) reject('levelIds are required')
        if (!body.levelIds.length) reject('levelIds are required')
        resolve(null);
    })
    .then(() => {
        tierLevelsModel.findOneAndUpdate({companyId: companyId, _id: ObjectID(_id)}, {
            $pull: {tierLevels: { _id: { $in: levelIds } }}
        }, {new: true})
        .then(response => {
            if (response) {
                res.status(200).json({
                    status: true,
                    message: "Successfully deleted levels",
                    data: response
                })
            }
            else {
                res.status(404).json({
                    status: false,
                    message: "tier levels not found"
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                status: false,
                message: "Error while deleting levels",
                error: err
            })
        })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

/**
*@author Bhaskar Keelu
*@date 28/05/2019
Method to update the Tier level data.
*/
let updateTierLevels = (req, res) => {
    let body = req.body;
    let companyId =  req.jwt.companyId;
    let tier_id = body._id;
    delete body._id;
    delete body.companyId
    delete body.createdAt
    delete body.updatedAt
    return new Promise((resolve, reject) => {
        if (!tier_id) reject('_id is required')
        if (!body.tierLevels) reject('tierLevels array is required')
        if (!body.tierLevels.length) reject('tierLevels array is required')
        resolve(null);
    })
    .then(() => {
        tierLevelsModel.findOneAndUpdate({companyId: companyId, _id: tier_id}, {$set: body}, {new: true})
            .then(response => {
                if (response) {
                    res.status(200).json({
                        status: true,
                        message: "Successfully updated tier levels",
                        data: response
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "Tier levels not found"
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({
                    status: false,
                    message: "Error while updating tier levels",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}


/**
*@author Bhaskar Keelu
*@date 28/05/2019
Method to create a new field of time policies.
*/

let createTimePolicies = (req, res) => {
    let body = req.body;
    timeOffPoliciesModel.create(body).then((result) => {
        res.status(200).json({
            "Status": true,
            "Message": "Successfully inserted the Time Policies.",
            "data": result ? result : []
        })
    }).catch((err) => {
        res.status(400).json({
            "Status": false,
            "Message": "Failed to Execute",
            "Err": err
        })
    });
};





/**
*@author Bhaskar Keelu
*@date 31/05/2019
Method to fetch the policy details.
*/

let getPolicyDetails = (req, res) => {
    let companyId = req.params.company_id;
    timeOffPoliciesModel.find({ companyId: companyId },
        {
            policytype: 1,
            time_earned: 1,
            expirationoptions: 1,
            policy_effective_date: 1,
            employee_count: 1,
            _id: 1
        }
    ).populate({ path: 'leave_Eligibility_group', select: 'name' }).then((result) => {
        res.status(200).json({
            "status": true,
            "data": result ? result : []
        })
    }).catch((err) => {
        res.status(400).json({
            "status": false,
            "Message": "Failed to Execute",
            "Err": err
        })
    });
};




/**
*@author Bhaskar Keelu
*@date 29/05/2019
Method to fetch the policy data.
*/
let getPolicy = (req, res) => {
    let id = req.params._id;
    timeOffPoliciesModel.findById({_id : id}, 
    ).then((result) => {
        console.log(result);
            if(result != null) {
                let timeEarned = result.time_earned;
                let tierLevelId = timeEarned.tireslevel ? timeEarned.tireslevel.tier_level_id ? timeEarned.tireslevel.tier_level_id : [] : [];
                if(tierLevelId[0]){
                    console.log(tierLevelId,"tierLevelId");
                    tierLevelModel.find({_id : {$in : tierLevelId}}).then((tierResult) => {
                        res.status(200).json({
                            "status"  : true,
                            "message" : "Successfully fetched the Time Policy Data.",
                            "policyData" : result,
                            "tierLevelData" : tierResult ? tierResult :[]
                        })
                    }).catch((exception) => {
                        res.status(400).json({
                            "status": false,
                            "Message": "Failed to Execute",
                            "Err": exception
                        })
                    })
                }else{
                    res.status(200).json({
                        "status"  : true,
                        "message" : "Successfully fetched the Time Policy Data with No tier Level data",
                        "policyData" : result,
                        "tierLevelData" : null
                    })
                }
            }else{
                res.status(400).json({
                    "status": false,
                    "Message": "There is no data for this policy Type.",
                    "data": null
                })
            }
        })
        .catch((err) => {
            console.log(err)
            res.status(400).json({
                "status": false,
                "Message": "Failed to Execute",
                "Err": err
            })
        });
};

/**
*@author Bhaskar Keelu
*@date 31/05/2019
Method to update the time off policy.
*/
let updateTimeOffPolicy = (req, res) => {
    let body = req.body;
    let _id = body._id;
    delete body._id;
    timeOffPoliciesModel.findOneAndUpdate({ _id: _id }, { $set: body.params })
        .then((result) => {
            res.status(200).json({
                "status": true,
                "message": "Successfully updated the Time Off Policy.",
                "data": result ? result : []
            })
        }).catch((err) => {
            res.status(400).json({
                "status": false,
                "Message": "Failed to update.",
                "Err": err
            })
        });
}

/**
*@author Bhaskar Keelu
*@date 3/05/2019
Method to delete the time off policy and tier data.
*/
let deletePolicyAndTier = (req, res) => {
    let id = req.body._id;
    timeOffPoliciesModel.find({ _id: { $in: id } },
    ).then((result) => {
        if (result.length != 0) {
            result.forEach(element => {
                let timeEarned = element.time_earned;
                let tierLevelId = timeEarned.tireslevel ? timeEarned.tireslevel.tier_level_id : [];
                if (tierLevelId.length > 0) {
                    console.log(tierLevelId);
                    tierLevelModel.deleteMany({ _id: { $in: tierLevelId } }).then((tierResult) => {
                        timeOffPoliciesModel.deleteMany({ _id: { $in: id } }).then((finalResp) => {
                            res.status(200).json({
                                "status": true,
                                "message": "Successfully deleted the Time Off Policy Data.",
                                "data": finalResp
                            })
                        })
                    }).catch((exception) => {
                        res.status(400).json({
                            "status": false,
                            "Message": "Failed to Execute",
                            "Err": exception
                        })
                    })
                }else{
                    timeOffPoliciesModel.deleteMany({ _id: { $in: id } }).then((finalResp) => {
                        res.status(200).json({
                            "status": true,
                            "message": "Successfully deleted the Time Off Policy Data.",
                            "data": finalResp
                        })
                    })
                }
            });

        } else {
            res.status(400).json({
                "status": false,
                "Message": "There is no data for this policy Type.",
                "data": null
            })
        }
    }).catch((err) => {
        res.status(400).json({
            "status": false,
            "Message": "Failed to Execute",
            "Err": err
        })
    });
};

/**
*@author Bhaskar Keelu
*@date 11/06/2019
Method to upload xlsx file to update the employee list.
*/
let importXlsx = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let file;
        if(req.files) file = req.files.file;
        // console.log('body ==> ',req.body);
        // console.log('files ==> ',req.files);
        // console.log(file);

        var data = xlsx.parse(file.path)[0].data;
        // console.log("===========>", data);
        // console.log("===========>", data[0].data);
        let toAdd = [];
        for(let i = 1;i < data.length;i++) {
            let obj = {};
            if(data[i][0] && data[i][1] && data[i][2] && data[i][3]) {
                obj.empId = data[i][0];
                obj.empName = data[i][1];
                obj.policyName = data[i][2];
                obj.accuralBalance = data[i][3]
                obj.effectiveDate = new Date((data[i][4] - (25567 + 2))*86400*1000);
                obj.tierlevel = data[i][5];
                obj.company_id = req.body.companyId
                toAdd.push(obj);
            }
        }
        return toAdd;
        // console.log(toAdd);
        // return StructureModel.create(toAdd);
    }).then(async toAdd => {
            var employeeData = 0;
            // async function processArray(array) {
            for (const each of toAdd) { 
                // console.log(each);

                let doc = await userModel.findOne({"job.employeeId":each.empId,"companyId" :each.company_id},{_id :1})
                .sort({_id:-1}).then((result) => {
                    if(result != null){
                        // console.log(result)
                        timeOffPoliciesModel.findOne({"companyId" :each.company_id,"policytype" : each.policyName},{"time_earned.tireslevel.tier_level_id" : 1})
                        .then((policyResult) => {
                            var tier_id = policyResult.time_earned ? policyResult.time_earned.tireslevel ? policyResult.time_earned.tireslevel.tier_level_id[0] ? policyResult.time_earned.tireslevel.tier_level_id[0] : [] :[] :[];
                            tierLevelModel.findOne({"tierLevel.level" : each.tierLevel,"_id" : tier_id},{"tierLevel._id":1})
                            .then((tierResult) => {
                                var final_req = {
                                    policytype : each.policyName,
                                    policy_effective_date : each.effectiveDate,
                                    import_flag : true,
                                    accural_balance : each.accuralBalance,
                                    tier_level_import_id : [tierResult._id],
                                    userId : result._id,
                                    companyId :each.company_id
                                }
                                timeOffPoliciesModel.create(final_req).then((response)=>{
                                    console.log(response)
                                })
                            })

                        })
                    }
                });
            }
            console.log('Done!');
        // }
        return toAdd;
    }).then((data) => {
        console.log("data",data);
        if(data) {
            res.status(200).json({status: true, message: "Successfully Added "+data.length+' Records'});
        } else {
            res.status(200).json({status: true, message: "Data not found in the file"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: err});
    });
};

module.exports = {
    createTierLevels,
    getTierLevels,
    deleteTierLevels,
    updateTierLevels,

    createTimePolicies,
    getPolicyDetails,
    
    getPolicy,
    updateTimeOffPolicy,
    deletePolicyAndTier,
    importXlsx
}