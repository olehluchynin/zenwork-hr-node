let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants = require('./../../../common/constants');

const dayEnum   =   ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];

let Schema = new mongoose.Schema({
    workScheduleTitle: String,
    startDate: String,
    daysApplicable: [],
    totalHrsPerDay: String,
    totalHrsPerWeek: String,
    scheduleType: String,
    lunchBreak: Boolean,
    fullTimings: [],
    assignScheduleDuringNewHire: Boolean,
    assignToAJobTitle: String,
    assignToASpecificPerson: String,
    companyId: { type: ObjectID, ref: 'companies' },
    createdBy: { type: ObjectID, ref: 'companies' },
    updatedBy: { type: ObjectID, ref: 'companies' },
    createdDate: { type: Date, default: Date.now() },
    updatedDate: { type: Date, default: Date.now() },
});


let PolicySchema = new mongoose.Schema({
    policyType: String,
    eligibleGroups: [],

    companyId: { type: ObjectID, ref: 'companies' },
    createdBy: { type: ObjectID, ref: 'companies' },
    updatedBy: { type: ObjectID, ref: 'companies' },
    createdDate: { type: Date, default: Date.now() },
    updatedDate: { type: Date, default: Date.now() },
});

//Surendra(29-05-2019):New model for attendence records,code starts here
let EmployeeAttendenceSchema = new mongoose.Schema({
    punchType: {type:String,required:true},
    day: {type:String,enum:dayEnum,required:true},
    punchTime: {type:Date,required:true},
    punchDate:{type:Date,required:true},
    comments: String,
    isApproved:{type: Boolean, default: false},
    approvedBy:{ type: ObjectID, ref: 'users' },
    userId: { type: ObjectID, ref: 'users', required:true},
    companyId: { type: ObjectID, ref: 'companies',required:true},
    createdBy: { type: ObjectID, ref: 'users' },
    updatedBy: { type: ObjectID, ref: 'users' },
},{
    //Surendra(27-05-2019):Added after change for timestamp
    timestamps: true,
    versionKey: false,
    strict: false
});

//Surendra(29-05-2019):New model for attendence records,code ends here


let TeamScheduleSchema = new mongoose.Schema({
    workScheduleName: String,
    openTime: String,
    closeTime: String,
    intervalDefination: String,
    resourseType: String,
    resourses: [],
    resoursesPerInterval : String,
    daysApplicable : [],
    startDate : String,
    endDate : String,
    EligibilityCriteria :[],
    includeSpecificPerson :[],
    excludeSpecificPerson : [],
    companyId: { type: ObjectID, ref: 'companies' },
    createdBy: { type: ObjectID, ref: 'companies' },
    updatedBy: { type: ObjectID, ref: 'companies' },
    createdDate: { type: Date, default: Date.now() },
    updatedDate: { type: Date, default: Date.now() }
})


Schema.plugin(autoIncrement.plugin, {model:'workschedule', field:'workscheduleId', startAt: 1, incrementBy: 1});
let WorkScheduleModel = mongoose.model('workschedule', Schema);

Schema.plugin(autoIncrement.plugin, {model:'attendence', field:'attendenceId', startAt: 1, incrementBy: 1});
let AttendanceModel = mongoose.model('attendence', EmployeeAttendenceSchema)

module.exports = {
    WorkScheduleModel: WorkScheduleModel,
    AttendanceModel: AttendanceModel
};
