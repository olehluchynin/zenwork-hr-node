let mongoose = require('mongoose');
let moment = require('moment');
let ObjectID = mongoose.Types.ObjectId;

let WorkScheduleModel = require('./model').WorkScheduleModel;
let AttendanceModel = require('./model').AttendanceModel;
// let EmployeeCollection = require('../employees/model');//Surendra(27-05-2019):Change in schema from employees to users
let userCollection = require('../users/model');//Surendra(27-05-2019):added after Change in schema from employees to users

let mailer = require('./../../../common/mailer');

//Surendra(30-May-2019):Const for punch in and punch out
const punchTypeOptions  =   {
    "punchin"   :   "IN",
    "punchout"  :   "OUT"
};

let addSchedule = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        // if (!body.name) reject("Invalid name");
        // else if (!body.email || !helpers.isValidEmail(body.email)) reject("Invalid email");
        // else if (!body.type) reject('Invalid type');
        /* else */ resolve(null);
    }).then(() => {
        body.createdBy = req.jwt.companyId;
        body.companyId = req.jwt.companyId;
        return Promise.all([WorkScheduleModel.create(body)]);
    }).then(([schedule]) => {
        if (schedule) {
            res.status(200).json({ status: true, message: "Schedule added successfully", data: schedule });
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
};

let createNewPolicy = (req, res) => {

};

    /**
     * This method is used to add punch in for requested user.
     * 
     * @param {*} req 
     * @param {*} res 
     * @author : Surendra Waso
     */
let punchIn = (req, res) => {
   
  savePunchInOut(req,res,punchTypeOptions.punchin);
};

    /**
     * This method is used to add punch out for requested user.
     * 
     * @param {*} req 
     * @param {*} res 
     * @author : Surendra Waso
     */
let punchOut = (req, res) => {
    
    savePunchInOut(req,res,punchTypeOptions.punchout);
 };

 /**
     * This method is used to add punch in and out for requested user.
     * 
     * @param {*} req 
     * @param {*} res 
     * @author : Surendra Waso
     */
function savePunchInOut(req,res,punchType){
    
    let body = req.body;
    return new Promise((resolve, reject) => {
        if(!body.userId){
           reject("userId is mandatory");
           return;
        }

    body.day        = moment().format('dddd');
    body.punchTime  = new Date();//moment().format('LTS');
    body.punchDate  = new Date(new Date().setHours(0,0,0,0));
    body.userId     = req.body.userId;//req.jwt._id;
    body.createdBy  = req.body.userId;//req.jwt._id;
    body.punchType  = punchType;
    userCollection.findOne({ _id: body.userId })
    .then((employee) => {
        if (!employee) {                       
            reject("Employee Not Found");
        } else {
            resolve(employee);
            }
        })
    }).then((employee) => {   
        body.companyId = employee.companyId;
        return Promise.all([AttendanceModel.create(body)]);
    }).then(([punch]) => {
        if (punch) {
            
            res.status(200).json({ status: true, message: "Punch request added successfully", data: punch });
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

/**
     * This method is used to add missing punches for requested user or for employee by the manager or an admin.
     * 
     * @param {*} req 
     * @param {*} res 
     * @author : Surendra Waso
     */
let addMissingPunches   =   (req,res) =>{
    
    let body = req.body;
    
    var rejectedArray   =   [];
    return new Promise((resolve, reject) => {
       
        if(!body.userId){
           reject("userId is mandatory");
           return;
        }
        if(!body.addedBy){
            reject("addedBy is mandatory");
            return;
         }
        if(!body.punchArray){
            reject("punchArray is mandatory");
            return;
        }
        
        userCollection.findOne({ _id: body.userId })
        .then((employee) => {
            if (!employee) {                       
                reject("Employee Not Found");
            } else {
                resolve(employee);
                }
        })
    }).then((employee) => { 
        var allPunches      =   [];
        var punchArray      =   body.punchArray;
        for(var i=0;i<punchArray.length;i++){
            
            if(!punchArray[i].day || !punchArray[i].punchTime || !punchArray[i].punchType){
                rejectedArray.push(punchArray[i]);
            }else{
                var tempPunch        = {};
                
                tempPunch.day           = punchArray[i].day;
                tempPunch.punchTime     = new Date(punchArray[i].punchTime);
                tempPunch.punchDate     = new Date(new Date(punchArray[i].punchTime).toLocaleDateString());
                tempPunch.punchType     = punchArray[i].punchType;
                tempPunch.companyId     = req.body.companyId;
                tempPunch.userId        = req.body.userId;//req.jwt._id;
                tempPunch.createdBy     = req.body.addedBy;//req.jwt._id;

                allPunches.push(tempPunch);
            }
        }
        //res.status(200).json({ status: true, message: "Punches added successfully", data: {"success":allPunches,"rejected": rejectedArray}});
        return Promise.all([AttendanceModel.create(allPunches)]);
    }).then(([punch]) => {
        if (punch) {
            res.status(200).json({ status: true, message: "Punches added successfully", data: {"success":punch,"rejected": rejectedArray}});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

let deletePunchDetails  =   (req,res) =>{
    var allParams       =   req.params;

    return new Promise((resolve,reject) => {
        if(!allParams.userId){
            reject("userId is mandatory");
            return;
         }

         if(!allParams.payCycle){
            reject("payCycle is mandatory");
            return;
         }
         if(!allParams.timesheetEmployee){
            reject("timesheetEmployee is mandatory");
         }
         if(!allParams.punchDate){
            reject("punchDate is mandatory");
         }

         userCollection.findOne({ _id: allParams.userId })
         .then((employee) => {
            if(!employee){
                reject("Employee not found");
            }else{
                userCollection.findOne({ _id: allParams.timesheetEmployee })
                .then((timesheetEmployee) =>{
                    if(!timesheetEmployee){
                        reject("requested timesheetEmployee not found");
                    }else{
                        var filter  =   {
                            "userId"    :   timesheetEmployee._id,
                            // "pay_cycle" :   allParams.payCycle,
                            "punchDate" :   new Date(allParams.punchDate)
                        };
                        var valuesToSelect  =   {
                            _id         :   true
                        }
                        AttendanceModel.find(filter,valuesToSelect)
                        .then((attendanceData) => {
                            if(attendanceData.length <= 0){
                                reject("Attendance data for requested date is not available");
                            }else{
                                AttendanceModel.deleteMany({_id:{$in:attendanceData}})
                                .then((result) => {
                                    res.status(200).json({status:true, message: "Attendance data deleted",data:result});
                                })
                               
                                
                            }
                        })
                    }
                })
            }
         })
    }).catch((err) =>{
        res.status(400).json({status:false,message:err,data:null});
    })

}

let dailyPunchSheetDetails = (req, res) => {
    let query = {};
    if (req.body.dateRange == 'week') {
        query.startDate = moment().subtract(7, 'days').calendar();
        query.endDate = moment().format('L');
    } else if (req.body.dateRange == '2 weeks') {
        query.startDate = moment().subtract(14, 'days').calendar();
        query.endDate = moment().format('L');
    } else if (req.body.dateRange == 'Month') {
        query.startDate = moment().subtract(30, 'days').calendar();
        query.endDate = moment().format('L');
    } else if (req.body.dateRange == 'specific') {
        query.startDate = req.body.startDate;
        query.endDate = req.body.endDate;
    }
    return new Promise((resolve, reject) => {
        AttendanceModel.find({ employeeId: ObjectID(req.jwt.userId), punchDate: { $gte: query.startDate, $lte: query.endDate } })
            .then(
                (employee) => {
                    if (!employee) {
                        reject("Employee Punches Not Found")
                    } else {
                        resolve(employee);
                    }
                }
            )
    }).then((punch) => {
        if (punch) {
            res.status(200).json({ status: true, message: "Punch details Found", data: punch });
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

let createTeamWorkSchedule = (req,res)=>{
    let body = req.body;
    
}

module.exports = {
    addSchedule,
    createNewPolicy,
    punchIn,//To add punch in for the day at time
    punchOut,//To add punch out for the day at time
    addMissingPunches,//To add all missing punch in or out for the specified day 
    deletePunchDetails,//To delete punch in and punch out data for selected date
    dailyPunchSheetDetails
}
