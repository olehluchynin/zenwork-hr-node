let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let WorkScheduleController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');


router.post('/addSchedule', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, authMiddleware.isCompany, WorkScheduleController.addSchedule);

router.post('/punchIn', authMiddleware.isUserLogin, WorkScheduleController.punchIn);

router.post('/punchOut', authMiddleware.isUserLogin, WorkScheduleController.punchOut);

router.post('/addmissingpunches', authMiddleware.isUserLogin, WorkScheduleController.addMissingPunches);

router.post('/timeSheet', authMiddleware.isUserLogin, WorkScheduleController.dailyPunchSheetDetails);

router.delete('/deleteattendancefordate/:userId/:timesheetEmployee/:punchDate/:payCycle',authMiddleware.isUserLogin,WorkScheduleController.deletePunchDetails);

/* router.put('/',authMiddleware.isUserLogin,validationsMiddleware.reqiuresBody,ZenworkersController.updateSelfData);

router.get('/',authMiddleware.isUserLogin,ZenworkersController.getSelfData);

router.get('/all',authMiddleware.isUserLogin,ZenworkersController.getAll);

router.put('/:_id',authMiddleware.isUserLogin,authMiddleware.isZenworkerLogin,validationsMiddleware.reqiuresBody,authMiddleware.isSrSupport,ZenworkersController.udpateById);

router.get('/:_id',authMiddleware.isUserLogin,authMiddleware.isSrSupport,ZenworkersController.getById); */

//Surendra(30-05-2019)Timesheet approval
router.use('/timesheetapproval', require('./timesheetmanagement/timesheetapproval/route'));
router.use('/timesheetmaintenance',require('./timesheetmanagement/timesheetmaintenance/route'));
module.exports = router;