let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../config/config');
let _ = require('underscore');
let ManagerRequestsModel = require('./model');
let InboxModel = require('../inbox/model');
let UserModel = require('../users/model');
let RolesModel = require('../roles/model');

let submitRequest = (req, res) => {
    let body =  req.body
    return new Promise((resolve, reject) => {
        if (!body.companyId) reject('companyId is required');
        else if (!body.changeRequestType) reject('changeRequestType is required');
        else if (!body.requestedBy) reject('requestedBy is required');
        else if (!body.requestedFor) reject('requestedFor is required');
        else if (!body.newStatus) reject('newStatus is required');
        else if (body.newStatus && !Object.keys(body.newStatus).length > 0) reject("Please enter new values");
        else resolve(null);
    })
    .then(() => {
        ManagerRequestsModel.create(body)
            .then(managerRequest => {

                managerRequest = managerRequest.toJSON()
                console.log(managerRequest)
                let priority1Details = managerRequest.approvalPriority.filter(priorityObj => priorityObj.priority == '1')
                // console.log(priority1Details)
                let priority1Obj = priority1Details[0]

                if(priority1Obj.personId) {
                    let inbox_obj = {
                        companyId: body.companyId,
                        from: body.requestedBy,
                        to: priority1Obj.personId,
                        impactedEmployee: body.requestedFor,
                        subject: 'Manager Requests',
                        manager_request_id: managerRequest._id,
                        request_status: "Pending"
                    }
                    console.log(inbox_obj)
                    InboxModel.create(inbox_obj)
                        .then(priority1Notification => {
                            console.log(priority1Notification)
                            console.log('priority1Notification sent')
                            res.status(200).json({
                                status: true,
                                message: "Request submitted successfully",
                                data: managerRequest
                            })
                        })
                        .catch(err => {
                            console.log(err)
                            res.status(400).json({
                                status: false,
                                message: "Error while submitting request",
                                error: err
                            })
                        })
                }
                else if (priority1Obj.roleId) {
                    let notifs = []
                    UserModel.find({"job.Site_AccessRole" : priority1Obj.roleId},{'personal.name':1}).lean().exec()
                        .then(users_under_role => {
                            if (users_under_role.length > 0) {
                                for (let user of users_under_role) {
                                    notifs.push({
                                        companyId: body.companyId,
                                        from: body.requestedBy,
                                        to: user._id,
                                        impactedEmployee: body.requestedFor,
                                        subject: 'Manager Requests',
                                        manager_request_id: managerRequest._id,
                                        request_status: "Pending"
                                    })
                                }
                                InboxModel.insertMany(notifs)
                                    .then(response => {
                                        console.log('notifs insert response', response)
                                        res.status(200).json({
                                            status: true,
                                            message: "Request submitted successfully",
                                            data: managerRequest
                                        })
                                    })
                                    .catch(err => {
                                        console.log(err)
                                        res.status(400).json({
                                            status: false,
                                            message: "Error while submitting request",
                                            error: err
                                        })
                                    })
                            }
                            else {
                                console.log('users under role length 0')
                                res.status(400).json({
                                    status: false,
                                    message: "No users found with given role in workflow"
                                })
                            }
                            
                        })
                        .catch(err => {
                            console.log(err)
                            res.status(400).json({
                                status: false,
                                message: "Error while fetching users under specified role in workflow",
                                error : err
                            })
                        })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while submitting request",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let requestsList = (req, res) => {
    let body = req.body
    let userId = body.userId
    let companyId = body.companyId
    let roleId = body.roleId
    return new Promise((resolve, reject) => {
        // if (!userId) reject('userId is required');
        if (!companyId) reject('companyId is required');
        // else if (!body.request_status) reject('request_status is required');
        else if (!body.changeRequestType) reject('changeRequestType is required');
        resolve(null);
    })
    .then(() => {
        let query = {
            companyId: ObjectID(companyId)
        };

        // query['to'] = ObjectID(userId)

        if (userId) {
            query['to'] = ObjectID(userId)
        }

        query['subject'] = 'Manager Requests'

        let per_page = body.per_page ? body.per_page : 10
        let page_no = body.page_no ? body.page_no : 1
        let sort_obj = {
            "manager_request.requestedDate": -1
        }

        console.log('query ' , query)

        let match_stage1 = {
            $match: query
        }

        console.log('match_stage1 ' , match_stage1)

        let manager_request_lookup_stage = {
            $lookup: {
                from: "manager_requests",
                localField: "manager_request_id",
                foreignField: "_id",
                as: "manager_request"
            }
        }

        console.log('manager_request_lookup_stage ' , manager_request_lookup_stage)

        let impactedEmployee_lookup_stage = {
            $lookup: {
                from: "users",
                localField: "impactedEmployee",
                foreignField: "_id",
                as: "impactedEmployee"
            }
        }

        console.log('impactedEmployee_lookup_stage ' , impactedEmployee_lookup_stage)

        let project_stage1 = {
            $project: {
                "manager_request": { $arrayElemAt: ["$manager_request", 0] },
                "impactedEmployee": { $arrayElemAt: ["$impactedEmployee", 0] },
                archive : 1,
                read : 1,
                companyId: 1,
                from : 1,
                to : 1,
                subject : 1,
                manager_request_id : 1,
                createdAt : 1,
                updatedAt : 1
            }
        }

        console.log('project_stage1 ' , project_stage1)

        let project_stage2 = {
            $project: {
                "manager_request._id" : 1,
                "manager_request.approvalPriority" : 1,
                "manager_request.approvalStage" : 1,
                "manager_request.changeRequestType": 1,
                "manager_request.requestedDate": 1,
                "impactedEmployee._id": 1,
                "impactedEmployee.personal": 1,
                "impactedEmployee.job": 1,
                archive : 1,
                read : 1,
                companyId: 1,
                from : 1,
                to : 1,
                subject : 1,
                manager_request_id : 1,
                createdAt : 1,
                updatedAt : 1
            }
        }

        console.log('project_stage2 ' , project_stage2)

        let query2 = {
            // 'manager_request.approvalPriority.personId' : ObjectID(userId),
            // 'manager_request.approvalPriority.request_status' : body.request_status,
            'manager_request.changeRequestType' : body.changeRequestType,
            'manager_request.approvalPriority': {}
            // 'manager_request.approvalPriority': {
            //     '$elemMatch': {
            //         personId: ObjectID(userId)
            //     }
            // }
        }

        if (userId) {
            query2['manager_request.approvalPriority']['$elemMatch'] = {}
            // query2['manager_request.approvalPriority']['$elemMatch']['personId'] = ObjectID(userId)
            query2['manager_request.approvalPriority']['$elemMatch']['$or'] = [
                {
                    personId: ObjectID(userId)
                },
                {
                    roleId: ObjectID(roleId)
                }
            ]
        }
        else {
            query2['manager_request.approvalPriority']['$elemMatch'] = {}
        }

        if (body.request_status) {
            query2['manager_request.approvalPriority']['$elemMatch']['request_status'] = body.request_status
        }
        else {
            query2['manager_request.approvalPriority']['$elemMatch']['request_status'] = { $ne: 'Pending' }
        }

        console.log('query2 - elemMatch obj', query2['manager_request.approvalPriority']['$elemMatch'])

        if(body.location) {
            query2['impactedEmployee.job.location'] = body.location
        }

        if (body.from_date && body.to_date) {
            let from_date = new Date(body.from_date)
            let to_date = new Date(body.to_date)
            from_date.setHours(0, 0, 0, 0);
            to_date.setHours(23, 59, 59, 999);
            console.log(from_date);
            console.log(to_date);
            query2['manager_request.requestedDate'] = {
                '$gte': from_date,
                '$lte': to_date
            }
        }
        else if (body.from_date) {
            let from_date = new Date(body.from_date)
            from_date.setHours(0, 0, 0, 0);
            console.log(from_date);
            query2['manager_request.requestedDate'] = {
                '$gte': from_date
            }
        }

        if (body.search_query) {
            query2['$or'] = [
                {
                    'impactedEmployee.personal.name.firstName': { '$regex': body.search_query, '$options': 'i' }
                },
                {
                    'impactedEmployee.personal.name.middleName': { '$regex': body.search_query, '$options': 'i' }
                },
                {
                    'impactedEmployee.personal.name.lastName': { '$regex': body.search_query, '$options': 'i' }
                },
                {
                    'impactedEmployee.personal.name.preferredName': { '$regex': body.search_query, '$options': 'i' }
                },
                {
                    'impactedEmployee.job.jobTitle': { '$regex': body.search_query, '$options': 'i' }
                },
                {
                    'impactedEmployee.job.department': { '$regex': body.search_query, '$options': 'i' }
                }
            ]
        }

        console.log('query2 ' , query2)

        let match_stage2 = {
            $match: query2
        }

        console.log('match_stage2 ' , match_stage2)

        let sort_stage = {
            $sort: sort_obj
        }

        console.log('sort_stage ' , sort_stage)

        let skip_stage = {
            $skip: (page_no - 1) * per_page
        }

        console.log('skip_stage ' , skip_stage)

        let limit_stage = {
            $limit: per_page
        }

        console.log('limit_stage ' , limit_stage)

        Promise.all([
            InboxModel.aggregate([
                match_stage1,
                manager_request_lookup_stage,
                impactedEmployee_lookup_stage,
                project_stage1,
                project_stage2,
                match_stage2,
                sort_stage,
                skip_stage,
                limit_stage
            ]),
            InboxModel.aggregate([
                match_stage1,
                manager_request_lookup_stage,
                impactedEmployee_lookup_stage,
                project_stage1,
                // project_stage2,
                match_stage2,
                {
                    $count: "total_count"
                }
            ])
        ])
        .then(async ([requests, total_count_array]) => {
            // console.log(requests)
            // console.log(total_count_array)            
            for(let request of requests) {
                if (body.request_status == 'Pending' && request.manager_request.approvalStage != request.manager_request.approvalPriority.length) {
                    let next_priority_number = String(request.manager_request.approvalStage + 2)
                    let next_priority_person_details = request.manager_request.approvalPriority.filter(x => x.priority == next_priority_number)
                    next_priority_person_details = next_priority_person_details[0]
                    if (next_priority_person_details && next_priority_person_details.personId) {
                        request.next_priority = await UserModel.findOne({companyId: companyId, _id : next_priority_person_details.personId}, {'personal.name': 1}).lean().exec()
                    }
                    else if(next_priority_person_details && next_priority_person_details.roleId){
                        request.next_priority = await RolesModel.findOne({_id: next_priority_person_details.roleId}).lean().exec()
                    }
                    else {
                        request.next_priority = null
                    }
                }
                if (userId) {
                    let current_request_status_details = request.manager_request.approvalPriority.filter(x => x.personId == userId)
                    if (current_request_status_details[0]) {
                        request.request_status = current_request_status_details[0].request_status
                    }
                    else {
                        // let user_details = await UserModel.findOne({_id: ObjectID(userId)}).lean().exec()
                        // console.log(user_details)
                        current_request_status_details = request.manager_request.approvalPriority.filter(x => x.roleId == roleId)
                        if (current_request_status_details[0]) {
                            request.request_status = current_request_status_details[0].request_status
                        }
                    }
                }
                else {
                    let request_status_details = request.manager_request.approvalPriority.filter(x => x.priority == String(request.manager_request.approvalPriority.length))
                    request.request_status = request_status_details[0].request_status
                }
            }
            res.status(200).json({
                status: true,
                message: "Successfully fetched requests",
                total_count: total_count_array[0] ? total_count_array[0].total_count : 0,
                data: requests
            })
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: "Error while fetching requests",
                error: err
            })
        })

    })
}

let getManagerRequest = (req, res) => {
    let companyId = req.params.company_id
    let manager_request_id = req.params.manager_request_id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        let find_query = {
            companyId: ObjectID(companyId),
            _id: ObjectID(manager_request_id)
        }
        ManagerRequestsModel.findOne(find_query).populate('requestedFor', 'personal.name').populate('approvalPriority.personId', 'personal.name').populate('approvalPriority.roleId').lean().exec()
            .then(managerRequest => {
                if (managerRequest) {
                    res.status(200).json({
                        status: true,
                        message: "Successfully fetched manager request",
                        data: managerRequest
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "Manager request not found",
                        data: null
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while fetching manager request",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

// approve or reject
let editRequest = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let managerRequestId = body.manager_request_id
    let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!managerRequestId) reject('manager_request_id is required');
        else if (!body.request_status) reject('request_status is required');
        else if (!userId) reject('userId is required');
        else if (!companyId) reject('companyId is required')
        else resolve(null);
    })
    .then(() => {
        // change request status in manager request approval priority 
        // change approval stage - +1
        // if apprroved send notification to next person
        // archive corresponding notification
        //add approval or rejection date

        // add code related to last approval priority reject or approved the request
        // if roledId send notifications to all users with that role
        let find_filter = {
            companyId: companyId,
            _id: managerRequestId
        }
        ManagerRequestsModel.findOne(find_filter)
            .then(manager_request => {
                manager_request = manager_request.toJSON()
                // console.log(manager_request)
                
                let approval_stage = manager_request.approvalStage
                let current_priority = String(approval_stage + 1)
                let NextPersonDetails;
                let notifs = [];

                let LastPriority = manager_request.approvalPriority.length
                let NextPriority = String(parseInt(current_priority) + 1) 
                console.log('last priority - ', LastPriority)
                console.log('next priority - ', NextPriority)

                if (NextPriority != String(LastPriority + 1) ) {
                    NextPersonDetails = manager_request.approvalPriority.filter(priorityObj => priorityObj.priority == NextPriority)
                    NextPersonDetails = NextPersonDetails[0]
                    if (NextPersonDetails.personId) {
                        console.log('next person person')
                        notifs.push({
                            companyId: companyId,
                            from: manager_request.requestedBy,
                            to: NextPersonDetails.personId,
                            impactedEmployee: manager_request.requestedFor,
                            subject: 'Manager Requests',
                            manager_request_id: managerRequestId,
                            request_status: "Pending"
                        })
                    }
                    else if (NextPersonDetails.roleId) {
                        console.log('next person role')
                        UserModel.find({"job.Site_AccessRole" : NextPersonDetails.roleId},{'personal.name':1}).lean().exec()
                        .then(users_under_role => {
                            if (users_under_role.length > 0) {
                                for (let user of users_under_role) {
                                    notifs.push({
                                        companyId: companyId,
                                        from: manager_request.requestedBy,
                                        to: user._id,
                                        impactedEmployee: manager_request.requestedFor,
                                        subject: 'Manager Requests',
                                        manager_request_id: manager_request._id,
                                        request_status: "Pending"
                                    })
                                }
                            }
                            else {
                                console.log('users under role length 0')
                                res.status(400).json({
                                    status: false,
                                    message: "No users found with given role in workflow"
                                })
                            }
                        })
                        .catch(err => {
                            console.log(err)
                            res.status(400).json({
                                status: false,
                                message: "Error while fetching users under specified role in workflow",
                                error : err
                            })
                        })
                    }
                    
                }

                if (body.request_status == 'Approved') {
                    let update_filter = {
                        companyId: companyId,
                        _id: managerRequestId,
                        "approvalPriority.priority" : current_priority
                    }
                    let update_obj = {
                        $set: {
                            "approvalPriority.$.request_status" : 'Approved',
                            "approvalPriority.$.approval_date" : new Date(),
                            "approvalStage" : approval_stage + 1
                        }
                    }
                    ManagerRequestsModel.findOneAndUpdate(
                        update_filter,
                        update_obj,
                        {
                            new : true
                        }
                    )
                    .then(async response => {
                        if (response) {
                            let promises = [
                                InboxModel.findOneAndUpdate(
                                    {
                                        companyId: companyId,
                                        to: userId,
                                        manager_request_id: managerRequestId
                                    },
                                    {
                                        $set: {
                                            // archive: true,
                                            request_status: body.request_status
                                        }
                                    }
                                )
                            ]

                            console.log('notifs - ', notifs)
                            if (notifs.length > 0) {
                                promises.push([
                                    InboxModel.insertMany(notifs)
                                ])
                            }

                            // if ((approval_stage + 1) == LastPriority ) {
                            //     // update user data according to change request
                            //     // create update obj and push corresponding promise
                            //     let user_update_obj = {}
                            //     let user_new_job_obj = {}
                            //     let user = await UserModel.findOne({companyId: companyId, _id: userId}).exec()
                            //     if (manager_request.changeRequestType == 'Job') {
                            //         user.job.job_information_history[user.job.job_information_history.length - 1].end_date = manager_request.newStatus.jobTitle.effective_date
                            //         user_new_job_obj = {
                            //             jobTitle: manager_request.newStatus.jobTitle.new_value,
                            //             department: manager_request.newStatus.department.new_value,
                            //             // businessUnit: String,
                            //             // unionFields: String,
                            //             location: manager_request.newStatus.department.new_value,
                            //             // reportingLocation: String,
                            //             // remoteWork: {
                            //             //     type: String,
                            //             //     enum: ['Yes', 'No']
                            //             // },
                            //             changeReason: manager_request.changeReason,
                            //             notes: manager_request.notes,
                            //             // ReportsTo: { type: ObjectID, ref: 'users' },
                            //             effective_date: manager_request.newStatus.jobTitle.effective_date
                            //         }
                            //         if (user.job.job_information_history[user.job.job_information_history.length - 1].businessUnit) {
                            //             user_new_job_obj.businessUnit = user.job.job_information_history[user.job.job_information_history.length - 1].businessUnit
                            //         }
                            //         if (user.job.job_information_history[user.job.job_information_history.length - 1].unionFields) {
                            //             user_new_job_obj.unionFields = user.job.job_information_history[user.job.job_information_history.length - 1].unionFields
                            //         }
                            //         if (user.job.job_information_history[user.job.job_information_history.length - 1].reportingLocation) {
                            //             user_new_job_obj.reportingLocation = user.job.job_information_history[user.job.job_information_history.length - 1].reportingLocation
                            //         }
                            //         if (user.job.job_information_history[user.job.job_information_history.length - 1].remoteWork) {
                            //             user_new_job_obj.remoteWork = user.job.job_information_history[user.job.job_information_history.length - 1].remoteWork
                            //         }
                            //         if (user.job.job_information_history[user.job.job_information_history.length - 1].ReportsTo) {
                            //             user_new_job_obj.ReportsTo = user.job.job_information_history[user.job.job_information_history.length - 1].ReportsTo
                            //         }
                            //         user.job.job_information_history.push(user_new_job_obj)
                            //         await user.save()
                            //     }
                            //     else if (manager_request.changeRequestType == 'Salary') {

                            //     }

                            // }
                            
                            Promise.all(promises)
                                .then(responses => {
                                    console.log('responses', responses)
                                    res.status(200).json({
                                        status: true,
                                        message: "Request approved successfully"
                                    })
                                })
                                .catch(err => {
                                    console.log(err)
                                    res.status(400).json({
                                        status: true,
                                        message: "Error while approving request",
                                        error: err
                                    })
                                })
                        }
                        else {
                            res.status(404).json({
                                status: false,
                                message: "Manager request not found"
                            })
                        }
                    })
                }
                else if (body.request_status == 'Rejected') {
                    let update_filter = {
                        companyId: companyId,
                        _id: managerRequestId,
                        "approvalPriority.priority" : current_priority
                    }
                    let update_obj = {
                        $set: {
                            "approvalPriority.$.request_status" : 'Rejected',
                            "approvalPriority.$.rejection_date" : new Date(),
                            "approvalStage" : approval_stage + 1
                        }
                    }
                    ManagerRequestsModel.findOneAndUpdate(
                        update_filter,
                        update_obj,
                        {
                            new : true
                        }
                    )
                    .then(response => {
                        if (response) {
                            InboxModel.findOneAndUpdate(
                                {
                                    companyId: companyId,
                                    to: userId,
                                    manager_request_id: managerRequestId
                                },
                                {
                                    $set: {
                                        // archive: true,
                                        request_status: body.request_status
                                    }
                                }
                            )
                            .then(resp => {
                                if (resp) {
                                    res.status(200).json({
                                        status: true,
                                        message: "Request rejected successfully"
                                    })
                                }
                                else {
                                    res.status(404).json({
                                        status: false,
                                        message: "Notification not found"
                                    })
                                }
                            })
                        }
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while editing request",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

module.exports = {
    submitRequest,
    requestsList,
    getManagerRequest,
    editRequest
}