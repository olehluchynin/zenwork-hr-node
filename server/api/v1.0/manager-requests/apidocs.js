/**
 * @api {post} /manager-requests/submitRequest SubmitRequest
 * @apiName SubmitRequest
 * @apiGroup ManagerRequests
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "changeRequestType": "Job",
    "requestedBy": "5cdeb987c8b3566a2c99d450",
    "requestedFor": "5cdec1d5df4fd06b83354eb8",
    "newStatus": {
        "jobTitle": {
            "new_value": "Software Developer",
            "effective_date": "2019-05-25T04:39:16.390Z"
        },
        "department": {
            "new_value": "Tech",
            "effective_date": "2019-05-25T04:39:16.390Z"
        },
        "location": {
            "new_value": "Hyderabad",
            "effective_date": "2019-05-25T04:39:16.390Z"
        }
    },
    "changeReason": "Promotion",
    "notes": "I am requesting this job change as Smith's performance has been impressive in the past years",
    "impactsSalary": true,
    "selected_workflow": "5cff8d0d0cb9b86a2eaf2b9e",
    "approvalPriority": [
    	{
    		"priority": "1",
    		"key": "reportsTo",
    		"levelId": "5cf60d2ec8f4768976c8196c",
    		"personId": "5cd91d94f308c21c43e50553"
    	},
    	{
    		"priority": "3",
    		"key": "hr",
    		"levelId": "5cf6040dc8f4768976c80ee6",
    		"personId": "5cf9059412bccb31e028e038"
    	},
    	{
    		"priority": "2",
    		"key": "specialPerson",
    		"personId": "5cd91e68f308c21c43e50559"
    	},
    	{
    		"priority": "4",
    		"key": "specialRole",
    		"roleId": "5cdac212107c831d51cd05ee"
    	}
    ]
}
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Request submitted successfully",
    "data": {
        "newStatus": {
            "jobTitle": {
                "new_value": "Software Developer",
                "effective_date": "2019-05-25T04:39:16.390Z"
            },
            "department": {
                "new_value": "Tech",
                "effective_date": "2019-05-25T04:39:16.390Z"
            },
            "location": {
                "new_value": "Hyderabad",
                "effective_date": "2019-05-25T04:39:16.390Z"
            }
        },
        "requestedDate": "2019-05-24T04:44:16.939Z",
        "_id": "5ce776a6b715f80428a4950f",
        "companyId": "5c9e11894b1d726375b23058",
        "changeRequestType": "Job",
        "requestedBy": "5cdeb987c8b3566a2c99d450",
        "requestedFor": "5cdec1d5df4fd06b83354eb8",
        "changeReason": "Promotion",
        "notes": "I am requesting this job change as Smith's performance has been impressive in the past years",
        "impactsSalary": true,
        "selected_workflow": "5cff8d0d0cb9b86a2eaf2b9e",
        "approvalPriority": [
            {
                "priority": "1",
                "key": "reportsTo",
                "levelId": "5cf60d2ec8f4768976c8196c",
                "personId": "5cd91d94f308c21c43e50553"
            },
            {
                "priority": "3",
                "key": "hr",
                "levelId": "5cf6040dc8f4768976c80ee6",
                "personId": "5cf9059412bccb31e028e038"
            },
            {
                "priority": "2",
                "key": "specialPerson",
                "personId": "5cd91e68f308c21c43e50559"
            },
            {
                "priority": "4",
                "key": "specialRole",
                "roleId": "5cdac212107c831d51cd05ee"
            }
        ],
        "createdAt": "2019-05-24T04:44:22.460Z",
        "updatedAt": "2019-05-24T04:44:22.460Z"
    }
}
 * @apiParamExample {json} Request-Example2:
{
	"companyId": "5c9e11894b1d726375b23058",
    "changeRequestType": "Salary",
    "requestedBy": "5cdeb987c8b3566a2c99d450",
    "requestedFor": "5cdec1d5df4fd06b83354eb8",
    "newStatus": {
        "payRate": {
            "new_value": {
                "amount": 950,
                "unit": "USD"
            },
            "effective_date": "2019-05-25T04:39:16.390Z"
        },
        "payType": {
            "new_value": "Salary",
            "effective_date": "2019-05-25T04:39:16.390Z"
        },
        "Pay_frequency": {
            "new_value": "Weekly",
            "effective_date": "2019-05-25T04:39:16.390Z"
        },
        "percentage_change": {
            "new_value": 6.741,
            "effective_date": "2019-05-25T04:39:16.390Z"
        }
    },
    "oldStatus": {
        "payRate": {
            "old_value": {
                "amount": 900,
                "unit": "USD"
            },
            "effective_date": "2018-05-25T04:39:16.390Z"
        },
        "payType": {
            "old_value": "Salary",
            "effective_date": "2018-05-25T04:39:16.390Z"
        },
        "Pay_frequency": {
            "old_value": "Weekly",
            "effective_date": "2018-05-25T04:39:16.390Z"
        },
        "percentage_change": {
            "old_value": 6.741,
            "effective_date": "2018-05-25T04:39:16.390Z"
        }
    },
    "changeReason": "Promotion",
    "notes": "I am requesting this salary change as Smith's performance has been impressive in the past years",
    "selected_workflow": "5cff8d0d0cb9b86a2eaf2b9e",
    "approvalPriority": [
    	{
    		"priority": "1",
    		"key": "reportsTo",
    		"levelId": "5cf60d2ec8f4768976c8196c",
    		"personId": "5cd91d94f308c21c43e50553"
    	},
    	{
    		"priority": "3",
    		"key": "hr",
    		"levelId": "5cf6040dc8f4768976c80ee6",
    		"personId": "5cf9059412bccb31e028e038"
    	},
    	{
    		"priority": "2",
    		"key": "specialPerson",
    		"personId": "5cd91e68f308c21c43e50559"
    	},
    	{
    		"priority": "4",
    		"key": "specialRole",
    		"roleId": "5cdac212107c831d51cd05ee"
    	}
    ]
}
 * @apiSuccessExample Success-Response2:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Request submitted successfully",
    "data": {
        "newStatus": {
            "payRate": {
                "new_value": {
                    "amount": 950,
                    "unit": "USD"
                },
                "effective_date": "2019-05-25T04:39:16.390Z"
            },
            "payType": {
                "new_value": "Salary",
                "effective_date": "2019-05-25T04:39:16.390Z"
            },
            "Pay_frequency": {
                "new_value": "Weekly",
                "effective_date": "2019-05-25T04:39:16.390Z"
            },
            "percentage_change": {
                "new_value": 6.741,
                "effective_date": "2019-05-25T04:39:16.390Z"
            }
        },
        "oldStatus": {
            "payRate": {
                "old_value": {
                    "amount": 900,
                    "unit": "USD"
                },
                "effective_date": "2018-05-25T04:39:16.390Z"
            },
            "payType": {
                "old_value": "Salary",
                "effective_date": "2018-05-25T04:39:16.390Z"
            },
            "Pay_frequency": {
                "old_value": "Weekly",
                "effective_date": "2018-05-25T04:39:16.390Z"
            },
            "percentage_change": {
                "old_value": 6.741,
                "effective_date": "2018-05-25T04:39:16.390Z"
            }
        },
        "requestedDate": "2019-05-24T04:44:16.939Z",
        "_id": "5ce77c24b715f80428a49510",
        "companyId": "5c9e11894b1d726375b23058",
        "changeRequestType": "Salary",
        "requestedBy": "5cdeb987c8b3566a2c99d450",
        "requestedFor": "5cdec1d5df4fd06b83354eb8",
        "changeReason": "Promotion",
        "notes": "I am requesting this salary change as Smith's performance has been impressive in the past years",
        "selected_workflow": "5cff8d0d0cb9b86a2eaf2b9e",
        "approvalPriority": [
            {
                "priority": "1",
                "key": "reportsTo",
                "levelId": "5cf60d2ec8f4768976c8196c",
                "personId": "5cd91d94f308c21c43e50553"
            },
            {
                "priority": "3",
                "key": "hr",
                "levelId": "5cf6040dc8f4768976c80ee6",
                "personId": "5cf9059412bccb31e028e038"
            },
            {
                "priority": "2",
                "key": "specialPerson",
                "personId": "5cd91e68f308c21c43e50559"
            },
            {
                "priority": "4",
                "key": "specialRole",
                "roleId": "5cdac212107c831d51cd05ee"
            }
        ]
        "createdAt": "2019-05-24T05:07:48.635Z",
        "updatedAt": "2019-05-24T05:07:48.635Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /manager-requests/requestsList requestsList
 * @apiName requestsList
 * @apiGroup ManagerRequests
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cd91d94f308c21c43e50553",
	"request_status": "Pending",
    "changeRequestType": "Salary",
    "per_page": 10,
    "page_no": 1,
    "from_date": "2019-06-26T10:12:48.647Z",
    "to_date": "2019-07-01T05:30:20.603Z",
    "search_query": "s"
}
 * @apiParamExample {json} Request-Example2:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cd91d94f308c21c43e50553",
	"request_status": "Pending",
	"changeRequestType": "Salary"
}
 * @apiParamExample {json} Request-Example3:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"request_status": "Pending",
	"changeRequestType": "Salary"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched requests",
    "total_count": 1,
    "data": [
        {
            "_id": "5d130b9520c8f23e84898bd6",
            "archive": false,
            "read": false,
            "companyId": "5c9e11894b1d726375b23058",
            "from": "5cff696e386d686297b5132d",
            "to": "5cd91d94f308c21c43e50553",
            "subject": "Manager Requests",
            "manager_request_id": "5d130b9520c8f23e84898bd1",
            "createdAt": "2019-06-26T06:07:17.412Z",
            "updatedAt": "2019-06-26T06:07:17.412Z",
            "manager_request": {
                "_id": "5d130b9520c8f23e84898bd1",
                "requestedDate": "2019-06-26T06:07:09.234Z",
                "changeRequestType": "Salary",
                "approvalPriority": [
                    {
                        "request_status": "Pending",
                        "_id": "5d130b9520c8f23e84898bd5",
                        "priority": "1",
                        "key": "reportsTo",
                        "levelId": "5cf60d2ec8f4768976c8196c",
                        "personId": "5cd91d94f308c21c43e50553"
                    },
                    {
                        "request_status": "Pending",
                        "_id": "5d130b9520c8f23e84898bd4",
                        "priority": "3",
                        "key": "hr",
                        "levelId": "5cf6040dc8f4768976c80ee6",
                        "personId": "5cf9059412bccb31e028e038"
                    },
                    {
                        "request_status": "Pending",
                        "_id": "5d130b9520c8f23e84898bd3",
                        "priority": "2",
                        "key": "specialPerson",
                        "personId": "5cd91e68f308c21c43e50559"
                    },
                    {
                        "request_status": "Pending",
                        "_id": "5d130b9520c8f23e84898bd2",
                        "priority": "4",
                        "key": "specialRole",
                        "roleId": "5cdac212107c831d51cd05ee"
                    }
                ]
            },
            "impactedEmployee": {
                "_id": "5cdec1d5df4fd06b83354eb8",
                "personal": {
                    "name": {
                        "firstName": "Smith ",
                        "lastName": "ford",
                        "middleName": "Dunkun ",
                        "preferredName": "Smith"
                    },
                    "dob": "1997-02-10T18:30:00.000Z",
                    "gender": "male",
                    "maritalStatus": "Married",
                    "effectiveDate": "2019-05-20T18:30:00.000Z",
                    "veteranStatus": "Other Veteran",
                    "ssn": "123-32-2343",
                    "ethnicity": "Hispanic or Latino",
                    "address": {
                        "street1": "Rugby street",
                        "street2": "lane 2",
                        "city": "Newyork",
                        "state": "NewYork",
                        "zipcode": "45675",
                        "country": "United States"
                    },
                    "contact": {
                        "workPhone": "7653456785",
                        "extention": "1234",
                        "mobilePhone": "7702265585",
                        "homePhone": "5566445678",
                        "workMail": "gopikrishna.annaldas@hotmail.com",
                        "personalMail": "gopikrishna.annaldas@hotmail.com"
                    },
                    "social_links": {
                        "linkedin": "",
                        "twitter": "",
                        "facebook": ""
                    },
                    "education": [],
                    "languages": [],
                    "visa_information": []
                },
                "job": {
                    "current_employment_status": {
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-06T12:36:46.000Z"
                    },
                    "Specific_Workflow_Approval": "No",
                    "employeeId": "16099577",
                    "employeeType": "Full Time",
                    "hireDate": "2019-05-21T18:30:00.000Z",
                    "jobTitle": "Test",
                    "department": "Tech",
                    "location": "Banglore",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "FLSA_Code": "Exempt",
                    "EEO_Job_Category": "Technicians",
                    "Site_AccessRole": "5cdac017af231c1ca23a20c3",
                    "VendorId": "123456",
                    "employment_status_history": [
                        {
                            "_id": "5cdec1d5df4fd06b83354eb9",
                            "status": "Active",
                            "effective_date": "2019-05-17T14:14:45.767Z",
                            "end_date": "2019-05-21T12:25:08.108Z"
                        },
                        {
                            "_id": "5ce3ee241532171a8b5948cc",
                            "status": "Leave of Absence",
                            "effective_date": "2019-05-07T12:24:39.000Z",
                            "end_date": "2019-05-21T12:33:26.043Z"
                        },
                        {
                            "_id": "5ce3f0161532171a8b5948cd",
                            "status": "Leave of Absence",
                            "effective_date": "2019-05-12T12:33:04.000Z",
                            "end_date": "2019-05-21T12:34:08.513Z"
                        },
                        {
                            "_id": "5ce3f0401532171a8b5948ce",
                            "status": "Leave of Absence",
                            "effective_date": "2019-05-01T12:33:55.000Z",
                            "end_date": "2019-05-21T12:37:04.532Z"
                        },
                        {
                            "_id": "5ce3f0f01532171a8b5948cf",
                            "status": "Leave of Absence",
                            "effective_date": "2019-05-06T12:36:46.000Z"
                        }
                    ],
                    "job_information_history": [
                        {
                            "_id": "5cdec1d5df4fd06b83354eba",
                            "jobTitle": "Internship",
                            "department": "Sales",
                            "businessUnit": "Unit 1",
                            "unionFields": "Union 1",
                            "effective_date": "2019-05-17T14:14:45.767Z",
                            "end_date": "2019-05-22T06:36:59.768Z"
                        },
                        {
                            "_id": "5ce4ee0b1532171a8b5949f6",
                            "jobTitle": "Internship",
                            "department": "Tech",
                            "businessUnit": "Unit 1",
                            "unionFields": "Union 1",
                            "location": "Banglore",
                            "ReportsTo": "5cd91cc3f308c21c43e5054d",
                            "changeReason": "A-Promotion",
                            "remoteWork": "Yes",
                            "effective_date": "2019-05-06T18:30:00.000Z",
                            "notes": "sample",
                            "end_date": "2019-05-22T06:37:45.600Z"
                        },
                        {
                            "_id": "5ce4ee391532171a8b594a00",
                            "jobTitle": "Test",
                            "department": "Tech",
                            "businessUnit": "Unit 1",
                            "unionFields": "Union 1",
                            "location": "Banglore",
                            "ReportsTo": "5cd91cc3f308c21c43e5054d",
                            "changeReason": "A-Promotion",
                            "remoteWork": "Yes",
                            "effective_date": "2019-05-06T18:30:00.000Z",
                            "notes": "sample"
                        }
                    ],
                    "ReportsTo": "5ce6275ee2c6ab2b5061ae88"
                }
            }
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /manager-requests/get/:company_id/:manager_request_id GetManagerRequest
 * @apiName GetManagerRequest
 * @apiGroup ManagerRequests
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} manager_request_id Manager requests unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched manager request",
    "data": {
        "_id": "5d1350fd61be9e4204a6f358",
        "newStatus": {
            "jobTitle": {
                "new_value": "5cc15c75142fe761da3e2a84",
                "effective_date": "2019-06-10T18:30:00.000Z"
            },
            "department": {
                "new_value": "",
                "effective_date": "2019-06-12T18:30:00.000Z"
            },
            "location": {
                "new_value": "",
                "effective_date": "2019-06-06T18:30:00.000Z"
            }
        },
        "requestedDate": "2019-06-26T10:12:48.647Z",
        "approvalStage": 0,
        "companyId": "5c9e11894b1d726375b23058",
        "changeRequestType": "Job",
        "requestedBy": "5d00a2442b285670bf28946c",
        "requestedFor": {
            "_id": "5cd91d94f308c21c43e50553",
            "personal": {
                "name": {
                    "firstName": "raju",
                    "middleName": "king",
                    "lastName": "raju",
                    "preferredName": "raj"
                }
            }
        },
        "changeReason": "Promotion",
        "notes": "dtyry",
        "impactsSalary": true,
        "selected_workflow": "5d0cffe2e16c4a5dcb9094dc",
        "approvalPriority": [
            {
                "request_status": "Pending",
                "_id": "5d1350fd61be9e4204a6f35c",
                "priority": "3",
                "key": "reportsTo",
                "levelId": "5cf60d2ec8f4768976c8196c",
                "personId": {
                    "_id": "5cf9059412bccb31e028e038",
                    "personal": {
                        "name": {
                            "firstName": "vinay",
                            "middleName": "m",
                            "lastName": "s",
                            "preferredName": "vinnu"
                        }
                    }
                }
            },
            {
                "request_status": "Pending",
                "_id": "5d1350fd61be9e4204a6f35b",
                "priority": "1",
                "key": "hr",
                "levelId": "5cf6040dc8f4768976c80ee9",
                "personId": {
                    "_id": "5cd91d94f308c21c43e50553",
                    "personal": {
                        "name": {
                            "firstName": "raju",
                            "middleName": "king",
                            "lastName": "raju",
                            "preferredName": "raj"
                        }
                    }
                }
            },
            {
                "request_status": "Pending",
                "_id": "5d1350fd61be9e4204a6f35a",
                "priority": "4",
                "key": "specialPerson",
                "personId": {
                    "_id": "5cd92313f308c21c43e50599",
                    "personal": {
                        "name": {
                            "firstName": "ravi",
                            "middleName": "kumar",
                            "lastName": "singam",
                            "preferredName": "ravi"
                        }
                    }
                }
            },
            {
                "request_status": "Pending",
                "_id": "5d1350fd61be9e4204a6f359",
                "priority": "2",
                "key": "specialRole",
                "roleId": {
                    "_id": "5cdabfd9af231c1ca23a208a",
                    "status": "1",
                    "baseRoleId": "5cbe98a3561562212689f747",
                    "name": "us manager",
                    "description": "us access only",
                    "access": "full",
                    "roletype": "1",
                    "companyId": "5c9e11894b1d726375b23058",
                    "createdBy": "5c9e11894b1d726375b23058",
                    "updatedBy": "5c9e11894b1d726375b23058",
                    "createdAt": "2019-05-14T13:17:13.163Z",
                    "updatedAt": "2019-05-14T13:17:13.163Z",
                    "roleId": 102
                }
            }
        ],
        "createdAt": "2019-06-26T11:03:25.059Z",
        "updatedAt": "2019-06-26T11:03:25.059Z"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {post} /manager-requests/editRequest EditRequest-Approve Or Reject Request
 * @apiName EditRequest
 * @apiGroup ManagerRequests
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"manager_request_id": "5d1352cb61be9e4204a6f35e",
	"userId": "5cda377f51f0be25b3adceb4",
	"request_status": "Approved"
}
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Request approved successfully"
}
 * @apiParamExample {json} Request-Example2:
{
	"companyId": "5c9e11894b1d726375b23058",
	"manager_request_id": "5d1352cb61be9e4204a6f35e",
	"userId": "5cda377f51f0be25b3adceb4",
	"request_status": "Rejected"
}
 * @apiSuccessExample Success-Response2:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Request rejected successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */
