let express = require('express');

let router = express.Router();

let authMiddleware = require('../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.post(
    '/submitRequest',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.submitRequest
);

router.post(
    '/requestsList',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.requestsList
);

router.get(
    '/get/:company_id/:manager_request_id',
    authMiddleware.isUserLogin,
    Controller.getManagerRequest
);

router.post(
    '/edit',
    authMiddleware.isUserLogin,
    authMiddleware.isNotCompany,
    validationsMiddleware.reqiuresBody,
    Controller.editRequest
);

module.exports = router;