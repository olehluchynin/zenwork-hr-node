let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');


let Schema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    changeRequestType: {
        type: String,
        enum: ['Job', 'Salary'],
        required: true
    },
    requestedBy: { type:ObjectID, ref:'users', required: true},
    requestedFor: { type:ObjectID, ref:'users', required: true },
    newStatus: {
        jobTitle: {
            new_value: String,
            effective_date: Date
        },
        department: {
            new_value: String,
            effective_date: Date
        },
        location: {
            new_value: String,
            effective_date: Date
        },
        payRate: {
            new_value: {
                amount: Number,
                unit: String
            },
            effective_date: Date
        },
        payType: {
            new_value: String,
            effective_date: Date
        },
        Pay_frequency: {
            new_value: String,
            effective_date: Date
        },
        percentage_change: {
            new_value: Number,
            effective_date: Date
        }
    },
    changeReason: {
        type: String
    },
    oldStatus: {
        jobTitle: {
            old_value: String,
            effective_date: Date
        },
        department: {
            old_value: String,
            effective_date: Date
        },
        location: {
            old_value: String,
            effective_date: Date
        },
        payRate: {
            old_value: {
                amount: Number,
                unit: String
            },
            effective_date: Date
        },
        payType: {
            old_value: String,
            effective_date: Date
        },
        Pay_frequency: {
            old_value: String,
            effective_date: Date
        },
        percentage_change: {
            old_value: Number,
            effective_date: Date
        }
    },
    notes: String,
    requestedDate: {
        type: Date,
        default: new Date()
    },
    impactsSalary: Boolean,
    selected_workflow: {
        type: ObjectID,
        ref: 'workflow',
        required: true
    },
    approvalPriority: [
        {
            priority: {type: String, enum: ["1","2","3","4"]},
            key: {
                type: String,
                enum: ['reportsTo', 'hr', 'specialPerson', 'specialRole']
            },
            levelId: {type: ObjectID, ref: 'levels' },
            personId: {type: ObjectID, ref: 'users'},
            roleId: {type: ObjectID, ref: 'roles'},
            request_status: {type: String, enum: ['Pending', 'Approved', 'Rejected'], default: "Pending"},
            approval_date: Date,
            rejection_date: Date,
            approved_by: {
                type: ObjectID, ref: 'users'
            }
        }
    ],
    approvalStage: {type: Number, enum: [0,1,2,3,4], default : 0}

}, {
    timestamps: true,
    versionKey: false
})

let ManagerRequestsModel = mongoose.model('manager_requests',Schema);

module.exports = ManagerRequestsModel;