let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.post(
    "/contactus",
    validationsMiddleware.reqiuresBody,
    Controller.sendContactUsMail
);

module.exports = router