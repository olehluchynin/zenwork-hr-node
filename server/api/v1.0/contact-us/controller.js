let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;

let config = require('../../../config/config');

let utils = require('./../../../common/utils');
let helpers = require('./../../../common/helpers');
let constants = require('./../../../common/constants');
var Handlebars = require('handlebars');
let fs = require('fs');
let mailer = require('./../../../common/mailer');


let sendContactUsMail = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        if (!body.email || !helpers.isValidEmail(body.email))
            reject("Invalid email");
        else if (!body.phNo || !helpers.isValidPhoneNumber(body.phNo))
            reject('Invalid Phone Number')
        // else if (!body.message) 
        //     reject('Please enter a message')
        else if (!body.contactName) 
            reject('Contact Name is required')
        else if (!body.companyName)
            reject('Company Name is required')
        else if (!body.companySize) 
            reject('Company Size is required')
        else
            resolve(null);
    })
    .then(async () => {
        if (!body.contactName) {
            body.contactName = 'Someone'
        }
        let to = 'info@zenworkhr.com'
        // let from = config.mail.defaultFromAddress
        let from = "info@zenworkhr.com"
        let subject = "ZenworkHR - " + body.contactName + " wants to get in touch with you"
        let text = "ZenworkHR"
        if (!body.message) {
            body.message = 'None'
        }
        
        fs.readFile(__dirname + '/contactustemplate.hbs', 'utf8', async (err, file_data) => {
            if(!err) {
                // console.log(file_data)
                let html_data = (Handlebars.compile(file_data))(body)
                console.log(to, from, subject, text, html_data)
                try {
                    await mailer.sendContactUsEmail(to, from, subject, text, html_data)
                    res.status(200).json({
                        status: true,
                        message: "Message sent successfully"
                    })   
                }
                catch(e) {
                    console.log(err)
                    res.status(400).json({
                        status: false,
                        message: "Error while sending message",
                        error: err
                    })
                }
            }
            else {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while sending message",
                    error: err
                })
            }
            
        })
        
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({ status: false, message: err, data: null });
    })
};

module.exports = {
    sendContactUsMail
}