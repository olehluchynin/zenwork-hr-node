/**
 * @api {post} /contact-us/contactus SendContactUsMessage
 * @apiName SendContactUsMessage
 * @apiGroup ContactUs
 * 
 * 
 *
 * @apiParamExample {json} Request-Example:
 *
{
	"email": "asma.mubeen@mtwlabs.com",
	"phNo": "9966639171",
	"contactName": "Asma",
	"companyName": "Anon PVT LMTD",
	"companySize": "1",
	"message": ""
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Message sent successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */