let express = require('express');

let router = express.Router();

let authMiddleware = require('../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../middlewares/validations');


router.post(
    "/list",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.getUserNotifications
);

router.post(
    "/markAsComplete",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.markAsComplete
);

router.post(
    "/archive",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.archive
);

router.post(
    "/restore",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.restore
);

router.post(
    "/delete",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deleteNotifs
);

router.post(
    "/markAsRead",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.markAsRead
);

module.exports = router;