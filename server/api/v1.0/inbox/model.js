let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
// let constants  = require('../../../common/constants');

let Schema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    from: {type: ObjectID, ref: 'users'},
    to: {type: ObjectID, ref: 'users'},
    impactedEmployee: {type: ObjectID, ref: 'users'},
    subject: {
        type: String,
        enum: ['Off-boarding Tasks', 'On-boarding Tasks', 'Manager Requests', 'Employee Requests', 'Workflow Approvals', 'Benefit Requests', 'Leave Management Requests', 'Performance Requests']
    },
    onboarding_template_id: {type: ObjectID, ref: 'onboarding_templates'},
    offboarding_template_id: {type: ObjectID, ref: 'offboarding_templates'},
    task_id: {type: ObjectID},
    task_category: {type: String},
    manager_request_id: {type: ObjectID, ref: "manager_requests"},
    archive: {
        type: Boolean,
        default: false
    },
    read: {
        type: Boolean,
        default: false
    },
    request_status: {
        type: String,
        enum: ['Pending', 'Approved', 'Rejected']
    }

}, {
    timestamps: true,
    versionKey: false
})

let InboxModel = mongoose.model('inbox',Schema, 'inbox');

// let InboxCommentsSchema = new mongoose.Schema({
//     companyId: {type: ObjectID, ref:'companies', required: true},
//     inbox_record_id: {type: ObjectID, ref: 'inbox'},

// })

module.exports = InboxModel;