/**
 * @api {post} /notifications/list NotificationsList
 * @apiName NotificationsList
 * @apiGroup Inbox
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "userId": "5cda3ad10e57b225e1240c9d",
    "category": "On-boarding Tasks",
    "per_page": 10,
    "page_no": 1,
    "archive": false
}
 * @apiParamExample {json} Request-Example2:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "userId": "5cda3ad10e57b225e1240c9d",
    "category": "On-boarding Tasks",
    "per_page": 10,
    "page_no": 1,
    "archive": true
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched notifications",
    "total_count": 1,
    "data": [
        {
            "_id": "5cf8bc4cf098d90ff0f10be5",
            "archive": false,
            "read": false,
            "companyId": "5c9e11894b1d726375b23058",
            "from": {
                "_id": "5c9e11894b1d726375b23057",
                "status": "active",
                "email": "nisar@mtwlabs.com",
                "password": "123456789",
                "type": "company",
                "createdAt": "2019-03-29T12:37:29.915Z",
                "updatedAt": "2019-03-29T12:37:29.933Z",
                "userId": 4,
                "companyId": "5c9e11894b1d726375b23058"
            },
            "to": "5cda3ad10e57b225e1240c9d",
            "impactedEmployee": {
                "_id": "5cf8bc49f098d90ff0f10bde",
                "personal": {
                    "name": {
                        "firstName": "vinay",
                        "middleName": "m",
                        "lastName": "s",
                        "preferredName": "vinnu"
                    },
                    "address": {
                        "street1": "hyd",
                        "street2": "hyd",
                        "city": "hyd",
                        "state": "Alabama",
                        "zipcode": "12345",
                        "country": "Afghanistan"
                    },
                    "contact": {
                        "workPhone": "1234567876",
                        "extention": "1234",
                        "mobilePhone": "1234567897",
                        "homePhone": "1234567897",
                        "workMail": "vinay5t@mtwlabs.com",
                        "personalMail": "vinay5@gmail.con"
                    },
                    "dob": "1993-03-03T18:30:00.000Z",
                    "gender": "male",
                    "maritalStatus": "Married",
                    "effectiveDate": "2019-06-02T18:30:00.000Z",
                    "veteranStatus": "No Military Service",
                    "ssn": "234567898765",
                    "ethnicity": "Not Hispanic or Latino",
                    "education": [],
                    "languages": [],
                    "visa_information": []
                },
                "job": {
                    "current_employment_status": {
                        "status": "Active",
                        "effective_date": "2019-06-06T07:10:01.617Z"
                    },
                    "Specific_Workflow_Approval": "Yes",
                    "employeeId": "10024",
                    "employeeType": "Full Time",
                    "hireDate": "2019-03-13T18:30:00.000Z",
                    "jobTitle": "Software Engineer",
                    "department": "Tech",
                    "location": "Banglore",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "FLSA_Code": "Exempt",
                    "EEO_Job_Category": "Sales Workers",
                    "Site_AccessRole": "5cd922b7f308c21c43e50560",
                    "VendorId": "1234567898",
                    "HR_Contact": "5cd91d94f308c21c43e50553",
                    "ReportsTo": "5cda3ad10e57b225e1240c9d",
                    "employment_status_history": [
                        {
                            "_id": "5cf8bc49f098d90ff0f10bdf",
                            "status": "Active",
                            "effective_date": "2019-06-06T07:10:01.617Z"
                        }
                    ],
                    "job_information_history": [
                        {
                            "_id": "5cf8bc49f098d90ff0f10be0",
                            "jobTitle": "Software Engineer",
                            "department": "Tech",
                            "businessUnit": "Unit 1",
                            "unionFields": "Union 1",
                            "ReportsTo": "5cda3ad10e57b225e1240c9d",
                            "effective_date": "2019-06-06T07:10:01.617Z"
                        }
                    ]
                },
                "status": "active",
                "groups": [],
                "module_settings": {
                    "eligible_for_benifits": false,
                    "assign_leave_management_rules": false,
                    "assign_performance_management": false,
                    "assaign_employee_number": "System Generated",
                    "onboarding_template": "5cf8bc4bf098d90ff0f10be4"
                },
                "email": "vinay5t@mtwlabs.com",
                "companyId": "5c9e11894b1d726375b23058",
                "type": "employee",
                "onboardedBy": "5c9e11894b1d726375b23057",
                "password": "r9dDVsIK",
                "compensation": {
                    "NonpaidPosition": false,
                    "Pay_group": "Group2",
                    "Pay_frequency": "Weekly",
                    "Salary_Grade": "low",
                    "compensationRatio": "2",
                    "current_compensation": [
                        {
                            "_id": "5cf8bc49f098d90ff0f10be1",
                            "effective_date": "2019-03-19T18:30:00.000Z",
                            "payRate": {
                                "amount": 2
                            },
                            "Pay_frequency": "Weekly",
                            "changeReason": "--",
                            "notes": "--"
                        }
                    ],
                    "compensation_history": []
                },
                "createdAt": "2019-06-06T07:10:01.721Z",
                "updatedAt": "2019-06-06T07:10:03.697Z",
                "userId": 239
            },
            "subject": "On-boarding Tasks",
            "onboarding_template_id": {
                "_id": "5cf8bc4bf098d90ff0f10be4",
                "templateType": "derived",
                "templateName": "Tech team",
                "Onboarding Tasks": {
                    "IT Tasks": [
                        {
                            "taskName": "Cell phone",
                            "taskAssignee": "Slyvia@gmail.com",
                            "assigneeEmail": "Slyvia@gmail.com",
                            "taskDesription": "Give a cell phone to the new employee and record the task as done.",
                            "timeToComplete": {
                                "time": 1,
                                "unit": "Day"
                            },
                            "category": "IT Tasks",
                            "_id": "5cf8bc4bf098d90ff0f10be2",
                            "status": "Pending",
                            "task_assigned_date": "2019-06-06T07:10:03.237Z",
                            "task_assigned_by": "5c9e11894b1d726375b23057"
                        }
                    ],
                    "New Hire Tasks": [
                        {
                            "taskName": "Laptop",
                            "taskAssignee": "flor@gmail.com",
                            "assigneeEmail": "flor@gmail.com",
                            "taskDesription": "Give a laptop to the new employee",
                            "timeToComplete": {
                                "time": 1,
                                "unit": "Day"
                            },
                            "category": "New Hire Tasks",
                            "_id": "5cf8bc4bf098d90ff0f10be3",
                            "status": "Pending",
                            "task_assigned_date": "2019-06-06T07:10:03.237Z",
                            "task_assigned_by": "5c9e11894b1d726375b23057"
                        }
                    ],
                    "HR Tasks": [],
                    "New Hire Forms": [],
                    "IT Setup": [],
                    "Manager Tasks": []
                },
                "companyId": "5c9e11894b1d726375b23058",
                "assigned_date": "2019-06-06T07:10:03.201Z",
                "onboarding_complete": "No",
                "createdAt": "2019-06-06T07:10:03.246Z",
                "updatedAt": "2019-06-06T07:10:03.246Z",
                "otId": 119
            },
            "task_id": "5cf8bc4bf098d90ff0f10be2",
            "task_category": "IT Tasks",
            "createdAt": "2019-06-06T07:10:04.376Z",
            "updatedAt": "2019-06-06T07:10:04.376Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {post} /notifications/markAsComplete MarkAsComplete
 * @apiName MarkAsComplete
 * @apiGroup Inbox
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"notif_id": "5cf9059712bccb31e028e03f",
	"notes": "testing"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Success",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {post} /notifications/archive Archive
 * @apiName Archive
 * @apiGroup Inbox
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"_ids": [
		"5cf8bc4cf098d90ff0f10be5"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Archived successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /notifications/restore Restore
 * @apiName Restore
 * @apiGroup Inbox
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"_ids": [
		"5cf8bc4cf098d90ff0f10be5"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Restored successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /notifications/markAsRead MarkAsRead
 * @apiName MarkAsRead
 * @apiGroup Inbox
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"_id": "5cf8bc4cf098d90ff0f10be5"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Marked as read",
    "data": {
        "archive": false,
        "read": true,
        "_id": "5cf8bc4cf098d90ff0f10be5",
        "companyId": "5c9e11894b1d726375b23058",
        "from": "5c9e11894b1d726375b23057",
        "to": "5cda3ad10e57b225e1240c9d",
        "impactedEmployee": "5cf8bc49f098d90ff0f10bde",
        "subject": "On-boarding Tasks",
        "onboarding_template_id": "5cf8bc4bf098d90ff0f10be4",
        "task_id": "5cf8bc4bf098d90ff0f10be2",
        "task_category": "IT Tasks",
        "createdAt": "2019-06-06T07:10:04.376Z",
        "updatedAt": "2019-06-28T05:39:12.861Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /notifications/delete Delete
 * @apiName Delete
 * @apiGroup Inbox
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"_ids": [
		"5cf8bc4cf098d90ff0f10be5"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted notifications successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */