let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../config/config');
let _ = require('underscore');
let InboxModel = require('./model');
let OnboardingTemplateModel = require('../employee-management/onboarding/onboard-template/model');
let OffboardingTemplateModel = require('../offboarding/model').OffBoardingModel;

let getUserNotifications = (req, res) => {
    let body = req.body
    let userId = body.userId
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!userId) reject('userId is required');
        else if (!companyId) reject('companyId is required');
        resolve(null);
    })
    .then(() => {
        let query = {
            companyId: ObjectID(companyId)
        };

        query['to'] = ObjectID(userId)

        if (body.category) {
            query['subject'] = body.category
        }

        if (body.hasOwnProperty('archive')) {
            query['archive'] = body.archive
        }

        let per_page = body.per_page ? body.per_page : 10
        let page_no = body.page_no ? body.page_no : 1
        let sort_obj = {
            "updatedAt": -1
        }

        Promise.all([
            InboxModel.find(query)
                .populate('from')
                .populate('impactedEmployee')
                .populate('onboarding_template_id')
                .populate('offboarding_template_id')
                .populate('manager_request_id')
                .sort(sort_obj).skip((page_no - 1) * per_page).limit(per_page).lean().exec(),
            InboxModel.countDocuments(query)
        ])
        .then(([notifications, count]) => {
            return res.status(200).json({ 
                status: true, 
                message: "Successfully fetched notifications", 
                total_count: count, 
                data: notifications 
            });
        })
        .catch(err => {
            console.log(err)
            return res.status(400).json({ 
                status: false, 
                message: "Error while fetching notifications", 
                error: err
            });
        })
    })
    .catch(err => {
        console.log(err)
        return res.status(400).json({
            status: false,
            message: err
        })
    });
}

let markAsComplete = (req, res) => {
    let body = req.body
    let notif_id = body.notif_id
    return new Promise((resolve, reject) => {
        if (!notif_id) reject("notif_id is required");
        resolve(null);
    })
    .then(() => {
        InboxModel.findOne({_id: notif_id})
            .then(notification => {

                if(notification.subject == "On-boarding Tasks") {
                    let find_query = {
                        _id: notification.onboarding_template_id
                    }
                    let update_query = { $set: {} };
                    find_query['Onboarding Tasks.' + notification.task_category + "._id"] = notification.task_id
                    update_query['$set']['Onboarding Tasks.' + notification.task_category + ".$.status"] = 'Completed'
                    update_query['$set']['Onboarding Tasks.' + notification.task_category + ".$.task_completion_date"] = new Date()
                    if (body.notes) {
                        update_query['$set']['Onboarding Tasks.' + notification.task_category + ".$.notes"] = body.notes
                    }
                    console.log(find_query)
                    console.log(update_query)
                    OnboardingTemplateModel.updateOne(find_query, update_query, {new: true})
                        .then(response => {
                            console.log(response)
                            res.status(200).json({
                                status: true,
                                message: "Success",
                                data: null
                            })
                        })
                        .catch(err => {
                            console.log(err);
                            res.status(400).json({
                                status: false,
                                message: "Error",
                                error: err
                            })
                        });
                }
                else if (notification.subject == 'Off-boarding Tasks') {
                    let find_query = {
                        _id: notification.offboarding_template_id
                    }
                    let update_query = { $set: {} };
                    find_query['offBoardingTasks.' + notification.task_category + "._id"] = notification.task_id
                    update_query['$set']['offBoardingTasks.' + notification.task_category + ".$.status"] = 'Completed'
                    update_query['$set']['offBoardingTasks.' + notification.task_category + ".$.task_completion_date"] = new Date()
                    if (body.notes) {
                        update_query['$set']['offBoardingTasks.' + notification.task_category + ".$.notes"] = body.notes
                    }
                    console.log(find_query)
                    console.log(update_query)
                    OffboardingTemplateModel.updateOne(find_query, update_query, {new: true})
                        .then(response => {
                            console.log(response)
                            res.status(200).json({
                                status: true,
                                message: "Success",
                                data: null
                            })
                        })
                        .catch(err => {
                            console.log(err);
                            res.status(400).json({
                                status: false,
                                message: "Error",
                                error: err
                            })
                        });
                }
                
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({
                    status: false,
                    message: err
                });
            });
    })
    .catch(err => {
        console.log(err);
        res.status(400).json({
            status: false,
            message: err
        });
    })
}

let archive = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else resolve(null);
    })
    .then(() => {
        InboxModel.updateMany(
            {
                companyId: companyId,
                _id: {$in : body._ids}
            },
            {
                $set: {
                    archive: true
                }
            }
        )
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Archived successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while archiving notifications",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let restore = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else resolve(null);
    })
    .then(() => {
        InboxModel.updateMany(
            {
                companyId: companyId,
                _id: {$in : body._ids}
            },
            {
                $set: {
                    archive: false
                }
            }
        )
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Restored successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while restoring notifications",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let deleteNotifs = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else resolve(null);
    })
    .then(() => {
        InboxModel.deleteMany(
            {
                companyId: companyId,
                _id: {$in : body._ids}
            }
        )
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted notifications successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting notifications",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let markAsRead = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let _id = body._id
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else if (!body._id) reject('_id is required');
        else resolve(null);
    })
    .then(() => {
        let find_filter = {
            companyId: companyId,
            _id: _id
        }
        InboxModel.findOneAndUpdate(
            find_filter,
            {
                $set : {
                    read: true
                }
            },
            {
                new: true
            }
        )
        .then(response => {
            if (response) {
                res.status(200).json({
                    status: true,
                    message: "Marked as read",
                    data: response
                })
            }
            else {
                res.status(404).json({
                    status: false,
                    message: "notification not found"
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: "Error processing your request",
                error: err
            })
        })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}


module.exports = {
    getUserNotifications,
    markAsComplete,
    archive,
    restore,
    deleteNotifs,
    markAsRead
}