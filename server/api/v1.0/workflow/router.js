
var express = require("express");
var Controller = require("./controller");
var router = express.Router();
let authMiddleware = require('./../middlewares/auth');

router.post('/create', Controller.createWorkflow);
router.get('/get/:company_id/:workflow_type', Controller.getWorkflow);
router.put('/update/:workflow_id', Controller.updateWorkflow);
router.delete('/delete/:workflow_id', Controller.deleteWorkflow);
router.post('/levels/create', Controller.createNewLevels)
router.get('/levels/get/:company_id/:level_type', Controller.getLevelsByType)
router.get('/levels/get/:company_id/:workflow_type/:requested_by/:priority', Controller.findReportingOrHrPerson)

router.get(
    '/currentlevel/:company_id/:level_type',
    Controller.getCurrentHighestLevel
);

router.get(
    '/checkPersonInWorkflows/:user_id',
    authMiddleware.isUserLogin,
    Controller.checkPersonInWorkflows
);

module.exports = router;

