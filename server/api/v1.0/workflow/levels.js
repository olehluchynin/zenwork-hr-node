let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;

let Schema = new mongoose.Schema({
    levelType: {type: String, required: true, enum: ["HR", "Manager"]},    
    levelIndex: {type: Number, required: true},
    levelName: {type: String, required: true},
    companyId: {type: ObjectID, ref: 'companies', required: true}
},{
    timestamps: true,
    versionKey: false
});
//Schema.plugin(autoIncrement.plugin, {model:'levels', field:'levelId', startAt: 1, incrementBy: 1});

// Schema.index({loc: '2dsphere'}); //loc: { type: "Point", coordinates: [ longitude, latitude ] },

let LevelsModel = mongoose.model('levels',Schema);

module.exports = LevelsModel;
