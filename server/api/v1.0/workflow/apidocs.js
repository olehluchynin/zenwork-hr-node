/**
 * @api {get} /workflow/levels/get/:company_id/:workflow_type/:requested_by/:priority FindCorrespondingPersonId
 * @apiName FindCorrespondingPersonId
 * @apiGroup Workflow
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {String} workflow_type Workflow type (job, compensation, demographic)
 * @apiParam {ObjectId} requested_by Assigned Training unique _id
 * @apiParam {String} priority Approval priority number (1,2,3,4)
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Getting results success",
    "resultType": "user",
    "data": "5cf6095fecc3dd104e07cd7d"
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /workflow/currentlevel/:company_id/:level_type GetCurrentHighestLevel
 * @apiName GetCurrentHighestLevel
 * @apiGroup Workflow
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {String} level_type Level type (HR, Manager)
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Current level fetched successfully",
    "data": 9
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /workflow/checkPersonInWorkflows/:user_id CheckPersonInWorkflows
 * @apiName CheckPersonInWorkflows
 * @apiGroup Workflow
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} user_id User unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Success",
    "data": {
        "isAssignedInWorkflow": true
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */