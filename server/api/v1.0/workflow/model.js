let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');

let Schema = new mongoose.Schema({

    workflowType: {type: String, enum: Object.keys(constants.workflowType), required: true},
    
    reportsTo: {
        priority: String,
        levelId: {type: ObjectID, ref: 'levels'}
    },

    hr: {
        priority: String,
        levelId: {type: ObjectID, ref: 'levels'}
    },

    specialPerson: {
        priority: String,
        personId: {type: ObjectID, ref: 'users'}
    },

    specialRole: {
        priority: String,
        roleId: {type: ObjectID, ref: 'roles'}
    },
    
    companyId: {type: ObjectID, ref: 'companies', required: true}

},{
    timestamps: true,
    versionKey: false
});
Schema.plugin(autoIncrement.plugin, {model:'workflow', field:'workflowId', startAt: 1, incrementBy: 1});

// Schema.index({loc: '2dsphere'}); //loc: { type: "Point", coordinates: [ longitude, latitude ] },

let WorkflowModel = mongoose.model('workflow',Schema);

module.exports = WorkflowModel;
