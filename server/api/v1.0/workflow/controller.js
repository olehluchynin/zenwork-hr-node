let mongoose = require('mongoose');
let ObjectID = require('mongodb').ObjectID;
let WorkflowModel = require('./model');
let LevelsModel = require("./levels");
let UserModel = require("../users/model")

/**
 * This method is used to create new workflow
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let createWorkflow = (req, res) => {
    let reqBody = req.body;

    return new Promise((resolve, reject) => {
        if (!reqBody.companyId) reject('companyId is required');
        else if(!reqBody.workflowType) reject('workflowType is required');
        else if(reqBody.reportsTo || reqBody.hr || reqBody.specialRole || reqBody.specialPerson) {
            resolve(null)
        }
        else {
            reject('Invalid data found')
        }
    })
    .then(() => {
        WorkflowModel.findOne({companyId: new ObjectID(reqBody.companyId), workflowType: reqBody.workflowType}).then((result) => {
            if(result) {
                res.status(400).json({
                    status: false,
                    message: "Workflow already exists with the details",
                })
            } 
            else {
                WorkflowModel.create(reqBody).then((result) => {
                    res.status(200).json({
                        status: true,
                        message: "Workflow created successfully",
                        data: result ? result : []
                    })
                }).catch((error) => {
                    res.status(400).json({
                        status: false,
                        message: "Workflow creation failed",
                        error: error
                    })
                })
            }
        }).catch((error) => {
            console.log(error);
            res.status(400).json({
                status: false,
                message: "Workflow creation failed",
                error: error
            })
        });
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
    
    // if(reqBody.companyId != "" && reqBody.reportedBy != "" && reqBody.manager != "" && reqBody.hr != "" && reqBody.specialPerson != "") {
}

/**
 * This method is used to get workflow by company id
 * 
 * @param {*} req 
 * @param {*} res 
 * @author jagadeesh patta
 */

let getWorkflow = (req, res) => {
    let company_id = req.params.company_id;
    let workflow_type = req.params.workflow_type;

    return new Promise((resolve, reject) => {
        WorkflowModel.find({companyId: new ObjectID(company_id), workflowType: workflow_type}).then((result) => {
            resolve(result)
        }).catch((error) => {
            reject(error)
        })
    }).then((result) => {
        res.status(200).json({
            status: true,
            message: "Getting workflow success",
            data: result ? result : []
        })
    }).catch((error) => {
        res.status(200).json({
            status: true,
            message: "Getting workflow failed",
            error: error
        })
    })
}

/**
 * This method is used to update current workflow.
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let updateWorkflow = (req, res) => {
    let workflow_id = req.params.workflow_id;

    return new Promise((resolve, reject) => {
        WorkflowModel.findByIdAndUpdate({_id: new ObjectID(workflow_id)}, {$set: req.body}, {new: true}).then((result) => {
            resolve(result)
        }).catch((error) => {
            reject(error)
        })
    }).then((result) => {
        res.status(200).json({
            status: true,
            message: "Updation of workflow success",
            data: result ? result : []
        })
    }).catch((error) => {
        res.status(200).json({
            status: true,
            message: "Updation of workflow failed",
            error: error
        })
    })
}

/**
 * This method is used to delete the workflow based on id
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let deleteWorkflow = (req, res) => {
    let workflow_id = req.params.workflow_id;

    return new Promise((resolve, reject) => {
        WorkflowModel.findOneAndDelete({_id: new ObjectID(workflow_id)}).then((result) => {
            resolve(result)
        }).catch((error) => {
            reject(error)
        })
    }).then((result) => {
        res.status(200).json({
            status: true,
            message: "Deletion of workflow success",
            data: result ? result : []
        })
    }).catch((error) => {
        res.status(200).json({
            status: true,
            message: "Deletion of workflow failed",
            error: error
        })
    })
}

// ============== Implementation functions for Levels =====================

/**
 * This method is used to create list of levels
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let createNewLevels = (req, res) => {
    
    if (req.body.levelIndex == null || req.body.levelType == null || req.body.companyId == null) {
        res.status(400).json({
            status: false,
            message: "Invalid input data detected"
        })
    } else {

        let promiseArray = []
        let levelIndexArray = []

        for (index=1; index<=req.body.levelIndex; index++) {
            let levelObj = {}
            levelIndexArray.push(index)
            levelObj.levelIndex = index;
            if (req.body.levelType == "HR") {
                levelObj.levelType = "HR"
                levelObj.levelName = "HR Level "+index
            }
            if (req.body.levelType == "Manager") {
                levelObj.levelType = "Manager"
                levelObj.levelName = "Manager Level "+index
            }
            levelObj.companyId = new ObjectID(req.body.companyId);
            
            promiseArray.push(new Promise((resolve, reject) => {
                LevelsModel.findOneAndUpdate({levelIndex: index, levelType: levelObj.levelType, companyId: ObjectID(req.body.companyId)}, levelObj, {upsert: true, new: true}).then(result => {
                    resolve(result)
                }).catch(error => {
                    reject(error)
                })
            }))
        }

        promiseArray.push(new Promise((resolve, reject) => {
            LevelsModel.remove({levelIndex: {$nin: levelIndexArray}, levelType: req.body.levelType, companyId: ObjectID(req.body.companyId)}).then(result => {
                resolve(result)
            }).catch(error => {
                reject(error)
            })
        }))

        Promise.all(promiseArray).then(result => {
            res.status(200).json({
                status: true,
                message: "Level creation success",
                data: result
            })
        }).catch(error => {
            res.status(400).json({
                status: false,
                message: "Levels creation failed",
                error: error
            })
        })
    }
} 

/**
 * This method is used to get all levels by company id and level type
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getLevelsByType = (req, res) => {
    let companyId = req.params.company_id,
        levelType = req.params.level_type;

    if(levelType == null || companyId == null) {
        res.status(400).json({
            status: false,
            message: "Invalid input detected"
        })
    } else {
        LevelsModel.find({companyId: new ObjectID(companyId), levelType: levelType}).sort({levelIndex: 1}).lean().then(result => {
            res.status(200).json({
                status: true,
                message: "Getting leves success",
                data: result ? result : []
            })    
        }).catch(error => {
            res.status(400).json({
                status: false,
                message: "Getting levels failed",
                error: error
            })
        })
    }
}


/**
*@author Asma
*@date 05/07/2019 12:11
Method to get current number of levels declared under manager or hr for a company
*/
let getCurrentHighestLevel = (req, res) => {
    let companyId = req.params.company_id
    let levelType = req.params.level_type
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else if (!levelType) reject('levelType is required');
        else resolve(null);
    })
    .then(() => {
        let find_filter = {
            companyId: new ObjectID(companyId),
            levelType: levelType
        }
        LevelsModel.countDocuments(find_filter)
            .then(levelsCount => {
                console.log(levelsCount)
                res.status(200).json({
                    status: true,
                    message: "Current level fetched successfully",
                    data: levelsCount
                })
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: 'Error while fetching levels',
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

/**
 * This method is used to findout the person based on the company id and level id
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let findReportingOrHrPerson = (req, res) => {
    let company_id = req.params.company_id;
    let reported_by = req.params.requested_by;
    let workflow_type = req.params.workflow_type;
    let priority = req.params.priority

    WorkflowModel.findOne({companyId: company_id, workflowType: workflow_type})
    .populate("reportsTo.levelId")
    .populate("hr.levelId")
    .populate("specialPerson.personId")
    .populate("specialRole.roleId")
    .exec()
    .then(result => {
        
        if (result) {

            var workflow = result.toObject()

            var filter_type = undefined,
                workflow_filter = undefined;

            if (workflow["reportsTo"]["priority"] == priority) {
                filter_type = "ReportsTo",
                workflow_filter = "reportsTo"
            }

            if (workflow["hr"]["priority"] == priority) {
                filter_type = "HR_Contact",
                workflow_filter = "hr"
            }

            if (filter_type != undefined) {
                var level_index = workflow[workflow_filter]["levelId"]["levelIndex"]
                console.log('level_index')
                console.log("workflow type", workflow_filter)
                console.log(workflow[workflow_filter])
                UserModel.findById({_id: new ObjectID(reported_by)}).then(result => {
                    let resultObj = []
                    resultObj.push(result["job"][filter_type]);
                    findNextLevelPerson(result["job"][filter_type], resultObj, filter_type, level_index).then(innerResult => {
                        
                        console.log('inner result ', innerResult)
                        res.status(200).json({
                            status: true,
                            message: "Getting results success",
                            resultType: "user",
                            data: innerResult.length <= workflow[workflow_filter]["levelId"]["levelIndex"] ? innerResult[(workflow[workflow_filter]["levelId"]["levelIndex"] - 1)] ? innerResult[(workflow[workflow_filter]["levelId"]["levelIndex"] - 1)] : innerResult[innerResult.length - 1] : []
                        })
                    }).catch(error => {
                        console.log(error)
                        res.status(400).json({
                            status: false,
                            message: "Inner loop error",
                            error: error
                        })
                    })
                }).catch(error => {
                    res.status(400).json({
                        status: false,
                        message: "Getting users failed",
                        error: error
                    })
                })
            } else {
                res.status(200).json({
                    status: true,
                    message: "Getting results success",
                    resultType: workflow["specialPerson"]["priority"] == priority ? "user" : "role",
                    data: workflow["specialPerson"]["priority"] == priority ? workflow["specialPerson"]["personId"] : workflow["specialRole"]["roleId"]
                })
            }
        } else {
            res.status(400).json({
                status: false,
                message: "No workflows available with this data",
                error: error
            })
        }
    }).catch(error => {
        res.status(400).json({
            status: false,
            message: "Workflow details not found",
            error: error
        })
    })
}

/**
 * This function is used to findout reporting persons of specific user
 * 
 * @param {*} reported_by 
 * @param {*} reportsArray 
 * @author Jagadeesh Patta
 */

let findNextLevelPerson  = async (reported_by, reportsArray, filter_type, level_index) => {
    // console.log(reportsArray)        
    // console.log('reported by - ', reported_by)
    let reportedPerson = await UserModel.findById({_id: reported_by}).exec()
    // console.log('reported person job ' , filter_type , reportedPerson["job"][filter_type])
    // console.log('reports array length - ', reportsArray.length , ' level index - ', level_index)
    if (reportedPerson["job"][filter_type] == undefined || reportsArray.length == level_index) {
        // reportsArray.push(reportedPerson._id)
        return reportsArray;
    } else {
        reportsArray.push(reportedPerson["job"][filter_type])
        // console.log(reportsArray)
        let resultArray = await findNextLevelPerson(reportedPerson["job"][filter_type], reportsArray, filter_type, level_index)
        console.log('result array - ', resultArray)
        reportsArray.concat(resultArray)
    }
    return reportsArray;
}

let checkPersonInWorkflows = (req, res) => {
    let user_id = req.params.user_id
    let companyId = req.jwt.companyId
    return new Promise((resolve, reject) => {
        resolve(WorkflowModel.findOne({companyId: companyId, "specialPerson.personId": new ObjectID(user_id)}).lean().exec())
    })
    .then(workflow => {
        if (workflow) {
            res.status(200).json({
                status: true,
                message: "Success",
                data: {
                    isAssignedInWorkflow: true
                }
            })
        }
        else {
            res.status(200).json({
                status: true,
                message: "Success",
                data: {
                    isAssignedInWorkflow: false
                }
            })
        }
    })
    .catch(err => {
        console.log(err)
        res.status(500).json({
            status: false,
            message: "Error",
            error: err
        })
    })
}

module.exports = {
    createWorkflow,
    getWorkflow,
    updateWorkflow,
    deleteWorkflow,
    createNewLevels,
    getLevelsByType,
    findReportingOrHrPerson,
    getCurrentHighestLevel,
    checkPersonInWorkflows
}