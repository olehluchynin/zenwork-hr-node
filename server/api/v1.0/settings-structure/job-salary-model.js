let mongoose = require('mongoose')
let ObjectID = mongoose.Schema.ObjectId;

let JobTitleSalaryGradeLinkSchema = new mongoose.Schema({
    companyId: {type:ObjectID,ref:'companies', required: true},
    jobTitle: {type: ObjectID, ref: 'structures', required: true},
    EeoJobCategoryCode: {type: Number},
    salary_grade : {type:ObjectID,ref:'salary_grades'},
    grade_band: {type: ObjectID, ref: "grade_bands", unique: true, sparse: true },
    createdBy: {type:ObjectID,ref:'users'},
    updatedBy:{type:ObjectID,ref:'users'}
},{
    timestamps: true,
    versionKey: false
});

// JobTitleSalaryGradeLinkSchema.index({companyId: 1, grade_band: 1}, {unique: true, sparse: true})

let JobTitleSalaryGradeLinkModel = mongoose.model('job_salary_links', JobTitleSalaryGradeLinkSchema, 'job_salary_links');

module.exports = JobTitleSalaryGradeLinkModel;
