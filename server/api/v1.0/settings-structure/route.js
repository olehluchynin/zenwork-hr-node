let express = require('express');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let StructuresController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.get('/getFields', authMiddleware.isUserLogin, authMiddleware.isCompany, StructuresController.getFields);

router.post('/addUpdate', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, authMiddleware.isCompany, StructuresController.addUpdate);

// router.put('/update', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, authMiddleware.isCompany, StructuresController.update);

router.get('/getTotalStructure', authMiddleware.isUserLogin, authMiddleware.isCompany, StructuresController.getTotalStructure);

router.get(
    '/getSubDocs/:field', 
    (req,res, next) => {
        console.log('withouttoken = ', parseInt(req.query.withouttoken))
        if (req.query && parseInt(req.query.withouttoken)) {
            StructuresController.getSubDocs(req, res)
        }
        else if (req.query && !parseInt(req.query.withouttoken)){
            next()
        }
    },
    authMiddleware.isUserLogin, 
    // authMiddleware.isCompany, 
    StructuresController.getSubDocs);

router.delete('/:_id', authMiddleware.isUserLogin, authMiddleware.isCompany, StructuresController.del);

router.post('/uploadxlsx', authMiddleware.isUserLogin, authMiddleware.isCompany, multipartMiddleware, StructuresController.uploadxlsx);

router.get(
    '/getTrainingDetails', 
    authMiddleware.isUserLogin, 
    // authMiddleware.isCompany, 
    StructuresController.getTrainingDetails
);


router.get(
    '/getFieldsForAddEmployee', 
    authMiddleware.isUserLogin, 
    // authMiddleware.isCompany, 
    StructuresController.getFieldsForAddEmployee
);

router.get('/getFieldsForRoles', authMiddleware.isUserLogin, authMiddleware.isCompany, StructuresController.getFieldsForRoles);

router.get(
    '/getStandardAndCustomFields/:company_id',
    authMiddleware.isUserLogin,
    StructuresController.getStandardAndCustomFields
);

router.get(
    '/getJobSalaryLinkDetails/:jobTitleId',
    authMiddleware.isUserLogin,
    StructuresController.getJobSalaryLinkDetails
)

router.post(
    '/multi-del', 
    authMiddleware.isUserLogin, 
    authMiddleware.isCompany, 
    validationsMiddleware.reqiuresBody,
    StructuresController.multi_del
);

module.exports = router;
