/**
 * @apiDefine DataNotFoundError
 *
 * @apiError DataNotFound The data the you are requested was not found.
 *
 * @apiErrorExample Data Not Found Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "message": "Data Not Found"
 *       "data": null
 *     }
 */

/**
 * @apiDefine InternalServerError
 *
 * @apiError InternalServerError Internal Server error.
 *
 * @apiErrorExample Internal Server Error:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "status": false,
 *       "message": "Internal server error"
 *       "data": null
 *     }
 */

/**
 * @apiDefine TokenNotFound
 *
 * @apiError TokenNotFound User Token Not Found.
 *
 * @apiErrorExample Token Not Found:
 *     HTTP/1.1 400 Token Not Found
 *     {
 *       "status": false,
 *       "message": "Token Not Found"
 *       "data": null
 *     }
 */

/**
 * @apiDefine BadRequest
 *
 * @apiError BadRequest Bad request.
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": false,
 *       "message": "Invalid key"
 *       "data": null
 *     }
 */

/**
 * @apiDefine AccessDeneid
 *
 * @apiError AccessDenied Access denied for this operation.
 *
 * @apiErrorExample Access Deneid:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "status": false,
 *       "message": "Access Denied for this operation"
 *       "data": null
 *     }
 */

/**
 * @apiDefine DataAlreadyExists
 *
 * @apiError DataConflict Date already exists.
 *
 * @apiErrorExample Data Conflict Error:
 *     HTTP/1.1 402 Data Conflict
 *     {
 *       "status": false,
 *       "message": "Data already exist"
 *       "data": null
 *     }
 */


/**
 * @api {get} /structure/getFields Get Fields
 * @apiName getFields
 * @apiGroup Structures
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Successfully",
    "data": [
        {
            "name": "Termination Reason",
            "isChecked": true
        },
        {
            "name": "Pay Type",
            "isChecked": false
        },
        {
            "name": "Pay Frequency",
            "isChecked": false
        },
        {
            "name": "Employee Type",
            "isChecked": false
        },
        {
            "name": "FLSA Code",
            "isChecked": false
        },
        {
            "name": "EEO Job Category",
            "isChecked": false
        },
        {
            "name": "Employment Status",
            "isChecked": false
        },
        {
            "name": "Emergency Contact Relationship",
            "isChecked": false
        },
        {
            "name": "Assets",
            "isChecked": false
        },
        {
            "name": "Training Subject",
            "isChecked": false
        },
        {
            "name": "Training Type",
            "isChecked": false
        },
        {
            "name": "Training - Average Time to Complete",
            "isChecked": false
        },
        {
            "name": "Union",
            "isChecked": false
        },
        {
            "name": "Location",
            "isChecked": false
        },
        {
            "name": "Department",
            "isChecked": false
        },
        {
            "name": "Termination Type",
            "isChecked": false
        },
        {
            "name": "Document Type",
            "isChecked": false
        },
        {
            "name": "Visa Type",
            "isChecked": false
        },
        {
            "name": "Visa Status",
            "isChecked": false
        },
        {
            "name": "Schedule Type",
            "isChecked": false
        },
        {
            "name": "Workflow Request Reasons",
            "isChecked": false
        },
        {
            "name": "Martial Status",
            "isChecked": false
        },
        {
            "name": "Veteran Status",
            "isChecked": false
        },
        {
            "name": "Race",
            "isChecked": false
        },
        {
            "name": "Ethnicity",
            "isChecked": false
        },
        {
            "name": "State",
            "isChecked": false
        },
        {
            "name": "Country",
            "isChecked": false
        },
        {
            "name": "Language",
            "isChecked": false
        },
        {
            "name": "Language Fluency",
            "isChecked": false
        },
        {
            "name": "Rehire Status",
            "isChecked": false
        },
        {
            "name": "Onboarding Template Tasks",
            "isChecked": false
        },
        {
            "name": "Offboarding Template Tasks",
            "isChecked": false
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */


/**
 * @api {post} /structure/addUpdate Add/Update Structure Data
 * @apiName addStructureData
 * @apiGroup Structures
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example1:
 *
 * [{
	"_id":"5ca4a042eb774d0825c1d457",
	"name":"Systemed",
	"code":"xyz",
	"status":"1",   //[0(inactive),1(active)];
	"parentRef":"Department"    //["Location","Department","Business Unit","Salary Grade"]
},{
	"name":"Backedfvddnd1",
	"code":"xyz",
	"parentRef":"Department"
},{
	"name":"Backedfvddnd2",
	"code":"xyz",
	"parentRef":"Department"
}]
* @apiParamExample {json} Request-Example2:
[
    {
        "status": 1,
        "_id": "5d4179e9ca1b3f3cdd6e8674",
        "name": "Product Manager",
        "parentRef": "Job Title",
        "updatedBy": "5c9e11894b1d726375b23057",
        "companyId": "5c9e11894b1d726375b23058",
        "createdBy": "5c9e11894b1d726375b23057",
        "prefix": "JT",
        "code": 9,
        "createdAt": "2019-07-31T11:22:17.784Z",
        "updatedAt": "2019-07-31T11:22:17.784Z",
        "structureId": 239,
        "editField": false,
        "isChecked": true
    },
    {
    	"name": "People manager",
        "parentRef": "Job Title",
        "status": 1
    },
    {
    	"name": "SDE II",
        "parentRef": "Job Title",
        "status": 1,
        "salary_grade": "5d4162c5ca1b3f3cdd6e866f",
        "grade_band": "5d416517ca1b3f3cdd6e8671",
        "EeoJobCategoryCode": "EJC3"
    }
]
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Structure Field created successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */



/**
 * @api {put} /structure/update Update Structure
 * @apiName updateStructuresData
 * @apiGroup Structures
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
 *
 *{
    "status": 0,
    "_id": "5ca49ff01e82fc07c0f9f8d2",
    "name": "Hyderabad",
    "code": "xyz",
    "parentRef": "Location",
    "companyId": "5c9e11894b1d726375b23058",
    "createdBy": "5c9e11894b1d726375b23057",
    "updatedBy": "5c9e11894b1d726375b23057",
    "createdAt": "2019-04-03T11:58:40.333Z",
    "updatedAt": "2019-04-03T11:58:40.333Z",
    "structureId": 1
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Data updated successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */



/**
 * @api {get} /structure/getTotalStructure  Get Total Structure
 * @apiName getTotalStructure
 * @apiGroup Structures
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Successfully",
    "groupedData": {
        "Location": [
            {
                "status": 0,
                "_id": "5ca49ff01e82fc07c0f9f8d2",
                "name": "Hyderabad",
                "code": "xyz",
                "parentRef": "Location",
                "companyId": "5c9e11894b1d726375b23058",
                "createdBy": "5c9e11894b1d726375b23057",
                "updatedBy": "5c9e11894b1d726375b23057",
                "createdAt": "2019-04-03T11:58:40.333Z",
                "updatedAt": "2019-04-03T11:58:40.333Z",
                "structureId": 1
            },
            {
                "status": 0,
                "_id": "5ca4a00ceb774d0825c1d453",
                "name": "Banglore",
                "code": "xyz",
                "parentRef": "Location",
                "companyId": "5c9e11894b1d726375b23058",
                "createdBy": "5c9e11894b1d726375b23057",
                "updatedBy": "5c9e11894b1d726375b23057",
                "createdAt": "2019-04-03T11:59:08.234Z",
                "updatedAt": "2019-04-03T11:59:08.234Z",
                "structureId": 2
            }
        ],
        "Department": [
            {
                "status": 0,
                "_id": "5ca4a03aeb774d0825c1d456",
                "name": "HR",
                "code": "xyz",
                "parentRef": "Department",
                "companyId": "5c9e11894b1d726375b23058",
                "createdBy": "5c9e11894b1d726375b23057",
                "updatedBy": "5c9e11894b1d726375b23057",
                "createdAt": "2019-04-03T11:59:54.321Z",
                "updatedAt": "2019-04-03T11:59:54.321Z",
                "structureId": 5
            },
            {
                "status": 0,
                "_id": "5ca4a042eb774d0825c1d457",
                "name": "System",
                "code": "xyz",
                "parentRef": "Department",
                "companyId": "5c9e11894b1d726375b23058",
                "createdBy": "5c9e11894b1d726375b23057",
                "updatedBy": "5c9e11894b1d726375b23057",
                "createdAt": "2019-04-03T12:00:02.047Z",
                "updatedAt": "2019-04-03T12:00:02.047Z",
                "structureId": 6
            }
        ]
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */


/**
 * @api {get} /structure/getSubDocs/:field  Get SubDocs Of Field
 * @apiName getSubDocsOfField
 * @apiGroup Structures
 * @apiHeader {String} x-access-token User login token.

 * @apiParam {String} field Field Name (["Location","Department","Business Unit","Salary Grade"])
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Success",
    "default": [
        {
            "name": "American Indian or Alaska Native",
            "code": "R1"
        },
        {
            "name": "Asian",
            "code": "R2"
        },
        {
            "name": "Black or African American",
            "code": "R3"
        },
        {
            "name": "Native Hawaiian or Other Pacific Islander",
            "code": "R4"
        },
        {
            "name": "White",
            "code": "R5"
        }
    ],
    "custom": [
        {
            "status": 0,
            "_id": "5cb18197b581a72b3dc2377a",
            "name": "Backedfv1",
            "code": 6,
            "parentRef": "Race",
            "updatedBy": "5c9e11894b1d726375b23057",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5c9e11894b1d726375b23057",
            "prefix": "R",
            "createdAt": "2019-04-13T06:28:39.680Z",
            "updatedAt": "2019-04-13T06:30:32.892Z",
            "structureId": 170
        },
        {
            "status": 0,
            "_id": "5cb18197b581a72b3dc2377b",
            "name": "Backedfvddnd2",
            "code": 7,
            "parentRef": "Race",
            "updatedBy": "5c9e11894b1d726375b23057",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5c9e11894b1d726375b23057",
            "prefix": "R",
            "createdAt": "2019-04-13T06:28:39.680Z",
            "updatedAt": "2019-04-13T06:28:39.680Z",
            "structureId": 169
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

/**
 * @api {get} /structure/getStandardAndCustomFields/:company_id?fields=['Location','Department']  GetStandardAndCustomFields
 * @apiName GetStandardAndCustomFields
 * @apiGroup Structures
 * @apiHeader {String} x-access-token User login token.
 * @apiDescription Gives values of all fields when query parameter is absent in the request.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {Array} fields Array of field names ["Location","Department"]
 *
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched standard and custom fields",
    "data": {
        "Location": {
            "default": [],
            "custom": [
                {
                    "status": 1,
                    "_id": "5cb1d02318598b356a883bc3",
                    "name": "Banglore",
                    "parentRef": "Location",
                    "updatedBy": "5c9e11894b1d726375b23057",
                    "companyId": "5c9e11894b1d726375b23058",
                    "createdBy": "5c9e11894b1d726375b23057",
                    "prefix": "LO",
                    "code": 1,
                    "createdAt": "2019-04-13T12:03:47.794Z",
                    "updatedAt": "2019-04-15T18:33:00.642Z",
                    "structureId": 177
                },
                {
                    "status": 0,
                    "_id": "5cb1d2705b654577fd344ea4",
                    "parentRef": "Location",
                    "name": "b",
                    "code": 2,
                    "companyId": "5c9e11894b1d726375b23058",
                    "createdBy": "5c9e11894b1d726375b23057",
                    "updatedBy": "5c9e11894b1d726375b23057",
                    "prefix": "LO",
                    "createdAt": "2019-04-13T12:13:36.475Z",
                    "updatedAt": "2019-04-13T12:13:36.475Z",
                    "structureId": 184
                },
                {
                    "status": 1,
                    "_id": "5cb1d2715b654577fd344ea5",
                    "parentRef": "Location",
                    "name": "c edit loc",
                    "code": 3,
                    "companyId": "5c9e11894b1d726375b23058",
                    "createdBy": "5c9e11894b1d726375b23057",
                    "updatedBy": "5c9e11894b1d726375b23057",
                    "prefix": "LO",
                    "createdAt": "2019-04-13T12:13:37.118Z",
                    "updatedAt": "2019-04-15T18:33:00.643Z",
                    "structureId": 185
                },
                {
                    "status": 0,
                    "_id": "5cb1d2715b654577fd344ea6",
                    "parentRef": "Location",
                    "name": "d",
                    "code": 4,
                    "companyId": "5c9e11894b1d726375b23058",
                    "createdBy": "5c9e11894b1d726375b23057",
                    "updatedBy": "5c9e11894b1d726375b23057",
                    "prefix": "LO",
                    "createdAt": "2019-04-13T12:13:37.759Z",
                    "updatedAt": "2019-04-13T12:13:37.759Z",
                    "structureId": 186
                },
                {
                    "status": 1,
                    "_id": "5cb4ce2021762f54e031b066",
                    "name": "Nick Location",
                    "parentRef": "Location",
                    "updatedBy": "5c9e11894b1d726375b23057",
                    "companyId": "5c9e11894b1d726375b23058",
                    "createdBy": "5c9e11894b1d726375b23057",
                    "prefix": "LO",
                    "code": 5,
                    "createdAt": "2019-04-15T18:32:00.244Z",
                    "updatedAt": "2019-04-15T18:33:00.643Z",
                    "structureId": 191
                }
            ]
        },
        "Department": {
            "default": [],
            "custom": [
                {
                    "status": 1,
                    "_id": "5cb1d26f5b654577fd344ea3",
                    "parentRef": "Department",
                    "name": "Tech",
                    "code": 1,
                    "companyId": "5c9e11894b1d726375b23058",
                    "createdBy": "5c9e11894b1d726375b23057",
                    "updatedBy": "5c9e11894b1d726375b23057",
                    "prefix": "DP",
                    "createdAt": "2019-04-13T12:13:35.834Z",
                    "updatedAt": "2019-05-09T08:51:29.778Z",
                    "structureId": 183
                },
                {
                    "status": 1,
                    "_id": "5cb1d2725b654577fd344ea7",
                    "parentRef": "Department",
                    "name": "Sales",
                    "code": 2,
                    "companyId": "5c9e11894b1d726375b23058",
                    "createdBy": "5c9e11894b1d726375b23057",
                    "updatedBy": "5c9e11894b1d726375b23057",
                    "prefix": "DP",
                    "createdAt": "2019-04-13T12:13:38.392Z",
                    "updatedAt": "2019-05-09T08:51:29.779Z",
                    "structureId": 187
                },
                {
                    "status": 1,
                    "_id": "5cd3ea11c583847552fa33ce",
                    "name": "IT",
                    "parentRef": "Department",
                    "updatedBy": "5c9e11894b1d726375b23057",
                    "companyId": "5c9e11894b1d726375b23058",
                    "createdBy": "5c9e11894b1d726375b23057",
                    "prefix": "DP",
                    "code": 3,
                    "createdAt": "2019-05-09T08:51:29.780Z",
                    "updatedAt": "2019-05-09T08:51:29.780Z",
                    "structureId": 209
                }
            ]
        },
        "Business Unit": {
            "default": [],
            "custom": [
                {
                    "status": 1,
                    "_id": "5cbdaecce1c6df4dba2ea520",
                    "name": "Unit 1",
                    "parentRef": "Business Unit",
                    "updatedBy": "5c9e11894b1d726375b23057",
                    "companyId": "5c9e11894b1d726375b23058",
                    "createdBy": "5c9e11894b1d726375b23057",
                    "prefix": "BN",
                    "code": 1,
                    "createdAt": "2019-04-22T12:08:44.530Z",
                    "updatedAt": "2019-04-22T12:08:44.530Z",
                    "structureId": 196
                },
                {
                    "status": 1,
                    "_id": "5cbdaecce1c6df4dba2ea521",
                    "name": "Unit 2",
                    "parentRef": "Business Unit",
                    "updatedBy": "5c9e11894b1d726375b23057",
                    "companyId": "5c9e11894b1d726375b23058",
                    "createdBy": "5c9e11894b1d726375b23057",
                    "prefix": "BN",
                    "code": 2,
                    "createdAt": "2019-04-22T12:08:44.530Z",
                    "updatedAt": "2019-04-22T12:08:44.530Z",
                    "structureId": 197
                }
            ]
        },
        "Salary Grade": {
            "default": [],
            "custom": []
        }
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

/**
 * @api {delete} /structure/:_id    Delete A SubDoc Record
 * @apiName deleteSingleStructureRecord
 * @apiGroup Structures
 * @apiHeader {String} x-access-token User login token.

 * @apiParam {String} _id Record unique _id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Successfully Removed"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */


/**
 * @api {post} /structure/uploadxlsx    Upload xlsx file
 * @apiName uploadxlsxfile
 * @apiGroup Structures
 * @apiHeader {String} x-access-token User login token.

 * @apiParam {file} file file.xlsx
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Successfully Removed"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */


/**
 * @api {get} /structure/getJobSalaryLinkDetails/:jobTitleId   GetJobSalaryLinkDetails
 * @apiName GetJobSalaryLinkDetails
 * @apiGroup Structures
 * @apiHeader {String} x-access-token User login token.

 * @apiParam {ObjectId} jobTitleId structures model unique _id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Fetched job salary details successfully",
    "data": {
        "_id": "5d529683a07285293e8dcb59",
        "createdBy": "5c9e11894b1d726375b23057",
        "companyId": "5c9e11894b1d726375b23058",
        "jobTitle": "5cd3bda9c583847552fa3383",
        "createdAt": "2019-08-13T10:52:51.326Z",
        "updatedAt": "2019-08-14T06:59:02.879Z",
        "updatedBy": "5c9e11894b1d726375b23057",
        "EeoJobCategoryCode": 10,
        "grade_band": {
            "_id": "5d52b409e8b6c22a3deabbb0",
            "grade": "qwe",
            "min": 23,
            "mid": 233,
            "max": 444,
            "average_salary": null,
            "average_market_value": null,
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5c9e11894b1d726375b23057",
            "salary_grade": "5d52b409e8b6c22a3deabbaf",
            "createdAt": "2019-08-13T12:58:49.094Z",
            "updatedAt": "2019-08-13T20:58:35.919Z",
            "updatedBy": "5c9e11894b1d726375b23057"
        },
        "salary_grade": {
            "_id": "5d52b409e8b6c22a3deabbaf",
            "archive": false,
            "companyId": "5c9e11894b1d726375b23058",
            "salary_grade_name": "qwewqe",
            "pay_frequency": "Weekly",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-08-13T12:58:49.092Z",
            "updatedAt": "2019-08-13T20:58:35.916Z",
            "updatedBy": "5c9e11894b1d726375b23057"
        },
        "EeoJobCategoryDetails": {
            "name": "Service Workers",
            "code": "EJC10"
        }
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

/**
 * @api {post} /structure/multi-del DeleteStructureFields
 * @apiName DeleteStructureFields
 * @apiGroup Structures
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
 *
{
    "_ids": [
        "5ceba79bb538bd4df7bac6ea"
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */
