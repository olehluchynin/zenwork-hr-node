let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');
let StructureFieldsL1 = require('./../../../common/structures').StructureFieldsL1;

let Schema = new mongoose.Schema({
    companyId: {type:ObjectID,ref:'companies'},

    // type: {type: String, enum: ['structureField', 'currentValue']},
    name: {type: String, required: true},
    prefix: {type: String},
    code: {type: Number},
    status: {type: Number, default:0, enum: constants.activeStatus},
    parentRef:{type: String, enum: constants.StructureFieldsL1},

    createdBy: {type:ObjectID,ref:'users'},
    updatedBy:{type:ObjectID,ref:'users'},
},{
    timestamps: true,
    versionKey: false
});

Schema.plugin(autoIncrement.plugin, {model:'structures', field:'structureId', startAt: 1, incrementBy: 1});

Schema.index({companyId: 1, parentRef: 1, name: 1}, {unique: true});

let StructuresModel = mongoose.model('structures',Schema);

module.exports = StructuresModel;
