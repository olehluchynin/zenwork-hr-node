let mongoose = require('mongoose');
let _ = require('underscore');
let ObjectID = mongoose.Types.ObjectId;
var xlsx = require('node-xlsx');
var async = require('async');
let StructureModel = require('./model');
let JobSalaryLinkModel = require('./job-salary-model');
let CompanySettingsModel = require('./../companies/settings/model');
let utils = require('./../../../common/utils');
let mailer = require('./../../../common/mailer');
let constants = require('./../../../common/constants');
let StructureFieldLastNumbers = require('./../../../common/structures').StructureFieldLastNumbers;
let StructureFieldPrefixes = require('./../../../common/structures').StructureFieldPrefixes;
let StructureFieldsL1 = require('./../../../common/structures').StructureFieldsL1;
let StructureFieldsL2 = require('./../../../common/structures').StructureFieldsL2;

let alphaCompare = (a, b) => {
    var nameA = a.name.toUpperCase(); // ignore upper and lowercase
    var nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
  
    // names must be equal
    return 0;
  }
/**
 * Author:Sai Reddy  Date:13/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To GET Total Structure fields.
 */
let getFields = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        res.status(200).json({status: true, message: "Successfully", data:StructureFieldsL1.sort(alphaCompare)});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let getFieldsForRoles = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        res.status(200).json({status: true, message: "Successfully", data:[
                {name: "Business Unit", value:'businessUnit', isChecked: true},
                {name: "Department", value:'department', isChecked: false},
                {name: "Location", value:'location', isChecked: false},
                {name: "Employee Type", value:'employeeType', isChecked: false},
                {name: "Union", value:'unionFields', isChecked: false}
            ]});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:03/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To Add Structure Field
 */
let addUpdate = (req, res) => {
    var body = req.body;
    let companyId = req.jwt.companyId
    let userId = req.jwt.userId
    return new Promise((resolve, reject) => {
        if (body.length < 1) reject("No data to add or update");
        else if (body[0].parentRef == 'Job Title') {
            let grade_bands = body.map(x => x.grade_band)
            grade_bands = grade_bands.filter(x => x != null)
            console.log('grade_bands -', grade_bands)
            if (grade_bands.length != new Set(grade_bands).size) {
                reject('Grade bands must be unique');
            }
            else {
                let names = body.map(x => x.name)
                let default_names = StructureFieldsL2[body[0].parentRef].map(x => x.name)
                names = names.concat(default_names)
                console.log('names - ', names)
                if (names.length != new Set(names).size) {
                    reject('Duplicate record is not allowed');
                }
                else {
                    resolve(null);
                }
            }
        }
        else {
            let names = body.map(x => x.name)
            let default_names = StructureFieldsL2[body[0].parentRef].map(x => x.name)
            names = names.concat(default_names)
            console.log('names - ', names)
            if (names.length != new Set(names).size) {
                reject('Duplicate record is not allowed');
            }
            else {
                resolve(null);
            }
        }
    }).then(() => {
        for (let structureValue of body) {
            if(!structureValue.name) {
                throw 'Name is required'
            }
            if(!structureValue.hasOwnProperty('status')) {
                throw 'Status is required'
            }
            if(structureValue.status === '') {
                throw 'Status is required'
            }
        }
        return StructureModel.findOne({companyId: companyId,parentRef:body[0].parentRef}).sort({_id:-1});
    }).then(async doc => {
        let n;
        let parentRef = body[0].parentRef;
        let queries = [];
        if (parentRef == 'Job Title') {
            if(doc) n = doc.code;
            else n = StructureFieldLastNumbers[parentRef];
            

            for (let body_doc of body) {
                let jobStructureDoc = { updatedBy: userId };
                let jobSalaryLinkDoc;
                
                if (body_doc._id) {
                    jobStructureDoc['name'] = body_doc.name
                    jobStructureDoc['status'] = body_doc.status
                    queries.push(StructureModel.findOneAndUpdate({companyId: companyId, _id: body_doc._id}, {$set: jobStructureDoc}, {new: true}))
                    // jobSalaryLinkDoc = await JobSalaryLinkModel.findOne({companyId: companyId, jobTitleCode: body_doc.code}).lean().exec()
                    jobSalaryLinkDoc = await JobSalaryLinkModel.findOne({companyId: companyId, jobTitle: body_doc._id}).lean().exec()
                    if (jobSalaryLinkDoc) {
                        let update_doc = {
                            updatedBy : userId
                        }
                        if (typeof body_doc.EeoJobCategoryCode === 'string') {
                            update_doc['EeoJobCategoryCode'] = parseInt(body_doc.EeoJobCategoryCode.replace('EJC', ''))
                        }
                        else if (typeof body_doc.EeoJobCategoryCode === 'number'){
                            update_doc['EeoJobCategoryCode'] = body_doc.EeoJobCategoryCode
                        }
    
                        if (body_doc.salary_grade) {
                            update_doc['salary_grade'] = body_doc.salary_grade
                        }
                        if (body_doc.grade_band) {
                            update_doc['grade_band'] = body_doc.grade_band
                        }
                        
                        queries.push(JobSalaryLinkModel.findOneAndUpdate(
                            {
                                companyId: companyId, jobTitle: body_doc._id
                            }, 
                            {
                                $set: update_doc
                            },
                            { new : true}
                        ))
                    }
                    else {
                        jobSalaryLinkDoc = {
                            createdBy: userId,
                            companyId: companyId,
                            // jobTitleCode: body_doc.code
                            jobTitle: body_doc._id
                        }
                        console.log(jobSalaryLinkDoc)
                        if (typeof body_doc.EeoJobCategoryCode === 'string') {
                            jobSalaryLinkDoc['EeoJobCategoryCode'] = parseInt(body_doc.EeoJobCategoryCode.replace('EJC', ''))
                        }
                        else if (typeof body_doc.EeoJobCategoryCode === 'number'){
                            jobSalaryLinkDoc['EeoJobCategoryCode'] = body_doc.EeoJobCategoryCode
                        }
    
                        if (body_doc.salary_grade) {
                            jobSalaryLinkDoc['salary_grade'] = body_doc.salary_grade
                        }
                        if (body_doc.grade_band) {
                            jobSalaryLinkDoc['grade_band'] = body_doc.grade_band
                        }
                        queries.push(JobSalaryLinkModel.create(jobSalaryLinkDoc))
                    }
                }
                else {
                    jobStructureDoc['name'] = body_doc.name
                    jobStructureDoc['status'] = body_doc.status
                    jobStructureDoc['parentRef'] = body_doc.parentRef
                    jobStructureDoc['companyId'] = companyId;
                    jobStructureDoc.createdBy = userId;
                    jobStructureDoc.prefix = StructureFieldPrefixes[parentRef];
                    n = n+1;
                    jobStructureDoc.code = n;
                    // queries.push(StructureModel.create(jobStructureDoc))
                    let new_job_structure_doc = await StructureModel.create(jobStructureDoc)
                    jobSalaryLinkDoc = {
                        createdBy: userId,
                        companyId: companyId,
                        // jobTitleCode: n
                        jobTitle: new_job_structure_doc._id
                    }
                    console.log(jobSalaryLinkDoc)
                    if (typeof body_doc.EeoJobCategoryCode === 'string') {
                        jobSalaryLinkDoc['EeoJobCategoryCode'] = parseInt(body_doc.EeoJobCategoryCode.replace('EJC', ''))
                    }
                    else if (typeof body_doc.EeoJobCategoryCode === 'number'){
                        jobSalaryLinkDoc['EeoJobCategoryCode'] = body_doc.EeoJobCategoryCode
                    }

                    if (body_doc.salary_grade) {
                        jobSalaryLinkDoc['salary_grade'] = body_doc.salary_grade
                    }
                    if (body_doc.grade_band) {
                        jobSalaryLinkDoc['grade_band'] = body_doc.grade_band
                    }
                    queries.push(JobSalaryLinkModel.create(jobSalaryLinkDoc))
                }
            }
        }
        else {
            if(doc) n = doc.code;
            else n = StructureFieldLastNumbers[parentRef];
            
            body.forEach(function (each) {
                queries.push(new Promise((resolve, reject) => {
                    each.updatedBy = req.jwt.userId;
                    if(each._id) {
                        delete each.code;
                        StructureModel.update({_id:each._id},{$set:each})
                            .then((response) => {
                                resolve(response);
                            }).catch((err) => {
                            console.log("==> ",err);
                            reject(err);
                        });
                    } else {
                        each.companyId = req.jwt.companyId;
                        each.createdBy = req.jwt.userId;
                        each.prefix = StructureFieldPrefixes[parentRef];
                        n = n+1;
                        each.code = n;
                        // console.log(each);
                        StructureModel.create(each)
                            .then((response) => {
                                resolve(response);
                            }).catch((err) => {
                            console.log("==> ",err);
                            reject(err);
                        });
                    }
                }));
            });
        }
        
        // console.log(body, queries);
        return Promise.all(queries);
    }).then(data => {
        res.status(200).json({status: true, message: "Structure Field created successfully"});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:03/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To Update Training Library data
 */
let update = (req, res) => {
    var body = req.body;
    return new Promise((resolve, reject) => {
        if (!body.name) reject("Please enter name.");
        else if (!body.code) reject("Please enter code.");
        else resolve(null);
    }).then(() => {
        delete body.structureId;
        body.updatedBy = req.jwt.userId;
        return StructureModel.update({_id:body._id},body).exec();
    }).then(data => {
        res.status(200).json({status: true, message: "Data updated successfully"});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:03/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To GET Total Structure data
 */
let getTotalStructure = (req, res) => {
    var body = req.body;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return StructureModel.find({companyId:req.jwt.companyId});
    }).then(data => {
        var groupedData = _.groupBy(data, 'parentRef');
        res.status(200).json({status: true, message: "Successfully", groupedData});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:03/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To GET SubDocs of Fields data
 */
let getSubDocs = (req, res) => {
    console.log(req.params);
    if(!req.jwt) {
        console.log('no jwt - sending empty custom array')
        return new Promise((resolve, reject) => {
            resolve(null);
        }).then(() => {
            res.status(200).json({status: true, message: "Success", default:StructureFieldsL2[req.params.field], custom:[]});
        }).catch((err) => {
            console.log(err);
            res.status(400).json({status: false, message: err, data: null});
        });
    }
    else {
        if (req.params.field == 'Job Title') {
            return new Promise((resolve, reject) => {
                resolve(
                    Promise.all([
                        // JobSalaryLinkModel.find({companyId: req.jwt.companyId}).populate("salary_grade").populate("grade_band").lean().exec(),
                        JobSalaryLinkModel.find({companyId: req.jwt.companyId}).lean().exec(),
                        StructureModel.find({companyId: req.jwt.companyId, parentRef: req.params.field}).lean().exec(),
                        StructureModel.find({companyId: req.jwt.companyId, parentRef: "EEO Job Category"}).lean().exec(),
                    ])
                )
            })
            .then(([job_salary_links, job_title_values, eeo_category_values]) => {
                for (let value of job_title_values) {
                    let job_salary_link = job_salary_links.filter(x => String(x.jobTitle) == String(value._id))
                    job_salary_link = job_salary_link[0]
                    if (job_salary_link) {
                        value.salary_grade = job_salary_link.salary_grade
                        value.grade_band = job_salary_link.grade_band
                        if (job_salary_link.EeoJobCategoryCode) {
                            value.EeoJobCategoryCode = 'EJC'+ job_salary_link.EeoJobCategoryCode
                        }
                    }
                }
                res.status(200).json({
                    status: true, message: "Success", default:[], custom: job_title_values
                })
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({status: false, message: err, data: null});
            })
        }
        else {
            return new Promise((resolve, reject) => {
                resolve(null);
            }).then(() => {
                return StructureModel.find({companyId: req.jwt.companyId, parentRef: req.params.field, status: 1});
            }).then(data => {
                res.status(200).json({status: true, message: "Success", default:StructureFieldsL2[req.params.field], custom:data});
            }).catch((err) => {
                console.log(err);
                res.status(400).json({status: false, message: err, data: null});
            });
        }
        
    }
    
};


/**
 * Author:Sai Reddy  Date:03/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To GET Single Training Library data
 */
let del = (req, res) => {
    var id = req.params._id;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return StructureModel.findOneAndRemove({_id:id, companyId: req.jwt.companyId}).exec();
    }).then((data) => {
        if(data) {
            res.status(200).json({status: true, message: "Successfully Removed"});
        } else {
            res.status(404).json({status: false, message: "Data not found with that id on your company"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let multi_del = (req, res) => {
    let companyId = req.jwt.companyId
    let body = req.body
    return new Promise((resolve, reject) => {
        if (!body._ids) reject('_ids are required');
        else if(body._ids.length <= 0) {
            reject('_ids are required');
        }
        else resolve(null);
    })
    .then(() => {
        StructureModel.deleteMany({companyId: companyId, _id: {$in: body._ids}})
            .then(response => {
                res.status(200).json({
                    status: true,
                    message: "Deleted successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while deleting structure fields",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

/**
 * Author:Sai Reddy  Date:04/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To GET Single Training Library data
 */
let uploadxlsx = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let file;
        if(req.files) file = req.files.file;
        // console.log('body ==> ',req.body);
        // console.log('files ==> ',req.files);
        // console.log(file);

        var data = xlsx.parse(file.path)[0].data;
        // console.log("===========>", data);
        // console.log("===========>", data[0].data);
        let toAdd = [];
        for(let i = 1;i < data.length;i++) {
            let obj = {};
            if(data[i][0] && data[i][1] && data[i][2] && data[i][3]) {
                obj.parentRef = data[i][0];
                obj.name = data[i][1];
                obj.code = data[i][2];
                obj.status = data[i][3] === 'Active' ? 1 : 0;
                obj.companyId = req.jwt.companyId;
                obj.createdBy = req.jwt.userId;
                obj.updatedBy = req.jwt.userId;
                toAdd.push(obj);
            }
        }
        return toAdd;
        // return StructureModel.create(toAdd);
    }).then(async toAdd => {
        // async function processArray(array) {
            for (const each of toAdd) {
                let doc = await StructureModel.findOne({companyId:req.jwt.companyId,parentRef:each.parentRef}).sort({_id:-1});
                let n;
                let parentRef = each.parentRef;
                if(doc) n = doc.code;
                else n = StructureFieldLastNumbers[parentRef];

                each.prefix = StructureFieldPrefixes[parentRef];
                n = n+1;
                each.code = n;
                // console.log(doc, each, n);
                let created = await StructureModel.create(each);
                // console.log(created);
            }
            console.log('Done!');
        // }
        return toAdd;
    }).then((data) => {
        // console.log(data);
        if(data) {
            res.status(200).json({status: true, message: "Successfully Added "+data.length+' Records'});
        } else {
            res.status(200).json({status: true, message: "Data not found in the file"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let readXlsx = () => {
    var obj = xlsx.parse(__dirname + '/import.xlsx');
    console.log("===========>", obj);
    console.log("===========>", obj[0].data);
};


/**
 * Author:Sai Reddy  Date:10/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-8
 * Login API To get training structure data
 */
let getTrainingDetails = (req, res) => {
    let body = req.body;
    return new Promise( (resolve, reject) => {
        resolve(null);
    }).then(() => {
        return StructureModel.find({companyId: req.jwt.companyId, parentRef: {$in: ['Training Subject','Training Type','Training - Average Time to Complete']},status:1},{name:1,parentRef:1});
    }).then((data) => {
        if(data) {
            data = _.groupBy(data, function (each) {
                return each.parentRef;
            });
            if(!data['Training Subject']) data['Training Subject'] = [];
            if(!data['Training Type']) data['Training Type'] = [];
            if(!data['Training - Average Time to Complete']) data['Training - Average Time to Complete'] = [];
            let temp = {
                'Training Subject': StructureFieldsL2['Training Subject'].concat(data['Training Subject']),
                'Training Type': StructureFieldsL2['Training Type'].concat(data['Training Type']),
                'Training - Average Time to Complete': StructureFieldsL2['Training - Average Time to Complete'].concat(data['Training - Average Time to Complete']),
            };
            res.status(200).json({status: true, message: "Success",temp});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};



let getFieldsForAddEmployee = (req, res) => {
    let companyId = req.jwt.companyId;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return Promise.all([
            StructureModel.find({companyId: req.jwt.companyId, parentRef: {$in: ["Employee Type","Job Title","Department","Location","Business Unit","Union","FLSA Code","EEO Job Category","Pay Frequency","Pay Type"]},status:1},{name:1,parentRef:1}),
            CompanySettingsModel.findOne({companyId:companyId},{idNumber:1,_id:0})
        ]);
    }).then(([data, settings]) => {
        if(data) {
            data = _.groupBy(data, function (each) {
                return each.parentRef;
            });
            if(!data['Employee Type']) data['Employee Type'] = [];
            if(!data['Job Title']) data['Job Title'] = [];
            if(!data['Department']) data['Department'] = [];
            if(!data['Location']) data['Location'] = [];
            if(!data['Business Unit']) data['Business Unit'] = [];
            if(!data['Union']) data['Union'] = [];
            if(!data['FLSA Code']) data['FLSA Code'] = [];
            if(!data['EEO Job Category']) data['EEO Job Category'] = [];
            if(!data['Pay Frequency']) data['Pay Frequency'] = [];
            if(!data['Pay Type']) data['Pay Type'] = [];
            let temp = {
                'Employee Type': StructureFieldsL2['Employee Type'].concat(data['Employee Type']),
                'Job Title': StructureFieldsL2['Job Title'].concat(data['Job Title']),
                'Department': StructureFieldsL2['Department'].concat(data['Department']),
                'Location': StructureFieldsL2['Location'].concat(data['Location']),
                'Business Unit': StructureFieldsL2['Business Unit'].concat(data['Business Unit']),
                'Union': StructureFieldsL2['Union'].concat(data['Union']),
                'FLSA Code': StructureFieldsL2['FLSA Code'].concat(data['FLSA Code']),
                'EEO Job Category': StructureFieldsL2['EEO Job Category'].concat(data['EEO Job Category']),
                // 'Pay Frequency': StructureFieldsL2['Pay Frequency'].concat(data['Pay Frequency']),
                // 'Pay Type': StructureFieldsL2['Pay Type'].concat(data['Pay Type']),
            };
            res.status(200).json({status: true, message: "Success",data:temp,settings});
        } else {
            res.status(404).json({status: false, message: "Company not found"});
        }
    }).catch((err) => {
        console.log(err);
        return res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Method to get standard and cutom values of all the fields or given fields
 * @param {*} req 
 * @param {*} res 
 * @author Asma
 */
let getStandardAndCustomFields = (req, res) => {
    var query = req.query;
    var companyId = req.params.company_id
    if (query.fields) {
        // console.log(query.fields)
        var fields = JSON.parse(query.fields)
        console.log(fields)
        if (fields && fields.length > 0) {
            return new Promise((resolve, reject) => {
                resolve(null);
            }).then(() => {
                let filter = {companyId: companyId, parentRef : {$in : fields}, status: 1}
                // console.log(filter)
                return StructureModel.find(filter);
            }).then(data => {
                // console.log(data)
                var groupedData = _.groupBy(data, 'parentRef');
                // console.log(groupedData)
                var StandardAndCustom = {
                }
                async.each(fields, (field, callback) => {
                    StandardAndCustom[field] = {
                        "default": StructureFieldsL2[field] ? StructureFieldsL2[field] : [],
                        "custom": groupedData[field] ? groupedData[field] : []
                    }
                    callback()
                }, function(err) {
                    if (!err) {
                        console.log('standard and custom fields object creation done')
                        res.status(200).json({status: true, message: "Successfully fetched standard and custom fields", data: StandardAndCustom});
                    }
                    else {
                        console.log(err);
                        res.status(400).json({status: false, message: err, data: null});
                    }
                })
                
            }).catch((err) => {
                console.log(err);
                res.status(400).json({status: false, message: err, data: null});
            });
        }
        else {
            res.status(400).json({status: false, message: "Query param fields array length 0", data: null});
        }
    }
    else {
        return new Promise((resolve, reject) => {
            resolve(null);
        }).then(() => {
            return StructureModel.find({companyId: companyId});
        }).then(data => {
            var groupedData = _.groupBy(data, 'parentRef');
            var StandardAndCustom = {
            }
            async.each(Object.keys(StructureFieldsL2), (field, callback) => {
                StandardAndCustom[field] = {
                    "default": StructureFieldsL2[field] ? StructureFieldsL2[field] : [],
                    "custom": groupedData[field] ? groupedData[field] : []
                }
                callback()
            }, function(err) {
                if (!err) {
                    console.log('standard and custom fields object creation done')
                    res.status(200).json({status: true, message: "Successfully fetched standard and custom fields", data: StandardAndCustom});
                }
                else {
                    console.log(err);
                    res.status(400).json({status: false, message: err, data: null});
                }
            })
            
        }).catch((err) => {
            console.log(err);
            res.status(400).json({status: false, message: err, data: null});
        });
    }
    
}

let getJobSalaryLinkDetails = ( req, res) => {
    let jobTitleId = req.params.jobTitleId
    let companyId = req.jwt.companyId
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        Promise.all([
            JobSalaryLinkModel.findOne({companyId: companyId, jobTitle: ObjectID(jobTitleId)}).populate('salary_grade').populate('grade_band').lean().exec(),
            StructureModel.find({companyId: req.jwt.companyId, parentRef: 'EEO Job Category'})
        ])
            .then(([job_salary_link, EeoCustom]) => {
                let JobEeoCode = job_salary_link.EeoJobCategoryCode
                let EeoStandard = StructureFieldsL2['EEO Job Category']
                let JobEeoDetails = EeoStandard.filter(x => x.code == 'EJC' + JobEeoCode)
                if (JobEeoDetails[0]) {
                    job_salary_link.EeoJobCategoryDetails = JobEeoDetails[0]
                }
                else {
                    let JobEeoDetails = EeoCustom.filter(x => x.code == JobEeoCode)
                    job_salary_link.EeoJobCategoryDetails = JobEeoDetails[0]
                }
                res.status(200).json({
                    status: true,
                    message: "Fetched job salary details successfully",
                    data: job_salary_link
                })
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while fetching job salary details",
                    error : err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: "Error while fetching job salary details",
            error : err
        })
    })
        
    
}

module.exports = {
    getFields,
    addUpdate,
    update,
    getTotalStructure,
    getSubDocs,
    del,
    uploadxlsx,
    getTrainingDetails,
    getFieldsForAddEmployee,
    getFieldsForRoles,
    getStandardAndCustomFields,
    getJobSalaryLinkDetails,
    multi_del
};
