let express = require('express');

let router = express.Router();

let authMiddleware = require('../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.post(
    '/add',
    authMiddleware.isUserLogin,
    authMiddleware.isCompany,
    validationsMiddleware.reqiuresBody,
    Controller.addSalaryGrade
);

router.post(
    '/list',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.listSalaryGrades
);

router.get(
    '/get/:company_id/:_id',
    authMiddleware.isUserLogin,
    Controller.getSalaryGrade
);

router.post(
    '/list-grade-bands',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.listGradeBands
);

router.put(
    '/edit',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editSalaryGrade
);

router.delete(
    '/delete/:salary_grade_id',
    authMiddleware.isUserLogin,
    Controller.deleteSalaryGrade
);

router.post(
    '/delete',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deleteSalaryGrades
);

router.post(
    '/delete-grade-bands',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deleteGradebands
);

router.put(
    '/change-status',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.changeStatus
);


module.exports = router