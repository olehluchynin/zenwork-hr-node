let mongoose = require('mongoose')
let ObjectID = mongoose.Schema.ObjectId;

// let SalaryGradesSchema1 = new mongoose.Schema({
//     companyId: {type: ObjectID, ref:'companies', required: true},
//     createdBy: {type: ObjectID, ref: 'users'},
//     updatedBy: {type: ObjectID, ref: 'users'},
//     salary_grade_name: {type: String, required: true, unique: true},
//     grade: {type: String, required: true},
//     min: {type: Number, required: true},
//     mid: {type: Number, required: true},
//     max: {type: Number, required: true},
//     average_salary: {type: Number},
//     average_market_value: {type: Number}
// }, {
//     timestamps: true,
//     versionKey: false
// })


// let SalaryGradesSchema = new mongoose.Schema({
//     companyId: {type: ObjectID, ref:'companies', required: true},
//     createdBy: {type: ObjectID, ref: 'users'},
//     updatedBy: {type: ObjectID, ref: 'users'},
//     salary_grade_name: {type: String, required: true, unique: true},
//     grades: [new mongoose.Schema({
//         grade: {type: String, required: true},
//         min: {type: Number, required: true},
//         mid: {type: Number, required: true},
//         max: {type: Number, required: true},
//         average_salary: {type: Number},
//         average_market_value: {type: Number}
//     }, {
//         timestamps: true,
//         versionKey: false
//     })]
// }, {
//     timestamps: true,
//     versionKey: false
// })

let SalaryGradesSchema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    createdBy: {type: ObjectID, ref: 'users'},
    updatedBy: {type: ObjectID, ref: 'users'},
    salary_grade_name: {type: String, required: true},
    pay_frequency: {type: String, required: true},
    archive: {type: Boolean, default: false}
}, {
    timestamps: true,
    versionKey: false
})

let GradeBandsSchema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    createdBy: {type: ObjectID, ref: 'users'},
    updatedBy: {type: ObjectID, ref: 'users'},
    salary_grade: {type: ObjectID, ref: 'salary_grades'},
    grade: {type: String, required: true},
    min: {type: Number, required: true},
    mid: {type: Number, required: true},
    max: {type: Number, required: true},
    average_salary: {type: Number},
    average_market_value: {type: Number}
}, {
    timestamps: true,
    versionKey: false
})

GradeBandsSchema.index({companyId: 1, salary_grade: 1, grade: 1}, {unique: true});

SalaryGradesSchema.index({companyId: 1, salary_grade_name: 1}, {unique: true})

SalaryGradesSchema.index({companyId: 1, salary_grade_name: 1, pay_frequency: 1}, {unique: true})

let SalaryGradesModel = mongoose.model('salary_grades', SalaryGradesSchema, 'salary_grades');

let GradeBandsModel = mongoose.model('grade_bands', GradeBandsSchema, 'grade_bands');


module.exports = {
    SalaryGradesModel, 
    GradeBandsModel
}