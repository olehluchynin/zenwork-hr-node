/**
 * @api {post} /salary-grades/add AddSalaryGrade
 * @apiName AddSalaryGrade
 * @apiGroup SalaryGrades
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "salary_grade_name": "Zenwork1",
    "pay_frequency": "Weekly"
}
 * @apiParamExample {json} Request-Example2:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "salary_grade_name": "Zenwork2",
    "pay_frequency": "Weekly",
    "grades": [
        {
            "grade": "A",
            "min": 20000,
            "mid": 30000,
            "max": 40000,
            "average_salary": 35000,
            "average_market_value": 36000
        },
        {
            "grade": "B",
            "min": 41000,
            "mid": 50000,
            "max": 60000
        }
    ]
}
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully added salary grade",
    "data": {
        "_id": "5d2edadb53cb402280e3483e",
        "companyId": "5c9e11894b1d726375b23058",
        "salary_grade_name": "Zenwork1",
        "pay_frequency": "Weekly",
        "createdBy": "5c9e11894b1d726375b23057",
        "createdAt": "2019-07-17T08:22:51.776Z",
        "updatedAt": "2019-07-17T08:22:51.776Z"
    }
}
 * @apiSuccessExample Success-Response2:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully added salary grade and grade bands",
    "data": [
        {
            "_id": "5d2edb4f53cb402280e34840",
            "grade": "A",
            "min": 20000,
            "mid": 30000,
            "max": 40000,
            "average_salary": 35000,
            "average_market_value": 36000,
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5c9e11894b1d726375b23057",
            "salary_grade": "5d2edb4e53cb402280e3483f",
            "createdAt": "2019-07-17T08:24:47.098Z",
            "updatedAt": "2019-07-17T08:24:47.098Z"
        },
        {
            "_id": "5d2edb4f53cb402280e34841",
            "grade": "B",
            "min": 41000,
            "mid": 50000,
            "max": 60000,
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5c9e11894b1d726375b23057",
            "salary_grade": "5d2edb4e53cb402280e3483f",
            "createdAt": "2019-07-17T08:24:47.098Z",
            "updatedAt": "2019-07-17T08:24:47.098Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /salary-grades/list ListSalaryGrades
 * @apiName ListSalaryGrades
 * @apiGroup SalaryGrades
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"per_page": 5,
    "page_no": 1,
    "archive": false
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched list of salary grades",
    "total_count": 3,
    "data": [
        {
            "_id": "5d2ee25af182ad379c4cf563",
            "archive": false,
            "companyId": "5c9e11894b1d726375b23058",
            "salary_grade_name": "Zenwork3",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-07-17T08:54:50.583Z",
            "updatedAt": "2019-07-17T08:54:50.583Z"
        },
        {
            "_id": "5d2edb4e53cb402280e3483f",
            "companyId": "5c9e11894b1d726375b23058",
            "salary_grade_name": "Zenwork2",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-07-17T08:24:46.873Z",
            "updatedAt": "2019-07-17T08:24:46.873Z",
            "archive": false
        },
        {
            "_id": "5d2edadb53cb402280e3483e",
            "companyId": "5c9e11894b1d726375b23058",
            "salary_grade_name": "Zenwork1",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-07-17T08:22:51.776Z",
            "updatedAt": "2019-07-17T08:22:51.776Z",
            "archive": false
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /salary-grades/get/:company_id/:_id GetSalaryGrade
 * @apiName GetSalaryGrade
 * @apiGroup SalaryGrades
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} _id SalaryGrades unique _id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Fetched salary grade successfully",
    "grades_total_count": 2,
    "data": {
        "_id": "5d2ee25af182ad379c4cf563",
        "archive": false,
        "companyId": "5c9e11894b1d726375b23058",
        "salary_grade_name": "Zenwork3",
        "pay_frequency": "Weekly",
        "createdBy": "5c9e11894b1d726375b23057",
        "createdAt": "2019-07-17T08:54:50.583Z",
        "updatedAt": "2019-07-17T08:54:50.583Z",
        "grades": [
            {
                "_id": "5d2ee25af182ad379c4cf564",
                "grade": "A",
                "min": 20000,
                "mid": 30000,
                "max": 40000,
                "average_salary": 35000,
                "average_market_value": 36000,
                "companyId": "5c9e11894b1d726375b23058",
                "createdBy": "5c9e11894b1d726375b23057",
                "salary_grade": "5d2ee25af182ad379c4cf563",
                "createdAt": "2019-07-17T08:54:50.816Z",
                "updatedAt": "2019-07-17T08:54:50.816Z"
            },
            {
                "_id": "5d2ee25af182ad379c4cf565",
                "grade": "B",
                "min": 41000,
                "mid": 50000,
                "max": 60000,
                "companyId": "5c9e11894b1d726375b23058",
                "createdBy": "5c9e11894b1d726375b23057",
                "salary_grade": "5d2ee25af182ad379c4cf563",
                "createdAt": "2019-07-17T08:54:50.817Z",
                "updatedAt": "2019-07-17T08:54:50.817Z"
            }
        ]
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /salary-grades/list-grade-bands ListGradeBands
 * @apiName ListGradeBands
 * @apiGroup SalaryGrades
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "companyId": "5c9e11894b1d726375b23058",
    "salary_grade_id": "5d2ee25af182ad379c4cf563",
	"per_page": 5,
    "page_no": 1
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched list of grade bands",
    "total_count": 2,
    "data": [
        {
            "_id": "5d2ee25af182ad379c4cf565",
            "grade": "B",
            "min": 41000,
            "mid": 50000,
            "max": 60000,
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5c9e11894b1d726375b23057",
            "salary_grade": "5d2ee25af182ad379c4cf563",
            "createdAt": "2019-07-17T08:54:50.817Z",
            "updatedAt": "2019-07-17T08:54:50.817Z"
        },
        {
            "_id": "5d2ee25af182ad379c4cf564",
            "grade": "A",
            "min": 20000,
            "mid": 30000,
            "max": 40000,
            "average_salary": 35000,
            "average_market_value": 36000,
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5c9e11894b1d726375b23057",
            "salary_grade": "5d2ee25af182ad379c4cf563",
            "createdAt": "2019-07-17T08:54:50.816Z",
            "updatedAt": "2019-07-17T08:54:50.816Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /salary-grades/edit EditSalaryGrade
 * @apiName EditSalaryGrade
 * @apiGroup SalaryGrades
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "_id": "5d2ee25af182ad379c4cf563",
    "archive": false,
    "companyId": "5c9e11894b1d726375b23058",
    "salary_grade_name": "Zenwork_3",
    "pay_frequency": "Weekly",
    "createdBy": "5c9e11894b1d726375b23057",
    "createdAt": "2019-07-17T08:54:50.583Z",
    "updatedAt": "2019-07-17T08:54:50.583Z",
    "grades": [
        {
            "_id": "5d2ee25af182ad379c4cf564",
            "grade": "A_1",
            "min": 20000,
            "mid": 30000,
            "max": 40000,
            "average_salary": 35000,
            "average_market_value": 36000,
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5c9e11894b1d726375b23057",
            "salary_grade": "5d2ee25af182ad379c4cf563",
            "createdAt": "2019-07-17T08:54:50.816Z",
            "updatedAt": "2019-07-17T08:54:50.816Z"
        },
        {
            "_id": "5d2ee25af182ad379c4cf565",
            "grade": "B",
            "min": 41000,
            "mid": 50000,
            "max": 60000,
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5c9e11894b1d726375b23057",
            "salary_grade": "5d2ee25af182ad379c4cf563",
            "createdAt": "2019-07-17T08:54:50.817Z",
            "updatedAt": "2019-07-17T08:54:50.817Z"
        },
        {
            "grade": "C",
            "min": 41000,
            "mid": 50000,
            "max": 60000
        }
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Salary grade and grade bands updated successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /salary-grades/delete DeleteSalaryGrades
 * @apiName DeleteSalaryGrades
 * @apiGroup SalaryGrades
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "companyId": "5c9e11894b1d726375b23058",
    "_ids": [
        "5d2ee25af182ad379c4cf563"
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {post} /salary-grades/delete-grade-bands DeleteGradeBands
 * @apiName DeleteGradeBands
 * @apiGroup SalaryGrades
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "companyId": "5c9e11894b1d726375b23058",
    "salary_grade_id": "5d2ee25af182ad379c4cf563",
    "_ids": [
        "5d2efdf6dc888c3570f59484"
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /salary-grades/change-status changeStatus
 * @apiName changeStatus
 * @apiGroup SalaryGrades
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "companyId": "5c9e11894b1d726375b23058",
    "_id": "5d2ee25af182ad379c4cf563",
    "archive": true
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Changed status of salary grade successfully",
    "data": {
        "archive": true,
        "_id": "5d2f0875821f2617528c6ace",
        "companyId": "5c9e11894b1d726375b23058",
        "salary_grade_name": "hksajdaks",
        "createdBy": "5c9e11894b1d726375b23057",
        "createdAt": "2019-07-17T11:37:25.494Z",
        "updatedAt": "2019-07-17T12:37:25.749Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {delete} /salary-grades/delete/:salary_grade_id DeleteSalaryGrade
 * @apiName DeleteSalaryGrade
 * @apiGroup SalaryGrades
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} salary_grade_id SalaryGrades unique _id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 * @apiErrorExample CannotBeDeleted:
{
    "status": false,
    "message": "This salary grade cannot be deleted as it is attached to a job title."
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */