let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../config/config');
let _ = require('underscore');
let SalaryGradesModel = require('./model').SalaryGradesModel;
let GradeBandsModel = require('./model').GradeBandsModel;
let JobSalaryLinkModel = require('./../settings-structure/job-salary-model');

/**
 * Method to add salary grade
 * Can add salary grade with just the name or with grade bands
 * @param {*} req 
 * @param {*} res 
 * @author Asma Mubeen (17-07-2019)
 */
let addSalaryGrade = (req,res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve,reject) => {
        if (!companyId) reject('companyId is required');
        else if(!body.salary_grade_name) reject('salary grade name is required');
        else if (!body.pay_frequency) reject('pay_frequency is required');
        else if (!body.grades) reject('Add at least one grade band');
        else if (!body.grades.length) reject('Add at least one grade band');
        else resolve(null);
    })
    .then(() => {
        body.createdBy =  ObjectID(req.jwt.userId)
        let grades = body.grades
        delete body.grades
        var grade_names = grades.map(x => x.grade)
        SalaryGradesModel.findOne({companyId: companyId, salary_grade_name: body.salary_grade_name})
            .then(async salary_grade => {
                // let salary_grade_with_frequency = await SalaryGradesModel.findOne({companyId: companyId, pay_frequency: body.pay_frequency}).lean().exec()
                if(salary_grade) {
                    res.status(400).json({
                        status: false,
                        message: "Salary grade already exists with this name"
                    })
                }
                // else if (salary_grade_with_frequency) {
                //     res.status(400).json({
                //         status: false,
                //         message: "Salary grade already exists with this pay frequency"
                //     })
                // }
                else if (grades.length != new Set(grade_names).size){
                    res.status(400).json({
                        status: false,
                        message: "Grades must be unique. Please enter different grade names"
                    })
                }
                else {
                    SalaryGradesModel.create(body)
                        .then(new_salary_grade => {
                            if (grades && grades.length > 0) {
                                for (let grade of grades) {
                                    grade.companyId = companyId
                                    grade.createdBy = ObjectID(req.jwt.userId)
                                    grade.salary_grade = new_salary_grade._id
                                }
                                console.log('grades - ', grades)
                                GradeBandsModel.insertMany(grades)
                                    .then(response => {
                                        console.log('grades insert many response - ', response)
                                        res.status(200).json({
                                            status: true,
                                            message: "Successfully added salary grade and grade bands",
                                            data: response
                                        })
                                    })
                                    .catch(err => {
                                        console.log(err)
                                        if(err.code == 11000) {
                                            res.status(400).json({
                                                status: false,
                                                message: "Grades must be unique. Please enter different grade names"
                                            })
                                        }
                                        else {
                                            res.status(500).json({
                                                status: false,
                                                message: "Error while adding grades",
                                                error : err
                                            })
                                        }
                                    })
                            }
                            else {
                                res.status(200).json({
                                    status: true,
                                    message: "Successfully added salary grade",
                                    data: new_salary_grade
                                })
                            }
                        })
                        .catch(err => {
                            console.log(err)
                            res.status(500).json({
                                status: false,
                                message: "Error while adding salary grade",
                                error : err
                            })
                        })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({
                    status: false,
                    message: "Error while adding salary grade",
                    error : err
                })
            })
        
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    });
}

/**
 * Method to fetch list of salary grades in a company
 * @param {*} req 
 * @param {*} res 
 * @author Asma Mubeen (17-07-2019)
 */
let listSalaryGrades = (req, res ) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if(!companyId) reject('companyId is required')
        else resolve(null);
    })
    .then(() => {
        let sort = {
            updatedAt : -1
        }
        let filter = {
            companyId: ObjectID(companyId),
        }
        let per_page = body.per_page ? body.per_page : null
        let page_no = body.page_no ? body.page_no : 1
        let skip = per_page ? (page_no - 1) * per_page : 0
        let limit = per_page
        if (body.hasOwnProperty('archive')) {
            filter['archive'] = body.archive
        }
        Promise.all([
            SalaryGradesModel.find(filter).sort(sort).skip(skip).limit(limit).lean().exec(),
            SalaryGradesModel.countDocuments(filter)
        ])
        .then(([salary_grades, total_count]) => {
            res.status(200).json({
                status: true, message: "Successfully fetched list of salary grades", total_count: total_count, data: salary_grades
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while fetching salary grades",
                error: err
            })
        });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

/**
*@author Asma
*@date 17/07/2019 14:39
Method to get one salary grade details along with grade bands
*/
let getSalaryGrade = (req, res) => {
    let companyId = req.params.company_id
    let salary_grade_id = req.params._id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        let find_query = {
            _id: ObjectID(salary_grade_id),
            companyId: ObjectID(companyId)
        }
        SalaryGradesModel.findOne(find_query).lean().exec()
            .then(salary_grade => {
                if(salary_grade) {
                    let grades_find_filter = {
                        companyId: companyId, 
                        salary_grade: ObjectID(salary_grade_id)
                    }
                    Promise.all([
                        // GradeBandsModel.find(grades_find_filter).sort({updatedAt: -1}).skip(0).limit(10).lean().exec(),
                        GradeBandsModel.find(grades_find_filter).sort({grade: 1}).skip(0).limit(10).lean().exec(),
                        GradeBandsModel.countDocuments(grades_find_filter)
                    ])
                    .then(([grade_bands, total_count]) => {
                        salary_grade.grades = grade_bands
                        res.status(200).json({
                            status: true,
                            message: "Fetched salary grade successfully",
                            grades_total_count: total_count,
                            data: salary_grade
                        })
                    })
                    .catch(err => {
                        console.log(err)
                        res.status(500).json({
                            status: false,
                            message: "Error while fecthing salary grade",
                            error: err
                        })
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "Salary grade not found",
                        data: null
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while fecthing salary grade",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err);
        res.status(400).json({
            status: false,
            message: "Error while fecthing salary grade",
            error: err
        })
    })
}

let listGradeBands = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let salary_grade_id = body.salary_grade_id
    return new Promise((resolve, reject) => {
        if(!companyId) reject('companyId is required');
        else if(!salary_grade_id) reject('salary_grade_id is required');
        else resolve(null);
    })
    .then(() => {
        let sort = {
            // updatedAt : -1
            grade: 1
        }
        let filter = {
            companyId: ObjectID(companyId),
            salary_grade: ObjectID(salary_grade_id)
        }
        let per_page = body.per_page ? body.per_page : 10
        let page_no = body.page_no ? body.page_no : 1
        Promise.all([
            GradeBandsModel.find(filter).sort(sort).skip((page_no - 1) * per_page).limit(per_page).lean().exec(),
            GradeBandsModel.countDocuments(filter)
        ])
        .then(([grade_bands, total_count]) => {
            res.status(200).json({
                status: true, 
                message: "Successfully fetched list of grade bands", 
                total_count: total_count, 
                data: grade_bands
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while fetching grade bands",
                error: err
            })
        });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

/**
*@author Asma
*@date 17/07/2019 16:14
Method to edit salary grade and grade bands
*/
let editSalaryGrade = (req,res) => {
    let body = req.body
    let salary_grade_id = body._id
    let companyId = body.companyId
    let grades = body.grades
    delete body.grades
    return new Promise((resolve, reject) => {
        var grade_names = grades.map(x => x.grade)
        if (!salary_grade_id) {
            reject('_id is required')
        }
        else if (!companyId) {
            reject('companyId is required')
        }
        else if (grades.length != new Set(grade_names).size) {
            reject('Grades must be unique. Please enter different grade names')
        }
        else {
            resolve(null);
        }
    })
    .then(() => {
        delete body.companyId
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        console.log(body)
        let filter = {
            _id: ObjectID(salary_grade_id), 
            companyId : ObjectID(companyId)
        }
        console.log(filter)
        body.updatedBy = ObjectID(req.jwt.userId)
        SalaryGradesModel.findOneAndUpdate(filter, {$set: body}, {new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                if (grades && grades.length > 0) {
                    let promises = []
                    for (let grade of grades) {
                        if (grade._id) {
                            let grade_update_filter = {
                                _id: grade._id,
                                companyId: companyId
                            }
                            delete grade.companyId
                            delete grade.createdAt
                            delete grade.updatedAt
                            delete grade.salary_grade
                            delete grade._id
                            grade.updatedBy = ObjectID(req.jwt.userId)
                            promises.push(GradeBandsModel.findOneAndUpdate(grade_update_filter, { $set: grade }))
                        }
                        else {
                            grade.companyId = companyId
                            grade.salary_grade = salary_grade_id
                            grade.createdBy = ObjectID(req.jwt.userId)
                            promises.push(GradeBandsModel.create(grade))
                        }
                    }
                    Promise.all(promises)
                        .then(results => {
                            console.log(results)
                            res.status(200).json({
                                status: true,
                                message: "Salary grade and grade bands updated successfully",
                            })
                        })
                        .catch(err => {
                            console.log(err)
                            res.status(500).json({
                                status: false,
                                message: "Error while updating grade bands",
                                error : err
                            })
                        })
                }
                else {
                    res.status(200).json({
                        status: true,
                        message: "Salary grade updated successfully",
                        data: response
                    })
                }
                
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while updating salary grade",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    });
}

let deleteSalaryGrades = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else resolve(null);
    })
    .then(() => {
        SalaryGradesModel.deleteMany({
            companyId: companyId,
            _id: {$in : body._ids}
        })
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting salary grades",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let deleteSalaryGrade = (req, res) => {
    let companyId = req.jwt.companyId
    let salary_grade_id = req.params.salary_grade_id
    return new Promise((resolve, reject) => {
        resolve(JobSalaryLinkModel.findOne({companyId: companyId, salary_grade: ObjectID(salary_grade_id)}).lean().exec());
    })
    .then(jobSalaryLink => {
        if(jobSalaryLink) {
            res.status(400).json({
                status: false,
                message: 'This salary grade cannot be deleted as it is attached to a job title.'
            })
        }
        else {
            SalaryGradesModel.deleteOne({
                companyId: companyId,
                _id: ObjectID(salary_grade_id)
            })
            .then(response => {
                res.status(200).json({
                    status: true,
                    message: "Deleted successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while deleting salary grade",
                    error: err
                })
            })
        }

        
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let deleteGradebands = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let salary_grade_id = body.salary_grade_id
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else if (!salary_grade_id) reject('salary_grade_id is required');
        else resolve(GradeBandsModel.find({companyId: companyId, salary_grade: salary_grade_id}).lean().exec());
    })
    .then(async grade_bands => {
        body._ids = Array.from(new Set(body._ids))
        console.log('body._ids.length - ', body._ids.length)
        console.log('grade_bands.length - ', grade_bands.length)
        if (body._ids.length == grade_bands.length) {
            res.status(400).json({
                status: false,
                message: "Cannot delete all the records as there should be at least one grade band present for a salary grade. Deselect one and try again or delete the whole salary grade"
            })
        }
        else {
            let cannot_delete_grade_bands = []
            let cannot_delete_grade_bands_ids = []
            console.log('body._ids - ', body._ids)
            body._ids = Array.from(new Set(body._ids))
            console.log('body._ids - ', body._ids)
            for (let _id of body._ids) {
                let jobSalaryLink = await JobSalaryLinkModel.findOne({companyId: companyId, grade_band: _id}).lean().exec()
                if (jobSalaryLink) {
                    let grade_band = await GradeBandsModel.findOne({companyId: companyId, _id: _id}).lean().exec()
                    cannot_delete_grade_bands.push(grade_band.grade)
                    cannot_delete_grade_bands_ids.push(_id)
                    // let delete_index = body._ids.indexOf(_id);
                    // if (delete_index > -1) {
                    //     body._ids.splice(delete_index, 1);
                    // }
                    body._ids = body._ids.filter(x => x!=_id)
                }
            }
            console.log('body._ids - ', body._ids)
            console.log('cannot_delete_grade_bands - ', cannot_delete_grade_bands)
            console.log('cannot_delete_grade_bands_ids - ', cannot_delete_grade_bands_ids)
            GradeBandsModel.deleteMany({
                companyId: companyId,
                salary_grade: salary_grade_id,
                _id: {$in : body._ids}
            })
            .then(response => {
                if (!cannot_delete_grade_bands.length) {
                    res.status(200).json({
                        status: true,
                        message: "Deleted successfully",
                        data: response
                    })
                }
                else {
                    res.status(200).json({
                        status: true,
                        message: "Cannot delete " + cannot_delete_grade_bands.join(', ') + 'grade band(s) is/are assigned to job title(s). Deleted other grade bands successfully',
                        data: response
                    })
                }
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while deleting grade bands",
                    error: err
                })
            })
        }
        
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let changeStatus = (req, res) => {
    let body = req.body
    let salary_grade_id = body._id
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!salary_grade_id) {
            reject('_id is required')
        }
        else if (!body.hasOwnProperty("archive")) {
            reject('archive field is required')
        }
        else if (!companyId) {
            reject('companyId is required')
        }
        else {
            resolve(null);
        }
    })
    .then(()=> {
        delete body.companyId
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        let filter = {
            _id: ObjectID(salary_grade_id),
            companyId : ObjectID(companyId), 
        }
        console.log(filter)
        SalaryGradesModel.findOneAndUpdate(filter, {$set: body}, {new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                if (response) {
                    res.status(200).json({
                        status: true,
                        message: "Changed status of salary grade successfully",
                        data: response
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "Salary grade not found"
                    })
                }
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while changing status",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while changing status",
            error: err
        })
    })
}

module.exports = {
    addSalaryGrade,
    listSalaryGrades,
    getSalaryGrade,
    listGradeBands,
    editSalaryGrade,
    deleteSalaryGrades,
    deleteGradebands,
    changeStatus,
    deleteSalaryGrade
}