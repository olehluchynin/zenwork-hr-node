let mongoose = require('mongoose')
let ObjectID = mongoose.Schema.ObjectId;

let PayrollCalendarSchema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    createdBy: {type: ObjectID, ref: 'users'},
    updatedBy: {type: ObjectID, ref: 'users'},
    calendar_name: {type: String, required: true, unique: true},
    calendar_year: {type: String, required: true},
    number_of_cycles: {type: Number, required: true},
    number_of_days_in_a_cycle: {type: Number, required: true},
    first_cycle_start_date: {type: Date, required: true},
    first_cycle_end_date: {type: Date, required: true},
    assigned_payroll_group: {type: ObjectID, ref: 'payroll_groups'},
    archive: {type: Boolean, default: false}
}, {
    timestamps: true,
    versionKey: false
})

let PayCycleDates = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    createdBy: {type: ObjectID, ref: 'users'},
    updatedBy: {type: ObjectID, ref: 'users'},
    calendar: {type: ObjectID, ref: 'payroll_calendars', required: true},
    pay_cycle_number: {type: Number, required: true},
    pay_cycle_name: {type: String},
    pay_cycle_start_date: {type: Date, required: true},
    pay_cycle_end_date: {type: Date, required: true},
    status: {
        type: String, enum: ['Open', 'Closed'], default: 'Closed'
    }
}, {
    timestamps: true,
    versionKey: false
})

PayrollCalendarSchema.index({companyId: 1, calendar_name: 1}, {unique: true})

let PayrollCalendarsModel = mongoose.model('payroll_calendars', PayrollCalendarSchema, 'payroll_calendars');
let PayCyclesModel = mongoose.model('pay_cycles', PayCycleDates, 'pay_cycles');

module.exports = {
    PayrollCalendarsModel,
    PayCyclesModel
}