let express = require('express');

let router = express.Router();

let authMiddleware = require('../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.post(
    '/get-pay-cycle-dates',
    authMiddleware.isUserLogin,
    authMiddleware.isCompany,
    validationsMiddleware.reqiuresBody,
    Controller.getPayCycleDates
);

router.post(
    '/add',
    authMiddleware.isUserLogin,
    authMiddleware.isCompany,
    validationsMiddleware.reqiuresBody,
    Controller.addPayrollCalendar
);


router.get(
    '/get/:company_id/:_id',
    authMiddleware.isUserLogin,
    Controller.getPayrollCalendar
);

router.post(
    '/list',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.list
);

// router.post(
//     '/delete',
//     authMiddleware.isUserLogin,
//     validationsMiddleware.reqiuresBody,
//     Controller.deletePayrollCalendars
// );

router.delete(
    '/delete/:company_id/:calendar_id',
    authMiddleware.isUserLogin,
    Controller.deletePayrollCalendar
)

router.put(
    '/edit',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editPayrollCalendar
);


router.get(
    '/current-and-next-cycle-details/:company_id/:calendar_id',
    authMiddleware.isUserLogin,
    Controller.getCurrentAndNextCycle
);


router.put(
    '/start-next-cycle',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.startNextCycle
);

router.post(
    '/create-copy',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.copy_calendar
);

router.put(
    '/reopen-cycle',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.reopenPreviousCycle
);

router.get(
    '/previous-and-current-cycle-details/:company_id/:calendar_id',
    authMiddleware.isUserLogin,
    Controller.GetPreviousAndCurrentCycle
);


module.exports = router