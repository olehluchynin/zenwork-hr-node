/**
 * @api {post} /payroll-calendars/get-pay-cycle-dates GetPayCycleDates
 * @apiName GetPayCycleDates
 * @apiGroup PayrollCalendars
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"number_of_cycles": "26",
	"calendar_name": "Calendar1_Semi_Monthly",
	"number_of_days_in_a_cycle": "15",
	"first_cycle_start_date": "2019-01-01T00:00:00.000Z",
	"first_cycle_end_date": "2019-01-15T23:59:59.000Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Succesfully calculated pay cycle dates",
    "data": [
        {
            "pay_cycle_number": 1,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 1-26",
            "pay_cycle_start_date": "2019-01-01T00:00:00.000Z",
            "pay_cycle_end_date": "2019-01-16T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 2,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 2-26",
            "pay_cycle_start_date": "2019-01-16T18:30:00.000Z",
            "pay_cycle_end_date": "2019-02-01T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 3,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 3-26",
            "pay_cycle_start_date": "2019-02-01T18:30:00.000Z",
            "pay_cycle_end_date": "2019-02-17T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 4,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 4-26",
            "pay_cycle_start_date": "2019-02-17T18:30:00.000Z",
            "pay_cycle_end_date": "2019-03-05T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 5,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 5-26",
            "pay_cycle_start_date": "2019-03-05T18:30:00.000Z",
            "pay_cycle_end_date": "2019-03-21T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 6,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 6-26",
            "pay_cycle_start_date": "2019-03-21T18:30:00.000Z",
            "pay_cycle_end_date": "2019-04-06T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 7,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 7-26",
            "pay_cycle_start_date": "2019-04-06T18:30:00.000Z",
            "pay_cycle_end_date": "2019-04-22T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 8,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 8-26",
            "pay_cycle_start_date": "2019-04-22T18:30:00.000Z",
            "pay_cycle_end_date": "2019-05-08T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 9,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 9-26",
            "pay_cycle_start_date": "2019-05-08T18:30:00.000Z",
            "pay_cycle_end_date": "2019-05-24T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 10,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 10-26",
            "pay_cycle_start_date": "2019-05-24T18:30:00.000Z",
            "pay_cycle_end_date": "2019-06-09T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 11,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 11-26",
            "pay_cycle_start_date": "2019-06-09T18:30:00.000Z",
            "pay_cycle_end_date": "2019-06-25T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 12,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 12-26",
            "pay_cycle_start_date": "2019-06-25T18:30:00.000Z",
            "pay_cycle_end_date": "2019-07-11T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 13,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 13-26",
            "pay_cycle_start_date": "2019-07-11T18:30:00.000Z",
            "pay_cycle_end_date": "2019-07-27T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 14,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 14-26",
            "pay_cycle_start_date": "2019-07-27T18:30:00.000Z",
            "pay_cycle_end_date": "2019-08-12T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 15,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 15-26",
            "pay_cycle_start_date": "2019-08-12T18:30:00.000Z",
            "pay_cycle_end_date": "2019-08-28T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 16,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 16-26",
            "pay_cycle_start_date": "2019-08-28T18:30:00.000Z",
            "pay_cycle_end_date": "2019-09-13T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 17,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 17-26",
            "pay_cycle_start_date": "2019-09-13T18:30:00.000Z",
            "pay_cycle_end_date": "2019-09-29T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 18,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 18-26",
            "pay_cycle_start_date": "2019-09-29T18:30:00.000Z",
            "pay_cycle_end_date": "2019-10-15T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 19,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 19-26",
            "pay_cycle_start_date": "2019-10-15T18:30:00.000Z",
            "pay_cycle_end_date": "2019-10-31T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 20,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 20-26",
            "pay_cycle_start_date": "2019-10-31T18:30:00.000Z",
            "pay_cycle_end_date": "2019-11-16T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 21,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 21-26",
            "pay_cycle_start_date": "2019-11-16T18:30:00.000Z",
            "pay_cycle_end_date": "2019-12-02T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 22,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 22-26",
            "pay_cycle_start_date": "2019-12-02T18:30:00.000Z",
            "pay_cycle_end_date": "2019-12-18T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 23,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 23-26",
            "pay_cycle_start_date": "2019-12-18T18:30:00.000Z",
            "pay_cycle_end_date": "2020-01-03T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 24,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 24-26",
            "pay_cycle_start_date": "2020-01-03T18:30:00.000Z",
            "pay_cycle_end_date": "2020-01-19T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 25,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 25-26",
            "pay_cycle_start_date": "2020-01-19T18:30:00.000Z",
            "pay_cycle_end_date": "2020-02-04T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 26,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 26-26",
            "pay_cycle_start_date": "2020-02-04T18:30:00.000Z",
            "pay_cycle_end_date": "2020-02-20T18:29:59.000Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /payroll-calendars/add AddPayrollCalendar
 * @apiName AddPayrollCalendar
 * @apiGroup PayrollCalendars
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"calendar_name": "Calendar1_Semi_Monthly",
	"calendar_year": "2019",
	"number_of_cycles": "26",
	"number_of_days_in_a_cycle": "15",
	"first_cycle_start_date": "2019-01-01T00:00:00.000Z",
	"first_cycle_end_date": "2019-01-15T23:59:59.000Z",
	"assigned_payroll_group": "5d2d89f7c9dcf43300723e18",
	"paycycle_dates": [
        {
            "pay_cycle_number": 1,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 1-26",
            "pay_cycle_start_date": "2019-01-01T00:00:00.000Z",
            "pay_cycle_end_date": "2019-01-16T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 2,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 2-26",
            "pay_cycle_start_date": "2019-01-16T18:30:00.000Z",
            "pay_cycle_end_date": "2019-02-01T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 3,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 3-26",
            "pay_cycle_start_date": "2019-02-01T18:30:00.000Z",
            "pay_cycle_end_date": "2019-02-17T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 4,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 4-26",
            "pay_cycle_start_date": "2019-02-17T18:30:00.000Z",
            "pay_cycle_end_date": "2019-03-05T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 5,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 5-26",
            "pay_cycle_start_date": "2019-03-05T18:30:00.000Z",
            "pay_cycle_end_date": "2019-03-21T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 6,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 6-26",
            "pay_cycle_start_date": "2019-03-21T18:30:00.000Z",
            "pay_cycle_end_date": "2019-04-06T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 7,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 7-26",
            "pay_cycle_start_date": "2019-04-06T18:30:00.000Z",
            "pay_cycle_end_date": "2019-04-22T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 8,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 8-26",
            "pay_cycle_start_date": "2019-04-22T18:30:00.000Z",
            "pay_cycle_end_date": "2019-05-08T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 9,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 9-26",
            "pay_cycle_start_date": "2019-05-08T18:30:00.000Z",
            "pay_cycle_end_date": "2019-05-24T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 10,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 10-26",
            "pay_cycle_start_date": "2019-05-24T18:30:00.000Z",
            "pay_cycle_end_date": "2019-06-09T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 11,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 11-26",
            "pay_cycle_start_date": "2019-06-09T18:30:00.000Z",
            "pay_cycle_end_date": "2019-06-25T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 12,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 12-26",
            "pay_cycle_start_date": "2019-06-25T18:30:00.000Z",
            "pay_cycle_end_date": "2019-07-11T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 13,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 13-26",
            "pay_cycle_start_date": "2019-07-11T18:30:00.000Z",
            "pay_cycle_end_date": "2019-07-27T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 14,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 14-26",
            "pay_cycle_start_date": "2019-07-27T18:30:00.000Z",
            "pay_cycle_end_date": "2019-08-12T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 15,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 15-26",
            "pay_cycle_start_date": "2019-08-12T18:30:00.000Z",
            "pay_cycle_end_date": "2019-08-28T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 16,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 16-26",
            "pay_cycle_start_date": "2019-08-28T18:30:00.000Z",
            "pay_cycle_end_date": "2019-09-13T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 17,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 17-26",
            "pay_cycle_start_date": "2019-09-13T18:30:00.000Z",
            "pay_cycle_end_date": "2019-09-29T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 18,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 18-26",
            "pay_cycle_start_date": "2019-09-29T18:30:00.000Z",
            "pay_cycle_end_date": "2019-10-15T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 19,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 19-26",
            "pay_cycle_start_date": "2019-10-15T18:30:00.000Z",
            "pay_cycle_end_date": "2019-10-31T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 20,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 20-26",
            "pay_cycle_start_date": "2019-10-31T18:30:00.000Z",
            "pay_cycle_end_date": "2019-11-16T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 21,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 21-26",
            "pay_cycle_start_date": "2019-11-16T18:30:00.000Z",
            "pay_cycle_end_date": "2019-12-02T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 22,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 22-26",
            "pay_cycle_start_date": "2019-12-02T18:30:00.000Z",
            "pay_cycle_end_date": "2019-12-18T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 23,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 23-26",
            "pay_cycle_start_date": "2019-12-18T18:30:00.000Z",
            "pay_cycle_end_date": "2020-01-03T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 24,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 24-26",
            "pay_cycle_start_date": "2020-01-03T18:30:00.000Z",
            "pay_cycle_end_date": "2020-01-19T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 25,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 25-26",
            "pay_cycle_start_date": "2020-01-19T18:30:00.000Z",
            "pay_cycle_end_date": "2020-02-04T18:29:59.000Z"
        },
        {
            "pay_cycle_number": 26,
            "pay_cycle_name": "Calendar1_Semi_Monthly: 26-26",
            "pay_cycle_start_date": "2020-02-04T18:30:00.000Z",
            "pay_cycle_end_date": "2020-02-20T18:29:59.000Z"
        }
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully added payroll calendar",
    "data": {
        "archive": false,
        "_id": "5d30544bbe55f2083c48ef62",
        "companyId": "5c9e11894b1d726375b23058",
        "calendar_name": "Calendar1_Semi_Monthly",
        "calendar_year": "2019",
        "number_of_cycles": 26,
        "number_of_days_in_a_cycle": 15,
        "first_cycle_start_date": "2019-01-01T00:00:00.000Z",
        "first_cycle_end_date": "2019-01-15T23:59:59.000Z",
        "assigned_payroll_group": "5d2d89f7c9dcf43300723e18",
        "createdBy": "5c9e11894b1d726375b23057",
        "createdAt": "2019-07-18T11:13:15.800Z",
        "updatedAt": "2019-07-18T11:13:15.800Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /payroll-calendars/get/:company_id/:_id GetPayrollCalendar
 * @apiName GetPayrollCalendar
 * @apiGroup PayrollCalendars
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} _id PayrollCalendars unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched payroll calendar",
    "data": {
        "_id": "5d30544bbe55f2083c48ef62",
        "archive": false,
        "companyId": "5c9e11894b1d726375b23058",
        "calendar_name": "Calendar1_Semi_Monthly",
        "calendar_year": "2019",
        "number_of_cycles": 26,
        "number_of_days_in_a_cycle": 15,
        "first_cycle_start_date": "2019-01-01T00:00:00.000Z",
        "first_cycle_end_date": "2019-01-15T23:59:59.000Z",
        "assigned_payroll_group": "5d2d89f7c9dcf43300723e18",
        "createdBy": "5c9e11894b1d726375b23057",
        "createdAt": "2019-07-18T11:13:15.800Z",
        "updatedAt": "2019-07-18T11:13:15.800Z",
        "paycycle_dates": [
            {
                "_id": "5d30544cbe55f2083c48ef7c",
                "pay_cycle_number": 26,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 26-26",
                "pay_cycle_start_date": "2020-02-04T18:30:00.000Z",
                "pay_cycle_end_date": "2020-02-20T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.038Z",
                "updatedAt": "2019-07-18T11:13:16.038Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef74",
                "pay_cycle_number": 18,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 18-26",
                "pay_cycle_start_date": "2019-09-29T18:30:00.000Z",
                "pay_cycle_end_date": "2019-10-15T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.037Z",
                "updatedAt": "2019-07-18T11:13:16.037Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef75",
                "pay_cycle_number": 19,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 19-26",
                "pay_cycle_start_date": "2019-10-15T18:30:00.000Z",
                "pay_cycle_end_date": "2019-10-31T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.037Z",
                "updatedAt": "2019-07-18T11:13:16.037Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef76",
                "pay_cycle_number": 20,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 20-26",
                "pay_cycle_start_date": "2019-10-31T18:30:00.000Z",
                "pay_cycle_end_date": "2019-11-16T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.037Z",
                "updatedAt": "2019-07-18T11:13:16.037Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef77",
                "pay_cycle_number": 21,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 21-26",
                "pay_cycle_start_date": "2019-11-16T18:30:00.000Z",
                "pay_cycle_end_date": "2019-12-02T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.037Z",
                "updatedAt": "2019-07-18T11:13:16.037Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef78",
                "pay_cycle_number": 22,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 22-26",
                "pay_cycle_start_date": "2019-12-02T18:30:00.000Z",
                "pay_cycle_end_date": "2019-12-18T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.037Z",
                "updatedAt": "2019-07-18T11:13:16.037Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef79",
                "pay_cycle_number": 23,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 23-26",
                "pay_cycle_start_date": "2019-12-18T18:30:00.000Z",
                "pay_cycle_end_date": "2020-01-03T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.037Z",
                "updatedAt": "2019-07-18T11:13:16.037Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef7a",
                "pay_cycle_number": 24,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 24-26",
                "pay_cycle_start_date": "2020-01-03T18:30:00.000Z",
                "pay_cycle_end_date": "2020-01-19T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.037Z",
                "updatedAt": "2019-07-18T11:13:16.037Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef7b",
                "pay_cycle_number": 25,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 25-26",
                "pay_cycle_start_date": "2020-01-19T18:30:00.000Z",
                "pay_cycle_end_date": "2020-02-04T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.037Z",
                "updatedAt": "2019-07-18T11:13:16.037Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef66",
                "pay_cycle_number": 4,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 4-26",
                "pay_cycle_start_date": "2019-02-17T18:30:00.000Z",
                "pay_cycle_end_date": "2019-03-05T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef67",
                "pay_cycle_number": 5,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 5-26",
                "pay_cycle_start_date": "2019-03-05T18:30:00.000Z",
                "pay_cycle_end_date": "2019-03-21T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef68",
                "pay_cycle_number": 6,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 6-26",
                "pay_cycle_start_date": "2019-03-21T18:30:00.000Z",
                "pay_cycle_end_date": "2019-04-06T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef69",
                "pay_cycle_number": 7,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 7-26",
                "pay_cycle_start_date": "2019-04-06T18:30:00.000Z",
                "pay_cycle_end_date": "2019-04-22T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef6a",
                "pay_cycle_number": 8,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 8-26",
                "pay_cycle_start_date": "2019-04-22T18:30:00.000Z",
                "pay_cycle_end_date": "2019-05-08T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef6b",
                "pay_cycle_number": 9,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 9-26",
                "pay_cycle_start_date": "2019-05-08T18:30:00.000Z",
                "pay_cycle_end_date": "2019-05-24T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef6c",
                "pay_cycle_number": 10,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 10-26",
                "pay_cycle_start_date": "2019-05-24T18:30:00.000Z",
                "pay_cycle_end_date": "2019-06-09T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef6d",
                "pay_cycle_number": 11,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 11-26",
                "pay_cycle_start_date": "2019-06-09T18:30:00.000Z",
                "pay_cycle_end_date": "2019-06-25T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef6e",
                "pay_cycle_number": 12,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 12-26",
                "pay_cycle_start_date": "2019-06-25T18:30:00.000Z",
                "pay_cycle_end_date": "2019-07-11T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef6f",
                "pay_cycle_number": 13,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 13-26",
                "pay_cycle_start_date": "2019-07-11T18:30:00.000Z",
                "pay_cycle_end_date": "2019-07-27T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef70",
                "pay_cycle_number": 14,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 14-26",
                "pay_cycle_start_date": "2019-07-27T18:30:00.000Z",
                "pay_cycle_end_date": "2019-08-12T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef71",
                "pay_cycle_number": 15,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 15-26",
                "pay_cycle_start_date": "2019-08-12T18:30:00.000Z",
                "pay_cycle_end_date": "2019-08-28T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef72",
                "pay_cycle_number": 16,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 16-26",
                "pay_cycle_start_date": "2019-08-28T18:30:00.000Z",
                "pay_cycle_end_date": "2019-09-13T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef73",
                "pay_cycle_number": 17,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 17-26",
                "pay_cycle_start_date": "2019-09-13T18:30:00.000Z",
                "pay_cycle_end_date": "2019-09-29T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.036Z",
                "updatedAt": "2019-07-18T11:13:16.036Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef63",
                "pay_cycle_number": 1,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 1-26",
                "pay_cycle_start_date": "2019-01-01T00:00:00.000Z",
                "pay_cycle_end_date": "2019-01-16T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.035Z",
                "updatedAt": "2019-07-18T11:13:16.035Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef64",
                "pay_cycle_number": 2,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 2-26",
                "pay_cycle_start_date": "2019-01-16T18:30:00.000Z",
                "pay_cycle_end_date": "2019-02-01T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.035Z",
                "updatedAt": "2019-07-18T11:13:16.035Z"
            },
            {
                "_id": "5d30544cbe55f2083c48ef65",
                "pay_cycle_number": 3,
                "pay_cycle_name": "Calendar1_Semi_Monthly: 3-26",
                "pay_cycle_start_date": "2019-02-01T18:30:00.000Z",
                "pay_cycle_end_date": "2019-02-17T18:29:59.000Z",
                "companyId": "5c9e11894b1d726375b23058",
                "calendar": "5d30544bbe55f2083c48ef62",
                "createdAt": "2019-07-18T11:13:16.035Z",
                "updatedAt": "2019-07-18T11:13:16.035Z"
            }
        ]
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {post} /payroll-calendars/list ListPayrollCalendars
 * @apiName ListPayrollCalendars
 * @apiGroup PayrollCalendars
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"per_page": 5,
	"page_no": 1,
	"archive": false
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched payroll calendars",
    "total_count": 1,
    "data": [
        {
            "_id": "5d30544bbe55f2083c48ef62",
            "archive": false,
            "companyId": "5c9e11894b1d726375b23058",
            "calendar_name": "Calendar1_Semi_Monthly",
            "calendar_year": "2019",
            "number_of_cycles": 26,
            "number_of_days_in_a_cycle": 15,
            "first_cycle_start_date": "2019-01-01T00:00:00.000Z",
            "first_cycle_end_date": "2019-01-15T23:59:59.000Z",
            "assigned_payroll_group": "5d2d89f7c9dcf43300723e18",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-07-18T11:13:15.800Z",
            "updatedAt": "2019-07-18T11:13:15.800Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {put} /payroll-calendars/edit EditPayrollCalendar
 * @apiName EditPayrollCalendar
 * @apiGroup PayrollCalendars
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "_id": "5d358db57779d43e53b6310c",
    "archive": false,
    "number_of_cycles": 3,
    "calendar_name": "test_3",
    "number_of_days_in_a_cycle": 15,
    "first_cycle_start_date": "2019-07-01T18:30:00.000Z",
    "first_cycle_end_date": "2019-07-16T18:30:00.000Z",
    "assigned_payroll_group": "5d358d767779d43e53b6310b",
    "calendar_year": "2019",
    "companyId": "5c9e11894b1d726375b23058",
    "createdBy": "5c9e11894b1d726375b23057",
    "createdAt": "2019-07-22T10:19:33.649Z",
    "updatedAt": "2019-07-22T10:19:33.649Z",
    "paycycle_dates": [
        {
            "_id": "5d358db57779d43e53b6310d",
            "pay_cycle_number": 1,
            "pay_cycle_name": "test_2: 1-3",
            "pay_cycle_start_date": "2019-07-01T18:30:00.000Z",
            "pay_cycle_end_date": "2019-07-16T18:30:00.000Z",
            "companyId": "5c9e11894b1d726375b23058",
            "calendar": "5d358db57779d43e53b6310c",
            "createdAt": "2019-07-22T10:19:33.657Z",
            "updatedAt": "2019-07-22T10:19:33.657Z"
        },
        {
            "_id": "5d358db57779d43e53b6310e",
            "pay_cycle_number": 2,
            "pay_cycle_name": "test_2: 2-3",
            "pay_cycle_start_date": "2019-07-17T18:30:00.000Z",
            "pay_cycle_end_date": "2019-08-01T18:30:00.000Z",
            "companyId": "5c9e11894b1d726375b23058",
            "calendar": "5d358db57779d43e53b6310c",
            "createdAt": "2019-07-22T10:19:33.657Z",
            "updatedAt": "2019-07-22T10:19:33.657Z"
        },
        {
            "_id": "5d358db57779d43e53b6310f",
            "pay_cycle_number": 3,
            "pay_cycle_name": "test_2: 3-3",
            "pay_cycle_start_date": "2019-08-02T18:30:00.000Z",
            "pay_cycle_end_date": "2019-08-17T18:30:00.000Z",
            "companyId": "5c9e11894b1d726375b23058",
            "calendar": "5d358db57779d43e53b6310c",
            "createdAt": "2019-07-22T10:19:33.657Z",
            "updatedAt": "2019-07-22T10:19:33.657Z"
        }
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Payroll calendar and pay cycles updated successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /payroll-calendars/current-and-next-cycle-details/:company_id/:calendar_id GetCurrentAndNextCyclesInfo
 * @apiName GetCurrentAndNextCyclesInfo
 * @apiGroup PayrollCalendars
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} calendar_id PayrollCalendars unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched next pay cycle details.",
    "data": {
        "current_paycycle": null,
        "nextCycleIsFirstCycle": true,
        "next_pay_cycle": {
            "_id": "5d3944b9fe20e2443b752f89",
            "pay_cycle_number": 1,
            "pay_cycle_name": "t: 1-9",
            "pay_cycle_start_date": "2019-07-15T18:30:00.000Z",
            "pay_cycle_end_date": "2019-07-30T18:30:00.000Z",
            "companyId": "5c9e11894b1d726375b23058",
            "calendar": "5d35ae6103db363ff425325a",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-07-25T05:57:13.511Z",
            "updatedAt": "2019-07-25T05:57:13.511Z"
        }
    }
}
 * @apiSuccessExample Success-Response2:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched current and next pay cycle details.",
    "data": {
        current_paycycle: {
            "_id": "5d3944b9fe20e2443b752f89",
            "pay_cycle_number": 1,
            "pay_cycle_name": "t: 1-9",
            "pay_cycle_start_date": "2019-07-15T18:30:00.000Z",
            "pay_cycle_end_date": "2019-07-30T18:30:00.000Z",
            "companyId": "5c9e11894b1d726375b23058",
            "calendar": "5d35ae6103db363ff425325a",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-07-25T05:57:13.511Z",
            "updatedAt": "2019-07-25T05:57:13.511Z"
        },
        currentCycleIsLastCycle: false,
        next_pay_cycle : {
            "_id": "5d3944b9fe20e2443b752f8a",
            "pay_cycle_number": 2,
            "pay_cycle_name": "t: 2-9",
            "pay_cycle_start_date": "2019-07-31T18:30:00.000Z",
            "pay_cycle_end_date": "2019-08-15T18:30:00.000Z",
            "companyId": "5c9e11894b1d726375b23058",
            "calendar": "5d35ae6103db363ff425325a",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-07-25T05:57:13.511Z",
            "updatedAt": "2019-07-25T05:57:13.511Z"
        }
    }
}
 * @apiSuccessExample Success-Response3:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched current pay cycle details. This is the last paycyle in this calendar.",
    "data": {
        current_paycycle: {
            "_id": "5d3944b9fe20e2443b752f91",
            "pay_cycle_number": 9,
            "pay_cycle_name": "t: 9-9",
            "pay_cycle_start_date": "2019-11-20T18:30:00.000Z",
            "pay_cycle_end_date": "2019-12-05T18:30:00.000Z",
            "companyId": "5c9e11894b1d726375b23058",
            "calendar": "5d35ae6103db363ff425325a",
            "createdBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-07-25T05:57:13.511Z",
            "updatedAt": "2019-07-25T05:57:13.511Z"
        },
        currentCycleIsLastCycle: true,
        next_pay_cycle : null
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /payroll-calendars/start-next-cycle StartNextCycle
 * @apiName StartNextCycle
 * @apiGroup PayrollCalendars
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"_id": "5d30544bbe55f2083c48ef62"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Started next cycle.",
    "data": {
        "status": "Open",
        "_id": "5d30544cbe55f2083c48ef70",
        "pay_cycle_number": 14,
        "pay_cycle_name": "Calendar1_Semi_Monthly: 14-26",
        "pay_cycle_start_date": "2019-07-27T18:30:00.000Z",
        "pay_cycle_end_date": "2019-08-12T18:29:59.000Z",
        "companyId": "5c9e11894b1d726375b23058",
        "calendar": "5d30544bbe55f2083c48ef62",
        "createdAt": "2019-07-18T11:13:16.036Z",
        "updatedAt": "2019-07-26T06:47:58.742Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /payroll-calendars/create-copy CopyCalendar
 * @apiName CopyCalendar
 * @apiGroup PayrollCalendars
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"_id": "5d3ad4cb1642a304423a28fc"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully created a copy of the calendar",
    "data": {
        "archive": false,
        "number_of_cycles": 12,
        "calendar_name": "tes_2 - copy",
        "number_of_days_in_a_cycle": 31,
        "first_cycle_start_date": "2019-07-26T18:30:00.000Z",
        "first_cycle_end_date": "2019-08-26T18:30:00.000Z",
        "assigned_payroll_group": "5d3acba1b10ad902a6b43bcc",
        "calendar_year": "2019",
        "companyId": "5c9e11894b1d726375b23058",
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /payroll-calendars/reopen-cycle ReopenPreviousCycle
 * @apiName ReopenPreviousCycle
 * @apiGroup PayrollCalendars
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"_id": "5d30544bbe55f2083c48ef62"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Reopened previous cycle.",
    "data": {
        "status": "Open",
        "_id": "5d30544cbe55f2083c48ef70",
        "pay_cycle_number": 14,
        "pay_cycle_name": "Calendar1_Semi_Monthly: 14-26",
        "pay_cycle_start_date": "2019-07-27T18:30:00.000Z",
        "pay_cycle_end_date": "2019-08-12T18:29:59.000Z",
        "companyId": "5c9e11894b1d726375b23058",
        "calendar": "5d30544bbe55f2083c48ef62",
        "createdAt": "2019-07-18T11:13:16.036Z",
        "updatedAt": "2019-07-26T06:47:58.742Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /payroll-calendars/previous-and-current-cycle-details/:company_id/:calendar_id GetPreviousAndCurrentCycleInfo
 * @apiName GetPreviousAndCurrentCycleInfo
 * @apiGroup PayrollCalendars
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} calendar_id PayrollCalendars unique _id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched previous and current cycle details",
    "data": {
        "previous_cycle": {
            "status": "Closed",
            "_id": "5d4004076f05772f0419fa29",
            "pay_cycle_number": 1,
            "pay_cycle_name": "new_today: 1-5",
            "pay_cycle_start_date": "2019-07-29T18:30:00.000Z",
            "pay_cycle_end_date": "2019-07-30T18:30:00.000Z",
            "companyId": "5c9e11894b1d726375b23058",
            "calendar": "5d4004076f05772f0419fa28",
            "createdAt": "2019-07-30T08:47:03.082Z",
            "updatedAt": "2019-07-30T08:47:03.082Z"
        },
        "active_cycle": {
            "_id": "5d4004076f05772f0419fa2a",
            "status": "Closed",
            "pay_cycle_number": 2,
            "pay_cycle_name": "new_today: 2-5",
            "pay_cycle_start_date": "2019-07-31T00:00:00.000Z",
            "pay_cycle_end_date": "2019-08-27T18:30:00.000Z",
            "companyId": "5c9e11894b1d726375b23058",
            "calendar": "5d4004076f05772f0419fa28",
            "createdAt": "2019-07-30T08:47:03.082Z",
            "updatedAt": "2019-07-30T08:47:03.082Z"
        }
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {delete} /payroll-calendars/delete/:company_id/:calendar_id DeletePayrollCalendar
 * @apiName DeletePayrollCalendar
 * @apiGroup PayrollCalendars
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} calendar_id PayrollCalendars unique _id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully deleted payroll calendar",
    "data": [
        {
            "n": 1,
            "ok": 1
        },
        {
            "n": 12,
            "ok": 1
        }
    ]
}
 * @apiErrorExample ErrorResponse:
    HTTP/1.1 400 Bad Request
{
    "status": false,
    "message": "This calendar is active. It cannot be deleted"
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */