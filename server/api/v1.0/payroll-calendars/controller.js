let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let async = require('async');
let config = require('../../../config/config');
let _ = require('underscore');
let PayrollCalendarsModel = require('./model').PayrollCalendarsModel;
let PayCyclesModel = require('./model').PayCyclesModel;
let PayrollGroupsModel = require('../payroll-groups/model');

const _MS_PER_DAY = 1000 * 60 * 60 * 24;

/**
 * Method to calculate all pay cycle dates when follow parameters are given
 * @param {Number} totalCycles 
 * @param {Number} totalDaysInCycle 
 * @param {Date} firstCycleStart 
 * @param {Date} firstCycleEnd 
 * @author Surendra Waso 
 */
function getPayCycleCalendarDatesForBiWeekly(totalCycles, totalDaysInCycle, firstCycleStart, firstCycleEnd) {
    var oneDay = _MS_PER_DAY
    var allPayCycleDates = [];
    var payCycleObject = {
        "payCycleNumber": 1,
        "payCycleStart": new Date(firstCycleStart),
        "payCycleEnd": new Date(new Date(firstCycleEnd).setHours(23, 59, 59))
    };

    allPayCycleDates.push(payCycleObject);

    for (var i = 1; i < totalCycles; i++) {
        var thisPayCycleStart = new Date(new Date(allPayCycleDates[i - 1].payCycleEnd).setHours(0, 0, 0, 0) + oneDay);
        var thisPayCycleEnd = new Date(new Date(allPayCycleDates[i - 1].payCycleEnd.getTime() + (oneDay * totalDaysInCycle) + oneDay).setHours(23, 59, 59));
        allPayCycleDates.push({
            "payCycleNumber": i + 1,
            "payCycleStart": thisPayCycleStart,//new Date(firstCycleStart),
            "payCycleEnd": thisPayCycleEnd
        });
    }

    return allPayCycleDates;
}

/**
 * API to calculate all pay cycle dates
 * @param {*} req 
 * @param {*} res 
 * @author Asma Mubeen (18-07-2019)
 */
let getPayCycleDates = (req, res) => {
    let body = req.body
    return new Promise((resolve, reject) => {
        if (!body.number_of_cycles) reject('number_of_cycles is required');
        else if (!body.calendar_name) reject('calendar_name is required');
        else if (!body.number_of_days_in_a_cycle) reject('number_of_days_in_a_cycle is required');
        else if (!body.first_cycle_start_date) reject('first_cycle_start_date is required');
        else if (!body.first_cycle_end_date) reject('first_cycle_end_date is required');
        else if (dateDiffInDays(new Date(body.first_cycle_start_date), new Date(body.first_cycle_end_date)) != body.number_of_days_in_a_cycle) reject('Difference between given dates is not equal to given number of days in a cycle');
        else resolve(null);
    })
        .then(() => {
            var oneDay = _MS_PER_DAY
            var allPayCycleDates = [];
            var payCycleObject = {
                "pay_cycle_number": 1,
                "pay_cycle_name": body.calendar_name + ": " + 1 + "-" + body.number_of_cycles,
                "pay_cycle_start_date": new Date(body.first_cycle_start_date),
                // "pay_cycle_end_date": new Date(new Date(body.first_cycle_end_date).setHours(23, 59, 59))
                "pay_cycle_end_date": new Date(body.first_cycle_end_date)
            };


            allPayCycleDates.push(payCycleObject);

            for (var i = 1; i < body.number_of_cycles; i++) {
                // var thisPayCycleStart = new Date(new Date(allPayCycleDates[i - 1].payCycleEnd).setHours(0, 0, 0, 0) + oneDay);
                // var thisPayCycleStart = new Date(new Date(allPayCycleDates[i - 1].pay_cycle_end_date.getTime() + oneDay).setHours(0,0,0));
                var thisPayCycleStart = new Date(allPayCycleDates[i - 1].pay_cycle_end_date.getTime() + oneDay);
                // var thisPayCycleEnd = new Date(new Date(allPayCycleDates[i - 1].pay_cycle_end_date.getTime() + (oneDay * body.number_of_days_in_a_cycle) + oneDay).setHours(23, 59, 59));
                var thisPayCycleEnd = new Date(allPayCycleDates[i - 1].pay_cycle_end_date.getTime() + (oneDay * body.number_of_days_in_a_cycle) + oneDay);
                allPayCycleDates.push({
                    "pay_cycle_number": i + 1,
                    "pay_cycle_name": body.calendar_name + ": " + (i + 1) + "-" + body.number_of_cycles,
                    "pay_cycle_start_date": thisPayCycleStart,
                    "pay_cycle_end_date": thisPayCycleEnd
                });
            }

            res.status(200).json({
                status: true,
                message: "Succesfully calculated pay cycle dates",
                data: allPayCycleDates
            })
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })

}

/**
 * Method to get difference between two dates in days
 * @param {*} date1 
 * @param {*} date2 
 * Asma Mubeen
 */
let dateDiffInDays = (date1, date2) => {
    // Discard the time and time-zone information.

    const utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
    const utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

let addPayrollCalendar = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let paycycle_dates = body.paycycle_dates
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required')
        else if (!paycycle_dates) reject('paycycle_dates field is required')
        else if (!paycycle_dates.length > 0) reject('paycycle_dates not found');
        else resolve(null);
    })
        .then(() => {
            body.createdBy = ObjectID(req.jwt.userId)
            let promises = [
                PayrollCalendarsModel.findOne({ companyId: companyId, calendar_name: body.calendar_name }).lean().exec(),
            ]

            promises.push(PayCyclesModel.find({ companyId: companyId, assigned_payroll_group: body.assigned_payroll_group }).sort({ pay_cycle_end_date: -1 }).limit(1).lean().exec())
            promises.push(PayCyclesModel.find({ companyId: companyId, assigned_payroll_group: body.assigned_payroll_group }).sort({ pay_cycle_first_date: 1 }).limit(1).lean().exec())
            promises.push(getAllCalendarsStartAndEndDates(companyId, body.assigned_payroll_group))

            Promise.all(promises)
                .then(([calendar_with_name, existing_last_paycycle, existing_first_paycyle, all_calendars_start_end_dates]) => {
                    console.log('all_calendars_start_end_dates - ', all_calendars_start_end_dates)
                    console.log('existing last pay cycle - ', existing_last_paycycle)
                    console.log('existing first pay cycle - ', existing_first_paycyle)
                    let given_last_paycycle = paycycle_dates[paycycle_dates.length - 1]
                    let given_first_paycycle = paycycle_dates[0]
                    let given_cal_start_date = new Date(given_first_paycycle.pay_cycle_start_date)
                    let given_cal_end_date = new Date(given_last_paycycle.pay_cycle_end_date)
                    existing_last_paycycle = existing_last_paycycle[0]
                    existing_first_paycyle = existing_first_paycyle[0]

                    let existing_cal_start_date = existing_first_paycyle ? new Date(existing_first_paycyle.pay_cycle_start_date) : null
                    let existing_cal_end_date = existing_last_paycycle ? new Date(existing_last_paycycle.pay_cycle_end_date) : null

                    if (calendar_with_name) {
                        res.status(400).json({
                            status: false,
                            message: "Payroll calendar already exists with this name"
                        })
                    }
                    // else if (existing_first_paycyle && !((given_cal_start_date < existing_cal_start_date && given_cal_end_date < existing_cal_start_date) || (given_cal_start_date > existing_cal_end_date && given_cal_end_date > existing_cal_end_date))){
                    //     res.status(400).json({
                    //         status: false,
                    //         message: "Calendar dates are invalid. Dates are overlapping with other calendars."
                    //     })
                    // }
                    else {
                        for (let calendar of all_calendars_start_end_dates) {
                            if ((new Date(calendar.start_date) <= given_cal_start_date && given_cal_start_date <= new Date(calendar.end_date)) || (new Date(calendar.start_date) <= given_cal_end_date && given_cal_end_date <= new Date(calendar.end_date))) {
                                throw "DATES_OVERLAP_ERROR"
                            }
                        }
                        let allPayCycleDates = []
                        for (let paycycle_dates_obj of paycycle_dates) {
                            allPayCycleDates.push(paycycle_dates_obj.pay_cycle_start_date)
                            allPayCycleDates.push(paycycle_dates_obj.pay_cycle_end_date)
                        }
                        for (let i = 0; i < allPayCycleDates.length - 1; i++) {
                            console.log('date', i, ' - ', allPayCycleDates[i])
                            console.log('date', (i + 1), ' - ', allPayCycleDates[i + 1])
                            if (allPayCycleDates[i] < allPayCycleDates[i + 1]) { }
                            else {
                                res.status(400).json({
                                    status: false,
                                    message: "Paycycle dates are overlapping"
                                })
                            }
                        }
                        PayrollCalendarsModel.create(body)
                            .then(payroll_calendar => {
                                for (let paycycle_dates_obj of paycycle_dates) {
                                    paycycle_dates_obj.companyId = companyId
                                    paycycle_dates_obj.calendar = payroll_calendar._id
                                }
                                PayCyclesModel.insertMany(paycycle_dates)
                                    .then(paycycles_insert_response => {
                                        console.log('paycycles insert response - ', paycycles_insert_response)
                                        res.status(200).json({
                                            status: true,
                                            message: "Successfully added payroll calendar",
                                            data: payroll_calendar
                                        })
                                    })
                                    .catch(err => {
                                        console.log(err)
                                        res.status(500).json({
                                            status: false,
                                            message: "Error while adding payroll calendar",
                                            error: err
                                        })
                                    })
                            })
                            .catch(err => {
                                console.log(err)
                                res.status(400).json({
                                    status: false,
                                    message: "Error while adding payroll calendar",
                                    error: err
                                })
                            })
                    }
                })
                .catch(err => {
                    console.log(err)
                    if (err === 'DATES_OVERLAP_ERROR') {
                        res.status(400).json({
                            status: false,
                            message: "Calendar dates are overlapping with existing calendars with same payroll group"
                        })
                    }
                    else {
                        res.status(400).json({
                            status: false,
                            message: "Error while adding payroll calendar",
                            error: err
                        })
                    }

                })

        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        });
}

let getAllCalendarsStartAndEndDates = (companyId, assigned_payroll_group) => {
    return new Promise((resolve, reject) => {
        var calendar_dates = []
        PayrollCalendarsModel.find({ companyId: companyId, assigned_payroll_group: assigned_payroll_group }, { _id: 1 }).lean().exec()
            .then(async calendars => {
                for (let calendar of calendars) {
                    let calendar_details = {
                        calendar_id: calendar._id
                    }
                    let first_pay_cycle = await PayCyclesModel.find({ companyId: companyId, calendar: calendar._id }).sort({ pay_cycle_start_date: 1 }).limit(1).lean().exec()
                    let last_pay_cycle = await PayCyclesModel.find({ companyId: companyId, calendar: calendar._id }).sort({ pay_cycle_end_date: -1 }).limit(1).lean().exec()
                    first_pay_cycle = first_pay_cycle[0]
                    last_pay_cycle = last_pay_cycle[0]
                    calendar_details['start_date'] = first_pay_cycle.pay_cycle_start_date
                    calendar_details['end_date'] = last_pay_cycle.pay_cycle_end_date
                    calendar_dates.push(calendar_details)
                }
                resolve(calendar_dates)
            })
            .catch(err => reject(err))
    })
}

let getAllExceptOneCalendarsStartAndEndDates = (companyId, assigned_payroll_group, payroll_calendar_id) => {
    return new Promise((resolve, reject) => {
        var calendar_dates = []
        PayrollCalendarsModel.find({ companyId: companyId, assigned_payroll_group: assigned_payroll_group, _id: { $ne: payroll_calendar_id } }, { _id: 1 }).lean().exec()
            .then(async calendars => {
                for (let calendar of calendars) {
                    let calendar_details = {
                        calendar_id: calendar._id
                    }
                    let first_pay_cycle = await PayCyclesModel.find({ companyId: companyId, calendar: calendar._id }).sort({ pay_cycle_start_date: 1 }).limit(1).lean().exec()
                    let last_pay_cycle = await PayCyclesModel.find({ companyId: companyId, calendar: calendar._id }).sort({ pay_cycle_end_date: -1 }).limit(1).lean().exec()
                    first_pay_cycle = first_pay_cycle[0]
                    last_pay_cycle = last_pay_cycle[0]
                    calendar_details['start_date'] = first_pay_cycle.pay_cycle_start_date
                    calendar_details['end_date'] = last_pay_cycle.pay_cycle_end_date
                    calendar_dates.push(calendar_details)
                }
                resolve(calendar_dates)
            })
            .catch(err => reject(err))
    })
}

let getPayrollCalendar = (req, res) => {
    let companyId = req.params.company_id
    let payroll_calendar_id = req.params._id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
        .then(() => {
            let find_filter = {
                companyId: ObjectID(companyId),
                _id: ObjectID(payroll_calendar_id)
            }
            let pay_cycle_dates_filter = {
                companyId: ObjectID(companyId),
                calendar: ObjectID(payroll_calendar_id)
            }
            Promise.all([
                PayrollCalendarsModel.findOne(find_filter).populate('assigned_payroll_group').lean().exec(),
                PayCyclesModel.find(pay_cycle_dates_filter).sort({ pay_cycle_number: 1 }).lean().exec()
            ])
                .then(([payroll_calendar, paycycle_dates]) => {
                    if (payroll_calendar) {
                        payroll_calendar.paycycle_dates = paycycle_dates
                        res.status(200).json({
                            status: true,
                            message: "Successfully fetched payroll calendar",
                            data: payroll_calendar
                        })
                    }
                    else {
                        res.status(404).json({
                            status: false,
                            message: "Payroll calendar not found"
                        })
                    }
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).json({
                        status: false,
                        message: "Error while fecthing payroll calendar",
                        error: err
                    })
                })
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                status: false,
                message: "Error while fecthing payroll calendar",
                error: err
            })
        })

}

let list = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required')
        else resolve(null);
    })
        .then(() => {
            let sort = {
                updatedAt: -1
            }
            let filter = {
                companyId: ObjectID(companyId),
            }
            let per_page = body.per_page ? body.per_page : 10
            let page_no = body.page_no ? body.page_no : 1
            if (body.hasOwnProperty('archive')) {
                filter['archive'] = body.archive
            }
            Promise.all([
                PayrollCalendarsModel.find(filter).sort(sort).skip((page_no - 1) * per_page).limit(per_page).lean().exec(),
                PayrollCalendarsModel.countDocuments(filter)
            ])
                .then(async ([payroll_calendars, total_count]) => {
                    // let active_cycle = await PayCyclesModel.findOne({ companyId: companyId, pay_cycle_start_date: { $lte: new Date() }, pay_cycle_end_date: { $gte: new Date() } }).lean().exec()
                    // console.log("active_cycle - ", active_cycle)
                    for (let calendar of payroll_calendars) {
                        let active_cycle = await PayCyclesModel.findOne({ companyId: companyId, calendar: calendar._id, pay_cycle_start_date: { $lte: new Date() }, pay_cycle_end_date: { $gte: new Date() } }).lean().exec()
                        calendar.active_cycle = active_cycle
                    }
                    res.status(200).json({
                        status: true, message: "Successfully fetched payroll calendars", total_count: total_count, data: payroll_calendars
                    })
                })
                .catch(err => {
                    console.log(err)
                    res.status(400).json({
                        status: false,
                        message: "Error while fetching payroll calendars",
                        error: err
                    })
                });
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

/**
 * Method to delete multiple payroll calendars
 * @param {*} req 
 * @param {*} res 
 */
let deletePayrollCalendars = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        else resolve(null);
    })
        .then(async () => {
            let promises = []

            //check active cycles for each calendar
            let active_cycle = await PayCyclesModel.findOne({ companyId: companyId, pay_cycle_start_date: { $lte: new Date() }, pay_cycle_end_date: { $gte: new Date() } }).lean().exec()
            let pay_calendars = await PayrollCalendarsModel.find({ companyId: companyId, _id: { $in: body._ids } }).lean().exec()
            // console.log(pay_calendars)
            async.each(pay_calendars, (calendar, callback) => {
                // if (calendar && calendar.assigned_payroll_group) {
                //     callback("Calendar '" + calendar.calendar_name + "' is assigned to a payroll group. It cannot be deleted.")
                // }
                if (calendar._id == active_cycle.calendar) {
                    callback("Calendar '" + calendar.calendar_name + "' is active. It cannot be deleted.")
                }
                else {
                    promises.push(PayrollCalendarsModel.deleteOne({ companyId: companyId, _id: calendar._id }))
                    promises.push(PayCyclesModel.deleteMany({ companyId: companyId, calendar: calendar._id }))
                    callback()
                }
                // console.log('---------------')
            }, (err) => {
                console.log(err)
                if (err) {
                    // console.log(err)

                    Promise.all(promises)
                        .then(responses => {
                            console.log(responses)
                            res.status(400).json({
                                status: false,
                                message: err
                            })
                        })
                        .catch(e => {
                            console.log(e)
                            res.satus(500).json({
                                status: false,
                                message: e
                            })
                        })
                }
                else {
                    Promise.all(promises)
                        .then(responses => {
                            console.log(responses)
                            res.status(200).json({
                                status: true,
                                message: "Deleted calendars successfully"
                            })
                        })
                        .catch(err => {
                            console.log(err)
                            res.status(500).json({
                                status: false,
                                message: "Error while deleting calendars",
                                error: error
                            })
                        })
                }
            })
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

/**
 * Method delete one payroll calendar
 * If the given calendar has an active paycyle then it cannot be deleted
 * @param {*} req 
 * @param {*} res 
 */
let deletePayrollCalendar = (req, res) => {
    let companyId = ObjectID(req.params.company_id)
    let calendar_id = ObjectID(req.params.calendar_id)
    return new Promise((resolve, reject) => {
        resolve(PayCyclesModel.findOne({ companyId: companyId, calendar: calendar_id, pay_cycle_start_date: { $lte: new Date() }, pay_cycle_end_date: { $gte: new Date() } }).lean().exec());
    })
        .then(async active_cycle => {
            if (!active_cycle) {
                let payroll_calendar = await PayrollCalendarsModel.findOne({companyId: companyId, _id: calendar_id}).lean().exec()
                let assigned_payroll_group = await PayrollGroupsModel.findOne({companyId: companyId, _id: payroll_calendar.assigned_payroll_group}).lean().exec()
                if (assigned_payroll_group.status === 'Inactive') {
                    Promise.all([
                        PayrollCalendarsModel.deleteOne({ companyId: companyId, _id: calendar_id }),
                        PayCyclesModel.deleteMany({ companyId: companyId, calendar: calendar_id })
                    ])
                        .then(responses => {
                            res.status(200).json({
                                status: true,
                                message: "Successfully deleted payroll calendar",
                                data: responses
                            })
                        })
                        .catch(err => {
                            console.log(err)
                            res.status(500).json({
                                status: false,
                                message: "Error while deleting payroll calendar",
                                error: err
                            })
                        })
                }
                else {
                    res.status(400).json({
                        status: false,
                        message: "This calendar is assigned to an active payroll group. It cannot be deleted"
                    })
                }                
            }
            else {
                res.status(400).json({
                    status: false,
                    message: "This calendar is active. It cannot be deleted"
                })
            }

        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })
}


let editPayrollCalendar = (req, res) => {
    let body = req.body
    let payroll_calendar_id = body._id
    let companyId = body.companyId
    let paycycle_dates = body.paycycle_dates
    delete body.paycycle_dates
    return new Promise((resolve, reject) => {
        if (!payroll_calendar_id) {
            reject('_id is required')
        }
        else if (!companyId) {
            reject('companyId is required')
        }
        else if (!paycycle_dates) reject('paycycle_dates field is required')
        else if (!paycycle_dates.length > 0) reject('paycycle_dates not found');
        else {
            resolve(null);
        }
    })
        .then(() => {
            delete body.companyId
            delete body._id
            delete body.createdAt
            delete body.updatedAt
            console.log(body)

            let promises = [
                PayrollCalendarsModel.findOne({ companyId: companyId, calendar_name: body.calendar_name, _id: { $ne: ObjectID(payroll_calendar_id) } }).lean().exec(),
            ]

            promises.push(PayCyclesModel.find({ companyId: companyId, assigned_payroll_group: body.assigned_payroll_group, _id: { $ne: ObjectID(payroll_calendar_id) } }).sort({ pay_cycle_end_date: -1 }).limit(1).lean().exec())
            promises.push(PayCyclesModel.find({ companyId: companyId, assigned_payroll_group: body.assigned_payroll_group, _id: { $ne: ObjectID(payroll_calendar_id) } }).sort({ pay_cycle_first_date: 1 }).limit(1).lean().exec())
            promises.push(getAllExceptOneCalendarsStartAndEndDates(companyId, body.assigned_payroll_group, payroll_calendar_id))

            Promise.all(promises)
                .then(([calendar_with_name, existing_last_paycycle, existing_first_paycyle, all_calendars_start_end_dates]) => {
                    console.log('all_calendars_start_end_dates - ', all_calendars_start_end_dates)
                    console.log('existing last pay cycle - ', existing_last_paycycle)
                    console.log('existing first pay cycle - ', existing_first_paycyle)
                    let given_last_paycycle = paycycle_dates[paycycle_dates.length - 1]
                    let given_first_paycycle = paycycle_dates[0]
                    let given_cal_start_date = new Date(given_first_paycycle.pay_cycle_start_date)
                    let given_cal_end_date = new Date(given_last_paycycle.pay_cycle_end_date)
                    existing_last_paycycle = existing_last_paycycle[0]
                    existing_first_paycyle = existing_first_paycyle[0]

                    if (calendar_with_name) {
                        res.status(400).json({
                            status: false,
                            message: "Payroll calendar already exists with this name"
                        })
                    }
                    else {
                        for (let calendar of all_calendars_start_end_dates) {
                            if ((new Date(calendar.start_date) <= given_cal_start_date && given_cal_start_date <= new Date(calendar.end_date)) || (new Date(calendar.start_date) <= given_cal_end_date && given_cal_end_date <= new Date(calendar.end_date))) {
                                throw "DATES_OVERLAP_ERROR"
                            }
                        }
                        let allPayCycleDates = []
                        for (let paycycle_dates_obj of paycycle_dates) {
                            allPayCycleDates.push(paycycle_dates_obj.pay_cycle_start_date)
                            allPayCycleDates.push(paycycle_dates_obj.pay_cycle_end_date)
                        }
                        for (let i = 0; i < allPayCycleDates.length - 1; i++) {
                            console.log('date', i, ' - ', allPayCycleDates[i])
                            console.log('date', (i + 1), ' - ', allPayCycleDates[i + 1])
                            if (allPayCycleDates[i] < allPayCycleDates[i + 1]) { }
                            else {
                                res.status(400).json({
                                    status: false,
                                    message: "Paycycle dates are overlapping"
                                })
                            }
                        }

                        let filter = {
                            _id: ObjectID(payroll_calendar_id),
                            companyId: ObjectID(companyId)
                        }
                        console.log(filter)
                        body.updatedBy = ObjectID(req.jwt.userId)
                        PayrollCalendarsModel.findOneAndUpdate(filter, { $set: body }, { new: true, useFindAndModify: false })
                            .then(async response => {
                                console.log(response)
                                if (paycycle_dates && paycycle_dates.length > 0) {
                                    let promises = []
                                    let paycycle_ids = paycycle_dates.map(x => x._id)
                                    console.log('paycycle_ids - ', paycycle_ids)
                                    let delete_response = await PayCyclesModel.deleteMany({companyId: companyId, calendar: ObjectID(payroll_calendar_id), _id: {$nin: paycycle_ids}})
                                    console.log('delete_response - ', delete_response)
                                    for (let paycycle_date of paycycle_dates) {
                                        if (paycycle_date._id) {
                                            let paycycle_update_filter = {
                                                _id: paycycle_date._id,
                                                companyId: companyId
                                            }
                                            delete paycycle_date.companyId
                                            delete paycycle_date.createdAt
                                            delete paycycle_date.updatedAt
                                            delete paycycle_date._id
                                            delete paycycle_date.calendar
                                            paycycle_date.updatedBy = ObjectID(req.jwt.userId)
                                            promises.push(PayCyclesModel.findOneAndUpdate(paycycle_update_filter, { $set: paycycle_date }))
                                        }
                                        else {
                                            paycycle_date.companyId = companyId
                                            paycycle_date.calendar = payroll_calendar_id
                                            paycycle_date.createdBy = ObjectID(req.jwt.userId)
                                            promises.push(PayCyclesModel.create(paycycle_date))
                                        }
                                    }
                                    Promise.all(promises)
                                        .then(results => {
                                            console.log(results)
                                            res.status(200).json({
                                                status: true,
                                                message: "Payroll calendar and pay cycles updated successfully",
                                            })
                                        })
                                        .catch(err => {
                                            console.log(err)
                                            res.status(500).json({
                                                status: false,
                                                message: "Error while updating pay cycles",
                                                error: err
                                            })
                                        })
                                }
                                else {
                                    res.status(200).json({
                                        status: true,
                                        message: "Payroll calendar updated successfully",
                                        data: response
                                    })
                                }

                            })
                            .catch(err => {
                                res.status(400).json({
                                    status: false,
                                    message: "Error while updating payroll calendar",
                                    error: err
                                })
                            })
                    }
                })
                .catch(err => {
                    console.log(err)
                    if (err === 'DATES_OVERLAP_ERROR') {
                        res.status(400).json({
                            status: false,
                            message: "Calendar dates are overlapping with existing calendars with same payroll group"
                        })
                    }
                    else {
                        res.status(400).json({
                            status: false,
                            message: "Error while updating payroll calendar",
                            error: err
                        })
                    }
                })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: err
            })
        });
}

let startNextCycle = (req, res) => {
    let body = req.body
    let companyId = ObjectID(body.companyId)
    let calendar_id = ObjectID(body._id)
    return new Promise((resolve, reject) => {
        if (!companyId) reject("companyId is required");
        else if (!calendar_id) reject("_id is required")
        else resolve(PayCyclesModel.findOne({ companyId: companyId, calendar: calendar_id, pay_cycle_start_date: { $lte: new Date() }, pay_cycle_end_date: { $gte: new Date() } }).lean().exec())
    })
        .then(active_cycle => {
            if (!active_cycle) {
                PayCyclesModel.findOneAndUpdate(
                    {
                        companyId: companyId, calendar: calendar_id, pay_cycle_number: 1
                    },
                    {
                        $set: {
                            status: 'Open'
                        }
                    },
                    {
                        new: true,
                    }
                )
                    .then(response => {
                        if (response) {
                            res.status(200).json({
                                status: true,
                                message: "Started next cycle.",
                                data: response
                            })
                        }
                        else {
                            res.status(404).json({
                                status: false,
                                message: "First pay cycle not found"
                            })
                        }
                    })
                    .catch(err => {
                        console.log(err)
                        res.status(500).json({
                            status: false,
                            message: "Error while fecthing first pay cycle",
                            error: err
                        })
                    })
            }
            else {
                PayCyclesModel.findOneAndUpdate(
                    {
                        companyId: companyId, calendar: calendar_id, pay_cycle_number: (active_cycle.pay_cycle_number + 1)
                    },
                    {
                        $set: {
                            status: 'Open'
                        }
                    },
                    {
                        new: true,
                    }
                )
                    .then(response => {
                        if (response) {
                            res.status(200).json({
                                status: true,
                                message: "Started next cycle.",
                                data: response
                            })
                        }
                        else {
                            res.status(404).json({
                                status: false,
                                message: "This is the last pay cycle in this calendar"
                            })
                        }
                    })
                    .catch(err => {
                        console.log(err)
                        res.status(500).json({
                            status: false,
                            message: "Error while fecthing next pay cycle",
                            error: err
                        })
                    })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

let getCurrentAndNextCycle = (req, res) => {
    let companyId = ObjectID(req.params.company_id)
    let calendar_id = ObjectID(req.params.calendar_id)
    return new Promise((resolve, reject) => {
        resolve(
            Promise.all([
                PayCyclesModel.findOne({ companyId: companyId, calendar: calendar_id, pay_cycle_start_date: { $lte: new Date() }, pay_cycle_end_date: { $gte: new Date() } }).lean().exec(),
                PayCyclesModel.find({ companyId: companyId, calendar: calendar_id }).sort({ pay_cycle_number: -1 }).lean().exec()
            ])
        )
    })
        .then(([current_paycycle, paycycles]) => {
            let next_pay_cycle = null
            let last_pay_cycle = paycycles[0]

            if (current_paycycle && last_pay_cycle.pay_cycle_number == current_paycycle.pay_cycle_number) {
                res.status(200).json({
                    status: true,
                    message: "Successfully fetched current pay cycle details. This is the last paycyle in this calendar.",
                    data: {
                        current_paycycle: current_paycycle,
                        currentCycleIsLastCycle: true,
                        next_pay_cycle: next_pay_cycle
                    }
                })
            }
            else if (current_paycycle) {
                next_pay_cycle = paycycles.filter(x => x.pay_cycle_number == (current_paycycle.pay_cycle_number + 1))
                if (next_pay_cycle && next_pay_cycle.length != 1) {
                    console.log('next pay cycle array length not equal to 1')
                }
                next_pay_cycle = next_pay_cycle[0]
                res.status(200).json({
                    status: true,
                    message: "Successfully fetched current and next pay cycle details.",
                    data: {
                        current_paycycle: current_paycycle,
                        currentCycleIsLastCycle: false,
                        next_pay_cycle: next_pay_cycle
                    }
                })
            }
            else {
                next_pay_cycle = paycycles[paycycles.length - 1]
                res.status(200).json({
                    status: true,
                    message: "Successfully fetched next pay cycle details.",
                    data: {
                        current_paycycle: null,
                        nextCycleIsFirstCycle: true,
                        next_pay_cycle: next_pay_cycle
                    }
                })
            }

        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                status: false,
                message: "Error while fetching paycycles information",
                error: err
            })
        })
}

let copy_calendar = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let calendar_id = body._id
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required')
        else if (!calendar_id) reject('_id is required')
        else resolve(PayrollCalendarsModel.findOne({ companyId: companyId, _id: calendar_id }).lean().exec())
    })
        .then(calendar => {
            if (calendar) {
                delete calendar._id
                delete calendar.createdBy
                delete calendar.createdAt
                delete calendar.updatedAt
                calendar.calendar_name += ' - copy'
                res.status(200).json({
                    status: true,
                    message: "Successfully created a copy of the calendar",
                    data: calendar
                })
            }
            else {
                res.status(404).json({
                    status: false,
                    message: "Calendar not found"
                })
            }

        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

let reopenPreviousCycle = (req, res) => {
    let body = req.body
    let companyId = ObjectID(body.companyId)
    let calendar_id = ObjectID(body._id)
    return new Promise((resolve, reject) => {
        if (!companyId) reject("companyId is required");
        else if (!calendar_id) reject("_id is required");
        // else if (!body.active_cycle_number) reject('active_cycle_number is required');
        // else if (body.active_cycle_number == 1) reject('There is no previous cycle') ;
        else resolve(PayCyclesModel.findOne({ companyId: companyId, calendar: calendar_id, pay_cycle_start_date: { $lte: new Date() }, pay_cycle_end_date: { $gte: new Date() } }).lean().exec());
    })
        .then(active_cycle => {
            if (!active_cycle) {
                res.status(400).json({
                    status: false,
                    message: "There is no previous cycle"
                })
            }
            else if (active_cycle.pay_cycle_number == 1) {
                res.status(400).json({
                    status: false,
                    message: "There is no previous cycle"
                })
            }
            else {
                PayCyclesModel.findOneAndUpdate(
                    {
                        companyId: companyId, calendar: calendar_id, pay_cycle_number: (active_cycle.pay_cycle_number - 1)
                    },
                    {
                        $set: {
                            status: 'Open'
                        }
                    },
                    {
                        new: true,
                    }
                )
                    .then(response => {
                        if (response) {
                            res.status(200).json({
                                status: true,
                                message: "Reopened previous cycle.",
                                data: response
                            })
                        }
                        else {
                            res.status(404).json({
                                status: false,
                                message: "Previous cycle not found"
                            })
                        }
                    })
                    .catch(err => {
                        console.log(err)
                        res.status(500).json({
                            status: false,
                            message: "Error while fecthing previous pay cycle",
                            error: err
                        })
                    })
            }

        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

let GetPreviousAndCurrentCycle = (req, res) => {
    let companyId = ObjectID(req.params.company_id)
    let calendar_id = ObjectID(req.params.calendar_id)
    console.log('companyId - ', companyId)
    console.log('calendar_id - ', calendar_id)
    return new Promise((resolve, reject) => {
        if (!companyId) reject("companyId is required");
        else if (!calendar_id) reject("_id is required");
        else resolve(PayCyclesModel.findOne({ companyId: companyId, calendar: calendar_id, pay_cycle_start_date: { $lte: new Date() }, pay_cycle_end_date: { $gte: new Date() } }).lean().exec());
    })
        .then(active_cycle => {
            console.log("active_cycle - ", active_cycle)
            if (!active_cycle) {
                res.status(400).json({
                    status: false,
                    message: "There is no previous cycle"
                })
            }
            else if (active_cycle.pay_cycle_number == 1) {
                res.status(400).json({
                    status: false,
                    message: "There is no previous cycle"
                })
            }
            else {
                PayCyclesModel.findOne(
                    {
                        companyId: companyId, calendar: calendar_id, pay_cycle_number: (active_cycle.pay_cycle_number - 1)
                    }
                )
                    .then(response => {
                        if (response) {
                            res.status(200).json({
                                status: true,
                                message: "Successfully fetched previous and current cycle details",
                                data: {
                                    previous_cycle: response,
                                    active_cycle: active_cycle
                                }
                            })
                        }
                        else {
                            res.status(404).json({
                                status: false,
                                message: "Previous cycle not found"
                            })
                        }
                    })
                    .catch(err => {
                        console.log(err)
                        res.status(500).json({
                            status: false,
                            message: "Error while fecthing previous pay cycle",
                            error: err
                        })
                    })
            }

        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

module.exports = {
    getPayCycleDates,
    addPayrollCalendar,
    getPayrollCalendar,
    list,
    deletePayrollCalendars,
    deletePayrollCalendar,
    editPayrollCalendar,
    startNextCycle,
    getCurrentAndNextCycle,
    copy_calendar,
    reopenPreviousCycle,
    GetPreviousAndCurrentCycle
}


