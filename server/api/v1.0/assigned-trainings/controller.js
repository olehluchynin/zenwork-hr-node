let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../config/config');
let _ = require('underscore');
let AssignedTrainingsModel = require('./model');

/**
*@author Asma
*@date 17/05/2019 07:32
API to list trainings of a user
*/
let listUserTrainings = (req, res) => {
    let body = req.body
    let userId = body.userId
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!userId) {
            reject("userId is required")
        }
        else if (!companyId) {
            reject("companyId is required")
        }
        else {
            resolve(null);
        }
    })
        .then(() => {
            if (body.onlyActive) {
                let only_active_filter = {
                    "status": "In Progress",
                    "userId": ObjectID(userId),
                    "companyId": ObjectID(companyId)
                }
                AssignedTrainingsModel.find(only_active_filter)
                    .populate("training")
                    .then(active_trainings => {
                        res.status(200).json({
                            status: true,
                            message: "Successfully fetched active trainings",
                            data: active_trainings
                        })
                    })
                    .catch(err => {
                        res.status(400).json({
                            status: false,
                            message: "Error while fetching active trainings",
                            error: err
                        })
                    });
            }
            else {
                let query = {
                    "userId": ObjectID(userId),
                    "companyId": ObjectID(companyId),
                    "status": 'Completed'
                }

                let per_page = body.per_page ? body.per_page : 10
                let page_no = body.page_no ? body.page_no : 1
                let sort_obj = {
                    "updatedAt": -1
                }

                if (body.from_date && body.to_date) {
                    let from_date = new Date(body.from_date)
                    let to_date = new Date(body.to_date)
                    from_date.setHours(0, 0, 0, 0);
                    to_date.setHours(23, 59, 59, 999);
                    console.log(from_date);
                    console.log(to_date);
                    query.start_date = {
                        '$gte': from_date,
                        '$lte': to_date
                    }
                }
                else if (body.from_date) {
                    let from_date = new Date(body.from_date)
                    from_date.setHours(0, 0, 0, 0);
                    console.log(from_date);
                    query.start_date = {
                        '$gte': from_date
                    }
                }

                if(body.search_query) {
                    query['$or'] = [
                        {
                            'training.trainingName' : {$regex: body.search_query, $options: "i"} 
                        }
                    ]
                }

                let lookup_stage = {
                    $lookup:
                    {
                        from: "trainings",
                        localField: "training",
                        foreignField: "_id",
                        as: "training"
                    }
                }

                let project_stage = {
                    $project: {
                        "training": { $arrayElemAt: ["$training", 0] },
                        status: 1,
                        companyId: 1,
                        userId: 1,
                        assigned_date: 1,
                        due_date: 1,
                        start_date: 1,
                        createdAt: 1,
                        updatedAt: 1,
                        completion_date: 1,
                        result: 1,
                        score: 1
                    }
                }

                let match_stage = {
                    $match: query
                }

                let sort_stage = {
                    $sort: sort_obj
                }

                let skip_stage = {
                    $skip: (page_no - 1) * per_page
                }

                let limit_stage = {
                    $limit: per_page
                }

                Promise.all([
                    AssignedTrainingsModel.aggregate([
                        lookup_stage,
                        project_stage,
                        match_stage,
                        sort_stage,
                        skip_stage,
                        limit_stage
                    ]),
                    AssignedTrainingsModel.aggregate([
                        lookup_stage,
                        project_stage,
                        match_stage,
                        {
                            $count: "total_count"
                        }
                    ])
                ])
                .then(([training_history, total_count_array]) => {
                    console.log(total_count_array)
                    res.status(200).json({
                        status: true,
                        message: "Successfully fetched training history",
                        total_count: total_count_array[0] ? total_count_array[0].total_count : 0,
                        data: training_history
                    })
                })
                .catch(err => {
                    res.status(400).json({
                        status: false,
                        message: "Error while fetching training history",
                        error: err
                    })
                })

            }
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

/**
*@author Asma
*@date 17/05/2019 07:33
API to change a training's status to complete
*/
let markAsComplete = (req, res) => {
    let body = req.body
    let completion_date = body.completion_date
    let userId = body.userId
    let companyId = body.companyId
    let _id = body._id
    return new Promise((resolve, reject) => {
        if (!completion_date) {
            reject('completion_date is required')
        }
        else if (!userId) {
            reject("userId is required")
        }
        else if (!companyId) {
            reject("companyId is required")
        }
        else if (!_id) {
            reject("_id is required")
        }
        else {
            resolve(null)
        }
    })
        .then(() => {
            let assigned_training_filter = {
                "_id": _id,
                "userId": userId,
                "companyId": companyId,
                "status": "In Progress"
            }
            AssignedTrainingsModel.findOneAndUpdate(assigned_training_filter,
                {
                    $set: {
                        "completion_date": completion_date,
                        "status": "Completed"
                    }
                },
                {
                    new: true
                })
                .then(response => {
                    console.log(response)
                    if (response) {
                        res.status(200).json({
                            status: true,
                            message: "updated assigned training successfully",
                            data: response
                        })
                    }
                    else {
                        res.status(200).json({
                            status: true,
                            message: "Training is already marked as Complete",
                            // data: response
                        })
                    }

                })
                .catch(err => {
                    console.log(err)
                    res.status(400).json({
                        status: false,
                        message: "Error while updating assigned training",
                        error: err
                    });
                });
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            });
        })
}

/**
*@author Asma
*@date 17/05/2019 07:33
API to get details of one training
*/
let getAssignedTraining = (req, res) => {
    let userId = req.params.user_id
    let companyId = req.params.company_id
    let _id = req.params._id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
        .then(() => {
            let find_filter = {
                _id: _id,
                companyId: companyId,
                userId: userId
            }
            AssignedTrainingsModel.findOne(find_filter).populate("training").lean().exec()
                .then(response => {
                    if (response) {
                        res.status(200).json({
                            status: true,
                            message: "Successfully fetched assigned training",
                            data: response
                        })
                    }
                    else {
                        res.status(400).json({
                            status: true,
                            message: "Assigned training not found",
                            data: null
                        })
                    }
                })
                .catch(err => {
                    res.status(400).json({
                        status: true,
                        message: "Error while fetching assigned training",
                        error: err
                    })
                });
        })
        .catch(err => {
            res.status(400).json({
                status: true,
                message: err
            })
        });
}

/**
*@author Asma
*@date 17/05/2019 07:34
API to edit an assigned training
*/
let editAssignedTraining = (req, res) => {
    let body = req.body
    let userId = body.userId
    let companyId = body.companyId
    let _id = body._id
    return new Promise((resolve, reject) => {
        if (!userId) {
            reject("userId is required")
        }
        else if (!companyId) {
            reject("companyId is required")
        }
        else if (!_id) {
            reject("_id is required")
        }
        else {
            resolve(null)
        }
    })
        .then(() => {
            delete body.userId
            delete body.companyId
            delete body._id
            delete body.training
            let update_filter = {
                "_id": _id,
                "userId": userId,
                "companyId": companyId
            }
            AssignedTrainingsModel.findOneAndUpdate(
                update_filter,
                {
                    $set: body
                },
                {
                    new: true
                })
                .then(response => {
                    res.status(200).json({
                        status: true,
                        message: "updated assigned training successfully",
                        data: response
                    })
                })
                .catch(err => {
                    res.status(400).json({
                        status: false,
                        message: "Error while updating assigned training",
                        error: err
                    })
                });
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            });
        })
}

/**
*@author Asma
*@date 17/05/2019 07:34
API to delete one or more assigned trainings of a user
*/
let deleteMany = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!userId) {
            reject("userId is required")
        }
        else if (!companyId) {
            reject("companyId is required")
        }
        else {
            resolve(null);
        }

    })
        .then(() => {
            AssignedTrainingsModel.deleteMany({
                companyId: companyId,
                userId: userId,
                _id: { $in: body._ids }
            })
                .then(response => {
                    res.status(200).json({
                        status: true,
                        message: "Deleted successfully",
                        data: response
                    })
                })
                .catch(err => {
                    res.status(400).json({
                        status: false,
                        message: "Error while deleting assigned trainings",
                        error: err
                    })
                })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting assigned trainings",
                error: err
            })
        })
}

module.exports = {
    // list with status filter, pagination , search with name , status, most recent - 1 month, 
    // view one training
    // edit one training
    // deletemany
    // mark as complete

    listUserTrainings,
    markAsComplete,
    deleteMany,
    getAssignedTraining,
    editAssignedTraining
}