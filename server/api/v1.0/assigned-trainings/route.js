let express = require('express');

let router = express.Router();

let authMiddleware = require('../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.post(
    "/listTrainings",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.listUserTrainings
);

router.post(
    "/mark-as-complete",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.markAsComplete
);

router.post(
    "/deleteMany",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deleteMany
);

router.get(
    "/get/:company_id/:user_id/:_id",
    authMiddleware.isUserLogin,
    Controller.getAssignedTraining
);

router.put(
    "/edit",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editAssignedTraining
);

module.exports = router;