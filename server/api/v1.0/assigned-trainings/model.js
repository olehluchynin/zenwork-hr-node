let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');

// mongoose.set('debug', true);

let Schema = new mongoose.Schema({
    training: {
        type:ObjectID,
        ref:'training'
    },
    companyId: {
        type:ObjectID,
        ref:'companies'
    },
    userId: {
        type: ObjectID,
        ref:'users'
    },
    assigned_date: {
        type: Date
    },
    due_date: {
        type: Date
    },
    start_date: {
        type: Date
    },
    completion_date: {
        type: Date
    },
    status: {
        type: String, 
        enum: ['In Progress', 'Completed'], 
        default: "In Progress"
    },
    score: {
        type: Number
    },
    result: {
        type: String,
        enum: ['Pass', 'Fail', 'N/A']
    }
},{
    timestamps: true,
    versionKey: false
});

Schema.index({ training: 1, companyId: 1, userId: 1 }, {unique: true});

Schema.index({ companyId: 1, userId: 1 });

let AssignedTrainingsModel = mongoose.model('assigned_trainings',Schema);

module.exports = AssignedTrainingsModel;
