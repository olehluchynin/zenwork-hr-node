/**
 * @api {post} /assigned-trainings/mark-as-complete MarkAsComplete
 * @apiName MarkAsComplete
 * @apiGroup AssignedTrainings
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cd93e4a8e9dea1f4bb987dc",
	"_id": "5cdc1a9a6c7a7817c4c2e4fd",
	"completion_date": "2019-05-16T06:05:01.506Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "updated assigned training successfully",
    "data": {
        "status": "Completed",
        "_id": "5cdc1a9a6c7a7817c4c2e4fd",
        "training": "5cdb96d78ed19a301ececd1b",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd93e4a8e9dea1f4bb987dc",
        "assigned_date": "2019-05-15T13:56:42.039Z",
        "due_date": "2019-05-16T13:56:42.038Z",
        "start_date": "2019-05-15T13:56:42.039Z",
        "createdAt": "2019-05-15T13:56:42.064Z",
        "updatedAt": "2019-05-16T06:05:51.488Z",
        "completion_date": "2019-05-16T06:05:01.506Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /assigned-trainings/deleteMany DeleteAssignedTrainings
 * @apiName DeleteAssignedTrainings
 * @apiGroup AssignedTrainings
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "userId": "5cd92313f308c21c43e50599",
    "_ids": [
    	"5cdc1a9a6c7a7817c4c2e4fe"
    	]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /assigned-trainings/get/:company_id/:user_id/:_id GetAssignedTraining
 * @apiName GetAssignedTraining
 * @apiGroup AssignedTrainings
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} user_id Users unique _id
 * @apiParam {ObjectId} _id Assigned Training unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched assigned training",
    "data": {
        "_id": "5cdd096ba7685a3da83d455c",
        "status": "In Progress",
        "training": {
            "_id": "5cd2d22d4e614c71661abd65",
            "status": 0,
            "trainingName": "Test",
            "trainingId": "12345",
            "trainingSubject": "New one",
            "trainingType": "Instructor Led",
            "timeToComplete": "1 Day",
            "trainingUrl": "sdfdsfsdfs",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5c9e11894b1d726375b23057",
            "updatedBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-05-08T12:57:17.484Z",
            "updatedAt": "2019-05-08T12:57:17.484Z",
            "trainingSerialNumber": 22
        },
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd92313f308c21c43e50599",
        "assigned_date": "2019-05-16T06:55:39.634Z",
        "due_date": "2019-05-17T06:55:39.634Z",
        "start_date": "2019-05-16T06:55:39.634Z",
        "createdAt": "2019-05-16T06:55:39.640Z",
        "updatedAt": "2019-05-16T06:55:39.640Z"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /assigned-trainings/edit EditAssignedTraining
 * @apiName EditAssignedTraining
 * @apiGroup AssignedTrainings
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "userId": "5cd92313f308c21c43e50599",
    "_id": "5cdd096ba7685a3da83d455c",
    "start_date": "2019-05-16T06:58:39.634Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "updated assigned training successfully",
    "data": {
        "status": "In Progress",
        "_id": "5cdd096ba7685a3da83d455c",
        "training": "5cd2d22d4e614c71661abd65",
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cd92313f308c21c43e50599",
        "assigned_date": "2019-05-16T06:55:39.634Z",
        "due_date": "2019-05-17T06:55:39.634Z",
        "start_date": "2019-05-16T06:58:39.634Z",
        "createdAt": "2019-05-16T06:55:39.640Z",
        "updatedAt": "2019-05-16T09:01:24.556Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /assigned-trainings/listTrainings ListAssignedTrainings
 * @apiName ListAssignedTrainings
 * @apiGroup AssignedTrainings
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example1:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cd93e4a8e9dea1f4bb987dc",
	"onlyActive": 1
}
 * @apiParamExample {json} Request-Example2:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"userId": "5cd93e4a8e9dea1f4bb987dc",
	"onlyActive": 0,
	"per_page": 5,
	"page_no": 1,
	"search_query": "Node"
}
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched training history",
    "total_count": 1,
    "data": [
        {
            "_id": "5cdc1a9a6c7a7817c4c2e4fd",
            "status": "Completed",
            "companyId": "5c9e11894b1d726375b23058",
            "userId": "5cd93e4a8e9dea1f4bb987dc",
            "assigned_date": "2019-05-15T13:56:42.039Z",
            "due_date": "2019-05-16T13:56:42.038Z",
            "start_date": "2019-05-15T13:56:42.039Z",
            "createdAt": "2019-05-15T13:56:42.064Z",
            "updatedAt": "2019-05-17T09:19:23.945Z",
            "completion_date": "2019-05-16T06:05:01.506Z",
            "training": {
                "_id": "5cdb96d78ed19a301ececd1b",
                "status": 0,
                "trainingName": "NodeJS",
                "trainingId": "345647690476",
                "trainingSubject": "Professional",
                "trainingType": "Instructor Led",
                "timeToComplete": "1 Day",
                "trainingUrl": "sdnfpihreinvk f",
                "companyId": "5c9e11894b1d726375b23058",
                "createdBy": "5c9e11894b1d726375b23057",
                "updatedBy": "5c9e11894b1d726375b23057",
                "createdAt": "2019-05-15T04:34:31.535Z",
                "updatedAt": "2019-05-15T04:34:31.535Z",
                "trainingSerialNumber": 24
            }
        }
    ]
}
 * @apiSuccessExample Success-Response2:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched training history",
    "total_count": 2,
    "data": [
        {
            "_id": "5cdd098da7685a3da83d455d",
            "status": "Completed",
            "training": {
                "_id": "5cd2c6d94e614c71661abd64",
                "status": 0,
                "trainingName": "Istqb Training Classes",
                "trainingId": "12345",
                "trainingSubject": "New one",
                "trainingType": "Instructor Led",
                "timeToComplete": "1 Day",
                "trainingUrl": "sadfsasd",
                "companyId": "5c9e11894b1d726375b23058",
                "createdBy": "5c9e11894b1d726375b23057",
                "updatedBy": "5c9e11894b1d726375b23057",
                "createdAt": "2019-05-08T12:08:57.749Z",
                "updatedAt": "2019-05-08T12:26:29.093Z",
                "trainingSerialNumber": 21
            },
            "companyId": "5c9e11894b1d726375b23058",
            "userId": "5cd93e4a8e9dea1f4bb987dc",
            "assigned_date": "2019-05-16T06:56:13.415Z",
            "due_date": "2019-05-17T06:56:13.415Z",
            "start_date": "2019-05-16T06:56:13.415Z",
            "createdAt": "2019-05-16T06:56:13.416Z",
            "updatedAt": "2019-05-16T06:58:29.802Z",
            "completion_date": "2019-05-16T06:05:01.506Z"
        },
        {
            "_id": "5cdc1a9a6c7a7817c4c2e4fd",
            "status": "Completed",
            "training": {
                "_id": "5cdb96d78ed19a301ececd1b",
                "status": 0,
                "trainingName": "NodeJS",
                "trainingId": "345647690476",
                "trainingSubject": "Professional",
                "trainingType": "Instructor Led",
                "timeToComplete": "1 Day",
                "trainingUrl": "sdnfpihreinvk f",
                "companyId": "5c9e11894b1d726375b23058",
                "createdBy": "5c9e11894b1d726375b23057",
                "updatedBy": "5c9e11894b1d726375b23057",
                "createdAt": "2019-05-15T04:34:31.535Z",
                "updatedAt": "2019-05-15T04:34:31.535Z",
                "trainingSerialNumber": 24
            },
            "companyId": "5c9e11894b1d726375b23058",
            "userId": "5cd93e4a8e9dea1f4bb987dc",
            "assigned_date": "2019-05-15T13:56:42.039Z",
            "due_date": "2019-05-16T13:56:42.038Z",
            "start_date": "2019-05-15T13:56:42.039Z",
            "createdAt": "2019-05-15T13:56:42.064Z",
            "updatedAt": "2019-05-16T06:05:51.488Z",
            "completion_date": "2019-05-16T06:05:01.506Z"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */