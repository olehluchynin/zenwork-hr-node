let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let PackagesController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');


router.post('/add',
authMiddleware.isUserLogin,validationsMiddleware.reqiuresBody,PackagesController.addNew);

router.post('/list',/*authMiddleware.isUserLogin,*/PackagesController.list);
router.get('/all',/*authMiddleware.isUserLogin,*/PackagesController.getAll);

router.put('/:_id',authMiddleware.isUserLogin,authMiddleware.isZenworkerLogin,validationsMiddleware.reqiuresBody,
// authMiddleware.isSrSupport,
authMiddleware.isSrSupportOrZenworkerOrAdministrator,
PackagesController.udpateById);

router.get('/:_id',authMiddleware.isUserLogin,
// authMiddleware.isSrSupport,
authMiddleware.isSrSupportOrZenworkerOrAdministrator,
PackagesController.getById);

router.post(
    '/delete',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    PackagesController.deletePackages
);

module.exports = router;
