let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    name:{type:String,unique:true,required:true},
    price:Number,//In USDs
    subscriptionType : {type:String},
    startDate : String,
    endDate : String,
    modules : {},
    costPerEmployee : Number,
    status:{type:String,enum:['active','inactive'],default:'active'},
    createdBy:{type:ObjectID,ref:'zenworkers'},
    updatedBy:{type:ObjectID,ref:'zenworkers'},
    createdDate:{type:Date, default:Date.now()},
    updatedDate:{type:Date,default:Date.now()},
});

// Schema.index({loc: '2dsphere'}); //loc: { type: "Point", coordinates: [ longitude, latitude ] },

Schema.plugin(autoIncrement.plugin, {model:'packages', field:'packageId', startAt: 1, incrementBy: 1});
let PackagesModel = mongoose.model('packages',Schema);

module.exports = PackagesModel;
