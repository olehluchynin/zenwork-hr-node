/**
 * @apiDefine DataNotFoundError
 *
 * @apiError DataNotFound The data the you are requested was not found.
 *
 * @apiErrorExample Data Not Found Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "message": "Data Not Found"
 *       "data": null 
 *     }
 */


 /**
 * @apiDefine InternalServerError
 *
 * @apiError InternalServerError Internal Server error.
 *
 * @apiErrorExample Internal Server Error:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "status": false,
 *       "message": "Internal server error"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine TokenNotFound
 *
 * @apiError TokenNotFound User Token Not Found.
 *
 * @apiErrorExample Token Not Found:
 *     HTTP/1.1 400 Token Not Found
 *     {
 *       "status": false,
 *       "message": "Token Not Found"
 *       "data": null 
 *     }
 */

  /**
 * @apiDefine BadRequest
 *
 * @apiError BadRequest Bad request.
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": false,
 *       "message": "Invalid key"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine AccessDeneid
 *
 * @apiError AccessDenied Access denied for this operation.
 *
 * @apiErrorExample Access Deneid:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "status": false,
 *       "message": "Access Denied for this operation"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine DataAlreadyExists
 *
 * @apiError DataConflict Date already exists.
 *
 * @apiErrorExample Data Conflict Error:
 *     HTTP/1.1 402 Data Conflict
 *     {
 *       "status": false,
 *       "message": "Data already exist"
 *       "data": null 
 *     }
 */



  /**
 * @api {post} /packages/add Add new
 * @apiName AddNewPackages
 * @apiGroup Packages
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
 *
 {
    "name":"Premium",
    "price":2000
 }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "New package added successfully",
    "data": {
        "status": "active",
        "createdDate": "2019-01-02T09:11:10.383Z",
        "updatedDate": "2019-01-02T09:11:10.383Z",
        "_id": "5c2c80593112f91c21db2077",
        "name": "Premius",
        "price": 2000,
        "createdBy": "5c126409d84721019cf167be",
        "__v": 0
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */



   /**
 * @api {put} /packages/:_id Update package
 * @apiName UpdatePackage
 * @apiGroup Packages
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
 *
 {
    "name":"Premium",
    "price":2000
 }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Package updated",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */



 /**
 * @api {get} /packages/:_id Get Package Info
 * @apiName GetPackageData
 * @apiGroup Packages
  * @apiHeader {String} x-access-token User login token.

 * @apiParam {String} _id package unique _id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Package details fetched successfully",
   "data": {
        "status": "active",
        "createdDate": "2019-01-02T09:11:10.383Z",
        "updatedDate": "2019-01-02T09:11:10.383Z",
        "_id": "5c2c80593112f91c21db2077",
        "name": "Premius",
        "price": 2000,
        "createdBy": "5c126409d84721019cf167be",
        "__v": 0
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

  /**
 * @api {post} /packages/list ListPackages
 * @apiName ListPackages
 * @apiGroup Packages
  * @apiHeader {String} x-access-token User login token.
 * @apiParamExample {json} Request-Example:
{
    "per_page": 10,
    "page_no": 1
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Data Fetched",
    "data": [
        {
            "status": "active",
            "createdDate": "2019-01-02T09:11:10.383Z",
            "updatedDate": "2019-01-02T09:11:10.383Z",
            "_id": "5c2c804e3112f91c21db2076",
            "name": "Basic",
            "price": 1000,
            "createdBy": "5c126409d84721019cf167be",
            "__v": 0
        },
        {
            "status": "active",
            "createdDate": "2019-01-02T09:11:10.383Z",
            "updatedDate": "2019-01-02T09:11:10.383Z",
            "_id": "5c2c80593112f91c21db2077",
            "name": "Premius",
            "price": 2000,
            "createdBy": "5c126409d84721019cf167be",
            "__v": 0
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

  /**
 * @api {get} /packages/all?status=active Get All packages
 * @apiName GetAllPackagesData
 * @apiGroup Packages
  * @apiHeader {String} x-access-token User login token.
  * 
  * @apiParam {String} status package status (can be empty or active, inactive)

 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Data Fetched",
    "data": [
        {
            "status": "active",
            "createdDate": "2019-01-02T09:11:10.383Z",
            "updatedDate": "2019-01-02T09:11:10.383Z",
            "_id": "5c2c804e3112f91c21db2076",
            "name": "Basic",
            "price": 1000,
            "createdBy": "5c126409d84721019cf167be",
            "__v": 0
        },
        {
            "status": "active",
            "createdDate": "2019-01-02T09:11:10.383Z",
            "updatedDate": "2019-01-02T09:11:10.383Z",
            "_id": "5c2c80593112f91c21db2077",
            "name": "Premius",
            "price": 2000,
            "createdBy": "5c126409d84721019cf167be",
            "__v": 0
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /packages/delete DeletePackages
 * @apiName DeletePackages
 * @apiGroup Packages
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"_ids": [
		"5ca26ce12bdee465121c8136"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted packages successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */