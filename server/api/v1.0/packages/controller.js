let PackagesModel = require('./model');

let addNew = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        if (!body.name) reject("Invalid name");
        else if (!body.price) reject("Invalid price");
        else resolve(null);
    }).then(()=>{
        return PackagesModel.findOne({name:body.name})
    }).then((package) => {
        if(package){
            res.status(401).json({status:false,message:"Package already exists",data:null});
            return;
        }else{
            body.createdBy = req.jwt.zenworkerId;
            return PackagesModel.create(body);
        }
    }).then((package)=>{
        if(package){
            res.status(200).json({status:true,message:"New package added successfully",data:package});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
};


let getById = (req, res) => {

    let packageId = req.params._id;

    return new Promise((resolve, reject) => {
        if (!packageId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return PackagesModel.findOne({_id:packageId});
    }).then(package=>{
        if(!package){
            res.status(400).json({ status: false, message: "Data not found", data: null });
            return null;
        }else{
            res.status(200).json({status:true,message:"User details fetched successfully",data:package});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}


let udpateById = (req, res) => {

    let body = req.body;

    let pacakgeId = req.params._id;

    return new Promise((resolve, reject) => {
        if (!pacakgeId) reject("Invalid pacakgeId");
        else resolve(null);
    }).then(() => {
        return PackagesModel.findOne({_id:pacakgeId});
    }).then(package=>{
        if(!package){
            res.status(400).json({ status: false, message: "Data not found", data: null });
            return null;
        }else{
            body.udpateById  = req.jwt.zenworkerId;
            body.updatedDate = new Date();
            return PackagesModel.update({_id:package},body); 
        }
    }).then(updated=>{
        if(updated){
            res.status(200).json({status:true,message:"Updated successfully",data:null});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

let list = (req,res)=>{
    let body = req.body
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let per_page = body.per_page ? body.per_page : 10
        let page_no = body.page_no ? body.page_no : 1
        let sort = {
            updatedAt : -1
        }
        return  Promise.all([
            PackagesModel.find({}).sort(sort).skip((page_no - 1) * per_page).limit(per_page).lean().exec(),
            PackagesModel.countDocuments({})
            ])
    }).then(([pacakges, count])=>{
        res.status(200).json({ status: true, message: "Data Fetched", data: pacakges, count: count});
    }).catch((err) => {
        console.log(err);
        if(typeof err =='object'){
            res.status(500).json({ status: false, message: "Internal server error", data: null });
        }else{
            res.status(400).json({ status: false, message: err, data: null });
        }
    });

}

let getAll = (req,res)=>{
    let queryParams = req.query
    console.log(queryParams)
    let status = queryParams.status
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let find_query = {}
        
        find_query['status'] = 'active'
        
        return  PackagesModel.find(find_query);
    }).then(pacakges=>{
        res.status(200).json({ status: true, message: "Data Fetched", data: pacakges});
    }).catch((err) => {
        console.log(err);
        if(typeof err =='object'){
            res.status(500).json({ status: false, message: "Internal server error", data: null });
        }else{
            res.status(400).json({ status: false, message: err, data: null });
        }
    });

}

let deletePackages = (req, res) => {
    let body = req.body
    return new Promise((resolve, reject) => {
        if(!body._ids) reject('_ids are required')
        resolve(null);
    })
    .then(() => {
        PackagesModel.deleteMany({
            _id: {$in : body._ids}
        })
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted packages successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting packages",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while deleting packages",
            error: err
        })
    })
}


module.exports = {
    addNew,
    getById,
    udpateById,
    getAll,
    list,
    deletePackages
}
