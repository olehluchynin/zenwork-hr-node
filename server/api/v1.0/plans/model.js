let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    clientId:{type:ObjectID,ref:'companies'},
    type:{type:String,enum:['package','ad-on']},
    package:{type:ObjectID,ref:'packages'},
    adon:{type:ObjectID,ref:'ad_ons'},
    startDate:Date,
    endDate:Date,
    status:{type:String,enum:['active','inactive'],default:'active'},
    orderId:{type:ObjectID,ref:'orders'},
    createdBy:{type:ObjectID,ref:'zenworkers'},
    updatedBy:{type:ObjectID,ref:'zenworkers'},
    createdDate:{type:Date, default:Date.now()},
    updatedDate:{type:Date,default:Date.now()},
});

// Schema.index({loc: '2dsphere'}); //loc: { type: "Point", coordinates: [ longitude, latitude ] },

Schema.plugin(autoIncrement.plugin, {model:'plans', field:'planId', startAt: 1, incrementBy: 1});
let PlansModel = mongoose.model('plans',Schema);

module.exports = PlansModel;
