let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let PlansController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');


router.get('/all',authMiddleware.isUserLogin,PlansController.getAll);

router.put('/:_id',authMiddleware.isUserLogin,authMiddleware.isZenworkerLogin,validationsMiddleware.reqiuresBody,
// authMiddleware.isSrSupport,
authMiddleware.isSrSupportOrZenworkerOrAdministrator,
PlansController.udpateById);

router.get('/:_id',authMiddleware.isUserLogin,
// authMiddleware.isSrSupport,
authMiddleware.isSrSupportOrZenworkerOrAdministrator,
PlansController.getById);


module.exports = router;