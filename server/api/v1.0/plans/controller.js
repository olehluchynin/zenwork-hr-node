let PlansModel = require('./model');



let getById = (req, res) => {

    let planId = req.params._id;

    return new Promise((resolve, reject) => {
        if (!planId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return PlansModel.findOne({_id:planId});
    }).then(plan=>{
        if(!plan){
            res.status(400).json({ status: false, message: "Data not found", data: null });
            return null;
        }else{
            res.status(200).json({status:true,message:"User details fetched successfully",data:plan});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}


let udpateById = (req, res) => {

    let body = req.body;

    let planId = req.params._id;

    return new Promise((resolve, reject) => {
        if (!planId) reject("Invalid pacakgeId");
        else resolve(null);
    }).then(() => {
        return PlansModel.findOne({_id:planId});
    }).then(plan=>{
        if(!plan){
            res.status(400).json({ status: false, message: "Data not found", data: null });
            return null;
        }else{
            body.udpateById  = req.jwt.zenworkerId;
            body.updatedDate = new Date();
            return PlansModel.update({_id:plan},body); 
        }
    }).then(updated=>{
        if(updated){
            res.status(200).json({status:true,message:"Updated successfully",data:null});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

let getAll = (req,res)=>{
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return  PlansModel.find({});
    }).then(plans=>{
        res.status(200).json({ status: true, message: "Data Fetched", data: plans});
    }).catch((err) => {
        console.log(err);
        if(typeof err =='object'){
            res.status(500).json({ status: false, message: "Internal server error", data: null });
        }else{
            res.status(400).json({ status: false, message: err, data: null });
        }
    });

}


module.exports = {
    getById,
    udpateById,
    getAll
}
