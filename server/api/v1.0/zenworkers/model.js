let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('../../../common/constants');

let Schema = new mongoose.Schema({
    userId:{type:ObjectID,ref:'users'},
    name:String,
    email:{type:String,unique:true,required:true},
    // type:{type:String,enum:Object.keys(constants.zenworkRoles)},
    address:{
        address1:String,
        address2:String,
        city:String,
        zipcode:String,
        state:String
    },
    // loc :  { type: {type:String}, coordinates: [Number]},
    companyId: {type:ObjectID,ref:'companies'},
    isAdmin: {type: Boolean, default: false},
    roleId: {type:ObjectID,ref:'roles'},
    status:{type:String,enum:['active','inactive'],default:'active'},
    createdBy:{type:ObjectID,ref:'zenworkers'},
    updatedBy:{type:ObjectID,ref:'zenworkers'},
    createdDate:{type:Date, default:Date.now()},
    updatedDate:{type:Date,default:Date.now()},
});
Schema.plugin(autoIncrement.plugin, {model:'zenworkers', field:'zenworkerId', startAt: 1, incrementBy: 1});

// Schema.index({loc: '2dsphere'}); //loc: { type: "Point", coordinates: [ longitude, latitude ] },

let ZenworkersModel = mongoose.model('zenworkers',Schema);

module.exports = ZenworkersModel;
