let mongoose = require('mongoose');
let ZenworkersModel = require('./model');
let UserController = require('../users/controller');
let UsersModel = require('../users/model');

let jwt = require('jsonwebtoken');
let config = require('../../../config/config');

let utils = require('../../../common/utils');
let helpers = require('../../../common/helpers');
let constants = require('../../../common/constants');

let mailer = require('../../../common/mailer');

let addNew = (req, res) => {
    let body = req.body;
    // console.log(body);
    if(body.companyId) {
        body.isAdmin = true;
        body.type = 'administrator';
    } else {
        body.isAdmin = false;
        body.type = 'zenworker';
    }
    
    return new Promise((resolve, reject) => {
        if (!body.name) reject("Invalid name");
        else if (!body.email || !helpers.isValidEmail(body.email)) reject("Invalid email");
        // else if (!body.type) reject('Invalid type');
        else resolve(null);
    }).then(()=>{
        return UsersModel.findOne({email:body.email})
    }).then((user) => {
        if(user){
            res.status(401).json({status:false,message:"User already exists",data:null});
            return;
        }else{
            body.createdBy = req.jwt.zenworkerId;
            body.password = utils.encryptPassword(utils.generateRandomPassword());
            let userBody = body;
            delete userBody.userId;
            return Promise.all([UserController.addNewUser(userBody),ZenworkersModel.create(body)]);
        }
    }).then(([user,zenworker]) => {
        if(user && zenworker){
            user.zenworkerId = zenworker._id;
            zenworker.userId = user._id;
            zenworker.save();
            user.save();
        }
        if(zenworker){
            mailer.sendUsernamePasswordToZenworker(zenworker.email,zenworker.name,zenworker.email,body.password,'Administrator'/*constants.zenworkRoles[zenworker.type]*/);
            zenworker = utils.formatZenworkerData(zenworker.toJSON());
            res.status(200).json({status:true,message:"New zenworker added successfully",data:zenworker});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
};

let login = (req, res) => {
    let body = req.body;

    return new Promise((resolve, reject) => {
        if (!body.email || !helpers.isValidEmail(body.email)) reject("Invalid email");
        else if (!body.password) reject("Invalid passoword");
        else resolve(null);
    }).then(() => {
        return ZenworkersModel.findOne({email:body.email});
    }).then(zenworker=>{
        if(!zenworker){
            res.status(400).json({ status: false, message: "User not found", data: null });
        }else{
            if(body.password == utils.decryptPassword(zenworker.password)){
                zenworker = utils.formatZenworkerData(zenworker.toJSON());
                zenworker.token = utils.generateJWTForUser(zenworker);
                res.status(200).json({status:true,message:"Logged successfully",data:zenworker});
            }else{
                res.status(200).json({status:false,message:"Invalid password",data:null});
            }
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

let getById = (req, res) => {

    let userId = req.params._id;

    return new Promise((resolve, reject) => {
        if (!userId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return ZenworkersModel.findOne({_id:userId});
    }).then(zenworker=>{
        if(!zenworker){
            res.status(400).json({ status: false, message: "User not found", data: null });
            return null;
        }else{
            zenworker = utils.formatZenworkerData(zenworker.toJSON());
            res.status(200).json({status:true,message:"User details fetched successfully",data:zenworker});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

let getSelfData = (req, res) => {

    let userId = req.jwt.zenworkerId;

    return new Promise((resolve, reject) => {
        if (!userId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return ZenworkersModel.findOne({_id:userId});
    }).then(zenworker=>{
        if(!zenworker){
            res.status(400).json({ status: false, message: "User not found", data: null });
            return null;
        }else{
            zenworker = utils.formatZenworkerData(zenworker.toJSON());
            res.status(200).json({status:true,message:"User details fetched successfully",data:zenworker});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}


let updateSelfData = (req, res) => {

    let body = req.body;

    let userId = req.jwt.zenworkerId;

    return new Promise((resolve, reject) => {
        if (!userId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return ZenworkersModel.findOne({_id:userId});
    }).then(zenworker=>{
        if(!zenworker){
            res.status(400).json({ status: false, message: "User not found", data: null });
            return null;
        }else{
            if(body.password)delete body.password;
            body.udpateById  = req.jwt.zenworkerId;
            body.updatedDate = new Date();
            return ZenworkersModel.update({_id:userId},body); 
        }
    }).then(updated=>{
        if(updated){
            res.status(200).json({status:true,message:"Updated successfully",data:null});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

let udpateById = (req, res) => {

    let body = req.body;

    let userId = req.params._id;

    return new Promise((resolve, reject) => {
        if (!userId) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return ZenworkersModel.findOne({_id:userId});
    }).then(zenworker=>{
        if(!zenworker){
            res.status(400).json({ status: false, message: "User not found", data: null });
            return null;
        }else{
            if(body.password)delete body.password;
            body.udpateById  = req.jwt.zenworkerId;
            body.updatedDate = new Date();
            return ZenworkersModel.update({_id:userId},body); 
        }
    }).then(updated=>{
        if(updated){
            res.status(200).json({status:true,message:"Updated successfully",data:null});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}

let getAll = (req,res)=>{

    let queryParams = req.query;
    var searchQuery = req.query.searchQuery || '';
    var status = req.query.status;
    var role = req.query.role;
    var companyId = req.query.companyId;
    var type = '';

    // if (req.query.hasOwnProperty('role')){
    //     type = req.query.role
    // }


    return new Promise((resolve, reject) => {
        // if (!body.name || !body.name.first || !body.name.last) reject("invalid name field");
        // else if (!body.email || !helpers.isValidEmail(body.email)) reject("invalid email");
        // else if (!body.password || !helpers.isValidPasword(body.password)) reject("invalid password");
        // else if(!body.mobile || !helpers.isValidPhoneNumber(body.mobile.number))reject("invalid Phone number");
        // else if (body.role === "admin") reject('User type admin cannot be allowed');
        // else
        resolve(null);
    }).then(() => {
        let options = {};
        if(queryParams && queryParams.requiredFields){
            options = JSON.parse(queryParams.requiredFields);
            options.createdDate = 1;
            options.updatedDate = 1;
        }

        let sortOptions = {};
        if(queryParams && queryParams.sort){
            sortOptions = JSON.parse(queryParams.sort);
        }

        let perPage = queryParams.perPage ? parseInt(queryParams.perPage):20;
        let pageNumber = queryParams.pageNumber ? parseInt(queryParams.pageNumber):1;

        let findQuery ;

        if(searchQuery){
            if(!findQuery)findQuery = {};
            findQuery['$or']= [{name:new RegExp(searchQuery,'i')},{email:new RegExp(searchQuery,'i')}];
        }

        if(status){
            if(!findQuery)findQuery = {};
            findQuery['status']= status;
        }

        // if(type){
        //     if(!findQuery)findQuery = {};
        //     findQuery['type']= type;
        // }

        if(role) {
            if(!findQuery) findQuery = {};
            findQuery['roleId']= mongoose.Types.ObjectId(role);
        }

        if(companyId){
            if(!findQuery)findQuery = {};
            findQuery['companyId']= mongoose.Types.ObjectId(companyId);
        } else {
            if(!findQuery)findQuery = {};
            findQuery['isAdmin']= false;
        }
        
        console.log("findQuery",findQuery);
        console.log("sortOptions",sortOptions);
        console.log("options",options);

        var aggregatePipes = [];
        if(findQuery){
            aggregatePipes.push(
                {
                    $match:findQuery
                }
            );
        }

        var aggregatePipeForCount = [];

        aggregatePipes.forEach(pipe=>{
            aggregatePipeForCount.push(pipe);
        });
        aggregatePipeForCount.push(
            {
                $group:{
                    _id:null,
                    count:{$sum:1}
                }
            }
        )

        aggregatePipes.push(
            {
                $lookup: {
                    from: "roles",
                    localField: "roleId",
                    foreignField: "_id",
                    as: "roleId"
                }
            },
            {
                $unwind: "$roleId"
            },
            {
                $project: {
                    roleId:{
                        "createdBy": 0,
                        "updatedBy": 0,
                        "createdAt": 0,
                        "updatedAt": 0,
                        "roleId": 0
                    }
                }
            },
            {
                $skip:perPage*(pageNumber-1)
            }
        );
        aggregatePipes.push(
            {
                $limit:perPage
            },
        );
        // console.log('requiredFields',options);
        // options.roleId.name=1;
        if(queryParams.requiredFields){
            aggregatePipes.push(
                {
                    $project:options
                },
            );
        }
        console.log(aggregatePipes)
        return  Promise.all([
                ZenworkersModel.aggregate(aggregatePipes),
                ZenworkersModel.aggregate(aggregatePipeForCount),
        ]);
    }).then(([zenworkers,countJSON])=>{
        var count = 0;
        if(countJSON && countJSON.length>0)count=countJSON[0].count;
        res.status(200).json({ status: true, message: "Zenworkers data Fetched", data: zenworkers,count});
    }).catch((err) => {
        console.log(err);
        if(typeof err =='object'){
            res.status(500).json({ status: false, message: "Internal server error", data: null });
        }else{
            res.status(400).json({ status: false, message: err, data: null });
        }
    });

}


let deleteZenworkers = (req, res) => {
    let body = req.body
    return new Promise((resolve, reject)=> {
        resolve(null);
    })
    .then(() => {
        ZenworkersModel.deleteMany({
            _id: {$in : body._ids}
        })
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting zenworkers",
                error: err
            })
        })
    }) 
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}


module.exports = {
    addNew,
    login,
    updateSelfData,
    getSelfData,
    udpateById,
    getById,
    getAll,

    deleteZenworkers
}
