let express = require('express');

let router = express.Router();

let authMiddleware = require('../middlewares/auth');

let ZenworkersController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');


router.post(
    '/add',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    ZenworkersController.addNew
);

router.put('/',authMiddleware.isUserLogin,validationsMiddleware.reqiuresBody,ZenworkersController.updateSelfData);

router.get('/',authMiddleware.isUserLogin,ZenworkersController.getSelfData);

router.get('/all',authMiddleware.isUserLogin,ZenworkersController.getAll);

// router.get('/all/:_id',authMiddleware.isUserLogin,ZenworkersController.getAll);

router.put('/:_id',authMiddleware.isUserLogin,authMiddleware.isZenworkerLogin,validationsMiddleware.reqiuresBody,
// authMiddleware.isSrSupport,
authMiddleware.isSrSupportOrZenworkerOrAdministrator,
ZenworkersController.udpateById);

router.get(
    '/:_id',
    authMiddleware.isUserLogin,
    // authMiddleware.isSrSupport,
    authMiddleware.isSrSupportOrZenworkerOrAdministrator,
    ZenworkersController.getById
);

router.post(
    '/delete',
    authMiddleware.isUserLogin,
    // authMiddleware.isSrSupport,
    validationsMiddleware.reqiuresBody,
    ZenworkersController.deleteZenworkers
);

module.exports = router;
