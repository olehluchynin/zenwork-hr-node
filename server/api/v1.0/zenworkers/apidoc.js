/**
 * @apiDefine DataNotFoundError
 *
 * @apiError DataNotFound The data the you are requested was not found.
 *
 * @apiErrorExample Data Not Found Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "message": "Data Not Found"
 *       "data": null 
 *     }
 */


 /**
 * @apiDefine InternalServerError
 *
 * @apiError InternalServerError Internal Server error.
 *
 * @apiErrorExample Internal Server Error:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "status": false,
 *       "message": "Internal server error"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine TokenNotFound
 *
 * @apiError TokenNotFound User Token Not Found.
 *
 * @apiErrorExample Token Not Found:
 *     HTTP/1.1 400 Token Not Found
 *     {
 *       "status": false,
 *       "message": "Token Not Found"
 *       "data": null 
 *     }
 */

  /**
 * @apiDefine BadRequest
 *
 * @apiError BadRequest Bad request.
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": false,
 *       "message": "Invalid key"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine AccessDeneid
 *
 * @apiError AccessDenied Access denied for this operation.
 *
 * @apiErrorExample Access Deneid:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "status": false,
 *       "message": "Access Denied for this operation"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine DataAlreadyExists
 *
 * @apiError DataConflict Date already exists.
 *
 * @apiErrorExample Data Conflict Error:
 *     HTTP/1.1 402 Data Conflict
 *     {
 *       "status": false,
 *       "message": "Data already exist"
 *       "data": null 
 *     }
 */



 /**
 * @api {post} /zenworkers/add Add new
 * @apiName AddNewZenworker
 * @apiGroup Zerworkers
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
 *{
    "name":"Admin",
    "email":"rajesh.d@gmail.com",
    "role":"billing", // billing,sr-support,support
    "adddress":{
        "address1":"MTWLABS",
        "address2":"MADHAPUR",
        "city":"Hyderabad",
        "zipcode":"500067",
        "state":"TTG"
    },
    "loc":{
    	"type":"Point",
    	"coordinates": [ 1.00000000, 1.00000000000 ]
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "New Zenworker added successfully",
    "data": {
        "name":"Admin",
        "email":"rajesh.d@gmail.com",
        "role":"billing",
        "adddress":{
            "address1":"MTWLABS",
            "address2":"MADHAPUR",
            "city":"Hyderabad",
            "zipcode":"500067",
            "state":"TTG"
        },
        "loc":{
            "type":"Point",
            "coordinates": [ 1.00000000, 1.00000000000 ]
        }
        "createdBy": "5c0e2d985464f22d65ee070b",
        "__v": 0
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */







 /**
 * @api {get} /zenworkers/ Get logged zenworker info
 * @apiName GetLoggedZenworkedData
 * @apiGroup Zerworkers
 * @apiHeader {String} x-access-token User login token.
*
 * @apiParam {String} _id zenworker unique _id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "User details fetched successfully",
    "data": {
        "location": {
            "address1": "MTWLABS",
            "address2": "MADHAPUR",
            "city": "Hyderabad",
            "zipcode": "500067",
            "state": "TTG"
        },
        "status": "inactive",
        "createdDate": "2018-12-10T09:49:26.292Z",
        "updatedDate": "2018-12-10T09:49:26.292Z",
        "_id": "5c0e36ddf3876337626e1651",
        "name": "Rajesh",
        "email": "billing@gmail.com",
        "role": "billing",
        "createdBy": "5c0e2d985464f22d65ee070b",
        "__v": 0
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */



 /**
 * @api {get} /zenworkers/:_id Get Zenworker Info
 * @apiName GetZenworkedData
 * @apiGroup Zerworkers
  * @apiHeader {String} x-access-token User login token.

 * @apiParam {String} _id zenworker unique _id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "User details fetched successfully",
    "data": {
        "location": {
            "address1": "MTWLABS",
            "address2": "MADHAPUR",
            "city": "Hyderabad",
            "zipcode": "500067",
            "state": "TTG"
        },
        "status": "inactive",
        "createdDate": "2018-12-10T09:49:26.292Z",
        "updatedDate": "2018-12-10T09:49:26.292Z",
        "_id": "5c0e36ddf3876337626e1651",
        "name": "Rajesh",
        "email": "billing@gmail.com",
        "role": "billing",
        "createdBy": "5c0e2d985464f22d65ee070b",
        "__v": 0
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */





 /**
 * @api {get} /zenworkers/all Get All
 * @apiName GetAllZenworkersData
 * @apiGroup Zerworkers
 * @apiHeader {String} x-access-token User login token.
 * @apiParam {String} searchQuery(queryParam) search query string.
 * @apiParam {String} role(queryParam) role for filter (billing,sr-support,support).
 * @apiParam {String} status(queryParam) status for filter (active,inactive).
 * @apiParam {String} requiredFields(queryParam) to get required fields only in data
 * @apiParam {Number} perPage(queryParam) records count per page.
 * @apiParam {Number} pageNumber(queryParam) page number
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "User details fetched successfully",
    "data": [
        {
        "location": {
            "address1": "MTWLABS",
            "address2": "MADHAPUR",
            "city": "Hyderabad",
            "zipcode": "500067",
            "state": "TTG"
        },
        "status": "inactive",
        "createdDate": "2018-12-10T09:49:26.292Z",
        "updatedDate": "2018-12-10T09:49:26.292Z",
        "_id": "5c0e36ddf3876337626e1651",
        "name": "Rajesh",
        "email": "billing@gmail.com",
        "role": "billing",
        "createdBy": "5c0e2d985464f22d65ee070b",
        "__v": 0
    }
]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */



 /**
 * @api {put} /zenworkers/:_id Update others info
 * @apiName UpdateNewZenworker
 * @apiGroup Zerworkers
 * 
 * @apiParam {String} _id zenworker unique _id.
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
 *{
    "name":"Admin",
    "adddress":{
        "address1":"MTWLABS",
        "address2":"MADHAPUR",
        "city":"Hyderabad",
        "zipcode":"500067",
        "state":"TTG"
    },
    "loc":{
    	"type":"Point",
    	"coordinates": [ 1.00000000, 1.00000000000 ]
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Zenworker updated successfully",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */




 /**
 * @api {put} /zenworkers/ Update self info
 * @apiName UpdateSelfInfo
 * @apiGroup Zerworkers
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
 *{
    "name":"Admin",
    "adddress":{
        "address1":"MTWLABS",
        "address2":"MADHAPUR",
        "city":"Hyderabad",
        "zipcode":"500067",
        "state":"TTG"
    },
    "loc":{
    	"type":"Point",
    	"coordinates": [ 1.00000000, 1.00000000000 ]
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Zenworker updated successfully",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /zenworkers/delete DeleteMany
 * @apiName DeleteMany
 * @apiGroup Zerworkers
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "_ids": [
        "5ceb8187cf509c4bb5555c4e"
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */