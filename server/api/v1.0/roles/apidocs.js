/**
 * @api {post} /roles/create-copy CreateCopyOfRole
 * @apiName CreateCopyOfRole
 * @apiGroup Roles
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"_id": "5d75f4a03d04c8071d23de4d"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully created a copy of the role",
    "data": {
        "status": "1",
        "baseRoleId": "5cbe98f8561562212689f748",
        "name": "Billing HR - copy",
        "description": "billing access only",
        "access": "full",
        "roletype": "1",
        "companyId": "5c9e11894b1d726375b23058",
        "roleId": 147
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /roles/users/:role_id GetAllUsersByRole
 * @apiName GetAllUsersByRole
 * @apiGroup Roles
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} role_id Roles unique _id
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Getting user details success",
    "data": [
        {
            "personal": {
                "name": {
                    "firstName": "rock",
                    "middleName": "star",
                    "lastName": "lsdkf",
                    "preferredName": "rockstar"
                }
            },
            "_id": "5d761ebb0db0b007748f5222"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /roles/userIds/:role_id GetAllUserIdsByRole
 * @apiName GetAllUserIdsByRole
 * @apiGroup Roles
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} role_id Roles unique _id
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched users with given role",
    "data": [
        "5d761ebb0db0b007748f5222"
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /roles/change-user-role ChangeUserRole
 * @apiName ChangeUserRole
 * @apiGroup Roles
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"userId": "5d761ebb0db0b007748f5222",
	"newRoleId": "5cbe98a3561562212689f747"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Changed user role successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /roles/userAccessControls GetUserAccessControls
 * @apiName GetUserAccessControls
 * @apiGroup Roles
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched user access controls",
    "data": [
        {
            "page": "Dashboard - ESS",
            "access": "no"
        },
        {
            "page": "Dashboard - MSS",
            "access": "full"
        },
        {
            "page": "My Info (EE's) - Personal",
            "access": "no"
        },
        {
            "page": "My Info (EE's) - Job",
            "access": "full"
        },
        {
            "page": "My Info (EE's) - Time Schedule",
            "access": "full"
        },
        {
            "page": "My Info (EE's) - Notes",
            "access": "full"
        },
        {
            "page": "My Info (EE's) - Compensation",
            "access": "full"
        },
        {
            "page": "My Info (EE's) - Benifits",
            "access": "full"
        },
        {
            "page": "My Info (EE's) - Training",
            "access": "full"
        },
        {
            "page": "My Info (EE's) - On-Boarding",
            "access": "no"
        },
        {
            "page": "My Info (EE's) - Off-Boarding",
            "access": "view"
        },
        {
            "page": "My Info (EE's) - Assets",
            "access": "full"
        },
        {
            "page": "My Info (EE's) - Emergency Contact",
            "access": "full"
        },
        {
            "page": "My Info (EE's) - Custom",
            "access": "full"
        },
        {
            "page": "My Info (EE's) - Documents",
            "access": "view"
        },
        {
            "page": "My Info (EE's) - Audit Trial",
            "access": "full"
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */