let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    name:String,
    description:String,
    access:{type:String,enum: Object.keys(constants.roleAccess)},
    status:{type:String,enum: Object.keys(constants.activeStatus),default:1},
    roletype: {type:String/*,enum: Object.keys(constants.roleTypes)*/, required: true},
    createdBy:{type:ObjectID,ref:'users'},
    updatedBy:{type:ObjectID,ref:'users'},
    baseRoleId: {type: ObjectID, ref: 'roles'},

    // custom fields added for company roles

    companyId: {type: ObjectID, ref: 'companies'}

},{
    timestamps: true,
    versionKey: false
});
Schema.plugin(autoIncrement.plugin, {model:'roles', field:'roleId', startAt: 1, incrementBy: 1});

// Schema.index({loc: '2dsphere'}); //loc: { type: "Point", coordinates: [ longitude, latitude ] },

let ZenworkersModel = mongoose.model('roles',Schema);

module.exports = ZenworkersModel;
