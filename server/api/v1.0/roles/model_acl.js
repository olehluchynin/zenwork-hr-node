let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    field_name:String,
    no_access: {type:Boolean, required: true},
    full_access: {type:Boolean, required: true},
    not_req_approval: {type:Boolean, required: true},
    req_approval:{type:Boolean, required: true},
    updatedBy:{type:ObjectID,ref:'users'},
    companyId: {type: ObjectID, ref: 'companies'},
    roleId: {type: ObjectID, ref: 'roles'},
    category: {type: String, required: 'true'}
},{
    timestamps: true,
    versionKey: false
});
Schema.plugin(autoIncrement.plugin, {model:'roles_acls', field:'aclId', startAt: 1, incrementBy: 1});

// Schema.index({loc: '2dsphere'}); //loc: { type: "Point", coordinates: [ longitude, latitude ] },

let RolesAclModel = mongoose.model('roles_acls',Schema);

module.exports = RolesAclModel;
