let mongoose = require('mongoose');
let ObjectID = require('mongodb').ObjectID;
let RolesModel = require('./model');
let UserModel = require('../users/model');
let RolesAclModel = require('./model_acl');
let WorkflowModel = require('./../workflow/model');
let PagesModel = require('./../pages/model')

let addNew = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        if (!body.name) reject("Invalid name");
        else if (!body.description) reject("Invalid description");
        else if (!body.access) reject('Invalid access');
        else resolve(null);
    }).then(() => {
        if (body._id) {
            body.updatedBy = req.jwt.userId;
            return RolesModel.findOneAndUpdate({ _id: body._id }, { $set: body });
        } else {
            body.createdBy = req.jwt.userId;
            body.updatedBy = req.jwt.userId;
            return RolesModel.create(body);
        }
    }).then((role) => {
        if (body._id) {
            res.status(200).json({ status: true, message: "Role updated successfully", role });
        } else {
            res.status(200).json({ status: true, message: "Role added successfully", role });
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
};

let getRole = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return RolesModel.findOne({ _id: req.params._id }).populate('baseRoleId').lean().exec();
    }).then(role => {
        if (role) {
            if (role.baseRoleId) {
                role.baseRoleName = role.baseRoleId.name
                role.baseRoleId = role.baseRoleId._id
            }
            res.status(200).json({ status: true, message: "Success", data: role });
        }
        else {
            res.status(404).json({ status: false, message: "Not found" });
        }

    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
};

/**
 * This method is used to get all base roles available in the system
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getAllBaseRoles = (req, res) => {
    let body = req.body;

    return new Promise((resolve, reject) => {
        RolesModel.find({ roletype: 0 }).then((roles) => {
            resolve(roles);
        }).catch((error) => {
            reject(error);
        })
    }).then((roles) => {
        res.status(200).json({
            status: true,
            message: "Success",
            data: roles
        });
    }).catch((err) => {
        res.status(400).json({
            status: false,
            message: err,
            data: null
        });
    });
};

/**
 * This method is used to get all roles available in the system
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getAllRoles = (req, res) => {

    return new Promise((resolve, reject) => {
        RolesModel.find({}).then((roles) => {
            resolve(roles);
        }).catch((error) => {
            reject(error);
        })
    }).then((roles) => {
        res.status(200).json({
            status: true,
            message: "Success",
            data: roles
        });
    }).catch((err) => {
        res.status(400).json({
            status: false,
            message: err,
            data: null
        });
    });
};

/**
 * This method is used to delete specific role based on role id
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */
/**
*@author Asma
*@date 29/08/2019 12:40
A role cannot be deleted if it is used in the workflows or if it is assigned to users/employees
*/
let deleteRole = (req, res) => {
    let role_id = req.params.id;
    let companyId = req.jwt.companyId
    return new Promise((resolve, reject) => {
        resolve(
            Promise.all([
                WorkflowModel.findOne({ companyId: companyId, "specialRole.roleId": new ObjectID(role_id) }).lean().exec(),
                UserModel.findOne({ companyId: companyId, 'job.Site_AccessRole': new ObjectID(role_id) }, { 'personal.name': 1 }).lean().exec()
            ])
        )
    }).then(([workflow, user]) => {
        if (workflow) {
            res.status(400).json({
                status: false,
                message: "This role is used in the workflow approval priorities. It cannot be deleted"
            })
        }
        else if (user) {
            res.status(400).json({
                status: false,
                message: "This role is attached to one or more users. It cannot be deleted"
            })
        }
        else {
            RolesModel.deleteOne({ _id: new ObjectID(role_id) }).then((result) => {
                res.status(200).json({
                    status: true,
                    message: "Delete role success",
                    data: result
                });
            }).catch((error) => {
                res.status(500).json({
                    status: false,
                    message: "Failed to delete role",
                    error: error
                });
            });
        }

    }).catch((error) => {
        res.status(500).json({
            status: false,
            message: "Failed to delete role",
            error: error
        });
    });
}

/**
 * This method is used to get roles under specific company
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getCompanyRoles = (req, res) => {
    let company_id = req.params.company_id;

    return new Promise((resolve, reject) => {
        RolesModel.find({ companyId: new ObjectID(company_id) }).populate('baseRoleId', 'name').then((result) => {
            resolve(result);
        }).catch((error) => {
            reject(error);
        });
    }).then((roles) => [
        res.status(200)
            .json({
                status: true,
                message: 'Getting company roles success',
                data: (roles) ? roles : []
            })
    ]).catch((error) => {
        res.status(500)
            .json({
                status: false,
                message: 'Getting company roles failed',
                error: [error]
            })
    });
}

/**
 * This method is used to update role details
 * 
 * @param {*} req 
 * @param {*} res 
 */

let updateRole = (req, res) => {
    let id = req.params.id;

    return new Promise((resolve, reject) => {
        RolesModel.findOneAndUpdate({ _id: new ObjectID(id) }, { $set: req.body }).then((result) => {
            resolve(result)
        }).catch((error) => {
            reject(error)
        })
    }).then((result) => {
        res.status(200).json({
            status: true,
            message: "Update role details success",
            data: (result) ? result : []
        })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "Update role details failed",
            error: error
        })
    })
}

/**
 * This method is used to get all HR roles
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getManagersOrHrs = (req, res) => {
    let role_id = req.params.base_role_id,
        company_id = req.params.company_id;

    RolesModel.find({ roletype: '1', baseRoleId: new ObjectID(role_id), companyId: new ObjectID(company_id) }).then((result) => {
        let rolesIds = result.map(role => {
            return role._id;
        })
        UserModel.find({ "job.Site_AccessRole": { $in: rolesIds } }, { 'personal.name.preferredName': 1 }).then((result) => {
            res.status(200).json({
                status: false,
                message: "User details found",
                data: result ? result : []
            })
        }).catch((error) => {
            res.status(400).json({
                status: false,
                message: "User details not found",
                error: error
            })
        });
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "Invalid roles found",
            error: error
        })
    })
}

/**
 * This method is used to get all the users comes under single derived role
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getAllUsersByRole = (req, res) => {
    let role_id = req.params.role_id;
    // console.log(role_id);

    return new Promise((resolve, reject) => {
        UserModel.find({ "job.Site_AccessRole": role_id }, { 'personal.name': 1 }).then((users) => {
            resolve(users);
        }).catch((error) => {
            reject(error);
        })
    }).then((result) => {
        res.status(200).json({
            status: true,
            message: "Getting user details success",
            data: (result) ? result : []
        })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "Getting user details failed",
            error: error
        })
    })
}

/**
*@author Asma
*@date 10/09/2019 11:54
Method to get IDs of users with given role
*/
let getAllUsersIdsByRole = (req, res) => {
    let role_id = req.params.role_id;
    // console.log(role_id);

    return new Promise((resolve, reject) => {
        UserModel.find({ "job.Site_AccessRole": role_id }, { '_id': 1 }).lean().exec().then((users) => {
            resolve(users);
        }).catch((error) => {
            reject(error);
        })
    }).then(users => {
        let userIds = users.map(x => x._id)
        res.status(200).json({
            status: true,
            message: "Successfully fetched users with given role",
            data: userIds
        })
    }).catch((error) => {
        console.log(error)
        res.status(400).json({
            status: false,
            message: "Getting user details failed",
            error: error
        })
    })
}

let changeUserRole = (req, res) => {
    let body = req.body
    let companyId = req.jwt.companyId
    return new Promise((resolve, reject) => {
        if (!body.userId) reject('userId is required');
        if (!body.newRoleId) reject('newRoleId is required');
        resolve(null);
    })
        .then(() => {
            UserModel.findOneAndUpdate({ companyId: companyId, _id: body.userId }, {
                $set: {
                    "job.Site_AccessRole": body.newRoleId
                }
            },
                { new: true }
            )
                .then(response => {
                    if (response) {
                        res.status(200).json({
                            status: true,
                            message: "Changed user role successfully"
                        })
                    }
                    else {
                        res.status(404).json({
                            status: false,
                            message: "User not found"
                        })
                    }
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).json({
                        status: false,
                        message: "Error while changing user role"
                    })
                })
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

/**
 * This method is used to create custom personal field inside role based
 * 
 * @param {*} req 
 * @param {*} res
 * @author Jagadeesh Patta
 */

let addPersonalFields = (req, res) => {
    let company_id = req.body.company_id,
        role_id = req.body.role_id,
        category = req.body.category,
        fields_array = req.body.fields,
        promise_array = [];

    if (!company_id || !role_id) {
        res.status(400).json({
            status: false,
            message: "Failed to create user related fields"
        })
    } else {
        // fields_array.forEach(field => {
        //     promise_array.push(
        //         new Promise((resolve, reject) => {
        //             field["companyId"] = company_id;
        //             field["roleId"] = role_id;
        //             field["category"] = category;
        //             field["updatedBy"] = company_id;
        //             RolesAclModel.create(field).then((result) => {
        //                 resolve(result)
        //             }).catch((error) => {
        //                 reject(error)
        //             })
        //         })
        //     );
        // });
        fields_array.forEach(field => {
            field["companyId"] = company_id;
            field["roleId"] = role_id;
            field["category"] = category;
            field["updatedBy"] = company_id;
            promise_array.push(RolesAclModel.create(field))
        });
        Promise.all(promise_array).then(result => {
            res.status(200).json({
                status: true,
                message: "Custom fields created successfully",
                data: result
            })
        }).catch((error) => {
            res.status(400).json({
                status: true,
                message: "Custom fields created failed",
                error: error
            })
        })
    }

}

/**
 * This service is used to get all the fields based on search criteria
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getPersonalFields = (req, res) => {
    let company_id = req.params.company_id,
        role_id = req.params.role_id,
        category = req.params.category;

    console.log(company_id, role_id, category)

    if (company_id == undefined || company_id == "" ||
        role_id == undefined || role_id == "") {
        res.status(400).json({
            status: false,
            message: "Invalid input data found"
        })
    } else {
        return new Promise((resolve, reject) => {
            RolesAclModel.find({ companyId: new ObjectID(company_id), roleId: new ObjectID(role_id), category: category }).then((result) => {
                resolve(result)
            }).catch((error) => {
                reject(error)
            })
        }).then((result) => {
            res.status(200).json({
                status: true,
                message: "Records fetching success",
                data: (result) ? result : []
            })
        }).catch((error) => {
            res.status(400).json({
                status: false,
                message: "Records fetching failed",
                error: error
            })
        })
    }
}

/**
 * This method is used to update existing fields data
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let updateFields = (req, res) => {
    let update_list = req.body,
        promise_array = [];

    update_list.forEach(field => {
        promise_array.push(
            new Promise((resolve, reject) => {
                RolesAclModel.update({ _id: ObjectID(field._id) }, field).then((result) => {
                    resolve(result)
                }).catch((error) => {
                    reject(error)
                })
            })
        )
    });

    Promise.all(promise_array).then((result) => {
        res.status(200).json({
            status: true,
            message: "Records Updated",
            data: (result) ? result : []
        })
    }).catch((error) => {
        res.status(200).json({
            status: true,
            message: "Records Updatation failed",
            error: error
        })
    })
}

/**
 * This method is used to get all custom roles by base role id
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getRolesByBaseRole = (req, res) => {
    let role_id = req.params.base_role_id,
        company_id = req.params.company_id;

    RolesModel.find({ roletype: "1", baseRoleId: new ObjectID(role_id), companyId: new ObjectID(company_id) }).then((result) => {
        res.status(200).json({
            status: false,
            message: "Getting custom roles success",
            data: result ? result : []
        })
    }).catch((error) => {
        res.status(400).json({
            status: false,
            message: "Invalid roles found",
            error: error
        })
    })
}

/**
 * This method is used to get all the users comes under one base role
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getAllUsersByBaseRole = (req, res) => {
    let company_id = req.params.company_id,
        base_role_id = req.params.base_role_id;

    RolesModel.find({ companyId: new ObjectID(company_id), baseRoleId: new ObjectID(base_role_id) }).then(result => {
        let roles = result.map(role => role._id);
        roles.push(base_role_id)

        if (roles.length) {
            UserModel.find({ "job.Site_AccessRole": { $in: roles } }).then(result => {
                res.status(200).json({
                    status: true,
                    message: "Results getting sucecss",
                    data: result
                })
            }).catch(error => {
                res.status(400).json({
                    status: false,
                    message: "Results getting failed",
                    error: error
                })
            })
        } else {
            res.status(400).json({
                status: false,
                message: "No derived roles existed with this role"
            })
        }
    }).catch(error => [
        res.status(400).json({
            status: false,
            message: "Getting derived roles failed",
            error: error
        })
    ])
}

let getAllAccessControlRoles = (req, res) => {
    let body = req.body;

    return new Promise((resolve, reject) => {
        RolesModel.find({ roletype: 2 }).sort({ updatedAt: 1 }).lean().exec().then((roles) => {
            for (let role of roles) {
                if (role.name == 'Level 1') {
                    role.name = "Level 1 Support & Implementation"
                }
                if (role.name == 'Level 2') {
                    role.name = "Level 2 Sr Support & Implementation"
                }
            }
            resolve(roles);
        }).catch((error) => {
            reject(error);
        })
    }).then((roles) => {
        res.status(200).json({
            status: true,
            message: "Success",
            data: roles
        });
    }).catch((err) => {
        res.status(400).json({
            status: false,
            message: err,
            data: null
        });
    });
};

let createCopyOfRole = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let roleId = body._id
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required')
        else if (!roleId) reject('_id is required')
        else resolve(RolesModel.findOne({ companyId: companyId, _id: roleId }).lean().exec())
    })
        .then(role => {
            if (role) {
                delete role._id
                delete role.createdBy
                delete role.updatedBy
                delete role.createdAt
                delete role.updatedAt
                role.name += ' - copy'
                res.status(200).json({
                    status: true,
                    message: "Successfully created a copy of the role",
                    data: role
                })
            }
            else {
                res.status(404).json({
                    status: false,
                    message: "Role not found"
                })
            }

        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

let getUserAccessControls = (req, res) => {
    let companyId = req.jwt.companyId
    let userId = req.jwt.userId
    return new Promise((resolve, reject) => {
        resolve(UserModel.findOne({ companyId: companyId, _id: userId }, { 'personal.name': 1, 'job': 1, 'type': 1 }).populate('job.Site_AccessRole').lean().exec())
    })
        .then(async user => {
            console.log(user.type)
            if (user.type != 'employee') {
                res.status(400).json({
                    status: false,
                    message: 'Cannot get access controls for this user type'
                })
            }
            else {
                let pages_acls = [];
                if (user.job.Site_AccessRole.roletype == '1') {
                    console.log('user.job.Site_AccessRole.baseRoleId - ', user.job.Site_AccessRole.baseRoleId)
                    user.job.Site_AccessRole.baseRoleId = await RolesModel.findOne({ _id: ObjectID(user.job.Site_AccessRole.baseRoleId) }).lean().exec();
                    console.log(user.job.Site_AccessRole.baseRoleId)
                    let pages = await PagesModel.find({ companyId: companyId, roleId: ObjectID(user.job.Site_AccessRole._id) }).lean().exec()
                    console.log('pages.length - ', pages.length)
                    for (let page of pages) {
                        let page_acl = {
                            page: page.name,
                            access: page[user.job.Site_AccessRole.baseRoleId['name'].toLowerCase()]
                        }
                        pages_acls.push(page_acl)
                    }
                }
                else if (user.job.Site_AccessRole.roletype == '0') {
                    let pages = await PagesModel.find({ companyId: companyId, roleId: {$exists: false} }).lean().exec()
                    console.log('pages.length - ', pages.length)
                    for (let page of pages) {
                        let page_acl = {
                            page: page.name,
                            access: page[user.job.Site_AccessRole.name.toLowerCase()]
                        }
                        pages_acls.push(page_acl)
                    }
                }
                res.status(200).json({
                    status: true,
                    message: "Successfully fetched user access controls",
                    data: pages_acls
                })
            }

        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                status: false,
                message: "Error while fetching access controls",
                error: err
            })
        })
}

module.exports = {
    addNew,
    getRole,
    getAllRoles,
    deleteRole,
    getCompanyRoles,
    getAllUsersByRole,
    getAllUsersIdsByRole,
    getAllBaseRoles,
    addPersonalFields,
    getPersonalFields,
    updateFields,
    updateRole,
    getManagersOrHrs,
    getRolesByBaseRole,
    getAllUsersByBaseRole,
    getAllAccessControlRoles,
    createCopyOfRole,
    changeUserRole,
    getUserAccessControls,
};
