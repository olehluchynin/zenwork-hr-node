let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let RolesController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.post('/addUpdate',authMiddleware.isUserLogin,validationsMiddleware.reqiuresBody,RolesController.addNew);

router.get('/role/:_id',authMiddleware.isUserLogin,RolesController.getRole);

router.get('/roles'/*,authMiddleware.isUserLogin*/,RolesController.getAllRoles);
router.put('/update/:id'/*,authMiddleware.isUserLogin*/, RolesController.updateRole)

// delete role 
router.delete(
    '/delete/:id', 
    authMiddleware.isUserLogin, 
    // authMiddleware.isCompany,
    RolesController.deleteRole
);

// Get company specific roles
router.get('/company/:company_id', /*[authMiddleware.isUserLogin, authMiddleware.isCompany],*/ RolesController.getCompanyRoles);

// Get all users comes under single role
router.get(
    '/users/:role_id', 
    authMiddleware.isUserLogin,
    RolesController.getAllUsersByRole
);

router.get(
    '/userIds/:role_id', 
    authMiddleware.isUserLogin,
    RolesController.getAllUsersIdsByRole
);

router.get('/users/:company_id/:base_role_id', RolesController.getAllUsersByBaseRole);
router.get('/baseroles', RolesController.getAllBaseRoles);
router.get('/getrolebasedusers/:company_id/:base_role_id', RolesController.getManagersOrHrs);

router.post('/fields/create', RolesController.addPersonalFields);
router.get('/fields/get/:company_id/:role_id/:category', RolesController.getPersonalFields);
router.put('/fields/update', RolesController.updateFields);
router.get('/derived/:company_id/:base_role_id', RolesController.getRolesByBaseRole);

router.get(
    '/acls',
    RolesController.getAllAccessControlRoles
);

router.post(
    '/create-copy',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    RolesController.createCopyOfRole
);

router.put(
    '/change-user-role',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    RolesController.changeUserRole
);

router.get(
    '/userAccessControls',
    authMiddleware.isUserLogin,
    RolesController.getUserAccessControls
);

module.exports = router;
