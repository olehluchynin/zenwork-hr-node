let config = require('../../../config/config');
let UserModel = require('../users/model');
let ZenworkersModel = require('../zenworkers/model');
let CompaniesModel = require('../companies/model');
let utils = require('./../../../common/utils');
var jwt = require('jsonwebtoken');


let isUserLogin = (req, res, next) => {
    // console.log('in user login')
    let token = req.headers['x-access-token'] || req.cookies['token'];
    //   let token = ;
    if (token) {
        let decodedData;
        try {
            decodedData = utils.decodeJWTForUser(token);
        } catch (err) {
            res.status(401).json({ status: false, message: "Invalid token", data: null });
            return;
        }
        var model, findQuery;
        if (['sr-support', 'support', 'billing'].indexOf(decodedData.type) >= 0) {
        } else if (['company', 'reseller', 'employe'].indexOf(decodedData.type) >= 0) {
            model = CompaniesModel;
        }
        UserModel.findOne({ _id: decodedData.userId })
            .then(function (user) {
                console.log(decodedData)
                req.jwt = decodedData;
                req.jwt.email = user.email;
                req.jwt.type = user.type;
                next();
            }).catch(function (err) {
                console.log(err);
                res.status(401).json({ status: false, message: "User not found", data: null });
            });
    } else {
        res.status(401).json({ status: false, message: "User token not found", data: null });
    }
}


let isZenworkerLogin = (req, res, next) => {
    let token = req.headers['x-access-token'] || req.cookies['token'];
    //   let token = ;
    if (token) {
        let decodedData;
        try {
            decodedData = utils.decodeJWTForUser(token);
        } catch (err) {
            res.status(401).json({ status: false, message: "Invalid token", data: null });
            return;
        }
        ZenworkersModel.findOne({ _id: decodedData.zenworkerId, role: decodedData.role })
            .then(function (user) {
                req.jwt = decodedData;
                next();
            }).catch(function (err) {
                console.log(err);
                res.status(401).json({ status: false, message: "User not found", data: null });
            });
    } else {
        res.status(401).json({ status: false, message: "User token not found", data: null });
    }
}

let isSrSupport = (req, res, next) => {
    console.log(req.jwt.type)
    if (req.jwt.type === 'sr-support') {
        next();
    } else {
        res.status(401).json({ status: false, message: "Access Denied for this operation", data: null });
    }
}

let isActive = (req, res, next) => {
    if (req.jwt.status === 'active') {
        next();
    } else {
        res.status(401).json({ status: false, message: "Access Denied for this operation", data: null });
    }
};

let isInRoles = (...roles) => {
    return (req, res, next) => {
        var hasPermission = false;
        console.log("roles", roles, typeof roles);
        roles.forEach(role => {
            hasPermission = hasPermission || (role == req.jwt.role);
        });
        hasPermission = true;
        if (hasPermission) next();
        else res.status(401).json({ status: false, message: "Access Denied for this operation", data: null });
    };
}

let isCompany = (req, res, next) => {
    console.log(req.jwt.type);
    if (req.jwt.type === 'company' || req.jwt.type === 'reseller' || req.jwt.type === 'sr-support') {
        next();
    } else {
        res.status(401).json({ status: false, message: "Access Denied for this operation", data: null });
    }
}

let isAdmin = (req, res, next) => {
    if (req.jwt.role === 'sr-support') {
        next();
    } else {
        res.status(401).json({ status: false, message: "Access Denied for this operation", data: null });
    }
}




let decodeTokenIfAvailable = (req, res, next) => {
    let token = req.headers['x-access-token'] || req.cookies['token'];
    //   let token = ;
    if (token) {
        let decodedData;
        try {
            decodedData = utils.decodeJWTForUser(token);
        } catch (err) {
            next();
        }
        UserModel.findOne({ _id: decodedData.userId, role: decodedData.role })
            .then(function (user) {

                req.jwt = decodedData;
                // console.log("In AUth",decodedData);
                req.jwt.email = user.email;
                req.jwt.name = user.name;
                req.jwt.role = user.type;
                req.jwt.isEmailVerified = user.emailVerification.status;

                next();
            }).catch(function (err) {
                next();
            });
    } else {
        next();
    }
}

let isSrSupportOrZenworkerOrAdministrator = (req, res, next) => {
    console.log(req.jwt.type)
    if (req.jwt.type === 'sr-support' || req.jwt.type === 'zenworker' || req.jwt.type === 'administrator') {
        next();
    } else {
        res.status(401).json({ status: false, message: "Access Denied for this operation", data: null });
    }
}

let isNotCompany = (req, res, next) => {
    console.log(req.jwt.type)
    if (req.jwt.type != 'company') {
        next();
    } else {
        res.status(401).json({ status: false, message: "Access Denied for this operation", data: null });
    }
}


module.exports = {
    isUserLogin,
    decodeTokenIfAvailable,
    isZenworkerLogin,
    isSrSupport,
    isSrSupportOrZenworkerOrAdministrator,
    isAdmin,
    isInRoles,
    isActive,
    isCompany,
    isNotCompany
}
