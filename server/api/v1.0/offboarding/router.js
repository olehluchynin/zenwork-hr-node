
let express = require('express'),
    router = express.Router(),
    controller = require("./controller");
let authMiddleware = require('../middlewares/auth');
let multipart = require('connect-multiparty');

let multipartMiddleware = multipart();

router.post('/template/base/create', controller.createBaseTemplate);
router.get('/template/base/get', controller.getOffboardingTemplate);

router.post('/template/create', controller.createTemplate);
router.get('/template/get/:template_id', controller.getTemplateByTemplateId);
router.put('/template/update/:template_id', controller.updateTemplate);
router.delete('/template/delete/:template_id', controller.deleteTemplate);
router.get('/template/company/get/:company_id', controller.getCompanyTemplates);
router.delete('/template/:task_id', controller.deleteTaskById)

router.get(
    '/getDirectReports/:company_id/:user_id/:page_no/:per_page',
    authMiddleware.isUserLogin,
    controller.getDirectReports
);

router.get(
    '/getAllDirectReports/:company_id/:user_id',
    authMiddleware.isUserLogin,
    controller.getDirectReportsWOP // without pagination
);

router.post(
    '/terminateEmployee',
    authMiddleware.isUserLogin,
    multipartMiddleware,
    controller.terminateEmployee
);

router.get(
    '/assignedTasks/:user_id',
    authMiddleware.isUserLogin,
    controller.getDirectTasks
);

module.exports = router;