/**
 * @api {post} /offboarding/terminateEmployee TerminateEmployee
 * @apiName TerminateEmployee
 * @apiGroup Offboarding
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {multipart/form-data} Request-Example:
 *
{
    -F file=@/C:/Users/Downloads/file1.pdf \
    -F file=@/C:/Users/Downloads/file2.pdf \
    -F 'data={
        "direct_reports": [{
            "_id": "5cda3d4a2fec3c2610219939",
            "ReportsTo": "5cda377f51f0be25b3adceb4"
        }],
        "direct_tasks": [
            {
                "taskType": "On-Boarding",
                "templateId": "5cb870f2b494651a2bd98750",
                "taskCategory": "New Hire Forms",
                "taskId": "5cb9775328e5a5231db1f8c8",
                "taskName": "State Tax Form",
                "taskAssignee": "5cb870f2b494651a2bd98751",
                "assigneeEmail": "xyz@gmail.com"
            },
            {
                "taskType": "On-Boarding",
                "templateId": "5d47c396edd0e346622cc761",
                "taskCategory": "bill",
                "taskId": "5d47c396edd0e346622cc760",
                "taskName": "hiring",
                "taskAssignee": "5cb870f2b494651a2bd98751",
                "assigneeEmail": "xyz@gmail.com"
            }
        ],
        "companyId": "5c9e11894b1d726375b23058",
        "userId": "5cda3ad10e57b225e1240c9d",
        "off_boarding_template_assigned": "5ce280f51b4a170f16e73752",
        "employmentStatus": "Terminated",
        "terminationDate": "2019-05-22T08:47:51.060Z",
        "terminationType": "Voluntary",
        "terminationReason": "Resigned",
        "eligibleForRehire": true,
        "last_day_of_work": "2019-05-22T08:47:51.060Z",
        "termination_comments": "dmmlfmlmls"
    }'
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Employee terminated successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /offboarding/getDirectReports/:company_id/:user_id/:page_no/:per_page GetDirectReports
 * @apiName GetDirectReports
 * @apiGroup Offboarding
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} user_id Users unique _id
 * @apiParam {Number} page_no Page number
 * @apiParam {Number} per_page Number of results wanted per page
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched direct reports",
    "total_count": 1,
    "data": [
        {
            "_id": "5cda3d4a2fec3c2610219939",
            "personal": {
                "name": {
                    "firstName": "Lina",
                    "lastName": "Dunker",
                    "middleName": "James",
                    "preferredName": "Lina"
                },
                "dob": "1995-06-07T18:30:00.000Z",
                "gender": "female",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "3655694769330",
                "ethnicity": "Ethnicity11",
                "address": {
                    "street1": "shdgioidvknkv",
                    "street2": "sdhiyfirnknckbk",
                    "city": "Manhattan",
                    "state": "NewYork",
                    "zipcode": "235365263",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "7745365555",
                    "extention": "3746",
                    "mobilePhone": "23652536649",
                    "homePhone": "233446546025",
                    "workMail": "lina@gmail.com",
                    "personalMail": "lina@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "education": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d2",
                        "university": "",
                        "degree": "",
                        "specialization": "",
                        "gpa": null,
                        "start_date": null,
                        "end_date": null
                    }
                ],
                "languages": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d1",
                        "language": "",
                        "level_of_fluency": "Native proficiency"
                    }
                ],
                "visa_information": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d0",
                        "visa_type": "",
                        "issuing_country": "",
                        "issued_date": null,
                        "expiration_date": null,
                        "status": "",
                        "notes": ""
                    }
                ]
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-05-17T07:40:56.342Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "16099574",
                "employeeType": "Full Time",
                "hireDate": "2019-05-14T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "4346404",
                "employment_status_history": [
                    {
                        "_id": "5cda3d4a2fec3c261021993a",
                        "status": "Active",
                        "effective_date": "2019-05-14T04:00:10.410Z",
                        "end_date": "2019-05-17T03:23:07.896Z"
                    },
                    {
                        "_id": "5cde291b4cdb8624bcc28075",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:05:56.342Z",
                        "end_date": "2019-05-17T03:29:53.021Z"
                    },
                    {
                        "_id": "5cde2ab1a286b927accea5f0",
                        "status": "Active",
                        "effective_date": "2019-05-17T03:05:56.342Z",
                        "end_date": "2019-05-17T03:37:25.999Z"
                    },
                    {
                        "_id": "5cde2c765e2e562494b8187e",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:40:56.342Z",
                        "end_date": "2019-05-17T03:39:00.627Z"
                    },
                    {
                        "_id": "5cde2cd4e83d4e082c7ff95c",
                        "status": "Active",
                        "effective_date": "2019-05-17T04:40:56.342Z",
                        "end_date": "2019-05-17T03:41:08.613Z"
                    },
                    {
                        "_id": "5cde2d541352531830068e82",
                        "status": "Leave",
                        "effective_date": "2019-05-17T06:40:56.342Z",
                        "end_date": "2019-05-17T03:42:49.391Z"
                    },
                    {
                        "_id": "5cde2db9bb4f582cf8013065",
                        "status": "Active",
                        "effective_date": "2019-05-17T07:40:56.342Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cda3d4a2fec3c261021993b",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-14T04:00:10.410Z",
                        "end_date": "2019-05-17T04:43:15.819Z"
                    },
                    {
                        "_id": "5cde3be3f2285524c04b21e2",
                        "jobTitle": "Senior Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-17T04:05:44.350Z"
                    }
                ],
                "ReportsTo": "5cda377f51f0be25b3adceb4"
            }
        }
    ]
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /offboarding/getAllDirectReports/:company_id/:user_id GetAllDirectReports
 * @apiName GetAllDirectReports
 * @apiGroup Offboarding
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} user_id Users unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched direct reports",
    "total_count": 3,
    "data": [
        {
            "_id": "5cf60d14ecc3dd104e07cdc1",
            "personal": {
                "name": {
                    "firstName": "pavan",
                    "middleName": "m",
                    "lastName": "k",
                    "preferredName": "pavan"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "Alaska",
                    "zipcode": "34567",
                    "country": "Afghanistan"
                },
                "contact": {
                    "workPhone": "1234567890",
                    "extention": "2345",
                    "mobilePhone": "2345678976",
                    "homePhone": "2345678976",
                    "workMail": "pavan@gmaiul.com",
                    "personalMail": "pavan3@gmail.com"
                },
                "dob": "2019-06-05T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Married",
                "effectiveDate": "2019-04-10T18:30:00.000Z",
                "veteranStatus": "Other Veteran",
                "ssn": "234567898765",
                "ethnicity": "Not Hispanic or Latino",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-06-04T06:17:56.238Z"
                },
                "Specific_Workflow_Approval": "Yes",
                "employeeId": "10003",
                "employeeType": "Full Time",
                "hireDate": "2019-04-11T18:30:00.000Z",
                "jobTitle": "Internship",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 2",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "First/Mid Level Officials and Managers",
                "Site_AccessRole": "5cdabfd9af231c1ca23a208a",
                "VendorId": "1234567876543",
                "HR_Contact": "5cd91e68f308c21c43e50559",
                "ReportsTo": "5cda377f51f0be25b3adceb4",
                "employment_status_history": [
                    {
                        "_id": "5cf60d14ecc3dd104e07cdc2",
                        "status": "Active",
                        "effective_date": "2019-06-04T06:17:56.238Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cf60d14ecc3dd104e07cdc3",
                        "jobTitle": "Internship",
                        "department": "Tech",
                        "businessUnit": "Unit 2",
                        "unionFields": "Union 1",
                        "ReportsTo": "5cda377f51f0be25b3adceb4",
                        "effective_date": "2019-06-04T06:17:56.238Z"
                    }
                ]
            }
        },
        {
            "_id": "5cda3d4a2fec3c2610219939",
            "personal": {
                "name": {
                    "firstName": "Lina",
                    "lastName": "Dunker",
                    "middleName": "James",
                    "preferredName": "Lina"
                },
                "dob": "1995-06-07T18:30:00.000Z",
                "gender": "female",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-14T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "3655694769330",
                "ethnicity": "Ethnicity11",
                "address": {
                    "street1": "shdgioidvknkv",
                    "street2": "sdhiyfirnknckbk",
                    "city": "Manhattan",
                    "state": "NewYork",
                    "zipcode": "235365263",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "7745365555",
                    "extention": "3746",
                    "mobilePhone": "23652536649",
                    "homePhone": "233446546025",
                    "workMail": "lina@gmail.com",
                    "personalMail": "lina@gmail.com"
                },
                "social_links": {
                    "linkedin": "",
                    "twitter": "",
                    "facebook": ""
                },
                "education": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d2",
                        "university": "",
                        "degree": "",
                        "specialization": "",
                        "gpa": null,
                        "start_date": null,
                        "end_date": null
                    }
                ],
                "languages": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d1",
                        "language": "",
                        "level_of_fluency": "Native proficiency"
                    }
                ],
                "visa_information": [
                    {
                        "_id": "5cdfa11b0eabda6ead6d27d0",
                        "visa_type": "",
                        "issuing_country": "",
                        "issued_date": null,
                        "expiration_date": null,
                        "status": "",
                        "notes": ""
                    }
                ],
                "profile_pic_details": {
                    "originalFilename": "Capture.JPG",
                    "s3Path": "5d079dc0211e63024813e590__Capture.JPG"
                }
            },
            "job": {
                "current_employment_status": {
                    "status": "Terminated",
                    "effective_date": "2019-05-13T18:30:00.000Z"
                },
                "Specific_Workflow_Approval": "No",
                "employeeId": "16099574",
                "employeeType": "Full Time",
                "hireDate": "2019-05-14T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
                "Site_AccessRole": "5cd922b7f308c21c43e50560",
                "VendorId": "4346404",
                "employment_status_history": [
                    {
                        "_id": "5cda3d4a2fec3c261021993a",
                        "status": "Active",
                        "effective_date": "2019-05-14T04:00:10.410Z",
                        "end_date": "2019-05-17T03:23:07.896Z"
                    },
                    {
                        "_id": "5cde291b4cdb8624bcc28075",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:05:56.342Z",
                        "end_date": "2019-05-17T03:29:53.021Z"
                    },
                    {
                        "_id": "5cde2ab1a286b927accea5f0",
                        "status": "Active",
                        "effective_date": "2019-05-17T03:05:56.342Z",
                        "end_date": "2019-05-17T03:37:25.999Z"
                    },
                    {
                        "_id": "5cde2c765e2e562494b8187e",
                        "status": "Leave",
                        "effective_date": "2019-05-17T03:40:56.342Z",
                        "end_date": "2019-05-17T03:39:00.627Z"
                    },
                    {
                        "_id": "5cde2cd4e83d4e082c7ff95c",
                        "status": "Active",
                        "effective_date": "2019-05-17T04:40:56.342Z",
                        "end_date": "2019-05-17T03:41:08.613Z"
                    },
                    {
                        "_id": "5cde2d541352531830068e82",
                        "status": "Leave",
                        "effective_date": "2019-05-17T06:40:56.342Z",
                        "end_date": "2019-05-17T03:42:49.391Z"
                    },
                    {
                        "_id": "5cde2db9bb4f582cf8013065",
                        "status": "Active",
                        "effective_date": "2019-05-17T07:40:56.342Z",
                        "end_date": "2019-05-23T09:06:11.926Z"
                    },
                    {
                        "_id": "5ce6628320c97f2d35a2802b",
                        "status": "Leave of Absence",
                        "effective_date": "2019-05-14T18:30:00.000Z",
                        "end_date": "2019-05-23T09:51:37.374Z"
                    },
                    {
                        "_id": "5ce66d297732592f983702ac",
                        "status": "Terminated",
                        "effective_date": "2019-05-13T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cda3d4a2fec3c261021993b",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-14T04:00:10.410Z",
                        "end_date": "2019-05-17T04:43:15.819Z"
                    },
                    {
                        "_id": "5cde3be3f2285524c04b21e2",
                        "jobTitle": "Senior Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-17T04:05:44.350Z"
                    }
                ],
                "ReportsTo": "5cda377f51f0be25b3adceb4",
                "eligibleForRehire": false,
                "last_day_of_work": "2019-05-19T18:30:00.000Z",
                "off_boarding_template_assigned": "5ce66d297732592f983702b0",
                "terminationDate": "2019-05-13T18:30:00.000Z",
                "terminationReason": "Abandonment",
                "terminationType": "Voluntary",
                "termination_comments": "jhgjh",
                "HR_Contact": "5d0cfcb4e16c4a5dcb9094d0"
            }
        },
        {
            "_id": "5cd91cc3f308c21c43e5054d",
            "personal": {
                "name": {
                    "firstName": "vipin",
                    "middleName": "M",
                    "lastName": "reddy",
                    "preferredName": "vipin"
                },
                "address": {
                    "street1": "hyd",
                    "street2": "hyd",
                    "city": "hyd",
                    "state": "NewYork",
                    "zipcode": "121222222",
                    "country": "United States"
                },
                "contact": {
                    "workPhone": "1234567654",
                    "extention": "2345",
                    "mobilePhone": "2345654345",
                    "homePhone": "6543321234",
                    "workMail": "info@gmail.com",
                    "personalMail": "vipin.reddy@mtwlabs.com"
                },
                "dob": "1992-06-10T18:30:00.000Z",
                "gender": "male",
                "maritalStatus": "Single",
                "effectiveDate": "2019-05-13T18:30:00.000Z",
                "veteranStatus": "Single",
                "ssn": "123321233",
                "ethnicity": "Ethnicity11",
                "education": [],
                "languages": [],
                "visa_information": []
            },
            "job": {
                "current_employment_status": {
                    "status": "Active",
                    "effective_date": "2019-06-06T18:30:00.000Z"
                },
                "employeeId": "1234",
                "employeeType": "Full Time",
                "hireDate": "2018-08-12T18:30:00.000Z",
                "jobTitle": "Software Engineer",
                "department": "Tech",
                "location": "Banglore",
                "businessUnit": "Unit 1",
                "unionFields": "Union 1",
                "FLSA_Code": "Exempt",
                "EEO_Job_Category": "Professionals",
                "Site_AccessRole": "5cd91346f308c21c43e50511",
                "VendorId": "234532",
                "Specific_Workflow_Approval": "No",
                "employment_status_history": [
                    {
                        "_id": "5cd91cc3f308c21c43e5054e",
                        "status": "Active",
                        "effective_date": "2019-05-13T07:29:07.042Z",
                        "end_date": "2019-05-23T08:08:22.872Z"
                    },
                    {
                        "_id": "5ce654f6bee9b42bc9f15f31",
                        "status": "Active",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "end_date": "2019-05-23T08:34:58.834Z"
                    },
                    {
                        "_id": "5ce65b3220c97f2d35a2801d",
                        "status": "Terminated",
                        "effective_date": "2019-05-06T18:30:00.000Z",
                        "end_date": "2019-05-24T02:01:22.631Z"
                    },
                    {
                        "_id": "5ce750723db9243152d77797",
                        "status": "Terminated",
                        "effective_date": "2019-05-07T18:30:00.000Z",
                        "end_date": "2019-06-07T04:32:33.257Z"
                    },
                    {
                        "_id": "5cf9e8e1912a72225bb3fb25",
                        "status": "Active",
                        "effective_date": "2019-06-06T18:30:00.000Z"
                    }
                ],
                "job_information_history": [
                    {
                        "_id": "5cd91cc3f308c21c43e5054f",
                        "jobTitle": "Software Engineer",
                        "department": "Tech",
                        "businessUnit": "Unit 1",
                        "unionFields": "Union 1",
                        "effective_date": "2019-05-13T07:29:07.042Z"
                    }
                ],
                "eligibleForRehire": true,
                "last_day_of_work": "2019-06-20T18:30:00.000Z",
                "off_boarding_template_assigned": "5cf9e8e1912a72225bb3fb27",
                "terminationDate": "2019-06-06T18:30:00.000Z",
                "terminationReason": "Attendance",
                "terminationType": "Voluntary",
                "termination_comments": "ghhj",
                "ReportsTo": "5cda377f51f0be25b3adceb4"
            }
        }
    ]
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /offboarding/assignedTasks/:user_id GetAssignedTasks
 * @apiName GetAssignedTasks
 * @apiGroup Offboarding
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} user_id Users unique _id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched tasks",
    "count": 2,
    "data": [
        {
            "taskType": "On-Boarding",
            "templateId": "5cb870f2b494651a2bd98750",
            "taskCategory": "New Hire Forms",
            "taskId": "5cb9775328e5a5231db1f8c8",
            "taskName": "State Tax Form"
        },
        {
            "taskType": "On-Boarding",
            "templateId": "5d47c396edd0e346622cc761",
            "taskCategory": "bill",
            "taskId": "5d47c396edd0e346622cc760",
            "taskName": "hiring"
        }
    ]
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */