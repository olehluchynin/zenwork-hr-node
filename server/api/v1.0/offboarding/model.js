let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment'),
    ObjectID = mongoose.Schema.ObjectId,
    constants  = require('../../../common/constants');

let schema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies'},
    templateType: {type: String, enum: constants.templateTypes, default: 'base'},
    templateName: {type: String}, 
    assigned_date: {type: Date},
    completion_date: {type: Date},
    offboarding_complete: {
        type: String,
        enum: ['Yes', 'No']
    }, 
    offBoardingTasks: {
        HrTasks: [
            {
                taskName: String,
                taskAssignee: String,
                assigneeEmail: String,
                taskDesription: String,
                timeToComplete: {
                    time: Number,
                    unit: {type:String, enum:["Day","Week","Month"]}
                },
                category: String,
                task_assigned_date: {type:Date},
                task_completion_date: {type:Date},
                task_assigned_by: {type: ObjectID, ref: 'users'},
                notes: String,
                Date: {type:Date},
            }
        ],
        ManagerTasks: [
            {
                taskName: String,
                taskAssignee: String,
                assigneeEmail: String,
                taskDesription: String,
                timeToComplete: {
                    time: Number,
                    unit: {type:String, enum:["Day","Week","Month"]}
                },
                category: String,
                task_assigned_date: {type:Date},
                task_completion_date: {type:Date},
                task_assigned_by: {type: ObjectID, ref: 'users'},
                notes: String,
                Date: {type:Date},
            }
        ],
        ITSetup: [
            {
                taskName: String,
                taskAssignee: String,
                assigneeEmail: String,
                taskDesription: String,
                timeToComplete: {
                    time: Number,
                    unit: {type:String, enum:["Day","Week","Month"]}
                },
                category: String,
                task_assigned_date: {type:Date},
                task_completion_date: {type:Date},
                task_assigned_by: {type: ObjectID, ref: 'users'},
                notes: String,
                Date: {type:Date},
            }
        ]
    },
    createdBy: {type: ObjectID, ref: 'users'},
    updatedBy: {type: ObjectID, ref: 'users'}
},{
    timestamps: true,
    versionKey: false,
    strict: false,
    toJSON: {
        transform: function (doc, ret) {
            if(ret.offBoardingTasks.HrTasks === undefined || ret.offBoardingTasks.HrTasks.length === 0) delete ret.offBoardingTasks.HrTasks
            if(ret.offBoardingTasks.HrTasks === undefined || ret.offBoardingTasks.ManagerTasks.length === 0) delete ret.offBoardingTasks.ManagerTasks
            if(ret.offBoardingTasks.HrTasks === undefined || ret.offBoardingTasks.ITSetup.length === 0) delete ret.offBoardingTasks.ITSetup
        }
    }
})


let offboarding_docs_schema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    userId: {type: ObjectID, ref: 'users', required: true},
    originalFilename: {type: String},   
    s3Path: {type: String}
},{
    timestamps: true,
    versionKey: false
})

schema.plugin(autoIncrement.plugin, {model:'offboarding_templates', field:'otId', startAt: 1, incrementBy: 1});

let OffBoardingModel = mongoose.model('offboarding_templates', schema);

let OffboardingDocsModel = mongoose.model('offboarding_docs',  offboarding_docs_schema)

module.exports = {
    OffBoardingModel,
    OffboardingDocsModel
};