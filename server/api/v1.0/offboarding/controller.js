
let ObjectID = require('mongodb').ObjectID,
    OffBoardingModel = require('./model').OffBoardingModel,
    OffBoardingDocsModel = require('./model').OffboardingDocsModel,
    User = require('./../users/model');
let s3 = require('./../../../common/s3/s3');
let async = require('async');
let InboxModel = require('../inbox/model');
const OnboardingModel = require('./../employee-management/onboarding/onboard-template/model');

/**
 * This method is used to create new base template
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let createBaseTemplate = (req, res) => {

    OffBoardingModel.findOne({ templateType: 'base' }).then(result => {
        if (result) {
            OffBoardingModel.findByIdAndUpdate({ _id: new ObjectID(result._id) }, { $set: req.body }).then(successResponse => {
                res.status(200).json({
                    status: true,
                    message: "Existing template got updated"
                })
            }).catch(error => {
                res.status(400).json({
                    status: true,
                    message: "Existing template updation failed",
                    error: error
                })
            })
        } else {
            OffBoardingModel.create(req.body).then(result => {
                res.status(200).json({
                    status: true,
                    message: "Creation of new template success",
                    data: result ? result : []
                })
            }).catch(error => {
                res.status(200).json({
                    status: true,
                    message: "Creation of new template failed",
                    error: error
                })
            })
        }
    })
}

/**
 * This function is used to create new offboarding template
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let createTemplate = (req, res) => {

    let company_id = req.body.companyId,
        templateName = req.body.templateName,
        templateType = req.body.templateType;
    console.log(company_id, templateName, templateType)

    if (templateType == null || templateType == "base" || company_id == null || company_id == "" || templateName == null || templateName == "") {
        res.status(400).json({
            status: true,
            message: "Invalid inputs detected",
            error: []
        })
    } else {

        let requestData = req.body,
            offboardingTasks = requestData.offBoardingTasks,
            taskCategories = Object.keys(offboardingTasks);

        // taskCategories.forEach(element => {
        //     if (["HrTasks", "ManagerTasks", "ITSetup"].indexOf(element) == -1) {
        //         let category = onboardingTasks[element];

        //         let updatedTasks = category.map(task => {
        //             task._id = new ObjectID()
        //             return task
        //         })
        //         if(updatedTasks.length > 0) {
        //             onboardingTasks[element] = updatedTasks;
        //         }
        //     }
        // });

        offboardingTasks = addItsToInnerObjects(taskCategories, offboardingTasks);

        OffBoardingModel.create(requestData).then(result => {
            res.status(200).json({
                status: true,
                message: "Creation of new template success",
                data: result ? result : []
            })
        }).catch(error => {
            res.status(200).json({
                status: true,
                message: "Creation of new template failed",
                error: error
            })
        })
    }
}

/**
 * This method is used to assign objected id to innre object
 * 
 * @param {*} taskCategories 
 * @param {*} onboardingTasks 
 * @author Jagadeesh Patta
 */

let addItsToInnerObjects = (taskCategories, onboardingTasks) => {
    taskCategories.forEach(element => {
        if (["HrTasks", "ManagerTasks", "ITSetup"].indexOf(element) == -1) {
            let category = onboardingTasks[element];

            let updatedTasks = category.map(task => {
                task._id = new ObjectID()
                return task
            })
            if (updatedTasks.length > 0) {
                onboardingTasks[element] = updatedTasks;
            }
        }
    });

    return onboardingTasks;
}

/**
 * This method is used to get all existing base offboarding template
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getOffboardingTemplate = (req, res) => {

    OffBoardingModel.findOne({ templateType: 'base' }).then(result => {
        console.log(result)
        res.status(200).json({
            status: true,
            message: "Getting base template success",
            data: result ? result : []
        })
    }).catch(error => {
        res.status(400).json({
            status: false,
            message: "Getting base template failed",
            error: error
        })
    })
}

/**
 * This method is used to get company specific templates
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getCompanyTemplates = (req, res) => {
    let company_id = req.params.company_id;

    if (company_id == null || company_id == "") {
        res.status(400).json({
            status: false,
            message: "Invalid inputs detected"
        })
    } else {
        // OffBoardingModel.find({companyId: new ObjectID(company_id), templateType: "derived"}).then(result => {
        OffBoardingModel.find({ companyId: new ObjectID(company_id), templateType: "custom" }).then(result => {
            res.status(200).json({
                status: true,
                message: "Getting company specific templates success",
                data: result ? result : []
            })
        }).catch(error => {
            res.status(400).json({
                status: false,
                message: "Getting company specific templates failed",
                error: error
            })
        })
    }
}

/**
 * This method is used to get template by template id
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let getTemplateByTemplateId = (req, res) => {
    let template_id = req.params.template_id;

    if (template_id == null || template_id == "") {
        res.status(400).json({
            status: false,
            message: "Template id required"
        })
    } else {
        OffBoardingModel.find({ _id: new ObjectID(template_id) }).then(result => {

            let results = result.map(template => {
                return template.toJSON()
            })

            res.status(200).json({
                status: true,
                message: "Getting specific template by id is success",
                data: results ? results : []
            })
        }).catch(error => {
            res.status(400).json({
                status: false,
                message: "Getting template by id is false",
                error: error
            })
        })
    }
}

/**
 * This method is used to update the template based on specic template id
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let updateTemplate = (req, res) => {
    let template_id = req.params.template_id;

    if (template_id == null || template_id == "") {
        res.status(400).json({
            status: false,
            message: "Template Id Required"
        })
    } else {

        let requestData = req.body,
            offboardingTasks = requestData.offBoardingTasks,
            taskCategories = Object.keys(offboardingTasks);

        offboardingTasks = addItsToInnerObjects(taskCategories, offboardingTasks);

        //requestData.offboardingTasks = offboardingTasks;

        OffBoardingModel.findByIdAndUpdate({ _id: new ObjectID(template_id) }, { $set: requestData }).then(result => {
            res.status(200).json({
                status: true,
                message: "Update template success",
                data: result ? result : []
            })
        }).catch(error => {
            res.status(400).json({
                status: false,
                message: "Template updataion failed",
                error: error
            })
        })
    }
}

/**
 * This method is used to delete specific template based on template id
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let deleteTemplate = (req, res) => {
    let template_id = req.params.template_id;

    if (template_id == null || template_id == "") {
        res.status(400).json({
            status: false,
            message: "Template id is required"
        })
    } else {
        OffBoardingModel.findByIdAndRemove({ _id: new ObjectID(template_id) }).then(result => {
            res.status(200).json({
                status: true,
                message: "Selected template deleted successfully",
                data: result ? result : []
            })
        }).catch(error => {
            res.status(400).json({
                status: false,
                message: "Selected template deletion failed",
                error: error
            })
        })
    }
}

/**
 * This method is used to delete task by task id
 * 
 * @param {*} req 
 * @param {*} res 
 * @author Jagadeesh Patta
 */

let deleteTaskById = (req, res) => {
    let task_id = req.params.task_id,
        group_id = req.params.group_id;

    if (task_id == "" || task_id === undefined) {
        res.status(400).json({
            status: false,
            message: "Invalid input detected"
        })
    } else {

        OffBoardingModel.findByIdAndRemove({ _id: new ObjectID(task_id) }).then(result => {
            res.status(200).json({
                status: true,
                message: "deleted successfully",
                data: result ? result : []
            })
        }).catch(error => {
            res.status(400).json({
                status: false,
                message: "delete failed",
                error: error
            })
        })
    }
}

/**
 * Method to get employees who are reporting to the given user
 * @param {*} req 
 * @param {*} res 
 * @author Asma
 */
let getDirectReports = (req, res) => {
    let userId = req.params.user_id
    let companyId = req.params.company_id
    let per_page = parseInt(req.params.per_page)
    let page_no = parseInt(req.params.page_no)
    return new Promise((resolve, reject) => {
        resolve(null);
    })
        .then(() => {
            let find_filter = {
                companyId: companyId,
                'job.ReportsTo': userId
            }
            return Promise.all([
                User.find(find_filter, { personal: 1, job: 1 }).sort({ createdAt: -1 }).skip((page_no - 1) * per_page).limit(per_page).lean().exec(),
                User.countDocuments(find_filter)
            ])
        })
        .then(([employees_list, count]) => {
            res.status(200).json({
                status: true,
                message: "Successfully fetched direct reports",
                total_count: count,
                data: employees_list
            })
        })
        .catch(err => {
            res.status(400).json({
                status: true,
                message: "Error while fetching direct reports",
                error: err
            })
        });
}

/**
*@author Asma
*@date 06/08/2019 15:59
Method to get all direct reports without pagination
*/
let getDirectReportsWOP = (req, res) => {
    let userId = req.params.user_id
    let companyId = req.params.company_id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
        .then(() => {
            let find_filter = {
                companyId: companyId,
                'job.ReportsTo': userId
            }
            return Promise.all([
                User.find(find_filter, { personal: 1, job: 1 }).sort({ createdAt: -1 }).lean().exec(),
                User.countDocuments(find_filter)
            ])
        })
        .then(([employees_list, count]) => {
            res.status(200).json({
                status: true,
                message: "Successfully fetched direct reports",
                total_count: count,
                data: employees_list
            })
        })
        .catch(err => {
            res.status(400).json({
                status: true,
                message: "Error while fetching direct reports",
                error: err
            })
        });
}

/**
 * Method to terminate an employee
 * Updates user with the new termination details
 * Creates derived offboarding template
 * uploads termination related files if present
 * Saves files info 
 * Updates reportsTo field of direct report users
 * @param {*} req 
 * @param {*} res 
 */
let terminateEmployee = (req, res) => {
    console.log('req.files ', req.files)
    console.log(req.body)
    let body = JSON.parse(req.body.data);
    console.log('body ', body)
    let companyId = body.companyId
    let userId = body.userId
    delete body.companyId
    delete body.userId
    let files = req.files.file
    let uploaded_files_info = []
    return new Promise((resolve, reject) => {
        if (!companyId) reject("companyId is required");
        else if (!userId) reject("userId is required");
        else if (files && files.length > 0 && files.length > 5) reject('Maximum 5 files are allowed');
        else resolve(null);
    })
        .then(() => {
            let find_filter = {
                companyId: companyId,
                _id: userId
            }
            Promise.all([
                User.findOne(find_filter),
                OffBoardingModel.findOne({ _id: body.off_boarding_template_assigned })
            ])
            .then(async ([user, offboarding_template]) => {
                offboarding_template = offboarding_template.toJSON()
                console.log(offboarding_template)
                user.job.employment_status_history[user.job.employment_status_history.length - 1].end_date = new Date()
                user.job.employment_status_history.push({
                    'status': body.employmentStatus,
                    "effective_date": body.terminationDate
                })
                user.job.current_employment_status = {
                    'status': body.employmentStatus,
                    "effective_date": body.terminationDate
                }
                user.job.terminationType = body.terminationType
                user.job.terminationReason = body.terminationReason
                user.job.terminationDate = body.terminationDate
                user.job.eligibleForRehire = body.eligibleForRehire
                user.job.last_day_of_work = body.last_day_of_work
                user.job.termination_comments = body.termination_comments
                user.offboardedBy = req.jwt.userId
                let new_template = offboarding_template
                delete new_template._id, delete new_template.otId, delete new_template.createdAt, delete new_template.updatedAt;
                new_template.templateType = 'derived'
                new_template.assigned_date = new Date()
                new_template.offboarding_complete = 'No'
                console.log(new_template)
                console.log(Object.keys(new_template['offBoardingTasks']))
                Object.keys(new_template['offBoardingTasks']).forEach(category => {
                    console.log(category)
                    if (new_template['offBoardingTasks'][category]) {
                        new_template['offBoardingTasks'][category].forEach(offBoardingTask => {
                            offBoardingTask.status = 'Pending'
                            offBoardingTask._id = new ObjectID();
                            offBoardingTask.task_assigned_date = new Date()
                            offBoardingTask.task_assigned_by = req.jwt.userId
                        });
                    }
                });
                let new_template_created = await OffBoardingModel.create(new_template)
                console.log('new_template_created ', new_template_created)
                let saved_template = await new_template_created.save()
                console.log('saved_template ', saved_template)
                user.job.off_boarding_template_assigned = saved_template._id
                saved_template = saved_template.toJSON()
                let notifications = []
                try {
                    for (let category of Object.keys(saved_template['offBoardingTasks'])) {
                        console.log(category)
                        if (saved_template['offBoardingTasks'][category].length > 0) {
                            console.log(category)
    
                            for (const offboarding_task of saved_template['offBoardingTasks'][category]) {
                                let insert_notif = {};
                                console.log(offboarding_task.assigneeEmail)
                                let taskAssignee = await User.findOne({ email: offboarding_task.assigneeEmail })
                                insert_notif['companyId'] = companyId
                                insert_notif['from'] = req.jwt.userId
                                insert_notif['to'] = taskAssignee._id
                                insert_notif['impactedEmployee'] = userId
                                insert_notif['subject'] = "Off-boarding Tasks"
                                insert_notif['offboarding_template_id'] = saved_template._id
                                insert_notif['task_id'] = offboarding_task._id
                                insert_notif['task_category'] = category
                                console.log('insert notif', insert_notif)
                                notifications.push(insert_notif)
                            }
                        }
                    }
                    console.log('notifications', notifications);
                    let notifications_response = await InboxModel.insertMany(notifications)
                    console.log('notifications response', notifications_response)
                }
                catch(e) {
                    console.log(e)
                }
                let updated_user = await user.save();
                return updated_user;
            })
            .then(updated_user => {
                console.log('updated_user ', updated_user)
                let s3_upload_queries = []
                if (files && files.length > 0) {
                    files.forEach(file => {
                        let file_info = {};
                        file_info['companyId'] = companyId
                        file_info['userId'] = userId
                        let _id = new ObjectID();
                        file_info['originalFilename'] = file.originalFilename
                        let s3Path = _id + '__' + file.originalFilename;
                        file_info['s3Path'] = s3Path
                        uploaded_files_info.push(file_info)
                        s3_upload_queries.push(s3.uploadFile('offboarding_docs/' + s3Path, file.path))
                    })
                }
                return Promise.all(s3_upload_queries)
            })
            .then((s3_promise_response) => {
                console.log(s3_promise_response)
                return OffBoardingDocsModel.insertMany(uploaded_files_info)
            })
            .then((offboarding_docs_insert_response) => {
                console.log(offboarding_docs_insert_response);
                
                let direct_reports_queries = [];
                if (body.direct_reports && body.direct_reports.length > 0) {
                    body.direct_reports.forEach(user_details => {
                        let update_filter = {
                            companyId: companyId,
                            _id: user_details._id
                        }
                        direct_reports_queries.push(User.findByIdAndUpdate(update_filter, { $set: { 'job.ReportsTo': user_details.ReportsTo } }))
                    })
                }

                return Promise.all(direct_reports_queries)
            })
            .then((direct_reports_update_response) => {
                console.log(direct_reports_update_response)
                let direct_tasks_queries = []
                if (body.direct_tasks && body.direct_tasks) {
                    for (let task of body.direct_tasks) {
                        if (task.taskType == 'On-Boarding') {
                            let update_filter = {
                                _id: task.templateId,
                            }
                            update_filter['Onboarding Tasks.' + task.taskCategory + '.' + '_id'] = task.taskId
                            let updateObj = {}
                            updateObj['$set'] = {}
                            updateObj['$set']['Onboarding Tasks.' + task.taskCategory + '.$.' + 'taskAssignee'] = task.taskAssignee
                            updateObj['$set']['Onboarding Tasks.' + task.taskCategory + '.$.' + 'assigneeEmail'] = task.assigneeEmail
                            direct_tasks_queries.push(OnboardingModel.updateOne(update_filter, updateObj, {new: true}))
                        }
                        if (task.taskType ==  'Off-Boarding') {
                            let update_filter = {
                                _id: task.templateId,
                            }
                            update_filter['offBoardingTasks' + task.taskCategory + '.' + '_id'] = task.taskId
                            let updateObj = {}
                            updateObj['$set'] = {}
                            updateObj['$set']['offBoardingTasks' + task.taskCategory + '.$.' + 'taskAssignee'] = task.taskAssignee
                            updateObj['$set']['offBoardingTasks' + task.taskCategory + '.$.' + 'assigneeEmail'] = task.assigneeEmail
                            direct_tasks_queries.push(OffBoardingModel.updateOne(update_filter, updateObj, {new: true}))
                        }
                    }
                }
                return Promise.all(direct_tasks_queries)
            })
            .then(direct_tasks_responses => {
                console.log('direct_tasks_responses - ', direct_tasks_responses)
                res.status(200).json({
                    status: true,
                    message: "Employee terminated successfully"
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: 'Error while terminating employee',
                    error: err
                })
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

let getDirectTasks = (req, res) => {
    let companyId = req.jwt.companyId
    let userId = req.params.user_id
    return new Promise((resolve, reject) => {
        resolve(
            Promise.all([
                OffBoardingModel.find({companyId: companyId, templateType: 'base'}).lean().exec(),
                OnboardingModel.find({companyId: companyId, templateType: 'base'}).lean().exec(),
                User.findOne({companyId: companyId, _id: ObjectID(userId)}, {email: 1})
            ])
        )
    })
    .then(([offboarding_templates, onboarding_templates, user]) => {
        console.log('user -', user)
        console.log('offboarding_templates.length - ', offboarding_templates.length)
        console.log('onboarding_templates.length - ', onboarding_templates.length)
        let tasks_assigned_to_this_user = []
        for(let template of offboarding_templates) {
            for (let [task_category, tasks_array] of Object.entries(template['offBoardingTasks'])) {
                for(let task of tasks_array) {
                    if (task.assigneeEmail == user.email) {
                        let direct_tasks_obj = {
                            taskType: 'Off-Boarding',
                            templateId: template._id,
                            taskCategory: task_category,
                            taskId: task._id,
                            taskName: task.taskName
                        }
                        tasks_assigned_to_this_user.push(direct_tasks_obj)
                    }
                }
            }
        }
        for(let template of onboarding_templates) {
            for (let [task_category, tasks_array] of Object.entries(template['Onboarding Tasks'])) {
                for(let task of tasks_array) {
                    if (task.assigneeEmail == user.email) {
                        let direct_tasks_obj = {
                            taskType: 'On-Boarding',
                            templateId: template._id,
                            taskCategory: task_category,
                            taskId: task._id,
                            taskName: task.taskName
                        }
                        tasks_assigned_to_this_user.push(direct_tasks_obj)
                    }
                }
            }
        }
        res.status(200).json({
            status: true,
            message: "Successfully fetched tasks",
            count: tasks_assigned_to_this_user.length,
            data: tasks_assigned_to_this_user
        })
    })
    .catch(err => {
        console.log(err)
        res.status(200).json({
            status: false,
            message: "Error while fetching tasks",
            error: err
        })
    })
}


module.exports = {
    createTemplate,
    getOffboardingTemplate,
    getCompanyTemplates,
    updateTemplate,
    deleteTemplate,
    createBaseTemplate,
    getTemplateByTemplateId,
    deleteTaskById,

    terminateEmployee,
    getDirectReports,
    getDirectReportsWOP, // without pagination
    getDirectTasks

}

