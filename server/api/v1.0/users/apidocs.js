/**
 * @apiDefine DataNotFoundError
 *
 * @apiError DataNotFound The data the you are requested was not found.
 *
 * @apiErrorExample Data Not Found Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "message": "Data Not Found"
 *       "data": null 
 *     }
 */


 /**
 * @apiDefine InternalServerError
 *
 * @apiError InternalServerError Internal Server error.
 *
 * @apiErrorExample Internal Server Error:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "status": false,
 *       "message": "Internal server error"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine TokenNotFound
 *
 * @apiError TokenNotFound User Token Not Found.
 *
 * @apiErrorExample Token Not Found:
 *     HTTP/1.1 400 Token Not Found
 *     {
 *       "status": false,
 *       "message": "Token Not Found"
 *       "data": null 
 *     }
 */

  /**
 * @apiDefine BadRequest
 *
 * @apiError BadRequest Bad request.
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": false,
 *       "message": "Invalid key"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine AccessDeneid
 *
 * @apiError AccessDenied Access denied for this operation.
 *
 * @apiErrorExample Access Deneid:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "status": false,
 *       "message": "Access Denied for this operation"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine DataAlreadyExists
 *
 * @apiError DataConflict Date already exists.
 *
 * @apiErrorExample Data Conflict Error:
 *     HTTP/1.1 402 Data Conflict
 *     {
 *       "status": false,
 *       "message": "Data already exist"
 *       "data": null 
 *     }
 */



  /**
 * @api {post} /users/login Login
 * @apiName UsersLogin
 * @apiGroup Users
 * @apiParamExample {json} Request-Example:
 *
{
	"email":"admin@gmail.com",
	"password":"123456789"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Logged successfully",
    "data": {
        "createdDate": "2018-12-31T11:04:41.965Z",
        "updatedDate": "2018-12-31T11:04:41.965Z",
        "_id": "5c1263a8d84721019cf167bd",
        "email": "admin@gmail.com",
        "__v": 0,
        "type": "sr-support",
        "zenworkerId": {
            "status": "active",
            "createdDate": "2018-12-31T11:04:41.976Z",
            "updatedDate": "2018-12-31T11:04:41.976Z",
            "_id": "5c126409d84721019cf167be",
            "name": "Admin",
            "email": "admin@gmail.com",
            "role": "sr-support",
            "location": {
                "address1": "MTWLABS",
                "address2": "MADHAPUR",
                "city": "Hyderabad",
                "zipcode": "500067",
                "state": "TTG"
            },
            "userId": "5c1263a8d84721019cf167bd"
        },
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzEyNjQwOWQ4NDcyMTAxOWNmMTY3YmUiLCJ1c2VySWQiOiI1YzEyNjNhOGQ4NDcyMTAxOWNmMTY3YmQiLCJ0eXBlIjoic3Itc3VwcG9ydCIsImlhdCI6MTU0NjI1NDYwMiwiZXhwIjoxNTc3NzkwNjAyfQ.7ZkkzAfimQ0lRMNIe8ualMjXhYAmhoGF-n4xXqnaus8"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */



  /**
 * @api {put} /users/password Password update
 * @apiName UsersPassword   Update
 * @apiGroup Users
 * @apiParamExample {json} Request-Example:
 *
{
	"currentPassword":"987654321",
	"newPassword":"123456789"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Password updated",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /users/update UpdateUser
 * @apiName UpdateUser
 * @apiGroup Users
 * 
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "_id": "5cd02c6e0606e63cd0b385bb",
    "personal": {
        "name": {
            "firstName": "Suresh",
            "middleName": "Malapati",
            "lastName": "Reddy",
            "preferredName": "Sureshh"
        },
        "address": {
            "street1": "hyd",
            "street2": "hyd",
            "city": "hyd",
            "state": "Florida",
            "zipcode": "123256666",
            "country": "India"
        },
        "contact": {
            "workPhone": "1324432423",
            "extention": "1234",
            "mobilePhone": "2134321421",
            "homePhone": "1243321456",
            "workMail": "suresh@hjdsgf.com",
            "personalMail": "suri@kjsdhfjksd.com"
        },
        "dob": "2019-05-15T18:30:00.000Z",
        "gender": "male",
        "maritalStatus": "Single",
        "effectiveDate": "2019-05-28T18:30:00.000Z",
        "veteranStatus": "Single",
        "ssn": "132487",
        "ethnicity": "Ethnicity11",
        "social_links": {
            "linkedin": "https://linkedin.comfsdf",
            "twitter": "",
            "facebook": ""
        },
        "education": [
            {
                "_id": "5d4c01ccd43bfd78326223df",
                "university": "gfdsgfrere",
                "degree": "Bachelors",
                "specialization": "IT",
                "gpa": 5,
                "start_date": "2019-08-28T18:30:00.000Z",
                "end_date": "2019-09-25T18:30:00.000Z"
            }
        ],
        "languages": [
            {
                "_id": "5d4c0467d43bfd78326223e0",
                "language": "French",
                "level_of_fluency": "Intermediate"
            }
        ],
        "visa_information": [],
        "profile_pic_details": {
            "originalFilename": "_20170810_090706.jpg",
            "s3Path": "5d4c1babd43bfd78326223f9___20170810_090706.jpg",
            "url": "https://zenworkhr-docs.s3.amazonaws.com/profile_photos/5d4c1babd43bfd78326223f9___20170810_090706.jpg?AWSAccessKeyId=AKIAQQWFVNX2IASWO5S7&Expires=1565355359&Signature=jS4ux1cvqMXE5YhHVh09Ymq4OEw%3D"
        }
    },
    "compensation": {
        "payRate": {
            "amount": 2134
        },
        "NonpaidPosition": true,
        "Pay_group": "Group2",
        "Pay_frequency": "Yearly",
        "First_Pay_Date": "2019-05-09T18:30:00.000Z",
        "compensationRatio": "1234",
        "Salary_Grade": "a"
    },
    "status": "active",
    "groups": [],
    "job": {
        "employeeId": "",
        "employeeType": "Part Time",
        "hireDate": "2019-05-21T18:30:00.000Z",
        "jobTitle": "Internship",
        "department": "Sales",
        "location": "Banglore",
        "businessUnit": "Unit 1",
        "unionFields": "Union 1",
        "FLSA_Code": "Exempt",
        "EEO_Job_Category": "Technicians",
        "Site_AccessRole": "5ccfbd540606e63cd0b38568",
        "VendorId": "2134214",
        "Specific_Workflow_Approval": "Approval1"
    },
    "module_settings": {
        "eligible_for_benifits": true,
        "assign_leave_management_rules": false,
        "assign_performance_management": true,
        "assaign_employee_number": "System Generated",
        "onboarding_template": "5cb870f2b494651a2bd98750"
    },
    "email": "suresh@hjdsgf.com",
    "companyId": "5c9e11894b1d726375b23058",
    "createdAt": "2019-05-06T12:45:34.595Z",
    "updatedAt": "2019-05-06T12:45:34.595Z",
    "userId": 75,
    "type": "employee"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successsfully updated user",
    "data": {
        "personal": {
            "name": {
                "firstName": "Suresh",
                "middleName": "Malapati",
                "lastName": "Reddy",
                "preferredName": "Sureshh"
            },
            "address": {
                "street1": "hyd",
                "street2": "hyd",
                "city": "hyd",
                "state": "Florida",
                "zipcode": "123256666",
                "country": "India"
            },
            "contact": {
                "workPhone": "1324432423",
                "extention": "1234",
                "mobilePhone": "2134321421",
                "homePhone": "1243321456",
                "workMail": "suresh@hjdsgf.com",
                "personalMail": "suri@kjsdhfjksd.com"
            },
            "dob": "2019-05-15T18:30:00.000Z",
            "gender": "male",
            "maritalStatus": "Single",
            "effectiveDate": "2019-05-28T18:30:00.000Z",
            "veteranStatus": "Single",
            "ssn": "132487",
            "ethnicity": "Ethnicity11"
        },
        "job": {
            "employeeId": "",
            "employeeType": "Part Time",
            "hireDate": "2019-05-21T18:30:00.000Z",
            "jobTitle": "Internship",
            "department": "Sales",
            "location": "Banglore",
            "businessUnit": "Unit 1",
            "unionFields": "Union 1",
            "FLSA_Code": "Exempt",
            "EEO_Job_Category": "Technicians",
            "Site_AccessRole": "5ccfbd540606e63cd0b38568",
            "VendorId": "2134214",
            "Specific_Workflow_Approval": "Approval1"
        },
        "compensation": {
            "payRate": {
                "amount": 2134
            },
            "NonpaidPosition": true,
            "Pay_group": "Group2",
            "Pay_frequency": "Yearly",
            "First_Pay_Date": "2019-05-09T18:30:00.000Z",
            "compensationRatio": "1234",
            "Salary_Grade": "a"
        },
        "module_settings": {
            "eligible_for_benifits": true,
            "assign_leave_management_rules": false,
            "assign_performance_management": true,
            "assaign_employee_number": "System Generated",
            "onboarding_template": "5cb870f2b494651a2bd98750"
        },
        "status": "active",
        "groups": [],
        "_id": "5cd02c6e0606e63cd0b385bb",
        "email": "suresh@hjdsgf.com",
        "companyId": "5c9e11894b1d726375b23058",
        "password": "jOws1VqZ",
        "createdAt": "2019-05-06T12:45:34.595Z",
        "updatedAt": "2019-05-08T11:17:23.213Z",
        "userId": 75,
        "type": "employee"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /users/getOtherUser/:company_id/:user_id GetOtherUser
 * @apiName GetOtherUser
 * @apiGroup Users
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 * @apiParam {ObjectId} user_id Users unique _id
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "User fetched successfully",
    "data": {
        "_id": "5cda3d4a2fec3c2610219939",
        "personal": {
            "name": {
                "firstName": "Lina",
                "middleName": "James",
                "lastName": "Dunker",
                "preferredName": "Lina"
            },
            "address": {
                "street1": "shdgioidvknkv",
                "street2": "sdhiyfirnknckbk",
                "city": "Manhattan",
                "state": "NewYork",
                "zipcode": "235365263",
                "country": "United States"
            },
            "contact": {
                "workPhone": "7745365555",
                "extention": "3746",
                "mobilePhone": "23652536649",
                "homePhone": "233446546025",
                "workMail": "lina@gmail.com",
                "personalMail": "lina@gmail.com"
            },
            "dob": "1995-06-07T18:30:00.000Z",
            "gender": "female",
            "maritalStatus": "Single",
            "effectiveDate": "2019-05-14T18:30:00.000Z",
            "veteranStatus": "Single",
            "ssn": "3655694769330",
            "ethnicity": "Ethnicity11",
            "education": [],
            "languages": [],
            "visa_information": []
        },
        "job": {
            "current_employment_status": {
                "status": "Active",
                "effective_date": "2019-05-14T04:00:10.410Z"
            },
            "Specific_Workflow_Approval": "No",
            "employeeId": "16099574",
            "employeeType": "Full Time",
            "hireDate": "2019-05-14T18:30:00.000Z",
            "jobTitle": "Software Engineer",
            "department": "Tech",
            "location": "Banglore",
            "businessUnit": "Unit 1",
            "unionFields": "Union 1",
            "FLSA_Code": "Exempt",
            "EEO_Job_Category": "Executive/Senior Level Officials and Managers",
            "Site_AccessRole": "5cd922b7f308c21c43e50560",
            "VendorId": "4346404",
            "employment_status_history": [
                {
                    "_id": "5cda3d4a2fec3c261021993a",
                    "status": "Active",
                    "effective_date": "2019-05-14T04:00:10.410Z"
                }
            ],
            "job_information_history": [
                {
                    "_id": "5cda3d4a2fec3c261021993b",
                    "jobTitle": "Software Engineer",
                    "department": "Tech",
                    "businessUnit": "Unit 1",
                    "unionFields": "Union 1",
                    "effective_date": "2019-05-14T04:00:10.410Z"
                }
            ]
        },
        "status": "active",
        "groups": [],
        "module_settings": {
            "eligible_for_benifits": true,
            "assign_leave_management_rules": true,
            "assign_performance_management": true,
            "assaign_employee_number": "System Generated",
            "onboarding_template": "5cda3d4a2fec3c261021993f"
        },
        "email": "lina@gmail.com",
        "companyId": "5c9e11894b1d726375b23058",
        "type": "employee",
        "compensation": {
            "NonpaidPosition": true,
            "HighlyCompensatedEmployee": true,
            "Pay_group": "Group2",
            "Pay_frequency": "Weekly",
            "Salary_Grade": "09",
            "compensationRatio": "60%",
            "current_compensation": [
                {
                    "_id": "5cdb887e948f732a13a5731b",
                    "payRate": {
                        "amount": 98,
                        "unit": ""
                    },
                    "effective_date": "2019-05-18T12:15:34.000Z",
                    "payType": "Monthly",
                    "Pay_frequency": "Yearly",
                    "notes": "ertey",
                    "changeReason": "A-Promotion"
                }
            ],
            "compensation_history": []
        },
        "createdAt": "2019-05-14T04:00:10.460Z",
        "updatedAt": "2019-05-15T03:33:18.036Z",
        "userId": 95
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /users/passwordresetlink SendResetPasswordLink
 * @apiName SendResetPasswordLink
 * @apiGroup Users
 * @apiParamExample {json} Request-Example:
 *
{
	"email":"admin@gmail.com"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Reset password link is sent to your email"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /users/resetpasswordwithtoken ResetPasswordWithToken
 * @apiName ResetPasswordWithToken
 * @apiGroup Users
 * @apiParamExample {json} Request-Example:
 *
{
    "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzllMTE4OTRiMWQ3MjYzNzViMjMwNTgiLCJ1c2VySWQiOiI1YzllMTE4OTRiMWQ3MjYzNzViMjMwNTciLCJ0eXBlIjoiY29tcGFueSIsImlhdCI6MTU1NzIxOTUyOCwiZXhwIjoxNTg4NzU1NTI4fQ.0uJRihWypFYW0LtEFrdLHtc60gMubd0wRcbnMzLBgNk",
    "newPassword": "xyz",
    "confirmPassword": "xyz",
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Password changed successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */