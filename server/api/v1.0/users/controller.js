let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let UserModel = require('./model');
let CompaniesModel = require('./../companies/model');
let CompanySettingsModel = require('./../companies/settings/model');
let OnboardingModel = require('../employee-management/onboarding/onboard-template/model');
let OrdersModel = require('./../orders/model');
let jwt = require('jsonwebtoken');
let config = require('../../../config/config');
let onboardingController = require('../employee-management/onboarding/onboard-template/controller');
let utils = require('./../../../common/utils');
let helpers = require('./../../../common/helpers');
let constants = require('./../../../common/constants');
let rolesModel = require('./../roles/model');
const url = require('url');
let mailer = require('./../../../common/mailer');
let s3 = require('./../../../common/s3/s3');
var Handlebars = require('handlebars');
let fs = require('fs');
let JobSalaryLinkModel = require('./../settings-structure/job-salary-model');
let GradeBandsModel = require('./../salary-grades/model').GradeBandsModel;
let PagesModel = require('./../pages/model');
let PagesTemplate = require('./../pages/pages_template')
let _ = require("underscore");

//sign up from landing page
let signup = (req, res) => {
    let body = req.body;
    if (body.signUpType == 'google') {
        body.password = "test@123";
    }
    
    return new Promise((resolve, reject) => {
        if (!body.name) reject("Invalid company name"); //company name
        else if (!body.employeCount || body.employeCount < 1) reject("Invalid company count");
        else if (!body.primaryContact.name) reject("Invalid name");
        else if (!body.primaryContact.phone) reject("Invalid phone number");
        else if (!body.email || !helpers.isValidEmail(body.email)) reject("Invalid email");
        else if (!body.password) reject("Invalid passoword");
        else {
            body.email = body.email.toLowerCase()
            resolve(UserModel.findOne({ email: body.email }).lean().exec())
        }
    })
    .then(data => {
        if (data) {
            res.status(409).json({ status: false, message: "Email already exists", data: null });
        } else {
            body.type = 'company';
            body.password = utils.encryptPassword(body.password);
            if (body.signUpType == 'google') {
                body.primaryContact.isEmailVerified = true
            }
            Promise.all([
                CompaniesModel.create(body), 
                UserModel.create(body), 
                CompanySettingsModel.create(body)
            ])
            .then(async ([company, user, companySettings]) => {
                console.log('company - ', company)
                console.log('user - ', user)
                console.log('companySettings - ', companySettings)
                if(!company || !user || !companySettings) {
                    console.log('Error while creating user')
                    throw 'Error while creating user'
                }
                else {
                    let standardTemplate = await onboardingController.addDefaultOBTemplate(company._id);
                    console.log(standardTemplate)
                    user.companyId = company._id;
                    company.userId = user._id;
                    company.settingsId = companySettings._id;
                    companySettings.companyId = company._id;
                    let companyPages = PagesTemplate.pages.map(x => {
                        x.companyId = company._id;
                        x.createdBy = user._id
                        x.updatedBy = user._id
                        return x;
                    })
                    companyPages = await PagesModel.insertMany(companyPages)
                    console.log('companyPages', companyPages)
                    company.save();
                    user.save();
                    companySettings.save();
                    // welcome aboard email
                    let to = body.email
                    let from = config.mail.defaultFromAddress
                    let subject = 'Welcome to the Zenwork HR family'
                    let email_body = {
                        logo_url: config.baseUrl + '/api/client/images/mainblacklogo.png',
                        login_url: config.frontEndUrl + '/zenwork-login',
                        name: body.primaryContact.name
                    }
                    let text = "ZenworkHR"
                    // let url = 'https://52.207.167.138:3003/api/client/images/mainblacklogo.png'
                    // let login_url = 'https://zenworkhr.com/zenwork-login'
                    fs.readFile(__dirname + '/self_sign_on_email.hbs', 'utf8', async (err, file_data) => {
                        if (!err) {
                            // console.log(file_data)
                            let html_data = (Handlebars.compile(file_data))(email_body)
                            // console.log(to, from, subject, text, html_data)
                            try {
                                await mailer.sendSignUpEmail(to, from, subject, text, html_data)
                                res.status(200).json({ status: true, message: "Details added successfully", data: company });
                            }
                            catch (e) {
                                console.log(err)
                                res.status(400).json({
                                    status: false,
                                    message: "Error while sending email",
                                    error: err
                                })
                            }
                        }
                        else {
                            console.log(err)
                            res.status(400).json({
                                status: false,
                                message: "Error while sending email",
                                error: err
                            })
                        }
                    })
                }
                
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({ status: false, message: "Error while creating user. Please try again", error: err});
            })
        }
    })
    .catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err});
    });
};

let login = (req, res) => {
    let body = req.body;
    return new Promise((resolve, reject) => {
        if (!body.email || !helpers.isValidEmail(body.email)) reject("Invalid email");
        else if (!body.password) reject("Invalid passoword");
        else resolve(null);
    }).then(() => {
        return UserModel.findOne({ email: body.email })
            .populate('zenworkerId')
            .populate('companyId')
            .populate('job.Site_AccessRole');

        /**
        *@author Asma
        *@date 08/05/2019 12:12
        
        */
        // .populate('employeeId');
    }).then(async user => {
        console.log('user', user);
        if (!user) {
            res.status(400).json({ status: false, message: "User not found", data: null });
        } else {
            if (body.password == utils.decryptPassword(user.password)) {
                user = utils.formatUserData(user.toJSON());
                var _id;
                var employeeId;
                var companyId;
                var zenworkerId;

                if (user.type == 'sr-support' || user.type == 'support' || user.type == 'billing' || user.type == 'zenworker') {
                    user.zenworkerId = utils.formatZenworkerData(user.zenworkerId);
                    let acl = await rolesModel.findOne({ _id: ObjectID(user.zenworkerId.roleId) });
                    console.log('acl', acl)
                    user.zenworkerId.roleId = acl
                    // _id = user.zenworkerId._id;
                    _id = user._id
                    zenworkerId = user.zenworkerId._id;
                } else if (user.type == 'company' || user.type == 'reseller' || user.type == 'reseller-dr' || user.type == 'reseller-client') {
                    user.companyId = utils.formatCompanyData(user.companyId);
                    // _id = user.companyId._id;
                    _id = user._id
                    companyId = user.companyId._id;
                }
                // else  if(user.type == 'employee'  ||  user.type == 'manager'){
                //     user.employeeId = utils.formatEmployeeData(user.employeeId);
                //     _id = user.employeeId._id; 

                // }

                /**
                *@author Asma
                *@date 08/05/2019 12:12
                
                */
                else if (user.type == 'employee') {
                    // user.employeeId = utils.formatEmployeeData(user.employeeId);
                    // _id = user.employeeId._id; 

                    // let pages_acls = [];
                    _id = user._id
                    companyId = user.companyId._id;
                    console.log('user.job.Site_AccessRole.roletype - ', user.job.Site_AccessRole.roletype)
                    if(user.job.Site_AccessRole.roletype == '1') {
                        console.log('user.job.Site_AccessRole.baseRoleId - ', user.job.Site_AccessRole.baseRoleId)
                        user.job.Site_AccessRole.baseRoleId = await rolesModel.findOne({ _id: ObjectID(user.job.Site_AccessRole.baseRoleId) }).lean().exec();
                        console.log(user.job.Site_AccessRole.baseRoleId)
                        // let pages = await PagesModel.find({companyId: companyId, roleId: ObjectID(user.job.Site_AccessRole._id)}).lean().exec()
                        // for (let page of pages) {
                        //     let page_acl = {
                        //         page: page.name,
                        //         access: page[user.job.Site_AccessRole.baseRoleId['name'].toLowerCase()]
                        //     }
                        //     pages_acls.push(page_acl)
                        // }
                    }
                    // else if (user.job.Site_AccessRole.roletype == '0'){
                    //     let pages = await PagesModel.find({companyId: companyId}).lean().exec()
                    //     for (let page of pages) {
                    //         let page_acl = {
                    //             page: page.name,
                    //             access: page[user.job.Site_AccessRole.name.toLowerCase()]
                    //         }
                    //         pages_acls.push(page_acl)
                    //     }
                    // }
                    // user.pages_acls = pages_acls
                }
                else if (user.type == 'administrator') {
                    let acl = await rolesModel.findOne({ _id: ObjectID(user.zenworkerId.roleId) });
                    console.log('acl', acl)
                    user.zenworkerId.roleId = acl
                    _id = user._id
                    companyId = user.companyId._id;
                }
                // console.log("=========================");
                // console.log(_id,user._id,user.type);
                user.token = utils.generateJWTForUser(_id, companyId, zenworkerId, user.type);
                res.status(200).json({ status: true, message: "Logged successfully", data: user });
            } else {
                res.status(400).json({ status: false, message: "Invalid password", data: null });
            }
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ status: false, message: err, data: null });
    });
}


let loginGoogle = (req, res) => {
    var name = "";
    // var name1 = "";
    // var name = { 
    //     firstName: String, 
    //     lastName: String, 
    //     middleName: String, 
    //     preferredName: String, 
    //     salutation: String 
    // },
    if (req["user"]["name"]["givenName"])
        name += " " + req["user"]["name"]["givenName"];
    if (req["user"]["name"]["middleName"])
        name += " " + req["user"]["name"]["middleName"];
    if (req["user"]["name"]["familyName"])
        name += " " + req["user"]["name"]["familyName"];
    var email;

    try {
        if (req["user"]["emails"]) email = req["user"]["emails"][0]["value"];
    } catch (err) { if (req['user']['email']) email = req['user']['email'] }

    var id = req["user"]["id"];

    let body = {
        googleId: id,
        name: name,
        email: email
    };

    let findQuery;

    findQuery = { $or: [{ googleId: id }, { email: body.email }] };

    if (email) findQuery = { $or: [{ googleId: id }, { email: body.email }] };
    else findQuery = { googleId: id };

    UserModel.findOne(findQuery)
        .then(response => {
            if (response) {
                UserModel.findOne({ email: body.email }).populate('zenworkerId').populate('companyId')
                    .then(async user => {
                        user = utils.formatUserData(user.toJSON());
                        var _id;
                        var employeeId;
                        var companyId;
                        var zenworkerId;
                        if (user.type == 'sr-support' || user.type == 'support' || user.type == 'billing' || user.type == 'zenworker') {
                            user.zenworkerId = utils.formatZenworkerData(user.zenworkerId);
                            let acl = await rolesModel.findOne({ _id: ObjectID(user.zenworkerId.roleId) });
                            console.log('acl', acl)
                            user.zenworkerId.roleId = acl
                            _id = user._id
                            zenworkerId = user.zenworkerId._id;
                        }
                        else if (user.type == 'company' || user.type == 'reseller' || user.type == 'reseller-dr' || user.type == 'reseller-client') {
                            user.companyId = utils.formatCompanyData(user.companyId);
                            _id = user._id
                            companyId = user.companyId._id;
                        }
                        else if (user.type == 'employee') {
                            _id = user._id
                            companyId = user.companyId._id;
                        }
                        else if (user.type == 'administrator') {
                            let acl = await rolesModel.findOne({ _id: ObjectID(user.zenworkerId.roleId) });
                            console.log('acl', acl)
                            user.zenworkerId.roleId = acl
                            _id = user._id
                            companyId = user.companyId._id;
                        }
                        user.token = utils.generateJWTForUser(_id, companyId, zenworkerId, user.type);
                        let query_body = {
                            token: user.token,
                            type: user.type,
                            _id: String(user._id),
                            email: user.email,
                            companyId: JSON.stringify(user.companyId)
                            // companyId: user.companyId
                            // zenworkerId: String(user.zenworkerId)
                        }
                        if (user.zenworkerId) {
                            query_body["zenworkerId"] = JSON.stringify(user.zenworkerId)
                        }
                        // console.log("user", user)
                        console.log('query_body', query_body)
                        let login_redirect_url = url.format({
                            pathname: config.frontEndUrl + "/social",
                            // pathname: config.baseUrl + "/api/v1.0/users/social",
                            query: query_body
                        })
                        // console.log()
                        console.log(login_redirect_url)
                        res.redirect(login_redirect_url)
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }
            else {
                // body.signUpType = 'google';
                // body.password = utils.encryptPassword("test@123");

                // body.emailVerification = { status: true };
                // body.type = "company"
                // UserModel.create(body).then(response => {
                //     response.save(function (err, response) {
                //         if (err) {
                //             console.log(err);
                //             res.status(400).json({
                //                 status: false,
                //                 message: err,
                //                 data: null
                //             });
                //         } else {
                //             let data = response.toJSON();
                //             data = utils.formatUserData(response.toJSON());
                //             data.token = utils.generateJWTForUser(data);
                //             res.redirect(config.clientUrl + '/social/' + data._id)
                //         }
                //     });
                // }).catch(err => {
                //     console.log(err)
                //     res.status(400).json({ status: false, message: err, data: null });
                // });;
                let redirecturl = url.format({
                    pathname: config.frontEndUrl + "/self-signon-detail",
                    // pathname: config.baseUrl + "/api/v1.0/users/self-signon-detail",
                    query: body
                })
                console.log(redirecturl);
                res.redirect(redirecturl);
            }
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({ status: false, message: err, data: null });
        });
}

let getLoggedUserDetails = (req, res) => {
    return getUserDetails(req.jwt.userId, res);
}

let getOtherUserDetails = (req, res) => {
    return getUserDetails(req.params._id, res);
}


let getUserDetails = (_id, res) => {
    return UserModel.findOne({ _id })
        .populate('zenworkerId')
        .populate('companyId')
        .then((user) => {
            user = utils.formatUserData(user.toJSON());
            if (user.type == 'sr-support' || user.type == 'support' || user.type == 'billing') {
                user.zenworkerId = utils.formatZenworkerData(user.zenworkerId);
            } else if (user.type == 'company' || user.type == 'reseller') {
                user.companyId = utils.formatCompanyData(user.companyId);
            }
            res.status(200).json({ status: true, message: 'User Details fetched', data: user });
        }).catch((err) => {
            console.log(err);
            res.status(500).json({ status: false, message: 'User Not Found', data: null });
        });
}


let updateSelfPassword = (req, res) => {
    let _id = req.jwt.userId;
    var body = req.body;
    body.updatedDate = Date.now();
    return updatePassword(body.currentPassword, body.newPassword, _id, res);
}

let updateOtherUserPassword = (req, res) => {
    let _id = req.params._id;
    var body = req.body;
    body.updatedDate = Date.now();
    return updatePassword(body.currentPassword, body.newPassword, _id, res);
}


var updatePassword = (currentPassword, newPassword, userId, res) => {
    UserModel.findOne({ _id: userId })
        .then(user => {
            if (!user) {
                res.status(400).json({ status: false, message: "User not found", data: null });
            } else if (utils.encryptPassword(currentPassword) == user.password) {
                user.password = utils.encryptPassword(newPassword);
                user.save();
                res.status(200).json({ status: true, message: "Password updated", data: null });
            } else {
                res.status(400).json({ status: false, message: "Invalid current password", data: null });
            }
        }).catch(err => {
            res.status(500).json({ status: false, message: "Cannot update the password", data: null });
        });
}


let sendPasswordResetLInk = (req, res) => {
    let email = req.body.email;
    return UserModel.findOne({ email: { $regex: email.toLowerCase(), $options: "i"} })
        .populate('zenworkerId')
        .populate('companyId')
        .then(user => {
            if (!user) res.status(500).json({ status: false, message: 'User not found. Enter email linked to your Zenwork HR account', data: null });
            else {
                let passwordResetLink = utils.generatePasswordResetLinkForUser(user.email);
                var name;
                if (user.type == 'sr-support' || user.type == 'support' || user.type == 'billing') {
                    name = user.zenworkerId.name;
                } else if (user.type == 'company' || user.type == 'reseller') {
                    name = user.companyId.name;
                } else if (user.type == 'employee') {
                    // name = user.personal.name.firstName;
                    name = Object.values(user.personal.name).slice(0, Object.values(user.personal.name).length - 2 ).join(" ")
                }

                let to = user.email
                let from = config.mail.defaultFromAddress
                let subject = 'Zenwork HR - Password Reset'
                let email_body = {
                    logo_url: config.baseUrl + '/api/client/images/mainblacklogo.png',
                    reset_password_link: passwordResetLink,
                    name: name
                }
                let text = "ZenworkHR"
            
                fs.readFile(__dirname + '/reset_password_email.hbs', 'utf8', async (err, file_data) => {
                    if (!err) {
                        let html_data = (Handlebars.compile(file_data))(email_body)
                        // console.log(to, from, subject, text, html_data)
                        try {
                            await mailer.sendMail(to, from, subject, text, html_data)
                            res.status(200).json({ status: true, message: "Reset password link is sent to your email"});
                        }
                        catch (e) {
                            console.log(err)
                            res.status(400).json({
                                status: false,
                                message: "Error while sending email",
                                error: err
                            })
                        }
                    }
                    else {
                        console.log(err)
                        res.status(400).json({
                            status: false,
                            message: "Error while sending email",
                            error: err
                        })
                    }
                })  
            }
        }).catch(err => {
            console.log(err);
            res.status(500).json({ status: false, message: 'Internal server error', data: null });
        });
}

let resetPasswordWithToken = (req, res) => {
    let body = req.body
    let resetToken = body.token;
    return new Promise((resolve, reject) => {
        if (!body.token) reject('token is required')
        if (!body.newPassword) reject('newPassword is required')
        if (!helpers.isValidPasword(body.newPassword)) reject('Password must contain at least 6 characters')
        if (!body.confirmPassword) reject('confirmPassword is required')
        if (body.newPassword != body.confirmPassword) reject('Passwords do not match')
        resolve(null);
    })
    .then(() => {
        utils.decodePasswordResetToken(resetToken)
        .then(data => {
            let email = data.email;
            console.log(email);
            Promise.all([
                // UserModel.update({ email }, { password: req.body.password}),
                UserModel.update({ email }, { password: utils.encryptPassword(body.newPassword)}),
                UserModel.findOne({ email: email }).populate('zenworkerId').populate('companyId').lean().exec()
            ])
                .then(([response, user]) => {
                    console.log(response);
                    var name;
                    if (user.type == 'sr-support' || user.type == 'support' || user.type == 'billing') {
                        name = user.zenworkerId.name;
                    } else if (user.type == 'company' || user.type == 'reseller') {
                        name = user.companyId.name;
                    } else if (user.type == 'employee') {
                        name = Object.values(user.personal.name).slice(0, Object.values(user.personal.name).length - 2 ).join(" ");
                    }

                    let to = email
                    let from = config.mail.defaultFromAddress
                    let subject = 'Zenwork HR - Password Reset Successful'
                    let email_body = {
                        logo_url: config.baseUrl + '/api/client/images/mainblacklogo.png',
                        name: name
                    }
                    let text = "ZenworkHR"
                
                    fs.readFile(__dirname + '/reset_password_successful_email.hbs', 'utf8', async (err, file_data) => {
                        if (!err) {
                            let html_data = (Handlebars.compile(file_data))(email_body)
                            // console.log(to, from, subject, text, html_data)
                            try {
                                await mailer.sendMail(to, from, subject, text, html_data)
                                res.status(200).json({ status: true, message: "Password changed successfully"});
                            }
                            catch (e) {
                                console.log(err)
                                res.status(400).json({
                                    status: false,
                                    message: "Error while sending email",
                                    error: err
                                })
                            }
                        }
                        else {
                            console.log(err)
                            res.status(400).json({
                                status: false,
                                message: "Error while sending email",
                                error: err
                            })
                        }
                    })  

                    // mailer.sendPasswordResetSuccessIntimationToUser(email, name);
                    // res.status(200).send({ status: true, message: "Password changed successfully"});
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).send({ status: false, message: "Intenal server error", error: err });
                });
        }).catch(err => {
            console.log(err)
            res.status(500).send({ status: false, message: "Intenal server error", error: err });
        });
    })
    .catch(err => {
        console.log(err)
        res.status(400).send({ status: false, message: err});
    });
    
}

let getAll = (req, res) => {

    let queryParams = req.query;

    var searchQuery = req.query.searchQuery || '';
    var type = req.query.type;


    return new Promise((resolve, reject) => {
        // if (!body.name || !body.name.first || !body.name.last) reject("invalid name field");
        // else if (!body.email || !helpers.isValidEmail(body.email)) reject("invalid email");
        // else if (!body.password || !helpers.isValidPasword(body.password)) reject("invalid password");
        // else if(!body.mobile || !helpers.isValidPhoneNumber(body.mobile.number))reject("invalid Phone number");
        // else if (body.role === "admin") reject('User type admin cannot be allowed');
        // else
        resolve(null);
    }).then(() => {
        let options = {};
        if (queryParams && queryParams.requiredFields) {
            queryParams.requiredFields = JSON.parse(queryParams.requiredFields);
            options = queryParams.requiredFields;
            options.createdDate = 1;
            options.updatedDate = 1;
        }

        let sortOptions = {};
        if (queryParams && queryParams.sort) {
            sortOptions = JSON.parse(queryParams.sort);
        }

        let perPage = queryParams.perPage ? parseInt(queryParams.perPage) : 20;
        let pageNumber = queryParams.pageNumber ? parseInt(queryParams.pageNumber) : 1;

        let findQuery;

        if (searchQuery) {
            if (!findQuery) findQuery = {};
            findQuery['email'] = new RegExp(searchQuery, 'i');
        }

        if (type) {
            if (!findQuery) findQuery = {};
            findQuery['type'] = type;
        }

        console.log("findQuery", findQuery);
        console.log("sortOptions", sortOptions);
        console.log("options", options);

        var aggregatePipes = [];
        if (findQuery) {
            aggregatePipes.push(
                {
                    $match: findQuery
                }
            );
        }

        var aggregatePipeForCount = [];

        aggregatePipes.forEach(pipe => {
            aggregatePipeForCount.push(pipe);
        });
        aggregatePipeForCount.push(
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 }
                }
            }
        )

        aggregatePipes.push(
            {
                $skip: perPage * (pageNumber - 1)
            }
        );
        aggregatePipes.push(
            {
                $limit: perPage
            },
        );

        if (queryParams.requiredFields) {
            aggregatePipes.push(
                {
                    $project: options
                },
            );
        }
        return Promise.all([
            UserModel.aggregate(aggregatePipes),
            UserModel.aggregate(aggregatePipeForCount),
        ]);
    }).then(([users, countJSON]) => {
        var count = 0;
        if (countJSON && countJSON.length > 0) count = countJSON[0].count;
        res.status(200).json({ status: true, message: "Users data Fetched", data: users, count });
    }).catch((err) => {
        console.log(err);
        if (typeof err == 'object') {
            res.status(500).json({ status: false, message: "Internal server error", data: null });
        } else {
            res.status(400).json({ status: false, message: err, data: null });
        }
    });

}


var addNewUser = (data) => {
    if (/*data.email && */data.password) {
        return UserModel.create(data);
    } else {
        return Promise.reject('Invalid data');
    }
}

var addNewEmployee = (data) => {
    let body = {
        email: data.contact.workMail,
        password: data.password,
        type: 'employee'
    }
    if (body.email && body.password) {
        return UserModel.create(body);
    } else {
        // console.log(data.contact.workMail , data.password)
        return Promise.reject('Invalid data');
    }
}

let deleteUser = (req, res) => {
    let userId = req.params._id;
    return new Promise((resolve, reject) => {
        if (!userId) reject("Enter userId");
        resolve(null);
    }).then(() => {
        return CompaniesModel.findOne({ userId: userId });
    }).then(company => {
        // console.log(userId,company);
        if (company) {
            let companyId = company._id;
            OrdersModel.remove({ clientId: companyId }).exec();
            CompanySettingsModel.remove({ companyId: companyId }).exec();
            OnboardingModel.remove({ companyId: companyId }).exec();
            CompaniesModel.remove({ _id: companyId }).exec();
            UserModel.remove({ _id: userId }).exec();
            UserModel.remove({ resellerId: userId }).exec();
            UserModel.remove({ companyId: companyId }).exec();
            res.status(200).json({ status: true, message: "Successfully deleted" });
        } else {
            res.status(404).json({ status: false, message: "Data Not Found" });
        }
    }).catch((err) => {
        console.log(err);
        if (typeof err == 'object') {
            res.status(500).json({ status: false, message: "Internal server error", data: null });
        } else {
            res.status(400).json({ status: false, message: err, data: null });
        }
    });
};


/**
*@author Asma
*@date 08/05/2019 12:19
API to update user details
*/
let updateUser = (req, res) => {

    let body = req.body
    let user_id = req.body._id
    delete body._id
    delete body.password
    delete body.createdAt
    delete body.updatedAt
    delete body.userId
    delete body.type
    return new Promise((resolve, reject) => {
        if (!user_id) reject('user _id is required')
        if (body.personal && body.personal.languages && body.personal.languages.length) {
            let languages = body.personal.languages.map(x => x.language)
            if(languages.length != new Set(languages).size) {
                reject('languages must be unique');
            }
            else { resolve(null) }
        }
        resolve(null);
    })
        .then(() => {
            let update_filter = {
                _id: ObjectID(user_id)
            }
            UserModel.findOneAndUpdate(
                update_filter,
                { $set: body },
                {
                    new: true
                }
            )
                .then(response => {
                    res.status(200).json({
                        status: true,
                        message: "Successsfully updated user",
                        data: response
                    });
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).json({
                        status: false,
                        message: err
                    })
                });
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err
            })
        });
}

/**
*@author Asma
*@date 15/05/2019 08:43
API to get other user details
*/
let getOtherUser = (req, res) => {
    let userId = req.params.user_id
    let companyId = req.params.company_id
    // let body = req.body
    return new Promise((resolve, reject) => {
        resolve(null);
    })
        .then(() => {
            let user_filter = {
                _id: userId,
                companyId: companyId
            }
            // For pagination in My info Job Tab
            // let e_s_skip = 0, j_i_skip = 0, c_c_skip = 0, c_h_skip = 0;
            // let e_s_limit = 5, j_i_limit = 5, c_c_limit = 5, c_h_limit = 5;
            // if (body.employment_status_history) {
            //     let e_s_skip = (page_no - 1) * per_page
            // }
            // let user_projection = {
            //     "job.employment_status_history": { $slice: [e_s_skip, e_s_limit] },
            //     "job.job_information_history": { $slice: [j_i_skip, j_i_limit] },
            //     "compensation.current_compensation": { $slice: [c_c_skip, c_c_limit] },
            //     "compensation.compensation_history": { $slice: [c_h_skip, c_h_limit] }
            // }
            // UserModel.findOne(user_filter, user_projection).lean().exec()
            UserModel.findOne(user_filter).populate('job.job_information_history.ReportsTo', 'personal.name').populate('job.jobTitle').populate('job.job_information_history.jobTitle').populate('compensation.current_compensation.Pay_group').lean().exec()
                .then(async user => {
                    if (user) {
                        user = utils.formatUserData(user);
                        // user = user.toJSON()
                        console.log(user)
                        console.log(user.personal.profile_pic_details)
                        if (user.personal.profile_pic_details) {
                            let user_profile_pic_details_with_url = await s3.getPresignedUrlOfAfile('zenworkhr-docs', 'profile_photos', user.personal.profile_pic_details)
                            user.personal.profile_pic_details = user_profile_pic_details_with_url
                            console.log(user.personal.profile_pic_details)
                        }
                        if (user.job.employment_status_history) {
                            user.job.employment_status_history = user.job.employment_status_history.reverse();
                        }
                        if (user.job.job_information_history) {
                            user.job.job_information_history = user.job.job_information_history.reverse();
                        }
                        if (user.compensation.current_compensation) {
                            user.compensation.current_compensation = user.compensation.current_compensation.reverse()
                        }
                        if (user.compensation.compensation_history) {
                            user.compensation.compensation_history = user.compensation.compensation_history.reverse()
                        }

                        if(user.job && user.job.jobTitle && user.job.jobTitle._id) {
                            let jobSalaryLink = await JobSalaryLinkModel.findOne({companyId: companyId, jobTitle: user.job.jobTitle._id}).lean().exec()
                            user.compensation.Salary_Grade = jobSalaryLink.salary_grade
                            let grade_band = await GradeBandsModel.findOne({companyId: companyId, salary_grade: jobSalaryLink.salary_grade, _id: jobSalaryLink.grade_band}).lean().exec()
                            user.compensation.grade_band = grade_band
                        }
                        
                        res.status(200).json({
                            status: true,
                            message: "User fetched successfully",
                            data: user
                        })
                    }
                    else {
                        res.status(400).json({
                            status: true,
                            message: "User not found",
                            data: null
                        });
                    }
                })
                .catch(err => {
                    console.log(err)
                    res.status(400).json({
                        status: true,
                        message: "Error while fetching user",
                        error: err
                    });
                })
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: true,
                message: err,
                data: null
            });
        })
}

let temp = (req, res) => {
    console.log(req.query)
    res.send(req.query)
}

let temp2 = (req, res) => {
    let query = req.query
    console.log(query)
    console.log(query.companyId)
    console.log(JSON.parse(query.companyId))
    console.log(typeof JSON.parse(query.companyId))
    let companyIDJSON = JSON.parse(query.companyId)
    console.log(companyIDJSON._id)
    res.send(JSON.parse(query.companyId))

}

module.exports = {
    signup,
    login,
    getLoggedUserDetails,
    getOtherUserDetails,
    updateSelfPassword,
    updateOtherUserPassword,
    sendPasswordResetLInk,
    resetPasswordWithToken,
    getAll,
    addNewUser,
    addNewEmployee,
    deleteUser,



    /**
    *@author Asma
    *@date 08/05/2019 12:12
    
    */
    updateUser,
    getOtherUser,
    loginGoogle,
    temp,
    temp2
}
