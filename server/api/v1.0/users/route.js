let express = require('express');

let router = express.Router();

let config = require('../../../config/config');

let passport = require('passport');

let GoogleStrategy = require("passport-google-oauth2").Strategy;

let authMiddleware = require('./../middlewares/auth');

let UsersController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

require('https').globalAgent.options.rejectUnauthorized = false;

router.get(
    "/self-signon-detail",
    UsersController.temp
);

router.get(
    "/social",
    UsersController.temp2
);

router.post('/signup', validationsMiddleware.reqiuresBody, UsersController.signup);

router.post('/login', validationsMiddleware.reqiuresBody, UsersController.login);

router.put('/password/', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, UsersController.updateSelfPassword);

router.put('/password/:_id', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, UsersController.updateOtherUserPassword);

router.get('/', authMiddleware.isUserLogin, UsersController.getLoggedUserDetails);

router.get('/all', authMiddleware.isUserLogin, UsersController.getAll);

router.get('/:_id', authMiddleware.isUserLogin, authMiddleware.isAdmin, UsersController.getOtherUserDetails);

router.post('/passwordresetlink', validationsMiddleware.reqiuresBody, UsersController.sendPasswordResetLInk);

router.post('/resetpasswordwithtoken', validationsMiddleware.reqiuresBody, UsersController.resetPasswordWithToken);

router.delete('/delete/:_id', authMiddleware.isUserLogin, validationsMiddleware.reqiuresBody, UsersController.deleteUser);//not for regular use, only for experts.

router.put(
    '/update',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    UsersController.updateUser
);

router.get(
    "/getOtherUser/:company_id/:user_id",
    authMiddleware.isUserLogin,
    UsersController.getOtherUser
);


//social logins

passport.serializeUser(function (user, cb) {
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});

router.use(passport.initialize());
router.use(passport.session());

passport.use(
    new GoogleStrategy(
        {
            clientID: config.google.clientId,
            clientSecret: config.google.clientSecret,
            callbackURL: config.baseUrl + "/api/v1.0/users/login/google/return",
        },

        function (accessToken, refreshToken, profile, cb) {
            console.log('google profile', profile)
            return cb(null, profile);
        }
    )
);

// http://localhost:3003/api/v1.0/users/login/google

router.get(
    '/login/google',
    passport.authenticate('google', { scope: [
        "email",
        "profile"
        // 'https://www.googleapis.com/auth/userinfo.email', 
        // 'https://www.googleapis.com/auth/userinfo.profile'
        // 'https://www.googleapis.com/auth/plus.login',
        // 'https://www.googleapis.com/auth/plus.profile.emails.read'
    ] })
);



router.get(
    '/login/google/return',
    passport.authenticate("google", { failureRedirect: "/" }),
    function (req, res) {
        UsersController.loginGoogle(req, res);
    }
);

module.exports = router;
