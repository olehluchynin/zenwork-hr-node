let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    email: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    type: { type: String, enum: Object.keys(constants.userTypes) },
    zenworkerId: { type: ObjectID, ref: 'zenworkers' },
    resellerId: { type: ObjectID, ref: 'users' },
    companyId: { type: ObjectID, ref: 'companies' },
    onboardedBy: { type: ObjectID, ref: 'users' },
    offboardedBy: { type: ObjectID, ref: 'users' },
    // employeeId: {
    //     type: String,
    //     unique: true,
    //     sparse: true

    // },
    status: { type: String, enum: ['active', 'inactive'], default: 'active' },
    // status: {type: Number, default: 0, enum: Object.keys(constants.activeStatus)},

    groups: [{ type: ObjectID, ref: 'users' }],
    googleId: String,
    signUpType: {
        type: String,
        // enum: ["manual", "google", "facebook"],
        enum: ["manual", "google"],
        default: "manual",
        required: true
    },
    personal: {
        name: { firstName: String, lastName: String, middleName: String, preferredName: String, salutation: String },
        dob: { type: Date },
        gender: { type: String },
        maritalStatus: { type: String },
        effectiveDate: { type: Date },
        veteranStatus: String,
        ssn: String,
        race: String,
        ethnicity: String,
        address: {
            street1: String,
            street2: String,
            city: String,
            state: String,
            zipcode: String,
            country: String
        },
        contact: {
            workPhone: String,
            extention: String,
            mobilePhone: String,
            homePhone: String,
            workMail: String,
            personalMail: String
        },
        social_links: {
            linkedin: String,
            twitter: String,
            facebook: String
        },
        education: [{
            university: String,
            degree: String,
            specialization: String,
            gpa: Number,
            start_date: Date,
            end_date: Date
        }],
        languages: [{
            language: String,
            level_of_fluency: {
                type: String
                // enum: ['Native proficiency', 'Elementary proficiency', 'Limited working proficiency','Full professional proficiency']
            }
        }],
        visa_information: [{
            visa_type: String,
            issuing_country: String,
            issued_date: Date,
            expiration_date: Date,
            status: String,
            notes: String
        }],
        profile_pic_details: {
            originalFilename: { type: String },
            s3Path: { type: String }
        }
    },

    job: {
        jobTitle: {type: ObjectID, ref: 'structures'},
        department: String,
        businessUnit: String,
        unionFields: String,
        location: String,
        ReportsTo: { type: ObjectID, ref: 'users' },
        // ============
        employeeId: {
            type: String,
            unique: true,
            sparse: true
        },
        employeeType: String,
        workHours: Number,
        hireDate: Date,
        reHireDate: Date,
        terminationDate: Date,
        terminationReason: String,
        terminationType: {
            type: String,
            enum: ['Involuntary', 'Voluntary']
        },
        // eligibleForRehire: Boolean,
        rehireStatus: {
            type: String
        },
        last_day_of_work: Date,
        off_boarding_template_assigned: {
            type: ObjectID, ref: 'offboarding_templates'
        },
        termination_comments: String,
        archiveStatus: {
            type: String,
            enum: ["active", "inactive"]
        },
        reportingLocation: String,
        FLSA_Code: String,
        // EEO_Job_Category: String,
        Site_AccessRole: { type: ObjectID, ref: 'roles' },
        UserRole: String,
        VendorId: String,
        Specific_Workflow_Approval: {
            type: String,
            enum: ['Yes', 'No'],
            default: 'No'
        },
        HR_Contact: { type: ObjectID, ref: 'users' },
        current_employment_status: {
            status: String,
            effective_date: Date,
            end_date: Date
        },
        employment_status_history: [
            {
                status: String,
                effective_date: Date,
                end_date: Date
            }
        ],
        job_information_history: [{
            jobTitle: {type: ObjectID, ref: 'structures'},
            department: String,
            businessUnit: String,
            unionFields: String,
            location: String,
            reportingLocation: String,
            remoteWork: {
                type: String,
                enum: ['Yes', 'No']
            },
            changeReason: String,
            notes: String,
            ReportsTo: { type: ObjectID, ref: 'users' },
            effective_date: Date,
            end_date: Date
        }]
    },

    compensation: {
        NonpaidPosition: Boolean,
        HighlyCompensatedEmployee: Boolean,
        // Pay_group: { type: ObjectID, ref: 'payroll_groups' },
        // Salary_Grade: { type: ObjectID, ref: 'salary_grades' },
        // grade_band: {type: ObjectID, ref: 'grade_bands'},
        // compensationRatio: Number,
        current_compensation: [new mongoose.Schema({
            effective_date: Date,
            // payRate: {
            //     amount: Number,
            //     unit: String
            // },
            payRate: {type: Number},
            payRateUnit: {type: String},
            payType: String,
            Pay_group: { type: ObjectID, ref: 'payroll_groups' },
            compensationRatio: Number,
            Pay_frequency: String,
            changeReason: String,
            notes: String
        }, {timestamps: true, versionKey: false})],
        compensation_history: [new mongoose.Schema({
            effective_date: Date,
            // payRate: {
            //     amount: Number,
            //     unit: String
            // },
            payRate: {type: Number, required: true},
            payRateUnit: {type: String},
            payType: String,
            Pay_group: { type: ObjectID, ref: 'payroll_groups' },
            compensationRatio: Number,
            Pay_frequency: String,
            changeReason: String,
            notes: String
        }, {timestamps: true, versionKey: false})]

    },

    module_settings: {
        eligible_for_benifits: Boolean,
        assign_leave_management_rules: Boolean,
        assign_performance_management: Boolean,
        assaign_employee_number: { type: String, enum: constants.idNumberType },
        onboarding_template: { type: ObjectID, ref: 'onboarding_templates' }
    },
    leave_eligibility_group_history :  [{
        leave_eligibility_id: { type: ObjectID, ref: 'leave_eligility_groups' }, 
        start_date: {type: Date, default: Date.now}, 
        end_date: Date
    }],
    leave_eligibility_ids: [{ type: ObjectID, ref: 'leave_eligility_groups' }],
    blackout_groups: [{type: ObjectID, ref: 'blackoutempgrp'}],
    position_work_schedule_id : { type: ObjectID, ref: 'Position_Work_Schedule' },
    createdBy: { type: ObjectID, ref: 'users' },
    updatedBy: { type: ObjectID, ref: 'users' },
    isEmployeeSpecific : { type: ObjectID, ref: 'Employee_Specific_Schedule' }
}, {
        timestamps: true,
        versionKey: false
    });
autoIncrement.initialize(mongoose.connection);
// Schema.plugin(autoIncrement.plugin, {model:'users', field:'employeeId', startAt: 1, incrementBy: 1});
Schema.plugin(autoIncrement.plugin, { model: 'users', field: 'userId', startAt: 1, incrementBy: 1 });

let employeeIdCounterSchema = new mongoose.Schema({
    seq: { type: Number, default: 10000 }
}, {
        timestamps: true,
        versionKey: false
    })

let employeeIdCounter = mongoose.model('employee_id_counter', employeeIdCounterSchema);

// Schema.pre('save', function (next) {
//     var doc = this;
//     if (doc.type === 'employee') {
//         if (doc.job && doc.job.employeeId) {
//             next();
//         }
//         else if (doc.job && !doc.job.employeeId) {
//             employeeIdCounter.count({}, function (error, count) {
//                 if (error) {
//                     return next(error);

//                 } else if (count > 0) {
//                     employeeIdCounter.findOneAndUpdate({}, { $inc: { seq: 1 } }, function (error, counter) {
//                         if (error)
//                             return next(error);

//                         doc.job.employeeId = "" + (counter.seq + 1);
//                         next();
//                     });
//                 } else {
//                     var employeeIdCount = new employeeIdCounter({ seq: 10000 });
//                     employeeIdCount.save(function (error, counter) {
//                         if (error)
//                             return next(error);
//                         doc.job.employeeId = "" + counter.seq;
//                         next();
//                     })
//                 }

//             });
//         }
//     }
//     else {
//         next();
//     }
// });

let UserModel = mongoose.model('users', Schema);
module.exports = UserModel;
