/**
 * @api {post} /manager-front-page/announcement/add AddManagerAnnouncement
 * @apiName AddManagerAnnouncement
 * @apiGroup ManagerFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"description": "Meeting at 5PM today",
	"title": "Meeting",
	"scheduled_date": "2019-06-27T06:13:49.909Z",
	"expiration_date": "2019-06-30T06:13:49.909Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Added manager announcement successfully",
    "data": {
        "_id": "5d145eb182a481122c16fa91",
        "companyId": "5c9e11894b1d726375b23058",
        "description": "Meeting at 5PM today",
        "title": "Meeting",
        "scheduled_date": "2019-06-27T06:13:49.909Z",
        "expiration_date": "2019-06-30T06:13:49.909Z",
        "createdAt": "2019-06-27T06:14:09.812Z",
        "updatedAt": "2019-06-27T06:14:09.812Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /manager-front-page/announcement/list/:company_id ListManagerAnnouncements
 * @apiName ListManagerAnnouncements
 * @apiGroup ManagerFrontPage
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Success",
    "data": [
        {
            "_id": "5d145eb182a481122c16fa91",
            "companyId": "5c9e11894b1d726375b23058",
            "description": "Meeting at 5PM today",
            "title": "Meeting",
            "scheduled_date": "2019-06-27T06:13:49.909Z",
            "expiration_date": "2019-06-30T06:13:49.909Z",
            "createdAt": "2019-06-27T06:14:09.812Z",
            "updatedAt": "2019-06-27T06:14:09.812Z"
        }
    ]
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {put} /manager-front-page/announcement/edit EditManagerAnnouncement
 * @apiName EditManagerAnnouncement
 * @apiGroup ManagerFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
    "_id": "5d145eb182a481122c16fa91",
    "companyId": "5c9e11894b1d726375b23058",
    "description": "Meeting at 6PM today",
    "title": "Meeting",
    "scheduled_date": "2019-06-27T06:13:49.909Z",
    "expiration_date": "2019-06-30T06:13:49.909Z",
    "createdAt": "2019-06-27T06:14:09.812Z",
    "updatedAt": "2019-06-27T06:14:09.812Z"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Manager announcement updated successfully",
    "data": {
        "_id": "5d145eb182a481122c16fa91",
        "companyId": "5c9e11894b1d726375b23058",
        "description": "Meeting at 6PM today",
        "title": "Meeting",
        "scheduled_date": "2019-06-27T06:13:49.909Z",
        "expiration_date": "2019-06-30T06:13:49.909Z",
        "createdAt": "2019-06-27T06:14:09.812Z",
        "updatedAt": "2019-06-27T06:25:49.472Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /manager-front-page/announcement/delete DeleteManagerAnnouncement
 * @apiName DeleteManagerAnnouncement
 * @apiGroup ManagerFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
	"_ids": [
		"5d14631282a481122c16fa92"
		]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /manager-front-page/settings/addOrUpdate AddOrUpdateSettings
 * @apiName AddOrUpdateSettings
 * @apiGroup ManagerFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "show_team_summary": true,
    "show_to_do_list": true,
    "show_todays_reports": true,
    "show_employment_type": false,
    "show_birthdays": false,
    "birthday_settings": "My Team Only",
    "show_work_anniversary": true,
    "work_anniversary_settings": "My Team Only",
    "show_manager_links": "true"
}
 * @apiParamExample {json} Request-Example2:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "show_team_summary": true,
    "show_to_do_list": true,
    "show_todays_reports": true,
    "show_employment_type": false,
    "show_birthdays": false,
    "birthday_settings": "My Team Only",
    "show_work_anniversary": true,
    "work_anniversary_settings": "My Team Only",
    "show_manager_links": "true",
    "manager_links": [
        {

        }
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Manager settings updated successfully",
    "data": {
        "_id": "5d146b7fc8f4768976d524f0",
        "companyId": "5c9e11894b1d726375b23058",
        "birthday_settings": "My Team Only",
        "createdAt": "2019-06-27T07:08:48.181Z",
        "show_birthdays": false,
        "show_employment_type": true,
        "show_manager_links": true,
        "show_team_summary": true,
        "show_to_do_list": true,
        "show_todays_reports": true,
        "show_work_anniversary": true,
        "updatedAt": "2019-06-27T07:09:19.023Z",
        "work_anniversary_settings": "My Team Only",
        "manager_links": []
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {get} /manager-front-page/settings/get/:company_id GetManagerSettings
 * @apiName GetManagerSettings
 * @apiGroup ManagerFrontPage
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParam {ObjectId} company_id Company unique _id
 *
 * @apiSuccessExample Success-Response1:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Fetched manager settings successfully",
    "data": {
        "_id": "5d146b7fc8f4768976d524f0",
        "companyId": "5c9e11894b1d726375b23058",
        "birthday_settings": "My Team Only",
        "createdAt": "2019-06-27T07:08:48.181Z",
        "show_birthdays": false,
        "show_employment_type": true,
        "show_manager_links": true,
        "show_team_summary": true,
        "show_to_do_list": true,
        "show_todays_reports": true,
        "show_work_anniversary": true,
        "updatedAt": "2019-06-27T07:09:19.023Z",
        "work_anniversary_settings": "My Team Only"
    }
}
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

//===============================================================================


/**
 * @api {post} /manager-front-page/links/add AddManagerLink
 * @apiName AddManagerLink
 * @apiGroup ManagerFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
[
    {
        "site_name": "instagram",
        "site_link": "instagram.com/flor"
    },
    {
        "site_name": "linkedin",
        "site_link": "linkedin.com/flor"
    }
]
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Manager link added successfully",
    "data": {
        "_id": "5d148bd2cd2e5c2e741d6432",
        "companyId": "5c9e11894b1d726375b23058",
        "manager_links": [
            {
                "_id": "5d148bd2cd2e5c2e741d6433",
                "site_name": "fb",
                "site_link": "facebook.com/flor"
            }
        ],
        "createdAt": "2019-06-27T09:26:42.907Z",
        "updatedAt": "2019-06-27T09:26:42.907Z"
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */


/**
 * @api {put} /manager-front-page/links/edit EditManagerLink
 * @apiName EditManagerLink
 * @apiGroup ManagerFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "linkId": "5d148d0ca4cab61f786baeac",
    "site_name": "fb",
    "site_link": "www.facebook.com/Slyvia"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Updated manager link successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {post} /manager-front-page/links/delete DeleteManagerLinks
 * @apiName DeleteManagerLinks
 * @apiGroup ManagerFrontPage
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "_ids": [
    	"5d148ff4a4cab61f786baead"
    	]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted links successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */