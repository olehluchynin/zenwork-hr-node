let express = require('express');

let router = express.Router();

let authMiddleware = require('../middlewares/auth');

let Controller = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.post(
    '/announcement/add',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.addAnnouncement
);

router.get(
    // '/announcement/list/:company_id/:user_id',
    '/announcement/list/:company_id',
    authMiddleware.isUserLogin,
    Controller.list
);

router.put(
    '/announcement/edit',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editMA
);

router.post(
    '/announcement/delete',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deleteMA
);

router.post(
    '/settings/addOrUpdate',
    authMiddleware.isUserLogin,
    authMiddleware.isCompany,
    validationsMiddleware.reqiuresBody,
    Controller.addOrUpdateSettings
);

router.get(
    // '/settings/get/:company_id/:user_id',
    '/settings/get/:company_id',
    authMiddleware.isUserLogin,
    Controller.getSettings
);

router.post(
    '/links/add',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.addLink
);

router.put(
    '/links/edit',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.editLink
);

router.post(
    '/links/delete',
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    Controller.deleteLinks
);



module.exports = router;