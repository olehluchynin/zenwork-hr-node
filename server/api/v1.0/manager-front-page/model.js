let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;

let ManagerAnnouncementsSchema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    // userId: {type: ObjectID, ref: 'users', required:true},
    createdBy: {type: ObjectID, ref: 'users'},
    updatedBy: {type: ObjectID, ref: 'users'},
    description: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    scheduled_date: {
        type: Date
    },
    expiration_date: {
        type: Date
    }

}, {
    timestamps: true,
    versionKey: false
})

let ManagerAnnouncementsModel = mongoose.model('manager_announcements', ManagerAnnouncementsSchema, 'manager_announcements');

let ManagerFrontPageSettingsSchema = new mongoose.Schema({
    companyId: {type: ObjectID, ref:'companies', required: true},
    // userId: {type: ObjectID, ref: 'users', required: true},
    createdBy: {type: ObjectID, ref: 'users'},
    updatedBy: {type: ObjectID, ref: 'users'},
    show_team_summary: { type: Boolean },
    show_to_do_list: { type: Boolean },
    show_todays_reports: { type: Boolean },
    show_employment_type: { type: Boolean },
    show_birthdays: { type: Boolean },
    birthday_settings: {type: String, enum: ['My Team Only', 'My Department Only', 'Entire Company']},
    show_work_anniversary: { type: Boolean },
    work_anniversary_settings: {type: String, enum: ['My Team Only', 'My Department Only', 'Entire Company']},
    show_manager_links: { type: Boolean },
    manager_links: [
        {
            site_name: {
                type:String
            },
            site_link: {
                type: String
            },
            createdBy: {type: ObjectID, ref: 'users'},
            updatedBy: {type: ObjectID, ref: 'users'}
        }
    ]

}, {
    timestamps: true,
    versionKey: false
})

// ManagerFrontPageSettingsSchema.index({userId: 1, companyId: 1}, {unique: true})
ManagerFrontPageSettingsSchema.index({companyId: 1}, {unique: true})

let ManagerFrontPageSettingsModel = mongoose.model('manager_front_page_settings', ManagerFrontPageSettingsSchema, 'manager_front_page_settings');


module.exports = {
    ManagerAnnouncementsModel,
    ManagerFrontPageSettingsModel
};