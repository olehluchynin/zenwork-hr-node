let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let config = require('../../../config/config');
let _ = require('underscore');
let ManagerAnnouncementsModel = require('./model').ManagerAnnouncementsModel;
let ManagerFrontPageSettingsModel = require('./model').ManagerFrontPageSettingsModel;

let addAnnouncement = (req,res) => {
    let body = req.body
    return new Promise((resolve,reject) => {
        resolve(null);
    })
    .then(() => {
        body.createdBy =  ObjectID(req.jwt.userId)
        ManagerAnnouncementsModel.create(body)
            .then(ManagerAnnouncement => {
                ManagerAnnouncement.save(function (err, response) {
                    if (!err) {
                        res.status(200).json({
                            status: true,
                            message: "Added manager announcement successfully",
                            data: response
                        })
                    }
                    else {
                        console.log(err)
                        res.status(400).json({
                            status: false,
                            message: "Error while adding manager announcement",
                            error: err
                        })
                    }
                })
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while adding manager announcement",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    });
}

let list = (req, res ) => {
    let companyId = req.params.company_id
    // let userId = req.params.user_id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        let sort = {
            updatedAt : -1
        }
        let filter = {
            companyId: ObjectID(companyId),
            // userId : ObjectID(userId)
        }
        ManagerAnnouncementsModel.find(filter)
            .sort(sort)
            .lean().exec()
            .then(MngrAnncmnts => {
                res.status(200).json({
                    status: true, message: "Successfully fetched list of manager announcements", data: MngrAnncmnts
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while fetching manager announcements",
                    error: err
                })
            });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let editMA = (req,res) => {
    let body = req.body
    let ma_id = body._id
    let companyId = body.companyId
    // let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!ma_id) {
            reject('_id is required')
        }
        else if (!companyId) {
            reject('companyId is required')
        }
        // else if (!userId) {
        //     reject('userId is required')
        // }
        else {
            resolve(null);
        }
    })
    .then(() => {
        delete body.companyId
        // delete body.userId
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        console.log(body)
        let filter = {
            _id: ObjectID(ma_id), 
            companyId : ObjectID(companyId), 
            // userId: ObjectID(userId)
        }
        console.log(filter)
        body.updatedBy = ObjectID(req.jwt.userId)
        ManagerAnnouncementsModel.findOneAndUpdate(filter, {$set: body}, {new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                res.status(200).json({
                    status: true,
                    message: "Manager announcement updated successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while updating manager announcement",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    });
}

let deleteMA = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    // let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        // else if (!userId) reject('userId is required');
        else resolve(null);
    })
    .then(() => {
        ManagerAnnouncementsModel.deleteMany({
            companyId: companyId,
            // userId: userId,
            _id: {$in : body._ids}
        })
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting manager announcements",
                error: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let getSettings = (req, res) => {
    let companyId = req.params.company_id
    // let userId = req.params.user_id
    // let MS_id = req.params._id
    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        let find_query = {
            // _id: ObjectID(MS_id),
            // userId: ObjectID(userId),
            companyId: ObjectID(companyId)
        }
        ManagerFrontPageSettingsModel.findOne(find_query)
            .then(manager_settings => {
                if(manager_settings) {
                    if (manager_settings.manager_links) {
                        manager_settings.manager_links = manager_settings.manager_links.reverse();
                    }
                    res.status(200).json({
                        status: true,
                        message: "Fetched manager settings successfully",
                        data: manager_settings
                    })
                }
                else {
                    res.status(404).json({
                        status: false,
                        message: "Manager settings not found",
                        data: null
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(400).json({
                    status: false,
                    message: "Error while fecthing manager settings",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err);
        res.status(400).json({
            status: false,
            message: "Error while fecthing manager settings",
            error: err
        })
    })
}

let addOrUpdateSettings = (req,res) => {
    let body = req.body
    // let ms_id = body._id
    let companyId = body.companyId
    // let userId = body.userId
    
    return new Promise((resolve, reject) => {
        // if (!ms_id) {
        //     reject('_id is required')
        // }
        if (!companyId) {
            reject('companyId is required')
        }
        // else if (!userId) {
        //     reject('userId is required')
        // }
        else {
            resolve(null);
        }
    })
    .then(() => {
        // delete body.companyId
        // delete body.userId
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        console.log(body)
        let filter = {
            // _id: ObjectID(ms_id), 
            companyId : ObjectID(companyId), 
            // userId: ObjectID(userId)
        }
        console.log(filter)
        if (!body._id) {
            body.createdBy =  ObjectID(req.jwt.userId)
        }
        else {
            body.updatedBy = ObjectID(req.jwt.userId)
        }
        ManagerFrontPageSettingsModel.findOneAndUpdate(filter, {$set: body}, {upsert: true, new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                res.status(200).json({
                    status: true,
                    message: "Manager settings updated successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while updating manager settings",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    });
}

//can add multiple - addLinks
let addLink = (req, res) =>{
    let body = req.body
    let companyId = req.jwt.companyId
    // let userId = body.userId

    return new Promise((resolve, reject) => {
        resolve(null);
    })
    .then(() => {
        
        let filter = {
            companyId : ObjectID(companyId), 
            // userId: ObjectID(userId)
        }
        body = body.map(x => {
            x.createdBy = ObjectID(req.jwt.userId)
            return x
        })
        ManagerFrontPageSettingsModel.findOne(filter)
            .then(settings => {
                if (settings) {
                    settings.manager_links = settings.manager_links.concat(body)
                    settings.save((err, response) => {
                        if (!err) {
                            res.status(200).json({
                                status: true,
                                message: "Manager links added successfully",
                                data: response
                            })
                        }
                        else {
                            console.log(err);
                            res.status(400).json({
                                status: false,
                                message: "Error while adding manager links",
                                error: err
                            })
                        }
                    })
                }
                else {
                    ManagerFrontPageSettingsModel.create({
                        companyId: companyId,
                        manager_links: body
                    })
                    .then(response => {
                        res.status(200).json({
                            status: true,
                            message: "Manager links added successfully",
                            data: response
                        })
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(400).json({
                            status: false,
                            message: "Error while adding manager links",
                            error: err
                        })
                    })
                }
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({
                    status: false,
                    message: "Error while adding manager links",
                    error: err
                })
            })
    })
    .catch(err => {
        console.log(err);
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let editLink = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    // let userId = body.userId
    let linkId = body.linkId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        // else if (!userId) reject('userId is required');
        else if (!linkId) reject('linkId is requried')
        else resolve(null);
    })
    .then(() => {
        delete body.companyId
        // delete body.userId
        delete body.linkId
        body.updatedBy = ObjectID(req.jwt.userId)
        let update_filter = {
            companyId: companyId,
            // userId: userId,
            "manager_links._id" : linkId
        }
        console.log(body)
        var updateObj = {};
        for (let key in body) {
            updateObj['manager_links.$.' + key] = body[key]
        }
        console.log(updateObj)

        ManagerFrontPageSettingsModel.updateOne(
            update_filter, 
            {
                $set: updateObj
            }, 
            { 
                new: true
            }
        )
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Updated manager link successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while updating manager link",
                error : err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

let deleteLinks = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    // let userId = body.userId
    return new Promise((resolve, reject) => {
        if (!companyId) reject('companyId is required');
        // else if (!userId) reject('userId is required');
        else resolve(null);
    })
    .then(() => {
        let update_filter = {
            companyId: companyId,
            // userId: userId
        }
        ManagerFrontPageSettingsModel.updateOne(update_filter, {
            $pull: {
                'manager_links' : {_id: {$in : body._ids}}
            }
        }, {new: true})
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Deleted links successfully",
                data: response
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: "Error while deleting links",
                error : err
            })
        });
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

module.exports = {
    addAnnouncement,
    list,
    editMA,
    deleteMA,
    addOrUpdateSettings,
    getSettings,
    addLink,
    editLink,
    deleteLinks
}


// ManagerFrontPageSettingsModel.findOneAndUpdate(
//     filter,
//     {
//         $set: {
//             'manager_links' : body
//         }
//     },
//     {
//         new: true
//     }
// )
// .then(response => {
//     res.status(200).json({
//         status: true,
//         message: "Manager links added successfully",
//         data: response
//     })
// })
// .catch(err => {
//     console.log(err);
//     res.status(400).json({
//         status: false,
//         message: "Error while adding manager links",
//         error: err
//     })
// })