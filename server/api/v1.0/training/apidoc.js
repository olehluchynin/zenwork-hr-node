/**
 * @apiDefine DataNotFoundError
 *
 * @apiError DataNotFound The data the you are requested was not found.
 *
 * @apiErrorExample Data Not Found Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "message": "Data Not Found"
 *       "data": null
 *     }
 */

/**
 * @apiDefine InternalServerError
 *
 * @apiError InternalServerError Internal Server error.
 *
 * @apiErrorExample Internal Server Error:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "status": false,
 *       "message": "Internal server error"
 *       "data": null
 *     }
 */

/**
 * @apiDefine TokenNotFound
 *
 * @apiError TokenNotFound User Token Not Found.
 *
 * @apiErrorExample Token Not Found:
 *     HTTP/1.1 400 Token Not Found
 *     {
 *       "status": false,
 *       "message": "Token Not Found"
 *       "data": null
 *     }
 */

/**
 * @apiDefine BadRequest
 *
 * @apiError BadRequest Bad request.
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": false,
 *       "message": "Invalid key"
 *       "data": null
 *     }
 */

/**
 * @apiDefine AccessDeneid
 *
 * @apiError AccessDenied Access denied for this operation.
 *
 * @apiErrorExample Access Deneid:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "status": false,
 *       "message": "Access Denied for this operation"
 *       "data": null
 *     }
 */

/**
 * @apiDefine DataAlreadyExists
 *
 * @apiError DataConflict Date already exists.
 *
 * @apiErrorExample Data Conflict Error:
 *     HTTP/1.1 402 Data Conflict
 *     {
 *       "status": false,
 *       "message": "Data already exist"
 *       "data": null
 *     }
 */


/**
 * @api {post} /training/add Add Training Data
 * @apiName addTrainingData
 * @apiGroup Training
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
 *
 *{
	"trainingName":"xyz",
	"trainingId":"xyz",
	"trainingSubject":"xyz",
	"trainingType":"xyz",
	"timeToComplete":"xyz",
    "trainingUrl":"xyz",
    "status": 1
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Traning added successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */



/**
 * @api {get} /training/getSingleData/:_id Get Single Training Record
 * @apiName getSingleTrainingRecord
 * @apiGroup Training
 * @apiHeader {String} x-access-token User login token.

 * @apiParam {String} _id Record unique _id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Successfully",
    "data": {
        "status": 0,
        "_id": "5ca44376a032e00926aba2ca",
        "trainingName": "Sureshhhhh",
        "trainingId": "654654",
        "trainingSubject": "professional",
        "trainingType": "instructor led",
        "timeToComplete": "1 day",
        "trainingUrl": "zcxcbvnbn",
        "companyId": "5c9e11894b1d726375b23058",
        "createdBy": "5c9e11894b1d726375b23057",
        "updatedBy": "5c9e11894b1d726375b23057",
        "createdAt": "2019-04-03T05:24:06.325Z",
        "updatedAt": "2019-04-03T05:24:06.325Z",
        "trainingSerialNumber": 6
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */


/**
 * @api {put} /training/update Update Training data
 * @apiName updateTrainingData
 * @apiGroup Training
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
 *
 *{
        "status": 0,
        "_id": "5ca44376a032e00926aba2ca",
        "trainingName": "Sureshhhhh",
        "trainingId": "654654",
        "trainingSubject": "professional",
        "trainingType": "instructor led",
        "timeToComplete": "1 day",
        "trainingUrl": "zcxcbvnbn",
        "companyId": "5c9e11894b1d726375b23058",
        "createdBy": "5c9e11894b1d726375b23057",
        "updatedBy": "5c9e11894b1d726375b23057",
        "createdAt": "2019-04-03T05:24:06.325Z",
        "updatedAt": "2019-04-03T05:24:06.325Z"
    }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Data updated successfully"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */



/**
 * @api {get} /training/getAll Get All
 * @apiName GetAllTrainingData
 * @apiGroup Training
 *
 * @apiHeader {String} x-access-token User login token.
 * @apiParam {String} searchQuery(queryParam) search query string.
 * @apiParam {String} trainingSubject(queryParam) Training Subject for filter.
 * @apiParam {String} trainingType(queryParam) Training Type for filter
 * @apiParam {String} timeToComplete(queryParam) Time To Complete for filter
 * @apiParam {String} status(queryParam) status for filter (0,1).
 * @apiParam {Boolean} archive(queryParam) archive for filter (true, false).
 * @apiParam {Number} perPage(queryParam) records count per page.
 * @apiParam {Number} pageNumber(queryParam) page number
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Training data Fetched",
    "data": [
        {
            "_id": "5ca44376a032e00926aba2ca",
            "status": 0,
            "trainingName": "Sureshhhhh",
            "trainingId": "654654",
            "trainingSubject": "professional",
            "trainingType": "instructor led",
            "timeToComplete": "1 day",
            "trainingUrl": "zcxcbvnbn",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5c9e11894b1d726375b23057",
            "updatedBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-04-03T05:24:06.325Z",
            "updatedAt": "2019-04-03T05:24:06.325Z",
            "trainingSerialNumber": 6
        },
        {
            "_id": "5ca44434a032e00926aba2cb",
            "status": 0,
            "trainingName": "sample",
            "trainingId": "32121",
            "trainingSubject": "security",
            "trainingType": "reading",
            "timeToComplete": "1 hour",
            "trainingUrl": "fgtiyuoirtuy",
            "companyId": "5c9e11894b1d726375b23058",
            "createdBy": "5c9e11894b1d726375b23057",
            "updatedBy": "5c9e11894b1d726375b23057",
            "createdAt": "2019-04-03T05:27:16.114Z",
            "updatedAt": "2019-04-03T05:27:16.114Z",
            "trainingSerialNumber": 7
        }
    ],
    "count": 3
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */


/**
 * @api {delete} /training/:_id Delete Single Record
 * @apiName deleteSingleTrainingRecord
 * @apiGroup Training
 * @apiHeader {String} x-access-token User login token.

 * @apiParam {String} _id Record unique _id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 {
    "status": true,
    "message": "Successfully Removed"
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */


/**
 * @api {post} /training/assignTraining AssignTrainingToEmployees
 * @apiName AssignTrainingToEmployees
 * @apiGroup Training
 *
 * @apiHeader {String} x-access-token User login token.
 *
 * @apiParamExample {json} Request-Example:
 *
{
    "companyId": "5c9e11894b1d726375b23058",
    "trainingId": "5cdb96d78ed19a301ececd1b",
    "_ids": [
        "5cd93e4a8e9dea1f4bb987dc",
        "5cd92313f308c21c43e50599"
    ]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully assigned trainings",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */

/**
 * @api {post} /training/deleteMany DeleteTrainings
 * @apiName DeleteTrainings
 * @apiGroup Training
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "_ids": [
    	"5cdc1a9a6c7a7817c4c2e4fe"
    	]
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Deleted successfully",
    "data": {
        "n": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {put} /training/changeStatus changeStatus
 * @apiName changeStatus
 * @apiGroup Training
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
{
	"companyId": "5c9e11894b1d726375b23058",
    "_ids": [
        "5cef5b96e331715f9bc0ea34",
        "5cef5b96e331715f9bc0ea35"
    ],
    "archive": true
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Changed status successfully",
    "data": {
        "status": 0,
        "archive": true,
        "_id": "5cef5b96e331715f9bc0ea34",
        "trainingName": "Suresh11",
        "trainingId": "23456",
        "instructorName": "5cd91e68f308c21c43e50559",
        "trainingSubject": "New one",
        "trainingType": "Video",
        "timeToComplete": "1 hour",
        "trainingUrl": "dfkjgljlkh",
        "companyId": "5c9e11894b1d726375b23058",
        "createdBy": "5c9e11894b1d726375b23057",
        "updatedBy": "5c9e11894b1d726375b23057",
        "createdAt": "2019-05-30T04:27:02.072Z",
        "updatedAt": "2019-08-20T06:40:38.396Z",
        "trainingSerialNumber": 30
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */

/**
 * @api {get} /training/unarchive Unarchive
 * @apiName Unarchive
 * @apiGroup Training
 * 
 *  
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Unarchived successfully",
    "data": {
        "n": 1,
        "nModified": 1,
        "ok": 1
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */