let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    trainingName: {type:String},
    trainingId: {type:String},
    trainingUrl: {type:String},
    trainingSubject: {type:String},
    trainingType: {type:String},
    timeToComplete: {type:String},
    status: {type: Number, default:0, enum: Object.keys(constants.activeStatus)},
    instructorName: {type:ObjectID,ref:'users'},
    companyId: {type:ObjectID,ref:'companies'},
    createdBy: {type:ObjectID,ref:'users'},
    updatedBy:{type:ObjectID,ref:'users'},
    archive: {
        type: Boolean,
        default: false
    }
},{
    timestamps: true,
    versionKey: false
});

Schema.plugin(autoIncrement.plugin, {model:'training', field:'trainingSerialNumber', startAt: 1, incrementBy: 1});

Schema.index({companyId: 1, trainingName: 1}, {unique: true})

Schema.index({companyId: 1, trainingId: 1}, {unique: true})

let TrainingModel = mongoose.model('training',Schema);

module.exports = TrainingModel;
