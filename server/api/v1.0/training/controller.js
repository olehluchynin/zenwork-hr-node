let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;
let async = require('async')
let TrainingModel = require('./model');
let AssignedTrainingsModel = require('./../assigned-trainings/model');
let utils = require('./../../../common/utils');
let mailer = require('./../../../common/mailer');
let constants = require('./../../../common/constants');

/**
 * Author:Sai Reddy  Date:03/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-9
 * Login API To Add Training Library
 */
let add = (req, res) => {
    var body = req.body;
    return new Promise((resolve, reject) => {
        if (!body.trainingName) reject("Please enter Training Name.");
        else if (!body.trainingId) reject("Please enter Training Id.");
        else if (!body.trainingSubject) reject("Please enter Training Subject.");
        else if (!body.trainingType) reject("Please enter Training Type.");
        else if (!body.timeToComplete) reject("Please enter Training Completing time.");
        else if (!body.trainingUrl) reject("Please enter Training URL.");
        else resolve(
                Promise.all([
                    TrainingModel.findOne({companyId: req.jwt.companyId, trainingName: body.trainingName}),
                    TrainingModel.findOne({companyId: req.jwt.companyId, trainingId: body.trainingId})
                ])
            );
    }).then(([training_with_name, training_with_id]) => {
        if (training_with_name) {
            throw 'Training already exists with this name'
        }
        else if (training_with_id) {
            throw 'Training already exists with this TrainingID'
        }
        else {
            body.companyId = req.jwt.companyId;
            body.createdBy = req.jwt.userId;
            body.updatedBy = req.jwt.userId;
            return TrainingModel.create(body)
        }
    }).then(data => {
        res.status(200).json({status: true, message: "training created successfully"});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:03/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-9
 * Login API To GET Single Training Library data
 */
let getSingleData = (req, res) => {
    var id = req.params._id;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return TrainingModel.findOne({_id:id, companyId: req.jwt.companyId}).exec();
    }).then(data => {
        res.status(200).json({status: true, message: "Successfully", data:data});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:03/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-9
 * Login API To Update Training Library data
 */
let update = (req, res) => {
    var body = req.body;
    return new Promise((resolve, reject) => {
        if (!body.trainingName) reject("Please enter Training Name.");
        else if (!body.trainingId) reject("Please enter Training Id.");
        else if (!body.trainingSubject) reject("Please enter Training Subject.");
        else if (!body.trainingType) reject("Please enter Training Type.");
        else if (!body.timeToComplete) reject("Please enter Training Completing time.");
        else if (!body.trainingUrl) reject("Please enter Training URL.");
        else resolve(
            Promise.all([
                TrainingModel.findOne({companyId: req.jwt.companyId, trainingName: body.trainingName, _id: {$ne: body._id}}),
                TrainingModel.findOne({companyId: req.jwt.companyId, trainingId: body.trainingId, _id: {$ne: body._id}})
            ])
        );
    }).then(([training_with_name, training_with_id]) => {
        if (training_with_name) {
            throw 'Training already exists with this name'
        }
        if (training_with_id) {
            throw 'Training already exists with this TrainingID'
        }
        delete body.trainingSerialNumber;
        body.updatedBy = req.jwt.userId;
        return TrainingModel.update({_id:body._id},body).exec();
    }).then(data => {
        res.status(200).json({status: true, message: "Data updated successfully"});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:03/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-9
 * Login API To GET ALL Training Library Data of a particular company
 */
let getAll = (req, res) => {

    let queryParams = req.query;
    console.log(queryParams);

    var companyId = req.jwt.companyId;
    var searchQuery = req.query.searchQuery || '';

    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let findQuery = {companyId:mongoose.Types.ObjectId(companyId)};

        let sortOptions = {};
        if(queryParams && queryParams.sort){
            sortOptions = JSON.parse(queryParams.sort);
        }

        let pageNumber = queryParams.pageNumber !== undefined ? parseInt(queryParams.pageNumber):1;
        let perPage = queryParams.perPage !== undefined ? parseInt(queryParams.perPage):10;

        if(searchQuery){
            if(!findQuery) findQuery = {};
            findQuery['$or']= [
                {trainingName:new RegExp(searchQuery,'i')},
                {trainingId:new RegExp(searchQuery,'i')},
                {trainingSubject:new RegExp(searchQuery,'i')},
                {trainingType:new RegExp(searchQuery,'i')},
                {timeToComplete:new RegExp(searchQuery,'i')},
                {trainingUrl:new RegExp(searchQuery,'i')},
            ];
        }

        if(queryParams.status){
            if(!findQuery) findQuery = {};
            findQuery['status']= parseInt(queryParams.status);
        }

        if(queryParams.archive){
            if(!findQuery) findQuery = {};
            if (queryParams.archive == 'true') {
                findQuery['archive']= true;
            }
            if (queryParams.archive == 'false') {
                findQuery['archive']= false;
            }
        }

        if(queryParams.trainingSubject){
            if(!findQuery) findQuery = {};
            findQuery['trainingSubject']= queryParams.trainingSubject;
        }

        if(queryParams.trainingType){
            if(!findQuery) findQuery = {};
            findQuery['trainingType']= queryParams.trainingType;
        }

        if(queryParams.timeToComplete){
            if(!findQuery) findQuery = {};
            findQuery['timeToComplete']= queryParams.timeToComplete;
        }

        let aggregatePipes = [];
        if(findQuery){
            aggregatePipes.push(
                {
                    $match:findQuery
                }
            );
        }

        var aggregatePipeForCount = [];
        aggregatePipes.forEach(pipe=>{
            aggregatePipeForCount.push(pipe);
        });
        aggregatePipeForCount.push(
            {
                $group:{
                    _id:null,
                    count:{$sum:1}
                }
            }
        );

        aggregatePipes.push(
            {
                $sort: {updatedAt: -1}
            }
        );


        aggregatePipes.push(
            {
                $skip:perPage*(pageNumber-1)
            }
        );

        aggregatePipes.push(
            {
                $limit:perPage
            },
        );

        /*if(queryParams.requiredFields){
            aggregatePipes.push(
                {
                    $project:options
                },
            );
        }*/

        // console.log(aggregatePipes);
        return  Promise.all([
            TrainingModel.aggregate(aggregatePipes),
            TrainingModel.aggregate(aggregatePipeForCount),
        ]);
    }).then(([data,countJSON]) => {
        let count = 0;
        if(countJSON && countJSON.length>0) count=countJSON[0].count;
        res.status(200).json({ status: true, message: "Training data Fetched", data,count});

        // res.status(200).json({status: true, message: "Success", data:data});
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
 * Author:Sai Reddy  Date:03/04/2019 JiraId: https://zenwork.atlassian.net/browse/PROJ-9
 * Login API To GET Single Training Library data
 */
let del = (req, res) => {
    var id = req.params._id;
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return TrainingModel.findOneAndRemove({_id:id, companyId: req.jwt.companyId}).exec();
    }).then((data) => {
        if(data) {
            res.status(200).json({status: true, message: "Successfully Removed"});
        } else {
            res.status(404).json({status: false, message: "Data not found with that id on your company"});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

/**
*@author Asma
*@date 17/05/2019 07:37
API to assign training to employees
*/
let assignTrainingToUsers = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    let trainingId = body.trainingId
    let origin = body.origin
    delete body.origin 
    return new Promise((resolve, reject) => {
        if (!companyId) {
            reject('companyId is required');
        }
        else if (!trainingId) {
            reject('trainingId is required');
        }
        else {
            resolve(null);
        }
    })
    .then(() => {
        if (body._ids && body._ids.length > 0) {
            async.each(body._ids, (_id,callback) => {
                let find_filter = {
                    training: ObjectID(trainingId),
                    companyId: ObjectID(companyId),
                    userId: ObjectID(_id)
                }
                AssignedTrainingsModel.findOne(find_filter)
                    .then(assigned_training => {
                        if (assigned_training) {
                            callback()
                        }
                        else {
                            let today = new Date()
                            let nextDay = today.setDate(today.getDate() + 1)
                            let calculated_due_date = new Date(nextDay)
                            let insert_obj = {
                                training: ObjectID(trainingId),
                                companyId: ObjectID(companyId),
                                userId: ObjectID(_id),
                                assigned_date: new Date(),
                                due_date: calculated_due_date,
                                start_date: new Date()
                            }

                            if (origin && origin === 'FindTraining') {
                                delete insert_obj.due_date
                            }

                            AssignedTrainingsModel.create(insert_obj)
                                .then(response => {
                                    callback()
                                })
                                .catch(err => {
                                    callback(err)
                                });
                        }
                    })
                    .catch(err => {
                        callback(err)
                    })
            }, function(err) {
                if (!err) {
                    console.log('assigning training to users done')
                    res.status(200).json({
                        status: true,
                        message: "Successfully assigned trainings",
                        data: null
                    })
                }
                else {
                    console.log(err)
                    if(err.code == 11000) {
                        res.status(400).json({
                            status: false,
                            message: "Training is already assigned to one of the selected employees"
                        })
                    }
                    else {
                        res.status(400).json({
                            status: false,
                            message: "error while assigning trainings",
                            error: err
                        });
                    }   
                }
            })
        }
        else {
            res.status(400).json({
                status: false,
                message: "Please select employees to assign training"
            })
        }
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: err,
            data: null
        })
    })
}

// let changeTrainingStatus = (req, res) => 

let deleteMany = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!companyId) {
            reject("companyId is required")
        }
        else {
            resolve(null);
        }
    })
        .then(() => {
            TrainingModel.deleteMany({
                companyId: companyId,
                _id: { $in: body._ids }
            })
                .then(response => {
                    res.status(200).json({
                        status: true,
                        message: "Deleted successfully",
                        data: response
                    })
                })
                .catch(err => {
                    res.status(400).json({
                        status: false,
                        message: "Error while deleting trainings",
                        error: err
                    })
                })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: err
            })
        })
}

// archive or unarchive
let changeStatus = (req, res) => {
    let body = req.body
    let companyId = body.companyId
    return new Promise((resolve, reject) => {
        if (!body._ids) {
            reject('_ids are required')
        }
        else if (!body.hasOwnProperty("archive")) {
            reject('archive field is required')
        }
        else if (!companyId) {
            reject('companyId is required')
        }
        else {
            resolve(null);
        }
    })
    .then(()=> {
        delete body.companyId
        delete body._id
        delete body.createdAt
        delete body.updatedAt
        let filter = {
            _id: {$in: body._ids},
            companyId : ObjectID(companyId), 
        }
        console.log(filter)
        body.createdBy =  ObjectID(req.jwt.userId)
        TrainingModel.updateMany(filter, {$set: body}, {new: true, useFindAndModify: false})
            .then(response => {
                console.log(response)
                res.status(200).json({
                    status: true,
                    message: "Changed status successfully",
                    data: response
                })
            })
            .catch(err => {
                res.status(400).json({
                    status: false,
                    message: "Error while changing status",
                    error: err
                })
            })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            message: "Error while changing status",
            error: err
        })
    })
}

// unarchive all archived trainings
let unarchive = (req, res) => {
    let companyId = req.jwt.companyId
    return new Promise((resolve, reject) => {
        resolve(null)
    })
    .then(() => {
        let filter = {
            companyId: companyId,
            archive: true
        }
        TrainingModel.updateMany(
            filter, 
            {
                $set: {
                    archive: false
                }
            }, 
            {
                new: true, 
                useFindAndModify: false
            }
        )
        .then(response => {
            res.status(200).json({
                status: true,
                message: "Unarchived successfully",
                data: response
            })
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: true,
                message: "Error while unarchiving",
                error: err
            })
        })

    })
}

module.exports = {
    add,
    getSingleData,
    update,
    del,
    getAll,
    assignTrainingToUsers,
    deleteMany,
    changeStatus,
    unarchive
};
