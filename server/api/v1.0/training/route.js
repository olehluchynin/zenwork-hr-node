let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let TrainingController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.post('/add', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, authMiddleware.isCompany, TrainingController.add);

router.get('/getSingleData/:_id', authMiddleware.isUserLogin, authMiddleware.isCompany, TrainingController.getSingleData);

router.put('/update', validationsMiddleware.reqiuresBody, authMiddleware.isUserLogin, authMiddleware.isCompany, TrainingController.update);

router.get(
    '/getAll', 
    authMiddleware.isUserLogin, 
    // authMiddleware.isCompany, 
    TrainingController.getAll
);

router.delete('/:_id', authMiddleware.isUserLogin, authMiddleware.isCompany, TrainingController.del);

router.post(
    "/assignTraining",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    TrainingController.assignTrainingToUsers
);

router.post(
    "/deleteMany",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    TrainingController.deleteMany
);

router.put(
    "/changeStatus",
    authMiddleware.isUserLogin,
    validationsMiddleware.reqiuresBody,
    TrainingController.changeStatus
);

router.get(
    "/unarchive",
    authMiddleware.isUserLogin,
    TrainingController.unarchive
);

module.exports = router;
