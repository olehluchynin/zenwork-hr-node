let mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
let ObjectID = mongoose.Schema.ObjectId;
let constants  = require('./../../../common/constants');

let Schema = new mongoose.Schema({
    clientId:{type:ObjectID,ref:'companies'},
    id:{type:String,unique:true,required:true},
    type:{type:String,default:"plan",enum:['plan']},

    packages:[{id:{type:ObjectID,ref:'packages'},discount:Number,startDate:Date,endDate:Date,name:String, price:Number}],
    adons:[{id:{type:ObjectID,ref:'ad_ons'},startDate:Date,endDate:Date,name:String, price:Number}],

    // discount:
    startDate:Date,
    endDate:Date,
    amount:Number,
    paymentStatus:{type:String,default:'initialized',enum:['initialized','pending','failed','success']},
    paymentType:{type:String}, //,required:true
    paymentInfo:Object,
    billingFrequency:{type:String,default:'monthly'},
    createdBy:{type:ObjectID,ref:'users'},
    updatedBy:{type:ObjectID,ref:'users'},
    createdDate:{type:Date, default:Date.now()},
    updatedDate:{type:Date,default:Date.now()},
},{
    timestamps: true,
    versionKey: false
});

Schema.plugin(autoIncrement.plugin, {model:'orders', field:'orderId', startAt: 1, incrementBy: 1});

// Schema.index({loc: '2dsphere'}); //loc: { type: "Point", coordinates: [ longitude, latitude ] },

let OrdersModel = mongoose.model('orders',Schema);

module.exports = OrdersModel;
