let express = require('express');

let router = express.Router();

let authMiddleware = require('./../middlewares/auth');

let PlansController = require('./controller');

let validationsMiddleware = require('../middlewares/validations');

router.post('/createOrder', validationsMiddleware.reqiuresBody/*, authMiddleware.isUserLogin, authMiddleware.isZenworkerLogin, authMiddleware.isSrSupport*/, PlansController.createOrder);

// router.post('/createSelfOrder', validationsMiddleware.reqiuresBody, PlansController.createOrder);

router.post('/all', authMiddleware.isUserLogin, PlansController.getAll);

router.put('/callback/paypal/:_id', authMiddleware.isUserLogin, authMiddleware.isZenworkerLogin, validationsMiddleware.reqiuresBody, 
// authMiddleware.isSrSupport, 
authMiddleware.isSrSupportOrZenworkerOrAdministrator,
PlansController.paypalCallBack);

router.get('/:_id', authMiddleware.isUserLogin, 
// authMiddleware.isSrSupport, 
authMiddleware.isSrSupportOrZenworkerOrAdministrator,
PlansController.getById);

router.post('/credit-charge',/*authMiddleware.isUserLogin,*/PlansController.addPayment);

router.get('/BillingOfCompany/:_id', authMiddleware.isUserLogin, PlansController.BillingOfCompany);

router.get('/CurrentPlan/:_id', authMiddleware.isUserLogin, PlansController.CurrentPlan);

router.post('/emailInvoice', authMiddleware.isUserLogin, PlansController.emailInvoice);

router.post('/email-invoice', authMiddleware.isUserLogin, PlansController.emailInvoiceNew);

module.exports = router;
