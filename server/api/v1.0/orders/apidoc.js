/**
 * @apiDefine DataNotFoundError
 *
 * @apiError DataNotFound The data the you are requested was not found.
 *
 * @apiErrorExample Data Not Found Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "message": "Data Not Found"
 *       "data": null 
 *     }
 */


 /**
 * @apiDefine InternalServerError
 *
 * @apiError InternalServerError Internal Server error.
 *
 * @apiErrorExample Internal Server Error:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "status": false,
 *       "message": "Internal server error"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine TokenNotFound
 *
 * @apiError TokenNotFound User Token Not Found.
 *
 * @apiErrorExample Token Not Found:
 *     HTTP/1.1 400 Token Not Found
 *     {
 *       "status": false,
 *       "message": "Token Not Found"
 *       "data": null 
 *     }
 */

  /**
 * @apiDefine BadRequest
 *
 * @apiError BadRequest Bad request.
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": false,
 *       "message": "Invalid key"
 *       "data": null 
 *     }
 */

/**
 * @apiDefine AccessDeneid
 *
 * @apiError AccessDenied Access denied for this operation.
 *
 * @apiErrorExample Access Deneid:
 *     HTTP/1.1 401 Bad Request
 *     {
 *       "status": false,
 *       "message": "Access Denied for this operation"
 *       "data": null 
 *     }
 */

 /**
 * @apiDefine DataAlreadyExists
 *
 * @apiError DataConflict Date already exists.
 *
 * @apiErrorExample Data Conflict Error:
 *     HTTP/1.1 402 Data Conflict
 *     {
 *       "status": false,
 *       "message": "Data already exist"
 *       "data": null 
 *     }
 */



 /**
 * @api {post} /zenworkers/add Add new
 * @apiName AddNewZenworker
 * @apiGroup Zerworkers
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
 *{
    "name":"Admin",
    "email":"rajesh.d@gmail.com",
    "role":"billing", // billing,sr-support,support
    "adddress":{
        "address1":"MTWLABS",
        "address2":"MADHAPUR",
        "city":"Hyderabad",
        "zipcode":"500067",
        "state":"TTG"
    },
    "loc":{
    	"type":"Point",
    	"coordinates": [ 1.00000000, 1.00000000000 ]
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "New Zenworker added successfully",
    "data": {
        "name":"Admin",
        "email":"rajesh.d@gmail.com",
        "role":"billing",
        "adddress":{
            "address1":"MTWLABS",
            "address2":"MADHAPUR",
            "city":"Hyderabad",
            "zipcode":"500067",
            "state":"TTG"
        },
        "loc":{
            "type":"Point",
            "coordinates": [ 1.00000000, 1.00000000000 ]
        }
        "createdBy": "5c0e2d985464f22d65ee070b",
        "__v": 0
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */







 /**
 * @api {get} /zenworkers/ Get logged zenworker info
 * @apiName GetLoggedZenworkedData
 * @apiGroup Zerworkers
 * @apiHeader {String} x-access-token User login token.
*
 * @apiParam {String} _id zenworker unique _id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "User details fetched successfully",
    "data": {
        "location": {
            "address1": "MTWLABS",
            "address2": "MADHAPUR",
            "city": "Hyderabad",
            "zipcode": "500067",
            "state": "TTG"
        },
        "status": "inactive",
        "createdDate": "2018-12-10T09:49:26.292Z",
        "updatedDate": "2018-12-10T09:49:26.292Z",
        "_id": "5c0e36ddf3876337626e1651",
        "name": "Rajesh",
        "email": "billing@gmail.com",
        "role": "billing",
        "createdBy": "5c0e2d985464f22d65ee070b",
        "__v": 0
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound
 */



 /**
 * @api {get} /zenworkers/:_id Get Zenworker Info
 * @apiName GetZenworkedData
 * @apiGroup Zerworkers
  * @apiHeader {String} x-access-token User login token.

 * @apiParam {String} _id zenworker unique _id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "User details fetched successfully",
    "data": {
        "location": {
            "address1": "MTWLABS",
            "address2": "MADHAPUR",
            "city": "Hyderabad",
            "zipcode": "500067",
            "state": "TTG"
        },
        "status": "inactive",
        "createdDate": "2018-12-10T09:49:26.292Z",
        "updatedDate": "2018-12-10T09:49:26.292Z",
        "_id": "5c0e36ddf3876337626e1651",
        "name": "Rajesh",
        "email": "billing@gmail.com",
        "role": "billing",
        "createdBy": "5c0e2d985464f22d65ee070b",
        "__v": 0
    }
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */





 /**
 * @api {post} /orders/all GetAllOrders
 * @apiName GetAllOrders
 * @apiGroup Orders
 * @apiHeader {String} x-access-token User login token.
 * @apiParamExample {json} Request-Example:
{
	"per_page": 8,
	"page_no": 1,
	"from_date": "2019-03-29T12:24:08.604Z",
	"to_date": "2019-05-27T12:35:11.191Z",
	"search_query": "",
	"client_type": "reseller",
	"paymentStatus": "success"
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Successfully fetched orders",
    "total_count": 4,
    "data": [
        {
            "_id": "5c9e42692bdee465121c80e7",
            "type": "plan",
            "paymentStatus": "success",
            "billingFrequency": "monthly",
            "createdDate": "2019-03-29T12:55:37.206Z",
            "clientId": "5c9e425c2bdee465121c80e5",
            "endDate": "2019-04-29T16:05:18.841Z",
            "packages": [
                {
                    "_id": "5c9e42692bdee465121c80e8",
                    "name": "Premium",
                    "price": 1000,
                    "startDate": "2019-03-29T16:05:18.841Z",
                    "endDate": "2019-04-29T16:05:18.841Z",
                    "id": "5c7544b7a81a0215bb704c7e"
                }
            ],
            "adons": [
                {
                    "_id": "5c9e42692bdee465121c80e9",
                    "id": "5c8960302e43cc36dc1d21f8",
                    "price": 1234,
                    "startDate": "2019-03-29T16:05:18.841Z",
                    "endDate": "2019-04-29T16:05:18.841Z"
                }
            ],
            "id": "ZENgwznkeynmd",
            "orderId": 6,
            "amount": 240408,
            "paymentInfo": {
                "id": "card_1EJNEZDraju5duH00cTa6kZp",
                "object": "card",
                "address_city": null,
                "address_country": null,
                "address_line1": null,
                "address_line1_check": null,
                "address_line2": null,
                "address_state": null,
                "address_zip": null,
                "address_zip_check": null,
                "brand": "Visa",
                "country": "US",
                "customer": null,
                "cvc_check": "pass",
                "dynamic_last4": null,
                "exp_month": 2,
                "exp_year": 2021,
                "fingerprint": "nF0X91IZl2eCLh4l",
                "funding": "credit",
                "last4": "4242",
                "metadata": {},
                "name": null,
                "tokenization_method": null
            },
            "paymentType": "credit card",
            "company": {
                "_id": "5c9e425c2bdee465121c80e5",
                "stepsCompleted": 4,
                "createdDate": "2019-03-29T12:55:37.015Z",
                "updatedDate": "2019-03-29T12:55:37.015Z",
                "name": "test1",
                "tradeName": "v2",
                "employeCount": 12,
                "email": "vi@gmail.com",
                "type": "reseller",
                "createdBy": "5c6ba80fe342799adf1b177c",
                "companyId": 7,
                "__v": 0,
                "settingsId": "5c9e425c2bdee465121c80e6",
                "userId": "5c9e425c2bdee465121c80e4",
                "billingAddress": {
                    "address1": "",
                    "address2": "",
                    "city": "",
                    "state": "",
                    "zipcode": "",
                    "country": ""
                },
                "billingContact": {
                    "name": "",
                    "email": "",
                    "phone": "",
                    "extention": ""
                },
                "isPrimaryAddressIsBillingAddress": true,
                "isPrimaryContactIsBillingContact": true,
                "primaryAddress": {
                    "address1": "uhudshc",
                    "address2": "vsvjscvn",
                    "city": "cvxkcjvn",
                    "state": "newyork",
                    "zipcode": "21212",
                    "country": "United States"
                },
                "primaryContact": {
                    "name": "viii",
                    "email": "vi@gmail.com",
                    "phone": "2121212212",
                    "extention": "555"
                }
            }
        },
        {
            "_id": "5c9e42df2bdee465121c80ea",
            "type": "plan",
            "paymentStatus": "failed",
            "billingFrequency": "monthly",
            "createdDate": "2019-03-29T12:55:37.206Z",
            "clientId": "5c9e40492bdee465121c80e2",
            "endDate": "2019-03-29T16:07:39.147Z",
            "packages": [
                {
                    "_id": "5c9e42df2bdee465121c80eb",
                    "name": "Basic",
                    "price": 500,
                    "startDate": "2019-03-29T16:07:39.147Z",
                    "endDate": "2019-03-29T16:07:39.147Z",
                    "id": "5c6bb584a81a0215bb704c3f"
                }
            ],
            "adons": [
                {
                    "_id": "5c9e42df2bdee465121c80ec",
                    "id": "5c73c118a81a0215bb704c5a",
                    "price": 200,
                    "startDate": "2019-03-29T16:07:39.147Z",
                    "endDate": "2019-03-29T16:07:39.147Z"
                }
            ],
            "id": "ZEN5uk5nqpwj1",
            "orderId": 7,
            "company": {
                "_id": "5c9e40492bdee465121c80e2",
                "stepsCompleted": 3,
                "createdDate": "2019-03-29T12:55:37.015Z",
                "updatedDate": "2019-03-29T12:55:37.015Z",
                "name": "test",
                "tradeName": "report",
                "employeCount": 25,
                "email": "test@gmail.com",
                "type": "reseller",
                "createdBy": "5c6ba80fe342799adf1b177c",
                "companyId": 6,
                "__v": 0,
                "settingsId": "5c9e40492bdee465121c80e3",
                "userId": "5c9e40492bdee465121c80e1",
                "billingAddress": {
                    "address1": "",
                    "address2": "",
                    "city": "",
                    "state": "",
                    "zipcode": "",
                    "country": ""
                },
                "billingContact": {
                    "name": "",
                    "email": "",
                    "phone": "",
                    "extention": ""
                },
                "isPrimaryAddressIsBillingAddress": true,
                "isPrimaryContactIsBillingContact": true,
                "primaryAddress": {
                    "address1": "hyd",
                    "address2": "hyd22",
                    "city": "hyd23",
                    "state": "washington",
                    "zipcode": "23",
                    "country": "United States"
                },
                "primaryContact": {
                    "name": "vipin",
                    "email": "test@gmail.com",
                    "phone": "1212121212",
                    "extention": "220"
                }
            }
        },
        {
            "_id": "5c9e43322bdee465121c80ed",
            "type": "plan",
            "paymentStatus": "failed",
            "billingFrequency": "monthly",
            "createdDate": "2019-03-29T12:55:37.206Z",
            "clientId": "5c9e40492bdee465121c80e2",
            "endDate": "2019-03-29T16:07:39.147Z",
            "packages": [
                {
                    "_id": "5c9e43322bdee465121c80ee",
                    "name": "Basic",
                    "price": 500,
                    "startDate": "2019-03-29T16:07:39.147Z",
                    "endDate": "2019-03-29T16:07:39.147Z",
                    "id": "5c6bb584a81a0215bb704c3f"
                }
            ],
            "id": "ZENmpntglq825",
            "adons": [],
            "orderId": 8,
            "company": {
                "_id": "5c9e40492bdee465121c80e2",
                "stepsCompleted": 3,
                "createdDate": "2019-03-29T12:55:37.015Z",
                "updatedDate": "2019-03-29T12:55:37.015Z",
                "name": "test",
                "tradeName": "report",
                "employeCount": 25,
                "email": "test@gmail.com",
                "type": "reseller",
                "createdBy": "5c6ba80fe342799adf1b177c",
                "companyId": 6,
                "__v": 0,
                "settingsId": "5c9e40492bdee465121c80e3",
                "userId": "5c9e40492bdee465121c80e1",
                "billingAddress": {
                    "address1": "",
                    "address2": "",
                    "city": "",
                    "state": "",
                    "zipcode": "",
                    "country": ""
                },
                "billingContact": {
                    "name": "",
                    "email": "",
                    "phone": "",
                    "extention": ""
                },
                "isPrimaryAddressIsBillingAddress": true,
                "isPrimaryContactIsBillingContact": true,
                "primaryAddress": {
                    "address1": "hyd",
                    "address2": "hyd22",
                    "city": "hyd23",
                    "state": "washington",
                    "zipcode": "23",
                    "country": "United States"
                },
                "primaryContact": {
                    "name": "vipin",
                    "email": "test@gmail.com",
                    "phone": "1212121212",
                    "extention": "220"
                }
            }
        },
        {
            "_id": "5cc93f36b51d4b25d2b7c500",
            "type": "plan",
            "paymentStatus": "initialized",
            "billingFrequency": "monthly",
            "createdDate": "2019-05-01T06:08:29.774Z",
            "clientId": "5cc937789ee892259a5a9bae",
            "endDate": "2019-06-01T06:39:38.345Z",
            "packages": [
                {
                    "_id": "5cc93f36b51d4b25d2b7c501",
                    "name": "Basic",
                    "price": 500,
                    "startDate": "2019-05-01T06:06:02.335Z",
                    "endDate": "2019-06-01T06:39:38.345Z",
                    "id": "5c6bb584a81a0215bb704c3f"
                }
            ],
            "adons": [
                {
                    "_id": "5cc93f36b51d4b25d2b7c503",
                    "id": "5c73c118a81a0215bb704c5a",
                    "price": 200,
                    "startDate": "2019-05-01T06:06:02.335Z",
                    "endDate": "2019-06-01T06:39:38.345Z"
                },
                {
                    "_id": "5cc93f36b51d4b25d2b7c502",
                    "id": "5c754a4ea81a0215bb704c7f",
                    "price": 2000,
                    "startDate": "2019-05-01T06:06:02.335Z",
                    "endDate": "2019-06-01T06:39:38.345Z"
                }
            ],
            "id": "ZENeiupizurct",
            "orderId": 45,
            "company": {
                "_id": "5cc937789ee892259a5a9bae",
                "primaryContact": {
                    "name": "Sathish",
                    "email": "elitest21@gmail.com",
                    "phone": "",
                    "extention": ""
                },
                "secondaryContact": {
                    "isEmailVerified": false
                },
                "billingContact": {
                    "name": "",
                    "email": "",
                    "phone": "",
                    "extention": ""
                },
                "stepsCompleted": 3,
                "createdDate": "2019-05-01T06:06:31.597Z",
                "updatedDate": "2019-05-01T06:06:31.597Z",
                "name": "Eli21",
                "tradeName": "Elitest21",
                "employeCount": 10,
                "email": "elitest21@gmail.com",
                "type": "reseller",
                "createdBy": "5c6ba80fe342799adf1b177c",
                "companyId": 40,
                "__v": 0,
                "settingsId": "5cc937789ee892259a5a9baf",
                "userId": "5cc937789ee892259a5a9bad",
                "billingAddress": {
                    "address1": "",
                    "address2": "",
                    "city": "",
                    "state": "",
                    "zipcode": "",
                    "country": ""
                },
                "isPrimaryAddressIsBillingAddress": true,
                "isPrimaryContactIsBillingContact": true,
                "primaryAddress": {
                    "address1": "",
                    "address2": "",
                    "city": "",
                    "state": "",
                    "zipcode": "",
                    "country": ""
                }
            }
        }
    ]
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */



 /**
 * @api {put} /zenworkers/:_id Update others info
 * @apiName UpdateNewZenworker
 * @apiGroup Zerworkers
 * 
 * @apiParam {String} _id zenworker unique _id.
 * 
 * @apiPermission sr-support
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
 *{
    "name":"Admin",
    "adddress":{
        "address1":"MTWLABS",
        "address2":"MADHAPUR",
        "city":"Hyderabad",
        "zipcode":"500067",
        "state":"TTG"
    },
    "loc":{
    	"type":"Point",
    	"coordinates": [ 1.00000000, 1.00000000000 ]
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Zenworker updated successfully",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */




 /**
 * @api {put} /zenworkers/ Update self info
 * @apiName UpdateSelfInfo
 * @apiGroup Zerworkers
 * 
 * 
 * @apiHeader {String} x-access-token User login token.
 * 
 * @apiParamExample {json} Request-Example:
 *
 *{
    "name":"Admin",
    "adddress":{
        "address1":"MTWLABS",
        "address2":"MADHAPUR",
        "city":"Hyderabad",
        "zipcode":"500067",
        "state":"TTG"
    },
    "loc":{
    	"type":"Point",
    	"coordinates": [ 1.00000000, 1.00000000000 ]
    }
}
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "status": true,
    "message": "Zenworker updated successfully",
    "data": null
}
 *
 * @apiUse DataNotFoundError
 * @apiUse InternalServerError
 * @apiUse BadRequest
 * @apiUse AccessDeneid
 * @apiUse TokenNotFound 
 */