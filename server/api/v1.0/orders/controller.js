let OrdersModel = require('./model');
let CompaniesModel = require('./../companies/model');
let utils = require('./../../../common/utils');
let mailer = require('./../../../common/mailer');
let constants = require('./../../../common/constants');
let fs = require('fs');
var Handlebars = require('handlebars');
let PlansModel = require('./../plans/model');
let TaxRateModel = require('../zenwork/models').TaxRateModel
let mongoose = require('mongoose');
let ObjectID = mongoose.Types.ObjectId;

var stripe = require("stripe")("sk_test_2vBgujDEy5qGzeHFXEBZNJXp");

let createOrder = (req, res) => {
    var body = req.body;
    var order_id = body._id
    delete body._id
    return new Promise((resolve, reject) => {
        if (!body.packages) reject("Please select a package.");
        else if (!body.clientId) reject("Invalid clientId");
        else resolve(null);
        // else if (!type) reject("Invalid type"); 
        // if (!body.paymentType) reject("Invalid paymentType");      
        /* else */
    }).then(() => {
        if (order_id) {
            order_id = ObjectID(order_id)
            body.paymentStatus = 'initialized';
            if (body.adons) {
                body.adons.forEach(function (adon) {
                    adon.startDate = body.startDate;
                    adon.endDate = body.endDate;
                });
            }
            body.packages.forEach(function (pack) {
                pack.id = pack._id;
                pack.startDate = body.startDate;
                pack.endDate = body.endDate;
                delete pack._id;
            });
            
            return OrdersModel.findOneAndUpdate({_id: order_id}, {$set: body})
        }
        else {
            body.id = utils.getUniqueOrderID();
            body.paymentStatus = 'initialized';
            if (body.adons) {
                body.adons.forEach(function (adon) {
                    adon.startDate = body.startDate;
                    adon.endDate = body.endDate;
                });
            }
            body.packages.forEach(function (pack) {
                pack.id = pack._id;
                pack.startDate = body.startDate;
                pack.endDate = body.endDate;
                delete pack._id;
            });
            return OrdersModel.create(body);
        }
        
    }).then(order => {
        CompaniesModel.update({_id: body.clientId}, {$set: {stepsCompleted: '2'}});
        if (order_id) {
            res.status(200).json({status: true, message: "Order updated successfully", data: order});    
        }
        else {
            res.status(200).json({status: true, message: "Order created successfully", data: order});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let getById = (req, res) => {

    let orderID = req.params._id;

    return new Promise((resolve, reject) => {
        if (!orderID) reject("Invalid userId");
        else resolve(null);
    }).then(() => {
        return OrdersModel.findOne({_id: orderID});
    }).then(order => {
        if (!order) {
            res.status(400).json({status: false, message: "Data not found", data: null});
            return null;
        } else {
            res.status(200).json({status: true, message: "User details fetched successfully", data: plan});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let paypalCallBack = (req, res) => {

    let body = req.body;

    let orderId = req.params._id;

    return new Promise((resolve, reject) => {
        7;
        if (!orderId) reject("Invalid orderId");
        else resolve(null);
    }).then(() => {
        return OrdersModel.findOne({_id: orderId});
    }).then(order => {
        if (!order) {
            res.status(400).json({status: false, message: "Data not found", data: null});
            return null;
        } else {
            order.status = body.status;
            if (order.status == 'success') {

                var createPromises = [];
                if (order.type = 'plan') {
                    if (order.packages) {
                        order.packages.forEach(package => {
                            var planData = {
                                clientId: order.clientId,
                                type: 'package',
                                package: package._id,
                                startDate: package.startDate,
                                endDate: package.endDate,
                                status: 'active',
                                orderId: order._id,
                            };
                            createPromises.push(PlansModel.create(planData));
                        });
                    }
                    if (order.adons) {
                        order.adons.forEach(adon => {
                            var planData = {
                                clientId: order.clientId,
                                type: 'ad-on',
                                adon: adon._id,
                                startDate: package.startDate,
                                endDate: package.endDate,
                                status: 'active',
                                orderId: order._id,
                            };
                            createPromises.push(PlansModel.create(planData));
                        });
                    }
                }
                return Promise.all(createPromises);
            } else {
                res.status(500).json({status: true, message: "Payment failed", data: null});
            }
        }
    }).then(updated => {
        if (updated) {
            res.status(200).json({status: true, message: "Payment done successfully", data: null});
        }
    }).catch((err) => {
        console.log(err);
        res.status(400).json({status: false, message: err, data: null});
    });
};

let getAll = (req, res) => {
    let body = req.body
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        let query = {
        }

        let per_page = body.per_page ? body.per_page : 10
        let page_no = body.page_no ? body.page_no : 1
        let sort_obj = {
            "updatedAt": -1
        }

        if (body.from_date && body.to_date) {
            let from_date = new Date(body.from_date)
            let to_date = new Date(body.to_date)
            from_date.setHours(0, 0, 0, 0);
            to_date.setHours(23, 59, 59, 999);
            console.log(from_date);
            console.log(to_date);
            query.createdDate = {
                '$gte': from_date,
                '$lte': to_date
            }
        }
        else if (body.from_date) {
            let from_date = new Date(body.from_date)
            from_date.setHours(0, 0, 0, 0);
            console.log(from_date);
            query.createdDate = {
                '$gte': from_date
            }
        }

        if(body.search_query) {
            query['$or'] = [
                {
                    'company.name' : {$regex: body.search_query, $options: "i"} 
                }
            ]
        }

        if (body.hasOwnProperty('client_type') && body.client_type) {
            query['company.type'] = body.client_type
        }

        if (body.hasOwnProperty('paymentStatus') && body.paymentStatus) {
            query['paymentStatus'] = body.paymentStatus
        }

        let lookup_stage = {
            $lookup:
            {
                from: "companies",
                localField: "clientId",
                foreignField: "_id",
                as: "company"
            }
        }

        let project_stage = {
            $project: {
                "company": { $arrayElemAt: ["$company", 0] },
                type: 1,
                paymentStatus: 1,
                billingFrequency: 1,
                createdDate: 1,
                updatedDate: 1,
                clientId: 1,
                start_date: 1,
                endDate: 1,
                packages: 1,
                adons: 1,
                id: 1,
                orderId: 1,
                amount:1,
                paymentType: 1, 
                paymentInfo: 1,
                createdBy: 1,
                updatedBy: 1
            }
        }

        let match_stage = {
            $match: query
        }

        let sort_stage = {
            $sort: sort_obj
        }

        let skip_stage = {
            $skip: (page_no - 1) * per_page
        }

        let limit_stage = {
            $limit: per_page
        }

        Promise.all([
            OrdersModel.aggregate([
                lookup_stage,
                project_stage,
                match_stage,
                sort_stage,
                skip_stage,
                limit_stage
            ]),
            OrdersModel.aggregate([
                lookup_stage,
                project_stage,
                match_stage,
                {
                    $count: "total_count"
                }
            ])
        ])
        .then(([orders, total_count_array]) => {
            console.log(total_count_array)
            res.status(200).json({
                status: true,
                message: "Successfully fetched orders",
                total_count: total_count_array[0] ? total_count_array[0].total_count : 0,
                data: orders
            })
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({
                status: false,
                message: "Error while fetching orders",
                error: err
            })
        })
        // return OrdersModel.find({}).populate({path: 'clientId', select: 'name type'});
    }).catch((err) => {
        console.log(err);
        if (typeof err == 'object') {
            res.status(500).json({status: false, message: "Internal server error", data: null});
        } else {
            res.status(400).json({status: false, message: err, data: null});
        }
    });
};

let addPayment = (req, res) => {
    console.log("checking stripe", req.body, req.headers);
    stripe.charges.create({
        amount: Number(req.body.amount),
        currency: "usd",
        source: req.body.token, // obtained with Stripe.js
        description: "Testing"
    }, function (err, charge) {
        // console.log("error", err);
        // console.log("charge", charge);
        if (err) {
            console.log("error", err);
            OrdersModel.findOneAndUpdate({_id: req.body.orderId}, {
                $set: {
                    paymentStatus: 'failed'
                }
            }).then((doc) => {
                // console.log("docc", doc)
                if (doc) {
                    res.status(400).json({
                        status: false,
                        message: "Error occured during payment , please try again",
                        data: err
                    })

                } else {
                    res.status(400).json({
                        status: false,
                        message: "Error occured during payment ,please try again",
                        data: null
                    })

                }
            }).catch((err) => {
                console.log(err);
                res.status(500).json({
                    status: false,
                    message: "Error occured during payment1 , please try again",
                    data: err
                })

            })
        } else {
            let paymentType = charge.source.funding;
            if (paymentType === 'credit') paymentType = 'credit card';
            OrdersModel.findOneAndUpdate({_id: req.body.orderId}, {
                $set: {
                    paymentInfo: charge.source,
                    paymentType: paymentType,
                    paymentStatus: 'success',
                    amount: req.body.amount
                }
            }).then((doc) => {
                console.log("docc", doc);
                if (doc) {
                    CompaniesModel.findOneAndUpdate({_id: req.body.id}, {$set: {stepsCompleted: '4'}}).populate('userId',{email:1,password:1}).then((company) => {
                        let password = company.userId.password;
                        delete company.userId.password;
                        // mailer.sendUsernamePasswordToCompany(company.userId.email, company.name, company.userId.email, company.userId.password, constants.companyTypes[company.type]);
                        res.status(200).json({status: true, message: "Payment Success ", data: doc, company: company})
                    });

                } else {
                    res.status(400).json({
                        status: false,
                        message: "Error occured during payment ,please try again123",
                        data: null
                    })

                }
            }).catch((err) => {
                console.log(err);
                res.status(500).json({
                    status: false,
                    message: "Error occured during payment2 , please try again",
                    data: err
                })

            })
        }
    });
};


let BillingOfCompany = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return OrdersModel.find({clientId: req.params._id}, {paymentInfo: 0}).populate({
            path: 'clientId',
            select: 'name type'
        }).populate('packages.id', {name: 1}).populate('adons.id', {name: 1}).lean().exec();
    }).then(orders => {
        for (let order of orders) {
            for (let package of order.packages) {
                console.log(package)
                // if (package.startDate && package.endDate) {
                //     if (package.startDate.getTime() < new Date().getTime() && package.endDate.getTime() > new Date().getTime()) {
                //         package.status = 'Active'
                //         console.log(package)
                //     }
                //     else if (new Date().getTime() > package.endDate.getTime()) {
                //         package.status = 'Expired'
                //     }
                // }
                // else {
                //     package.status = 'Active'
                // }
                package.payment_status = order.paymentStatus
            }
            for (let addon of order.adons) {
                // if (addon.startDate && addon.endDate) {
                //     if (addon.startDate.getTime() < new Date().getTime() && addon.endDate.getTime() > new Date().getTime()) {
                //         addon.status = 'Active'
                //     }
                //     else if (new Date().getTime() > addon.endDate.getTime()) {
                //         addon.status = 'Expired'
                //     }
                // }
                // else {
                //     addon.status = 'Active'
                // }
                addon.payment_status = order.paymentStatus
            }
        }
        res.status(200).json({status: true, message: "Data Fetched", data: orders});
    }).catch((err) => {
        console.log(err);
        if (typeof err == 'object') {
            res.status(500).json({status: false, message: "Internal server error", data: null});
        } else {
            res.status(400).json({status: false, message: err, data: null});
        }
    });
};

let CurrentPlan = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        return OrdersModel.findOne({clientId: req.params._id, paymentStatus: 'success'}, {
            packages: 1,
            adons: 1,
            startDate: 1,
            endDate: 1,
            billingFrequency: 1,
            amount: 1
        }).populate('packages.id', {name: 1}).populate('adons.id', {name: 1}).sort({_id: -1});
    }).then(plan => {
        res.status(200).json({status: true, message: "Data Fetched", data: plan});
    }).catch((err) => {
        console.log(err);
        if (typeof err == 'object') {
            res.status(500).json({status: false, message: "Internal server error", data: null});
        } else {
            res.status(400).json({status: false, message: err, data: null});
        }
    });
};

let emailInvoice = (req, res) => {
    return new Promise((resolve, reject) => {
        resolve(null);
    }).then(() => {
        mailer.sendUsernamePasswordToCompany('sai@mtwlabs.com', 'Sai Reddy', 'sai@mtwlabs.com', '12345', 'Reseller');
    }).catch((err) => {
        console.log(err);
        if (typeof err == 'object') {
            res.status(500).json({status: false, message: "Internal server error", data: null});
        } else {
            res.status(400).json({status: false, message: err, data: null});
        }
    })
};

let emailInvoiceNew = (req, res) => {
    let body = req.body
    let clientId = body.clientId
    let orderId = body.orderId
    return new Promise((resolve, reject) => {
        if (!orderId) reject('orderId is required')
        else if (!clientId) reject('clientId is required')
        else resolve(null);
    })
    .then(() => {
        Promise.all([
            OrdersModel.findOne({clientId: clientId, _id: orderId})
            .populate('clientId')
            .populate('packages.id')
            .populate('adons.id').lean().exec(),
            TaxRateModel.findOne({}).lean().exec()
        ])
        .then(([order, taxrate]) => {
            order.tax_rate = taxrate.tax_rate
            order.createdAt = order.createdAt.toLocaleDateString()
            order.package = order.packages[0]
            order.package.startDate = order.package.startDate.toLocaleDateString()
            order.package.endDate = order.package.endDate.toLocaleDateString()
            order.total_employee_cost = order.package.id.costPerEmployee * order.clientId.employeCount
            let addons_prices = order.adons.map(addon => addon.price)
            order.totalPrice = addons_prices.reduce((a, b) => a + b) + order.total_employee_cost;
            order.tax_amount = order.totalPrice * (order.tax_rate / 100)
            order.grand_total = order.totalPrice + order.tax_amount
            order.tax_amount = order.tax_amount.toFixed(2)
            order.discount = 5
            console.log(order)
            console.log(order.package.id)
            console.log(order.adons[0].id)
            fs.readFile(__dirname + '/template.hbs', 'utf8', (err, file_data) => {
                if(!err) {
                    // console.log(file_data)
                    let html_data = (Handlebars.compile(file_data))(order)
                    mailer.sendInvoiceMail('vipin.reddy@mtwlabs.com', 'zenwork - invoice', html_data)    
                    res.status(200).json({
                        status: true,
                        message: "Invoice sent successfully"
                    })
                }
                else {
                    console.log(err)
                    res.status(400).json({
                        status: false,
                        message: "Error while sending invoice mail",
                        error: err
                    })
                }
                
            })
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: "Error while sending invoice mail",
                error: err
            })
        })
    })
    .catch(err => {
        console.log(err)
        res.status(400).json({
            status: false,
            message: err
        })
    })
}

module.exports = {
    createOrder,
    getById,
    paypalCallBack,
    getAll,
    addPayment,
    BillingOfCompany,
    CurrentPlan,
    emailInvoice,
    emailInvoiceNew
};
