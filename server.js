var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

let chalk = require('chalk');

var app = express();

// app.use(logger('dev'));
//don't show the log when it is test
if (process.env.NODE_ENV !== 'test') {
    //use morgan to log at command line
    app.use(logger('[:date[iso]] :method :url :status :response-time ms - :res[content-length]'))
}

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

// app.use(express.static('client', { index: '/views/index.html' }));

app.use("/api/client", express.static('client'));

let routesV1_0 = require('./server/routes/routes.v1.0');

require('./server/config/libs/mongoose');//initializing mongoose

let config = require('./server/config/config');

let awsConfig = require('./server/common/s3/awsConfig');

let fb = require('./server/config/libs/fb');

fb.initialize(app);

/* CORS ISSUE */
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', "*");

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'x-access-token,authorization,Content-Type,Access-Control-Request-Headers,enctype');

    // Set to true if you need the website to include cookies in  requests
    res.setHeader('Access-Control-Allow-Credentials', true);

    if (req.method === 'OPTIONS') {
        res.status(200);
        res.end();
    }
    else {
        // Pass to next layer of middleware
        next();
    }
});
/* CORS */


const dbConfig = {
    "database": config.db.mongo.database,
    "host": config.db.mongo.host,
    "username": config.db.mongo.options.user,
    "password": config.db.mongo.options.pass,
    "port": 27017
}


const s3Config = {
    accessKey: awsConfig.aws.accessKey,  //AccessKey
    secretKey: awsConfig.aws.secretKey,  //SecretKey
    region: awsConfig.aws.region,     //S3 Bucket Region
    accessPerm: "private", //S3 Bucket Privacy, Since, You'll be storing Database, Private is HIGHLY Recommended
    bucketName: awsConfig.aws.bucketName //Bucket Name
}

const backUpConfig = {
    cronJobTimeString: "0 1 * * *",
    fromMailId: config.mail.defaultFromAddress,
    mailIds: "asma.mubeen@mtwlabs.com"
}

require('./mongo-back-up')(dbConfig, s3Config, backUpConfig)
    .then(success => {
        console.log("Successfully initialized");
    }).catch(err => {
        console.log(err);
        console.log("Initializing failed");
    });

app.use('/apidocs', express.static('apidocs'));


app.use('/api/v1.0', routesV1_0);
app.post('/callback', function (req, res) {
    console.log(req);
});





app.listen(config.port);

console.log(chalk.green('Server started on port : ' + config.port + " with " + process.env.NODE_ENV + ' mode'));

module.exports = app;


//dev ip 13.233.37.94
